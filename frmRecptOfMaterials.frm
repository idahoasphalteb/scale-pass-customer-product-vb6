VERSION 5.00
Begin VB.Form frmRecptOfMaterials 
   BackColor       =   &H00FF00FF&
   Caption         =   "Load Processing (Reciept Of Goods) - Valero"
   ClientHeight    =   9450
   ClientLeft      =   3420
   ClientTop       =   3540
   ClientWidth     =   10485
   Icon            =   "frmRecptOfMaterials.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   9450
   ScaleWidth      =   10485
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrAutoReturnNoActivity 
      Interval        =   10000
      Left            =   0
      Top             =   0
   End
   Begin VB.CheckBox Check6 
      Caption         =   "Manual Scaling ON"
      Height          =   285
      Left            =   1485
      TabIndex        =   117
      Top             =   9000
      Width           =   1995
   End
   Begin VB.Frame Frame5 
      Caption         =   "Rear Load Specifications"
      Height          =   1680
      Left            =   5310
      TabIndex        =   93
      Top             =   1215
      Width           =   4965
      Begin VB.CheckBox Check5 
         Caption         =   "Select For Weight Targets"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   360
         TabIndex        =   104
         Top             =   270
         Value           =   1  'Checked
         Width           =   2490
      End
      Begin VB.TextBox Text44 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1665
         TabIndex        =   98
         Text            =   "29846"
         Top             =   585
         Width           =   735
      End
      Begin VB.TextBox Text43 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1665
         TabIndex        =   97
         Text            =   "21896"
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox Text42 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   3915
         TabIndex        =   96
         Text            =   "7.82"
         Top             =   585
         Width           =   735
      End
      Begin VB.TextBox Text41 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3915
         TabIndex        =   95
         Text            =   "2800"
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox Text40 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2160
         TabIndex        =   94
         Text            =   "2800"
         Top             =   1260
         Width           =   735
      End
      Begin VB.Label Label44 
         Caption         =   "Target Gross Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   103
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label43 
         Caption         =   "Target Net Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   102
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label42 
         Alignment       =   1  'Right Justify
         Caption         =   "Wt/Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   101
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label41 
         Alignment       =   1  'Right Justify
         Caption         =   "Target Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   100
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label40 
         Caption         =   "Selected Rear Gallons"
         Height          =   255
         Left            =   360
         TabIndex        =   99
         Top             =   1305
         Width           =   1755
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Overall/Front Load Specifications"
      Height          =   1680
      Left            =   135
      TabIndex        =   81
      Top             =   1215
      Width           =   4965
      Begin VB.CheckBox Check2 
         Caption         =   "Split Load"
         Height          =   285
         Left            =   360
         TabIndex        =   92
         Top             =   270
         Width           =   1050
      End
      Begin VB.TextBox Text39 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   2160
         TabIndex        =   90
         Text            =   "5650"
         Top             =   1260
         Width           =   735
      End
      Begin VB.TextBox Text38 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3915
         TabIndex        =   88
         Text            =   "5650"
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox Text37 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   3915
         TabIndex        =   86
         Text            =   "7.82"
         Top             =   585
         Width           =   735
      End
      Begin VB.TextBox Text36 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1665
         TabIndex        =   84
         Text            =   "44183"
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox Text20 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1665
         TabIndex        =   82
         Text            =   "75683"
         Top             =   585
         Width           =   735
      End
      Begin VB.Label Label39 
         Caption         =   "Selected Front Gallons"
         Height          =   255
         Left            =   360
         TabIndex        =   91
         Top             =   1305
         Width           =   1755
      End
      Begin VB.Label Label21 
         Alignment       =   1  'Right Justify
         Caption         =   "Target Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   89
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         Caption         =   "Wt/Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   87
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Target Net Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   85
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Target Gross Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   83
         Top             =   630
         Width           =   1215
      End
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Print S/&P"
      Height          =   375
      Left            =   6930
      TabIndex        =   80
      Top             =   8955
      Width           =   1005
   End
   Begin VB.TextBox Text19 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000F&
      Height          =   330
      Left            =   900
      TabIndex        =   78
      Text            =   "0"
      Top             =   8955
      Width           =   330
   End
   Begin VB.CommandButton Command7 
      Caption         =   "+Laps+"
      Height          =   330
      Left            =   180
      TabIndex        =   77
      Top             =   8955
      Width           =   690
   End
   Begin VB.CommandButton Command9 
      Caption         =   "&BOL..."
      Height          =   375
      Left            =   9270
      TabIndex        =   76
      Top             =   8955
      Width           =   1005
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Print &W/T"
      Height          =   375
      Left            =   8100
      TabIndex        =   75
      Top             =   8955
      Width           =   1005
   End
   Begin VB.Frame Frame6 
      Caption         =   "Rear Load Info"
      Height          =   5850
      Left            =   5310
      TabIndex        =   25
      Top             =   3015
      Width           =   4980
      Begin VB.ComboBox Combo4 
         Height          =   315
         Left            =   1350
         TabIndex        =   115
         Text            =   "W14"
         Top             =   2925
         Width           =   3255
      End
      Begin VB.TextBox Text46 
         Height          =   285
         Left            =   3645
         TabIndex        =   111
         Text            =   "8"
         Top             =   3960
         Width           =   1005
      End
      Begin VB.TextBox Text30 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3915
         TabIndex        =   107
         Text            =   "10.95"
         Top             =   1710
         Width           =   735
      End
      Begin VB.TextBox Text27 
         Height          =   285
         Left            =   1350
         TabIndex        =   63
         Text            =   "41"
         Top             =   3960
         Width           =   1005
      End
      Begin VB.TextBox Text26 
         Height          =   285
         Left            =   1350
         TabIndex        =   62
         Text            =   "0.50% TOA Superbond"
         Top             =   3630
         Width           =   3300
      End
      Begin VB.TextBox Text25 
         Height          =   285
         Left            =   1350
         TabIndex        =   61
         Text            =   "PG70-28"
         Top             =   3285
         Width           =   3300
      End
      Begin VB.TextBox Text24 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1350
         TabIndex        =   60
         Text            =   "7950"
         Top             =   4320
         Width           =   1005
      End
      Begin VB.TextBox Text23 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1350
         TabIndex        =   59
         Text            =   "29850"
         Top             =   4680
         Width           =   1005
      End
      Begin VB.TextBox Text22 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   58
         Text            =   "21900"
         Top             =   5040
         Width           =   1005
      End
      Begin VB.TextBox Text21 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   57
         Text            =   "10.95"
         Top             =   5400
         Width           =   1005
      End
      Begin VB.TextBox Text35 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   33
         Text            =   "17"
         Top             =   315
         Width           =   1455
      End
      Begin VB.TextBox Text34 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   32
         Text            =   "Johnny B Trucking"
         Top             =   660
         Width           =   3300
      End
      Begin VB.TextBox Text33 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   31
         Text            =   "H-K Contractors, Inc"
         Top             =   1005
         Width           =   3300
      End
      Begin VB.TextBox Text32 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   30
         Text            =   "0000009246"
         Top             =   1710
         Width           =   1455
      End
      Begin VB.TextBox Text31 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   29
         Text            =   "Stimpson Hills"
         Top             =   1350
         Width           =   3300
      End
      Begin VB.TextBox Text29 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   28
         Text            =   "5-023129"
         Top             =   2070
         Width           =   1455
      End
      Begin VB.TextBox Text28 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   27
         Text            =   "Cust. Split  (Rear)"
         Top             =   2415
         Width           =   1950
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Credit Return"
         Height          =   285
         Left            =   3420
         TabIndex        =   26
         Top             =   2430
         Width           =   1320
      End
      Begin VB.Label Label48 
         Caption         =   "Formula"
         Height          =   255
         Left            =   360
         TabIndex        =   116
         Top             =   2970
         Width           =   765
      End
      Begin VB.Label Label46 
         Caption         =   "Rack #"
         Height          =   255
         Left            =   2925
         TabIndex        =   112
         Top             =   4005
         Width           =   585
      End
      Begin VB.Label Label33 
         Alignment       =   1  'Right Justify
         Caption         =   "Req. Tons"
         Height          =   285
         Left            =   2835
         TabIndex        =   108
         Top             =   1755
         Width           =   960
      End
      Begin VB.Label Label30 
         Caption         =   "Tank #"
         Height          =   255
         Left            =   360
         TabIndex        =   70
         Top             =   4020
         Width           =   855
      End
      Begin VB.Label Label29 
         Caption         =   "Additive"
         Height          =   255
         Left            =   360
         TabIndex        =   69
         Top             =   3675
         Width           =   855
      End
      Begin VB.Label Label28 
         Caption         =   "Product"
         Height          =   255
         Left            =   360
         TabIndex        =   68
         Top             =   3315
         Width           =   855
      End
      Begin VB.Label Label27 
         Caption         =   "Tare"
         Height          =   255
         Left            =   360
         TabIndex        =   67
         Top             =   4365
         Width           =   855
      End
      Begin VB.Label Label26 
         Caption         =   "Gross"
         Height          =   255
         Left            =   360
         TabIndex        =   66
         Top             =   4725
         Width           =   855
      End
      Begin VB.Label Label25 
         Caption         =   "Net"
         Height          =   255
         Left            =   360
         TabIndex        =   65
         Top             =   5085
         Width           =   855
      End
      Begin VB.Label Label24 
         Caption         =   "Tons"
         Height          =   255
         Left            =   360
         TabIndex        =   64
         Top             =   5445
         Width           =   855
      End
      Begin VB.Label Label38 
         Caption         =   "Truck #"
         Height          =   255
         Left            =   360
         TabIndex        =   40
         Top             =   345
         Width           =   855
      End
      Begin VB.Label Label37 
         Caption         =   "Hauler"
         Height          =   255
         Left            =   360
         TabIndex        =   39
         Top             =   705
         Width           =   855
      End
      Begin VB.Label Label36 
         Caption         =   "Customer"
         Height          =   255
         Left            =   360
         TabIndex        =   38
         Top             =   1050
         Width           =   855
      End
      Begin VB.Label Label35 
         Caption         =   "Contract #"
         Height          =   255
         Left            =   360
         TabIndex        =   37
         Top             =   1770
         Width           =   855
      End
      Begin VB.Label Label34 
         Caption         =   "Project"
         Height          =   255
         Left            =   360
         TabIndex        =   36
         Top             =   1395
         Width           =   855
      End
      Begin VB.Label Label32 
         Caption         =   "BOL #"
         Height          =   255
         Left            =   360
         TabIndex        =   35
         Top             =   2100
         Width           =   855
      End
      Begin VB.Label Label31 
         Caption         =   "Commodity"
         Height          =   255
         Left            =   360
         TabIndex        =   34
         Top             =   2460
         Width           =   855
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "General Info"
      Height          =   1080
      Left            =   6390
      TabIndex        =   12
      Top             =   90
      Width           =   3885
      Begin VB.CheckBox Check1 
         Caption         =   "Driver On"
         Height          =   285
         Left            =   2745
         TabIndex        =   79
         Top             =   315
         Width           =   1095
      End
      Begin VB.TextBox Text18 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1800
         TabIndex        =   42
         Text            =   "10:49 AM"
         Top             =   675
         Width           =   870
      End
      Begin VB.TextBox Text17 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1800
         TabIndex        =   41
         Text            =   "10:23 AM"
         Top             =   285
         Width           =   870
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   945
         TabIndex        =   16
         Text            =   "JJJ"
         Top             =   675
         Width           =   780
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   945
         TabIndex        =   15
         Text            =   "PTC"
         Top             =   285
         Width           =   780
      End
      Begin VB.Label Label9 
         Caption         =   "Inbound"
         Height          =   255
         Left            =   135
         TabIndex        =   14
         Top             =   330
         Width           =   765
      End
      Begin VB.Label Label8 
         Caption         =   "Outbound"
         Height          =   255
         Left            =   135
         TabIndex        =   13
         Top             =   720
         Width           =   810
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Front Load Info"
      Height          =   5850
      Left            =   135
      TabIndex        =   0
      Top             =   3015
      Width           =   4980
      Begin VB.ComboBox Combo3 
         Height          =   315
         Left            =   1350
         TabIndex        =   113
         Text            =   "Standard"
         Top             =   2925
         Width           =   3255
      End
      Begin VB.TextBox Text45 
         Height          =   285
         Left            =   3600
         TabIndex        =   109
         Text            =   "8"
         Top             =   3975
         Width           =   1005
      End
      Begin VB.TextBox Text12 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   3915
         TabIndex        =   105
         Text            =   "22.10"
         Top             =   1710
         Width           =   735
      End
      Begin VB.TextBox Text16 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   55
         Text            =   "22.09"
         Top             =   5400
         Width           =   1005
      End
      Begin VB.TextBox Text15 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   53
         Text            =   "44180"
         Top             =   5040
         Width           =   1005
      End
      Begin VB.TextBox Text14 
         Alignment       =   1  'Right Justify
         Height          =   285
         Left            =   1350
         TabIndex        =   51
         Text            =   "75680"
         Top             =   4680
         Width           =   1005
      End
      Begin VB.TextBox Text13 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#,##0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1350
         TabIndex        =   49
         Text            =   "31500"
         Top             =   4320
         Width           =   1005
      End
      Begin VB.TextBox Text9 
         Height          =   285
         Left            =   1350
         TabIndex        =   45
         Text            =   "PG70-28"
         Top             =   3285
         Width           =   3255
      End
      Begin VB.TextBox Text8 
         Height          =   285
         Left            =   1350
         TabIndex        =   44
         Text            =   "0.50% TOA Superbond"
         Top             =   3630
         Width           =   3255
      End
      Begin VB.TextBox Text7 
         Height          =   285
         Left            =   1350
         TabIndex        =   43
         Text            =   "41"
         Top             =   3975
         Width           =   1005
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Credit Return"
         Height          =   285
         Left            =   3420
         TabIndex        =   23
         Top             =   2430
         Width           =   1320
      End
      Begin VB.TextBox Text11 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   22
         Text            =   "Cust. Split  (Front)"
         Top             =   2415
         Width           =   1950
      End
      Begin VB.TextBox Text6 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   20
         Text            =   "5-023123"
         Top             =   2070
         Width           =   1455
      End
      Begin VB.TextBox Text10 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   17
         Text            =   "Coffin Creek Development"
         Top             =   1350
         Width           =   3300
      End
      Begin VB.TextBox Text4 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   9
         Text            =   "0000009234"
         Top             =   1710
         Width           =   1455
      End
      Begin VB.TextBox Text3 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   8
         Text            =   "H-K Contractors, Inc"
         Top             =   1005
         Width           =   3300
      End
      Begin VB.TextBox Text2 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   7
         Text            =   "Johnny B Trucking"
         Top             =   660
         Width           =   3300
      End
      Begin VB.TextBox Text1 
         BackColor       =   &H8000000F&
         Height          =   285
         Left            =   1350
         TabIndex        =   6
         Text            =   "17"
         Top             =   315
         Width           =   1455
      End
      Begin VB.Label Label47 
         Caption         =   "Formula"
         Height          =   255
         Left            =   360
         TabIndex        =   114
         Top             =   2970
         Width           =   765
      End
      Begin VB.Label Label45 
         Caption         =   "Rack #"
         Height          =   255
         Left            =   2880
         TabIndex        =   110
         Top             =   4020
         Width           =   585
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Req. Tons"
         Height          =   285
         Left            =   3015
         TabIndex        =   106
         Top             =   1755
         Width           =   780
      End
      Begin VB.Label Label19 
         Caption         =   "Tons"
         Height          =   255
         Left            =   360
         TabIndex        =   56
         Top             =   5445
         Width           =   855
      End
      Begin VB.Label Label18 
         Caption         =   "Net"
         Height          =   255
         Left            =   360
         TabIndex        =   54
         Top             =   5085
         Width           =   855
      End
      Begin VB.Label Label17 
         Caption         =   "Gross"
         Height          =   255
         Left            =   360
         TabIndex        =   52
         Top             =   4725
         Width           =   855
      End
      Begin VB.Label Label16 
         Caption         =   "Tare"
         Height          =   255
         Left            =   405
         TabIndex        =   50
         Top             =   4365
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Product"
         Height          =   255
         Left            =   360
         TabIndex        =   48
         Top             =   3315
         Width           =   855
      End
      Begin VB.Label Label11 
         Caption         =   "Additive"
         Height          =   255
         Left            =   360
         TabIndex        =   47
         Top             =   3675
         Width           =   855
      End
      Begin VB.Label Label7 
         Caption         =   "Tank #"
         Height          =   255
         Left            =   360
         TabIndex        =   46
         Top             =   4020
         Width           =   855
      End
      Begin VB.Label Label14 
         Caption         =   "Commodity"
         Height          =   255
         Left            =   360
         TabIndex        =   21
         Top             =   2460
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "BOL #"
         Height          =   255
         Left            =   360
         TabIndex        =   19
         Top             =   2100
         Width           =   855
      End
      Begin VB.Label Label13 
         Caption         =   "Project"
         Height          =   255
         Left            =   360
         TabIndex        =   18
         Top             =   1395
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Contract #"
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   1770
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Customer"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   1050
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Hauler"
         Height          =   255
         Left            =   360
         TabIndex        =   3
         Top             =   705
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Truck #"
         Height          =   255
         Left            =   360
         TabIndex        =   2
         Top             =   345
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Scale Capture"
      Height          =   1080
      Left            =   135
      TabIndex        =   1
      Top             =   90
      Width           =   6180
      Begin VB.CommandButton Command3 
         Caption         =   "Tare (F5)"
         Height          =   330
         Left            =   4095
         TabIndex        =   74
         Top             =   585
         Width           =   915
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Gross (F7)"
         Height          =   330
         Left            =   5085
         TabIndex        =   73
         Top             =   585
         Width           =   915
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Tare (F2)"
         Height          =   330
         Left            =   2025
         TabIndex        =   24
         Top             =   585
         Width           =   915
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Gross (F4)"
         Height          =   330
         Left            =   3015
         TabIndex        =   11
         Top             =   585
         Width           =   915
      End
      Begin VB.TextBox Text5 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   645
         Left            =   180
         TabIndex        =   10
         Text            =   "29850"
         Top             =   315
         Width           =   1770
      End
      Begin VB.Line Line1 
         X1              =   4095
         X2              =   4725
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Line Line8 
         X1              =   5355
         X2              =   5985
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label23 
         Alignment       =   2  'Center
         Caption         =   "Rear"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4770
         TabIndex        =   72
         Top             =   315
         Width           =   555
      End
      Begin VB.Line Line6 
         X1              =   3285
         X2              =   3915
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Line Line5 
         X1              =   2025
         X2              =   2655
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label22 
         Alignment       =   2  'Center
         Caption         =   "Front"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2700
         TabIndex        =   71
         Top             =   315
         Width           =   555
      End
   End
End
Attribute VB_Name = "frmRecptOfMaterials"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
                        
            Unload Me
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False

End Sub
