VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmLoadSearch 
   BackColor       =   &H00FF00FF&
   Caption         =   "Load Search - Valero"
   ClientHeight    =   6810
   ClientLeft      =   795
   ClientTop       =   1650
   ClientWidth     =   14565
   ControlBox      =   0   'False
   Icon            =   "frmLoadSearch.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6810
   ScaleWidth      =   14565
   Begin VB.Timer tmrAutoReturnNoActivity 
      Interval        =   10000
      Left            =   0
      Top             =   0
   End
   Begin MSDataGridLib.DataGrid grdLoadSearch 
      Height          =   855
      Left            =   360
      TabIndex        =   2
      Top             =   360
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1508
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      TabAction       =   2
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         AllowRowSizing  =   0   'False
         AllowSizing     =   0   'False
         Locked          =   -1  'True
         RecordSelectors =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   2190
      TabIndex        =   1
      Top             =   1620
      Width           =   1005
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Height          =   375
      Left            =   1050
      TabIndex        =   0
      Top             =   1620
      Width           =   1005
   End
End
Attribute VB_Name = "frmLoadSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim rsLoadSearch As New ADODB.Recordset

Private lpTruck As String
Private lpHauler As String
Private lpCustomer As String
Private lpProject As String
Private lpContract As String
Private lpReqTons As String
Private lpOrder As String
Private lpPlant As String
Private lpBOLNbr As String
Private lpCommodity As String

Private Sub cmdCancel_Click()
    gbLastActivityTime = Date + Time
    
    gbProceed = False
    Unload Me

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdCancel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdOK_Click()
    gbLastActivityTime = Date + Time
    
    gbProceed = True
    Me.Hide

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdOK_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Load()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Dim SQL As String
    
    grdLoadSearch.Top = 100
    grdLoadSearch.Left = 100
    
    With rsLoadSearch
        .CursorLocation = adUseClient
        If gbLocalMode Then
            'RKL DEJ 2016-08-18 Added Carrier Info
            'Access Version
'            "IIf(additive_product_description = 'No Antistrip in Product', '', additive_product_description) As Additive, "
            .Open "Select order_id, shipper_sched_arrival, tractor_id, Commodity, contract_number, customer_name, Dest_Address, base_product_description, " & _
            "additive_product_description  As Additive, " & _
            "Tons, customer_id, custref_number, purchase_order, facility_id, facility_name, owner_name, trailer1_id, trailer2_id, dest_sched_arrival, destination_name, base_product_number, dest_directions, additive_product_Number, Weight, Weight_UM, Tank_NO, Carrier_ID, Carrier_Name " & _
            "from vsoMcLeodLoads_SGS where facility_id in (SELECT FacilityLoadID FROM tsoFacilityAddress_SGS WHERE City = '" & gbPlant & "') ORDER BY shipper_sched_arrival ASC", gbScaleConn, adOpenStatic, adLockBatchOptimistic
            Me.Caption = "Load Search (Server Disconnected) - Valero"
        Else
            'SQL Version
'            "case additive_product_description when 'No Antistrip in Product' then '' end AS Additive, "
            
            'RKL DEJ 2016-08-18 Added Carrier Info
            'RKL DEJ added to filter by lmeplanningonly
            SQL = "Select order_id, shipper_sched_arrival, tractor_id, Commodity, contract_number, customer_name, "
            SQL = SQL & "Dest_Address, base_product_description, additive_product_description AS Additive, "
            SQL = SQL & "Tons, customer_id, custref_number, lme.purchase_order, facility_id, facility_name, owner_name, "
            SQL = SQL & "trailer1_id, trailer2_id, dest_sched_arrival, destination_name, base_product_number, "
            SQL = SQL & "dest_directions, additive_product_Number, Weight, Weight_UM, Tank_NO, lme.Carrier_ID, lme.Carrier_Name "
            SQL = SQL & "from vsoMcLeodLoads_SGS lme with(NoLock) "
            SQL = SQL & "Inner Join tsoFacilityAddress_SGS f with(NoLock) "
            SQL = SQL & "on lme.facility_ID = f.FacilityLoadID "
            SQL = SQL & "and f.City = '" & gbPlant & "' "
            
            'RKL DEJ 2017-12-06 (START)
            'New filter for Customer Owned facilities
            SQL = SQL & "and Coalesce(f.CustOwnedProd,0) = '" & Int(Abs(gbCustOwnedProd)) & "' "
            'RKL DEJ 2017-12-06 (STOP)
            
            SQL = SQL & "Left Outer Join tsoSalesOrder so with(NoLock) "
            SQL = SQL & "on lme.contract_number = so.TranNo "
            SQL = SQL & "and so.trantype = 802 "
            SQL = SQL & "and f.CompanyID = so.CompanyID "
            SQL = SQL & "Left Outer Join tsoSalesOrderExt_SGS sox with(NoLock) "
            SQL = SQL & "on so.sokey = sox.sokey "
            SQL = SQL & "Where Case When sox.LMEPlanningOnly is Null Then 0 Else sox.LMEPlanningOnly End = 0 "
            SQL = SQL & "ORDER BY shipper_sched_arrival ASC "
            
            .Open SQL, gbMASConn, adOpenStatic, adLockBatchOptimistic
                        
            Me.Caption = "Load Search - Valaro"
        End If
        Set grdLoadSearch.DataSource = rsLoadSearch
    End With
    
    With grdLoadSearch
'        .Columns(0).Width = 0
        .Columns("order_id").Width = 800
        .Columns("order_id").Caption = "Order No"
        .Columns("shipper_sched_arrival").Width = 1900
        .Columns("shipper_sched_arrival").Caption = "Load Date/Time"
        .Columns("tractor_id").Width = 1000
        .Columns("tractor_id").Alignment = dbgCenter
        .Columns("tractor_id").Caption = "Truck No"
        .Columns("Commodity").Width = 1400
        .Columns("Commodity").Caption = "Commodity"
        .Columns("contract_number").Width = 1100
        .Columns("contract_number").Caption = "Contract No"
        .Columns("customer_name").Width = 1900
        .Columns("customer_name").Caption = "Customer Name"
        .Columns("Dest_Address").Width = 2250
        .Columns("Dest_Address").Caption = "Dest Address"
        .Columns("base_product_description").Width = 1500
        .Columns("base_product_description").Caption = "Product"
        .Columns("Additive").Width = 1500
        .Columns("Additive").Caption = "Additive"
        .Columns("Tons").Width = 500
        .Columns("Tons").Alignment = dbgCenter
        .Columns("Tons").Caption = "Tons"
        .Columns("customer_id").Visible = False
        .Columns("facility_id").Visible = False
        .Columns("customer_id").Visible = False
        .Columns("custref_number").Visible = False
        
        .Columns("purchase_order").Visible = True
        .Columns("purchase_order").Width = 1500
        .Columns("purchase_order").Alignment = dbgLeft
        .Columns("purchase_order").Caption = "Purchase_Order"
        
        .Columns("facility_id").Visible = False
        .Columns("facility_name").Visible = False
        .Columns("owner_name").Visible = False
        .Columns("trailer1_id").Visible = False
        .Columns("trailer2_id").Visible = False
        .Columns("dest_sched_arrival").Visible = False
        .Columns("destination_name").Visible = False
        .Columns("base_product_number").Visible = False
        .Columns("dest_directions").Visible = False
        .Columns("additive_product_Number").Visible = False
        .Columns("Weight").Visible = False
        .Columns("Weight_UM").Visible = False
        .Columns("Tank_NO").Visible = False
        
        'RKL DEJ 2016-08-18 Added Carrier Info (START)
        .Columns("Carrier_ID").Visible = True
        .Columns("Carrier_ID").Width = 1500
        .Columns("Carrier_ID").Alignment = dbgLeft
        .Columns("Carrier_ID").Caption = "Carrier_ID"
        
        .Columns("Carrier_Name").Visible = True
        .Columns("Carrier_Name").Width = 1500
        .Columns("Carrier_Name").Alignment = dbgLeft
        .Columns("Carrier_Name").Caption = "Carrier_Name"
        'RKL DEJ 2016-08-18 Added Carrier Info (STOP)
        
    End With
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & "Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Resize()
    gbLastActivityTime = Date + Time
    
    grdLoadSearch.Width = Me.Width - 350
    grdLoadSearch.Height = Me.Height - 1250
    
    
    cmdCancel.Top = Me.Height - 1020
    cmdOk.Top = Me.Height - 1020

    cmdCancel.Left = Me.Width - cmdCancel.Width - 200
    cmdOk.Left = Me.Width - cmdOk.Width - cmdCancel.Width - 300

    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    gbLastActivityTime = Date + Time
    
    rsLoadSearch.Close
    Set rsLoadSearch = Nothing

    gbLastActivityTime = Date + Time
End Sub

Private Sub grdLoadSearch_DblClick()
    gbLastActivityTime = Date + Time
    
    cmdOK_Click

    gbLastActivityTime = Date + Time
End Sub

Private Sub grdLoadSearch_HeadClick(ByVal ColIndex As Integer)
    On Error GoTo Error
    
    Dim SortField As String
    Dim SortString As String
        
    gbLastActivityTime = Date + Time

'    SortField = grdLoadSearch.Columns(ColIndex).Caption
    SortField = "[" & grdLoadSearch.Columns(ColIndex).DataField & "]"
    
'    If gbLocalMode = False And UCase(SortField) = UCase("[Additive]") Then Exit Sub     'Can not sort on this field with a Case Statement....
    
    If InStr(rsLoadSearch.Sort, "Asc") Then
        SortString = SortField & " Desc"
    Else
        SortString = SortField & " Asc"
    End If
    rsLoadSearch.Sort = SortString
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".grdLoadSearch_HeadClick(ByVal ColIndex As Integer)" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Public Property Get Truck() As String
    Truck = lpTruck

    gbLastActivityTime = Date + Time
End Property
Public Property Get Hauler() As String
    Hauler = lpHauler

    gbLastActivityTime = Date + Time
End Property
Public Property Get Customer() As String
    Customer = lpCustomer

    gbLastActivityTime = Date + Time
End Property
Public Property Get Project() As String
    Project = lpProject

    gbLastActivityTime = Date + Time
End Property
Public Property Get Contract() As String
    Contract = lpContract

    gbLastActivityTime = Date + Time
End Property
Public Property Get ReqTons() As String
    ReqTons = lpReqTons

    gbLastActivityTime = Date + Time
End Property
Public Property Get Order() As String
    Order = lpOrder

    gbLastActivityTime = Date + Time
End Property
Public Property Get Plant() As String
    Plant = lpPlant

    gbLastActivityTime = Date + Time
End Property
Public Property Get BOLNbr() As String
    BOLNbr = lpBOLNbr

    gbLastActivityTime = Date + Time
End Property
Public Property Get Commodity() As String
    Commodity = lpCommodity

    gbLastActivityTime = Date + Time
End Property

Private Sub grdLoadSearch_KeyPress(KeyAscii As Integer)
    gbLastActivityTime = Date + Time
   
   If KeyAscii = 13 Then
      KeyAscii = 0
      grdLoadSearch_DblClick
   End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub grdLoadSearch_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
            
'            MsgBox Me.Name & vbTab & "Add Code to close Locked records and return to Open Loads"
            
            Call cmdCancel_Click
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False

End Sub
