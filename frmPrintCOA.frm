VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmPrintCOA 
   BackColor       =   &H00FF00FF&
   Caption         =   "Reprint COA - Valero"
   ClientHeight    =   6675
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10455
   Icon            =   "frmPrintCOA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6675
   ScaleWidth      =   10455
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chDate 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   14
      Top             =   1560
      Width           =   1455
   End
   Begin VB.CheckBox chDateLess 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   17
      Top             =   1920
      Width           =   1455
   End
   Begin VB.ComboBox cmboPrinter 
      Height          =   315
      Left            =   6960
      TabIndex        =   24
      Text            =   "Combo1"
      Top             =   480
      Width           =   2295
   End
   Begin VB.TextBox txtCopyCount 
      Alignment       =   2  'Center
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   6960
      TabIndex        =   23
      Text            =   "1"
      Top             =   120
      Width           =   1335
   End
   Begin VB.CheckBox chCypress 
      Caption         =   "Print To Cypress"
      Height          =   195
      Left            =   6960
      TabIndex        =   20
      ToolTipText     =   "Print to Cypress after printing a paper copy."
      Top             =   840
      Width           =   1575
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print COA"
      Height          =   375
      Left            =   6960
      TabIndex        =   19
      Top             =   1320
      Width           =   1335
   End
   Begin VB.CheckBox chLot 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   11
      Top             =   1200
      Width           =   1455
   End
   Begin VB.TextBox txtLot 
      Height          =   285
      Left            =   1680
      TabIndex        =   10
      Top             =   1200
      Width           =   1935
   End
   Begin VB.CheckBox chCustomer 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   8
      Top             =   840
      Width           =   1455
   End
   Begin VB.TextBox txtCustomer 
      Height          =   285
      Left            =   1680
      TabIndex        =   7
      Top             =   840
      Width           =   1935
   End
   Begin VB.CheckBox chContract 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   5
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtContract 
      Height          =   285
      Left            =   1680
      TabIndex        =   4
      Top             =   480
      Width           =   1935
   End
   Begin VB.CheckBox chBOL 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   2
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox txtBOL 
      Height          =   285
      Left            =   1680
      TabIndex        =   1
      Top             =   120
      Width           =   1935
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh Data"
      Height          =   375
      Left            =   1800
      TabIndex        =   18
      Top             =   2400
      Width           =   1575
   End
   Begin MSDataGridLib.DataGrid grdBOLRecs 
      Align           =   2  'Align Bottom
      Height          =   3735
      Left            =   0
      TabIndex        =   21
      Top             =   2940
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   6588
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "BOL Records"
      ColumnCount     =   11
      BeginProperty Column00 
         DataField       =   "BOLTableKey"
         Caption         =   "BOLTableKey"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "BOLNbr"
         Caption         =   "BOL Nbr"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "ScaleInTime"
         Caption         =   "Scale In"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column03 
         DataField       =   "ScaleOutTime"
         Caption         =   "Scale Out"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column04 
         DataField       =   "ContractNbr"
         Caption         =   "Contract"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column05 
         DataField       =   "Customer"
         Caption         =   "Customer"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column06 
         DataField       =   "Project"
         Caption         =   "Project"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column07 
         DataField       =   "TruckNo"
         Caption         =   "Truck No"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column08 
         DataField       =   "ProductID"
         Caption         =   "ProductID"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column09 
         DataField       =   "ProductDesc"
         Caption         =   "Product Desc"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column10 
         DataField       =   "LotNbr"
         Caption         =   "Lot Nbr"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            Object.Visible         =   0   'False
         EndProperty
         BeginProperty Column01 
         EndProperty
         BeginProperty Column02 
            ColumnWidth     =   1725.165
         EndProperty
         BeginProperty Column03 
            ColumnWidth     =   1709.858
         EndProperty
         BeginProperty Column04 
         EndProperty
         BeginProperty Column05 
            ColumnWidth     =   1995.024
         EndProperty
         BeginProperty Column06 
         EndProperty
         BeginProperty Column07 
         EndProperty
         BeginProperty Column08 
         EndProperty
         BeginProperty Column09 
         EndProperty
         BeginProperty Column10 
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtScaleOut 
      Height          =   285
      Left            =   1680
      TabIndex        =   13
      Top             =   1560
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   503
      _Version        =   393216
      Format          =   171638785
      CurrentDate     =   42060
   End
   Begin MSComCtl2.DTPicker dtScaleOutLess 
      Height          =   285
      Left            =   1680
      TabIndex        =   16
      Top             =   1920
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   503
      _Version        =   393216
      Format          =   171638785
      CurrentDate     =   42060
   End
   Begin VB.Label Label3 
      Caption         =   "Scale Out Date >="
      Height          =   375
      Left            =   0
      TabIndex        =   12
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Scale Out Date <="
      Height          =   375
      Left            =   0
      TabIndex        =   15
      Top             =   1920
      Width           =   1455
   End
   Begin VB.Line Line1 
      X1              =   5520
      X2              =   5520
      Y1              =   0
      Y2              =   2760
   End
   Begin VB.Label Label1 
      Caption         =   "Printer:"
      Height          =   255
      Left            =   5880
      TabIndex        =   25
      Top             =   480
      Width           =   975
   End
   Begin VB.Label lblCopyCount 
      Caption         =   "# of Copies"
      Height          =   255
      Left            =   6000
      TabIndex        =   22
      Top             =   120
      Width           =   855
   End
   Begin VB.Label lblLot 
      Caption         =   "Lot Nbr Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label lblCustomer 
      Caption         =   "Customer Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label lblContract 
      Caption         =   "Contract Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   3
      Top             =   480
      Width           =   1455
   End
   Begin VB.Label lblBOL 
      Caption         =   "BOL Nbr Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "frmPrintCOA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim SQL As String
Dim RS As New ADODB.Recordset

Private Sub cmdPrint_Click()
    On Error GoTo Error
    gbLastActivityTime = Date + Time
    
    Dim lcTank As String
    Dim lcProductID As String
    Dim lcBOLNbr As String
    Dim lcNbrOfCopies As Integer
    
    If RS.EOF Or RS.BOF Then
        MsgBox "You need to select a BOL Nbr below first.", vbInformation, "Missing BOL Record"
        Exit Sub
    End If
    
    lcBOLNbr = ConvertToString(RS("BOLNbr").Value)
    lcTank = ConvertToString(RS("TankNumber").Value)
    lcProductID = ConvertToString(RS("ProductID").Value)
    
    If IsNumeric(txtCopyCount.Text) = True Then
        lcNbrOfCopies = CInt(txtCopyCount.Text)
    Else
        lcNbrOfCopies = 1
    End If
    
    If lcNbrOfCopies < 1 Then
        MsgBox "The number of copies must be greater than 0.", vbInformation, "Wrong Number of Copies"
        Exit Sub
    End If
    
    If PrintCOA(True, lcTank, lcProductID, lcBOLNbr, lcNbrOfCopies, cmboPrinter.Text) = False Then
        MsgBox "There was an error printing the COA.", vbInformation, "COA Did Not Print"
        Exit Sub
    End If
    
    If gbPrintCOACypress = True And chCypress.Value = vbChecked Then
        If PrintCypressCOA(True, lcTank, lcProductID, lcBOLNbr) = False Then
            MsgBox "There was an error printing the COA to Cypress.", vbInformation, "COA Cypress Print Error"
            Exit Sub
        End If
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdPrint_Click()" & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub


Private Sub cmdRefresh_Click()
    gbLastActivityTime = Date + Time
    Call GetData
    gbLastActivityTime = Date + Time

End Sub


Private Sub Form_Load()
    
    gbLastActivityTime = Date + Time
    
    'Default filter by date
    dtScaleOut.Value = DateAdd("d", -7, Date)
    chDate.Value = vbChecked
    
    Call GetData
    Call PopulateCombos
    gbLastActivityTime = Date + Time
    
    'Hide or Display the option to print to Cypress too.
    chCypress.Visible = gbPrintCOACypress
    
End Sub


Sub GetData()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
'    SQL = "Select top 100 BOLTableKey, BOLNbr, ContractNbr, Customer, Project, TruckNo, ProductID, ProductDesc, ScaleInTime, ScaleOutTime, LotNbr "
    SQL = "Select BOLTableKey, BOLNbr, TankNumber, ContractNbr, Customer, Project, TruckNo, ProductID, ProductDesc, ScaleInTime, ScaleOutTime, LotNbr "
    SQL = SQL & "From BOLPrinting "
    SQL = SQL & "Where 1=1 "
    
    'Add Filter Cryterial
    If chBOL.Value = vbChecked And Trim(txtBOL.Text) <> Empty Then
        SQL = SQL & "And BOLNbr like '%" & txtBOL.Text & "%' "
    End If
        
    If chContract.Value = vbChecked And Trim(txtContract.Text) <> Empty Then
        SQL = SQL & "And ContractNbr like '%" & txtContract.Text & "%' "
    End If
        
    If chCustomer.Value = vbChecked And Trim(txtCustomer.Text) <> Empty Then
        SQL = SQL & "And Customer like '%" & txtCustomer.Text & "%' "
    End If
        
    If chLot.Value = vbChecked And Trim(txtLot.Text) <> Empty Then
        SQL = SQL & "And LotNbr like '%" & txtLot.Text & "%' "
    End If
    
    If chDate.Value = vbChecked And IsDate(dtScaleOut.Value) = True Then
        SQL = SQL & "And ScaleOutTime >= #" & dtScaleOut.Value & "# "
    End If
    
    If chDateLess.Value = vbChecked And IsDate(dtScaleOutLess.Value) = True Then
        SQL = SQL & "And ScaleOutTime <= #" & dtScaleOutLess.Value & "# "
    End If
    
    SQL = SQL & "Order By ScaleOutTime Desc "
    
    Set grdBOLRecs.DataSource = Nothing
    
    If RS.State = 1 Then
        RS.Close
    End If
    
    RS.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    Set grdBOLRecs.DataSource = RS
    
    grdBOLRecs.Caption = "BOL Records (" & RS.RecordCount & ")"
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".GetData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub


Private Sub grdBOLRecs_HeadClick(ByVal ColIndex As Integer)
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Dim SortStr As String
    Dim SortDirection As String
    
    
    SortStr = UCase(Trim(RS.Sort))
    
    If SortStr = Empty Then
        SortDirection = "Desc"
    Else
        If InStr(1, SortStr, " DESC", vbTextCompare) > 0 Then
            SortDirection = "Asc"
        Else
            SortDirection = "Desc"
        End If
    End If
    
    SortStr = "[" & grdBOLRecs.Columns(ColIndex).DataField & "] " & SortDirection
    
    RS.Sort = SortStr
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    Err.Clear
    gbLastActivityTime = Date + Time
    
End Sub

Private Sub grdBOLRecs_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub


Private Sub txtCopyCount_Validate(Cancel As Boolean)
    If IsNumeric(txtCopyCount.Text) = True Then
        txtCopyCount.Text = CInt(txtCopyCount.Text)
    Else
        txtCopyCount.Text = 1
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Sub PopulateCombos()
    On Error GoTo Error
    
    Dim Prntr As Printer
    
    gbLastActivityTime = Date + Time
        
    cmboPrinter.Clear
    
    For Each Prntr In Printers
        cmboPrinter.AddItem Prntr.DeviceName
    Next
    
    cmboPrinter.Text = gbCOAPrinter
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".PopulateCombos()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub



