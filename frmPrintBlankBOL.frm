VERSION 5.00
Begin VB.Form frmPrintBlankBOL 
   BackColor       =   &H00FF00FF&
   Caption         =   "Print Blank BOL - Valero"
   ClientHeight    =   5130
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7815
   Icon            =   "frmPrintBlankBOL.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5130
   ScaleWidth      =   7815
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdFillIn 
      Caption         =   "Fill In Form"
      Height          =   375
      Left            =   5280
      TabIndex        =   23
      Top             =   2640
      Width           =   1575
   End
   Begin VB.TextBox txtCompanyID 
      BackColor       =   &H80000000&
      Height          =   285
      Left            =   1080
      Locked          =   -1  'True
      TabIndex        =   19
      Top             =   4440
      Width           =   1575
   End
   Begin VB.Frame frameLoadType 
      Caption         =   "Load Type"
      Height          =   1695
      Left            =   120
      TabIndex        =   14
      Top             =   2160
      Width           =   3735
      Begin VB.OptionButton optnLoadType 
         Caption         =   "STANDARD LOAD"
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   17
         Top             =   1200
         Value           =   -1  'True
         Width           =   3135
      End
      Begin VB.OptionButton optnLoadType 
         Caption         =   "SPLIT LOAD"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   3135
      End
      Begin VB.OptionButton optnLoadType 
         Caption         =   "CREDIT RETURN"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   3135
      End
      Begin VB.Frame frameFrntRear 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   495
         Left            =   480
         TabIndex        =   20
         Top             =   840
         Width           =   2655
         Begin VB.OptionButton OptnRear 
            Caption         =   "Rear"
            Height          =   255
            Left            =   1320
            TabIndex        =   22
            Top             =   120
            Width           =   1095
         End
         Begin VB.OptionButton optnFront 
            Caption         =   "Front"
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   120
            Value           =   -1  'True
            Width           =   1095
         End
      End
   End
   Begin VB.Frame frameItemclass 
      Caption         =   "Item Class"
      Height          =   1815
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   3735
      Begin VB.OptionButton OptnItemClass 
         Caption         =   "Antistrips, Antistrips  *** And All Others"
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   13
         Top             =   1320
         Value           =   -1  'True
         Width           =   3495
      End
      Begin VB.OptionButton OptnItemClass 
         Caption         =   "Antistrips, Antistrips  *** And Product = 1200"
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   960
         Width           =   3495
      End
      Begin VB.OptionButton OptnItemClass 
         Caption         =   "Cutback, Cutbacks"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Width           =   3495
      End
      Begin VB.OptionButton OptnItemClass 
         Caption         =   "AC, Asphalt, PMA, Extenders"
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   3495
      End
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print BOL"
      Height          =   375
      Left            =   5280
      TabIndex        =   5
      Top             =   1800
      Width           =   1455
   End
   Begin VB.CheckBox chCypress 
      Caption         =   "Print To Cypress"
      Enabled         =   0   'False
      Height          =   195
      Left            =   5280
      TabIndex        =   4
      ToolTipText     =   "Print to Cypress after printing a paper copy."
      Top             =   1320
      Width           =   1575
   End
   Begin VB.TextBox txtCopyCount 
      Alignment       =   2  'Center
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   5280
      TabIndex        =   3
      Text            =   "1"
      Top             =   600
      Width           =   1335
   End
   Begin VB.ComboBox cmboPrinter 
      Height          =   315
      Left            =   5280
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   960
      Width           =   2295
   End
   Begin VB.TextBox txtNotes 
      Height          =   285
      Left            =   1080
      TabIndex        =   1
      ToolTipText     =   "Prints at the bottom of the report"
      Top             =   3960
      Width           =   2775
   End
   Begin VB.CheckBox chAllCopies 
      Caption         =   "Print All Copies"
      Height          =   255
      Left            =   5280
      TabIndex        =   0
      ToolTipText     =   "Print all 6 copies of the BOL"
      Top             =   360
      Width           =   1575
   End
   Begin VB.Label lblCompanyID 
      Caption         =   "Company ID"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   4440
      Width           =   975
   End
   Begin VB.Label lblCopyCount 
      Caption         =   "# of Copies"
      Height          =   255
      Left            =   4320
      TabIndex        =   8
      Top             =   600
      Width           =   855
   End
   Begin VB.Label Label1 
      Caption         =   "Printer:"
      Height          =   255
      Left            =   4320
      TabIndex        =   7
      Top             =   960
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Copy Note:"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   3960
      Width           =   855
   End
End
Attribute VB_Name = "frmPrintBlankBOL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim SQL As String
Dim RS As New ADODB.Recordset
Dim RSLoads As New ADODB.Recordset

Private Sub cmdFillIn_Click()
    frmFillInBOL.Show vbModal, Me
End Sub

Private Sub cmdPrint_Click()
    On Error GoTo Error
    gbLastActivityTime = Date + Time
    
    Call GetData

    Dim lcTank As String
    Dim lcProductID As String
    Dim lcBOLNbr As String
    Dim lcNbrOfCopies As Integer
    Dim BOLTableKey As Long
    Dim bAllCopies As Boolean
    Dim bCertRequired As Boolean
    
    If RS.EOF Or RS.BOF Then
        MsgBox "Could not create a blank record.  Please Try Again.", vbInformation, "Print Failed"
        Call DeleteBlankRecord
        Exit Sub
    End If
    
    lcBOLNbr = ConvertToString(RS("BOLNbr").Value)
    lcTank = ConvertToString(RS("TankNumber").Value)
    lcProductID = ConvertToString(RS("ProductID").Value)
    BOLTableKey = RS("BOLTableKey").Value
    
    If Abs(ConvertToDouble(RS("COARequired").Value)) > 0 Then
        bCertRequired = True
    Else
        bCertRequired = False
    End If
    
    If chAllCopies.Value = vbChecked Then
        bAllCopies = True
    Else
        bAllCopies = False
    End If
    
    If IsNumeric(txtCopyCount.Text) = True Then
        lcNbrOfCopies = CInt(txtCopyCount.Text)
    Else
        lcNbrOfCopies = 1
    End If
    
    If lcNbrOfCopies < 1 Then
        MsgBox "The number of copies must be greater than 0.", vbInformation, "Wrong Number of Copies"
        Call DeleteBlankRecord
        Exit Sub
    End If
    
    Call PrintCrystalBOL(True, lcNbrOfCopies, BOLTableKey, cmboPrinter.Text, txtNotes.Text, bAllCopies)
'    Call PrintCrystalBOL(True, lcNbrOfCopies, BOLTableKey, cmboPrinter.Text, txtNotes.Text, bAllCopies, bCertRequired)

    If gbPrintBOLCypress = True And chCypress.Value = vbChecked Then
        Call PrintCypressBOL(True, BOLTableKey, txtNotes.Text)
    End If
    
    Call DeleteBlankRecord
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdPrint_Click()" & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
    
    Call DeleteBlankRecord
    
    gbLastActivityTime = Date + Time

End Sub


Private Sub Form_Load()
    
    gbLastActivityTime = Date + Time
    
    Call PopulateCombos
    gbLastActivityTime = Date + Time
    
    'Hide or Display the option to print to Cypress too.
    chCypress.Visible = gbPrintBOLCypress
    
    txtCompanyID.Text = gbMASCompanyID
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    
    
    If RSLoads.State = 1 Then
        If RSLoads.EOF = False Or RSLoads.BOF = False Then
            RSLoads.Delete
            RSLoads.UpdateBatch
        End If
        
        RSLoads.Close
    End If
    
    Set RSLoads = Nothing
    
    
    If RS.State = 1 Then
        If RS.EOF = False Or RS.BOF = False Then
            RS.Delete
            RS.UpdateBatch
        End If
        
        RS.Close
    End If
    
    Set RS = Nothing
End Sub

Private Sub optnLoadType_Click(Index As Integer)
    Select Case Index
        Case 0
            frameFrntRear.Enabled = False
        Case 1
            frameFrntRear.Enabled = True
        Case 2
            frameFrntRear.Enabled = False
        Case Else
            frameFrntRear.Enabled = False
    End Select
    
    
End Sub

Sub PopulateCombos()
    On Error GoTo Error
    
    Dim Prntr As Printer
    
    gbLastActivityTime = Date + Time
        
    cmboPrinter.Clear
    
    For Each Prntr In Printers
        cmboPrinter.AddItem Prntr.DeviceName
    Next
    
    cmboPrinter.Text = gbBLPRINTER
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".PopulateCombos()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub



Sub GetData()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Dim lcLoadsTableKey As Long
    
    SQL = "Select * From Loads Where 1=2"
    If RSLoads.State <> 1 Then
        
        RSLoads.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic

        
        RSLoads.AddNew
        RSLoads("ManualScaling").Value = 1
        RSLoads.UpdateBatch
        If RSLoads.EOF Or RSLoads.BOF Then
            Exit Sub
        End If
    End If
    
    lcLoadsTableKey = RSLoads("LoadsTableKey").Value
    
    SQL = "Select * "
    SQL = SQL & "From BOLPrinting "
    SQL = SQL & "Where 1=2 "
        
    If RS.State = 1 Then
        RS.Close
    End If
    
    RS.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    RS.AddNew
    
    'Get Item Class
    Select Case True
        Case OptnItemClass(0).Value
            RS("ProductClass").Value = "AC"
            RS("ProductID").Value = ""
        Case OptnItemClass(1).Value
            RS("ProductClass").Value = "Cutback"
            RS("ProductID").Value = ""
        Case OptnItemClass(2).Value
            RS("ProductClass").Value = "Antistrips"
            RS("ProductID").Value = "1200"
        Case OptnItemClass(3).Value
            RS("ProductClass").Value = "Antistrips"
            RS("ProductID").Value = ""
        Case Else
            RS("ProductClass").Value = "Antistrips"
            RS("ProductID").Value = ""
    End Select
        
    'Get Load Type
    Select Case True
        Case optnLoadType(0).Value
            RS("IsCreditReturn").Value = 1
            RS("IsSplitLoad").Value = 0
            RS("FrontRearFlag").Value = "Front"
        Case optnLoadType(1).Value
            RS("IsCreditReturn").Value = 0
            RS("IsSplitLoad").Value = 1
            If optnFront.Value = True Then
                RS("FrontRearFlag").Value = "Front"
            Else
                RS("FrontRearFlag").Value = "Rear"
            End If
        Case optnLoadType(2).Value
            RS("IsCreditReturn").Value = 0
            RS("IsSplitLoad").Value = 0
            RS("FrontRearFlag").Value = "Front"
        Case Else
            RS("IsCreditReturn").Value = 0
            RS("IsSplitLoad").Value = 0
            RS("FrontRearFlag").Value = "Front"
    End Select
    
    RS("MASFromCompanyID").Value = gbMASCompanyID
    RS("LoadsTableKey").Value = lcLoadsTableKey
    
    RS.UpdateBatch
    
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".GetData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub


Sub DeleteBlankRecord()
    On Error Resume Next
    
    If RS.State = 1 Then
        If RS.EOF = False Or RS.BOF = False Then
            RS.Delete
            RS.UpdateBatch
        End If
    End If
    
    Err.Clear
End Sub
