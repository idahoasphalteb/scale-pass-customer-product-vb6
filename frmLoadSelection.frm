VERSION 5.00
Begin VB.Form frmLoadProcessing 
   BackColor       =   &H00FF00FF&
   Caption         =   "Load Processing - Valero"
   ClientHeight    =   9795
   ClientLeft      =   3225
   ClientTop       =   2385
   ClientWidth     =   10365
   Icon            =   "frmLoadSelection.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9795
   ScaleWidth      =   10365
   Begin VB.Frame Frame3 
      BackColor       =   &H00FF00FF&
      BorderStyle     =   0  'None
      Height          =   855
      Left            =   0
      TabIndex        =   179
      Top             =   8895
      Width           =   10335
      Begin VB.CheckBox chAlternatePrinter 
         Caption         =   "Use Alternate Printer"
         DataField       =   "ManualScaling"
         Height          =   285
         Left            =   1920
         TabIndex        =   183
         Top             =   480
         Width           =   1845
      End
      Begin VB.CommandButton cmdPrintWTTicket 
         Caption         =   "Print &W.T."
         Height          =   375
         Left            =   1440
         TabIndex        =   192
         Top             =   60
         Visible         =   0   'False
         Width           =   285
      End
      Begin VB.CommandButton cmdPrintBOL 
         Caption         =   "Process &BOL..."
         Height          =   375
         Left            =   9000
         TabIndex        =   191
         Top             =   450
         Width           =   1245
      End
      Begin VB.CommandButton cmdLaps 
         Caption         =   "+Laps+"
         Height          =   330
         Left            =   120
         TabIndex        =   190
         Top             =   45
         Width           =   690
      End
      Begin VB.TextBox txtLaps 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "Laps"
         Height          =   285
         Left            =   840
         Locked          =   -1  'True
         TabIndex        =   189
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   60
         Width           =   330
      End
      Begin VB.CommandButton cmdPrintBS 
         Caption         =   "Print &Loading Batch Sheet"
         Height          =   375
         Left            =   6900
         TabIndex        =   188
         Top             =   450
         Width           =   2085
      End
      Begin VB.CheckBox chManualScale 
         Caption         =   "Manual Scaling ON"
         DataField       =   "ManualScaling"
         Height          =   285
         Left            =   120
         TabIndex        =   187
         Top             =   450
         Width           =   1725
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Close (Esc)"
         Height          =   375
         Left            =   3840
         TabIndex        =   186
         Top             =   450
         Width           =   1605
      End
      Begin VB.TextBox txtTotalGrossWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#,##0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   360
         Left            =   5550
         Locked          =   -1  'True
         TabIndex        =   185
         TabStop         =   0   'False
         Top             =   0
         Width           =   1125
      End
      Begin VB.TextBox txtTotalGrossTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   360
         Left            =   9360
         Locked          =   -1  'True
         TabIndex        =   184
         TabStop         =   0   'False
         Top             =   0
         Width           =   855
      End
      Begin VB.TextBox txtTotalNetWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00C0FFFF&
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "#,##0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   360
         Left            =   7380
         Locked          =   -1  'True
         TabIndex        =   182
         TabStop         =   0   'False
         Top             =   0
         Width           =   1125
      End
      Begin VB.CommandButton cmdPrintExtraLbl 
         Caption         =   "Print &Extra Label"
         Height          =   375
         Left            =   5490
         TabIndex        =   181
         Top             =   450
         Width           =   1365
      End
      Begin VB.CheckBox chLblSkipPrint 
         Caption         =   "Don't Print Label(s)"
         Height          =   255
         Left            =   1920
         TabIndex        =   180
         ToolTipText     =   "Don't print the lables with the LBS."
         Top             =   210
         Width           =   1695
      End
      Begin VB.Label Label50 
         Caption         =   "Total Wt:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3720
         TabIndex        =   196
         Top             =   90
         Width           =   975
      End
      Begin VB.Label Label52 
         Caption         =   "Gross"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4800
         TabIndex        =   195
         Top             =   90
         Width           =   675
      End
      Begin VB.Label Label53 
         Caption         =   "Tons"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   8700
         TabIndex        =   194
         Top             =   90
         Width           =   615
      End
      Begin VB.Label Label54 
         Caption         =   "Net"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6870
         TabIndex        =   193
         Top             =   90
         Width           =   465
      End
   End
   Begin VB.Frame FrameValeroFlds 
      BackColor       =   &H00FFFFC0&
      Caption         =   "Valero Source Products"
      Height          =   5175
      Left            =   5280
      TabIndex        =   165
      Top             =   3015
      Width           =   4980
      Begin VB.CommandButton cmdRSValues 
         Caption         =   "Test RS Values"
         Height          =   375
         Left            =   2160
         TabIndex        =   178
         Top             =   120
         Width           =   1455
      End
      Begin VB.TextBox txtSrcProductIDOne 
         DataField       =   "FrontSrcProductIDOne"
         Height          =   285
         Left            =   1350
         TabIndex        =   37
         Top             =   840
         Width           =   2295
      End
      Begin VB.CheckBox chIsTruckBlend 
         BackColor       =   &H00FFFFC0&
         Caption         =   "Truck Blend"
         DataField       =   "IsTruckBlend"
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox txtSrceQtyThree 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontSrcQtyThree"
         Height          =   285
         Left            =   1350
         TabIndex        =   48
         Top             =   4680
         Width           =   1005
      End
      Begin VB.TextBox txtSrcProductIDThree 
         DataField       =   "FrontSrcProductIDThree"
         Height          =   285
         Left            =   1350
         TabIndex        =   45
         Top             =   3960
         Width           =   2295
      End
      Begin VB.CommandButton cmdSrcProdThree 
         Caption         =   "Search"
         Height          =   285
         Left            =   3720
         TabIndex        =   46
         ToolTipText     =   "Search for a Product"
         Top             =   3990
         Width           =   855
      End
      Begin VB.TextBox txtSrcTankThree 
         DataField       =   "FrontSrcTankNbrThree"
         Height          =   285
         Left            =   1350
         TabIndex        =   47
         Top             =   4320
         Width           =   885
      End
      Begin VB.TextBox txtSrceQtyTwo 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontSrcQtyTwo"
         Height          =   285
         Left            =   1350
         TabIndex        =   44
         Top             =   3120
         Width           =   1005
      End
      Begin VB.TextBox txtSrcProductIDTwo 
         DataField       =   "FrontSrcProductIDTwo"
         Height          =   285
         Left            =   1350
         TabIndex        =   41
         Top             =   2400
         Width           =   2295
      End
      Begin VB.CommandButton cmdSrcProdTwo 
         Caption         =   "Search"
         Height          =   285
         Left            =   3720
         TabIndex        =   42
         ToolTipText     =   "Search for a Product"
         Top             =   2430
         Width           =   855
      End
      Begin VB.TextBox txtSrcTankTwo 
         DataField       =   "FrontSrcTankNbrTwo"
         Height          =   285
         Left            =   1350
         TabIndex        =   43
         Top             =   2760
         Width           =   885
      End
      Begin VB.TextBox txtSrceQtyOne 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontSrcQtyOne"
         Height          =   285
         Left            =   1350
         TabIndex        =   40
         Top             =   1560
         Width           =   1005
      End
      Begin VB.CommandButton cmdSrcProdOne 
         Caption         =   "Search"
         Height          =   285
         Left            =   3720
         TabIndex        =   38
         ToolTipText     =   "Search for a Product"
         Top             =   870
         Width           =   855
      End
      Begin VB.TextBox txtSrcTankOne 
         DataField       =   "FrontSrcTankNbrOne"
         Height          =   285
         Left            =   1350
         TabIndex        =   39
         Top             =   1200
         Width           =   885
      End
      Begin VB.Label lblSrcProductThree 
         BackColor       =   &H00FFFFC0&
         DataField       =   "FrontSrcProductThree"
         Height          =   255
         Left            =   1350
         TabIndex        =   177
         Top             =   3720
         Width           =   2295
      End
      Begin VB.Label lblSrcProductTwo 
         BackColor       =   &H00FFFFC0&
         DataField       =   "FrontSrcProductTwo"
         Height          =   255
         Left            =   1320
         TabIndex        =   176
         Top             =   2160
         Width           =   2295
      End
      Begin VB.Label lblSrcProductOne 
         BackColor       =   &H00FFFFC0&
         DataField       =   "FrontSrcProductOne"
         Height          =   255
         Left            =   1350
         TabIndex        =   175
         Top             =   600
         Width           =   2295
      End
      Begin VB.Label Label66 
         BackColor       =   &H00FFFFC0&
         Caption         =   "3rd Src Tons"
         Height          =   255
         Left            =   120
         TabIndex        =   174
         Top             =   4680
         Width           =   1215
      End
      Begin VB.Label Label65 
         BackColor       =   &H00FFFFC0&
         Caption         =   "3rd Src Product"
         Height          =   255
         Left            =   120
         TabIndex        =   173
         Top             =   3990
         Width           =   1215
      End
      Begin VB.Label Label64 
         BackColor       =   &H00FFFFC0&
         Caption         =   "3rd Src Tank #"
         Height          =   255
         Left            =   120
         TabIndex        =   172
         Top             =   4365
         Width           =   1215
      End
      Begin VB.Label Label63 
         BackColor       =   &H00FFFFC0&
         Caption         =   "2nd Src Tons"
         Height          =   255
         Left            =   120
         TabIndex        =   171
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label Label62 
         BackColor       =   &H00FFFFC0&
         Caption         =   "2nd Src Product"
         Height          =   255
         Left            =   120
         TabIndex        =   170
         Top             =   2430
         Width           =   1215
      End
      Begin VB.Label Label61 
         BackColor       =   &H00FFFFC0&
         Caption         =   "2nd Src Tank #"
         Height          =   255
         Left            =   120
         TabIndex        =   169
         Top             =   2805
         Width           =   1215
      End
      Begin VB.Label Label60 
         BackColor       =   &H00FFFFC0&
         Caption         =   "1st Src Tons"
         Height          =   255
         Left            =   120
         TabIndex        =   168
         Top             =   1560
         Width           =   1215
      End
      Begin VB.Label Label59 
         BackColor       =   &H00FFFFC0&
         Caption         =   "1st Src Product"
         Height          =   255
         Left            =   120
         TabIndex        =   167
         Top             =   870
         Width           =   1215
      End
      Begin VB.Label Label58 
         BackColor       =   &H00FFFFC0&
         Caption         =   "1st Src Tank #"
         Height          =   255
         Left            =   120
         TabIndex        =   166
         Top             =   1200
         Width           =   1215
      End
   End
   Begin VB.Timer tmpScaleIndicator 
      Interval        =   1500
      Left            =   2160
      Top             =   0
   End
   Begin VB.TextBox txtReadingScaleIndicator3 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1920
      Locked          =   -1  'True
      TabIndex        =   164
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtReadingScaleIndicator1 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1440
      Locked          =   -1  'True
      TabIndex        =   163
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox txtReadingScaleIndicator2 
      Enabled         =   0   'False
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   162
      Top             =   120
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.Timer tmrReqTons 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   1080
      Top             =   360
   End
   Begin VB.Timer tmrAutoReturnNoActivity 
      Interval        =   10000
      Left            =   480
      Top             =   360
   End
   Begin VB.Timer tmrScale 
      Interval        =   5000
      Left            =   0
      Top             =   360
   End
   Begin VB.Frame frameRearSpecs 
      Caption         =   "Rear Load Specifications"
      Height          =   1680
      Left            =   5310
      TabIndex        =   118
      Top             =   1215
      Visible         =   0   'False
      Width           =   4965
      Begin VB.CheckBox chWtTarget 
         Caption         =   "Select For Weight Targets"
         DataField       =   "SelectForWeightTargets"
         ForeColor       =   &H80000008&
         Height          =   285
         Left            =   360
         TabIndex        =   14
         Top             =   270
         Width           =   2490
      End
      Begin VB.TextBox txtRearTargetGrossWt 
         Alignment       =   1  'Right Justify
         DataField       =   "RearTargetGrossWt"
         Height          =   285
         Left            =   1665
         TabIndex        =   15
         Top             =   570
         Width           =   735
      End
      Begin VB.TextBox txtRearTargetNetWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearTargetNetWt"
         Height          =   285
         Left            =   1665
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox txtRearTargetWtGal 
         Alignment       =   1  'Right Justify
         DataField       =   "RearWtPerGallon"
         Height          =   285
         Left            =   3915
         TabIndex        =   16
         Top             =   585
         Width           =   735
      End
      Begin VB.TextBox txtRearTargetGal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearTargetGallons"
         Height          =   285
         Left            =   3915
         Locked          =   -1  'True
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox txtRearGal 
         Alignment       =   1  'Right Justify
         DataField       =   "SelectedRearGallons"
         Height          =   285
         Left            =   3900
         TabIndex        =   19
         Top             =   1230
         Width           =   735
      End
      Begin VB.Label Label44 
         Caption         =   "Target Gross Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   123
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label43 
         Caption         =   "Target Net Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   122
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label42 
         Alignment       =   1  'Right Justify
         Caption         =   "Wt/Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   121
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label41 
         Alignment       =   1  'Right Justify
         Caption         =   "Target Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   120
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label40 
         Alignment       =   1  'Right Justify
         Caption         =   "Selected Rear Gallons"
         Height          =   255
         Left            =   2070
         TabIndex        =   119
         Top             =   1275
         Width           =   1755
      End
   End
   Begin VB.Frame frameFrontSpecs 
      Caption         =   "Overall/Front Load Specifications"
      Height          =   1680
      Left            =   135
      TabIndex        =   112
      Top             =   1215
      Width           =   4965
      Begin VB.TextBox txtTotalTargetGal 
         Alignment       =   1  'Right Justify
         DataField       =   "TotalTargetGallons"
         Height          =   285
         Left            =   360
         TabIndex        =   147
         TabStop         =   0   'False
         Top             =   1260
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox txtTotalTargetGrossWt 
         Alignment       =   1  'Right Justify
         DataField       =   "TotalTargetGrossWt"
         Height          =   285
         Left            =   3915
         TabIndex        =   8
         Top             =   240
         Width           =   735
      End
      Begin VB.CheckBox chSplitLoad 
         Caption         =   "Split Load"
         DataField       =   "SplitLoad"
         Height          =   285
         Left            =   360
         TabIndex        =   7
         Top             =   270
         Width           =   1050
      End
      Begin VB.TextBox txtFrontGal 
         Alignment       =   1  'Right Justify
         DataField       =   "SelectedFrontGallons"
         Height          =   285
         Left            =   3900
         TabIndex        =   13
         Top             =   1230
         Width           =   735
      End
      Begin VB.TextBox txtFrontTargetGal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontTargetGallons"
         Height          =   285
         Left            =   3915
         Locked          =   -1  'True
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox txtFrontTargetWtGal 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontWtPerGallon"
         Height          =   285
         Left            =   3915
         TabIndex        =   10
         Top             =   585
         Width           =   735
      End
      Begin VB.TextBox txtFrontTargetNetWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontTargetNetWt"
         Height          =   285
         Left            =   1665
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   900
         Width           =   735
      End
      Begin VB.TextBox txtFrontTargetGrossWt 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontTargetGrossWt"
         Height          =   285
         Left            =   1665
         TabIndex        =   9
         Top             =   585
         Width           =   735
      End
      Begin VB.Label Label51 
         Alignment       =   1  'Right Justify
         Caption         =   "Overall Target Gross Wt"
         Height          =   255
         Left            =   1440
         TabIndex        =   145
         Top             =   285
         Width           =   2385
      End
      Begin VB.Label Label39 
         Alignment       =   1  'Right Justify
         Caption         =   "Selected Front Gallons"
         Height          =   255
         Left            =   2100
         TabIndex        =   117
         Top             =   1275
         Width           =   1725
      End
      Begin VB.Label Label21 
         Alignment       =   1  'Right Justify
         Caption         =   "Target Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   116
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         Caption         =   "Wt/Gal"
         Height          =   255
         Left            =   2610
         TabIndex        =   115
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Target Net Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   114
         Top             =   945
         Width           =   1215
      End
      Begin VB.Label Label5 
         Caption         =   "Target Gross Wt"
         Height          =   255
         Left            =   360
         TabIndex        =   113
         Top             =   630
         Width           =   1215
      End
   End
   Begin VB.Frame frameRearLoad 
      Caption         =   "Rear Load Info"
      Height          =   5850
      Left            =   5280
      TabIndex        =   85
      Top             =   3015
      Visible         =   0   'False
      Width           =   4980
      Begin VB.Frame frameRearLoadOverlay 
         BorderStyle     =   0  'None
         Caption         =   "Frame8"
         Height          =   4035
         Left            =   120
         TabIndex        =   143
         Top             =   210
         Visible         =   0   'False
         Width           =   4755
         Begin VB.Label Label49 
            Alignment       =   2  'Center
            Caption         =   "Rear information not applicable"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   435
            Left            =   240
            TabIndex        =   144
            Top             =   1770
            Width           =   4275
         End
      End
      Begin VB.CommandButton cmdRearSrchAdtv 
         Caption         =   "Search"
         Height          =   285
         Left            =   3720
         TabIndex        =   67
         ToolTipText     =   "Search for a Product"
         Top             =   3640
         Width           =   855
      End
      Begin VB.CommandButton cmdRearSrchPrdct 
         Caption         =   "Search"
         Height          =   285
         Left            =   3720
         TabIndex        =   65
         ToolTipText     =   "Search for a Product"
         Top             =   3310
         Width           =   855
      End
      Begin VB.Frame Frame7 
         Caption         =   "Invisible"
         Height          =   2295
         Left            =   2400
         TabIndex        =   136
         Top             =   4200
         Visible         =   0   'False
         Width           =   2475
         Begin VB.CheckBox chRearHighRisk 
            Alignment       =   1  'Right Justify
            Caption         =   "High Risk"
            DataField       =   "RearHighRisk"
            Enabled         =   0   'False
            Height          =   255
            Left            =   120
            TabIndex        =   157
            Top             =   1440
            Width           =   1695
         End
         Begin VB.TextBox txtRearSecTapeNbr 
            DataField       =   "RearSecurityTapNumber"
            DataSource      =   "Adodc1"
            Height          =   285
            Left            =   120
            MaxLength       =   100
            TabIndex        =   156
            Top             =   1875
            Width           =   1725
         End
         Begin VB.TextBox txtRearDestDs 
            DataField       =   "RearDestDirections"
            Height          =   315
            Left            =   1080
            TabIndex        =   149
            TabStop         =   0   'False
            Text            =   "Dest Ds"
            Top             =   570
            Width           =   855
         End
         Begin VB.TextBox txtRearFacilityID 
            DataField       =   "RearFacilityID"
            Height          =   315
            Left            =   1080
            TabIndex        =   142
            TabStop         =   0   'False
            Text            =   "FacilityID"
            Top             =   210
            Width           =   855
         End
         Begin VB.TextBox txtRearProductID 
            DataField       =   "RearProductID"
            Height          =   315
            Left            =   120
            TabIndex        =   140
            TabStop         =   0   'False
            Text            =   "ProductID"
            Top             =   210
            Width           =   855
         End
         Begin VB.TextBox txtRearAdditiveID 
            DataField       =   "RearAdditiveID"
            Height          =   315
            Left            =   120
            TabIndex        =   139
            TabStop         =   0   'False
            Text            =   "AdditiveID"
            Top             =   540
            Width           =   855
         End
         Begin VB.TextBox txtRearDestDt 
            DataField       =   "RearDestDt"
            Height          =   315
            Left            =   120
            TabIndex        =   138
            TabStop         =   0   'False
            Text            =   "Dest Dt"
            Top             =   1170
            Width           =   855
         End
         Begin VB.TextBox txtRearArrivalDt 
            DataField       =   "RearArrivalDt"
            Height          =   315
            Left            =   120
            TabIndex        =   137
            TabStop         =   0   'False
            Text            =   "Arrival Dt"
            Top             =   840
            Width           =   855
         End
         Begin VB.Label Label56 
            Caption         =   "Security Tape #"
            Height          =   255
            Left            =   120
            TabIndex        =   158
            Top             =   1680
            Width           =   1665
         End
      End
      Begin VB.ComboBox cboRearFormula 
         DataField       =   "RearFormula"
         Height          =   315
         Left            =   1350
         TabIndex        =   63
         Top             =   2925
         Width           =   3255
      End
      Begin VB.TextBox txtRearRack 
         DataField       =   "RearRackNbr"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   3645
         TabIndex        =   69
         Top             =   3960
         Width           =   1005
      End
      Begin VB.TextBox txtRearReqTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearReqTons"
         Height          =   285
         Left            =   3915
         Locked          =   -1  'True
         TabIndex        =   59
         TabStop         =   0   'False
         Top             =   1710
         Width           =   735
      End
      Begin VB.TextBox txtRearTank 
         DataField       =   "RearTankNbr"
         Height          =   285
         Left            =   1350
         TabIndex        =   68
         Top             =   3960
         Width           =   1005
      End
      Begin VB.TextBox txtRearAdditive 
         DataField       =   "RearAdditive"
         Height          =   285
         Left            =   1350
         TabIndex        =   66
         Top             =   3630
         Width           =   2295
      End
      Begin VB.TextBox txtRearProduct 
         DataField       =   "RearProduct"
         Height          =   285
         Left            =   1350
         TabIndex        =   64
         Top             =   3285
         Width           =   2295
      End
      Begin VB.TextBox txtRearTare 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearTareWt"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   70
         TabStop         =   0   'False
         Top             =   4320
         Width           =   1005
      End
      Begin VB.TextBox txtRearGross 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearGrossWt"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   71
         TabStop         =   0   'False
         Top             =   4680
         Width           =   1005
      End
      Begin VB.TextBox txtRearNet 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearNetWt"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   72
         TabStop         =   0   'False
         Top             =   5040
         Width           =   1005
      End
      Begin VB.TextBox txtRearTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearTons"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   102
         TabStop         =   0   'False
         Top             =   5400
         Width           =   1005
      End
      Begin VB.TextBox txtRearTruck 
         BackColor       =   &H8000000F&
         DataField       =   "TruckNo"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   53
         Top             =   315
         Width           =   1455
      End
      Begin VB.TextBox txtRearHauler 
         BackColor       =   &H8000000F&
         DataField       =   "RearHauler"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   55
         TabStop         =   0   'False
         Top             =   660
         Width           =   3300
      End
      Begin VB.TextBox txtRearCust 
         BackColor       =   &H8000000F&
         DataField       =   "RearCustomer"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   56
         TabStop         =   0   'False
         Top             =   1020
         Width           =   3300
      End
      Begin VB.TextBox txtRearContract 
         BackColor       =   &H8000000F&
         DataField       =   "RearContractNbr"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   58
         TabStop         =   0   'False
         Top             =   1710
         Width           =   1455
      End
      Begin VB.CommandButton cmdSearchRear 
         Caption         =   "Searc&h"
         Height          =   285
         Left            =   3420
         TabIndex        =   54
         Top             =   315
         Width           =   1230
      End
      Begin VB.TextBox txtRearDestAddress 
         BackColor       =   &H8000000F&
         DataField       =   "RearDestAddress"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   57
         TabStop         =   0   'False
         Top             =   1350
         Width           =   3300
      End
      Begin VB.TextBox txtRearBOLNbr 
         BackColor       =   &H8000000F&
         DataField       =   "RearBOL"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   60
         TabStop         =   0   'False
         Top             =   2070
         Width           =   1455
      End
      Begin VB.TextBox txtRearCommodity 
         BackColor       =   &H8000000F&
         DataField       =   "RearCommodity"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   61
         TabStop         =   0   'False
         Top             =   2400
         Width           =   1950
      End
      Begin VB.CheckBox chRearCdRtn 
         Caption         =   "Credit Return"
         DataField       =   "RearCreditReturn"
         Enabled         =   0   'False
         Height          =   285
         Left            =   3420
         TabIndex        =   62
         Top             =   2430
         Width           =   1320
      End
      Begin VB.CheckBox chPrntCOARear 
         Caption         =   "Print COA"
         DataField       =   "RearPrintCOA"
         Height          =   375
         Left            =   3435
         TabIndex        =   152
         Top             =   2100
         Width           =   1455
      End
      Begin VB.Label Label48 
         Caption         =   "Formula"
         Height          =   255
         Left            =   360
         TabIndex        =   129
         Top             =   2970
         Width           =   765
      End
      Begin VB.Label Label46 
         Caption         =   "Rack #"
         Height          =   255
         Left            =   2925
         TabIndex        =   127
         Top             =   4005
         Width           =   585
      End
      Begin VB.Label Label33 
         Alignment       =   1  'Right Justify
         Caption         =   "Req. Tons"
         Height          =   285
         Left            =   2835
         TabIndex        =   125
         Top             =   1755
         Width           =   960
      End
      Begin VB.Label Label30 
         Caption         =   "Tank #"
         Height          =   255
         Left            =   360
         TabIndex        =   109
         Top             =   4020
         Width           =   855
      End
      Begin VB.Label Label29 
         Caption         =   "Additive"
         Height          =   255
         Left            =   360
         TabIndex        =   108
         Top             =   3675
         Width           =   855
      End
      Begin VB.Label Label28 
         Caption         =   "Product"
         Height          =   255
         Left            =   360
         TabIndex        =   107
         Top             =   3315
         Width           =   855
      End
      Begin VB.Label Label27 
         Caption         =   "Tare"
         Height          =   255
         Left            =   360
         TabIndex        =   106
         Top             =   4365
         Width           =   855
      End
      Begin VB.Label Label26 
         Caption         =   "Gross"
         Height          =   255
         Left            =   360
         TabIndex        =   105
         Top             =   4725
         Width           =   855
      End
      Begin VB.Label Label25 
         Caption         =   "Net"
         Height          =   255
         Left            =   360
         TabIndex        =   104
         Top             =   5085
         Width           =   855
      End
      Begin VB.Label Label24 
         Caption         =   "Tons"
         Height          =   255
         Left            =   360
         TabIndex        =   103
         Top             =   5445
         Width           =   855
      End
      Begin VB.Label Label38 
         Caption         =   "Truck #"
         Height          =   255
         Left            =   360
         TabIndex        =   92
         Top             =   345
         Width           =   855
      End
      Begin VB.Label Label37 
         Caption         =   "Hauler"
         Height          =   255
         Left            =   360
         TabIndex        =   91
         Top             =   705
         Width           =   855
      End
      Begin VB.Label Label36 
         Caption         =   "Customer"
         Height          =   255
         Left            =   360
         TabIndex        =   90
         Top             =   1050
         Width           =   855
      End
      Begin VB.Label Label35 
         Caption         =   "Contract #"
         Height          =   255
         Left            =   360
         TabIndex        =   89
         Top             =   1770
         Width           =   855
      End
      Begin VB.Label Label34 
         Caption         =   "Contract Ds"
         Height          =   255
         Left            =   360
         TabIndex        =   88
         Top             =   1395
         Width           =   855
      End
      Begin VB.Label Label32 
         Caption         =   "BOL #"
         Height          =   255
         Left            =   360
         TabIndex        =   87
         Top             =   2100
         Width           =   855
      End
      Begin VB.Label Label31 
         Caption         =   "Commodity"
         Height          =   255
         Left            =   360
         TabIndex        =   86
         Top             =   2460
         Width           =   855
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "General Info"
      Height          =   1080
      Left            =   6390
      TabIndex        =   79
      Top             =   90
      Width           =   3885
      Begin VB.TextBox txtOutboundTime 
         BackColor       =   &H8000000F&
         DataField       =   "TimeOut"
         Height          =   285
         Left            =   1740
         TabIndex        =   94
         Top             =   675
         Width           =   2010
      End
      Begin VB.TextBox txtInboundTime 
         BackColor       =   &H8000000F&
         DataField       =   "TimeIn"
         Height          =   285
         Left            =   1740
         TabIndex        =   93
         Top             =   285
         Width           =   2010
      End
      Begin VB.ComboBox cboOutboundOperator 
         DataField       =   "OutBoundOperator"
         Height          =   315
         Left            =   945
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   675
         Width           =   750
      End
      Begin VB.ComboBox cboInboundOperator 
         DataField       =   "InboundOperator"
         Height          =   315
         Left            =   945
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   285
         Width           =   750
      End
      Begin VB.Label Label9 
         Caption         =   "Inbound"
         Height          =   255
         Left            =   135
         TabIndex        =   81
         Top             =   330
         Width           =   765
      End
      Begin VB.Label Label8 
         Caption         =   "Outbound"
         Height          =   255
         Left            =   135
         TabIndex        =   80
         Top             =   720
         Width           =   810
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Front Load Info"
      Height          =   5850
      Left            =   135
      TabIndex        =   73
      Top             =   3015
      Width           =   4980
      Begin VB.CheckBox chPrntCOAFront 
         Caption         =   "Print COA"
         DataField       =   "FrontPrintCOA"
         Height          =   375
         Left            =   3420
         TabIndex        =   151
         Top             =   2100
         Width           =   1455
      End
      Begin VB.TextBox txtFrontBOLNbr 
         BackColor       =   &H8000000F&
         DataField       =   "FrontBOL"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   150
         TabStop         =   0   'False
         Top             =   2070
         Width           =   1455
      End
      Begin VB.CommandButton cmdFrntSrchAdtv 
         Caption         =   "Search"
         Enabled         =   0   'False
         Height          =   285
         Left            =   3720
         TabIndex        =   33
         ToolTipText     =   "Search for a Additive"
         Top             =   3650
         Width           =   855
      End
      Begin VB.CommandButton cmdFrntSrchPrdct 
         Caption         =   "Search"
         Enabled         =   0   'False
         Height          =   285
         Left            =   3720
         TabIndex        =   31
         ToolTipText     =   "Search for a Product"
         Top             =   3320
         Width           =   855
      End
      Begin VB.Frame Frame6 
         Caption         =   "Invisible"
         Height          =   2025
         Left            =   2400
         TabIndex        =   131
         Top             =   4200
         Visible         =   0   'False
         Width           =   2475
         Begin VB.CheckBox chFrontHighRisk 
            Alignment       =   1  'Right Justify
            Caption         =   "High Risk"
            DataField       =   "FrontHighRisk"
            Enabled         =   0   'False
            Height          =   495
            Left            =   1200
            TabIndex        =   154
            Top             =   960
            Width           =   735
         End
         Begin VB.TextBox txtFrontSecTapeNbr 
            DataField       =   "FrontSecurityTapNumber"
            Height          =   285
            Left            =   120
            MaxLength       =   100
            TabIndex        =   153
            Top             =   1680
            Width           =   1725
         End
         Begin VB.TextBox txtFrontDestDs 
            DataField       =   "FrontDestDirections"
            Height          =   315
            Left            =   1110
            TabIndex        =   148
            TabStop         =   0   'False
            Text            =   "Dest Ds"
            Top             =   570
            Width           =   855
         End
         Begin VB.TextBox txtFrontFacilityID 
            DataField       =   "FrontFacilityID"
            Height          =   315
            Left            =   1110
            TabIndex        =   141
            TabStop         =   0   'False
            Text            =   "FacilityID"
            Top             =   210
            Width           =   855
         End
         Begin VB.TextBox txtFrontProductID 
            DataField       =   "FrontProductID"
            Height          =   315
            Left            =   150
            TabIndex        =   135
            TabStop         =   0   'False
            Text            =   "ProductID"
            Top             =   210
            Width           =   855
         End
         Begin VB.TextBox txtFrontAdditiveID 
            DataField       =   "FrontAdditiveID"
            Height          =   315
            Left            =   150
            TabIndex        =   134
            TabStop         =   0   'False
            Text            =   "AdditiveID"
            Top             =   540
            Width           =   855
         End
         Begin VB.TextBox txtFrontDestDt 
            DataField       =   "FrontDestDt"
            Height          =   315
            Left            =   150
            TabIndex        =   133
            TabStop         =   0   'False
            Text            =   "Dest Dt"
            Top             =   1170
            Width           =   855
         End
         Begin VB.TextBox txtFrontArrivalDt 
            DataField       =   "FrontArrivalDt"
            Height          =   315
            Left            =   150
            TabIndex        =   132
            TabStop         =   0   'False
            Text            =   "Arrival Dt"
            Top             =   840
            Width           =   855
         End
         Begin VB.Label Label55 
            Caption         =   "Security Tape #"
            Height          =   255
            Left            =   120
            TabIndex        =   155
            Top             =   1485
            Width           =   1665
         End
      End
      Begin VB.ComboBox cboFrontFormula 
         DataField       =   "FrontFormula"
         Height          =   315
         Left            =   1350
         TabIndex        =   29
         Text            =   "cboFrontFormula"
         Top             =   2925
         Width           =   3255
      End
      Begin VB.TextBox txtFrontRack 
         DataField       =   "FrontRackNbr"
         Height          =   285
         Left            =   3600
         TabIndex        =   35
         Top             =   3975
         Width           =   1005
      End
      Begin VB.TextBox txtFrontReqTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontReqTons"
         Height          =   285
         Left            =   3915
         Locked          =   -1  'True
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   1680
         Width           =   735
      End
      Begin VB.TextBox txtFrontTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontTons"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   52
         TabStop         =   0   'False
         Top             =   5400
         Width           =   1005
      End
      Begin VB.TextBox txtFrontNet 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontNetWt"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   51
         TabStop         =   0   'False
         Top             =   5040
         Width           =   1005
      End
      Begin VB.TextBox txtFrontGross 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontGrossWt"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   50
         TabStop         =   0   'False
         Top             =   4680
         Width           =   1005
      End
      Begin VB.TextBox txtFrontTare 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontTareWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   49
         TabStop         =   0   'False
         Top             =   4320
         Width           =   1005
      End
      Begin VB.TextBox txtFrontProduct 
         BackColor       =   &H8000000F&
         DataField       =   "FrontProduct"
         Enabled         =   0   'False
         Height          =   285
         Left            =   1350
         TabIndex        =   30
         Top             =   3285
         Width           =   2295
      End
      Begin VB.TextBox txtFrontAdditive 
         BackColor       =   &H8000000F&
         DataField       =   "FrontAdditive"
         Enabled         =   0   'False
         Height          =   285
         Left            =   1350
         TabIndex        =   32
         Top             =   3600
         Width           =   2295
      End
      Begin VB.TextBox txtFrontTank 
         DataField       =   "FrontTankNbr"
         Height          =   285
         Left            =   1350
         TabIndex        =   34
         Top             =   3975
         Width           =   1005
      End
      Begin VB.CheckBox chFrontCdRtn 
         Caption         =   "Credit Return"
         DataField       =   "FrontCreditReturn"
         Enabled         =   0   'False
         Height          =   285
         Left            =   3420
         TabIndex        =   28
         Top             =   2430
         Width           =   1320
      End
      Begin VB.TextBox txtFrontCommodity 
         BackColor       =   &H8000000F&
         DataField       =   "FrontCommodity"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   2415
         Width           =   1950
      End
      Begin VB.TextBox txtFrontDestAddress 
         BackColor       =   &H8000000F&
         DataField       =   "FrontDestAddress"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   1350
         Width           =   3300
      End
      Begin VB.CommandButton cmdSearchFront 
         Caption         =   "&Search"
         Height          =   285
         Left            =   3420
         TabIndex        =   21
         Top             =   315
         Width           =   1230
      End
      Begin VB.TextBox txtFrontContract 
         BackColor       =   &H8000000F&
         DataField       =   "FrontContractNbr"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   1710
         Width           =   1455
      End
      Begin VB.TextBox txtFrontHauler 
         BackColor       =   &H8000000F&
         DataField       =   "FrontHauler"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   660
         Width           =   3300
      End
      Begin VB.TextBox txtFrontTruck 
         DataField       =   "TruckNo"
         Height          =   285
         Left            =   1350
         TabIndex        =   20
         Top             =   315
         Width           =   1455
      End
      Begin VB.TextBox txtFrontCust 
         BackColor       =   &H8000000F&
         DataField       =   "FrontCustomer"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Top             =   1005
         Width           =   3300
      End
      Begin VB.Label Label47 
         Caption         =   "Formula"
         Height          =   255
         Left            =   360
         TabIndex        =   128
         Top             =   2970
         Width           =   765
      End
      Begin VB.Label Label45 
         Caption         =   "Rack #"
         Height          =   255
         Left            =   2880
         TabIndex        =   126
         Top             =   4020
         Width           =   585
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Req. Tons"
         Height          =   285
         Left            =   3015
         TabIndex        =   124
         Top             =   1755
         Width           =   780
      End
      Begin VB.Label Label19 
         Caption         =   "Tons"
         Height          =   255
         Left            =   360
         TabIndex        =   101
         Top             =   5445
         Width           =   855
      End
      Begin VB.Label Label18 
         Caption         =   "Net"
         Height          =   255
         Left            =   360
         TabIndex        =   100
         Top             =   5085
         Width           =   855
      End
      Begin VB.Label Label17 
         Caption         =   "Gross"
         Height          =   255
         Left            =   360
         TabIndex        =   99
         Top             =   4725
         Width           =   855
      End
      Begin VB.Label Label16 
         Caption         =   "Tare"
         Height          =   255
         Left            =   360
         TabIndex        =   98
         Top             =   4365
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Product"
         Height          =   255
         Left            =   360
         TabIndex        =   97
         Top             =   3315
         Width           =   855
      End
      Begin VB.Label Label11 
         Caption         =   "Additive"
         Height          =   255
         Left            =   360
         TabIndex        =   96
         Top             =   3675
         Width           =   855
      End
      Begin VB.Label Label7 
         Caption         =   "Tank #"
         Height          =   255
         Left            =   360
         TabIndex        =   95
         Top             =   4020
         Width           =   855
      End
      Begin VB.Label Label14 
         Caption         =   "Commodity"
         Height          =   255
         Left            =   360
         TabIndex        =   84
         Top             =   2460
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "BOL #"
         Height          =   255
         Left            =   360
         TabIndex        =   83
         Top             =   2100
         Width           =   855
      End
      Begin VB.Label Label13 
         Caption         =   "Contract Ds"
         Height          =   255
         Left            =   360
         TabIndex        =   82
         Top             =   1395
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Contract #"
         Height          =   255
         Left            =   360
         TabIndex        =   78
         Top             =   1770
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Customer"
         Height          =   255
         Left            =   360
         TabIndex        =   77
         Top             =   1050
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Hauler"
         Height          =   255
         Left            =   360
         TabIndex        =   76
         Top             =   705
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Truck #"
         Height          =   255
         Left            =   360
         TabIndex        =   75
         Top             =   345
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Scale Capture"
      Height          =   1080
      Left            =   135
      TabIndex        =   74
      Top             =   120
      Width           =   6180
      Begin VB.OptionButton optnDriverOnNo 
         Caption         =   "No"
         Height          =   195
         Left            =   1440
         TabIndex        =   161
         Top             =   810
         Width           =   600
      End
      Begin VB.OptionButton optnDriverOnYes 
         Caption         =   "Yes"
         Height          =   195
         Left            =   840
         TabIndex        =   159
         Top             =   810
         Width           =   615
      End
      Begin VB.CheckBox chDriverOn 
         Caption         =   "Driver On"
         DataField       =   "DriverOn"
         Height          =   225
         Left            =   2910
         TabIndex        =   130
         Top             =   90
         Visible         =   0   'False
         Width           =   1245
      End
      Begin VB.CommandButton cmdTareRear 
         Caption         =   "Tare (F5)"
         Height          =   330
         Left            =   4095
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   585
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.CommandButton cmdGrossRear 
         Caption         =   "Gross (F7)"
         Height          =   330
         Left            =   5085
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   585
         Visible         =   0   'False
         Width           =   915
      End
      Begin VB.CommandButton cmdTareFront 
         Caption         =   "Tare (F2)"
         Height          =   330
         Left            =   2025
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   585
         Width           =   915
      End
      Begin VB.CommandButton cmdGrossFront 
         Caption         =   "Gross (F4)"
         Height          =   330
         Left            =   3015
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   585
         Width           =   915
      End
      Begin VB.TextBox txtScale 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   21.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   525
         Left            =   180
         TabIndex        =   0
         Text            =   "0"
         Top             =   270
         Width           =   1770
      End
      Begin VB.Label Label57 
         Caption         =   "Driver On:"
         Height          =   195
         Left            =   100
         TabIndex        =   160
         Top             =   810
         Width           =   735
      End
      Begin VB.Line Line1 
         Visible         =   0   'False
         X1              =   4095
         X2              =   4725
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Line Line8 
         Visible         =   0   'False
         X1              =   5355
         X2              =   5985
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label23 
         Alignment       =   2  'Center
         Caption         =   "Rear"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4770
         TabIndex        =   111
         Top             =   315
         Visible         =   0   'False
         Width           =   555
      End
      Begin VB.Line Line6 
         X1              =   3285
         X2              =   3915
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Line Line5 
         X1              =   2025
         X2              =   2655
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label22 
         Alignment       =   2  'Center
         Caption         =   "Front"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2700
         TabIndex        =   110
         Top             =   315
         Width           =   555
      End
   End
   Begin VB.TextBox txtScaleMsg 
      BackColor       =   &H8000000F&
      Height          =   1035
      Left            =   30
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   146
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   10335
   End
   Begin VB.Menu mnuGetWeight 
      Caption         =   "Get Weight"
      Visible         =   0   'False
      Begin VB.Menu mnuFrontTare 
         Caption         =   "Front Tare"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuFrontGross 
         Caption         =   "Front Gross"
         Shortcut        =   {F4}
      End
      Begin VB.Menu mnuRearTare 
         Caption         =   "Rear Tare"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuRearGross 
         Caption         =   "Rear Gross"
         Shortcut        =   {F7}
      End
   End
End
Attribute VB_Name = "frmLoadProcessing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public WithEvents rsLoad As ADODB.Recordset
Attribute rsLoad.VB_VarHelpID = -1
Public WithEvents rsLoadExt As ADODB.Recordset  'RKL DEJ 2018-01-09 Added for the new fields
Attribute rsLoadExt.VB_VarHelpID = -1

Dim WithEvents SGSScale As SGSScaleComm.clsScaleComm
Attribute SGSScale.VB_VarHelpID = -1
Dim bCalculatingOverallWt As Boolean
Dim bOverallTargetGallons As Boolean
Dim bInitialLoad As Boolean

Dim bFrontTonsFlag As Boolean
Dim bRearTonsFlag As Boolean
Public lcLoadsTableKey As Long


Sub ResetSplitLoadData()
    On Error GoTo Error
        
    gbLastActivityTime = Date + Time
    
    If chSplitLoad.Value = vbChecked Then
        'Clear... will be different than the front
        
        txtRearFacilityID.Text = Empty
        Call UpdateRSFromObj(txtRearFacilityID)
'        txtRearTruck.Text = Empty
        txtRearHauler.Text = Empty
        Call UpdateRSFromObj(txtRearHauler)
        txtRearCust.Text = Empty
        Call UpdateRSFromObj(txtRearCust)
        txtRearDestAddress.Text = Empty
        Call UpdateRSFromObj(txtRearDestAddress)
        txtRearContract.Text = Empty
        Call UpdateRSFromObj(txtRearContract)
        txtRearReqTons.Text = 0
        Call UpdateRSFromObj(txtRearReqTons)
        txtRearBOLNbr.Text = Empty
        Call UpdateRSFromObj(txtRearBOLNbr)
        txtRearCommodity.Text = Empty
        Call UpdateRSFromObj(txtRearCommodity)
        
        txtRearProductID.Text = Empty
        Call UpdateRSFromObj(txtRearProductID)
        txtRearProduct.Text = Empty
        Call UpdateRSFromObj(txtRearProduct)
        
        txtRearAdditiveID.Text = Empty
        Call UpdateRSFromObj(txtRearAdditiveID)
        txtRearAdditive.Text = Empty
        Call UpdateRSFromObj(txtRearAdditive)
        
        txtRearTank.Text = Empty
        Call UpdateRSFromObj(txtRearTank)
        txtRearRack.Text = Empty
        Call UpdateRSFromObj(txtRearRack)
        txtRearArrivalDt.Text = Empty
        Call UpdateRSFromObj(txtRearArrivalDt)
        txtRearDestDt.Text = Empty
        Call UpdateRSFromObj(txtRearDestDt)
        txtRearDestDs.Text = Empty
        Call UpdateRSFromObj(txtRearDestDs)
        
        rsLoad("RearMcLeodOrderNumber").Value = Null
        rsLoad("RearProject").Value = Null
        rsLoad("RearProductClass").Value = Null
        rsLoad("RearAdditiveClass").Value = Null
        
        'RKL DeJ 2017-12-15 (Start)
        rsLoadExt("RearSrcProductIDOne").Value = Null
        rsLoadExt("RearSrcProductOne").Value = Null
        rsLoadExt("RearSrcProductClassOne").Value = Null

        rsLoadExt("RearSrcProductIDTwo").Value = Null
        rsLoadExt("RearSrcProductTwo").Value = Null
        rsLoadExt("RearSrcProductClassTwo").Value = Null

        rsLoadExt("RearSrcProductIDThree").Value = Null
        rsLoadExt("RearSrcProductThree").Value = Null
        rsLoadExt("RearSrcProductClassThree").Value = Null
        'RKL DeJ 2017-12-15 (Stop)
        
        
    Else
        'Make same as Front
        
        txtRearFacilityID.Text = txtFrontFacilityID.Text
        Call UpdateRSFromObj(txtRearFacilityID)
        txtRearTruck.Text = txtFrontTruck.Text
        Call UpdateRSFromObj(txtRearTruck)
        txtRearHauler.Text = txtFrontHauler.Text
        Call UpdateRSFromObj(txtRearHauler)
        txtRearCust.Text = txtFrontCust.Text
        Call UpdateRSFromObj(txtRearCust)
        txtRearDestAddress.Text = txtFrontDestAddress.Text
        Call UpdateRSFromObj(txtRearDestAddress)
        txtRearContract.Text = txtFrontContract.Text
        Call UpdateRSFromObj(txtRearContract)
        txtRearReqTons.Text = txtFrontReqTons.Text
        Call UpdateRSFromObj(txtRearReqTons)
        txtRearBOLNbr.Text = txtFrontBOLNbr.Text
        Call UpdateRSFromObj(txtRearBOLNbr)
        txtRearCommodity.Text = txtFrontCommodity.Text
        Call UpdateRSFromObj(txtRearCommodity)
        
        txtRearProductID.Text = txtFrontProductID.Text
        Call UpdateRSFromObj(txtRearProductID)
        txtRearProduct.Text = txtFrontProduct.Text
        Call UpdateRSFromObj(txtRearProduct)
        
        txtRearAdditiveID.Text = txtFrontAdditiveID.Text
        Call UpdateRSFromObj(txtRearAdditiveID)
        txtRearAdditive.Text = txtFrontAdditive.Text
        Call UpdateRSFromObj(txtRearAdditive)
        
        txtRearTank.Text = txtFrontTank.Text
        Call UpdateRSFromObj(txtRearTank)
        txtRearRack.Text = txtFrontRack.Text
        Call UpdateRSFromObj(txtRearRack)
        txtRearArrivalDt.Text = txtFrontArrivalDt.Text
        Call UpdateRSFromObj(txtRearArrivalDt)
        txtRearDestDt.Text = txtFrontDestDt.Text
        Call UpdateRSFromObj(txtRearDestDt)
        txtRearDestDs.Text = txtFrontDestDs.Text
        Call UpdateRSFromObj(txtRearDestDs)
        
        rsLoad("RearMcLeodOrderNumber").Value = rsLoad("FrontMcLeodOrderNumber").Value
        rsLoad("RearProject").Value = rsLoad("FrontProject").Value
        rsLoad("RearProductClass").Value = rsLoad("FrontProductClass").Value
        rsLoad("RearAdditiveClass").Value = rsLoad("FrontAdditiveClass").Value
    
        'RKL DeJ 2017-12-15 (Start)
        rsLoadExt("RearSrcProductIDOne").Value = rsLoadExt("FrontSrcProductIDOne").Value
        rsLoadExt("RearSrcProductOne").Value = rsLoadExt("FrontSrcProductOne").Value
        rsLoadExt("RearSrcProductClassOne").Value = rsLoadExt("FrontSrcProductClassOne").Value

        rsLoadExt("RearSrcProductIDTwo").Value = rsLoadExt("FrontSrcProductIDTwo").Value
        rsLoadExt("RearSrcProductTwo").Value = rsLoadExt("FrontSrcProductTwo").Value
        rsLoadExt("RearSrcProductClassTwo").Value = rsLoadExt("FrontSrcProductClassTwo").Value

        rsLoadExt("RearSrcProductIDThree").Value = rsLoadExt("FrontSrcProductIDThree").Value
        rsLoadExt("RearSrcProductThree").Value = rsLoadExt("FrontSrcProductThree").Value
        rsLoadExt("RearSrcProductClassThree").Value = rsLoadExt("FrontSrcProductClassThree").Value
        'RKL DeJ 2017-12-15 (Stop)
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".ResetSplitLoadData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub cboFrontFormula_Validate(Cancel As Boolean)
    
    gbLastActivityTime = Date + Time
    
    If ValidateFormula("Front", IIf(Trim(txtFrontAdditiveID.Text) > "", txtFrontAdditiveID.Text, txtFrontProductID.Text), txtFrontFacilityID.Text, cboFrontFormula.Text) Then
        If chSplitLoad.Value = vbUnchecked And Val(txtRearGal.Text) > 0 Then
            cboRearFormula.Text = cboFrontFormula.Text
            Call UpdateRSFromObj(cboRearFormula)
    '        cboRearFormula.ListIndex = cboFrontFormula.ListIndex
            rsLoad("RearFormula").Value = cboFrontFormula.Text
        End If
    Else
        MsgBox "Invalid Formula for given product/additive.", vbExclamation, "Front Formula Invalid"
        Cancel = True
    End If

    gbLastActivityTime = Date + Time
End Sub

Function ValidateFormula(sLoadFlag As String, sProductID As String, sWhseID As String, sFormulaName As String) As Boolean
    Dim SQL As String
    Dim rsFormula As New ADODB.Recordset
    Dim sFormula As String
    
    gbLastActivityTime = Date + Time
    
    ValidateFormula = False
    SQL = "Select * From vimGetItemFormulaInfo_SGS Where ItemID = '" & Trim(sProductID) & "' AND WhseID = '" & Trim(sWhseID) & "' AND FormulaName = '" & Trim(sFormulaName) & "'"
    rsFormula.CursorLocation = adUseClient
    rsFormula.Open SQL, gbMASConn, adOpenForwardOnly, adLockReadOnly
    
    If rsFormula.EOF Or rsFormula.BOF Then
        'No Formula Found
    Else
        ValidateFormula = True
    End If
    rsFormula.Close
    Set rsFormula = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".ValidateFormula(" & sLoadFlag & ")" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Function

Private Sub cboRearFormula_Validate(Cancel As Boolean)
    gbLastActivityTime = Date + Time
    
    If ValidateFormula("Rear", IIf(Trim(txtRearAdditiveID.Text) > "", txtRearAdditiveID.Text, txtRearProductID.Text), txtFrontFacilityID.Text, cboRearFormula.Text) Then
        'Good Formula
    Else
        MsgBox "Invalid Formula for given product/additive.", vbExclamation, "Rear Formula Invalid"
        Cancel = True
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chAlternatePrinter_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chDriverOn_Click()
    gbLastActivityTime = Date + Time
    
    If chDriverOn.Value = vbChecked Then
        optnDriverOnYes.Value = True
    Else
        optnDriverOnNo.Value = True
    End If
    
    gbLastActivityTime = Date + Time

End Sub

Private Sub chDriverOn_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chFrontCdRtn_Click()
    gbLastActivityTime = Date + Time
    
    If chFrontCdRtn.Value = vbChecked Then
        frameFrontSpecs.Enabled = False
        frameRearSpecs.Enabled = False
    Else
        frameFrontSpecs.Enabled = True
        frameRearSpecs.Enabled = True
    End If
    If chSplitLoad.Value = vbUnchecked Then
        chRearCdRtn.Value = chFrontCdRtn.Value
        Call UpdateRSFromObj(chRearCdRtn)
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chFrontCdRtn_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chFrontHighRisk_Click()
    Call UpdateRSFromObj(chFrontHighRisk)
End Sub

Private Sub chIsTruckBlend_Click()
    On Error GoTo Error
    
    'White - &H80000005
    'Grey - &H8000000F
    
    If chIsTruckBlend.Value = vbChecked Then
        EnableSrcFields &H80000005, True
    Else
        EnableSrcFields &H8000000F, False
    End If
    
    Exit Sub
Error:
End Sub

Private Sub chManualScale_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If chManualScale.Value = vbChecked Then
        txtScale.Locked = False
        txtScale.ForeColor = &H80000008
        txtScale.BackColor = &H80000005
        txtScale.TabStop = True
        
        'RKL DEJ 2017-11-06 START
        txtReadingScaleIndicator1.Visible = False
        txtReadingScaleIndicator2.Visible = False
        txtReadingScaleIndicator3.Visible = False
        'RKL DEJ 2017-11-06 STOP
        
        SGSScale.ClosePort
    Else
        txtScale.Locked = True
        txtScale.ForeColor = &HFFFFFF
        txtScale.BackColor = &H0&
        txtScale.TabStop = False
        
        'RKL DEJ 2017-11-06 START
        txtReadingScaleIndicator1.Visible = True
        txtReadingScaleIndicator2.Visible = True
        txtReadingScaleIndicator3.Visible = True
        
        txtReadingScaleIndicator1.BackColor = &H8000000F
        txtReadingScaleIndicator2.BackColor = &H8000000F
        txtReadingScaleIndicator3.BackColor = &H8000000F
        'RKL DEJ 2017-11-06 STOP
        
        SGSScale.OpenPort
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chManualScale_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chPrntCOAFront_Click()
    gbLastActivityTime = Date + Time

End Sub

Private Sub chPrntCOAFront_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chPrntCOARear_Click()
    gbLastActivityTime = Date + Time

End Sub

Private Sub chPrntCOARear_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chRearCdRtn_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chRearHighRisk_Click()
    Call UpdateRSFromObj(chRearHighRisk)

End Sub

Private Sub chSplitLoad_Click()
    
    gbLastActivityTime = Date + Time
    
    If chSplitLoad.Value = vbChecked Then
        chWtTarget.Value = 0
        Call UpdateRSFromObj(chWtTarget)
        chWtTarget.Enabled = False
        'txtTotalTargetGrossWt.Text = 0
        'txtTotalTargetGrossWt.Locked = True
        'txtTotalTargetGrossWt.TabStop = False
        'txtTotalTargetGrossWt.BackColor = &H8000000F
        txtTotalTargetGrossWt.Locked = False
        txtTotalTargetGrossWt.TabStop = True
        txtTotalTargetGrossWt.BackColor = &H80000005
        txtFrontTargetGrossWt.Locked = False
        txtFrontTargetGrossWt.TabStop = True
        txtFrontTargetGrossWt.BackColor = &H80000005
'        txtRearTargetGrossWt.Enabled = True
        txtRearTargetGrossWt.Locked = False
        txtRearTargetGrossWt.TabStop = True
        txtRearTargetGrossWt.BackColor = &H80000005
'        txtRearTargetWtGal.Enabled = True
        txtRearTargetWtGal.Locked = False
        txtRearTargetWtGal.TabStop = True
        txtRearTargetWtGal.BackColor = &H80000005
'        frameRearLoad.Visible = True
        frameRearLoadOverlay.Visible = False
    Else
'        frameRearLoad.Visible = False
        frameRearLoadOverlay.Visible = True
        chWtTarget.Enabled = True
        If chWtTarget.Value = vbChecked Then
            txtTotalTargetGrossWt.Locked = True
            txtTotalTargetGrossWt.TabStop = False
            txtTotalTargetGrossWt.BackColor = &H8000000F
            txtFrontTargetGrossWt.Locked = False
            txtFrontTargetGrossWt.TabStop = True
            txtFrontTargetGrossWt.BackColor = &H80000005
'            txtRearTargetGrossWt.Enabled = True
            txtRearTargetGrossWt.Locked = False
            txtRearTargetGrossWt.TabStop = True
            txtRearTargetGrossWt.BackColor = &H80000005
'            txtRearTargetWtGal.Enabled = True
            txtRearTargetWtGal.Locked = False
            txtRearTargetWtGal.TabStop = True
            txtRearTargetWtGal.BackColor = &H80000005
        Else
            txtTotalTargetGrossWt.Locked = False
            txtTotalTargetGrossWt.TabStop = True
            txtTotalTargetGrossWt.BackColor = &H80000005
            txtFrontTargetGrossWt.Locked = True
            txtFrontTargetGrossWt.TabStop = False
            txtFrontTargetGrossWt.BackColor = &H8000000F
            frameRearLoadOverlay.Visible = True
'            txtRearTargetGrossWt.Enabled = False
            txtRearTargetGrossWt.Locked = True
            txtRearTargetGrossWt.TabStop = False
            txtRearTargetGrossWt.BackColor = &H8000000F
'            txtRearTargetWtGal.Enabled = False
            txtRearTargetWtGal.Locked = True
            txtRearTargetWtGal.TabStop = False
            txtRearTargetWtGal.BackColor = &H8000000F
        End If
    End If
    
    If Not bInitialLoad Then
        Call ResetSplitLoadData
    End If
    
    gbLastActivityTime = Date + Time

End Sub

Private Sub chSplitLoad_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chWTTarget_Click()
    gbLastActivityTime = Date + Time
    
    If chWtTarget.Value = vbChecked Then
        frameRearLoadOverlay.Visible = True
        chSplitLoad.Value = 0
Call UpdateRSFromObj(chSplitLoad)
        chSplitLoad.Enabled = False
        txtTotalTargetGrossWt.Text = 0
        txtTotalTargetGrossWt.Locked = True
        txtTotalTargetGrossWt.TabStop = False
        txtTotalTargetGrossWt.BackColor = &H8000000F
        txtFrontTargetGrossWt.Locked = False
        txtFrontTargetGrossWt.TabStop = True
        txtFrontTargetGrossWt.BackColor = &H80000005
'        txtRearTargetGrossWt.Enabled = True
        txtRearTargetGrossWt.Locked = False
        txtRearTargetGrossWt.TabStop = True
        txtRearTargetGrossWt.BackColor = &H80000005
'        txtRearTargetWtGal.Enabled = True
        txtRearTargetWtGal.Locked = False
        txtRearTargetWtGal.TabStop = True
        txtRearTargetWtGal.BackColor = &H80000005
    Else
        chSplitLoad.Enabled = True
        If chSplitLoad.Value = vbChecked Then
            frameRearLoadOverlay.Visible = False
            chWtTarget.Enabled = True
'            txtRearTargetGrossWt.Enabled = True
            txtRearTargetGrossWt.Locked = False
            txtRearTargetGrossWt.TabStop = True
            txtRearTargetGrossWt.BackColor = &H80000005
'            txtRearTargetWtGal.Enabled = True
            txtRearTargetWtGal.Locked = False
            txtRearTargetWtGal.TabStop = True
            txtRearTargetWtGal.BackColor = &H80000005
        Else
            frameRearLoadOverlay.Visible = True
'            txtRearTargetGrossWt.Enabled = False
            txtRearTargetGrossWt.Locked = True
            txtRearTargetGrossWt.TabStop = False
            txtRearTargetGrossWt.BackColor = &H8000000F
'            txtRearTargetWtGal.Enabled = False
            txtRearTargetWtGal.BackColor = &H8000000F
            txtRearTargetWtGal.Locked = True
            txtRearTargetWtGal.TabStop = False
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chWtTarget_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdFrntSrchAdtv_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = False
    
    frmProductSearch.ItemID = ConvertToString(rsLoad("FrontAdditiveID").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoad("FrontAdditive").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoad("FrontAdditiveClass").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    rsLoad("FrontAdditiveID").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoad("FrontAdditive").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoad("FrontAdditiveClass").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
    If chSplitLoad.Value = vbUnchecked Then
        Call ResetSplitLoadData
    End If

    gbLastActivityTime = Date + Time
    
    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdFrntSrchAdtv_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True

End Sub

Private Sub cmdFrntSrchAdtv_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdFrntSrchPrdct_Click()
    On Error GoTo Error
    
    tmrAutoReturnNoActivity.Enabled = False
    gbLastActivityTime = Date + Time
    
    frmProductSearch.ItemID = ConvertToString(rsLoad("FrontProductID").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoad("FrontProduct").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoad("FrontProductClass").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    'Reset Additive after change in product
    txtFrontAdditiveID.Text = ""
Call UpdateRSFromObj(txtFrontAdditiveID)
    txtFrontAdditive.Text = ""
Call UpdateRSFromObj(txtFrontAdditive)
    DoEvents
    
    rsLoad("FrontProductID").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoad("FrontProduct").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoad("FrontProductClass").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
    If chSplitLoad.Value = vbUnchecked Then
        Call ResetSplitLoadData
    End If
    
    Call ItemClassAdditive      'RKL DEJ 12/18/14
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdFrntSrchPrdct_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
End Sub

Private Sub cmdFrntSrchPrdct_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdGrossFront_Click()
    gbLastActivityTime = Date + Time
    
    'RKL DEJ 2017-11-06 (START)
    txtReadingScaleIndicator1.BackColor = &H8000000F
    txtReadingScaleIndicator2.BackColor = &H8000000F
    txtReadingScaleIndicator3.BackColor = &H8000000F
    'RKL DEJ 2017-11-06 (STOP)
    
    If Val(txtFrontGross.Text) > 0 Then
        gbResponse = MsgBox("Front Gross Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    End If
    txtFrontGross.Text = Val(txtScale.Text)
    Call UpdateRSFromObj(txtFrontGross)
    If Trim(txtInboundTime.Text) = "" Then
        txtInboundTime.Text = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
        Call UpdateRSFromObj(txtInboundTime)
    Else
        txtOutboundTime.Text = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
        Call UpdateRSFromObj(txtOutboundTime)
    End If
    txtScale.Text = 0
    Call UpdateRSFromObj(txtScale)
    rsLoad("ManualScaling").Value = chManualScale
    txtScale.SetFocus

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdGrossFront_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdGrossRear_Click()
    gbLastActivityTime = Date + Time
    
    'RKL DEJ 2017-11-06 (START)
    txtReadingScaleIndicator1.BackColor = &H8000000F
    txtReadingScaleIndicator2.BackColor = &H8000000F
    txtReadingScaleIndicator3.BackColor = &H8000000F
    'RKL DEJ 2017-11-06 (STOP)
    
    If Val(txtRearGross.Text) > 0 Then
        gbResponse = MsgBox("Rear Gross Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    End If
    txtRearGross.Text = Val(txtScale.Text)
    Call UpdateRSFromObj(txtRearGross)
    txtScale.Text = 0
    Call UpdateRSFromObj(txtScale)
    rsLoad("ManualScaling").Value = chManualScale
    txtScale.SetFocus

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdGrossRear_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdLaps_Click()
    gbLastActivityTime = Date + Time
    
    txtLaps.Text = Val(txtLaps.Text) + 1
    Call UpdateRSFromObj(txtLaps)
    If Val(txtLaps.Text) > 4 Then
        'Reset to Zero
        txtLaps.Text = 0
        Call UpdateRSFromObj(txtLaps)
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdLaps_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdPrintBOL_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdPrintBS_Click()
    Dim prt As Printer
    Dim sDriverName As String
    Dim sDeviceName As String
    Dim sPort As String
    
    Dim bIsHighRisk As Boolean      'RKL DEJ
    Dim bCertRequired As Boolean    'RKL DEJ
    
    On Error GoTo Error
    
    tmrAutoReturnNoActivity.Enabled = False
    
    gbLastActivityTime = Date + Time
    
    Call UpdateRSFromObjs
    
'    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
            
        End If
'    End If
    
    'RKL DEJ 2018-01-09 Added Save
    If rsLoadExt.State = 1 Then
        If UpdateRS(rsLoadExt) = False Then
            MsgBox "There was an error saving the data (Extension).  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    
    If ValidateData("LBS") = False Then
        tmrAutoReturnNoActivity.Enabled = True
        Exit Sub
    End If
    
    'Check for Required COA
    Call CheckCOARequired
    
    Me.MousePointer = vbHourglass
    
    'RKL DEJ 11/25/14 moved printing code to new method
    If chAlternatePrinter = vbChecked Then
        sDeviceName = gbALTERNATEPRINTER
    Else
        sDeviceName = gbSCALEPASSPRINTER
    End If
    
    tmrAutoReturnNoActivity.Enabled = False
    
    If basMethods.PrintLBS(gbLoadsTableKey, False, 1, sDeviceName) = False Then
        'There was an error printing.
        MsgBox "The print LBS function returned failure.", vbInformation, "Scale Pass"
        
    End If
    
'    Set crxApp = New CRAXDRT.Application
'    Set crxRpt = crxApp.OpenReport(App.Path & "\Reports\LoadingBatchSheet.rpt")
'    crxRpt.RecordSelectionFormula = "{Loads.LoadsTableKey} = " & gbLoadsTableKey
'    If chAlternatePrinter = vbChecked Then
'        sDeviceName = gbALTERNATEPRINTER
'    Else
'        sDeviceName = gbSCALEPASSPRINTER
'    End If
'
'    For Each prt In Printers
'        If prt.DeviceName = sDeviceName Then
'            Set Printer = prt
'            sDriverName = prt.DriverName
'            sPort = prt.Port
'            Exit For
'        End If
'    Next
'    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
'
'    tmrAutoReturnNoActivity.Enabled = False
'
'    If Mid(Command$, 1, 7) = "Testing" Then
'        Call ShowCRViewer
'    Else
'        crxRpt.PrintOut False, 1
'    End If
        
'****************************************************************
'RKL DEJ 9/25/13 (START) - Print Lables
'****************************************************************
    If gbPrintLabels = True And gbPrintLabelAfterBOL = False And chLblSkipPrint.Value = vbUnchecked Then
        If Abs(ConvertToDouble(rsLoad("FrontCOARequired").Value)) > 0 Then
            bCertRequired = True
        Else
            bCertRequired = False
        End If
        
        If Abs(ConvertToDouble(rsLoad("FrontHighRisk").Value)) > 0 Then
            bIsHighRisk = True
        Else
            bIsHighRisk = False
        End If
        
        'Only print if not a return
        If Abs(ConvertToDouble(rsLoad("FrontCreditReturn").Value)) = 0 Then
            If IsTransferOrder(Trim(txtFrontCommodity.Text)) = False Then     'RKL DEJ 5/22/14 Kevin asked not to print for transfers
    '        If IsTransferOrder(ConvertToString(rsLoad("FrontCommodity").Value)) = False Then     'RKL DEJ 5/22/14 Kevin asked not to print for transfers
                'Print the Label
                If basMethods.PrintLabel(rsLoad("LoadsTableKey").Value, "Front", bIsHighRisk, bCertRequired, , , , True, True) = False Then   'RKL DEJ 1/22/14 added: True: print tons and True: isFromLBLs
                    MsgBox "There was an error printing the Label for the Front Load", vbInformation, "Bad Label Print"
                End If
            End If
        End If
        
        If Abs(ConvertToDouble(rsLoad("SplitLoad").Value)) = 1 Then
            'Split Load so Print Label for rear
            If Abs(ConvertToDouble(rsLoad("RearCOARequired").Value)) > 0 Then
                bCertRequired = True
            Else
                bCertRequired = False
            End If
            
            If Abs(ConvertToDouble(rsLoad("RearHighRisk").Value)) > 0 Then
                bIsHighRisk = True
            Else
                bIsHighRisk = False
            End If
            
            'Only print if not a return
            If Abs(ConvertToDouble(rsLoad("RearCreditReturn").Value)) = 0 Then
                If IsTransferOrder(Trim(txtRearCommodity.Text)) = False Then     'RKL DEJ 5/22/14 Kevin asked not to print for transfers
        '        If IsTransferOrder(ConvertToString(rsLoad("RearCommodity").Value)) = False Then     'RKL DEJ 5/22/14 Kevin asked not to print for transfers
                    'Print the Label
                    If basMethods.PrintLabel(rsLoad("LoadsTableKey").Value, "Rear", bIsHighRisk, bCertRequired, , , , , True) = False Then     'RKL DEJ 1/22/14 added True - isFromLBLs
                        MsgBox "There was an error printing the Label for the Rear Load", vbInformation, "Bad Label Print"
                    End If
                End If
            End If
        End If
    End If
'****************************************************************
'RKL DEJ 9/25/13 (STOP) - Print Lables
'****************************************************************
    
    gbLastActivityTime = Date + Time
    
    tmrAutoReturnNoActivity.Enabled = True
    
    Me.MousePointer = vbArrow
    
    gbLastActivityTime = Date + Time
    
    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    Me.MousePointer = vbNormal
    MsgBox Me.Name & ".cmdPrintBS_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time

    tmrAutoReturnNoActivity.Enabled = True
End Sub

Private Sub cmdPrintBS_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

'****************************************************************
'RKL DEJ 9/25/13 (START) - Print Lables
'****************************************************************
Private Sub cmdPrintExtraLbl_Click()
    On Error GoTo Error
    
    Dim bCertRequired As Boolean
    Dim bIsHighRisk As Boolean
    
    tmrAutoReturnNoActivity.Enabled = False
    
    gbLastActivityTime = Date + Time

    Call UpdateRSFromObjs
    
    If UpdateRS(rsLoad) = False Then
        MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        
    End If
    
    'RKL DEJ 2018-01-09 Added Save
    If rsLoadExt.State = 1 Then
        If UpdateRS(rsLoadExt) = False Then
            MsgBox "There was an error saving the data (Extension).  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    
    If Abs(ConvertToDouble(rsLoad("FrontCOARequired").Value)) > 0 Then
        bCertRequired = True
    Else
        bCertRequired = False
    End If
    
    If Abs(ConvertToDouble(rsLoad("FrontHighRisk").Value)) > 0 Then
        bIsHighRisk = True
    Else
        bIsHighRisk = False
    End If
    
    'Only print if not a return
    If Abs(ConvertToDouble(rsLoad("FrontCreditReturn").Value)) = 0 Then
        'Print the Label
        If basMethods.PrintLabel(rsLoad("LoadsTableKey").Value, "Front", bIsHighRisk, bCertRequired, True, gbLablePrinter) = False Then
            MsgBox "There was an error printing the Label for the Front Load", vbInformation, "Bad Label Print"
        End If
    End If

    If Abs(ConvertToDouble(rsLoad("SplitLoad").Value)) = 1 Then
        'Split Load so Print Label for rear
        If Abs(ConvertToDouble(rsLoad("RearCOARequired").Value)) > 0 Then
            bCertRequired = True
        Else
            bCertRequired = False
        End If
        
        If Abs(ConvertToDouble(rsLoad("RearHighRisk").Value)) = 0 Then
            bIsHighRisk = True
        Else
            bIsHighRisk = False
        End If
        
        'Only print if not a return
        If Abs(ConvertToDouble(rsLoad("RearCreditReturn").Value)) = 0 Then
            'Print the Label
            If basMethods.PrintLabel(rsLoad("LoadsTableKey").Value, "Rear", bIsHighRisk, bCertRequired, True, gbLablePrinter) = False Then
                MsgBox "There was an error printing the Label for the Rear Load", vbInformation, "Bad Label Print"
            End If
        End If
    End If
    gbLastActivityTime = Date + Time

    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    Me.MousePointer = vbNormal
    MsgBox Me.Name & ".cmdPrintExtraLbl_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time

    tmrAutoReturnNoActivity.Enabled = True
End Sub
'****************************************************************
'RKL DEJ 9/25/13 (STOP) - Print Lables
'****************************************************************

Private Sub cmdPrintWTTicket_Click()
    Dim wt As WeightTicket
    
    tmrAutoReturnNoActivity.Enabled = False
    
    gbLastActivityTime = Date + Time
    
    Call UpdateRSFromObjs
    
    If chDriverOn.Value = vbChecked Then
        wt.DriverOn = True
    Else
        wt.DriverOn = False
    End If
    
    wt.GrossFront = ConvertToDouble(txtFrontGross.Text)
    wt.GrossRear = ConvertToDouble(txtRearGross.Text)
    wt.GrossTotal = wt.GrossFront + wt.GrossRear
    
    wt.InBoundOpr = ConvertToString(cboInboundOperator.Text)
    wt.OutBoundOpr = ConvertToString(cboOutboundOperator.Text)
    
    If chManualScale.Value = vbChecked Then
        wt.ManualScaleing = True
    Else
        wt.ManualScaleing = False
    End If
    
    wt.NetFront = ConvertToDouble(txtFrontNet.Text)
    wt.NetRear = ConvertToDouble(txtRearNet.Text)
    wt.NetTotal = wt.NetFront + wt.NetRear
    
    wt.TareFront = ConvertToDouble(txtFrontTare.Text)
    wt.TareRear = ConvertToDouble(txtRearTare.Text)
    wt.TareTotal = wt.TareFront + wt.TareRear
    
    wt.TicketNo = Empty     'No Ticket Numbers....
    
    wt.TruckNo = Trim(txtFrontTruck.Text)
    
    If PrintWeightTicket(wt, 2) = True Then
        MsgBox "Ticket has been sent to the printer.", vbInformation, "Printed"
    Else
        MsgBox "There was an error printing the Ticket.", vbExclamation, "Not Printed"
    End If
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True

End Sub

Private Sub cmdPrintWTTicket_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdRearSrchAdtv_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    frmProductSearch.ItemID = ConvertToString(rsLoad("RearAdditiveID").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoad("RearAdditive").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoad("RearAdditiveClass").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    rsLoad("RearAdditiveID").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoad("RearAdditive").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoad("RearAdditiveClass").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdRearSrchAdtv_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdRearSrchAdtv_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdRearSrchPrdct_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    frmProductSearch.ItemID = ConvertToString(rsLoad("RearProductID").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoad("RearProduct").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoad("RearProductClass").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    'Reset Additive after change in product
    txtFrontAdditiveID.Text = ""
Call UpdateRSFromObj(txtFrontAdditiveID)
    txtFrontAdditive.Text = ""
Call UpdateRSFromObj(txtFrontAdditive)
    DoEvents
    
    rsLoad("RearProductID").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoad("RearProduct").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoad("RearProductClass").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
    Call ItemClassAdditive      'RKL DEJ 12/18/14
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdRearSrchAdtv_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Private Sub cmdRearSrchPrdct_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdRSValues_Click()
    On Error GoTo Error
    
    Dim ErrMsg As String
    Dim Fld As ADODB.Field
    
    For Each Fld In rsLoad.Fields
        If IsNull(Fld.Value) = True Then
            ErrMsg = ErrMsg & Fld.Name & " = Null" & vbCrLf
        Else
            ErrMsg = ErrMsg & Fld.Name & " = " & CStr(Fld.Value) & vbCrLf
        End If
        
        ErrMsg = ErrMsg & Fld.Name & ", "
    Next
    
    ErrMsg = ErrMsg & vbCrLf & vbCrLf & "****************************************************************************" & vbCrLf
    
    For Each Fld In rsLoad.Fields
        
        ErrMsg = ErrMsg & Fld.Name & ", "
    Next
    
    
    frmCommError.DisplayError ErrMsg
    frmCommError.Show vbModal, Me
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdRSValues_Click()" & vbCrLf & _
    "Error:" & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub

Private Sub cmdSave_Click()
    On Error GoTo Error
        
    gbLastActivityTime = Date + Time
    
    Call UpdateRSFromObjs
    
    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    
    'RKL DEJ 2018-01-09 Added Save
    If rsLoadExt.State = 1 Then
        If UpdateRS(rsLoadExt) = False Then
            MsgBox "There was an error saving the data (Extension).  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    
    gbLastActivityTime = Date + Time
    
    DoEvents
    
    Unload Me
    
    Exit Sub
Error:
    MsgBox Me.Name & "cmdSave_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    Unload Me

End Sub

Private Sub cmdSave_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdSearchFront_Click()
    On Error GoTo Error
    
    Dim rsExistingLoads As New ADODB.Recordset
    Dim SQL As String
    Dim TestRS As New ADODB.Recordset
    
    tmrAutoReturnNoActivity.Enabled = False
    
    gbLastActivityTime = Date + Time
    
    'Make sure the UnloadfrmLoadSearch form is not currently loaded... if it is the Record set will not be loaded.
    Call UnloadfrmLoadSearch
    
    gbProceed = False
    frmLoadSearch.Show vbModal
    
    Set TestRS = frmLoadSearch.grdLoadSearch.DataSource 'SGS DEJ 3/28/11 added to fix the error when no records are selected.
    
    If Not TestRS Is Nothing Then
        If TestRS.State = 1 Then
            If Not (TestRS.EOF Or TestRS.BOF) Then 'SGS DEJ 3/28/11 added to fix the error when no records are selected.
                If gbProceed = True Then
                    If Trim(gbPlantPrefix) + "-" + Trim(str(Val(frmLoadSearch.grdLoadSearch.Columns("Order No")))) = Trim(txtRearBOLNbr.Text) Then
                        MsgBox "Front load can't be the same as the Rear load.", vbExclamation, "Load Invalid"
                    Else
                        SQL = "Select * From Loads Where FrontMcLeodOrderNumber = '" & frmLoadSearch.grdLoadSearch.Columns("Order No") & "' OR RearMcLeodOrderNumber = '" & frmLoadSearch.grdLoadSearch.Columns("Order No") & "'"
                        rsExistingLoads.CursorLocation = adUseClient
                        rsExistingLoads.Open SQL, gbScaleConn, adOpenForwardOnly, adLockReadOnly
                        
                        If rsExistingLoads.RecordCount > 0 Then
                            MsgBox "Selected load already being scaled.", vbExclamation, "Load Invalid"
                        Else
                            txtFrontFacilityID.Text = Trim(frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                            Call UpdateRSFromObj(txtFrontFacilityID)
                            txtFrontTruck.Text = frmLoadSearch.grdLoadSearch.Columns("Truck No")
                            Call UpdateRSFromObj(txtFrontTruck)
                            txtFrontHauler.Text = frmLoadSearch.grdLoadSearch.Columns("owner_name")
                            Call UpdateRSFromObj(txtFrontHauler)
                            txtFrontCust.Text = frmLoadSearch.grdLoadSearch.Columns("Customer Name")
                            Call UpdateRSFromObj(txtFrontCust)
                            txtFrontDestAddress.Text = frmLoadSearch.grdLoadSearch.Columns("Dest Address")
                            Call UpdateRSFromObj(txtFrontDestAddress)
                            txtFrontContract.Text = frmLoadSearch.grdLoadSearch.Columns("Contract No")

'RKL DEJ 2016-06-30 (Start)
Call GetMASWhseKeyByBSO(Trim(txtFrontContract.Text), Trim(frmLoadSearch.grdLoadSearch.Columns("base_product_number")))
If gbCustOwnedProd = True Then
    Call GetMASWhseKey  'RKL DEJ 2017-12-19 Switched to this method for Valero Who has no MAS 500 Contracts.
End If
'RKL DEJ 2016-06-30 (Stop)

'RKL DEJ 2017-07-31 (Start)
rsLoad("FrontWarrantyChipSeal").Value = GetMASWarrantyChipSealFlg(Trim(txtFrontContract.Text))
'RKL DEJ 2017-07-31 (Stop)

'RKL DEJ 5/30/14 changed some logic with the Enforce COA
If IsCertRequired(txtFrontContract.Text, txtFrontFacilityID.Text, frmLoadSearch.grdLoadSearch.Columns("base_product_number")) = True Then
'If IsCertRequired(txtFrontContract.Text, txtFrontFacilityID.Text, frmLoadSearch.grdLoadSearch.Columns("base_product_number")) = True And gbEnforceCOA = True Then
    
    chPrntCOAFront.Value = vbChecked

    'RKL DEJ 5/30/14 (START)
    'Always allow the COA printing but only force the COA when the Enforce COA flag is set
    'Enforce by not allowing the user to unselect the Print COA check box
    If gbEnforceCOA = True Then
        chPrntCOAFront.Enabled = False
    Else
        chPrntCOAFront.Enabled = False  'RKL DEJ 2018-01-05 changed from true to false Always try to print the COA Lock this down.
    End If
    'RKL DEJ 5/30/14 (STOP)
    
    rsLoad("FrontPrintCOA").Value = 1
    rsLoad("FrontCOARequired").Value = 1

Else
    chPrntCOAFront.Value = vbChecked  'RKL DEJ 2018-01-05 changed from vbUnChecked to vbChecked Always try to print the COA Lock this down.
    chPrntCOAFront.Enabled = False    'RKL DEJ 2018-01-05 changed from true to false Always try to print the COA Lock this down.

    rsLoad("FrontPrintCOA").Value = 1       'RKL DEJ 2018-01-05 changed from 0 to 1 Always try to print the COA Lock this down.
    rsLoad("FrontCOARequired").Value = 0
End If

If IsHighRiskBSO(txtFrontContract.Text, txtFrontFacilityID.Text, frmLoadSearch.grdLoadSearch.Columns("base_product_number")) = True Then
    chFrontHighRisk.Value = vbChecked
    rsLoad("FrontHighRisk").Value = vbChecked
Else
    chFrontHighRisk.Value = vbUnchecked
    rsLoad("FrontHighRisk").Value = vbUnchecked
End If
                            Call UpdateRSFromObj(txtFrontContract)
                            txtFrontReqTons.Text = ConvertToDouble(frmLoadSearch.grdLoadSearch.Columns("Tons"))
                            Call UpdateRSFromObj(txtFrontReqTons)
                            txtFrontBOLNbr.Text = Trim(gbPlantPrefix) + "-" + Trim(str(Val(frmLoadSearch.grdLoadSearch.Columns("Order No"))))
                            Call UpdateRSFromObj(txtFrontBOLNbr)
                            txtFrontCommodity.Text = frmLoadSearch.grdLoadSearch.Columns("Commodity")
                            Call UpdateRSFromObj(txtFrontCommodity)
                            If InStr(1, txtFrontCommodity.Text, "/CR", vbTextCompare) > 1 Then
                                chFrontCdRtn.Value = vbChecked
                                Call UpdateRSFromObj(chFrontCdRtn)
                            Else
                                chFrontCdRtn.Value = vbUnchecked
                                Call UpdateRSFromObj(chFrontCdRtn)
                            End If
                            'Because of 'Change' logic elsewhere, the Additive needs to be set before the Product
                            txtFrontAdditiveID.Text = frmLoadSearch.grdLoadSearch.Columns("additive_product_number")
                            Call UpdateRSFromObj(txtFrontAdditiveID)
                            txtFrontAdditive.Text = frmLoadSearch.grdLoadSearch.Columns("Additive")
                            Call UpdateRSFromObj(txtFrontAdditive)
                            txtFrontProductID.Text = frmLoadSearch.grdLoadSearch.Columns("base_product_number")
                            Call UpdateRSFromObj(txtFrontProductID)
                            txtFrontProduct.Text = frmLoadSearch.grdLoadSearch.Columns("Product")
                            Call UpdateRSFromObj(txtFrontProduct)
                            
                            txtFrontTank.Text = frmLoadSearch.grdLoadSearch.Columns("tank_no")
                            Call UpdateRSFromObj(txtFrontTank)
                            txtFrontArrivalDt.Text = frmLoadSearch.grdLoadSearch.Columns("Load Date/Time")
                            Call UpdateRSFromObj(txtFrontArrivalDt)
                            txtFrontDestDt.Text = frmLoadSearch.grdLoadSearch.Columns("dest_sched_arrival")
                            Call UpdateRSFromObj(txtFrontDestDt)
                            txtFrontDestDs.Text = Trim(frmLoadSearch.grdLoadSearch.Columns("dest_directions"))
                            Call UpdateRSFromObj(txtFrontDestDs)
                            
                            rsLoad("FrontMcLeodOrderNumber").Value = frmLoadSearch.grdLoadSearch.Columns("Order No")
                            rsLoad("FrontProject").Value = frmLoadSearch.grdLoadSearch.Columns("destination_name")
                            rsLoad("FrontProductClass").Value = GetItemClassID(frmLoadSearch.grdLoadSearch.Columns("base_product_number"), frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                            rsLoad("FrontAdditiveClass").Value = GetItemClassID(frmLoadSearch.grdLoadSearch.Columns("additive_product_number"), frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                            
                    '        If chSplitLoad.Value <> vbChecked And chWtTarget.Value <> vbChecked Then
                            If chSplitLoad.Value <> vbChecked And Val(txtRearGal.Text) > 0 Then
                                'Load 'Rear' with same stuff
                                txtRearFacilityID.Text = Trim(frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                                Call UpdateRSFromObj(txtRearFacilityID)
                                txtRearTruck.Text = frmLoadSearch.grdLoadSearch.Columns("Truck No")
                                Call UpdateRSFromObj(txtRearTruck)
                                txtRearHauler.Text = frmLoadSearch.grdLoadSearch.Columns("owner_name")
                                Call UpdateRSFromObj(txtRearHauler)
                                txtRearCust.Text = frmLoadSearch.grdLoadSearch.Columns("Customer Name")
                                Call UpdateRSFromObj(txtRearCust)
                                txtRearDestAddress.Text = frmLoadSearch.grdLoadSearch.Columns("Dest Address")
                                Call UpdateRSFromObj(txtRearDestAddress)
                                txtRearContract.Text = frmLoadSearch.grdLoadSearch.Columns("Contract No")
                                Call UpdateRSFromObj(txtRearContract)
                                txtRearReqTons.Text = ConvertToDouble(frmLoadSearch.grdLoadSearch.Columns("Tons"))
                                Call UpdateRSFromObj(txtRearReqTons)
                                txtRearBOLNbr.Text = Trim(gbPlantPrefix) + "-" + Trim(str(Val(frmLoadSearch.grdLoadSearch.Columns("Order No"))))
                                Call UpdateRSFromObj(txtRearBOLNbr)
                                txtRearCommodity.Text = frmLoadSearch.grdLoadSearch.Columns("Commodity")
                                Call UpdateRSFromObj(txtRearCommodity)
                                txtRearProductID.Text = frmLoadSearch.grdLoadSearch.Columns("base_product_number")
                                Call UpdateRSFromObj(txtRearProductID)
                                txtRearProduct.Text = frmLoadSearch.grdLoadSearch.Columns("Product")
                                Call UpdateRSFromObj(txtRearProduct)
                                txtRearAdditiveID.Text = frmLoadSearch.grdLoadSearch.Columns("additive_product_number")
                                Call UpdateRSFromObj(txtRearAdditiveID)
                                txtRearAdditive.Text = frmLoadSearch.grdLoadSearch.Columns("Additive")
                                Call UpdateRSFromObj(txtRearAdditive)
                                txtRearTank.Text = frmLoadSearch.grdLoadSearch.Columns("tank_no")
                                Call UpdateRSFromObj(txtRearTank)
                                txtRearArrivalDt.Text = frmLoadSearch.grdLoadSearch.Columns("Load Date/Time")
                                Call UpdateRSFromObj(txtRearArrivalDt)
                                txtRearDestDt.Text = frmLoadSearch.grdLoadSearch.Columns("dest_sched_arrival")
                                Call UpdateRSFromObj(txtRearDestDt)
                                txtRearDestDs.Text = frmLoadSearch.grdLoadSearch.Columns("dest_directions")
                                Call UpdateRSFromObj(txtRearDestDs)
                                
                                rsLoad("RearMcLeodOrderNumber").Value = frmLoadSearch.grdLoadSearch.Columns("Order No")
                                rsLoad("RearProject").Value = frmLoadSearch.grdLoadSearch.Columns("destination_name")
                                rsLoad("RearProductClass").Value = GetItemClassID(frmLoadSearch.grdLoadSearch.Columns("base_product_number"), frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                                rsLoad("RearAdditiveClass").Value = GetItemClassID(frmLoadSearch.grdLoadSearch.Columns("additive_product_number"), frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                                
                            End If
                            
                            'SGS DEJ 11/05/09 (START)
                            rsLoad("Trailer1_ID").Value = frmLoadSearch.grdLoadSearch.Columns("Trailer1_ID")
                            rsLoad("Trailer2_ID").Value = frmLoadSearch.grdLoadSearch.Columns("Trailer2_ID")
                            rsLoad("Carrier").Value = Empty
                            rsLoad("FrontCarrier_ID").Value = frmLoadSearch.grdLoadSearch.Columns("Carrier_ID")
                            rsLoad("FrontCarrier_Name").Value = frmLoadSearch.grdLoadSearch.Columns("Carrier_Name")
                            
                                'RKL DEJ 2016-09-02 Check SAGE Contract/BSO to see if we should use the PO on the BSO (START)
'                                rsLoad("FrontPurchase_Order").Value = frmLoadSearch.grdLoadSearch.Columns("Purchase_Order")
                                'RKL DEJ 2019-01-31 - Valero - There are no MAS PO that data comes from LMe
                                rsLoad("FrontPurchase_Order").Value = Trim(ConvertToString(frmLoadSearch.grdLoadSearch.Columns("Purchase_Order")))
'                                rsLoad("FrontPurchase_Order").Value = GetCustPO(Trim(ConvertToString(frmLoadSearch.grdLoadSearch.Columns("Contract No"))), Trim(ConvertToString(frmLoadSearch.grdLoadSearch.Columns("Purchase_Order"))))
                                
                                'RKL DEJ 2016-09-02 Check SAGE Contract/BSO to see if we should use the PO on the BSO (STOP)
                                
                            'SGS DEJ 11/05/09 (STOP)
                            
                            rsExistingLoads.Close
                        End If
                    End If
                End If
            End If
        End If
    End If  'SGS DEJ 3/28/11 added to fix the error when no records are selected.
    

    Call ItemClassAdditive      'RKL DEJ 12/18/14

    'RKL DEJ 01/07/15 (START) added get message from LME
    Dim LoadID As String
    Dim LMEMsg As String
    Dim TonsMsg As String
    
    If gbShowLMEMsg = True Then
        LoadID = ConvertToString(rsLoad("FrontMcLeodOrderNumber").Value)
        LMEMsg = GetLMEMessage(LoadID)
        
'        If Abs(ConvertToDouble(txtFrontReqTons.Text) - 30) > 2 Then
'            TonsMsg = "This load is requesting " & Trim(txtFrontReqTons.Text) & " tons."
'
'            If LMEMsg <> Empty Then
'                LMEMsg = LMEMsg & vbCrLf & vbCrLf & TonsMsg
'            Else
'                LMEMsg = TonsMsg
'            End If
'        End If
        
        If LMEMsg <> Empty Then
            MsgBox LMEMsg, vbInformation, "Message from LME"
        End If
    End If
    'RKL DEJ 01/07/15 (STOP) added get message from LME

    gbLastActivityTime = Date + Time
    
    'RKL DEJ 2018-01-05 (START) Get Ship To Address For Valero
    LoadID = ConvertToString(rsLoad("FrontMcLeodOrderNumber").Value)
    Call GetLMEShipToAddr(LoadID)
    'RKL DEJ 2018-01-05 (STOP) Get Ship To Address For Valero
    
    Unload frmLoadSearch
    On Error Resume Next
    cboFrontFormula.SetFocus

    tmrAutoReturnNoActivity.Enabled = True
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSearchFront_Click()" & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Scale Pass 2009"
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = True
    gbLastActivityTime = Date + Time
End Sub


Function ValidateData(sMode As String) As Boolean
    On Error GoTo Error
    
    Dim strLog As String
    
    gbLastActivityTime = Date + Time
    
    ValidateData = False
    
    strLog = "Line: 001"
    If Trim(ConvertToString(rsLoad("InboundOperator").Value)) = "___" Then
    strLog = "Line: 002"
        MsgBox "The Inbound Operator is required.", vbExclamation, "Warning"
        cboInboundOperator.SetFocus
        Exit Function
    End If
    
    strLog = "Line: 003"
    If sMode <> "LBS" Then
    strLog = "Line: 004"
        If Trim(ConvertToString(rsLoad("OutBoundOperator").Value)) = "___" Then
    strLog = "Line: 005"
            MsgBox "The Outbound Operator is required.", vbExclamation, "Warning"
            cboOutboundOperator.SetFocus
            Exit Function
        End If
    End If
    
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    '   Start of Front Validation
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    strLog = "Line: 006"
    If chFrontCdRtn.Value = vbUnchecked Then
    strLog = "Line: 007"
        If ConvertToDouble(rsLoad("FrontTareWt").Value) <= 0 Then
    strLog = "Line: 008"
            MsgBox "The Front Tare Weight is required.", vbExclamation, "Warning"
            Exit Function
        End If
    End If
    
    strLog = "Line: 009"
    If sMode = "LBS" Then
    strLog = "Line: 010"
        If chFrontCdRtn.Value = vbChecked Then
    strLog = "Line: 011"
            If ConvertToDouble(rsLoad("FrontGrossWt").Value) <= 0 Then
    strLog = "Line: 012"
                MsgBox "The Front Gross Weight is required.", vbExclamation, "Warning"
                Exit Function
            End If
        Else
    strLog = "Line: 013"
            'Gross Wt Not Required
        End If
    strLog = "Line: 014"
    Else 'Gross Wt Required
    strLog = "Line: 015"
        If ConvertToDouble(rsLoad("FrontGrossWt").Value) <= 0 Then
    strLog = "Line: 016"
            MsgBox "The Front Gross Weight is required.", vbExclamation, "Warning"
            Exit Function
        End If
    strLog = "Line: 017"
    End If
    
    'Extra precations to ensure the Gross & Net are populated if either of the other is
    strLog = "Line: 018"
    If sMode <> "LBS" Then
    strLog = "Line: 019"
        If ConvertToDouble(rsLoad("FrontGrossWt").Value) = 0 Then
    strLog = "Line: 020"
            If ConvertToDouble(rsLoad("FrontTareWt").Value) = 0 Then
    strLog = "Line: 021"
                'Both Missing - No Sweat?
            Else
    strLog = "Line: 022"
                'Tare Present - Gross Required Too
                MsgBox "The Front Gross Weight is required.", vbExclamation, "Warning"
                Exit Function
            End If
    strLog = "Line: 023"
        Else
    strLog = "Line: 024"
            If ConvertToDouble(rsLoad("FrontTareWt").Value) = 0 Then
    strLog = "Line: 025"
                'Gross Present - Tare Required Too
                MsgBox "The Front Tare Weight is required.", vbExclamation, "Warning"
                Exit Function
            Else
    strLog = "Line: 026"
                'Both Present
            End If
    strLog = "Line: 027"
        End If
    strLog = "Line: 028"
    End If
    
    strLog = "Line: 029"
    If Trim(ConvertToString(rsLoad("FrontProduct").Value)) = Empty Then
    strLog = "Line: 030"
        MsgBox "The Front Product is required.", vbExclamation, "Warning"
        Exit Function
    End If
    
    strLog = "Line: 031"
    If Trim(ConvertToString(rsLoad("TruckNo").Value)) = Empty Then
    strLog = "Line: 032"
        MsgBox "The Truck Number is required.", vbExclamation, "Warning"
        Exit Function
    End If
    
    strLog = "Line: 033"
    If Trim(ConvertToString(rsLoad("FrontBOL").Value)) = Empty Then
    strLog = "Line: 034"
        MsgBox "The Front BOL is required.", vbExclamation, "Warning"
        Exit Function
    End If
    
    strLog = "Line: 035"
    If Trim(ConvertToString(rsLoad("FrontCustomer").Value)) = Empty Then
    strLog = "Line: 036"
        MsgBox "The Front Customer is required.", vbExclamation, "Warning"
        Exit Function
    End If
    
    strLog = "Line: 037"
    If Trim(ConvertToString(rsLoad("FrontTankNbr").Value)) = Empty Then
    strLog = "Line: 038"
        MsgBox "The Front Tank Number is required.", vbExclamation, "Warning"
        txtFrontTank.SetFocus
        Exit Function
    Else
        If IsValidTank(rsLoad("FrontTankNbr").Value) = False Then
            MsgBox "The Front Tank Number is not valid.  Please enter a valid tank first.", vbInformation, "Invalid Front Tank"
            Exit Function
        End If
    End If
    
    strLog = "Line: 039"
    If Trim(ConvertToString(rsLoad("FrontRackNbr").Value)) = Empty Then
    strLog = "Line: 040"
        MsgBox "The Front Rack Number is required.", vbExclamation, "Warning"
        txtFrontRack.SetFocus
        Exit Function
    End If
    
'    If cboFrontFormula.ListCount > 0 And cboFrontFormula.ListIndex = -1 Then
    strLog = "Line: 041"
    If cboFrontFormula.ListCount > 0 And Trim(cboFrontFormula.Text) = "" Then
    strLog = "Line: 042"
        MsgBox "The Front Formula is required.", vbExclamation, "Warning"
        cboFrontFormula.SetFocus
        Exit Function
    End If
    
'    If cboRearFormula.ListCount > 0 And cboRearFormula.ListIndex = -1 Then
    strLog = "Line: 043"
    If cboRearFormula.ListCount > 0 And Trim(cboRearFormula.Text) = "" Then
    strLog = "Line: 044"
        MsgBox "The Rear Formula is required.", vbExclamation, "Warning"
        cboRearFormula.SetFocus
        Exit Function
    End If
    
    strLog = "Line: 045"
    If sMode <> "LBS" Then
    strLog = "Line: 046"
        If chFrontCdRtn.Value = vbChecked Then
    strLog = "Line: 047"
            If ConvertToDouble(txtFrontNet.Text) > 0 Then
    strLog = "Line: 048"
                MsgBox "The Front Weight appears to be a shipment, please uncheck the return checkbox.", vbExclamation, "Warning"
                chFrontCdRtn.SetFocus
                Exit Function
            End If
    strLog = "Line: 049"
        Else
    strLog = "Line: 050"
            If ConvertToDouble(txtFrontNet.Text) < 0 Then
    strLog = "Line: 051"
                MsgBox "The Front Weight appears to be a return, please check the return checkbox.", vbExclamation, "Warning"
                chFrontCdRtn.SetFocus
                Exit Function
            End If
    strLog = "Line: 052"
        End If
    strLog = "Line: 053"
    End If
    strLog = "Line: 054"
    
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    '   End of Front Validation
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    
'*****************************************************************************************************************************

    '*****************************************************************************************************************
    '*****************************************************************************************************************
    '   Start of Rear Validation
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    strLog = "Line: 055"
    If chSplitLoad.Value = vbChecked Then
    strLog = "Line: 056"
        If gbPlantPrefix <> 6 Then 'Woods Cross (Plant 6) has fewer requirements
    strLog = "Line: 057"
            If ConvertToDouble(rsLoad("RearTareWt").Value) <= 0 Then
    strLog = "Line: 058"
                MsgBox "The Rear Tare Weight is required.", vbExclamation, "Warning"
                Exit Function
            End If
    strLog = "Line: 059"
        End If
        
    strLog = "Line: 060"
        If sMode <> "LBS" Then
    strLog = "Line: 061"
            If ConvertToDouble(rsLoad("RearGrossWt").Value) <= 0 Then
    strLog = "Line: 062"
                MsgBox "The Rear Gross Weight is required.", vbExclamation, "Warning"
                Exit Function
            End If
    strLog = "Line: 063"
        End If
    strLog = "Line: 064"
    End If

'    If ConvertToDouble(rsLoad("RearTareWt").Value) > 0 Then
    strLog = "Line: 065"
    If ConvertToDouble(rsLoad("SelectedRearGallons").Value) > 0 Then
        
        'Extra precations to ensure the Gross & Net are populated if either of the other is
    strLog = "Line: 066"
        If sMode <> "LBS" Then
    strLog = "Line: 067"
            If ConvertToDouble(rsLoad("RearGrossWt").Value) = 0 Then
    strLog = "Line: 068"
                If ConvertToDouble(rsLoad("RearTareWt").Value) = 0 Then
    strLog = "Line: 069"
                    'Both Missing - No Sweat?
                Else
    strLog = "Line: 070"
                    'Tare Present - Gross Required Too
                    MsgBox "The Rear Gross Weight is required.", vbExclamation, "Warning"
                    Exit Function
                End If
    strLog = "Line: 071"
            Else
    strLog = "Line: 072"
                If ConvertToDouble(rsLoad("RearTareWt").Value) = 0 Then
    strLog = "Line: 073"
                    'Gross Present - Tare Required Too
                    MsgBox "The Rear Tare Weight is required.", vbExclamation, "Warning"
                    Exit Function
                Else
    strLog = "Line: 074"
                    'Both Present
                End If
    strLog = "Line: 075"
            End If
    strLog = "Line: 076"
        End If
        
    strLog = "Line: 077"
        If Trim(ConvertToString(rsLoad("RearProduct").Value)) = Empty Then
    strLog = "Line: 078"
            MsgBox "The Rear Product is required.", vbExclamation, "Warning"
            Exit Function
        End If
        
    strLog = "Line: 079"
        If Trim(ConvertToString(rsLoad("RearBOL").Value)) = Empty Then
    strLog = "Line: 080"
            MsgBox "The Rear BOL is required.", vbExclamation, "Warning"
            Exit Function
        End If
        
    strLog = "Line: 081"
        If Trim(ConvertToString(rsLoad("RearCustomer").Value)) = Empty Then
    strLog = "Line: 082"
            MsgBox "The Rear Customer is required.", vbExclamation, "Warning"
            Exit Function
        End If
        
    strLog = "Line: 083"
        If Trim(ConvertToString(rsLoad("RearTankNbr").Value)) = Empty Then
    strLog = "Line: 084"
            MsgBox "The Rear Tank Number is required.", vbExclamation, "Warning"
            txtRearTruck.SetFocus
            Exit Function
        End If
        
    strLog = "Line: 085"
        If Trim(ConvertToString(rsLoad("RearRackNbr").Value)) = Empty Then
    strLog = "Line: 086"
            MsgBox "The Rear Rack Number is required.", vbExclamation, "Warning"
            txtRearRack.SetFocus
            Exit Function
        End If
    
    strLog = "Line: 087"
        If sMode <> "LBS" Then
    strLog = "Line: 088"
            If chRearCdRtn.Value = vbChecked Then
    strLog = "Line: 089"
                If ConvertToDouble(txtRearNet.Text) > 0 Then
    strLog = "Line: 090"
                    MsgBox "The Rear Weight appears to be a shipment, please uncheck the return checkbox.", vbExclamation, "Warning"
                    chRearCdRtn.SetFocus
                    Exit Function
                End If
    strLog = "Line: 091"
            Else
    strLog = "Line: 092"
                If ConvertToDouble(txtRearNet.Text) < 0 Then
    strLog = "Line: 093"
                    MsgBox "The Rear Weight appears to be a return, please check the return checkbox.", vbExclamation, "Warning"
                    chRearCdRtn.SetFocus
                    Exit Function
                End If
    strLog = "Line: 094"
            End If
    strLog = "Line: 095"
        End If
    strLog = "Line: 096"
    End If
    strLog = "Line: 097"
    
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    '   End of Rear Validation
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    'RKL DEJ 2017-12-18 (Start)
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    'Check the Variance
    If IsValidVariance() = False Then
        'Variance failed
        Exit Function
    End If
    
    'Check to see if the tank and product have been selected
    If gbForceVarianceCheck = True Then
        If ConvertToDouble(txtSrceQtyOne.Text) <> 0 Then
            If Trim(txtSrcTankOne.Text) = Empty Then
                'No Tank was entered
                MsgBox "Weight has been entered for the 1st Src Tons, but the Src Tank is missing.  Please enter a tank first.", vbInformation, "Missing 1st Src Tank"
                Exit Function
            Else
                If IsValidTank(txtSrcTankOne.Text) = False Then
                    MsgBox "The 1st Src Tank is not valid.  Please enter a valid tank first.", vbInformation, "Invalid 1st Src Tank"
                    Exit Function
                End If
            End If
            If Trim(txtSrcProductIDOne.Text) = Empty Then
                'No Tank was entered
                MsgBox "Weight has been entered for the 1st Src Tons, but the Src Product is missing.  Please enter a tank first.", vbInformation, "Missing 1st Src Tank"
                Exit Function
            End If
        End If
    
        If ConvertToDouble(txtSrceQtyTwo.Text) <> 0 Then
            If Trim(txtSrcTankTwo.Text) = Empty Then
                'No Tank was entered
                MsgBox "Weight has been entered for the 2nd Src Tons, but the Src Tank is missing.  Please enter a tank first.", vbInformation, "Missing 2nd Src Tank"
                Exit Function
            Else
                If IsValidTank(txtSrcTankTwo.Text) = False Then
                    MsgBox "The 2nd Src Tank is not valid.  Please enter a valid tank first.", vbInformation, "Invalid 2nd Src Tank"
                    Exit Function
                End If
            End If
            If Trim(txtSrcProductIDTwo.Text) = Empty Then
                'No Tank was entered
                MsgBox "Weight has been entered for the 2nd Src Tons, but the Src Product is missing.  Please enter a product first.", vbInformation, "Missing 2nd Src Product"
                Exit Function
            End If
        End If
    
        If ConvertToDouble(txtSrceQtyThree.Text) <> 0 Then
            If Trim(txtSrcTankThree.Text) = Empty Then
                'No Tank was entered
                MsgBox "Weight has been entered for the 3rd Src Tons, but the Src Tank is missing.  Please enter a tank first.", vbInformation, "Missing 3rd Src Tank"
                Exit Function
            Else
                If IsValidTank(txtSrcTankThree.Text) = False Then
                    MsgBox "The 3rd Src Tank is not valid.  Please enter a valid tank first.", vbInformation, "Invalid 3rd Src Tank"
                    Exit Function
                End If
            End If
            If Trim(txtSrcProductIDThree.Text) = Empty Then
                'No Tank was entered
                MsgBox "Weight has been entered for the 3rd Src Tons, but the Src Product is missing.  Please enter a product first.", vbInformation, "Missing 3rd Src Product"
                Exit Function
            End If
        End If
        
    End If
    
    
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    'RKL DEJ 2017-12-18 (Stop)
    '*****************************************************************************************************************
    '*****************************************************************************************************************
    
    'If we made it this far then the data is valid
    ValidateData = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".ValidateData() as Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & _
    "Error at trace point: " & strLog, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Function


Sub UnloadfrmLoadSearch()
    On Error Resume Next
    Dim frm As Form
    
    gbLastActivityTime = Date + Time
    
    For Each frm In Forms
'        Debug.Print frm.Name & vbTab & frm.Caption '& vbCrLf
        If frm.Name = frmLoadSearch.Name Then
            Unload frm
        End If
    Next

    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Private Sub cmdPrintBOL_Click()
    
    gbLastActivityTime = Date + Time
    
    'RKL DEJ 1/22/14 (START)
    If gbHideOverShip = False Then  'RKL DEJ 6/12/14 Kevin said to add hide flag
        If IsValidShipTons = False Then
            gbLastActivityTime = Date + Time
            Exit Sub
        End If
    End If
    'RKL DEJ 1/22/14 (STOP)
    
    Call UpdateRSFromObjs
    
    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
        gbWhseID = ConvertToString(rsLoad("FrontFacilityID").Value)
    End If
    
    'RKL DEJ 2018-01-09 Added Save
    If rsLoadExt.State = 1 Then
        If UpdateRS(rsLoadExt) = False Then
            MsgBox "There was an error saving the data (Extension).  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    
    If ValidateData("BOL") = False Then
        Exit Sub
    End If
    
    If rsLoad.State = 1 Then
        lcLoadsTableKey = rsLoad("LoadsTableKey").Value 'rkl dej 2016-06-30 Added
        LoadsTableKey = rsLoad("LoadsTableKey").Value   'rkl dej 2016-06-30 Added
        rsLoad.Close            'Closed because it has a lock on the record... the record cannot be updated from the BOL screen
    End If
    
    DoEvents
    
    tmrAutoReturnNoActivity.Enabled = False
    frmBOLPreview.Show vbModal
    
    tmrAutoReturnNoActivity.Enabled = True
    
    Call Form_Load
    Call chDriverOn_Click
    
    lcLoadsTableKey = 0
    
    'Load the form, and check to see if the Load has printed and is open
    If rsLoad.State = 1 Then
        If Not rsLoad.EOF And Not rsLoad.BOF Then
            If IsNull(rsLoad("LoadIsOpen").Value) = True Then Exit Sub
            
            If rsLoad("LoadIsOpen").Value = 2 Then
                Unload Me
            End If
        End If
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Function GetLoad() As Boolean
    On Error GoTo Error
    Dim SQL As String
    
    gbLastActivityTime = Date + Time
    
'    SQL = "Select * From Loads Where LoadsTableKey = " & gbLoadsTableKey  'LoadIsOpen = 1"
    
    SQL = "Select "
    SQL = SQL & "LoadsTableKey , MASLoadsTableKey, ManualScaling, PlantNo, TimeIn, TimeOut, InBoundOperator, OutBoundOperator"
    SQL = SQL & ", DriverOn, Laps, SplitLoad, SelectForWeightTargets, FrontTargetGrossWt, FrontTargetNetWt, FrontWtPerGallon"
    SQL = SQL & ", FrontTargetGallons, SelectedFrontGallons, RearTargetGrossWt, RearTargetNetWt, RearWtPerGallon, RearTargetGallons"
    SQL = SQL & ", SelectedRearGallons, TruckNo, FrontFacilityID, FrontMcLeodOrderNumber, FrontHauler, FrontCustomer, FrontProject"
    SQL = SQL & ", FrontContractNbr, FrontReqTons, FrontBOL, FrontCommodity, FrontCreditReturn, FrontFormula, FrontProductClass"
    SQL = SQL & ", FrontProduct, FrontProductID, FrontAdditiveClass, FrontAdditive, FrontAdditiveID, FrontTankNbr, FrontRackNbr"
    SQL = SQL & ", FrontTareWt, FrontGrossWt, FrontNetWt, FrontTons, FrontArrivalDt, FrontDestDt, FrontDestAddress, FrontDestDirections"
    SQL = SQL & ", RearFacilityID, RearMcLeodOrderNumber, RearHauler, RearCustomer, RearProject, RearContractNbr, RearReqTons, RearBOL"
    SQL = SQL & ", RearCommodity, RearCreditReturn, RearFormula, RearProductClass, RearProduct, RearProductID, RearAdditiveClass, RearAdditive"
    SQL = SQL & ", RearAdditiveID, RearTankNbr, RearRackNbr, RearTareWt, RearGrossWt, RearNetWt, RearTons, RearArrivalDt, RearDestDt"
    SQL = SQL & ", RearDestAddress, RearDestDirections, FrontBOLHasPrinted, RearBOLHasPrinted, WtTicketHasPrinted, BSHasPrinted"
    SQL = SQL & ", LoadIsOpen, LoadType, TotalTargetGrossWt, TotalTargetGallons, NetworkIsDown, Trailer1_ID, Trailer2_ID, Carrier"
    SQL = SQL & ", FrontPurchase_Order, RearPurchase_Order, FrontPrintCOA, FrontCOARequired, RearPrintCOA, RearCOARequired, FrontHighRisk"
    SQL = SQL & ", FrontSecurityTapNumber, RearHighRisk, RearSecurityTapNumber, FrontCarrier_ID, FrontCarrier_Name, RearCarrier_ID"
    SQL = SQL & ", RearCarrier_Name, FrontWarrantyChipSeal, RearWarrantyChipSeal "
    SQL = SQL & "  From Loads Where LoadsTableKey = " & gbLoadsTableKey
    
    
    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
        rsLoad.Close
    End If
    
    rsLoad.CursorLocation = adUseClient
    rsLoad.CursorType = adOpenDynamic
'    rsLoad.Properties("Update Criteria").Value = adCriteriaAllCols
    
'    Set rsLoad = gbScaleConn.Execute(SQL)
    rsLoad.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
        
    If rsLoad.State = 1 Then
        GetLoad = True
        
        
        If rsLoad.EOF Or rsLoad.BOF Then
            rsLoad.AddNew
            'Load Defaults
'            rsLoad("TimeIn").Value = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
            rsLoad("DriverOn").Value = 0
            rsLoad("InboundOperator").Value = "___"
            rsLoad("OutboundOperator").Value = "___"
            rsLoad("Laps").Value = 0
            rsLoad("SplitLoad").Value = 0
            rsLoad("SelectForWeightTargets").Value = 0
            rsLoad("FrontTargetGrossWt").Value = 0
            rsLoad("FrontTargetNetWt").Value = 0
            rsLoad("FrontWtPerGallon").Value = 0
            rsLoad("FrontTargetGallons").Value = 0
            rsLoad("SelectedFrontGallons").Value = 0
            rsLoad("RearTargetGrossWt").Value = 0
            rsLoad("RearTargetNetWt").Value = 0
            rsLoad("RearWtPerGallon").Value = 0
            rsLoad("RearTargetGallons").Value = 0
            rsLoad("SelectedRearGallons").Value = 0
            rsLoad("FrontTareWt").Value = 0
            rsLoad("FrontGrossWt").Value = 0
            rsLoad("FrontNetWt").Value = 0
            rsLoad("FrontTons").Value = 0
            rsLoad("RearTareWt").Value = 0
            rsLoad("RearGrossWt").Value = 0
            rsLoad("RearNetWt").Value = 0
            rsLoad("RearTons").Value = 0
            rsLoad("FrontBOLHasPrinted").Value = 0
            rsLoad("RearBOLHasPrinted").Value = 0
            rsLoad("WtTicketHasPrinted").Value = 0
            rsLoad("BSHasPrinted").Value = 0
            rsLoad("LoadIsOpen").Value = 1
            rsLoad("LoadType").Value = "LoadFromLME"
            
            rsLoad("FrontPrintCOA").Value = 1       'RKL DEJ 2018-01-05 Changed from 0 to 1 Always try to print the COA Lock this down.
            rsLoad("FrontCOARequired").Value = 0
            rsLoad("RearPrintCOA").Value = 0
            rsLoad("RearCOARequired").Value = 0
                        
            rsLoad("FrontHighRisk").Value = 0
            rsLoad("RearHighRisk").Value = 0
            
'            rsLoad.UpdateBatch
            If UpdateRS(rsLoad) = False Then
                MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
            End If
            LoadsTableKey = ConvertToDouble(rsLoad("LoadsTableKey").Value)
            
        End If
        
        Call GetLoadExt 'RKL DEJ 2018-01-09 added this mothod
        
        If cboFrontFormula.ListCount > 0 Then
            cboFrontFormula.Enabled = True
            cboFrontFormula.BackColor = &H80000005
        End If
            
        Call BindLoadObjects
        Call GetMASWhseKeyByBSO(Trim(txtFrontContract.Text), Trim(txtFrontProductID.Text))
        If gbCustOwnedProd = True Then
            Call GetMASWhseKey  'RKL DEJ 2017-12-19 Switched to this method for Valero Who has no MAS 500 Contracts.
        End If
        Call CheckCOARqrmnt
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".GetLoad()" & vbCrLf & _
    "The following error occurred while creating / retreaving the Load: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Function

Function GetLoadExt() As Boolean
    On Error GoTo Error
    Dim SQL As String
    
    gbLastActivityTime = Date + Time
    
    SQL = "Select "
    SQL = SQL & "LoadsTableKey, FrontSrcTankNbrOne, FrontSrcTankNbrTwo, FrontSrcTankNbrThree, FrontSrcProductIDOne"
    SQL = SQL & ", FrontSrcProductOne, FrontSrcProductClassOne, FrontSrcProductIDTwo, FrontSrcProductTwo, FrontSrcProductClassTwo"
    SQL = SQL & ", FrontSrcProductIDThree, FrontSrcProductThree, FrontSrcProductClassThree, RearSrcTankNbrOne, RearSrcTankNbrTwo"
    SQL = SQL & ", RearSrcTankNbrThree, RearSrcProductIDOne, RearSrcProductOne, RearSrcProductClassOne, RearSrcProductIDTwo"
    SQL = SQL & ", RearSrcProductTwo, RearSrcProductClassTwo, RearSrcProductIDThree, RearSrcProductThree, RearSrcProductClassThree"
    SQL = SQL & ", FrontSrcQtyOne, FrontSrcQtyTwo, FrontSrcQtyThree, RearSrcQtyOne, RearSrcQtyTwo, RearSrcQtyThree, IsTruckBlend"
    SQL = SQL & ", LMEDestLocationCode, LMEDestAddress1, LMEDestAddress2, LMEDestcity_Name, LMEDestState, LMEDestzip_code"
    SQL = SQL & ", LMEDestCustomer_ID, LMEDestAddrName "
    SQL = SQL & "  From LoadsExt Where LoadsTableKey = " & gbLoadsTableKey
    
    
    If rsLoadExt.State = 1 Then
'        rsLoadExt.UpdateBatch
        If UpdateRS(rsLoadExt) = False Then
            MsgBox "There was an error saving the data (method GetLoadExt() pt1).  If this persists contact your IT support.", vbInformation, "Warning"
        End If
        rsLoadExt.Close
    End If
    
    rsLoadExt.CursorLocation = adUseClient
    rsLoadExt.CursorType = adOpenDynamic
    
    rsLoadExt.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
        
    If rsLoadExt.State = 1 Then
        GetLoadExt = True
        
        If rsLoadExt.EOF Or rsLoadExt.BOF Then
            rsLoadExt.AddNew
            'Load Defaults
            rsLoadExt("LoadsTableKey").Value = gbLoadsTableKey
            
            rsLoadExt("IsTruckBlend").Value = vbUnchecked
            
            If UpdateRS(rsLoadExt) = False Then
                MsgBox "There was an error saving the data method GetLaodExt() pt2).  If this persists contact your IT support.", vbInformation, "Warning"
            End If
                       
        End If
            
        If cboFrontFormula.ListCount > 0 Then
            cboFrontFormula.Enabled = True
            cboFrontFormula.BackColor = &H80000005
        End If
            
        Call BindLoadObjects
        Call GetMASWhseKeyByBSO(Trim(txtFrontContract.Text), Trim(txtFrontProductID.Text))
        If gbCustOwnedProd = True Then
            Call GetMASWhseKey  'RKL DEJ 2017-12-19 Switched to this method for Valero Who has no MAS 500 Contracts.
        End If
        Call CheckCOARqrmnt
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".GetLoadExt()" & vbCrLf & _
    "The following error occurred while creating / retreaving the Load: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Function


Private Sub cmdSearchFront_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdSearchRear_Click()
    Dim rsExistingLoads As New ADODB.Recordset
    Dim SQL As String
    Dim RearWarrantyChipSeal As Long
    
    gbLastActivityTime = Date + Time
    
    gbProceed = False
    frmLoadSearch.Show vbModal
    
    gbLastActivityTime = Date + Time
    
    If gbProceed = True Then
        If Trim(gbPlantPrefix) + "-" + Trim(str(Val(frmLoadSearch.grdLoadSearch.Columns("Order No")))) = Trim(txtFrontBOLNbr.Text) Then
            MsgBox "Rear load can't be the same as the Front load.", vbExclamation, "Load Not Valid"
            Exit Sub
        Else
            SQL = "Select * From Loads Where FrontMcLeodOrderNumber = '" & frmLoadSearch.grdLoadSearch.Columns("Order No") & "' OR RearMcLeodOrderNumber = '" & Trim(str(Val(frmLoadSearch.grdLoadSearch.Columns("Order No")))) & "'"
            rsExistingLoads.CursorLocation = adUseClient
            rsExistingLoads.Open SQL, gbScaleConn, adOpenForwardOnly, adLockReadOnly
            If rsExistingLoads.RecordCount > 0 Then
                MsgBox "Selected load already being scaled.", vbExclamation, "Load Invalid"
            Else
                txtRearFacilityID.Text = Trim(frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                Call UpdateRSFromObj(txtRearFacilityID)
        '        txtRearTruck.Text = frmLoadSearch.grdLoadSearch.Columns("Truck No")
                txtRearHauler.Text = frmLoadSearch.grdLoadSearch.Columns("owner_name")
                Call UpdateRSFromObj(txtRearHauler)
                txtRearCust.Text = frmLoadSearch.grdLoadSearch.Columns("Customer Name")
                Call UpdateRSFromObj(txtRearCust)
                txtRearDestAddress.Text = frmLoadSearch.grdLoadSearch.Columns("Dest Address")
                Call UpdateRSFromObj(txtRearDestAddress)
                txtRearContract.Text = frmLoadSearch.grdLoadSearch.Columns("Contract No")
                
'RKL DEJ 2017-07-31 (Start)
rsLoad("RearWarrantyChipSeal").Value = GetMASWarrantyChipSealFlg(Trim(txtFrontContract.Text))
'RKL DEJ 2017-07-31 (Stop)

'RKL DEJ 5/30/14 changed some logic with the Enforce COA
If IsCertRequired(txtRearContract.Text, txtRearFacilityID.Text, frmLoadSearch.grdLoadSearch.Columns("base_product_number")) = True Then
'If IsCertRequired(txtRearContract.Text, txtRearFacilityID.Text, frmLoadSearch.grdLoadSearch.Columns("base_product_number")) = True And gbEnforceCOA = True Then
    chPrntCOARear.Value = vbChecked

    'RKL DEJ 5/30/14 (START)
    'Always allow the COA printing but only force the COA when the Enforce COA flag is set
    'Enforce by not allowing the user to unselect the Print COA check box
    If gbEnforceCOA = True Then
        chPrntCOARear.Enabled = False
    Else
        chPrntCOARear.Enabled = True
    End If
    'RKL DEJ 5/30/14 (STOP)
    
    rsLoad("RearPrintCOA").Value = 1
    rsLoad("RearCOARequired").Value = 1
Else
    chPrntCOARear.Value = vbUnchecked
    chPrntCOARear.Enabled = True
    
    rsLoad("RearPrintCOA").Value = 0
    rsLoad("RearCOARequired").Value = 0
End If
                
If IsHighRiskBSO(txtRearContract.Text, txtRearFacilityID.Text, frmLoadSearch.grdLoadSearch.Columns("base_product_number")) = True Then
    chRearHighRisk.Value = vbChecked
    rsLoad("FrontHighRisk").Value = vbChecked
Else
    chRearHighRisk.Value = vbUnchecked
    rsLoad("RearHighRisk").Value = vbUnchecked
End If

                Call UpdateRSFromObj(txtRearContract)
                txtRearReqTons.Text = ConvertToDouble(frmLoadSearch.grdLoadSearch.Columns("Tons"))
                Call UpdateRSFromObj(txtRearReqTons)
                txtRearBOLNbr.Text = Trim(gbPlantPrefix) + "-" + Trim(str(Val(frmLoadSearch.grdLoadSearch.Columns("Order No"))))
                Call UpdateRSFromObj(txtRearBOLNbr)
                txtRearCommodity.Text = frmLoadSearch.grdLoadSearch.Columns("Commodity")
                Call UpdateRSFromObj(txtRearCommodity)
                'Because of 'Change' logic elsewhere, the Additive needs to be set before the Product
                txtRearAdditiveID.Text = frmLoadSearch.grdLoadSearch.Columns("additive_product_number")
                Call UpdateRSFromObj(txtRearAdditiveID)
                txtRearAdditive.Text = frmLoadSearch.grdLoadSearch.Columns("Additive")
                Call UpdateRSFromObj(txtRearAdditive)
                txtRearProductID.Text = frmLoadSearch.grdLoadSearch.Columns("base_product_number")
                Call UpdateRSFromObj(txtRearProductID)
                txtRearProduct.Text = frmLoadSearch.grdLoadSearch.Columns("Product")
                Call UpdateRSFromObj(txtRearProduct)
                txtRearTank.Text = frmLoadSearch.grdLoadSearch.Columns("tank_no")
                Call UpdateRSFromObj(txtRearTank)
                txtRearArrivalDt.Text = frmLoadSearch.grdLoadSearch.Columns("Load Date/Time")
                Call UpdateRSFromObj(txtRearArrivalDt)
                txtRearDestDt.Text = frmLoadSearch.grdLoadSearch.Columns("dest_sched_arrival")
                Call UpdateRSFromObj(txtRearDestDt)
                txtRearDestDs.Text = frmLoadSearch.grdLoadSearch.Columns("dest_directions")
                Call UpdateRSFromObj(txtRearDestDs)
                txtRearDestDs.Text = Trim(frmLoadSearch.grdLoadSearch.Columns("dest_directions"))
                Call UpdateRSFromObj(txtRearDestDs)
                
                rsLoad("RearMcLeodOrderNumber").Value = frmLoadSearch.grdLoadSearch.Columns("Order No")
                rsLoad("RearProject").Value = frmLoadSearch.grdLoadSearch.Columns("destination_name")
                rsLoad("RearProductClass").Value = GetItemClassID(frmLoadSearch.grdLoadSearch.Columns("base_product_number"), frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                rsLoad("RearAdditiveClass").Value = GetItemClassID(frmLoadSearch.grdLoadSearch.Columns("additive_product_number"), frmLoadSearch.grdLoadSearch.Columns("facility_id"))
                
                'SGS DEJ 11/05/09 (START)
'                rsLoad("Trailer1_ID").Value = frmLoadSearch.grdLoadSearch.Columns("Trailer1_ID")
'                rsLoad("Trailer2_ID").Value = frmLoadSearch.grdLoadSearch.Columns("Trailer2_ID")
'                rsLoad("Carrier").Value = Empty 'frmLoadSearch.grdLoadSearch.Columns("?????")
                rsLoad("RearCarrier_ID").Value = frmLoadSearch.grdLoadSearch.Columns("Carrier_ID")
                rsLoad("RearCarrier_Name").Value = frmLoadSearch.grdLoadSearch.Columns("Carrier_Name")
                
                'RKL DEJ 2016-09-02 Check SAGE Contract/BSO to see if we should use the PO on the BSO (START)
'                rsLoad("RearPurchase_Order").Value = frmLoadSearch.grdLoadSearch.Columns("Purchase_Order")
                'RKL DEJ 2019-01-31 - Valero - There are no MAS 500 contracts - the data comes from LME
                rsLoad("RearPurchase_Order").Value = Trim(ConvertToString(frmLoadSearch.grdLoadSearch.Columns("Purchase_Order")))
'                rsLoad("RearPurchase_Order").Value = GetCustPO(Trim(ConvertToString(frmLoadSearch.grdLoadSearch.Columns("Contract No"))), Trim(ConvertToString(frmLoadSearch.grdLoadSearch.Columns("Purchase_Order"))))
                'RKL DEJ 2016-09-02 Check SAGE Contract/BSO to see if we should use the PO on the BSO (STOP)
                'SGS DEJ 11/05/09 (STOP)
                
'                On Error Resume Next
'                cboRearFormula.SetFocus
            End If
            rsExistingLoads.Close
        End If
    End If
    
    Call ItemClassAdditive      'RKL DEJ 12/18/14
    
    'RKL DEJ 01/07/15 (START) added get message from LME
    Dim LoadID As String
    Dim LMEMsg As String
    Dim TonsMsg As String
    
    If gbShowLMEMsg = True Then
        LoadID = ConvertToString(rsLoad("RearMcLeodOrderNumber").Value)
        LMEMsg = GetLMEMessage(LoadID)
        
'        If Abs(ConvertToDouble(txtRearReqTons.Text) - 30) > 2 Then
'            TonsMsg = "This load is requesting " & Trim(txtRearReqTons.Text) & " tons."
'
'            If LMEMsg <> Empty Then
'                LMEMsg = LMEMsg & vbCrLf & vbCrLf & TonsMsg
'            Else
'                LMEMsg = TonsMsg
'            End If
'        End If
        
        If LMEMsg <> Empty Then
            MsgBox LMEMsg, vbInformation, "Message from LME"
        End If
    End If
    'RKL DEJ 01/07/15 (STOP) added get message from LME
    
    gbLastActivityTime = Date + Time
    
    Unload frmLoadSearch
    On Error Resume Next
    cboRearFormula.SetFocus

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdSearchRear_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

'RKL DEJ 2017-12-15 Added the method cmdSrcProdOne_Click
Private Sub cmdSrcProdOne_Click()
    On Error GoTo Error
    
    tmrAutoReturnNoActivity.Enabled = False
    gbLastActivityTime = Date + Time
    
    frmProductSearch.ItemID = ConvertToString(rsLoadExt("FrontSrcProductIDOne").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoadExt("FrontSrcProductOne").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoadExt("FrontSrcProductClassOne").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    rsLoadExt("FrontSrcProductIDOne").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoadExt("FrontSrcProductOne").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoadExt("FrontSrcProductClassOne").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
'    If chSplitLoad.Value = vbUnchecked Then
'        Call ResetSplitLoadData
'    End If
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSrcProdOne_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
End Sub

'RKL DEJ 2017-12-15 Added the method cmdSrcProdThree_Click
Private Sub cmdSrcProdThree_Click()
    On Error GoTo Error
    
    tmrAutoReturnNoActivity.Enabled = False
    gbLastActivityTime = Date + Time
    
    frmProductSearch.ItemID = ConvertToString(rsLoadExt("FrontSrcProductIDThree").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoadExt("FrontSrcProductThree").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoadExt("FrontSrcProductClassThree").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    rsLoadExt("FrontSrcProductIDThree").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoadExt("FrontSrcProductThree").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoadExt("FrontSrcProductClassThree").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
'    If chSplitLoad.Value = vbUnchecked Then
'        Call ResetSplitLoadData
'    End If
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSrcProdThree_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True

End Sub

'RKL DEJ 2017-12-15 Added the method cmdSrcProdTwo_Click
Private Sub cmdSrcProdTwo_Click()
    On Error GoTo Error
    
    tmrAutoReturnNoActivity.Enabled = False
    gbLastActivityTime = Date + Time
    
    frmProductSearch.ItemID = ConvertToString(rsLoadExt("FrontSrcProductIDTwo").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoadExt("FrontSrcProductTwo").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoadExt("FrontSrcProductClassTwo").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    rsLoadExt("FrontSrcProductIDTwo").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoadExt("FrontSrcProductTwo").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoadExt("FrontSrcProductClassTwo").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
'    If chSplitLoad.Value = vbUnchecked Then
'        Call ResetSplitLoadData
'    End If
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSrcProdTwo_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True

End Sub

Private Sub cmdTareFront_Click()
    
    gbLastActivityTime = Date + Time
    
    Dim YesNo As Integer
        
    'RKL DEJ 2017-11-06 (START)
    txtReadingScaleIndicator1.BackColor = &H8000000F
    txtReadingScaleIndicator2.BackColor = &H8000000F
    txtReadingScaleIndicator3.BackColor = &H8000000F
    'RKL DEJ 2017-11-06 (STOP)
    
    If Val(txtFrontTare.Text) > 0 Then
        gbResponse = MsgBox("Front Tare Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    Else
        If chFrontCdRtn.Value = vbChecked And Val(txtFrontGross.Text = 0) Then
            MsgBox "When processing a Credit Return, Gross Weight required prior to capturing Tare Weight.", vbExclamation + vbOKOnly, "Problems Capturing Weights"
            Exit Sub
        End If
    End If
    txtFrontTare.Text = Val(txtScale.Text)
    Call UpdateRSFromObj(txtFrontTare)
    txtScale.Text = 0
    Call UpdateRSFromObj(txtScale)
    If chFrontCdRtn.Value = vbChecked Then
        txtOutboundTime.Text = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
        Call UpdateRSFromObj(txtOutboundTime)
    Else
        txtInboundTime.Text = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
        Call UpdateRSFromObj(txtInboundTime)
    End If
    rsLoad("ManualScaling").Value = chManualScale
    RecalculateFrontLoadSpecs
    
    'RKL DEJ 12/17/14 (START)
    'RKL DEJ 2016-06-02 Kevin asked to have this question be after we capture the weight.
    If gbDriverOn = True Then
        'Make sure the user has selected Yes or No
        If optnDriverOnYes.Value = False And optnDriverOnNo.Value = False Then
'            MsgBox "Driver On is required before getting the tare weight.", vbInformation, "Driver On?"
            YesNo = MsgBox("Was the Truck Driver on their truck?", vbYesNo, "Driver On?")
            If YesNo = vbYes Then
                optnDriverOnYes.Value = True
            End If
            
            If YesNo = vbNo Then
                optnDriverOnNo.Value = True
            End If
            gbLastActivityTime = Date + Time
'            Exit Sub
        End If
    End If
    'RKL DEJ 12/17/14 (STOP)
        
    txtScale.SetFocus
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdTareFront_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdTareRear_Click()
    gbLastActivityTime = Date + Time
    
    Dim YesNo As Integer
    
    'RKL DEJ 2017-11-06 (START)
    txtReadingScaleIndicator1.BackColor = &H8000000F
    txtReadingScaleIndicator2.BackColor = &H8000000F
    txtReadingScaleIndicator3.BackColor = &H8000000F
    'RKL DEJ 2017-11-06 (STOP)
    
    If Val(txtRearTare.Text) > 0 Then
        gbResponse = MsgBox("Rear Tare Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    Else
        If chRearCdRtn.Value = vbChecked And Val(txtRearGross.Text = 0) Then
            MsgBox "When processing a Credit Return, Gross Weight required prior to capturing Tare Weight.", vbExclamation + vbOKOnly, "Problems Capturing Weights"
            Exit Sub
        End If
    End If
    
    gbLastActivityTime = Date + Time
    
    txtRearTare.Text = Val(txtScale.Text)
    Call UpdateRSFromObj(txtRearTare)
    RecalculateRearLoadSpecs
    txtScale.Text = 0
    Call UpdateRSFromObj(txtScale)
    rsLoad("ManualScaling").Value = chManualScale
    
    'RKL DEJ 12/17/14 (START)
    'RKL DEJ 2016-06-02 Kevin asked to have this question be after we capture the weight.
    If gbDriverOn = True Then
        'Make sure the user has selected Yes or No
        If optnDriverOnYes.Value = False And optnDriverOnNo.Value = False Then
'            MsgBox "Driver On is required before getting the tare weight.", vbInformation, "Driver On?"
            YesNo = MsgBox("Was the Truck Driver on their truck?", vbYesNo, "Driver On?")
            If YesNo = vbYes Then
                optnDriverOnYes.Value = True
            End If
            
            If YesNo = vbNo Then
                optnDriverOnNo.Value = True
            End If
            gbLastActivityTime = Date + Time
'            Exit Sub
        End If
    End If
    'RKL DEJ 12/17/14 (STOP)
    
    txtScale.SetFocus

    gbLastActivityTime = Date + Time
End Sub


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo Error_Handler
    
    gbLastActivityTime = Date + Time
    
    Select Case KeyCode
           Case vbKeyF2
                cmdTareFront_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
           Case vbKeyF4
                cmdGrossFront_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
           Case vbKeyF5
                cmdTareRear_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
           Case vbKeyF7
                cmdGrossRear_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
            Case vbKeyEscape
                cmdSave_Click
    End Select
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error_Handler:
    Resume Next

    gbLastActivityTime = Date + Time
End Sub


Private Sub Form_Load()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Me.KeyPreview = True
    
    Me.Top = 500
    Me.Left = 500
    
'    Set SGSScale = New SGSScaleComm.clsScaleComm
    Set SGSScale = CreateObject("SGSScaleComm.clsScaleComm")
'    SGSScale.ParentAppTitle = App.Title    'RKL SGS 10/4/13 Replaced with connection string
    'RKL DEJ
'    Set SGSScale.conn = gbScaleConn
    SGSScale.conn = gbScaleConnStr

    Set rsLoad = New ADODB.Recordset
    Set rsLoadExt = New ADODB.Recordset

    If LoadDropDowns() = False Then
        MsgBox "Could not obtain list box info from Access DB.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
        Exit Sub
    End If
    
    'Misc Assignments
'    frameRearLoad.Visible = False
    'Set Form Defaults
    
    If gbLocalMode = True Then
        'Don't bother checking
    Else
        If HaveLostMASConnection = True Then
            gbLocalMode = True
        End If
    End If
    
    If gbLocalMode Then
        Me.Caption = "Load Processing (Server Disconnected) - Valero"
    Else
        Me.Caption = "Load Processing - Valero"
    End If

    txtFrontTargetGrossWt.Locked = True
    txtFrontTargetGrossWt.TabStop = False
    txtFrontTargetGrossWt.BackColor = &H8000000F
'    txtRearTargetGrossWt.Enabled = False
    txtRearTargetGrossWt.Locked = True
    txtRearTargetGrossWt.TabStop = False
    txtRearTargetGrossWt.BackColor = &H8000000F
'    txtRearTargetWtGal.Enabled = False
    txtRearTargetWtGal.Locked = True
    txtRearTargetWtGal.TabStop = False
    txtRearTargetWtGal.BackColor = &H8000000F
    cboFrontFormula.Enabled = False
    cboFrontFormula.BackColor = &H8000000F
    cboRearFormula.Enabled = False
    cboRearFormula.BackColor = &H8000000F
    If gbMANUALSCALING = True Then
        chManualScale = vbChecked
        txtScale.Locked = False
        txtScale.ForeColor = &H80000008
        txtScale.BackColor = &H80000005
        txtScale.TabStop = True
        
        'RKL DEJ 2017-11-06 START
        txtReadingScaleIndicator1.Visible = False
        txtReadingScaleIndicator2.Visible = False
        txtReadingScaleIndicator3.Visible = False
        'RKL DEJ 2017-11-06 STOP
                
        SGSScale.ClosePort
    Else
        chManualScale = vbUnchecked
        txtScale.Locked = True
        txtScale.ForeColor = &HFFFFFF
        txtScale.BackColor = &H0&
        txtScale.TabStop = False
        
        'RKL DEJ 2017-11-06 START
        txtReadingScaleIndicator1.Visible = True
        txtReadingScaleIndicator2.Visible = True
        txtReadingScaleIndicator3.Visible = True
        
        txtReadingScaleIndicator1.BackColor = &H8000000F
        txtReadingScaleIndicator2.BackColor = &H8000000F
        txtReadingScaleIndicator3.BackColor = &H8000000F
        'RKL DEJ 2017-11-06 STOP
        
        SGSScale.OpenPort
    End If
    bCalculatingOverallWt = False
    bOverallTargetGallons = False
    
    'RKL DEJ 2017-12-16 (Start)
    ShowHideRearFields
    'RKL DEJ 2017-12-16 (Stop)
    
    If GetLoad() = False Then
        MsgBox "Could not obtain or create the open load.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
    End If
    
    Call chIsTruckBlend_Click   '2018-01-04 added for Valero
    
    If LCase(gbMODE) = "debug" Then
        cmdRSValues.Visible = True
    Else
        cmdRSValues.Visible = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'    cmdSave_Click
'    txtScale.SetFocus

    Call UpdateRSFromObjs
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
        rsLoad.Close
    End If
    
    'RKL DEJ 2018-01-09 Added Save
    If rsLoadExt.State = 1 Then
        If UpdateRS(rsLoadExt) = False Then
            MsgBox "There was an error saving the data (Extension).  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    
    Call UnBindLoadObjects
    
    Set rsLoad = Nothing
    Set rsLoadExt = Nothing
'
'    If gbScaleConn.State = 1 Then gbScaleConn.Close
'    Set gbScaleConn = Nothing
    
'    If gbMASConn.State = 1 Then gbMASConn.Close
'    Set gbMASConn = Nothing
    
    SGSScale.ClosePort
    Set SGSScale = Nothing

    gbLastActivityTime = Date + Time
End Sub

Public Property Let LoadsTableKey(KeyVlu As Long)
    gbLoadsTableKey = KeyVlu

    gbLastActivityTime = Date + Time
End Property


Public Property Get LoadsTableKey() As Long
    LoadsTableKey = gbLoadsTableKey

    gbLastActivityTime = Date + Time
End Property


Sub BindLoadObjects()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If rsLoad.State <> 1 Then Exit Sub
    bInitialLoad = True
    
    optnDriverOnNo.Value = False    'RKL DEJ 12/17/14
    optnDriverOnYes.Value = False   'RKL DEJ 12/17/14
        
    Set cboInboundOperator.DataSource = rsLoad
    Set txtInboundTime.DataSource = rsLoad
    Set cboOutboundOperator.DataSource = rsLoad
    Set txtOutboundTime.DataSource = rsLoad
    Set chDriverOn.DataSource = rsLoad
    Set txtLaps.DataSource = rsLoad
    
    Set chSplitLoad.DataSource = rsLoad
    Set txtTotalTargetGrossWt.DataSource = rsLoad
    Set txtTotalTargetGal.DataSource = rsLoad
    Set txtFrontTargetGrossWt.DataSource = rsLoad
    Set txtFrontTargetWtGal.DataSource = rsLoad
    Set txtFrontTargetNetWt.DataSource = rsLoad
    Set txtFrontTargetGal.DataSource = rsLoad
    Set txtFrontGal.DataSource = rsLoad
    
    Set chWtTarget.DataSource = rsLoad
    Set txtRearTargetGrossWt.DataSource = rsLoad
    Set txtRearTargetWtGal.DataSource = rsLoad
    Set txtRearTargetNetWt.DataSource = rsLoad
    Set txtRearTargetGal.DataSource = rsLoad
    Set txtRearGal.DataSource = rsLoad
    
    Set txtFrontFacilityID.DataSource = rsLoad
    Set txtFrontTruck.DataSource = rsLoad
    Set txtFrontHauler.DataSource = rsLoad
    Set txtFrontCust.DataSource = rsLoad
    Set txtFrontDestAddress.DataSource = rsLoad
    Set txtFrontContract.DataSource = rsLoad
    
    Set chPrntCOAFront.DataSource = rsLoad
    Set chPrntCOARear.DataSource = rsLoad
        
    Set chFrontHighRisk.DataSource = rsLoad
    Set txtFrontSecTapeNbr.DataSource = rsLoad
    Set chRearHighRisk.DataSource = rsLoad
    Set txtRearSecTapeNbr.DataSource = rsLoad

    Set txtFrontReqTons.DataSource = rsLoad
    Set txtFrontBOLNbr.DataSource = rsLoad
    Set chPrntCOAFront.DataSource = rsLoad
    Set txtFrontCommodity.DataSource = rsLoad
    Set chFrontCdRtn.DataSource = rsLoad
    Set cboFrontFormula.DataSource = rsLoad
    Set txtFrontAdditiveID.DataSource = rsLoad
    Set txtFrontAdditive.DataSource = rsLoad
    Set txtFrontProductID.DataSource = rsLoad
    Set txtFrontProduct.DataSource = rsLoad
    Set txtFrontTank.DataSource = rsLoad
    Set txtFrontRack.DataSource = rsLoad
    Set txtFrontTare.DataSource = rsLoad
    Set txtFrontGross.DataSource = rsLoad
    Set txtFrontNet.DataSource = rsLoad
    Set txtFrontTons.DataSource = rsLoad
    Set txtFrontArrivalDt.DataSource = rsLoad
    Set txtFrontDestDt.DataSource = rsLoad
    Set txtFrontDestDs.DataSource = rsLoad
    
'Set chFrontHighRisk.DataSource = rsLoad
'Set txtFrontSecTapeNbr.DataSource = rsLoad
'Set chRearHighRisk.DataSource = rsLoad
'Set txtRearSecTapeNbr.DataSource = rsLoad
    
    Set txtRearFacilityID.DataSource = rsLoad
    Set txtRearTruck.DataSource = rsLoad
    Set txtRearHauler.DataSource = rsLoad
    Set txtRearCust.DataSource = rsLoad
    Set txtRearDestAddress.DataSource = rsLoad
    Set txtRearContract.DataSource = rsLoad
    Set txtRearReqTons.DataSource = rsLoad
    Set txtRearBOLNbr.DataSource = rsLoad
    Set chPrntCOARear.DataSource = rsLoad
    Set txtRearCommodity.DataSource = rsLoad
    Set chRearCdRtn.DataSource = rsLoad
    Set cboRearFormula.DataSource = rsLoad
    Set txtRearAdditiveID.DataSource = rsLoad
    Set txtRearAdditive.DataSource = rsLoad
    Set txtRearProductID.DataSource = rsLoad
    Set txtRearProduct.DataSource = rsLoad
    Set txtRearTank.DataSource = rsLoad
    Set txtRearRack.DataSource = rsLoad
    Set txtRearTare.DataSource = rsLoad
    Set txtRearGross.DataSource = rsLoad
    Set txtRearNet.DataSource = rsLoad
    Set txtRearTons.DataSource = rsLoad
    Set txtRearArrivalDt.DataSource = rsLoad
    Set txtRearDestDt.DataSource = rsLoad
    Set txtRearDestDs.DataSource = rsLoad
    
'*******************************************************************
'*******************************************************************
'RKL DEJ 2017-12-18 (START)
'*******************************************************************
'*******************************************************************
    Set lblSrcProductOne.DataSource = rsLoadExt
    Set lblSrcProductTwo.DataSource = rsLoadExt
    Set lblSrcProductThree.DataSource = rsLoadExt

    Set txtSrcTankOne.DataSource = rsLoadExt
    Set txtSrcProductIDOne.DataSource = rsLoadExt
    Set txtSrceQtyOne.DataSource = rsLoadExt

    Set txtSrcTankTwo.DataSource = rsLoadExt
    Set txtSrcProductIDTwo.DataSource = rsLoadExt
    Set txtSrceQtyTwo.DataSource = rsLoadExt

    Set txtSrcTankThree.DataSource = rsLoadExt
    Set txtSrcProductIDThree.DataSource = rsLoadExt
    Set txtSrceQtyThree.DataSource = rsLoadExt

    Set chIsTruckBlend.DataSource = rsLoadExt

'*******************************************************************
'*******************************************************************
'RKL DEJ 2017-12-18 (STOP)
'*******************************************************************
'*******************************************************************
    
    Call ItemClassAdditive      'RKL DEJ 12/18/14
        
    bInitialLoad = False
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".BindLoadObjects()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub


Sub UnBindLoadObjects()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Set cboInboundOperator.DataSource = Nothing
    Set txtInboundTime.DataSource = Nothing
    Set cboOutboundOperator.DataSource = Nothing
    Set txtOutboundTime.DataSource = Nothing
    Set chDriverOn.DataSource = Nothing
    Set txtLaps.DataSource = Nothing
    
    Set chSplitLoad.DataSource = Nothing
    Set txtTotalTargetGrossWt.DataSource = Nothing
    Set txtTotalTargetGal.DataSource = Nothing
    Set txtFrontTargetGrossWt.DataSource = Nothing
    Set txtFrontTargetWtGal.DataSource = Nothing
    Set txtFrontTargetNetWt.DataSource = Nothing
    Set txtFrontTargetGal.DataSource = Nothing
    Set txtFrontGal.DataSource = Nothing
    
    Set chWtTarget.DataSource = Nothing
    Set txtRearTargetGrossWt.DataSource = Nothing
    Set txtRearTargetWtGal.DataSource = Nothing
    Set txtRearTargetNetWt.DataSource = Nothing
    Set txtRearTargetGal.DataSource = Nothing
    Set txtRearGal.DataSource = Nothing
    
    Set txtFrontFacilityID.DataSource = Nothing
    Set txtFrontTruck.DataSource = Nothing
    Set txtFrontHauler.DataSource = Nothing
    Set txtFrontCust.DataSource = Nothing
    Set txtFrontDestAddress.DataSource = Nothing
    Set txtFrontContract.DataSource = Nothing
    Set txtFrontReqTons.DataSource = Nothing
    Set txtFrontBOLNbr.DataSource = Nothing
    Set chPrntCOAFront.DataSource = Nothing
    Set txtFrontCommodity.DataSource = Nothing
    Set chFrontCdRtn.DataSource = Nothing
    Set cboFrontFormula.DataSource = Nothing
    Set txtFrontProductID.DataSource = Nothing
    Set txtFrontProduct.DataSource = Nothing
    Set txtFrontAdditiveID.DataSource = Nothing
    Set txtFrontAdditive.DataSource = Nothing
    Set txtFrontTank.DataSource = Nothing
    Set txtFrontRack.DataSource = Nothing
    Set txtFrontTare.DataSource = Nothing
    Set txtFrontGross.DataSource = Nothing
    Set txtFrontNet.DataSource = Nothing
    Set txtFrontTons.DataSource = Nothing
    Set txtFrontArrivalDt.DataSource = Nothing
    Set txtFrontDestDt.DataSource = Nothing
    
    
Set chFrontHighRisk.DataSource = Nothing
Set txtFrontSecTapeNbr.DataSource = Nothing
Set chRearHighRisk.DataSource = Nothing
Set txtRearSecTapeNbr.DataSource = Nothing
    
    Set txtRearFacilityID.DataSource = Nothing
    Set txtRearTruck.DataSource = Nothing
    Set txtRearHauler.DataSource = Nothing
    Set txtRearCust.DataSource = Nothing
    Set txtRearDestAddress.DataSource = Nothing
    Set txtRearContract.DataSource = Nothing
    Set txtRearReqTons.DataSource = Nothing
    Set txtRearBOLNbr.DataSource = Nothing
    Set chPrntCOARear.DataSource = Nothing
    Set txtRearCommodity.DataSource = Nothing
    Set chRearCdRtn.DataSource = Nothing
    Set cboRearFormula.DataSource = Nothing
    Set txtRearProductID.DataSource = Nothing
    Set txtRearProduct.DataSource = Nothing
    Set txtRearAdditiveID.DataSource = Nothing
    Set txtRearAdditive.DataSource = Nothing
    Set txtRearTank.DataSource = Nothing
    Set txtRearRack.DataSource = Nothing
    Set txtRearTare.DataSource = Nothing
    Set txtRearGross.DataSource = Nothing
    Set txtRearNet.DataSource = Nothing
    Set txtRearTons.DataSource = Nothing
    Set txtRearArrivalDt.DataSource = Nothing
    Set txtRearDestDt.DataSource = Nothing
    Set txtRearDestDs.DataSource = Nothing
    
'*******************************************************************
'*******************************************************************
'RKL DEJ 2017-12-18 (START)
'*******************************************************************
'*******************************************************************
    Set lblSrcProductOne.DataSource = Nothing
    Set lblSrcProductTwo.DataSource = Nothing
    Set lblSrcProductThree.DataSource = Nothing
    
    Set txtSrcTankOne.DataSource = Nothing
    Set txtSrcProductIDOne.DataSource = Nothing
    Set txtSrceQtyOne.DataSource = Nothing
    
    Set txtSrcTankTwo.DataSource = Nothing
    Set txtSrcProductIDTwo.DataSource = Nothing
    Set txtSrceQtyTwo.DataSource = Nothing
    
    Set txtSrcTankThree.DataSource = Nothing
    Set txtSrcProductIDThree.DataSource = Nothing
    Set txtSrceQtyThree.DataSource = Nothing
    
    Set chIsTruckBlend.DataSource = Nothing
    
'*******************************************************************
'*******************************************************************
'RKL DEJ 2017-12-18 (STOP)
'*******************************************************************
'*******************************************************************
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".BindLoadObjects()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Function LoadDropDowns()
    On Error GoTo Error
    Dim SQL As String
    Dim rsOperator As New ADODB.Recordset
    
    gbLastActivityTime = Date + Time
    
    SQL = "Select * From Operator Where ActiveFg = 1 Order By OperatorID ASC"
    
    rsOperator.CursorLocation = adUseClient
    rsOperator.Open SQL, gbScaleConn, adOpenForwardOnly, adLockReadOnly
    
    rsOperator.MoveFirst
    cboInboundOperator.Clear
    cboOutboundOperator.Clear
    Do While Not rsOperator.EOF
        cboInboundOperator.AddItem rsOperator("OperatorID")
        cboOutboundOperator.AddItem rsOperator("OperatorID")
        rsOperator.MoveNext
    Loop
    
    rsOperator.Close
    Set rsOperator = Nothing
    
    LoadDropDowns = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".LoadDropDowns()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Function

Private Sub Frame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame6_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub frameFrontSpecs_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub frameRearLoad_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub frameRearLoadOverlay_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub frameRearSpecs_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label10_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label11_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label12_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label13_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label14_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label15_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label16_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label17_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label18_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label19_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label20_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label21_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label22_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label23_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label24_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label25_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label26_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label27_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label28_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label29_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label30_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label31_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label32_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label33_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label34_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label35_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label36_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label37_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label38_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label39_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label40_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label41_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label42_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label43_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label44_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label45_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label46_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label47_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label48_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label49_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label50_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label51_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label52_Click()
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label52_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label53_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label54_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label57_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label6_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label8_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label9_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub


Private Sub optnDriverOnNo_Click()
    gbLastActivityTime = Date + Time
    
    If optnDriverOnNo.Value = True Then
        chDriverOn.Value = vbUnchecked
    End If
    
    gbLastActivityTime = Date + Time

End Sub

Private Sub optnDriverOnNo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub optnDriverOnYes_Click()
    gbLastActivityTime = Date + Time
    
    If optnDriverOnYes.Value = True Then
        chDriverOn.Value = vbChecked
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub optnDriverOnYes_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub SGSScale_Error(ErrMsg As String)
'    txtScaleMsg.Text = ErrMsg & vbTab & txtScaleMsg.Text
'    txtScaleMsg.Visible = True
'    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
'    tmrScale.Enabled = True
'
'    frmCommError.DisplayError ErrMsg
    
'    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_ScaleWeight(Weight As Double, Sign As SGSScaleComm.Polarity, UnitOfMeasure As SGSScaleComm.WeightType, GN As SGSScaleComm.GrossOrNet)
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If Sign = Negative Then
        If Weight < 0 Then
            txtScale.Text = Weight
        Else
            txtScale.Text = Weight * -1
        End If
    ElseIf Sign = Positive Then
        If Weight > 0 Then
            txtScale.Text = Weight
        Else
            txtScale.Text = Weight * -1
        End If
    Else
        txtScale.Text = Weight
    End If
    
    '**************************************************************************************
    '**************************************************************************************
    'RKL DEJ 2017-11-07 (START)
    'Attempt to allow continuious flow adding a pause
    '**************************************************************************************
    '**************************************************************************************
    VerifyRead
    
'    Static Cntr As Integer
'
'    Cntr = Cntr + 1
'
'    If Cntr > 100 Then
'        Cntr = 0
'        SGSScale.ClosePort
'        DoEvents
'        SGSScale.OpenPort
'    End If
    '**************************************************************************************
    '**************************************************************************************
    'RKL DEJ 2017-11-07 (STOP)
    '**************************************************************************************
    '**************************************************************************************
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    txtScaleMsg.Text = Me.Name & ".SGSScale_ScaleWeight(): Error: " & Err.Number & " " & Err.Description & vbTab & txtScaleMsg.Text
    txtScaleMsg.Visible = True
    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_StatusChange(StatusStr As String)
'    txtScaleMsg.Text = StatusStr & vbTab & txtScaleMsg.Text
'    txtScaleMsg.Visible = True
'    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
'    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_Warning(WarningMsg As String)
'    txtScaleMsg.Text = WarningMsg & vbTab & txtScaleMsg.Text
'    txtScaleMsg.Visible = True
'    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
'    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub tmpScaleIndicator_Timer()
    On Error Resume Next
    
    txtReadingScaleIndicator1.BackColor = &H8000000F
    txtReadingScaleIndicator2.BackColor = &H8000000F
    txtReadingScaleIndicator3.BackColor = &H8000000F
    
    If chManualScale.Value = vbUnchecked Then
        If SGSScale.PortIsOpen = False Then
            SGSScale.ClosePort
            SGSScale.OpenPort
        End If
    End If
    
    Err.Clear
End Sub

Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
            
'            MsgBox Me.Name & vbTab & "Add Code to close Locked records and return to Open Loads"
            
            Call cmdSave_Click
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False
End Sub

Private Sub tmrReqTons_Timer()
    On Error Resume Next
    
    Static TxtColor01 As Variant
    Static BgColor01 As Variant
    Static bIni As Boolean
    
    If bIni = False Then
        bIni = True
        TxtColor01 = &HC0FFFF
        BgColor01 = &HFF&
    End If
    
    'Front
    If txtFrontReqTons.ForeColor = &HFF& Or txtFrontReqTons.BackColor = &HFF& Then
        With txtFrontReqTons
'            BgColor01 = .ForeColor
'            TxtColor01 = .BackColor
        
            .ForeColor = TxtColor01
            .BackColor = BgColor01
        End With
    End If
    
    'Rear
    If txtRearReqTons.ForeColor = &HFF& Or txtRearReqTons.BackColor = &HFF& Then
        With txtRearReqTons
'            BgColor01 = .ForeColor
'            TxtColor01 = .BackColor
        
            .ForeColor = TxtColor01
            .BackColor = BgColor01
        End With
    End If
    
    If TxtColor01 = &HC0FFFF Then
        TxtColor01 = &HFF&
        BgColor01 = &HC0FFFF
    Else
        TxtColor01 = &HC0FFFF
        BgColor01 = &HFF&
    End If
    
    Err.Clear
End Sub

Private Sub tmrScale_Timer()
    tmrScale.Enabled = False
    txtScaleMsg.Visible = False
    txtScaleMsg.Text = Empty
    
    'SGS DEJ 6/15/2011 On line doc says if a Frame or some other error happen you have to close and then re-open the port
    SGSScale.ClosePort
    If chManualScale.Value <> vbChecked Then
        SGSScale.OpenPort
    End If
    
    gbLastActivityTime = Date + Time
End Sub


Private Sub txtFrontAdditive_Change()

    gbLastActivityTime = Date + Time

'    If txtFrontAdditiveID.Text = "" Then
'        cboFrontFormula.Clear
'        cboFrontFormula.ListIndex = -1
'        cboFrontFormula.Enabled = False
'        cboFrontFormula.BackColor = &H8000000F
'    Else
'        If LoadFormulas("Front", txtFrontAdditiveID.Text) = False Then
'            MsgBox "Front Formula(s) not found or error reading database.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
'            Exit Sub
'        Else
'            cboFrontFormula.Enabled = True
'            cboFrontFormula.BackColor = &H80000005
'            If cboFrontFormula.ListCount = 1 Then
'                cboFrontFormula.ListIndex = 0
'            End If
'        End If
'    End If
End Sub

Private Sub txtFrontAdditive_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontAdditive_Validate(Cancel As Boolean)
    Dim cnt As Integer
    Dim ItemDesc As String
    Dim ItemID As String
    Dim ItemClassID As String
    
    gbLastActivityTime = Date + Time
    
    ItemDesc = txtFrontAdditive.Text
    ItemID = txtFrontAdditiveID.Text
    ItemClassID = ConvertToString(rsLoad("FrontAdditiveClass").Value)
    
    If Trim(txtFrontAdditive.Text) = Empty Then
        txtFrontAdditive.Text = Empty
Call UpdateRSFromObj(txtFrontAdditive)
        rsLoad("FrontAdditiveID").Value = Empty
        rsLoad("FrontAdditiveClass").Value = Empty
        
        Exit Sub
    End If
    
    If IsValidItem(ItemDesc, gbPlantPrefix, cnt, ItemID, ItemClassID) = False Then
        If ItemDesc = "No Antistrip in Product" Then
            'It's cool... Don't sweat it.
        Else
            MsgBox "This is not a valid Product.", vbExclamation, "Product Not Valid"
            Cancel = True
            Exit Sub
        End If
    Else
        If cnt > 1 Then
            MsgBox "There are more than one product with that description.  Please select one.", vbInformation, "More Than One Product"
            
            frmProductSearch.ItemDescFilter = txtFrontAdditive.Text

            Call cmdFrntSrchAdtv_Click
            
            If ConvertToString(rsLoad("FrontAdditiveID").Value) = Empty Then
                Cancel = True
            End If
            
            If chSplitLoad.Value = vbUnchecked Then
                Call ResetSplitLoadData
            End If
            
            Exit Sub
        End If
        
        txtFrontAdditive.Text = ItemDesc
Call UpdateRSFromObj(txtFrontAdditive)
        rsLoad("FrontAdditiveID").Value = ItemID
        rsLoad("FrontAdditiveClass").Value = ItemClassID
        
        If chSplitLoad.Value = vbUnchecked Then
            Call ResetSplitLoadData
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtFrontAdditiveID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontArrivalDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontBOLNbr_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontCommodity_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontContract_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontCust_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontDestAddress_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontDestDs_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontDestDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontFacilityID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontGal_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
   ActiveControl.Tag = ActiveControl.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontGal_LostFocus()
    
    gbLastActivityTime = Date + Time
    
    If txtFrontGal.Text <> txtFrontGal.Tag Then
        'If chSplitLoad = vbUnchecked And chWtTarget = vbUnchecked Then  (commented 6/19/09 per Suzanne to make Splits work like Fulls)
        If chWtTarget = vbUnchecked Then
            If Val(txtTotalTargetGal) = 0 Then
'                MsgBox "Overall Target Gross Weight required for calculations", vbOKOnly + vbExclamation, "Missing Information"
                If bCalculatingOverallWt Then
                    'Don't sweat it...
                Else
                    'gbResponse = MsgBox("Overall Target Gross Weight required for calculations unless using 'Selected Gallons' to calculate Gross Weights.  Continue calculating Gross Weights?", vbQuestion + vbYesNo, "Missing Information")
                    'If gbResponse = vbYes Then
                        bCalculatingOverallWt = True
                        gbResponse = MsgBox("Is this the Overall Target Gross Gallons?", vbQuestion + vbYesNo, "Overall vs Front Gallons Only")
                        If gbResponse = vbYes Then
                            bOverallTargetGallons = True
                        End If
                    'Else
                    '    txtFrontGal.Text = 0
                    '    Call UpdateRSFromObj(txtFrontGal)
                    '    Exit Sub
                    'End If
                End If
            End If
            'txtRearGal.Text = Val(txtTotalTargetGal) - Val(txtFrontGal.Text)
            CalculateRemainingWeights ("Front")
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub CalculateRemainingWeights(sLastModified As String)
    Dim dPct As Double
    
    gbLastActivityTime = Date + Time
    
    If sLastModified = "Rear" Then 'Rear Selected Gallons modified last, therefore is the Base Calc
        'Rear
        If Val(txtRearTargetWtGal.Text) = 0 Then
            txtRearTargetWtGal.Text = txtFrontTargetWtGal.Text
            Call UpdateRSFromObj(txtRearTargetWtGal)
        End If
        txtRearTargetGal.Text = txtRearGal.Text
        Call UpdateRSFromObj(txtRearTargetGal)
        txtRearTargetNetWt.Text = Round(Val(txtRearGal.Text) * Val(txtRearTargetWtGal.Text), 0)
        Call UpdateRSFromObj(txtRearTargetNetWt)
        txtRearTargetGrossWt.Text = Val(txtRearTargetNetWt.Text) + Val(txtRearTare.Text)
        Call UpdateRSFromObj(txtRearTargetGrossWt)
        'Front
        If bCalculatingOverallWt Then
            If bOverallTargetGallons Then
                'Don't override the Total Target Gross Wt
            Else
                txtTotalTargetGrossWt.Text = Val(txtFrontTargetGrossWt.Text) + Val(txtRearTargetGrossWt.Text)
                Call UpdateRSFromObj(txtTotalTargetGrossWt)
            End If
        End If
        txtFrontTargetGrossWt.Text = Val(txtTotalTargetGrossWt.Text) - Val(txtRearTargetGrossWt.Text)
        Call UpdateRSFromObj(txtFrontTargetGrossWt)
        txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text)
        Call UpdateRSFromObj(txtFrontTargetNetWt)
        If Val(txtFrontTargetWtGal.Text) = 0 Then
            If bCalculatingOverallWt Then
                'Don't recalculate the Front Gallons
            Else
                txtFrontGal.Text = 0
                Call UpdateRSFromObj(txtFrontGal)
            End If
        Else
            txtFrontGal.Text = Round(Val(txtFrontTargetNetWt.Text) / Val(txtFrontTargetWtGal.Text))
            Call UpdateRSFromObj(txtFrontGal)
        End If
        If Val(txtFrontTargetWtGal.Text) = 0 Then
            If bCalculatingOverallWt Then
                'Don't recalculate the Front Gallons
            Else
                txtFrontGal.Text = 0
            End If
        Else
            txtFrontGal.Text = Round(Val(txtFrontTargetNetWt.Text) / Val(txtFrontTargetWtGal.Text))
        End If
        Call UpdateRSFromObj(txtFrontGal)
        txtFrontTargetGal.Text = txtFrontGal.Text
        Call UpdateRSFromObj(txtFrontTargetGal)
    Else 'Front Selected Gallons modified last, therefore is the Base Calc
        'Front
        txtFrontTargetGal.Text = txtFrontGal.Text
        Call UpdateRSFromObj(txtFrontTargetGal)
        txtFrontTargetNetWt.Text = Round(Val(txtFrontGal.Text) * Val(txtFrontTargetWtGal.Text), 0)
        Call UpdateRSFromObj(txtFrontTargetNetWt)
        txtFrontTargetGrossWt.Text = Val(txtFrontTargetNetWt.Text) + Val(txtFrontTare.Text)
        Call UpdateRSFromObj(txtFrontTargetGrossWt)
        'Rear
        If Val(txtRearTargetWtGal.Text) = 0 Then
            txtRearTargetWtGal.Text = txtFrontTargetWtGal.Text
            Call UpdateRSFromObj(txtRearTargetWtGal)
        End If
        If bCalculatingOverallWt Then
            txtTotalTargetGrossWt.Text = Val(txtFrontTargetGrossWt.Text) + Val(txtRearTargetGrossWt.Text)
            Call UpdateRSFromObj(txtTotalTargetGrossWt)
        End If
        txtRearTargetGrossWt.Text = Val(txtTotalTargetGrossWt.Text) - Val(txtFrontTargetGrossWt.Text)
        Call UpdateRSFromObj(txtRearTargetGrossWt)
        txtRearTargetNetWt.Text = Val(txtRearTargetGrossWt.Text) - Val(txtRearTare.Text)
        Call UpdateRSFromObj(txtRearTargetNetWt)
        If Val(txtRearTargetWtGal.Text) = 0 Then
            txtRearGal.Text = 0
            Call UpdateRSFromObj(txtRearGal)
        Else
            txtRearGal.Text = Round(Val(txtRearTargetNetWt.Text) / Val(txtRearTargetWtGal.Text))
            Call UpdateRSFromObj(txtRearGal)
        End If
        txtRearTargetGal.Text = txtRearGal.Text
        Call UpdateRSFromObj(txtRearTargetGal)
    End If
    
'    'Rear
'    txtRearTargetGal.Text = txtRearGal.Text
'    If Val(txtRearTargetWtGal.Text) = 0 Then
'        txtRearTargetWtGal.Text = txtFrontTargetWtGal.Text
'        dPct = Val(txtRearGal.Text) / Val(txtFrontTargetGal.Text)
'        txtRearTargetNetWt.Text = Round(Val(txtFrontTargetNetWt.Text) * dPct, 0)
'        txtRearTargetGrossWt.Text = Val(txtRearTare.Text) + Val(txtRearTargetNetWt.Text)
'    Else
'        txtRearTargetGrossWt.Text = Val(txtRearGal.Text) * Val(txtRearTargetWtGal.Text)
'    End If
'
'    'Front
'    txtFrontTargetGal.Text = txtFrontGal.Text
'    txtFrontTargetNetWt.Text = Val(txtFrontTargetNetWt.Text) - Val(txtRearTargetNetWt.Text)
'    txtFrontTargetGrossWt.Text = Val(txtFrontTare.Text) + Val(txtFrontTargetNetWt.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontGross_Change()
    
    gbLastActivityTime = Date + Time
    
    txtFrontNet.Text = Val(txtFrontGross.Text) - Val(txtFrontTare.Text)
    Call UpdateRSFromObj(txtFrontNet)
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)
    Call UpdateRSFromObj(txtTotalGrossWt)
    txtTotalNetWt.Text = Val(txtFrontNet.Text) + Val(txtRearNet.Text)
    Call UpdateRSFromObj(txtTotalNetWt)
    txtTotalGrossTons.Text = Val(txtFrontTons.Text) + Val(txtRearTons.Text)
    Call UpdateRSFromObj(txtTotalGrossTons)

    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontGross_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontGross_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontHauler_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontNet_Change()
    gbLastActivityTime = Date + Time
    
    If chFrontCdRtn.Value = vbChecked Then
        txtFrontNet.Text = Abs(Val(txtFrontNet.Text)) * -1
        Call UpdateRSFromObj(txtFrontNet)
    End If
    txtFrontTons.Text = txtFrontNet.Text / 2000
    Call UpdateRSFromObj(txtFrontTons)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontNet_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontProduct_Change()
    
    gbLastActivityTime = Date + Time
    
    If txtFrontProductID.Text = "" Then
        cboFrontFormula.Clear
        cboFrontFormula.ListIndex = -1
        cboFrontFormula.Enabled = False
        cboFrontFormula.BackColor = &H8000000F
    Else
        If IsNull(txtFrontAdditiveID.Text) Then txtFrontAdditiveID.Text = "" 'Get rid of Null value
        If LoadFormulas("Front", IIf(txtFrontAdditiveID.Text = "" Or Trim(UCase(txtFrontAdditiveID.Text)) = "NONE", txtFrontProductID.Text, txtFrontAdditiveID.Text)) = False Then
            MsgBox "Front Formula(s) not found or error reading database.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
            Exit Sub
        Else
            If cboFrontFormula.ListCount = 0 Then
                cboFrontFormula.Enabled = False
                cboFrontFormula.BackColor = &H8000000F
            Else
                cboFrontFormula.Enabled = True
                cboFrontFormula.BackColor = &H80000005
                If cboFrontFormula.ListCount = 1 Then
                    cboFrontFormula.ListIndex = 0
                End If
            End If
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontProduct_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontProduct_Validate(Cancel As Boolean)
    Dim cnt As Integer
    Dim ItemDesc As String
    Dim ItemID As String
    Dim ItemClassID As String
    
    gbLastActivityTime = Date + Time
    
    ItemDesc = txtFrontProduct.Text
    ItemID = txtFrontProductID.Text
    ItemClassID = ConvertToString(rsLoad("FrontProductClass").Value)
    
    If Trim(txtFrontProduct.Text) = Empty Then
        txtFrontProduct.Text = Empty
Call UpdateRSFromObj(txtFrontProduct)
        rsLoad("FrontProductID").Value = Empty
        rsLoad("FrontProductClass").Value = Empty
        
        Exit Sub
    End If
    
    If IsValidItem(ItemDesc, gbPlantPrefix, cnt, ItemID, ItemClassID) = False Then
        MsgBox "This is not a valid Product.", vbExclamation, "Product Not Valid"
        Cancel = True
        Exit Sub
    Else
        If cnt > 1 Then
            MsgBox "There are more than one product with that description.  Please select one.", vbInformation, "More Than One Product"
            
            frmProductSearch.ItemDescFilter = txtFrontProduct.Text

            Call cmdFrntSrchPrdct_Click
            
            If ConvertToString(rsLoad("FrontProductID").Value) = Empty Then
                Cancel = True
            End If
            
            If chSplitLoad.Value = vbUnchecked Then
                Call ResetSplitLoadData
            End If
            Exit Sub
        End If
        
        txtFrontProduct.Text = ItemDesc
        Call UpdateRSFromObj(txtFrontProduct)
        rsLoad("FrontProductID").Value = ItemID
        rsLoad("FrontProductClass").Value = ItemClassID
    
        If chSplitLoad.Value = vbUnchecked Then
            Call ResetSplitLoadData
        End If
    End If
    
    
    gbLastActivityTime = Date + Time
End Sub


Private Sub txtFrontProductID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontRack_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontRack_Validate(Cancel As Boolean)
    If chSplitLoad.Value = vbUnchecked And Val(txtRearGal.Text) > 0 Then
'        txtRearRack.Text = txtFrontRack.Text
        rsLoad("RearRackNbr").Value = txtFrontRack.Text
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtFrontReqTons_Change()
    'RKL DEJ 1/22/14 (START)
    Call SetReqTonColorFrnt
    'RKL DEJ 1/22/14 (STOP)
    
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontReqTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTank_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTank_Validate(Cancel As Boolean)
    If chSplitLoad.Value = vbUnchecked And Val(txtRearGal.Text) > 0 Then
'        txtRearTank.Text = txtFrontTank.Text
        rsLoad("RearTankNbr").Value = txtFrontTank.Text
    End If

    gbLastActivityTime = Date + Time
    
    On Error GoTo Error
    
    If Trim(txtFrontTank.Text) = Empty Then Exit Sub
    
    If IsValidTank(txtFrontTank.Text) = False Then
        MsgBox "This tank is not valid.", vbInformation, "Invalid Tank"
        Cancel = True
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".txtFrontTank_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub


Private Sub txtFrontTare_Change()
    
    gbLastActivityTime = Date + Time
    
    txtFrontNet.Text = Val(txtFrontGross.Text) - Val(txtFrontTare.Text)
    Call UpdateRSFromObj(txtFrontNet)
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)
    Call UpdateRSFromObj(txtTotalGrossWt)
    txtTotalNetWt.Text = Val(txtFrontNet.Text) + Val(txtRearNet.Text)
    Call UpdateRSFromObj(txtTotalNetWt)
    txtTotalGrossTons.Text = Val(txtFrontTons.Text) + Val(txtRearTons.Text)
    Call UpdateRSFromObj(txtTotalGrossTons)
    If bCalculatingOverallWt Then
        txtFrontTargetGrossWt.Text = Val(txtFrontTargetGrossWt.Text) + Val(txtFrontTare.Text)
        Call UpdateRSFromObj(txtFrontTargetGrossWt)
        txtTotalTargetGrossWt.Text = Val(txtFrontTargetGrossWt.Text) + Val(txtRearTargetGrossWt.Text)
        Call UpdateRSFromObj(txtTotalTargetGrossWt)
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTare_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTare_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTargetGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTargetGrossWt_GotFocus()
    ActiveControl.SelStart = 0
    ActiveControl.SelLength = Len(ActiveControl.Text)
    ActiveControl.Tag = ActiveControl.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTargetGrossWt_LostFocus()
    
    gbLastActivityTime = Date + Time
    
    If txtFrontTargetGrossWt.Tag <> txtFrontTargetGrossWt.Text Then
        'Front
        txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text)
        Call UpdateRSFromObj(txtFrontTargetNetWt)
        If Val(txtFrontTargetWtGal.Text) > 0 Then
            txtFrontGal.Text = Round(Val(txtFrontTargetNetWt.Text) / Val(txtFrontTargetWtGal.Text), 0)
            Call UpdateRSFromObj(txtFrontGal)
            txtFrontTargetGal.Text = txtFrontGal.Text
            Call UpdateRSFromObj(txtFrontTargetGal)
        End If
        'Rear
        txtRearTargetGrossWt.Text = Val(txtTotalTargetGrossWt.Text) - Val(txtFrontTargetGrossWt.Text)
        Call UpdateRSFromObj(txtRearTargetGrossWt)
        txtRearTargetNetWt.Text = Val(txtRearTargetGrossWt.Text) - Val(txtRearTare.Text)
        Call UpdateRSFromObj(txtRearTargetNetWt)
        If Val(txtRearTargetWtGal.Text) > 0 Then
            txtRearGal.Text = Round(Val(txtRearTargetNetWt.Text) / Val(txtRearTargetWtGal.Text), 0)
            Call UpdateRSFromObj(txtRearGal)
            txtRearTargetGal.Text = txtRearGal.Text
            Call UpdateRSFromObj(txtRearTargetGal)
        End If
        If chSplitLoad = vbUnchecked And chWtTarget = vbUnchecked Then
            txtRearTargetGrossWt.Text = 0
            Call UpdateRSFromObj(txtRearTargetGrossWt)
            txtRearTargetNetWt.Text = 0
            Call UpdateRSFromObj(txtRearTargetNetWt)
            txtRearTargetWtGal.Text = 0
            Call UpdateRSFromObj(txtRearTargetWtGal)
            txtRearTargetGal.Text = 0
            Call UpdateRSFromObj(txtRearTargetGal)
            txtRearGal.Text = 0
            Call UpdateRSFromObj(txtRearGal)
        End If
    End If
'    If txtFrontTargetGrossWt.Tag <> txtFrontTargetGrossWt.Text Then
'        If Val(txtFrontTargetGrossWt) = 0 Then
'            txtFrontTargetNetWt.Text = 0
'        Else
'            If chSplitLoad = vbUnchecked And chWtTarget = vbUnchecked Then
'                txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text) - Val(txtRearTare.Text)
'            Else
'                txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text)
'            End If
'        End If
'    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTargetGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTargetNetWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTargetWtGal_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
   ActiveControl.Tag = ActiveControl.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTargetWtGal_LostFocus()
    gbLastActivityTime = Date + Time
    
    If txtFrontTargetWtGal.Tag <> txtFrontTargetWtGal.Text Then
        RecalculateFrontLoadSpecs
        If chSplitLoad = vbChecked Then
            'Leave the Rear alone
        Else
            txtRearTargetWtGal.Text = txtFrontTargetWtGal.Text
            Call UpdateRSFromObj(txtRearTargetWtGal)
            If bCalculatingOverallWt Then
                txtRearTargetGrossWt.Text = Val(txtRearGal.Text) * Val(txtRearTargetWtGal.Text)
                Call UpdateRSFromObj(txtRearTargetGrossWt)
                txtRearTargetNetWt.Text = Val(txtRearTargetGrossWt.Text) - Val(txtRearTare.Text)
                Call UpdateRSFromObj(txtRearTargetNetWt)
                txtTotalTargetGrossWt.Text = Val(txtFrontTargetGrossWt.Text) + Val(txtRearTargetGrossWt.Text)
                Call UpdateRSFromObj(txtTotalGrossWt)
                txtTotalNetWt.Text = Val(txtFrontNet.Text) + Val(txtRearNet.Text)
                Call UpdateRSFromObj(txtTotalNetWt)
            End If
    
        End If
    End If
'    If txtFrontTargetWtGal.Tag <> txtFrontTargetWtGal.Text Then
'        If Val(txtFrontTargetWtGal.Text) = 0 Then
'            txtFrontTargetGal.Text = 0
'        Else
'            txtFrontTargetGal.Text = Round(txtFrontTargetNetWt.Text / txtFrontTargetWtGal.Text, 0)
'        End If
'        If chSplitLoad = vbChecked Or chWtTarget = vbChecked Then
'            txtFrontGal.Text = txtFrontTargetGal.Text
'        Else
'            txtFrontGal.Text = txtFrontTargetGal.Text
'            txtRearGal.Text = 0
'        End If
'    End If

    gbLastActivityTime = Date + Time
End Sub
Private Sub RecalculateFrontLoadSpecs()
    
    gbLastActivityTime = Date + Time
    
    'If chSplitLoad = vbUnchecked And chWtTarget = vbUnchecked Then (commented 6/19/09 per Suzanne to make Splits work like Fulls)
'    If chWtTarget = vbUnchecked Then
'        txtFrontTargetGrossWt.Text = Val(txtTotalTargetGrossWt.Text) - Val(txtRearTargetGrossWt.Text)
'        txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text) - Val(txtRearTare.Text)
'    Else
'        txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text)
'    End If
    
    If bCalculatingOverallWt Then
        txtTotalTargetGrossWt.Text = Round((Val(txtFrontGal.Text) + Val(txtRearGal.Text)) * Val(txtFrontTargetWtGal.Text), 0) + Val(txtFrontTare.Text) + Val(txtRearTare.Text)
    Else
        If Val(txtTotalTargetGrossWt) = 0 Then
            txtTotalTargetGrossWt.Text = Round(Val(txtFrontGal.Text) * Val(txtFrontTargetWtGal.Text), 0)
        End If
    End If
    
    txtFrontTargetGrossWt.Text = Val(txtTotalTargetGrossWt.Text) - Val(txtRearTargetGrossWt.Text)
    Call UpdateRSFromObj(txtFrontTargetGrossWt)
    txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text)
    Call UpdateRSFromObj(txtFrontTargetNetWt)
    
    'txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text)
    If Val(txtFrontTargetGrossWt.Text) = 0 Then
        txtFrontTargetGrossWt.Text = Val(txtFrontTargetNetWt.Text) * -1
        Call UpdateRSFromObj(txtFrontTargetGrossWt)
    End If
    
    If Val(txtFrontTargetWtGal.Text) = 0 Then
        txtFrontTargetGal.Text = 0
        Call UpdateRSFromObj(txtFrontTargetGal)
    Else
        txtFrontTargetGal.Text = Round(txtFrontTargetNetWt.Text / txtFrontTargetWtGal.Text, 0)
        Call UpdateRSFromObj(txtFrontTargetGal)
    End If
    
    If Val(txtTotalTargetGal.Text) = 0 Then
        txtTotalTargetGal.Text = txtFrontTargetGal.Text 'Establish Total Gallons to work with
        Call UpdateRSFromObj(txtTotalTargetGal)
    End If
    
    'If chSplitLoad = vbChecked Or chWtTarget = vbChecked Then (commented 6/19/09 per Suzanne to make Splits work like Fulls)
    If chWtTarget = vbChecked Then
        txtFrontGal.Text = txtFrontTargetGal.Text
        Call UpdateRSFromObj(txtFrontGal)
    Else
        txtFrontGal.Text = txtFrontTargetGal.Text
        Call UpdateRSFromObj(txtFrontGal)
        'txtRearGal.Text = 0
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub RecalculateRearLoadSpecs()
    
    gbLastActivityTime = Date + Time
    
    txtRearTargetNetWt.Text = Val(txtRearTargetGrossWt.Text) - Val(txtRearTare.Text)
    Call UpdateRSFromObj(txtRearTargetNetWt)
    If Val(txtRearTargetGrossWt.Text) = 0 Then
        txtRearTargetGrossWt.Text = Val(txtRearTargetNetWt.Text) * -1
        Call UpdateRSFromObj(txtRearTargetGrossWt)
    End If
    
'    If Val(txtRearTargetGrossWt.Text) = 0 Then
'        txtRearTargetNetWt.Text = 0
'    Else
'        txtRearTargetNetWt.Text = Val(txtRearTargetGrossWt.Text) - Val(txtRearTare.Text)
'    End If
    If Val(txtRearTargetWtGal.Text) = 0 Then
        txtRearTargetGal.Text = 0
        Call UpdateRSFromObj(txtRearTargetGal)
        txtRearGal.Text = 0
        Call UpdateRSFromObj(txtRearGal)
    Else
        txtRearTargetGal.Text = Round(Val(txtRearTargetNetWt.Text) / Val(txtRearTargetWtGal.Text), 0)
        Call UpdateRSFromObj(txtRearTargetGal)
        txtRearGal.Text = txtRearTargetGal.Text
        Call UpdateRSFromObj(txtRearGal)
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTargetWtGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTons_Change()
    'RKL DEJ 1/22/14 (START)
    SetReqTonColorFrnt
    'RKL DEJ 1/22/14 (START)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTruck_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtInboundTime_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtLaps_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtOutboundTime_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearAdditive_Change()
    gbLastActivityTime = Date + Time

'    If txtRearAdditiveID.Text = "" Then
'        cboRearFormula.Clear
'        cboRearFormula.ListIndex = -1
'        cboRearFormula.Enabled = False
'        cboRearFormula.BackColor = &H8000000F
'    Else
'        If LoadFormulas("Rear", txtFrontAdditiveID.Text) = False Then
'            MsgBox "Rear Formula(s) not found or error reading database.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
'            Exit Sub
'        Else
'            cboRearFormula.Enabled = True
'            cboRearFormula.BackColor = &H80000005
'            If cboRearFormula.ListCount = 1 Then
'                cboRearFormula.ListIndex = 0
'            End If
'        End If
'    End If
End Sub

Private Sub txtRearAdditive_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearAdditive_Validate(Cancel As Boolean)
    Dim cnt As Integer
    Dim ItemDesc As String
    Dim ItemID As String
    Dim ItemClassID As String
    
    gbLastActivityTime = Date + Time
    
    ItemDesc = txtRearAdditive.Text
    ItemID = txtRearAdditiveID.Text
    ItemClassID = ConvertToString(rsLoad("RearAdditiveClass").Value)
    
    If Trim(txtRearAdditive.Text) = Empty Then
        txtRearAdditive.Text = Empty
        Call UpdateRSFromObj(txtRearAdditive)
        rsLoad("RearAdditiveID").Value = Empty
        rsLoad("RearAdditiveClass").Value = Empty
        
        Exit Sub
    End If
    
    If IsValidItem(ItemDesc, gbPlantPrefix, cnt, ItemID, ItemClassID) = False Then
        If ItemDesc = "No Antistrip in Product" Then
            'It's cool... Don't sweat it.
        Else
            MsgBox "This is not a valid Product.", vbExclamation, "Product Not Valid"
            Cancel = True
            Exit Sub
        End If
    Else
        If cnt > 1 Then
            MsgBox "There are more than one product with that description.  Please select one.", vbInformation, "More Than One Product"
            
            frmProductSearch.ItemDescFilter = txtRearAdditive.Text

            Call cmdRearSrchAdtv_Click
            
            If ConvertToString(rsLoad("RearAdditiveID").Value) = Empty Then
                Cancel = True
            End If
            
            Exit Sub
        End If
        
        txtRearAdditive.Text = ItemDesc
        Call UpdateRSFromObj(txtRearAdditive)
        rsLoad("RearAdditiveID").Value = ItemID
        rsLoad("RearAdditiveClass").Value = ItemClassID
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtRearAdditiveID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearArrivalDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearBOLNbr_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearCommodity_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearContract_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearCust_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearDestAddress_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearDestDs_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearDestDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearFacilityID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearGal_Change()
    gbLastActivityTime = Date + Time
    
    If chSplitLoad.Value <> vbChecked And Val(txtRearGal.Text) > 0 And Trim(txtFrontProductID.Text) > "" Then
        'Load 'Rear' with same stuff as 'Front'
        txtRearFacilityID.Text = txtFrontFacilityID.Text
        Call UpdateRSFromObj(txtRearFacilityID)
        txtRearTruck.Text = txtFrontTruck.Text
        Call UpdateRSFromObj(txtRearTruck)
        txtRearHauler.Text = txtFrontHauler.Text
        Call UpdateRSFromObj(txtRearHauler)
        txtRearCust.Text = txtFrontCust.Text
        Call UpdateRSFromObj(txtRearCust)
        txtRearDestAddress.Text = txtFrontDestAddress.Text
        Call UpdateRSFromObj(txtRearDestAddress)
        txtRearContract.Text = txtFrontContract.Text
        Call UpdateRSFromObj(txtRearContract)
        txtRearReqTons.Text = txtFrontReqTons.Text
        Call UpdateRSFromObj(txtRearReqTons)
        txtRearBOLNbr.Text = txtFrontBOLNbr.Text
        Call UpdateRSFromObj(txtRearBOLNbr)
        txtRearCommodity.Text = txtFrontCommodity.Text
        Call UpdateRSFromObj(txtRearCommodity)
        cboRearFormula.Text = cboFrontFormula.Text
        Call UpdateRSFromObj(cboRearFormula)
        'Because of 'Change' logic elsewhere, the Additive needs to be set before the Product
        txtRearAdditiveID.Text = txtFrontAdditiveID.Text
        Call UpdateRSFromObj(txtRearAdditiveID)
        txtRearAdditive.Text = txtFrontAdditive.Text
        Call UpdateRSFromObj(txtRearAdditive)
        txtRearProductID.Text = txtFrontProductID.Text
        Call UpdateRSFromObj(txtRearProductID)
        txtRearProduct.Text = txtFrontProduct.Text
        Call UpdateRSFromObj(txtRearProduct)
        txtRearTank.Text = txtFrontTank.Text
        Call UpdateRSFromObj(txtRearTank)
        txtRearRack.Text = txtFrontRack.Text
        Call UpdateRSFromObj(txtRearRack)
        txtRearArrivalDt.Text = txtFrontArrivalDt.Text
        Call UpdateRSFromObj(txtRearArrivalDt)
        txtRearDestDt.Text = txtFrontDestDt.Text
        Call UpdateRSFromObj(txtRearDestDt)
        txtRearDestDs.Text = txtFrontDestDs.Text
        Call UpdateRSFromObj(txtRearDestDs)
        
        rsLoad("RearMcLeodOrderNumber").Value = rsLoad("FrontMcLeodOrderNumber").Value
        rsLoad("RearProject").Value = rsLoad("FrontProject").Value
        rsLoad("RearProductClass").Value = rsLoad("FrontProductClass").Value
        rsLoad("RearAdditiveClass").Value = rsLoad("FrontAdditiveClass").Value
        
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGal_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
   ActiveControl.Tag = ActiveControl.Text
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearGross_Change()
    gbLastActivityTime = Date + Time
    
    txtRearNet.Text = txtRearGross.Text - txtRearTare.Text
    Call UpdateRSFromObj(txtRearNet)
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)
    Call UpdateRSFromObj(txtTotalGrossWt)
    txtTotalNetWt.Text = Val(txtFrontNet.Text) + Val(txtRearNet.Text)
    Call UpdateRSFromObj(txtTotalNetWt)
    txtTotalGrossTons.Text = Val(txtFrontTons.Text) + Val(txtRearTons.Text)
    Call UpdateRSFromObj(txtTotalGrossTons)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGross_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGross_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearHauler_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearNet_Change()
    gbLastActivityTime = Date + Time
    
    If chRearCdRtn.Value = vbChecked Then
        txtRearNet.Text = Abs(Val(txtRearNet.Text)) * -1
        Call UpdateRSFromObj(txtRearNet)
    End If
    txtRearTons.Text = txtRearNet.Text / 2000
    Call UpdateRSFromObj(txtRearTons)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearNet_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearProduct_Change()
    
    gbLastActivityTime = Date + Time
    
    If txtRearProductID.Text = "" Then
        cboRearFormula.Clear
        cboRearFormula.ListIndex = -1
        cboRearFormula.Enabled = False
        cboRearFormula.BackColor = &H8000000F
    Else
        If IsNull(txtRearAdditiveID.Text) Then txtRearAdditiveID.Text = "" 'Get rid of Null value
        If LoadFormulas("Rear", IIf(txtRearAdditiveID.Text = "" Or Trim(UCase(txtRearAdditiveID.Text)) = "NONE", Trim(txtRearProductID.Text), Trim(txtRearAdditiveID.Text))) = False Then
            MsgBox "Rear Formula(s) not found or error reading database.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
            Exit Sub
        Else
            If cboRearFormula.ListCount = 0 Then
                cboRearFormula.Enabled = False
                cboRearFormula.BackColor = &H8000000F
            Else
                cboRearFormula.Enabled = True
                cboRearFormula.BackColor = &H80000005
                If cboRearFormula.ListCount = 1 Then
                    cboRearFormula.ListIndex = 0
                End If
            End If
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearProduct_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearProduct_Validate(Cancel As Boolean)
    Dim cnt As Integer
    Dim ItemDesc As String
    Dim ItemID As String
    Dim ItemClassID As String
    
    gbLastActivityTime = Date + Time
    
    ItemDesc = txtRearProduct.Text
    ItemID = txtRearProductID.Text
    ItemClassID = ConvertToString(rsLoad("RearProductClass").Value)
    
    If Trim(txtRearProduct.Text) = Empty Then
        txtRearProduct.Text = Empty
        Call UpdateRSFromObj(txtRearProduct)
        rsLoad("RearProductID").Value = Empty
        rsLoad("RearProductClass").Value = Empty
        
        Exit Sub
    End If
    
    If IsValidItem(ItemDesc, gbPlantPrefix, cnt, ItemID, ItemClassID) = False Then
        MsgBox "This is not a valid Product.", vbExclamation, "Product Not Valid"
        Cancel = True
        Exit Sub
    Else
        If cnt > 1 Then
            MsgBox "There are more than one product with that description.  Please select one.", vbInformation, "More Than One Product"
            
            frmProductSearch.ItemDescFilter = txtRearProduct.Text

            Call cmdRearSrchPrdct_Click
            
            If ConvertToString(rsLoad("RearProductID").Value) = Empty Then
                Cancel = True
            End If
            
            Exit Sub
        End If
        
        txtRearProduct.Text = ItemDesc
        Call UpdateRSFromObj(txtRearProduct)
        rsLoad("RearProductID").Value = ItemID
        rsLoad("RearProductClass").Value = ItemClassID
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtRearProductID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearRack_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearReqTons_Change()
    'RKL DEJ 1/22/14 (START)
    Call SetReqTonColorRear
    'RKL DEJ 1/22/14 (STOP)

    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearReqTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTank_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTare_Change()
    gbLastActivityTime = Date + Time
    
    txtRearNet.Text = Val(txtRearGross.Text) - Val(txtRearTare.Text)
    Call UpdateRSFromObj(txtRearNet)
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)
    Call UpdateRSFromObj(txtTotalGrossWt)
    txtTotalNetWt.Text = Val(txtFrontNet.Text) + Val(txtRearNet.Text)
    Call UpdateRSFromObj(txtTotalNetWt)
    txtTotalGrossTons.Text = Val(txtFrontTons.Text) + Val(txtRearTons.Text)
    Call UpdateRSFromObj(txtTotalGrossTons)
    If chSplitLoad.Value <> vbChecked And Val(txtRearTare.Text) > 0 And Trim(txtFrontProductID.Text) > "" Then
        'Load 'Rear' with same stuff as 'Front'
        txtRearFacilityID.Text = txtFrontFacilityID.Text
        Call UpdateRSFromObj(txtRearFacilityID)
        txtRearTruck.Text = txtFrontTruck.Text
        Call UpdateRSFromObj(txtRearTruck)
        txtRearHauler.Text = txtFrontHauler.Text
        Call UpdateRSFromObj(txtRearHauler)
        txtRearCust.Text = txtFrontCust.Text
        Call UpdateRSFromObj(txtRearCust)
        txtRearDestAddress.Text = txtFrontDestAddress.Text
        Call UpdateRSFromObj(txtRearDestAddress)
        txtRearContract.Text = txtFrontContract.Text
        Call UpdateRSFromObj(txtRearContract)
        txtRearReqTons.Text = txtFrontReqTons.Text
        Call UpdateRSFromObj(txtRearReqTons)
        txtRearBOLNbr.Text = txtFrontBOLNbr.Text
        Call UpdateRSFromObj(txtRearBOLNbr)
        txtRearCommodity.Text = txtFrontCommodity.Text
        Call UpdateRSFromObj(txtRearCommodity)
        cboRearFormula.Text = cboFrontFormula.Text
        Call UpdateRSFromObj(cboRearFormula)
        'Because of 'Change' logic elsewhere, the Additive needs to be set before the Product
        txtRearAdditiveID.Text = txtFrontAdditiveID.Text
        Call UpdateRSFromObj(txtRearAdditiveID)
        txtRearAdditive.Text = txtFrontAdditive.Text
        Call UpdateRSFromObj(txtRearAdditive)
        txtRearProductID.Text = txtFrontProductID.Text
        Call UpdateRSFromObj(txtRearProductID)
        txtRearProduct.Text = txtFrontProduct.Text
        Call UpdateRSFromObj(txtRearProduct)
        txtRearTank.Text = txtFrontTank.Text
        Call UpdateRSFromObj(txtRearTank)
        txtRearRack.Text = txtFrontRack.Text
        Call UpdateRSFromObj(txtRearRack)
        txtRearArrivalDt.Text = txtFrontArrivalDt.Text
        Call UpdateRSFromObj(txtRearArrivalDt)
        txtRearDestDt.Text = txtFrontDestDt.Text
        Call UpdateRSFromObj(txtRearDestDt)
        txtRearDestDs.Text = txtFrontDestDs.Text
        Call UpdateRSFromObj(txtRearDestDs)
        
        rsLoad("RearMcLeodOrderNumber").Value = rsLoad("FrontMcLeodOrderNumber").Value
        rsLoad("RearProject").Value = rsLoad("FrontProject").Value
        rsLoad("RearProductClass").Value = rsLoad("FrontProductClass").Value
        rsLoad("RearAdditiveClass").Value = rsLoad("FrontAdditiveClass").Value
        
    End If
    
    gbLastActivityTime = Date + Time
    
    If bCalculatingOverallWt Then
        txtRearTargetGrossWt.Text = Val(txtRearTargetGrossWt.Text) + Val(txtRearTare.Text)
        Call UpdateRSFromObj(txtRearTargetGrossWt)
        txtTotalTargetGrossWt.Text = Val(txtFrontTargetGrossWt.Text) + Val(txtRearTargetGrossWt.Text)
        Call UpdateRSFromObj(txtTotalTargetGrossWt)
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTare_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtRearTare_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTargetGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTargetGrossWt_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
   ActiveControl.Tag = ActiveControl.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTargetGrossWt_LostFocus()
    
    gbLastActivityTime = Date + Time
    
    If txtRearTargetGrossWt.Text <> txtRearTargetGrossWt.Tag Then
        'Rear
        txtRearTargetNetWt.Text = Val(txtRearTargetGrossWt.Text) - Val(txtRearTare.Text)
        Call UpdateRSFromObj(txtRearTargetNetWt)
        If Val(txtRearTargetWtGal.Text) > 0 Then
            txtRearGal.Text = Round(Val(txtRearTargetNetWt.Text) / Val(txtRearTargetWtGal.Text), 0)
            Call UpdateRSFromObj(txtRearGal)
            txtRearTargetGal.Text = txtRearGal.Text
            Call UpdateRSFromObj(txtRearTargetGal)
        End If
        'Front
        txtFrontTargetGrossWt.Text = Val(txtTotalTargetGrossWt.Text) - Val(txtRearTargetGrossWt.Text)
        Call UpdateRSFromObj(txtFrontTargetGrossWt)
        txtFrontTargetNetWt.Text = Val(txtFrontTargetGrossWt.Text) - Val(txtFrontTare.Text)
        Call UpdateRSFromObj(txtFrontTargetNetWt)
        If Val(txtFrontTargetWtGal.Text) > 0 Then
            txtFrontGal.Text = Round(Val(txtFrontTargetNetWt.Text) / Val(txtFrontTargetWtGal.Text), 0)
            Call UpdateRSFromObj(txtFrontGal)
            txtFrontTargetGal.Text = txtFrontGal.Text
            Call UpdateRSFromObj(txtFrontTargetGal)
        End If
'        RecalculateRearLoadSpecs
    End If
''    If Val(txtRearTargetGrossWt.Text) = 0 Then
''        txtRearTargetNetWt.Text = 0
''    Else
''        txtRearTargetNetWt.Text = txtRearTargetGrossWt.Text - txtRearTare.Text
''    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGal_LostFocus()
    
    gbLastActivityTime = Date + Time
    
    If txtRearGal.Text <> txtRearGal.Tag Then
    'If chSplitLoad = vbUnchecked And chWtTarget = vbUnchecked Then  (commented 6/19/09 per Suzanne to make Splits work like Fulls)
        If Val(txtTotalTargetGal) = 0 Then
'                MsgBox "Overall Target Gross Weight required for calculations", vbOKOnly + vbExclamation, "Missing Information"
            If bCalculatingOverallWt Then
                'Don't sweat it...
            Else
                'gbResponse = MsgBox("Overall Target Gross Weight required for calculations unless using 'Selected Gallons' to calculate Gross Weights.  Continue calculating Gross Weights?", vbQuestion + vbYesNo, "Missing Information")
                'If gbResponse = vbYes Then
                    bCalculatingOverallWt = True
                'Else
                '    txtRearGal.Text = 0
                '    Call UpdateRSFromObj(txtRearGal)
                '    Exit Sub
                'End If
            End If
        End If
        CalculateRemainingWeights ("Rear")
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTargetGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTargetNetWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTargetWtGal_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
   ActiveControl.Tag = ActiveControl.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTargetWtGal_LostFocus()
    gbLastActivityTime = Date + Time
    
    If txtRearTargetWtGal.Text <> txtRearTargetWtGal.Tag Then
        RecalculateRearLoadSpecs
    End If
'    If Val(txtRearTargetWtGal.Text) = 0 Then
'        txtRearTargetGal.Text = 0
'    Else
'        txtRearTargetGal.Text = Round(txtRearTargetNetWt.Text / txtRearTargetWtGal.Text, 0)
'    End If
''    If chSplitLoad = vbChecked Or chWtTarget = vbChecked Then
''        txtRearGal.Text = txtRearTargetGal.Text
''    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTargetWtGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTons_Change()
    'RKL DEJ 1/22/14 (START)
    Call SetReqTonColorRear
    'RKL DEJ 1/22/14 (STOP)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTruck_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtScale_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Function LoadFormulas(sLoadFlag As String, sProductID As String) As Boolean
    On Error GoTo Error
    Dim SQL As String
    Dim rsFormula As New ADODB.Recordset
    Dim sFormula As String
    
    gbLastActivityTime = Date + Time
    
    SQL = "Select ItemID, FormulaName From vimGetItemFormulaInfo_SGS Where ItemID = '" & Trim(sProductID) & "' AND WhseID = '" & Trim(txtFrontFacilityID.Text) & "' Group By ItemID, FormulaName"
    
    rsFormula.CursorLocation = adUseClient
'    rsFormula.Open SQL, gbScaleConn, adOpenForwardOnly, adLockReadOnly
    rsFormula.Open SQL, gbMASConn, adOpenForwardOnly, adLockReadOnly
    
    If rsFormula.EOF Or rsFormula.BOF Then
        'No Record was selected
        If sLoadFlag = "Front" Then
            cboFrontFormula.Clear
        Else
            cboRearFormula.Clear
        End If
    Else
        rsFormula.MoveFirst
        If sLoadFlag = "Front" Then
            sFormula = cboFrontFormula.Text 'Save off selected formula before loading formula list
            cboFrontFormula.Clear
            Do While Not rsFormula.EOF
                cboFrontFormula.AddItem rsFormula("FormulaName")
                rsFormula.MoveNext
            Loop
            cboFrontFormula.Text = sFormula 'Restore saved off formula
        Else
            sFormula = cboRearFormula.Text 'Save off selected formula before loading formula list
            cboRearFormula.Clear
            Do While Not rsFormula.EOF
                cboRearFormula.AddItem rsFormula("FormulaName")
                rsFormula.MoveNext
            Loop
            cboRearFormula.Text = sFormula 'Restore saved off formula
        End If
    End If
    
    rsFormula.Close
    Set rsFormula = Nothing
    
    LoadFormulas = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".LoadFormulas(" & sLoadFlag & ")" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Function

Private Sub ShowCRViewer()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    frmReportViewer.CRViewer.ReportSource = crxRpt
    frmReportViewer.CRViewer.EnableGroupTree = False
    'frmReportViewer.CRViewer.Zoom (100)
    frmReportViewer.CRViewer.ViewReport
    DoEvents
    frmReportViewer.Show vbModal
    Set crxRpt = Nothing
    Set crxApp = Nothing

    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".ShowCRViewer()" & vbCrLf & "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtScale_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtScaleMsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub





Private Sub txtSrceQtyOne_Validate(Cancel As Boolean)
    
    txtSrceQtyOne.Text = Trim(txtSrceQtyOne.Text)
    
    If txtSrceQtyOne.Text = Empty Then Exit Sub
    
    If IsNumeric(txtSrceQtyOne.Text) = False Then
        Cancel = True
        MsgBox "The Source Tons must be a valid number.", vbExclamation, "Invalid Tons"
    End If
End Sub



Private Sub txtSrceQtyThree_Validate(Cancel As Boolean)
    
    txtSrceQtyThree.Text = Trim(txtSrceQtyThree.Text)
    
    If txtSrceQtyThree.Text = Empty Then Exit Sub
    
    If IsNumeric(txtSrceQtyThree.Text) = False Then
        Cancel = True
        MsgBox "The Source Tons must be a valid number.", vbExclamation, "Invalid Tons"
    End If

End Sub

Private Sub txtSrceQtyTwo_Validate(Cancel As Boolean)
    
    txtSrceQtyTwo.Text = Trim(txtSrceQtyTwo.Text)
    
    If txtSrceQtyTwo.Text = Empty Then Exit Sub
    
    If IsNumeric(txtSrceQtyTwo.Text) = False Then
        Cancel = True
        MsgBox "The Source Tons must be a valid number.", vbExclamation, "Invalid Tons"
    End If

End Sub

Private Sub txtSrcProductIDOne_Validate(Cancel As Boolean)
    Dim cnt As Integer
    Dim ItemDesc As String
    Dim ItemID As String
    Dim ItemClassID As String
    
    gbLastActivityTime = Date + Time
    
    ItemID = txtSrcProductIDOne.Text
    ItemDesc = ConvertToString(rsLoadExt("FrontSrcProductOne").Value)
    ItemClassID = ConvertToString(rsLoadExt("FrontSrcProductClassOne").Value)
    
    If Trim(txtSrcProductIDOne.Text) = Empty Then
        txtSrcProductIDOne.Text = Empty
        Call UpdateRSFromObj(txtSrcProductIDOne)
        rsLoadExt("FrontSrcProductOne").Value = Empty
        rsLoadExt("FrontSrcProductClassOne").Value = Empty
        
        Exit Sub
    End If
    
    If IsValidItemID(ItemDesc, gbPlantPrefix, cnt, ItemID, ItemClassID) = False Then
        MsgBox "This is not a valid Product ID.", vbExclamation, "Product ID Not Valid"
        Cancel = True
        Exit Sub
    Else
        If cnt > 1 Then
            MsgBox "There are more than one Product ID.  Please select one.", vbInformation, "More Than One Product ID"
            
            frmProductSearch.ItemIDFilter = txtSrcProductIDOne.Text

            Call cmdSrcProdOne_Click
            
            If ConvertToString(rsLoadExt("FrontSrcProductIDOne").Value) = Empty Then
                Cancel = True
            End If
            
            Exit Sub
        End If
        
        txtSrcProductIDOne.Text = ItemID
        Call UpdateRSFromObj(txtSrcProductIDOne)
        rsLoadExt("FrontSrcProductOne").Value = ItemDesc
        rsLoadExt("FrontSrcProductClassOne").Value = ItemClassID
    
    End If
    
    
    gbLastActivityTime = Date + Time

End Sub


Private Sub txtSrcProductIDThree_Validate(Cancel As Boolean)
    Dim cnt As Integer
    Dim ItemDesc As String
    Dim ItemID As String
    Dim ItemClassID As String
    
    gbLastActivityTime = Date + Time
    
    ItemID = txtSrcProductIDThree.Text
    ItemDesc = ConvertToString(rsLoadExt("FrontSrcProductThree").Value)
    ItemClassID = ConvertToString(rsLoadExt("FrontSrcProductClassThree").Value)
    
    If Trim(txtSrcProductIDThree.Text) = Empty Then
        txtSrcProductIDThree.Text = Empty
        Call UpdateRSFromObj(txtSrcProductIDThree)
        rsLoadExt("FrontSrcProductThree").Value = Empty
        rsLoadExt("FrontSrcProductClassThree").Value = Empty
        
        Exit Sub
    End If
    
    If IsValidItemID(ItemDesc, gbPlantPrefix, cnt, ItemID, ItemClassID) = False Then
        MsgBox "This is not a valid Product.", vbExclamation, "Product Not Valid"
        Cancel = True
        Exit Sub
    Else
        If cnt > 1 Then
            MsgBox "There is more than one product.  Please select one.", vbInformation, "More Than One Product"
            
            frmProductSearch.ItemIDFilter = txtSrcProductIDThree.Text

            Call cmdSrcProdThree_Click
            
            If ConvertToString(rsLoadExt("FrontSrcProductIDThree").Value) = Empty Then
                Cancel = True
            End If
            
            Exit Sub
        End If
        
        txtSrcProductIDThree.Text = ItemID
        Call UpdateRSFromObj(txtSrcProductIDThree)
        rsLoadExt("FrontSrcProductThree").Value = ItemDesc
        rsLoadExt("FrontSrcProductClassThree").Value = ItemClassID
    
    End If
    
    gbLastActivityTime = Date + Time
    
End Sub

Private Sub txtSrcProductIDTwo_Validate(Cancel As Boolean)
    Dim cnt As Integer
    Dim ItemDesc As String
    Dim ItemID As String
    Dim ItemClassID As String
    
    gbLastActivityTime = Date + Time
    
    ItemID = txtSrcProductIDTwo.Text
    ItemDesc = ConvertToString(rsLoadExt("FrontSrcProductTwo").Value)
    ItemClassID = ConvertToString(rsLoadExt("FrontSrcProductClassTwo").Value)
    
    If Trim(txtSrcProductIDTwo.Text) = Empty Then
        txtSrcProductIDTwo.Text = Empty
        Call UpdateRSFromObj(txtSrcProductIDTwo)
        rsLoadExt("FrontSrcProductTwo").Value = Empty
        rsLoadExt("FrontSrcProductClassTwo").Value = Empty
        
        Exit Sub
    End If
    
    If IsValidItemID(ItemDesc, gbPlantPrefix, cnt, ItemID, ItemClassID) = False Then
        MsgBox "This is not a valid Product.", vbExclamation, "Product Not Valid"
        Cancel = True
        Exit Sub
    Else
        If cnt > 1 Then
            MsgBox "There is more than one product.  Please select one.", vbInformation, "More Than One Product"
            
            frmProductSearch.ItemIDFilter = txtSrcProductIDTwo.Text

            Call cmdSrcProdTwo_Click
            
            If ConvertToString(rsLoadExt("FrontSrcProductIDTwo").Value) = Empty Then
                Cancel = True
            End If
            
            Exit Sub
        End If
        
        txtSrcProductIDTwo.Text = ItemID
        Call UpdateRSFromObj(txtSrcProductIDTwo)
        rsLoadExt("FrontSrcProductTwo").Value = ItemDesc
        rsLoadExt("FrontSrcProductClassTwo").Value = ItemClassID
    
    End If
    
    
    gbLastActivityTime = Date + Time


End Sub


Private Sub txtSrcTankOne_Validate(Cancel As Boolean)
    On Error GoTo Error
    
    If Trim(txtSrcTankOne.Text) = Empty Then Exit Sub
    
    If IsValidTank(txtSrcTankOne.Text) = False Then
        MsgBox "This tank is not valid.", vbInformation, "Invalid Tank"
        Cancel = True
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".txtSrcTankOne_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub





Private Sub txtSrcTankThree_Validate(Cancel As Boolean)
    On Error GoTo Error
    
    If Trim(txtSrcTankThree.Text) = Empty Then Exit Sub
    
    If IsValidTank(txtSrcTankThree.Text) = False Then
        MsgBox "This tank is not valid.", vbInformation, "Invalid Tank"
        Cancel = True
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".txtSrcTankThree_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear

End Sub

Private Sub txtSrcTankTwo_Validate(Cancel As Boolean)
    On Error GoTo Error
    
    If Trim(txtSrcTankTwo.Text) = Empty Then Exit Sub
    
    If IsValidTank(txtSrcTankTwo.Text) = False Then
        MsgBox "This tank is not valid.", vbInformation, "Invalid Tank"
        Cancel = True
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".txtSrcTankTwo_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear

End Sub

Private Sub txtTotalGrossTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalNetWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalTargetGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalTargetGrossWt_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
   ActiveControl.Tag = ActiveControl.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtTotalTargetGrossWt_LostFocus()
    gbLastActivityTime = Date + Time
    
    If txtTotalTargetGrossWt.Tag <> txtTotalTargetGrossWt.Text Then
        'txtFrontTargetGrossWt.Text = txtTotalTargetGrossWt.Text
        RecalculateFrontLoadSpecs
        RecalculateRearLoadSpecs
        'If chSplitLoad = vbUnchecked And chWtTarget = vbUnchecked Then (commented 6/19/09 per Suzanne to make Splits work like Fulls)
'        If chWtTarget = vbUnchecked Then
'            txtRearTargetGrossWt.Text = 0
'            txtRearTargetNetWt.Text = 0
'            txtRearTargetWtGal.Text = 0
'            txtRearTargetGal.Text = 0
'            txtRearGal.Text = 0
'        End If
    End If

    gbLastActivityTime = Date + Time
End Sub


Sub UpdateRSFromObjs()
    On Error Resume Next
    
    Dim obj As Control
    
    gbLastActivityTime = Date + Time
    
'    'RKL DEJ 2018-01-09 - Don't do any action... the Recordset is not updating fields so attempting to fix the issue by skipping this step
'    Exit Sub
'
    
    Me.Refresh
    
    For Each obj In Me.Controls
        Call UpdateRSFromObj(obj)   'RkL DEJ replaced the below code with this call
'        If PrtyIsDataField(obj) = True Then
'            If ConvertToString(obj.DataField) <> Empty Then
'                If obj.DataSource Is rsLoad Then
'                    If PrtyIsText(obj) = True Then
'                        rsLoad(obj.DataField).Value = obj.Text
'                    ElseIf PrtyIsValue(obj) = True Then
''If obj.Name = "chRearHighRisk" Or obj.Name = "chFrontHighRisk" Then
''    Debug.Print obj.Name
''End If
'                        rsLoad(obj.DataField).Value = obj.Value
'
'                    Else
'                        Debug.Print "Name = " & obj.Name & " ...Bad Property"
'                    End If
'
'                End If
'            End If
'        End If
    Next
    
    rsLoad.Fields.Update
        
    gbLastActivityTime = Date + Time
End Sub

Sub UpdateRSFromObj(obj As Object)
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If PrtyIsDataField(obj) = True Then
        If ConvertToString(obj.DataField) <> Empty Then
            If obj.DataSource Is rsLoad Then
                If PrtyIsText(obj) = True Then
                    rsLoad(obj.DataField).Value = obj.Text
                ElseIf PrtyIsValue(obj) = True Then
                    rsLoad(obj.DataField).Value = obj.Value
                Else
                    Debug.Print "Name = " & obj.Name & " ...Bad Property"
                End If
                
            End If
        End If
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Function PrtyIsText(obj As Object) As Boolean
    On Error GoTo Error
    Dim var As Variant
    
    gbLastActivityTime = Date + Time
    
    var = obj.Text
    
    PrtyIsText = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    PrtyIsText = False
    Err.Clear
    gbLastActivityTime = Date + Time
End Function


Function PrtyIsValue(obj As Object) As Boolean
    On Error GoTo Error
    Dim var As Variant
    
    gbLastActivityTime = Date + Time
    
    var = obj.Value
    
    PrtyIsValue = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    PrtyIsValue = False
    Err.Clear
    gbLastActivityTime = Date + Time
End Function

Function PrtyIsDataField(obj As Object) As Boolean
    On Error GoTo Error
    Dim var As Variant
    
    gbLastActivityTime = Date + Time
    
    var = obj.DataField
    
    PrtyIsDataField = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    Err.Clear
    PrtyIsDataField = False
    
    gbLastActivityTime = Date + Time
End Function


Private Sub txtTotalTargetGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Sub CheckCOARqrmnt()
    On Error GoTo Error
    
    If IsNull(rsLoad("FrontCOARequired").Value) = True Then
        chPrntCOAFront.Enabled = False  'RKL DEJ 2018-01-05 changed from true to false Always try to print the COA Lock this down.
    Else
        If rsLoad("FrontCOARequired").Value = 0 Then
            chPrntCOAFront.Enabled = False  'RKL DEJ 2018-01-05 changed from true to false Always try to print the COA Lock this down.
        Else
            chPrntCOAFront.Enabled = False
            
            rsLoad("FrontPrintCOA").Value = 1

        End If
    End If
    
    If IsNull(rsLoad("RearCOARequired").Value) = True Then
        chPrntCOARear.Enabled = True
    Else
        If rsLoad("RearCOARequired").Value = 0 Then
            chPrntCOARear.Enabled = True
        Else
            chPrntCOARear.Enabled = False
            rsLoad("RearPrintCOA").Value = 1
        End If
    End If
    
'rsLoad("FrontPrintCOA").Value = 0
'rsLoad("FrontCOARequired").Value = 0
'rsLoad("RearPrintCOA").Value = 0
'rsLoad("RearCOARequired").Value = 0
    
    Exit Sub
Error:
    MsgBox Me.Name & ".CheckCOARqrmnt()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
End Sub

'********************************************************************************************************************
'********************************************************************************************************************
'RKL DEJ 1/22/14 (START)
'********************************************************************************************************************
'********************************************************************************************************************
Sub SetReqTonColorFrnt()
    On Error Resume Next
        
    Dim BgColor01 As Variant
    Dim BgColor02 As Variant
    
    Dim TxtColor01 As Variant
    Dim TxtColor02 As Variant
    
    Dim dblReqTons As Double
    Dim dblActualTons As Double
    Dim dblActualTonsRear As Double
    
    BgColor01 = &H8000000F
    TxtColor01 = &H80000008
    
    BgColor02 = &HC0FFFF
    TxtColor02 = &HFF&
    
    'Get the Required Tons
    If IsNumeric(txtFrontReqTons.Text) = True Then
        dblReqTons = CDbl(txtFrontReqTons.Text)
    Else
        dblReqTons = 0
    End If
    
    If IsNumeric(txtFrontTons.Text) = True Then
        dblActualTons = CDbl(txtFrontTons.Text)
    Else
        dblActualTons = 0
    End If
    
    If IsNumeric(txtRearTons.Text) = True Then
        dblActualTonsRear = CDbl(txtRearTons.Text)
    Else
        dblActualTonsRear = 0
    End If
    
    
    If (chSplitLoad.Value = vbChecked And dblActualTons <= 0) Or (chSplitLoad.Value = vbUnchecked And dblActualTons <= 0 And dblActualTonsRear <= 0) Then
        'Scale In
        'We are scaleing in so only need to check the Requested Tons
        'Check the Required Tons
        'On Scale In we don't care if it is a split load... We only care if the Requested tons is less than gbStdTon
        If gbScaleInWarnTons = False Or dblReqTons = 0 Or dblReqTons >= gbStdTon Then
            'Do Not Flash the Requested Tons
            bFrontTonsFlag = False
            
            'Normal Tons
            txtFrontReqTons.ForeColor = TxtColor01
            txtFrontReqTons.BackColor = BgColor01
            
            txtFrontReqTons.FontBold = False
            txtFrontReqTons.FontSize = 8.5   '14 or 8
            txtFrontReqTons.Height = 285
            
        Else
            'Yes Do Flash the Requested Tons
            bFrontTonsFlag = True
            
            'Out of range of normal
            With txtFrontReqTons
                .ForeColor = TxtColor02
                .BackColor = BgColor02
            
                .Height = 380
                .FontBold = True
                .FontSize = 18   '14 or 8
                .Top = 1680
            End With
            
        End If
        
    Else
        'Scale Out  (We know it is scale out because the Actual Tons <> 0 )
        'We are scaleing out So need to check requted and Actual tons
        'Check the Required Tons
        If gbScaleOutWarnVar = False Or chSplitLoad.Value = vbChecked Then
            If gbScaleOutWarnVar = False Or dblReqTons = 0 Or (Abs(dblReqTons - dblActualTons) <= gbMaxTonVar) Then
                bFrontTonsFlag = False
                
                'Normal Tons
                txtFrontReqTons.ForeColor = TxtColor01
                txtFrontReqTons.BackColor = BgColor01
                
                txtFrontReqTons.FontBold = False
                txtFrontReqTons.FontSize = 8.5   '14 or 8
                txtFrontReqTons.Height = 285
                
            Else
                bFrontTonsFlag = True
                
                'Out of range of normal
                With txtFrontReqTons
                    .ForeColor = TxtColor02
                    .BackColor = BgColor02
                
                    .Height = 380
                    .FontBold = True
                    .FontSize = 18   '14 or 8
                    .Top = 1680
                End With
                
            End If
        Else
            'Validate the entire Load(Front)
            If dblReqTons = 0 Or (Abs(dblReqTons - (dblActualTons + dblActualTonsRear)) <= gbMaxTonVar) Then
                bFrontTonsFlag = False
                
                'Normal Tons
                txtFrontReqTons.ForeColor = TxtColor01
                txtFrontReqTons.BackColor = BgColor01
                
                txtFrontReqTons.FontBold = False
                txtFrontReqTons.FontSize = 8.5   '14 or 8
                txtFrontReqTons.Height = 285
                
            Else
                bFrontTonsFlag = True
                
                'Out of range of normal
                With txtFrontReqTons
                    .ForeColor = TxtColor02
                    .BackColor = BgColor02
                
                    .Height = 380
                    .FontBold = True
                    .FontSize = 18   '14 or 8
                    .Top = 1680
                End With
                
            End If
        End If
    End If
    
    
    If chSplitLoad.Value = vbUnchecked Then
        bRearTonsFlag = False   'When not split load the rear will always be false
    
        'Normal Tons
        txtRearReqTons.ForeColor = TxtColor01
        txtRearReqTons.BackColor = BgColor01
        
        txtRearReqTons.FontBold = False
        txtRearReqTons.FontSize = 8.5   '14 or 8
        txtRearReqTons.Height = 285
        
        txtRearReqTons.Refresh
    End If
        
    txtFrontReqTons.Refresh
    
    If bFrontTonsFlag = True Or bRearTonsFlag = True Then
        tmrReqTons.Enabled = True
    Else
        tmrReqTons.Enabled = False
    End If
    
    Err.Clear
End Sub

Sub SetReqTonColorRear()
    On Error Resume Next
        
    Dim BgColor01 As Variant
    Dim BgColor02 As Variant
    
    Dim TxtColor01 As Variant
    Dim TxtColor02 As Variant
    
    Dim dblReqTons As Double
    Dim dblReqTonsRear As Double
    Dim dblActualTons As Double
    Dim dblActualTonsRear As Double
    
    BgColor01 = &H8000000F
    TxtColor01 = &H80000008
    
    BgColor02 = &HC0FFFF
    TxtColor02 = &HFF&
    
    'Get the Required Tons
    If IsNumeric(txtFrontReqTons.Text) = True Then
        dblReqTons = CDbl(txtFrontReqTons.Text)
    Else
        dblReqTons = 0
    End If
    
    If IsNumeric(txtRearReqTons.Text) = True Then
        dblReqTonsRear = CDbl(txtRearReqTons.Text)
    Else
        dblReqTonsRear = 0
    End If
    
    If IsNumeric(txtFrontTons.Text) = True Then
        dblActualTons = CDbl(txtFrontTons.Text)
    Else
        dblActualTons = 0
    End If
    
    If IsNumeric(txtRearTons.Text) = True Then
        dblActualTonsRear = CDbl(txtRearTons.Text)
    Else
        dblActualTonsRear = 0
    End If
    
    If chSplitLoad.Value = vbChecked Then
        'Split Load
        If dblActualTonsRear <= 0 Then
            'Scale In
            If gbScaleInWarnTons = False Or dblReqTonsRear = 0 Or dblReqTonsRear >= gbStdTon Then
                'Do Not Flash the Requested Tons
                bRearTonsFlag = False
                
                'Normal Tons
                txtRearReqTons.ForeColor = TxtColor01
                txtRearReqTons.BackColor = BgColor01
                
                txtRearReqTons.FontBold = False
                txtRearReqTons.FontSize = 8.5   '14 or 8
                txtRearReqTons.Height = 285
                
            Else
                'Yes Do Flash the Requested Tons
                bRearTonsFlag = True
                
                'Out of range of normal
                With txtRearReqTons
                    .ForeColor = TxtColor02
                    .BackColor = BgColor02
                
                    .Height = 380
                    .FontBold = True
                    .FontSize = 18   '14 or 8
                    .Top = 1680
                End With
                
            End If
        
        Else
            'Scale Out
            If gbScaleOutWarnVar = False Or dblReqTonsRear = 0 Or (Abs(dblReqTonsRear - dblActualTonsRear) <= gbMaxTonVar) Then
                bRearTonsFlag = False
                
                'Normal Tons
                txtRearReqTons.ForeColor = TxtColor01
                txtRearReqTons.BackColor = BgColor01
                
                txtRearReqTons.FontBold = False
                txtRearReqTons.FontSize = 8.5   '14 or 8
                txtRearReqTons.Height = 285
                
            Else
                bRearTonsFlag = True
                
                'Out of range of normal
                With txtRearReqTons
                    .ForeColor = TxtColor02
                    .BackColor = BgColor02
                
                    .Height = 380
                    .FontBold = True
                    .FontSize = 18   '14 or 8
                    .Top = 1680
                End With
                
            End If
        End If
    Else
        'Not Split Load
        If dblActualTonsRear <= 0 And dblActualTons <= 0 Then
            'Scale In
            If gbScaleInWarnTons = False Or dblReqTons = 0 Or dblReqTons >= gbStdTon Then
                'Do Not Flash the Requested Tons
                bFrontTonsFlag = False
                
                'Normal Tons
                txtFrontReqTons.ForeColor = TxtColor01
                txtFrontReqTons.BackColor = BgColor01
                
                txtFrontReqTons.FontBold = False
                txtFrontReqTons.FontSize = 8.5   '14 or 8
                txtFrontReqTons.Height = 285
                
            Else
                'Yes Do Flash the Requested Tons
                bFrontTonsFlag = True
                
                'Out of range of normal
                With txtFrontReqTons
                    .ForeColor = TxtColor02
                    .BackColor = BgColor02
                
                    .Height = 380
                    .FontBold = True
                    .FontSize = 18   '14 or 8
                    .Top = 1680
                End With
                
            End If
        Else
            'Scale Out
            'Validate the entire Load(Front)
            If gbScaleOutWarnVar = False Or dblReqTons = 0 Or (Abs(dblReqTons - (dblActualTons + dblActualTonsRear)) <= gbMaxTonVar) Then
                bFrontTonsFlag = False
                
                'Normal Tons
                txtFrontReqTons.ForeColor = TxtColor01
                txtFrontReqTons.BackColor = BgColor01
                
                txtFrontReqTons.FontBold = False
                txtFrontReqTons.FontSize = 8.5   '14 or 8
                txtFrontReqTons.Height = 285
                
            Else
                bFrontTonsFlag = True
                
                'Out of range of normal
                With txtFrontReqTons
                    .ForeColor = TxtColor02
                    .BackColor = BgColor02
                
                    .Height = 380
                    .FontBold = True
                    .FontSize = 18   '14 or 8
                    .Top = 1680
                End With
                
            End If
        End If
    End If
    
    
    If chSplitLoad.Value = vbUnchecked Then
        bRearTonsFlag = False   'When not split load the rear will always be false

        'Normal Tons
        txtRearReqTons.ForeColor = TxtColor01
        txtRearReqTons.BackColor = BgColor01

        txtRearReqTons.FontBold = False
        txtRearReqTons.FontSize = 8.5   '14 or 8
        txtRearReqTons.Height = 285

    End If
        
    txtRearReqTons.Refresh
    txtFrontReqTons.Refresh
    
    If bFrontTonsFlag = True Or bRearTonsFlag = True Then
        tmrReqTons.Enabled = True
    Else
        tmrReqTons.Enabled = False
    End If
    
    Err.Clear
End Sub

Function IsValidShipTons() As Boolean
    On Error Resume Next
    Dim FrontRqdTons As Double
    Dim FrontActualTons As Double
    Dim RearRqdTons As Double
    Dim RearActualTons As Double
    Dim LMEMsg As String
    Dim LMEMsgRear As String
    Dim LMEMsgFront As String
    Dim LoadID As String
    
'    Dim bIsFrontValid As Boolean
'    Dim bIsRearValid As Boolean
    
    Dim ErrMsg As String
    
    'Defalut to true
    IsValidShipTons = True
    
    If chFrontCdRtn.Value = vbChecked Or chRearCdRtn.Value = vbChecked Then
        Exit Function
    End If
    
    If IsNumeric(txtFrontReqTons.Text) = True Then
        FrontRqdTons = CDbl(txtFrontReqTons.Text)
    Else
        FrontRqdTons = 0
    End If
    
    If IsNumeric(txtFrontTons.Text) = True Then
        FrontActualTons = CDbl(txtFrontTons.Text)
    Else
        FrontActualTons = 0
    End If
    
    If IsNumeric(txtRearReqTons.Text) = True Then
        RearRqdTons = CDbl(txtRearReqTons.Text)
    Else
        RearRqdTons = 0
    End If
    
    If IsNumeric(txtRearTons.Text) = True Then
        RearActualTons = CDbl(txtRearTons.Text)
    Else
        RearActualTons = 0
    End If
    
    If gbProcessBOLWarnVar = True Then
        If chSplitLoad.Value = vbChecked Then
            'Validate the Split Load
            If FrontRqdTons <> 0 And FrontActualTons <> 0 And Abs(FrontRqdTons - FrontActualTons) > gbMaxTonVar Then
                IsValidShipTons = False
                ErrMsg = "This front load was scheduled to take " & FrontRqdTons & " tons and you have filled it up to " & FrontActualTons & " tons." & vbCrLf
                
                'RKL DEJ 12/16/14 added get message from LME
                If gbShowLMEMsg = True Then
                    LoadID = ConvertToString(rsLoad("FrontMcLeodOrderNumber").Value)
                    LMEMsgFront = GetLMEMessage(LoadID)
                    If LMEMsgFront <> Empty Then
                        LMEMsgFront = "Front Load:" & LMEMsgFront
                    End If
                End If
            
            End If
            
            If RearRqdTons <> 0 And RearActualTons <> 0 And Abs(RearRqdTons - RearActualTons) > gbMaxTonVar Then
                IsValidShipTons = False
                If ErrMsg <> Empty Then
                    ErrMsg = ErrMsg & "And this rear load was scheduled to take " & RearRqdTons & " tons and you have filled it up to " & RearActualTons & " tons." & vbCrLf
                    
                    'RKL DEJ 12/16/14 added get message from LME
                    If gbShowLMEMsg = True Then
                        LoadID = ConvertToString(rsLoad("RearMcLeodOrderNumber").Value)
                        LMEMsgRear = GetLMEMessage(LoadID)
                        If LMEMsgRear <> Empty Then
                            LMEMsgRear = "Rear Load:" & LMEMsgRear
                        End If
                    End If
                Else
                    ErrMsg = "This rear load was scheduled to take " & RearRqdTons & " tons and you have filled it up to " & RearActualTons & " tons." & vbCrLf
                
                    'RKL DEJ 12/16/14 added get message from LME
                    If gbShowLMEMsg = True Then
                        LoadID = ConvertToString(rsLoad("RearMcLeodOrderNumber").Value)
                        LMEMsgRear = GetLMEMessage(LoadID)
                        If LMEMsgRear <> Empty Then
                            LMEMsgRear = "Rear Load:" & LMEMsgRear
                        End If
                    End If
                End If
            End If
        Else
            'Validate the entire Load(Front)
            If FrontRqdTons <> 0 And FrontActualTons <> 0 And Abs(FrontRqdTons - (FrontActualTons + RearActualTons)) > gbMaxTonVar Then
                IsValidShipTons = False
                ErrMsg = "This front load was scheduled to take " & FrontRqdTons & " tons and you have filled it up to " & (FrontActualTons + RearActualTons) & " tons." & vbCrLf
                
                'RKL DEJ 12/16/14 added get message from LME
                If gbShowLMEMsg = True Then
                    LoadID = ConvertToString(rsLoad("FrontMcLeodOrderNumber").Value)
                    LMEMsgFront = GetLMEMessage(LoadID)
                    If LMEMsgFront <> Empty Then
                        LMEMsgFront = "Front Load:" & LMEMsgFront
                    End If
                End If
            End If
            
        End If
    End If
    
'    'Validate the Front
'    If FrontRqdTons <> 0 And FrontActualTons <> 0 And Abs(FrontRqdTons - FrontActualTons) > 2 Then
'        IsValidShipTons = False
'        ErrMsg = "This front load was scheduled to take " & FrontRqdTons & " tons and you have filled it up to " & FrontActualTons & " tons." & vbCrLf
'
'    End If
'
'    If RearRqdTons <> 0 And RearActualTons <> 0 And Abs(RearRqdTons - RearActualTons) > 2 And chSplitLoad.Value = vbChecked Then
'        IsValidShipTons = False
'        If ErrMsg <> Empty Then
'            ErrMsg = ErrMsg & "And this rear load was scheduled to take " & RearRqdTons & " tons and you have filled it up to " & RearActualTons & " tons." & vbCrLf
'        Else
'            ErrMsg = "This rear load was scheduled to take " & RearRqdTons & " tons and you have filled it up to " & RearActualTons & " tons." & vbCrLf
'        End If
'    End If
    
    If IsValidShipTons = False Then
        ErrMsg = ErrMsg & vbCrLf & "Do you want to process the BOL anyway?"
        
        'RKL DEJ 12/16/14 added get message from LME
        If LMEMsgFront <> Empty Or LMEMsgRear <> Empty Then
            LMEMsg = "Message from LME: " & vbCrLf
            
            If LMEMsgFront <> Empty Then
                LMEMsg = LMEMsg & LMEMsgFront & vbCrLf
            End If
            
            If LMEMsgRear <> Empty Then
                LMEMsg = LMEMsg & vbCrLf & LMEMsgRear
            End If
            
            ErrMsg = ErrMsg & vbCrLf & vbCrLf & LMEMsg
        End If
        
        If MsgBox(ErrMsg, vbYesNoCancel, "Tons Shipped Not Valid") = vbYes Then
            IsValidShipTons = True
        End If
    End If
    
    Err.Clear

End Function

'RKL DEJ 12/18/14
Sub ItemClassAdditive()
    On Error GoTo Error
    gbLastActivityTime = Date + Time
    
    Dim FrontProdClass As String
    Dim RearProdClass As String
    
    'Do not allow edit of Addive unless the product item class is cutbacks
    
    If rsLoad Is Nothing Then
        Exit Sub
    End If
    
    If rsLoad.EOF Or rsLoad.BOF Then
        Exit Sub
    End If
    
    FrontProdClass = UCase(Trim(ConvertToString(rsLoad("FrontProductClass").Value)))
    RearProdClass = UCase(Trim(ConvertToString(rsLoad("RearProductClass").Value)))
    
    'Always check the front.
    If InStr(1, FrontProdClass, "CUTBACK") > 0 Then
        txtFrontAdditive.Enabled = False '2018-01-12 Don't allow enable (From True to False)
        txtFrontAdditive.BackColor = &H8000000F '2018-01-12 Don't allow enable (From &H80000005 to &H8000000F)
        cmdFrntSrchAdtv.Enabled = False  '2018-01-12 Don't allow enable  (From True to False)
    Else
        txtFrontAdditive.Enabled = False
        txtFrontAdditive.BackColor = &H8000000F
        cmdFrntSrchAdtv.Enabled = False
    End If
    
    'If split load then check the rear.
    If chSplitLoad.Value = vbChecked Then
        If InStr(1, RearProdClass, "CUTBACK") > 0 Then
            txtRearAdditive.Enabled = True
            txtRearAdditive.BackColor = &H80000005
            cmdRearSrchAdtv.Enabled = True
        Else
            txtRearAdditive.Enabled = False
            txtRearAdditive.BackColor = &H8000000F
            cmdRearSrchAdtv.Enabled = False
        End If
    Else
        txtRearAdditive.Enabled = True
        txtRearAdditive.BackColor = &H80000005
        cmdRearSrchAdtv.Enabled = True
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    gbLastActivityTime = Date + Time
    
    MsgBox Me.Name & ".ItemClassAdditive()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

'********************************************************************************************************************
'********************************************************************************************************************
'RKL DEJ 1/22/14 (STOP)
'********************************************************************************************************************
'********************************************************************************************************************

Sub CheckCOARequired()
    On Error GoTo Error
    
    'Check Front
    Call CheckCOA(True)
    
    'Check Rear
    If Abs(ConvertToDouble(rsLoad("SplitLoad").Value)) = 1 Then
        Call CheckCOA(False)
    End If
    
    Exit Sub
Error:
    gbLastActivityTime = Date + Time
    
    MsgBox Me.Name & ".CheckCOARequired()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

Sub CheckCOA(Optional bFront As Boolean = True)
    On Error GoTo Error
    
    Dim lPrintCOA As Integer
    Dim lCOARequired As Integer
    Dim lProductID As String
    Dim lCreditReturn As Integer
    Dim lTankNbr As String
    Dim lContractNbr As String
    Dim LotNumber As String
    Dim sMsg As String
    
    If bFront = True Then
        lPrintCOA = Abs(rsLoad("FrontPrintCOA").Value)
        lCOARequired = rsLoad("FrontCOARequired").Value
        lProductID = rsLoad("FrontProductID").Value
        lCreditReturn = rsLoad("FrontCreditReturn").Value
        lTankNbr = rsLoad("FrontTankNbr").Value
        lContractNbr = rsLoad("FrontContractNbr").Value
        
        sMsg = "For the Front Load, contract '" & Trim(lContractNbr) & "': "
    Else
        lPrintCOA = Abs(rsLoad("RearPrintCOA").Value)
        lCOARequired = rsLoad("RearCOARequired").Value
        lProductID = rsLoad("RearProductID").Value
        lCreditReturn = rsLoad("RearCreditReturn").Value
        lTankNbr = rsLoad("RearTankNbr").Value
        lContractNbr = rsLoad("RearContractNbr").Value
    
        sMsg = "For the Rear Load, contract '" & Trim(lContractNbr) & "': "
    End If
    
    If lPrintCOA = 1 And gbEnforceCOA = True And Abs(ConvertToDouble(lCreditReturn)) <> 1 Then
        'Commented out... this was moved above RKL DEJ 3/20/14
        LotNumber = Trim(GetLotNumber(Trim(lTankNbr), Trim(lProductID)))
        
        If LotNumber = Empty Then
            'There is no COA data to print (Not a Active Lot Number)
            'If Require don't allow to print/proceed Else give a warning
            If lCOARequired = 1 Then
                'Required don't allow to print unless User allows printing in MAS
                If CanPrintBOL(lContractNbr) = True Then
                    'Required but allow to print BOL
                    MsgBox sMsg & "The COA is required, and there is not an Active Lot Number to print a COA.  You can still print the BOL but no COA will be printed.", vbInformation, "Scale Pass"
                Else
                    'Required don't allow to print
                    MsgBox sMsg & "The COA is required, and there is not an Active Lot Number to print a COA.  You cannot print the BOL without a Active Lot and valid COA.", vbInformation, "Scale Pass"
                End If
            Else
                'Not required so allow to print
                MsgBox sMsg & "There is not an Active Lot Number to print a COA.  You can still print the BOL but no COA will be printed.", vbInformation, "Scale Pass"
            End If
        End If
    End If
    
    
    Exit Sub
Error:
    gbLastActivityTime = Date + Time
    
    MsgBox Me.Name & ".CheckCOA()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub

'RKL DEJ 2017-11-06 (START)
Sub VerifyRead()
    On Error Resume Next
    
    Static BgColor01 As Variant
    Static BgColor02 As Variant
    Static BgColor03 As Variant
    Static bIni As Boolean
    
    If bIni = False Then
        bIni = True
        BgColor01 = &HFF&
        BgColor02 = &HFF00&
        BgColor03 = &H80FFFF
    End If
    
    With txtReadingScaleIndicator1
        .BackColor = BgColor01
        .Refresh
    End With
    
    With txtReadingScaleIndicator2
        .BackColor = BgColor02
        .Refresh
    End With
            
    With txtReadingScaleIndicator3
        .BackColor = BgColor03
        .Refresh
    End With
            
    If BgColor01 = &HFF& Then
        BgColor01 = &HFF00&
        BgColor02 = &H80FFFF
        BgColor03 = &HFF&
    ElseIf BgColor01 = &HFF00& Then
        BgColor01 = &H80FFFF
        BgColor02 = &HFF&
        BgColor03 = &HFF00&
    Else
        BgColor01 = &HFF&
        BgColor02 = &HFF00&
        BgColor03 = &H80FFFF
    End If
    
    Err.Clear

End Sub
'RKL DEJ 2017-11-06 (STOP)



'RKL DEJ 2017-12-18 (START)
Function GetTotalSrcQty() As Double
    On Error GoTo Error
    Dim RtnVal As Double
    Dim Qty01 As Double
    Dim Qty02 As Double
    Dim Qty03 As Double
    
    Qty01 = ConvertToDouble(txtSrceQtyOne.Text)
    Qty02 = ConvertToDouble(txtSrceQtyTwo.Text)
    Qty03 = ConvertToDouble(txtSrceQtyThree.Text)
    
    RtnVal = Qty01 + Qty02 + Qty03
    
    GetTotalSrcQty = RtnVal
    
    Exit Function
    
Error:
    MsgBox Me.Name & ".GetTotalSrcQty()" & vbCrLf & _
    "Error:" & Err.Number & " " & Err.Description, vbCritical, "Scale Pass Error"
    
    Err.Clear
    
    GetTotalSrcQty = -1
End Function

Function IsValidVariance() As Boolean
    On Error GoTo Error
    
    Dim TotalSrcQty As Double
    Dim ScaleWeight As Double
    Dim ActualVarience As Double
    Dim ErrMsg As String
    
    TotalSrcQty = Abs(GetTotalSrcQty())
    
    ScaleWeight = Abs(ConvertToDouble(txtTotalGrossTons.Text))
    
    ActualVarience = Abs(ScaleWeight - TotalSrcQty)
    
    'Check to see if we are in tolerance
    'but only pop up warning if the force Variance check has been specified in setup.
    If (ActualVarience > Abs(gbSrcVariance)) And gbForceVarianceCheck = True And CDbl(txtTotalGrossWt.Text) > 0 And chIsTruckBlend.Value = vbChecked Then
        IsValidVariance = False
        
        ErrMsg = "The scaled weight is out of tolerance with the Source Product(s) weight." & vbCrLf
        ErrMsg = ErrMsg & "The scaled weight is: " & ScaleWeight & vbCrLf
        ErrMsg = ErrMsg & "The Source Product(s) weight is: " & TotalSrcQty & vbCrLf
        ErrMsg = ErrMsg & "The allowable Variance is: " & Abs(gbSrcVariance) & vbCrLf
        ErrMsg = ErrMsg & "The actual Variance is: " & ActualVarience & vbCrLf
        
        MsgBox ErrMsg, vbExclamation, "Source Weight Out of Tolerance"
        
'        ErrMsg = ErrMsg & "Do you want to continue anyway?"
        
'        If MsgBox(ErrMsg, vbYesNo, "Out of Tolerance") = vbYes Then
'            IsValidVariance = True
'        End If
    Else
        IsValidVariance = True
    End If
    
    Exit Function
Error:
    MsgBox Me.Name & ".IsValidVariance()" & vbCrLf & _
    "Error:" & Err.Number & " " & Err.Description, vbCritical, "Scale Pass Error"
    
    Err.Clear
End Function

Sub EnableSrcFields(BackColor As Variant, bEnabled As Boolean)
    On Error GoTo Error
    
    'White - &H80000005
    'Grey - &H8000000F
    
    txtSrcTankOne.Enabled = bEnabled
    txtSrcTankOne.BackColor = BackColor
    
    txtSrcProductIDOne.Enabled = bEnabled
    txtSrcProductIDOne.BackColor = BackColor
    
    txtSrceQtyOne.Enabled = bEnabled
    txtSrceQtyOne.BackColor = BackColor
    
    txtSrcTankTwo.Enabled = bEnabled
    txtSrcTankTwo.BackColor = BackColor
    
    txtSrcProductIDTwo.Enabled = bEnabled
    txtSrcProductIDTwo.BackColor = BackColor
    
    txtSrceQtyTwo.Enabled = bEnabled
    txtSrceQtyTwo.BackColor = BackColor
    
    txtSrcTankThree.Enabled = bEnabled
    txtSrcTankThree.BackColor = BackColor
    
    txtSrcProductIDThree.Enabled = bEnabled
    txtSrcProductIDThree.BackColor = BackColor
    
    txtSrceQtyThree.Enabled = bEnabled
    txtSrceQtyThree.BackColor = BackColor
    
    cmdSrcProdOne.Enabled = bEnabled
    cmdSrcProdTwo.Enabled = bEnabled
    cmdSrcProdThree.Enabled = bEnabled
    
    Exit Sub
Error:
    MsgBox Me.Name & ".EnableSrcFields()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub

Sub ShowHideRearFields()
    On Error GoTo Error
    
    Dim bVisible As Boolean
    
    If gbNotAllowSplitLoads = True Then
        bVisible = False
    Else
        bVisible = True
    End If
    
    frameFrontSpecs.Visible = bVisible
    frameRearSpecs.Visible = bVisible
    frameRearLoad.Visible = bVisible
    frameRearLoad.Visible = bVisible
    Line1.Visible = bVisible
    Line8.Visible = bVisible
    Label23.Visible = bVisible
    cmdTareRear.Visible = bVisible
    cmdGrossRear.Visible = bVisible
    
    If bVisible = False Then
        'Move up the remaining controls to the top of the form
        Frame1.Top = frameFrontSpecs.Top
        FrameValeroFlds.Top = frameFrontSpecs.Top
        Frame3.Top = Frame1.Top + Frame1.Height + 30
        Me.Height = Frame3.Top + 1305
    Else
        Frame1.Top = frameRearLoad.Top
        FrameValeroFlds.Top = frameRearLoad.Top
        Frame3.Top = Frame1.Top + Frame1.Height + 30
        Me.Height = Frame3.Top + 1305
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".ShowHideRearFields()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub

'RKL DEJ 2018-01-05 added GetLMEShipToAddr
Public Sub GetLMEShipToAddr(LoadID As String)
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim RtnVal As String
    
    If IsObject(gbMASConn) <> True Then
        Exit Sub
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Sub
    End If
    
    SQL = "Select * From vluLMELoadOrderAddess_RKL Where OrderID = '" & LoadID & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        
    Else
        'Records found
        RS.MoveFirst
        
        rsLoadExt("LMEDestLocationCode").Value = RS("LocationCode").Value
        rsLoadExt("LMEDestAddress1").Value = RS("Address1").Value
        rsLoadExt("LMEDestAddress2").Value = RS("Address2").Value
        rsLoadExt("LMEDestcity_Name").Value = RS("city_Name").Value
        rsLoadExt("LMEDestState").Value = RS("State").Value
        rsLoadExt("LMEDestzip_code").Value = RS("zip_code").Value
        rsLoadExt("LMEDestCustomer_ID").Value = RS("Customer_ID").Value
        rsLoadExt("LMEDestAddrName").Value = RS("AddrName").Value
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".GetLMEShipToAddr()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    GoTo CleanUP
End Sub

'RKL DEJ 2017-12-18 (STOP)


