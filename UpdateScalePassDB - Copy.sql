
CREATE INDEX FrontBOL ON Loads (FrontBOL);
CREATE INDEX FrontContractNbr ON Loads (FrontContractNbr);
CREATE INDEX FrontCustomer ON Loads (FrontCustomer);
CREATE INDEX TruckNo ON Loads (TruckNo);
CREATE INDEX SplitLoad ON Loads (SplitLoad);
CREATE INDEX [TimeOut] ON Loads ([TimeOut]);
CREATE INDEX [TimeIn] ON Loads ([TimeIn]);
CREATE INDEX LoadIsOpen ON Loads (LoadIsOpen);


CREATE INDEX BOLNbr ON BOLPrinting (BOLNbr);
CREATE INDEX ContractNbr ON BOLPrinting (ContractNbr);
CREATE INDEX Customer ON BOLPrinting (Customer);
CREATE INDEX LotNbr ON BOLPrinting (LotNbr);
CREATE INDEX ScaleOutTime ON BOLPrinting (ScaleOutTime);
CREATE INDEX ScaleOutTime ON BOLPrinting (ScaleOutTime);
CREATE INDEX HasPrintedSuccessfully ON BOLPrinting (HasPrintedSuccessfully);
CREATE INDEX HasMASBOLLogBeenCreated ON BOLPrinting (HasMASBOLLogBeenCreated);
CREATE INDEX HasMASSOBeenCreated ON BOLPrinting (HasMASSOBeenCreated);
CREATE INDEX HasBeenSentToMAS ON BOLPrinting (HasBeenSentToMAS);
CREATE INDEX CertIsRequired ON BOLPrinting (CertIsRequired);
CREATE INDEX CertHasPrinted ON BOLPrinting (CertHasPrinted);
CREATE INDEX HasPrintedSuccessfully ON BOLPrinting (HasPrintedSuccessfully);

CREATE INDEX UpdateIndex ON BOLPrinting (HasPrintedSuccessfully, HasMASBOLLogBeenCreated, HasMASSOBeenCreated, HasBeenSentToMAS);
