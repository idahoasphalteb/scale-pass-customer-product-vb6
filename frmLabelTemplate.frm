VERSION 5.00
Begin VB.Form frmLabelTemplate 
   BackColor       =   &H00FF00FF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Label Template - Valero"
   ClientHeight    =   6225
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   5400
   ControlBox      =   0   'False
   Icon            =   "frmLabelTemplate.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6225
   ScaleWidth      =   5400
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtAdditive 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   2
      Top             =   1200
      Width           =   4455
   End
   Begin VB.ComboBox cmboPrinter 
      Height          =   315
      Left            =   600
      TabIndex        =   13
      Text            =   "Combo1"
      Top             =   5400
      Width           =   2295
   End
   Begin VB.TextBox txtCopyCount 
      Alignment       =   2  'Center
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   3960
      TabIndex        =   15
      Text            =   "1"
      Top             =   5400
      Width           =   1335
   End
   Begin VB.TextBox txtTruckNbr 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   9
      Top             =   3480
      Width           =   4455
   End
   Begin VB.TextBox txtProjectNbr 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   8
      Top             =   3120
      Width           =   4455
   End
   Begin VB.TextBox txtTons 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3960
      TabIndex        =   6
      Top             =   2400
      Width           =   1335
   End
   Begin VB.TextBox txtTank 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   720
      TabIndex        =   5
      Top             =   2400
      Width           =   1695
   End
   Begin VB.TextBox txtCustomer 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   960
      TabIndex        =   7
      Top             =   2760
      Width           =   4335
   End
   Begin VB.TextBox txtBOL 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   4
      Top             =   2040
      Width           =   4455
   End
   Begin VB.TextBox txtDate 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   3
      Top             =   1680
      Width           =   4455
   End
   Begin VB.TextBox txtProduct 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   840
      TabIndex        =   1
      Top             =   800
      Width           =   4455
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   7
      Left            =   2520
      Locked          =   -1  'True
      TabIndex        =   25
      TabStop         =   0   'False
      Text            =   "Tons Shipped:"
      Top             =   2400
      Width           =   1335
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   6
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   24
      TabStop         =   0   'False
      Text            =   "Truck#:"
      Top             =   3480
      Width           =   5295
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   5
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   23
      TabStop         =   0   'False
      Text            =   "Project#:"
      Top             =   3120
      Width           =   5295
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   22
      TabStop         =   0   'False
      Text            =   "Customer:"
      Top             =   2760
      Width           =   5295
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   21
      TabStop         =   0   'False
      Text            =   "Tank #:"
      Top             =   2400
      Width           =   5295
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   2
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   20
      TabStop         =   0   'False
      Text            =   "BL#:"
      Top             =   2040
      Width           =   5295
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   19
      TabStop         =   0   'False
      Text            =   "Date:"
      Top             =   1680
      Width           =   5295
   End
   Begin VB.ComboBox cmboCompany 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      ItemData        =   "frmLabelTemplate.frx":000C
      Left            =   0
      List            =   "frmLabelTemplate.frx":001F
      Sorted          =   -1  'True
      TabIndex        =   10
      Top             =   3960
      Width           =   5295
   End
   Begin VB.ComboBox cmboPlant 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      ItemData        =   "frmLabelTemplate.frx":0088
      Left            =   0
      List            =   "frmLabelTemplate.frx":009E
      Sorted          =   -1  'True
      TabIndex        =   11
      Top             =   4560
      Width           =   5295
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   255
      Left            =   2760
      TabIndex        =   17
      Top             =   5880
      Width           =   1215
   End
   Begin VB.CommandButton cmpPrint 
      Caption         =   "Print"
      Default         =   -1  'True
      Height          =   255
      Left            =   1440
      TabIndex        =   16
      Top             =   5880
      Width           =   1215
   End
   Begin VB.ComboBox cmboProductClass 
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      ItemData        =   "frmLabelTemplate.frx":00FC
      Left            =   0
      List            =   "frmLabelTemplate.frx":010F
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   0
      Width           =   5295
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   18
      TabStop         =   0   'False
      Text            =   "Product:"
      Top             =   800
      Width           =   5295
   End
   Begin VB.TextBox txtCaption 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   8
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   26
      TabStop         =   0   'False
      Text            =   "Additive:"
      Top             =   1200
      Width           =   5295
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   7
      X1              =   0
      X2              =   5280
      Y1              =   1485
      Y2              =   1485
   End
   Begin VB.Label Label1 
      Caption         =   "Printer:"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   5400
      Width           =   855
   End
   Begin VB.Label lblCopyCount 
      Caption         =   "# of Copies"
      Height          =   255
      Left            =   3000
      TabIndex        =   14
      Top             =   5400
      Width           =   855
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   0
      X1              =   0
      X2              =   5280
      Y1              =   1080
      Y2              =   1080
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   6
      X1              =   0
      X2              =   5280
      Y1              =   3765
      Y2              =   3765
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   5
      X1              =   0
      X2              =   5280
      Y1              =   3405
      Y2              =   3405
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   4
      X1              =   0
      X2              =   5280
      Y1              =   3045
      Y2              =   3045
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   3
      X1              =   0
      X2              =   5280
      Y1              =   2685
      Y2              =   2685
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   2
      X1              =   0
      X2              =   5280
      Y1              =   2325
      Y2              =   2325
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      Index           =   1
      X1              =   0
      X2              =   5280
      Y1              =   1965
      Y2              =   1965
   End
End
Attribute VB_Name = "frmLabelTemplate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public bPrint As Boolean
Dim lblData As LabelData



Friend Function GetlblData() As LabelData
    GetlblData = lblData
End Function

Public Sub CreatelblData(LoadsTableKey As Long, IsFront As Boolean)
    'Lookup Label Data in table
    lblData = GetLabelData(LoadsTableKey, IsFront)
'    lblData = basMethods.GetLabelData(0, True)

    SetLabelValues
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Public Sub SetLabelValues()

    txtBOL.Text = lblData.BLNbr
    cmboCompany.Text = lblData.Company
    txtCustomer.Text = lblData.Customer
    cmboPlant.Text = lblData.Plant
    txtProduct.Text = lblData.Product
    txtAdditive.Text = lblData.Additive
    cmboProductClass.Text = lblData.ProductClass
    txtProjectNbr.Text = lblData.ProjectNbr
    txtDate.Text = lblData.ScaleInDate
    txtTank.Text = lblData.Tank
    txtTons.Text = lblData.TonsShipped
    txtTruckNbr.Text = lblData.TruckNbr

End Sub

Public Sub GetLabelValues()
    lblData.BLNbr = txtBOL.Text
    lblData.Company = cmboCompany.Text
    lblData.Customer = txtCustomer.Text
    lblData.Plant = cmboPlant.Text
    lblData.Product = txtProduct.Text
    lblData.Additive = txtAdditive.Text
    lblData.ProductClass = cmboProductClass.Text
    lblData.ProjectNbr = txtProjectNbr.Text
    lblData.ScaleInDate = txtDate.Text
    lblData.Tank = txtTank.Text
    lblData.TonsShipped = txtTons.Text
    lblData.TruckNbr = txtTruckNbr.Text
    lblData.TonsCaption = "Tons Shippped"
End Sub

Private Sub cmpPrint_Click()
    Call GetLabelValues
        
'    lblData = frmLabelTemplate.GetlblData()
    
    If basMethods.PrintLabelData(lblData, cmboPrinter.Text, CInt(txtCopyCount.Text)) = False Then
        MsgBox "The Label print had errors.", vbInformation, "Bad Print"
    End If
    
End Sub


Private Sub Form_Load()
    Call PopulateCombos
End Sub

Private Sub txtCopyCount_Validate(Cancel As Boolean)
    If IsNumeric(txtCopyCount.Text) = True Then
        txtCopyCount.Text = CInt(txtCopyCount.Text)
    Else
        txtCopyCount.Text = 1
    End If
    
    gbLastActivityTime = Date + Time

End Sub

Sub PopulateCombos()
    On Error GoTo Error
    
    Dim Prntr As Printer
    
    gbLastActivityTime = Date + Time
        
    cmboPrinter.Clear
    
    For Each Prntr In Printers
        cmboPrinter.AddItem Prntr.DeviceName
    Next
    
    cmboPrinter.Text = gbLablePrinter
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".PopulateCombos()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


