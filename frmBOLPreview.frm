VERSION 5.00
Begin VB.Form frmBOLPreview 
   BackColor       =   &H00FF00FF&
   Caption         =   "Bill of Lading - Valero"
   ClientHeight    =   8235
   ClientLeft      =   4680
   ClientTop       =   3630
   ClientWidth     =   10725
   Icon            =   "frmBOLPreview.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8235
   ScaleWidth      =   10725
   Visible         =   0   'False
   Begin VB.CheckBox chAltBOLPrinter 
      Caption         =   "Use Alternate Printer"
      DataSource      =   "Adodc1"
      Height          =   285
      Left            =   6480
      TabIndex        =   96
      Top             =   7800
      Width           =   1845
   End
   Begin VB.CommandButton cmdTestPrint 
      Caption         =   "&Test BOL"
      Height          =   375
      Left            =   8400
      TabIndex        =   97
      Top             =   7800
      Visible         =   0   'False
      Width           =   1005
   End
   Begin VB.CommandButton cmdPrintBOL 
      Caption         =   "Print &BOL"
      Height          =   375
      Left            =   9480
      TabIndex        =   98
      Top             =   7800
      Width           =   1005
   End
   Begin VB.Frame Frame4 
      Caption         =   "Product Information"
      Height          =   2160
      Left            =   180
      TabIndex        =   104
      Top             =   5520
      Width           =   10365
      Begin VB.TextBox txtWtTktNo 
         DataField       =   "WeightTktNo"
         Height          =   285
         Left            =   8490
         MaxLength       =   30
         TabIndex        =   95
         Text            =   " "
         Top             =   1200
         Width           =   1725
      End
      Begin VB.TextBox txtBatchNbr 
         DataField       =   "BatchNumber"
         Height          =   285
         Left            =   8490
         MaxLength       =   50
         TabIndex        =   93
         Text            =   " "
         Top             =   720
         Width           =   1725
      End
      Begin VB.TextBox txtViscocityTemp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "ViscosityTemp"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   5280
         Locked          =   -1  'True
         TabIndex        =   86
         TabStop         =   0   'False
         Text            =   " 0"
         Top             =   1440
         Width           =   645
      End
      Begin VB.TextBox txtLotNr 
         DataField       =   "LotNbr"
         Height          =   285
         Left            =   8490
         MaxLength       =   50
         TabIndex        =   91
         Text            =   " "
         Top             =   360
         Width           =   1725
      End
      Begin VB.TextBox txtNetGals 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "NetGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   71
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1530
         Width           =   1140
      End
      Begin VB.TextBox txtGrossGals 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "GrossGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   69
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1125
         Width           =   1140
      End
      Begin VB.TextBox txtResidue 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "Residue"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4425
         Locked          =   -1  'True
         TabIndex        =   89
         Top             =   1750
         Width           =   645
      End
      Begin VB.TextBox txtViscocity 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "Viscosity"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4425
         Locked          =   -1  'True
         TabIndex        =   84
         Top             =   1440
         Width           =   645
      End
      Begin VB.TextBox txtLoadTemp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "LoadTemp"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4425
         Locked          =   -1  'True
         TabIndex        =   81
         TabStop         =   0   'False
         Top             =   1125
         Width           =   645
      End
      Begin VB.TextBox txtFlashpoint 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "Flashpoint"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4425
         Locked          =   -1  'True
         TabIndex        =   73
         TabStop         =   0   'False
         Text            =   " "
         Top             =   150
         Width           =   645
      End
      Begin VB.TextBox txtSpecificGravity 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "SpecificGravity"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4425
         Locked          =   -1  'True
         TabIndex        =   78
         TabStop         =   0   'False
         Text            =   " "
         Top             =   810
         Width           =   645
      End
      Begin VB.TextBox txtLbsPerGal2 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "LbsPerGallon2"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4425
         Locked          =   -1  'True
         TabIndex        =   76
         TabStop         =   0   'False
         Text            =   " "
         Top             =   480
         Width           =   645
      End
      Begin VB.TextBox txtAdditive 
         BackColor       =   &H8000000F&
         DataField       =   "AdditiveDesc"
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   67
         TabStop         =   0   'False
         Text            =   " "
         Top             =   750
         Width           =   1770
      End
      Begin VB.TextBox txtProduct 
         BackColor       =   &H8000000F&
         DataField       =   "ProductDesc"
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   65
         TabStop         =   0   'False
         Text            =   " "
         Top             =   360
         Width           =   1770
      End
      Begin VB.Label Label27 
         Caption         =   "Weight Ticket #"
         Height          =   255
         Left            =   7290
         TabIndex        =   94
         Top             =   1245
         Width           =   1215
      End
      Begin VB.Label lblBatchNbr 
         Caption         =   "Batch #"
         Height          =   255
         Left            =   7290
         TabIndex        =   92
         Top             =   765
         Width           =   855
      End
      Begin VB.Label Label30 
         Alignment       =   1  'Right Justify
         Caption         =   "�F"
         Height          =   255
         Left            =   5880
         TabIndex        =   87
         Top             =   1440
         Width           =   240
      End
      Begin VB.Label Label35 
         Caption         =   "�F"
         Height          =   255
         Left            =   5100
         TabIndex        =   82
         Top             =   1125
         Width           =   255
      End
      Begin VB.Label lblSpecGravity 
         Caption         =   "@ 60�F"
         Height          =   255
         Left            =   5085
         TabIndex        =   79
         Top             =   810
         Width           =   600
      End
      Begin VB.Label Label33 
         Caption         =   "�F"
         Height          =   255
         Left            =   5145
         TabIndex        =   74
         Top             =   210
         Width           =   255
      End
      Begin VB.Label Label32 
         Caption         =   "Net Gals"
         Height          =   255
         Left            =   120
         TabIndex        =   70
         Top             =   1575
         Width           =   765
      End
      Begin VB.Label Label31 
         Caption         =   "Gross Gals"
         Height          =   255
         Left            =   120
         TabIndex        =   68
         Top             =   1170
         Width           =   855
      End
      Begin VB.Label lblViscocity 
         Caption         =   "@"
         Height          =   255
         Left            =   5070
         TabIndex        =   85
         Top             =   1440
         Width           =   240
      End
      Begin VB.Label Label29 
         Caption         =   "Residue"
         Height          =   255
         Left            =   3255
         TabIndex        =   88
         Top             =   1750
         Width           =   1215
      End
      Begin VB.Label Label28 
         Caption         =   "Viscosity"
         Height          =   255
         Left            =   3255
         TabIndex        =   83
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label lblLotNumber 
         Caption         =   "Lot Number"
         Height          =   255
         Left            =   7290
         TabIndex        =   90
         Top             =   405
         Width           =   855
      End
      Begin VB.Label Label26 
         Caption         =   "Load Temp"
         Height          =   255
         Left            =   3255
         TabIndex        =   80
         Top             =   1125
         Width           =   855
      End
      Begin VB.Label Label25 
         Caption         =   "Spec. Gravity"
         Height          =   255
         Left            =   3210
         TabIndex        =   77
         Top             =   810
         Width           =   1215
      End
      Begin VB.Label Label24 
         Caption         =   "Flashpoint"
         Height          =   255
         Left            =   3210
         TabIndex        =   72
         Top             =   150
         Width           =   855
      End
      Begin VB.Label Label23 
         Caption         =   "Lbs/Gal"
         Height          =   255
         Left            =   3210
         TabIndex        =   75
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label11 
         Caption         =   "Additive"
         Height          =   255
         Left            =   120
         TabIndex        =   66
         Top             =   795
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Product"
         Height          =   255
         Left            =   120
         TabIndex        =   64
         Top             =   390
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Weights/Gallons"
      Height          =   2175
      Left            =   180
      TabIndex        =   101
      Top             =   3240
      Width           =   7170
      Begin VB.TextBox txtRearAdditive 
         BackColor       =   &H8000000F&
         DataField       =   "RearAdditive"
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   115
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1050
         Visible         =   0   'False
         Width           =   2265
      End
      Begin VB.TextBox txtFrontAdditive 
         BackColor       =   &H8000000F&
         DataField       =   "FrontAdditive"
         Height          =   285
         Left            =   3960
         Locked          =   -1  'True
         TabIndex        =   114
         TabStop         =   0   'False
         Text            =   " "
         Top             =   720
         Width           =   2265
      End
      Begin VB.TextBox txtLbsPerGal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "LbsPerGallon"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4080
         Locked          =   -1  'True
         TabIndex        =   53
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1755
         Width           =   645
      End
      Begin VB.TextBox txtTotalGals 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "TotalGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   6255
         Locked          =   -1  'True
         TabIndex        =   55
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1755
         Width           =   690
      End
      Begin VB.TextBox txtFrontAdditiveGals 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontAdditiveGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   6240
         TabIndex        =   38
         Text            =   " "
         Top             =   720
         Width           =   690
      End
      Begin VB.TextBox txtRearAdditiveGals 
         Alignment       =   1  'Right Justify
         DataField       =   "RearAdditiveGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   6255
         TabIndex        =   44
         Text            =   " "
         Top             =   1035
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.TextBox txtTotalAdditiveGals 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "TotalAdditiveGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   6255
         Locked          =   -1  'True
         TabIndex        =   49
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1350
         Width           =   690
      End
      Begin VB.TextBox txtTotalBaseGals 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "TotalBaseGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2790
         Locked          =   -1  'True
         TabIndex        =   48
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1350
         Width           =   690
      End
      Begin VB.TextBox txtRearBaseGals 
         Alignment       =   1  'Right Justify
         DataField       =   "RearBaseGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2790
         TabIndex        =   42
         Text            =   " "
         Top             =   1035
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.TextBox txtRearTemp 
         Alignment       =   1  'Right Justify
         DataField       =   "RearTemp"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   3510
         TabIndex        =   43
         Text            =   " "
         Top             =   1035
         Visible         =   0   'False
         Width           =   420
      End
      Begin VB.TextBox txtFrontBaseGals 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontBaseGallons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2790
         TabIndex        =   36
         Text            =   " "
         Top             =   720
         Width           =   690
      End
      Begin VB.TextBox txtFrontTemp 
         Alignment       =   1  'Right Justify
         DataField       =   "FrontTemp"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   3510
         TabIndex        =   37
         Text            =   " "
         Top             =   720
         Width           =   420
      End
      Begin VB.TextBox txtNetTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "NetTons"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.000"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   51
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1755
         Width           =   645
      End
      Begin VB.TextBox txtFrontNetWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontNetWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   35
         TabStop         =   0   'False
         Text            =   " "
         Top             =   720
         Width           =   645
      End
      Begin VB.TextBox txtRearNetWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearNetWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   41
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1035
         Visible         =   0   'False
         Width           =   645
      End
      Begin VB.TextBox txtTotalNetWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "TotalNetWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   47
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1350
         Width           =   645
      End
      Begin VB.TextBox txtFrontTareWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontTareWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1530
         Locked          =   -1  'True
         TabIndex        =   34
         TabStop         =   0   'False
         Text            =   " "
         Top             =   720
         Width           =   645
      End
      Begin VB.TextBox txtRearTareWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearTareWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1530
         Locked          =   -1  'True
         TabIndex        =   40
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1035
         Visible         =   0   'False
         Width           =   645
      End
      Begin VB.TextBox txtTotalTareWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "TotalTareWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   1530
         Locked          =   -1  'True
         TabIndex        =   46
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1350
         Width           =   645
      End
      Begin VB.TextBox txtFrontGrossWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontGrossWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   33
         TabStop         =   0   'False
         Text            =   " "
         Top             =   720
         Width           =   645
      End
      Begin VB.TextBox txtRearGrossWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearGrossWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   39
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1035
         Visible         =   0   'False
         Width           =   645
      End
      Begin VB.TextBox txtTotalGrossWt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "TotalGrossWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   45
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1350
         Width           =   645
      End
      Begin VB.Label Label40 
         Alignment       =   1  'Right Justify
         Caption         =   "Lbs/Gal"
         Height          =   255
         Left            =   3150
         TabIndex        =   52
         Top             =   1800
         Width           =   765
      End
      Begin VB.Label Label39 
         Alignment       =   2  'Center
         Caption         =   "Additive"
         Height          =   255
         Left            =   4230
         TabIndex        =   112
         Top             =   495
         Width           =   1620
      End
      Begin VB.Label Label38 
         Alignment       =   1  'Right Justify
         Caption         =   "Total Gallons"
         Height          =   255
         Left            =   4950
         TabIndex        =   54
         Top             =   1800
         Width           =   1125
      End
      Begin VB.Label Label37 
         Alignment       =   2  'Center
         Caption         =   "Additive Gals"
         Height          =   480
         Left            =   6300
         TabIndex        =   111
         Top             =   315
         Width           =   585
      End
      Begin VB.Label Label36 
         Alignment       =   2  'Center
         Caption         =   "Temp �F"
         Height          =   390
         Left            =   3510
         TabIndex        =   110
         Top             =   315
         Width           =   480
      End
      Begin VB.Label Label21 
         Alignment       =   2  'Center
         Caption         =   "Base Gals"
         Height          =   480
         Left            =   2790
         TabIndex        =   109
         Top             =   315
         Width           =   585
      End
      Begin VB.Label Label22 
         Alignment       =   1  'Right Justify
         Caption         =   "Net Tons"
         Height          =   255
         Left            =   900
         TabIndex        =   50
         Top             =   1800
         Width           =   1125
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "Net"
         Height          =   255
         Left            =   2250
         TabIndex        =   108
         Top             =   495
         Width           =   495
      End
      Begin VB.Label Label16 
         Alignment       =   2  'Center
         Caption         =   "Tare"
         Height          =   255
         Left            =   1620
         TabIndex        =   107
         Top             =   495
         Width           =   495
      End
      Begin VB.Label Label20 
         Caption         =   "Front"
         Height          =   255
         Left            =   360
         TabIndex        =   106
         Top             =   765
         Width           =   495
      End
      Begin VB.Label Label15 
         Caption         =   "Rear"
         Height          =   255
         Left            =   360
         TabIndex        =   105
         Top             =   1080
         Visible         =   0   'False
         Width           =   450
      End
      Begin VB.Label Label17 
         Alignment       =   2  'Center
         Caption         =   "Gross"
         Height          =   255
         Left            =   945
         TabIndex        =   103
         Top             =   495
         Width           =   495
      End
      Begin VB.Label Label19 
         Caption         =   "Total"
         Height          =   255
         Left            =   360
         TabIndex        =   102
         Top             =   1395
         Width           =   495
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Timing"
      Height          =   2175
      Left            =   7425
      TabIndex        =   100
      Top             =   3240
      Width           =   3120
      Begin VB.TextBox txtLoadDt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "LoadTime"
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   57
         TabStop         =   0   'False
         Text            =   " "
         Top             =   405
         Width           =   2055
      End
      Begin VB.TextBox txtDeliverDt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         CausesValidation=   0   'False
         DataField       =   "DeliverTime"
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   59
         TabStop         =   0   'False
         Text            =   " "
         Top             =   810
         Width           =   2055
      End
      Begin VB.TextBox txtInDt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "ScaleInTime"
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   61
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1215
         Width           =   2055
      End
      Begin VB.TextBox txtOutDt 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "ScaleOutTime"
         Height          =   285
         Left            =   900
         Locked          =   -1  'True
         TabIndex        =   63
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1620
         Width           =   2055
      End
      Begin VB.Label Label14 
         Caption         =   "Load"
         Height          =   255
         Left            =   270
         TabIndex        =   56
         Top             =   450
         Width           =   495
      End
      Begin VB.Label Label9 
         Caption         =   "Deliver"
         Height          =   255
         Left            =   270
         TabIndex        =   58
         Top             =   855
         Width           =   630
      End
      Begin VB.Label Label8 
         Caption         =   "In"
         Height          =   255
         Left            =   270
         TabIndex        =   60
         Top             =   1260
         Width           =   630
      End
      Begin VB.Label Label7 
         Caption         =   "Out"
         Height          =   255
         Left            =   270
         TabIndex        =   62
         Top             =   1665
         Width           =   765
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Load Information"
      Height          =   3090
      Left            =   180
      TabIndex        =   99
      Top             =   135
      Width           =   10365
      Begin VB.TextBox txtDeliveryLoc 
         DataField       =   "MASDeliveryLoc"
         Height          =   435
         Left            =   6000
         MaxLength       =   50
         MultiLine       =   -1  'True
         TabIndex        =   27
         Top             =   345
         Width           =   4200
      End
      Begin VB.TextBox txtCarrierName 
         DataField       =   "Carrier_Name"
         Height          =   285
         Left            =   5790
         MaxLength       =   30
         TabIndex        =   25
         Text            =   " "
         ToolTipText     =   "Carrier Name"
         Top             =   2760
         Width           =   4395
      End
      Begin VB.TextBox txtShipToState 
         BackColor       =   &H8000000F&
         DataField       =   "ShipToState"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   2640
         Width           =   1455
      End
      Begin VB.CheckBox chDestDirectEdit 
         Caption         =   "Allow Edit"
         Height          =   195
         Left            =   7920
         TabIndex        =   29
         Top             =   895
         Width           =   1575
      End
      Begin VB.CheckBox chHighRisk 
         Alignment       =   1  'Right Justify
         Caption         =   "High Risk"
         DataField       =   "HighRisk"
         Enabled         =   0   'False
         Height          =   375
         Left            =   3000
         TabIndex        =   18
         Top             =   2350
         Width           =   735
      End
      Begin VB.TextBox txtSecTapeNbr 
         DataField       =   "SecurityTapNumber"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   3840
         MaxLength       =   100
         TabIndex        =   20
         Top             =   2400
         Width           =   1725
      End
      Begin VB.TextBox txtCarrierID 
         DataField       =   "Carrier_ID"
         Height          =   285
         Left            =   3555
         MaxLength       =   10
         TabIndex        =   24
         Text            =   " "
         ToolTipText     =   "Carrier ID"
         Top             =   2760
         Width           =   2115
      End
      Begin VB.TextBox txtPO 
         DataField       =   "Purchase_Order"
         Height          =   285
         Left            =   3435
         TabIndex        =   15
         Text            =   " "
         Top             =   1890
         Width           =   2115
      End
      Begin VB.TextBox txtTrailer2 
         DataField       =   "Trailer2_ID"
         Height          =   285
         Left            =   4215
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   330
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.TextBox txtTrailer1 
         DataField       =   "Trailer1_ID"
         Height          =   285
         Left            =   2800
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   330
         Width           =   1335
      End
      Begin VB.TextBox txtHauler 
         BackColor       =   &H8000000F&
         DataField       =   "Hauler"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   7
         TabStop         =   0   'False
         Text            =   " "
         Top             =   705
         Width           =   4200
      End
      Begin VB.TextBox txtRemarks 
         DataField       =   "Remarks"
         Height          =   555
         Left            =   5985
         MaxLength       =   74
         MultiLine       =   -1  'True
         TabIndex        =   32
         Top             =   2125
         Width           =   4200
      End
      Begin VB.TextBox txtDestination 
         BackColor       =   &H8000000F&
         DataField       =   "DestDirections"
         Enabled         =   0   'False
         Height          =   705
         Left            =   6000
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   30
         Top             =   1120
         Width           =   4200
      End
      Begin VB.TextBox txtTruck 
         BackColor       =   &H8000000F&
         DataField       =   "TruckNo"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   360
         Width           =   1400
      End
      Begin VB.TextBox txtCust 
         BackColor       =   &H8000000F&
         DataField       =   "Customer"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   9
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1095
         Width           =   4200
      End
      Begin VB.TextBox txtContract 
         BackColor       =   &H8000000F&
         DataField       =   "ContractNbr"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1890
         Width           =   1455
      End
      Begin VB.TextBox txtDestAddress 
         BackColor       =   &H8000000F&
         DataField       =   "Destination"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Text            =   " "
         Top             =   1485
         Width           =   4200
      End
      Begin VB.TextBox txtBOLNbr 
         BackColor       =   &H8000000F&
         DataField       =   "BOLNbr"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   17
         TabStop         =   0   'False
         Text            =   " "
         Top             =   2295
         Width           =   1455
      End
      Begin VB.Label lblDeliveryLoc 
         Caption         =   "Delivery Location"
         Height          =   255
         Left            =   6000
         TabIndex        =   26
         Top             =   120
         Width           =   2295
      End
      Begin VB.Label Label44 
         Caption         =   "Ship To State"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   2670
         Width           =   1095
      End
      Begin VB.Label Label55 
         Caption         =   "Security #"
         Height          =   255
         Left            =   3840
         TabIndex        =   19
         Top             =   2205
         Width           =   1665
      End
      Begin VB.Label Label43 
         Caption         =   "Carrier"
         Height          =   255
         Left            =   3000
         TabIndex        =   23
         Top             =   2760
         Width           =   615
      End
      Begin VB.Label Label42 
         Caption         =   "P.O."
         Height          =   255
         Left            =   3000
         TabIndex        =   14
         Top             =   1950
         Width           =   375
      End
      Begin VB.Label Label41 
         Alignment       =   2  'Center
         Caption         =   "Trailer 2"
         Height          =   255
         Left            =   4215
         TabIndex        =   4
         Top             =   120
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.Label Label34 
         Alignment       =   2  'Center
         Caption         =   "Trailer 1"
         Height          =   255
         Left            =   2800
         TabIndex        =   2
         Top             =   120
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "Remarks"
         Height          =   255
         Left            =   5985
         TabIndex        =   31
         Top             =   1900
         Width           =   855
      End
      Begin VB.Label Label5 
         Caption         =   "Destination Directions"
         Height          =   255
         Left            =   5985
         TabIndex        =   28
         Top             =   895
         Width           =   2265
      End
      Begin VB.Label Label1 
         Caption         =   "Truck #"
         Height          =   255
         Left            =   360
         TabIndex        =   0
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Hauler"
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   750
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Customer"
         Height          =   255
         Left            =   360
         TabIndex        =   8
         Top             =   1140
         Width           =   855
      End
      Begin VB.Label Label4 
         Caption         =   "Contract #"
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   1950
         Width           =   855
      End
      Begin VB.Label Label13 
         Caption         =   "Contract Ds"
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   1530
         Width           =   855
      End
      Begin VB.Label Label10 
         Caption         =   "BOL #"
         Height          =   255
         Left            =   360
         TabIndex        =   16
         Top             =   2325
         Width           =   855
      End
   End
   Begin VB.Timer tmrAutoReturnNoActivity 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   0
      Top             =   0
   End
   Begin VB.Label lblStatus 
      Caption         =   "Status"
      Height          =   375
      Left            =   240
      TabIndex        =   113
      Top             =   7800
      Visible         =   0   'False
      Width           =   8055
   End
End
Attribute VB_Name = "frmBOLPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ErrMsg As String

Dim lcUpdateOnly As Boolean

Dim WithEvents rsLoad As ADODB.Recordset
Attribute rsLoad.VB_VarHelpID = -1
Dim WithEvents rsLoadExt As ADODB.Recordset
Attribute rsLoadExt.VB_VarHelpID = -1
Dim WithEvents RSBOL As ADODB.Recordset
Attribute RSBOL.VB_VarHelpID = -1
Dim WithEvents RSBOLExt As ADODB.Recordset
Attribute RSBOLExt.VB_VarHelpID = -1

Dim sBOLPass As String
Dim DebugOnly As Boolean

Enum PlacardTypes
    NotRegulated0 = 0
    ElevatedTemperature = 1
    HotTars = 2
    NotRegulated = 3
    other = 4
End Enum

Private Type BOLFields
    FromCompany As String
    ToCompany As String
    FromWhse As String
    ToWhse As String
    CommodityID As String
    
    IsBtwnCmpny As Boolean
    IsTransfer As Boolean
    
    IsCreditReturn As Boolean
    CreditReturnComment As String
    IsSplitLoad As Boolean
    SplitLoadType As String 'Front / Rear
    IsHandKeyed As Boolean
    IsScaleEnabled As Boolean
    ScaleWeightMsg As String
    PlacardType As PlacardTypes
    
    UserID As String
    LoadID As String
    PreBillID As String
    CustomerName As String
    ScaleDate As String
    Facility As String
    FacilityName As String
    
    InBoundOperator As String
    OutBoundOperator As String
    LadingSibling As String
    TankNumber As String
    RackNbr As String
    Tons As String
    
    '*******************************************************
    'Start of Field on the printed BOL
    '*******************************************************
    BOLNumber As String
    RtnOrigBOLNumber As String
    Customer As String
    BOLDate As String
    ContractNo As String
    PONo As String
    Origin As String
    PAGroupNo As String
    Destination As String
    DestDirections As String
    ProjectNo As String
    ProjectDescription As String
    Carrier As String
    FreightBillNo As String
    WeightTicketNo As String
    TruckNo As String
    Trailer1No As String
    Trailer2No As String
    LoadTime As String
    DeliveryTime As String
    LoadTimeIn As String
    LoadTimeOut As String
    TimeArrived As String
    StartUnload As String
    FinishUnload As String
    Remarks As String
    Penetration As String
    PenetrationTemp As String
    PenetrationRemark As String
    Flashpoint As String
    LbsPerGal As String
    SpecGravity  As String
    Viscosity As String
    ViscosityTemp As String
    ViscosityRemark As String
    LotNumber As String
    ProductSpecRemarks As String
    Residue As String
    CertOfCompliance As String
    AdditivePercent As String
    Temp As String
    GrossGal As String
    NetGal As String
    GrossWtLbs1 As String
    TareWtLbs1 As String
    NetWtLbs1 As String
    AdditiveID As String
'    Additive As String
    AdditiveDesc As String
    ItemSpec As String
    ProductName As String
    GrossWtLbs2 As String
    TareWtLbs2 As String
    NetWtLbs2 As String
    GrossWtLbs3 As String
    TareWtLbs3 As String
    NetWtLbs3 As String
    Quantity As String
    HM As String
    ProductID As String
    ProductDesc As String
'    Product As String
    NetTons As String
    SecTapeNbr As String
    '*******************************************************
    'End of Field on the printed BOL
    '*******************************************************
    
    'RKL DEJ 2016-09-07 Added Fields START
    BatchNumber As String
    WeightTktNo As String
    Carrier_ID As String
    Carrier_Name As String
    Purchase_Order As String
    'RKL DEJ 2016-09-07 Added Fields End
    
    '************************************************
    'RKL DEJ 2017-12-28 (START) Valero
    '************************************************
    CustOwnedProd As Integer
    AutoPostIMTrans As Integer
    NotAllowSplitLoads As Integer
    SrcVariance As Double
    IMTranReasonCode As String
    ForceVarianceCheck As Integer
    
    FrontSrcTankNbrOne As String
    FrontSrcTankNbrTwo As String
    FrontSrcTankNbrThree As String
    
    FrontSrcQtyOne As Double
    FrontSrcQtyTwo As Double
    FrontSrcQtyThree As Double
    
    FrontSrcProductIDOne As String
    FrontSrcProductOne As String
    FrontSrcProductClassOne As String
    
    FrontSrcProductIDTwo As String
    FrontSrcProductTwo As String
    FrontSrcProductClassTwo As String
    
    FrontSrcProductIDThree As String
    FrontSrcProductThree As String
    FrontSrcProductClassThree As String
    
    RearSrcTankNbrOne As String
    RearSrcTankNbrTwo As String
    RearSrcTankNbrThree As String
    
    RearSrcQtyOne As Double
    RearSrcQtyTwo As Double
    RearSrcQtyThree As Double
    
    RearSrcProductIDOne As String
    RearSrcProductOne As String
    RearSrcProductClassOne As String
    
    RearSrcProductIDTwo As String
    RearSrcProductTwo As String
    RearSrcProductClassTwo As String
    
    RearSrcProductIDThree As String
    RearSrcProductThree As String
    RearSrcProductClassThree As String
    
    SrcTankNbrOne As String
    SrcTankNbrTwo As String
    SrcTankNbrThree As String
    
    SrcQtyOne As Double
    SrcQtyTwo As Double
    SrcQtyThree As Double
    
    SrcProductIDOne As String
    SrcProductOne As String
    SrcProductClassOne As String
    
    SrcProductIDTwo As String
    SrcProductTwo As String
    SrcProductClassTwo As String
    
    SrcProductIDThree As String
    SrcProductThree As String
    SrcProductClassThree As String
    
    TranType As Integer
        
    '************************************************
    'RKL DEJ 2017-12-28 (STOP) Valero
    '************************************************
End Type

Function CreateMASBOLPrinting(Optional SuppressMsg As Boolean = False) As Boolean
    On Error GoTo Error
    
    On Error GoTo Error
    Dim CMD As New ADODB.Command
    Dim CreateStatus As Integer
    Dim MAS500BOLPrintTableKey As Variant
    Dim DateImportedToMAS500 As Variant

    gbLastActivityTime = Date + Time
    
    CreateMASBOLPrinting = False
    
    If gbLocalMode = True Then
        Exit Function
    Else
        If HaveLostMASConnection = True Then
            Exit Function
        End If
    End If
    
    If gbMASConn.State <> 1 Then
        If SuppressMsg = False Then
            MsgBox "The connection to MAS has been lost.  Cannot insert the BOL Table Record into MAS 500.", vbExclamation, "BOL Table Record Not Created"
        End If
        Exit Function
    End If
    
    gbMASConn.BeginTrans
    
    Set CMD.ActiveConnection = gbMASConn
    CMD.CommandType = adCmdStoredProc
    CMD.CommandText = "spsoInstsoScalePassBOLPrinting"
    CMD.Parameters.Refresh
    CMD.CommandTimeout = 0      'Added to prevent time outs

    'Assign input parameter Values
    CMD.Parameters("@BOLTableKey").Value = RSBOL("BOLTableKey").Value
    CMD.Parameters("@MAS500BOLTableKey").Value = RSBOL("MAS500BOLTableKey").Value
    CMD.Parameters("@LoadsTableKey").Value = RSBOL("LoadsTableKey").Value
    CMD.Parameters("@FrontRearFlag").Value = RSBOL("FrontRearFlag").Value
    CMD.Parameters("@TruckNo").Value = RSBOL("TruckNo").Value
    CMD.Parameters("@Hauler").Value = RSBOL("Hauler").Value
    CMD.Parameters("@Customer").Value = RSBOL("Customer").Value
    CMD.Parameters("@Project").Value = RSBOL("Project").Value
    CMD.Parameters("@ContractNbr").Value = RSBOL("ContractNbr").Value
    CMD.Parameters("@BOLNbr").Value = RSBOL("BOLNbr").Value
    CMD.Parameters("@Destination").Value = RSBOL("Destination").Value
    CMD.Parameters("@DestDirections").Value = RSBOL("DestDirections").Value
    CMD.Parameters("@Remarks").Value = RSBOL("Remarks").Value
    
'    If ConvertToString(RSBOL("IsSplitLoad").Value) = "1" Then
    If Abs(ConvertToDouble(RSBOL("IsSplitLoad").Value)) = 1 Then
        If ConvertToString(RSBOL("FrontRearFlag").Value) = "Front" Then
            'Front Weights
            CMD.Parameters("@FrontGrossWt").Value = RSBOL("FrontGrossWt").Value
            CMD.Parameters("@FrontTareWt").Value = RSBOL("FrontTareWt").Value
            CMD.Parameters("@FrontNetWt").Value = RSBOL("FrontNetWt").Value
            'Rear Weights
            CMD.Parameters("@RearGrossWt").Value = 0
            CMD.Parameters("@RearTareWt").Value = 0
            CMD.Parameters("@RearNetWt").Value = 0
            'Total Weights
            CMD.Parameters("@TotalGrossWt").Value = RSBOL("FrontGrossWt").Value
            CMD.Parameters("@TotalTareWt").Value = RSBOL("FrontTareWt").Value
            CMD.Parameters("@TotalNetWt").Value = RSBOL("FrontNetWt").Value
        Else
            'Front Weights
            CMD.Parameters("@FrontGrossWt").Value = 0
            CMD.Parameters("@FrontTareWt").Value = 0
            CMD.Parameters("@FrontNetWt").Value = 0
            'Rear Weights
            CMD.Parameters("@RearGrossWt").Value = RSBOL("RearGrossWt").Value
            CMD.Parameters("@RearTareWt").Value = RSBOL("RearTareWt").Value
            CMD.Parameters("@RearNetWt").Value = RSBOL("RearNetWt").Value
            'Total Weights
            CMD.Parameters("@TotalGrossWt").Value = RSBOL("RearGrossWt").Value
            CMD.Parameters("@TotalTareWt").Value = RSBOL("RearTareWt").Value
            CMD.Parameters("@TotalNetWt").Value = RSBOL("RearNetWt").Value
        End If
    Else
        'Front Weights
        CMD.Parameters("@FrontGrossWt").Value = RSBOL("FrontGrossWt").Value
        CMD.Parameters("@FrontTareWt").Value = RSBOL("FrontTareWt").Value
        CMD.Parameters("@FrontNetWt").Value = RSBOL("FrontNetWt").Value
        'Rear Weights
        CMD.Parameters("@RearGrossWt").Value = RSBOL("RearGrossWt").Value
        CMD.Parameters("@RearTareWt").Value = RSBOL("RearTareWt").Value
        CMD.Parameters("@RearNetWt").Value = RSBOL("RearNetWt").Value
        'Total Weights
        CMD.Parameters("@TotalGrossWt").Value = RSBOL("TotalGrossWt").Value
        CMD.Parameters("@TotalTareWt").Value = RSBOL("TotalTareWt").Value
        CMD.Parameters("@TotalNetWt").Value = RSBOL("TotalNetWt").Value
    End If
    
    CMD.Parameters("@FrontBaseGallons").Value = RSBOL("FrontBaseGallons").Value
    CMD.Parameters("@FrontTemp").Value = RSBOL("FrontTemp").Value
    CMD.Parameters("@FrontAdditive").Value = RSBOL("FrontAdditive").Value
    CMD.Parameters("@FrontAdditiveGallons").Value = RSBOL("FrontAdditiveGallons").Value
    CMD.Parameters("@RearGrossWt").Value = RSBOL("RearGrossWt").Value
    CMD.Parameters("@RearTareWt").Value = RSBOL("RearTareWt").Value
    CMD.Parameters("@RearNetWt").Value = RSBOL("RearNetWt").Value
    CMD.Parameters("@RearBaseGallons").Value = RSBOL("RearBaseGallons").Value
    CMD.Parameters("@RearTemp").Value = RSBOL("RearTemp").Value
    CMD.Parameters("@RearAdditive").Value = RSBOL("RearAdditive").Value
    CMD.Parameters("@RearAdditiveGallons").Value = RSBOL("RearAdditiveGallons").Value
    CMD.Parameters("@TotalBaseGallons").Value = RSBOL("TotalBaseGallons").Value
    CMD.Parameters("@TotalAdditiveGallons").Value = RSBOL("TotalAdditiveGallons").Value
    CMD.Parameters("@NetTons").Value = RSBOL("NetTons").Value
    CMD.Parameters("@LbsPerGallon").Value = RSBOL("LbsPerGallon").Value
    CMD.Parameters("@TotalGallons").Value = RSBOL("TotalGallons").Value
    CMD.Parameters("@LoadTime").Value = RSBOL("LoadTime").Value
    CMD.Parameters("@DeliverTime").Value = RSBOL("DeliverTime").Value
    CMD.Parameters("@ScaleInTime").Value = RSBOL("ScaleInTime").Value
    CMD.Parameters("@ScaleOutTime").Value = RSBOL("ScaleOutTime").Value
    CMD.Parameters("@Product").Value = RSBOL("ProductID").Value
    CMD.Parameters("@Additive").Value = RSBOL("AdditiveID").Value
    CMD.Parameters("@GrossGallons").Value = RSBOL("GrossGallons").Value
    CMD.Parameters("@NetGallons").Value = RSBOL("NetGallons").Value
    CMD.Parameters("@Flashpoint").Value = RSBOL("Flashpoint").Value
    CMD.Parameters("@LbsPerGallon2").Value = RSBOL("LbsPerGallon2").Value
    CMD.Parameters("@SpecificGravity").Value = RSBOL("SpecificGravity").Value
    CMD.Parameters("@LotNbr").Value = RSBOL("LotNbr").Value
    CMD.Parameters("@LoadTemp").Value = RSBOL("LoadTemp").Value
    CMD.Parameters("@Viscosity").Value = RSBOL("Viscosity").Value
    CMD.Parameters("@Residue").Value = RSBOL("Residue").Value
    CMD.Parameters("@RackNbr").Value = RSBOL("RackNbr").Value
    CMD.Parameters("@Tons").Value = RSBOL("Tons").Value
    CMD.Parameters("@PrintDate").Value = RSBOL("PrintDate").Value
    CMD.Parameters("@ViscosityTemp").Value = RSBOL("ViscosityTemp").Value
    CMD.Parameters("@IsCreditReturn").Value = RSBOL("IsCreditReturn").Value
    CMD.Parameters("@IsSplitLoad").Value = RSBOL("IsSplitLoad").Value
    CMD.Parameters("@IsHandKeyed").Value = RSBOL("IsHandKeyed").Value
    CMD.Parameters("@IsScaleEnabled").Value = RSBOL("IsScaleEnabled").Value
    CMD.Parameters("@DidMASAPIRunSuccessfully").Value = RSBOL("DidMASAPIRunSuccessfully").Value
    CMD.Parameters("@UserID").Value = RSBOL("UserID").Value
    CMD.Parameters("@MASloadid").Value = RSBOL("MASloadid").Value
    CMD.Parameters("@MAScustref").Value = RSBOL("MAScustref").Value
    CMD.Parameters("@MASprebillid").Value = RSBOL("MASprebillid").Value
    CMD.Parameters("@MAScustomer").Value = RSBOL("MAScustomer").Value
    CMD.Parameters("@MAScustomername").Value = RSBOL("MAScustomername").Value
    CMD.Parameters("@MASscaledate").Value = RSBOL("MASscaledate").Value
    CMD.Parameters("@MAScontract").Value = RSBOL("MAScontract").Value
    CMD.Parameters("@MAScustomerpo").Value = RSBOL("MAScustomerpo").Value
    CMD.Parameters("@MASFacility").Value = RSBOL("MASFacility").Value
    CMD.Parameters("@MASFacilityName").Value = RSBOL("MASFacilityName").Value
    CMD.Parameters("@MASProjectNumber").Value = RSBOL("MASProjectNumber").Value
    CMD.Parameters("@MASProjectDescription").Value = RSBOL("MASProjectDescription").Value
    CMD.Parameters("@MAScarrier").Value = RSBOL("MAScarrier").Value
    CMD.Parameters("@MAStruck").Value = RSBOL("MAStruck").Value
    CMD.Parameters("@MAStrailer1").Value = RSBOL("MAStrailer1").Value
    CMD.Parameters("@MAStrailer2").Value = RSBOL("MAStrailer2").Value
    CMD.Parameters("@MASloadtime").Value = RSBOL("MASloadtime").Value
    CMD.Parameters("@MASdeliverytime").Value = RSBOL("MASdeliverytime").Value
    CMD.Parameters("@MASshortdestination").Value = RSBOL("MASshortdestination").Value
    CMD.Parameters("@MASdestination").Value = RSBOL("MASdestination").Value
    CMD.Parameters("@MASpenetration").Value = RSBOL("MASpenetration").Value
    CMD.Parameters("@MASpenetrationtemperature").Value = RSBOL("MASpenetrationtemperature").Value
    CMD.Parameters("@MASpenetrationremark").Value = RSBOL("MASpenetrationremark").Value
    CMD.Parameters("@MASflash").Value = RSBOL("MASflash").Value
    CMD.Parameters("@MASlbspergallon").Value = RSBOL("MASlbspergallon").Value
    CMD.Parameters("@MASspecificgravity").Value = RSBOL("MASspecificgravity").Value
    CMD.Parameters("@MASviscosity").Value = RSBOL("MASviscosity").Value
    CMD.Parameters("@MASviscositytemperature").Value = RSBOL("MASviscositytemperature").Value
    CMD.Parameters("@MASviscosityremark").Value = RSBOL("MASviscosityremark").Value
    CMD.Parameters("@MAScompliance").Value = RSBOL("MAScompliance").Value
    CMD.Parameters("@MASadditive").Value = RSBOL("MASadditive").Value
    CMD.Parameters("@MASadditivedescription").Value = RSBOL("MASadditivedescription").Value
    CMD.Parameters("@MASadditivepercent").Value = RSBOL("MASadditivepercent").Value
    CMD.Parameters("@MAStemperaturemultiplier").Value = ConvertToDouble(RSBOL("MAStemperaturemultiplier").Value)
    CMD.Parameters("@MASspecificationremark").Value = RSBOL("MASspecificationremark").Value
    CMD.Parameters("@MASplacardtype").Value = RSBOL("MASplacardtype").Value
    CMD.Parameters("@MASitem").Value = RSBOL("MASitem").Value
    CMD.Parameters("@MASitemdescription").Value = RSBOL("MASitemdescription").Value
    CMD.Parameters("@MASCommodity_ID").Value = RSBOL("MASCommodity_ID").Value
    CMD.Parameters("@MASCommodity").Value = RSBOL("MASCommodity").Value
    CMD.Parameters("@MASFromCompanyID").Value = RSBOL("MASFromCompanyID").Value
    CMD.Parameters("@MASFromWhseID").Value = RSBOL("MASFromWhseID").Value
    CMD.Parameters("@MASToCompanyID").Value = RSBOL("MASToCompanyID").Value
    CMD.Parameters("@MASToWhseID").Value = RSBOL("MASToWhseID").Value
    CMD.Parameters("@LadingSibling").Value = RSBOL("LadingSibling").Value
    CMD.Parameters("@TankNumber").Value = RSBOL("TankNumber").Value
    CMD.Parameters("@InBoundOperator").Value = RSBOL("InBoundOperator").Value
    CMD.Parameters("@OutBoundOperator").Value = RSBOL("OutBoundOperator").Value
    CMD.Parameters("@HasPrintedSuccessfully").Value = RSBOL("HasPrintedSuccessfully").Value
    CMD.Parameters("@HasBeenSentToMAS").Value = RSBOL("HasBeenSentToMAS").Value
    CMD.Parameters("@HasMASBOLLogBeenCreated").Value = RSBOL("HasMASBOLLogBeenCreated").Value
    CMD.Parameters("@HasMASSOBeenCreated").Value = RSBOL("HasMASSOBeenCreated").Value
    CMD.Parameters("@ScaleDate").Value = RSBOL("ScaleDate").Value
    CMD.Parameters("@Laps").Value = rsLoad("Laps").Value
    CMD.Parameters("@TargetGrossWt").Value = RSBOL("TargetGrossWt").Value
    CMD.Parameters("@TargetNetWt").Value = RSBOL("TargetNetWt").Value
    CMD.Parameters("@WtPerGallon").Value = RSBOL("WtPerGallon").Value
    CMD.Parameters("@TargetGallons").Value = RSBOL("TargetGallons").Value
    CMD.Parameters("@SelectedGallons").Value = RSBOL("SelectedGallons").Value
    
    CMD.Parameters("@BOLAddressAndPhone").Value = RSBOL("BOLAddressAndPhone").Value
    CMD.Parameters("@PASSPatent").Value = RSBOL("PASSPatent").Value
    CMD.Parameters("@PrintPASSPatent").Value = RSBOL("PrintPASSPatent").Value
    CMD.Parameters("@LotNbrCaption").Value = RSBOL("LotNbrCaption").Value
    
    CMD.Parameters("@ShipToState").Value = RSBOL("ShipToState").Value
    
    CMD.Parameters("@BatchNumber").Value = RSBOL("BatchNumber").Value
    
    CMD.Parameters("@WeightTktNo").Value = RSBOL("WeightTktNo").Value
    CMD.Parameters("@Carrier_ID").Value = RSBOL("Carrier_ID").Value
    CMD.Parameters("@Carrier_Name").Value = RSBOL("Carrier_Name").Value
    CMD.Parameters("@Purchase_Order").Value = RSBOL("Purchase_Order").Value
    
    'RKL DEJ 2017-12-16 (START)
    'New fields for Customer Owned Product *** Valero
    CMD.Parameters("@CustOwnedProd").Value = gbCustOwnedProd 'RKL DEJ 2017-12-04 Added For Valero project
    
    CMD.Parameters("@CustOwnedProd").Value = gbCustOwnedProd     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@AutoPostIMTrans").Value = gbAutoPostIMTrans     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@NotAllowSplitLoads").Value = gbNotAllowSplitLoads     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcVariance").Value = gbSrcVariance     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@IMTranReasonCode").Value = gbIMTranReasonCode     'RKL DEJ 2017-12-04 Added For Valero project

    CMD.Parameters("@FrontSrcTankNbrOne").Value = RSBOLExt("FrontSrcTankNbrOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcTankNbrTwo").Value = RSBOLExt("FrontSrcTankNbrTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcTankNbrThree").Value = RSBOLExt("FrontSrcTankNbrThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcQtyOne").Value = RSBOLExt("FrontSrcQtyOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcQtyTwo").Value = RSBOLExt("FrontSrcQtyTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcQtyThree").Value = RSBOLExt("FrontSrcQtyThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductIDOne").Value = RSBOLExt("FrontSrcProductIDOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductOne").Value = RSBOLExt("FrontSrcProductOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductClassOne").Value = RSBOLExt("FrontSrcProductClassOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductIDTwo").Value = RSBOLExt("FrontSrcProductIDTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductTwo").Value = RSBOLExt("FrontSrcProductTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductClassTwo").Value = RSBOLExt("FrontSrcProductClassTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductIDThree").Value = RSBOLExt("FrontSrcProductIDThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductThree").Value = RSBOLExt("FrontSrcProductThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@FrontSrcProductClassThree").Value = RSBOLExt("FrontSrcProductClassThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcTankNbrOne").Value = RSBOLExt("RearSrcTankNbrOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcTankNbrTwo").Value = RSBOLExt("RearSrcTankNbrTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcTankNbrThree").Value = RSBOLExt("RearSrcTankNbrThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcQtyOne").Value = RSBOLExt("RearSrcQtyOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcQtyTwo").Value = RSBOLExt("RearSrcQtyTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcQtyThree").Value = RSBOLExt("RearSrcQtyThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductIDOne").Value = RSBOLExt("RearSrcProductIDOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductOne").Value = RSBOLExt("RearSrcProductOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductClassOne").Value = RSBOLExt("RearSrcProductClassOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductIDTwo").Value = RSBOLExt("RearSrcProductIDTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductTwo").Value = RSBOLExt("RearSrcProductTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductClassTwo").Value = RSBOLExt("RearSrcProductClassTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductIDThree").Value = RSBOLExt("RearSrcProductIDThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductThree").Value = RSBOLExt("RearSrcProductThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@RearSrcProductClassThree").Value = RSBOLExt("RearSrcProductClassThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcTankNbrOne").Value = RSBOLExt("SrcTankNbrOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcTankNbrTwo").Value = RSBOLExt("SrcTankNbrTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcTankNbrThree").Value = RSBOLExt("SrcTankNbrThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcQtyOne").Value = RSBOLExt("SrcQtyOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcQtyTwo").Value = RSBOLExt("SrcQtyTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcQtyThree").Value = RSBOLExt("SrcQtyThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductIDOne").Value = RSBOLExt("SrcProductIDOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductOne").Value = RSBOLExt("SrcProductOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductClassOne").Value = RSBOLExt("SrcProductClassOne").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductIDTwo").Value = RSBOLExt("SrcProductIDTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductTwo").Value = RSBOLExt("SrcProductTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductClassTwo").Value = RSBOLExt("SrcProductClassTwo").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductIDThree").Value = RSBOLExt("SrcProductIDThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductThree").Value = RSBOLExt("SrcProductThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@SrcProductClassThree").Value = RSBOLExt("SrcProductClassThree").Value     'RKL DEJ 2017-12-04 Added For Valero project
    CMD.Parameters("@IsTruckBlend").Value = RSBOLExt("IsTruckBlend").Value     'RKL DEJ 2017-12-04 Added For Valero project
    
    'New fields for Customer Owned Product *** Valero
    'RKL DEJ 2017-12-16 (STOP)
    
    
    gbLastActivityTime = Date + Time
    
    'Execute The procedure
    CMD.Execute

    gbLastActivityTime = Date + Time
    
    'Obtain Output Parameters
    CreateStatus = ConvertToDouble(CMD.Parameters.Item("@Success").Value)
    MAS500BOLPrintTableKey = CMD.Parameters("@MAS500BOLPrintTableKey").Value
    DateImportedToMAS500 = CMD.Parameters("@DateImportedToMAS500").Value

    Select Case CreateStatus
        Case 1
            gbMASConn.CommitTrans
            CreateMASBOLPrinting = True
            If SuppressMsg = False Then
'                MsgBox "Successfully created the BOL Printing record into MAS 500.", vbInformation, "BOL To MAS Success"
            End If
            
        Case Else
            gbMASConn.RollbackTrans
            CreateMASBOLPrinting = False
            If SuppressMsg = False Then
                MsgBox "Failed to created the BOL Printing record into MAS 500.", vbInformation, "BOL To MAS Failure"
            End If
            
            ErrMsg = Me.Name & ".CreateMASBOLPrinting() As Boolean" & vbCrLf & "Failed to created the BOL Printing record into MAS 500."
            If RSBOL.State = 1 Then
                If RSBOL.EOF Or RSBOL.BOF Then
                    InsErrorLog 0, Date + Time, ErrMsg
                Else
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                End If
            Else
                InsErrorLog 0, Date + Time, ErrMsg
            End If

    End Select
    
    
CleanUP:
    Err.Clear
    On Error Resume Next
    Set CMD = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    ErrMsg = Me.Name & ".CreateMASBOLPrinting() As Boolean" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description
    
    If SuppressMsg = False Then
        MsgBox ErrMsg, vbCritical, "Error"
    End If
    
    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, ErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, ErrMsg
    End If
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Function

Private Function GetPrintValues(ByRef PrintValues As BOLFields, Optional SuppressMsg As Boolean = False) As Boolean
    On Error GoTo Error
    Dim lErrMsg As String
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    PrintValues.FromCompany = ConvertToString(RSBOL("MASFromCompanyID").Value)
    CurrentLine = 1
    PrintValues.ToCompany = ConvertToString(RSBOL("MASToCompanyID").Value)
    CurrentLine = 2
    PrintValues.FromWhse = ConvertToString(RSBOL("MASFromWhseID").Value)
    CurrentLine = 3
    PrintValues.ToWhse = ConvertToString(RSBOL("MASToWhseID").Value)
    
    CurrentLine = 4
    PrintValues.CommodityID = ConvertToString(RSBOL("MASCommodity_ID").Value)
    
    CurrentLine = 5
    If PrintValues.CommodityID <> Empty Then
        CurrentLine = 6
        'If Left 3 = IPT or Left 2 = PT Then TO or New SO (Not from Blaket or existing SO)
        If UCase(Left(PrintValues.CommodityID, 3)) = "IPT" Or UCase(Left(PrintValues.CommodityID, 2)) = "PT" Or UCase(Left(PrintValues.CommodityID, 4)) = "PTCR" Then
            CurrentLine = 7
            'Is Transfer
            PrintValues.IsTransfer = True
            CurrentLine = 8
            If PrintValues.FromCompany <> PrintValues.ToCompany Then
                CurrentLine = 9
                'Is Between Companies
                PrintValues.IsBtwnCmpny = True
            Else
                CurrentLine = 10
                'Is in the same company
                PrintValues.IsBtwnCmpny = False
            End If
            CurrentLine = 11
        Else
            CurrentLine = 12
'            If UCase(Left(PrintValues.CommodityID, 2)) = "CR" And Not IsNull(PrintValues.ToCompany) And PrintValues.FromCompany <> PrintValues.ToCompany Then
'                'Is Transfer
'                PrintValues.IsTransfer = True
'                'Is Between Companies
'                PrintValues.IsBtwnCmpny = True
'            Else
                'Is Not Transfer
                PrintValues.IsTransfer = False
                CurrentLine = 13
                PrintValues.IsBtwnCmpny = False
                CurrentLine = 14
'            End If
        End If
        CurrentLine = 15
    Else
        CurrentLine = 16
        PrintValues.IsTransfer = False
        CurrentLine = 17
        PrintValues.IsBtwnCmpny = False
        CurrentLine = 18
    End If
    
    CurrentLine = 19
'    PrintValues.IsCreditReturn = ConvertToString(RSBOL("IsCreditReturn").Value) 'As Boolean
    If Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) > 0 Then
        CurrentLine = 20
        PrintValues.IsCreditReturn = True
    Else
        CurrentLine = 21
        PrintValues.IsCreditReturn = False
    End If
    
    CurrentLine = 22
'    PrintValues.CreditReturnComment = ConvertToString(RSBOL("FieldName").Value)
    PrintValues.IsSplitLoad = ConvertToString(RSBOL("IsSplitLoad").Value) 'As Boolean
    CurrentLine = 23
    PrintValues.SplitLoadType = ConvertToString(RSBOL("FrontRearFlag").Value) 'Front / Rear
    CurrentLine = 24
    PrintValues.IsHandKeyed = ConvertToString(RSBOL("IsHandKeyed").Value) 'As Boolean
    CurrentLine = 25
    PrintValues.IsScaleEnabled = ConvertToString(RSBOL("IsScaleEnabled").Value) 'As Boolean
'    PrintValues.ScaleWeightMsg = ConvertToString(RSBOL("FieldName").Value)
    CurrentLine = 26
    PrintValues.PlacardType = ConvertToDouble(RSBOL("MASPlacardtype").Value) 'As PlacardTypes
    
    CurrentLine = 27
    PrintValues.UserID = ConvertToString(RSBOL("UserID").Value)
    CurrentLine = 28
    PrintValues.LoadID = ConvertToString(RSBOL("MASloadid").Value)
    CurrentLine = 29
    PrintValues.PreBillID = ConvertToString(RSBOL("MASprebillid").Value)
    CurrentLine = 30
    PrintValues.CustomerName = ConvertToString(RSBOL("MAScustomername").Value)
    CurrentLine = 31
    PrintValues.ScaleDate = ConvertToString(RSBOL("ScaleDate").Value)
    CurrentLine = 32
    PrintValues.Facility = ConvertToString(RSBOL("MASFacility").Value)
    CurrentLine = 33
    PrintValues.FacilityName = ConvertToString(RSBOL("MASFacilityName").Value)
    
    CurrentLine = 34
    PrintValues.InBoundOperator = ConvertToString(RSBOL("InboundOperator").Value)
    CurrentLine = 35
    PrintValues.OutBoundOperator = ConvertToString(RSBOL("OutBoundOperator").Value)
    
    CurrentLine = 36
    PrintValues.LadingSibling = ConvertToString(RSBOL("LadingSibling").Value)
    
    CurrentLine = 37
    PrintValues.TankNumber = ConvertToString(RSBOL("TankNumber").Value)
    
    CurrentLine = 38
    PrintValues.BOLNumber = ConvertToString(RSBOL("BOLNbr").Value)
'    PrintValues.RtnOrigBOLNumber = ConvertToString(RSBOL("FieldName").Value)
'    PrintValues.Customer = ConvertToString(RSBOL("Customer").Value)
    CurrentLine = 39
    PrintValues.Customer = ConvertToString(RSBOL("MASCustomer").Value)
    CurrentLine = 40
    PrintValues.BOLDate = Date 'RSBOL("FieldName").Value)
    CurrentLine = 41
    PrintValues.ContractNo = ConvertToString(RSBOL("ContractNbr").Value)
    CurrentLine = 42
    PrintValues.PONo = ConvertToString(RSBOL("MAScustomerpo").Value)      'Comes from McLeod (MAS New View) vluGetBOLInfo_SGS.customerpo
    CurrentLine = 43
    PrintValues.Origin = ConvertToString(RSBOL("MASFacilityName").Value)  'Comes from MAS View: vluGetBOLInfo_SGS.FacilityName
'    PrintValues.PAGroupNo = ConvertToString(RSBOL("FieldName").Value)
    CurrentLine = 44
    PrintValues.Destination = ConvertToString(RSBOL("Destination").Value)
    CurrentLine = 45
    PrintValues.DestDirections = ConvertToString(RSBOL("DestDirections").Value)
    CurrentLine = 46
    PrintValues.ProjectNo = ConvertToString(RSBOL("MASProjectNumber").Value)    'Comes from MAS View: vluGetBOLInfo_SGS.projectnumber
    CurrentLine = 47
    PrintValues.ProjectDescription = ConvertToString(RSBOL("MASProjectDescription").Value)
    CurrentLine = 48
    PrintValues.Carrier = ConvertToString(RSBOL("Hauler").Value)
    
    CurrentLine = 49
    PrintValues.FreightBillNo = Empty
    CurrentLine = 50
    PrintValues.WeightTicketNo = Empty

    CurrentLine = 51
    PrintValues.TruckNo = ConvertToString(RSBOL("TruckNo").Value)
    CurrentLine = 52
    PrintValues.Trailer1No = ConvertToString(RSBOL("MAStrailer1").Value)
    CurrentLine = 53
    PrintValues.Trailer2No = ConvertToString(RSBOL("MAStrailer2").Value)
    CurrentLine = 54
    PrintValues.LoadTime = ConvertToString(RSBOL("LoadTime").Value)
    CurrentLine = 55
    PrintValues.DeliveryTime = ConvertToString(RSBOL("DeliverTime").Value)
    CurrentLine = 56
    PrintValues.LoadTimeIn = ConvertToString(RSBOL("ScaleInTime").Value)
    CurrentLine = 57
    PrintValues.LoadTimeOut = ConvertToString(RSBOL("ScaleOutTime").Value)
    
    CurrentLine = 58
    PrintValues.TimeArrived = Empty
    CurrentLine = 59
    PrintValues.StartUnload = Empty
    CurrentLine = 60
    PrintValues.FinishUnload = Empty

    CurrentLine = 61
    PrintValues.Remarks = ConvertToString(RSBOL("Remarks").Value)
    CurrentLine = 62
    PrintValues.Penetration = ConvertToString(RSBOL("MASpenetration").Value)
    CurrentLine = 63
    PrintValues.PenetrationTemp = ConvertToString(RSBOL("MASpenetrationtemperature").Value)
    CurrentLine = 64
    PrintValues.PenetrationRemark = ConvertToString(RSBOL("MASpenetrationremark").Value)
    CurrentLine = 65
    PrintValues.Flashpoint = ConvertToString(RSBOL("Flashpoint").Value)
    CurrentLine = 66
    PrintValues.LbsPerGal = ConvertToString(RSBOL("lbspergallon2").Value)
    CurrentLine = 67
    PrintValues.SpecGravity = ConvertToString(RSBOL("SpecificGravity").Value)
    CurrentLine = 68
    PrintValues.Viscosity = ConvertToString(RSBOL("Viscosity").Value)
    CurrentLine = 69
    PrintValues.ViscosityTemp = ConvertToString(RSBOL("ViscosityTemp").Value)
    CurrentLine = 70
    PrintValues.ViscosityRemark = ConvertToString(RSBOL("MASviscosityremark").Value)
    CurrentLine = 71
    PrintValues.LotNumber = ConvertToString(RSBOL("LotNbr").Value)
    CurrentLine = 72
    PrintValues.ProductSpecRemarks = ConvertToString(RSBOL("MASspecificationremark").Value)
    CurrentLine = 73
    PrintValues.Residue = ConvertToString(RSBOL("Residue").Value)
    CurrentLine = 74
    PrintValues.CertOfCompliance = ConvertToString(RSBOL("MAScompliance").Value)
    CurrentLine = 75
    PrintValues.AdditivePercent = ConvertToString(RSBOL("MASadditivepercent").Value)
    CurrentLine = 76
    PrintValues.Temp = ConvertToString(RSBOL("LoadTemp").Value)
    CurrentLine = 77
    PrintValues.GrossGal = ConvertToString(RSBOL("GrossGallons").Value)
    CurrentLine = 78
    PrintValues.NetGal = ConvertToString(RSBOL("NetGallons").Value)
    
    CurrentLine = 79
'    If ConvertToString(RSBOL("IsSplitLoad").Value) = "-1" Then
    If Abs(ConvertToDouble(RSBOL("IsSplitLoad").Value)) = 1 Then
        CurrentLine = 80
        If ConvertToString(RSBOL("FrontRearFlag").Value) = "Front" Then
            CurrentLine = 81
            'Front Weights
            PrintValues.GrossWtLbs1 = ConvertToString(RSBOL("FrontGrossWt").Value)
            CurrentLine = 82
            PrintValues.TareWtLbs1 = ConvertToString(RSBOL("FrontTareWt").Value)
            CurrentLine = 83
            PrintValues.NetWtLbs1 = ConvertToString(RSBOL("FrontNetWt").Value)
            'Rear Weights
            CurrentLine = 84
            PrintValues.GrossWtLbs2 = 0
            CurrentLine = 85
            PrintValues.TareWtLbs2 = 0
            CurrentLine = 86
            PrintValues.NetWtLbs2 = 0
            'Total Weights
            CurrentLine = 87
            PrintValues.GrossWtLbs3 = ConvertToString(RSBOL("FrontGrossWt").Value)
            CurrentLine = 88
            PrintValues.TareWtLbs3 = ConvertToString(RSBOL("FrontTareWt").Value)
            CurrentLine = 89
            PrintValues.NetWtLbs3 = ConvertToString(RSBOL("FrontNetWt").Value)
        Else
            CurrentLine = 90
            'Front Weights
            PrintValues.GrossWtLbs1 = 0
            CurrentLine = 91
            PrintValues.TareWtLbs1 = 0
            CurrentLine = 92
            PrintValues.NetWtLbs1 = 0
            'Rear Weights
            CurrentLine = 93
            PrintValues.GrossWtLbs2 = ConvertToString(RSBOL("RearGrossWt").Value)
            CurrentLine = 94
            PrintValues.TareWtLbs2 = ConvertToString(RSBOL("RearTareWt").Value)
            CurrentLine = 95
            PrintValues.NetWtLbs2 = ConvertToString(RSBOL("RearNetWt").Value)
            'Total Weights
            CurrentLine = 96
            PrintValues.GrossWtLbs3 = ConvertToString(RSBOL("RearGrossWt").Value)
            CurrentLine = 97
            PrintValues.TareWtLbs3 = ConvertToString(RSBOL("RearTareWt").Value)
            CurrentLine = 98
            PrintValues.NetWtLbs3 = ConvertToString(RSBOL("RearNetWt").Value)
        End If
        CurrentLine = 99
    Else
        CurrentLine = 100
        'Front Weights
        PrintValues.GrossWtLbs1 = ConvertToString(RSBOL("FrontGrossWt").Value)
        CurrentLine = 101
        PrintValues.TareWtLbs1 = ConvertToString(RSBOL("FrontTareWt").Value)
        CurrentLine = 102
        PrintValues.NetWtLbs1 = ConvertToString(RSBOL("FrontNetWt").Value)
        'Rear Weights
        CurrentLine = 103
        PrintValues.GrossWtLbs2 = ConvertToString(RSBOL("RearGrossWt").Value)
        CurrentLine = 104
        PrintValues.TareWtLbs2 = ConvertToString(RSBOL("RearTareWt").Value)
        CurrentLine = 105
        PrintValues.NetWtLbs2 = ConvertToString(RSBOL("RearNetWt").Value)
        'Total Weights
        CurrentLine = 106
        PrintValues.GrossWtLbs3 = ConvertToString(RSBOL("TotalGrossWt").Value)
        CurrentLine = 107
        PrintValues.TareWtLbs3 = ConvertToString(RSBOL("TotalTareWt").Value)
        CurrentLine = 108
        PrintValues.NetWtLbs3 = ConvertToString(RSBOL("TotalNetWt").Value)
    End If
     
'    PrintValues.GrossWtLbs1 = ConvertToString(RSBOL("FrontGrossWt").Value)
'    PrintValues.TareWtLbs1 = ConvertToString(RSBOL("FrontTareWt").Value)
'    PrintValues.NetWtLbs1 = ConvertToString(RSBOL("FrontNetWt").Value)
    
    CurrentLine = 109
    PrintValues.AdditiveID = ConvertToString(RSBOL("AdditiveID").Value)
    CurrentLine = 110
    PrintValues.AdditiveDesc = ConvertToString(RSBOL("AdditiveDesc").Value)

    CurrentLine = 111
    PrintValues.ItemSpec = Empty
    CurrentLine = 112
    PrintValues.ProductName = Empty
    
'    PrintValues.GrossWtLbs2 = ConvertToString(RSBOL("RearGrossWt").Value)
'    PrintValues.TareWtLbs2 = ConvertToString(RSBOL("RearTareWt").Value)
'    PrintValues.NetWtLbs2 = ConvertToString(RSBOL("RearNetWt").Value)
'    PrintValues.GrossWtLbs3 = ConvertToString(RSBOL("TotalGrossWt").Value)
'    PrintValues.TareWtLbs3 = ConvertToString(RSBOL("TotalTareWt").Value)
'    PrintValues.NetWtLbs3 = ConvertToString(RSBOL("TotalNetWt").Value)
    
    CurrentLine = 113
    PrintValues.Quantity = "1 TK"
    CurrentLine = 114
    PrintValues.HM = "XX"
    
    CurrentLine = 115
    PrintValues.ProductID = ConvertToString(RSBOL("ProductID").Value)
    CurrentLine = 116
    PrintValues.ProductDesc = ConvertToString(RSBOL("ProductDesc").Value)
    
    CurrentLine = 117
    PrintValues.NetTons = ConvertToString(RSBOL("NetTons").Value)
    
    CurrentLine = 118
    PrintValues.SecTapeNbr = ConvertToString(RSBOL("SecurityTapNumber").Value)
    
    'RKL DEJ 2016-09-07 START
    CurrentLine = 200
    PrintValues.BatchNumber = ConvertToString(RSBOL("BatchNumber").Value)
    CurrentLine = 201
    PrintValues.WeightTktNo = ConvertToString(RSBOL("WeightTktNo").Value)
    CurrentLine = 202
    PrintValues.Carrier_ID = ConvertToString(RSBOL("Carrier_ID").Value)
    CurrentLine = 203
    PrintValues.Carrier_Name = ConvertToString(RSBOL("Carrier_Name").Value)
    CurrentLine = 204
    PrintValues.Purchase_Order = ConvertToString(RSBOL("Purchase_Order").Value)
    'RKL DEJ 2016-09-07 STOP
    
    '************************************************************************************************
    'RKL DEJ 2017-12-18 (START) Valero
    '************************************************************************************************
    CurrentLine = 300
    PrintValues.CustOwnedProd = Abs(gbCustOwnedProd)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 301
    PrintValues.AutoPostIMTrans = Abs(gbAutoPostIMTrans)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 302
    PrintValues.NotAllowSplitLoads = Abs(gbNotAllowSplitLoads)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 303
    PrintValues.SrcVariance = gbSrcVariance     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 304
    PrintValues.IMTranReasonCode = gbIMTranReasonCode     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 305
    PrintValues.ForceVarianceCheck = Abs(gbForceVarianceCheck)  'RKL DEJ 2017-12-18 Added For Valero project
    
    CurrentLine = 310
    PrintValues.FrontSrcTankNbrOne = ConvertToString(RSBOLExt("FrontSrcTankNbrOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 311
    PrintValues.FrontSrcTankNbrTwo = ConvertToString(RSBOLExt("FrontSrcTankNbrTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 312
    PrintValues.FrontSrcTankNbrThree = ConvertToString(RSBOLExt("FrontSrcTankNbrThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 313
    PrintValues.FrontSrcQtyOne = ConvertToDouble(RSBOLExt("FrontSrcQtyOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 315
    PrintValues.FrontSrcQtyTwo = ConvertToDouble(RSBOLExt("FrontSrcQtyTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 316
    PrintValues.FrontSrcQtyThree = ConvertToDouble(RSBOLExt("FrontSrcQtyThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 317
    PrintValues.FrontSrcProductIDOne = ConvertToString(RSBOLExt("FrontSrcProductIDOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 318
    PrintValues.FrontSrcProductOne = ConvertToString(RSBOLExt("FrontSrcProductOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 319
    PrintValues.FrontSrcProductClassOne = ConvertToString(RSBOLExt("FrontSrcProductClassOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 320
    PrintValues.FrontSrcProductIDTwo = ConvertToString(RSBOLExt("FrontSrcProductIDTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 321
    PrintValues.FrontSrcProductTwo = ConvertToString(RSBOLExt("FrontSrcProductTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 322
    PrintValues.FrontSrcProductClassTwo = ConvertToString(RSBOLExt("FrontSrcProductClassTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 323
    PrintValues.FrontSrcProductIDThree = ConvertToString(RSBOLExt("FrontSrcProductIDThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 324
    PrintValues.FrontSrcProductThree = ConvertToString(RSBOLExt("FrontSrcProductThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 325
    PrintValues.FrontSrcProductClassThree = ConvertToString(RSBOLExt("FrontSrcProductClassThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 326
    PrintValues.RearSrcTankNbrOne = ConvertToString(RSBOLExt("RearSrcTankNbrOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 327
    PrintValues.RearSrcTankNbrTwo = ConvertToString(RSBOLExt("RearSrcTankNbrTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 328
    PrintValues.RearSrcTankNbrThree = ConvertToString(RSBOLExt("RearSrcTankNbrThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 329
    PrintValues.RearSrcQtyOne = ConvertToDouble(RSBOLExt("RearSrcQtyOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 330
    PrintValues.RearSrcQtyTwo = ConvertToDouble(RSBOLExt("RearSrcQtyTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 331
    PrintValues.RearSrcQtyThree = ConvertToDouble(RSBOLExt("RearSrcQtyThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 332
    PrintValues.RearSrcProductIDOne = ConvertToString(RSBOLExt("RearSrcProductIDOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 333
    PrintValues.RearSrcProductOne = ConvertToString(RSBOLExt("RearSrcProductOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 334
    PrintValues.RearSrcProductClassOne = ConvertToString(RSBOLExt("RearSrcProductClassOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 335
    PrintValues.RearSrcProductIDTwo = ConvertToString(RSBOLExt("RearSrcProductIDTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 336
    PrintValues.RearSrcProductTwo = ConvertToString(RSBOLExt("RearSrcProductTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 337
    PrintValues.RearSrcProductClassTwo = ConvertToString(RSBOLExt("RearSrcProductClassTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 338
    PrintValues.RearSrcProductIDThree = ConvertToString(RSBOLExt("RearSrcProductIDThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 339
    PrintValues.RearSrcProductThree = ConvertToString(RSBOLExt("RearSrcProductThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 340
    PrintValues.RearSrcProductClassThree = ConvertToString(RSBOLExt("RearSrcProductClassThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 341
    PrintValues.SrcTankNbrOne = ConvertToString(RSBOLExt("SrcTankNbrOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 342
    PrintValues.SrcTankNbrTwo = ConvertToString(RSBOLExt("SrcTankNbrTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 343
    PrintValues.SrcTankNbrThree = ConvertToString(RSBOLExt("SrcTankNbrThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 344
    PrintValues.SrcQtyOne = ConvertToDouble(RSBOLExt("SrcQtyOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 345
    PrintValues.SrcQtyTwo = ConvertToDouble(RSBOLExt("SrcQtyTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 346
    PrintValues.SrcQtyThree = ConvertToDouble(RSBOLExt("SrcQtyThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 347
    PrintValues.SrcProductIDOne = ConvertToString(RSBOLExt("SrcProductIDOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 348
    PrintValues.SrcProductOne = ConvertToString(RSBOLExt("SrcProductOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 349
    PrintValues.SrcProductClassOne = ConvertToString(RSBOLExt("SrcProductClassOne").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 350
    PrintValues.SrcProductIDTwo = ConvertToString(RSBOLExt("SrcProductIDTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 351
    PrintValues.SrcProductTwo = ConvertToString(RSBOLExt("SrcProductTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 352
    PrintValues.SrcProductClassTwo = ConvertToString(RSBOLExt("SrcProductClassTwo").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 353
    PrintValues.SrcProductIDThree = ConvertToString(RSBOLExt("SrcProductIDThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 354
    PrintValues.SrcProductThree = ConvertToString(RSBOLExt("SrcProductThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 355
    PrintValues.SrcProductClassThree = ConvertToString(RSBOLExt("SrcProductClassThree").Value)     'RKL DEJ 2017-12-18 Added For Valero project
    
    CurrentLine = 356
    If PrintValues.IsCreditReturn = True Then
    CurrentLine = 357
        PrintValues.TranType = 702     'RKL DEJ 2017-12-18 Added For Valero project
    Else
    CurrentLine = 358
        PrintValues.TranType = 701     'RKL DEJ 2017-12-18 Added For Valero project
    End If
    '************************************************************************************************
    'RKL DEJ 2017-12-18 (STOP) Valero
    '************************************************************************************************
    
    CurrentLine = 119
    GetPrintValues = True
    
    CurrentLine = 120
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = Me.Name & ".GetPrintValues(ByRef PrintValues As BOLFields) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
        
    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, lErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, lErrMsg
    End If
    
    If SuppressMsg = False Then
        MsgBox lErrMsg, vbCritical, "Error"
    End If
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function



Function ProcessBOL(Optional ErrMsg As String = Empty) As Boolean
    On Error GoTo Error
    
    Dim PrintValues As BOLFields
    Dim PrintStr As String
    Dim NewKey As Double
    Dim lcErrMsg As String
    Dim CurrentLine As Integer
    
    Dim bIsHighRisk As Boolean      'RKL DEJ
    Dim bCertRequired As Boolean    'RKL DEJ
    Dim TotalGrossWt As Double      'RKL DEJ

    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    'Default Failer
    ProcessBOL = False
    
    CurrentLine = 1
    'Validate the data localy
    lblStatus.Caption = "Validating Data Locally"
    CurrentLine = 2
    If ValidateLocal(ErrMsg) = False Then
        If ErrMsg <> Empty Then
            ErrMsg = "Failed Local Validation" & vbCrLf & ErrMsg
'            MsgBox ErrMsg, vbInformation, "Warning"
        End If
        CurrentLine = 3
        Exit Function
    End If
    
    CurrentLine = 4
    'Get the Print Values
    lblStatus.Caption = "Getting the Print Values"
    DoEvents
    CurrentLine = 5
    If GetPrintValues(PrintValues) = False Then
        CurrentLine = 6
'        MsgBox "Could not obtain the vaules to print.", vbExclamation, "Print Error"
        ErrMsg = "Could not obtain the vaules to print."
        Exit Function
    End If

    CurrentLine = 7
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 8
        If HaveLostMASConnection = True Then
            CurrentLine = 9
            'No longer connected to MAS 500
        End If
    End If
    
    CurrentLine = 10
    'Validate The Data
    If gbLocalMode = False Then
        CurrentLine = 11
        lblStatus.Caption = "Validating the BOL and Sales Order data."
        DoEvents
        CurrentLine = 12
        If ValidateData(PrintValues, ErrMsg) = False Then
            'Validation Failed
            CurrentLine = 13
'            MsgBox "The Sales Order was not created.", vbExclamation, "Sales Order Failed"
            ErrMsg = "Failed Server Validation" & vbCrLf & ErrMsg
            Exit Function
        Else
            If ErrMsg <> Empty Then
                MsgBox ErrMsg, vbInformation, "Server Validation Success"
                ErrMsg = Empty
            End If
        End If
    Else
        CurrentLine = 14
        
        If MsgBox("You are not connected to MAS 500.  The MAS 500 Validation will not take place." & vbCrLf & vbCrLf & _
        "Do you want to continue with the print?" & vbCrLf & vbCrLf & _
        "If you continue no data will be passed back to MAS 500 (including the Sales Order) at this time.  The data will " & _
        "be passed next time Scale Pass is started and there is a valid connection to MAS 500.", vbYesNo + vbExclamation, "No MAS Validation") <> vbYes Then
            CurrentLine = 15
            ErrMsg = Empty
            Exit Function
        End If
    End If
    
    CurrentLine = 16
    
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 17
        If HaveLostMASConnection = True Then
            CurrentLine = 18
            'No longer connected to MAS 500
        End If
    End If
    
    CurrentLine = 19
    'Create / keep a log of printing BOL in Scale Pass (Only If Connected to MAS)
    If gbLocalMode = False And DebugOnly = False Then
        CurrentLine = 20
        lblStatus.Caption = "Creating a BOL log record in MAS 500"
        DoEvents
        CurrentLine = 21
        If InstsoBOLTblLog(PrintValues, NewKey) = False Then
            CurrentLine = 22

            ErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "Could not create a log record in MAS 500 for this print." & vbCrLf & "Do you want to continue with the print?" & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
            CurrentLine = 23
            If RSBOL.State = 1 Then
                CurrentLine = 24
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 25
                    InsErrorLog 0, Date + Time, ErrMsg
                Else
                    CurrentLine = 26
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                End If
            Else
                CurrentLine = 27
                InsErrorLog 0, Date + Time, ErrMsg
            End If
                        
            CurrentLine = 28
            'Could not create Log record
            If MsgBox("Could not create a log record in MAS 500 for this print." & vbCrLf & vbCrLf & _
            "Do you want to continue with the print?", vbYesNo & vbExclamation, "Scale Pass") <> vbYes Then
                CurrentLine = 29
                MsgBox "Print has been canceled", vbInformation, "Print Canceled"
                ErrMsg = Empty
                Exit Function
            End If
            
            CurrentLine = 30

        Else
            CurrentLine = 31
            'Set the Flag that the BOL Log has been created in MAS 500
            RSBOL("HasMASBOLLogBeenCreated").Value = 1
            CurrentLine = 32
'            RSBOL.UpdateBatch
            If UpdateRS(RSBOL) = False Then
                CurrentLine = 33
                lcErrMsg = Me.Name & "ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                
                CurrentLine = 34
                MsgBox lcErrMsg, vbInformation, "Warning"
                
                CurrentLine = 35
                If RSBOL.State = 1 Then
                    CurrentLine = 36
                    If RSBOL.EOF Or RSBOL.BOF Then
                        CurrentLine = 37
                        InsErrorLog 0, Date + Time, lcErrMsg
                    Else
                        CurrentLine = 38
                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                    End If
                Else
                    CurrentLine = 39
                    InsErrorLog 0, Date + Time, lcErrMsg
                End If
            End If
        End If
    End If
    
    CurrentLine = 40
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 41
        If HaveLostMASConnection = True Then
            CurrentLine = 42
            'No longer connected to MAS 500
        End If
    End If
    
    
    CurrentLine = 43
    If gbPlantPrefix = 7 Then 'Rawlins (Plant 7) requires a Weight Ticket for each load
        CurrentLine = 44
        'Print the Weight Ticket
        lblStatus.Caption = "Printing the Weight Ticket"
        DoEvents
        CurrentLine = 45
        If PrintWtTicket() = False Then
            CurrentLine = 46
'            MsgBox "Weight Ticket Failed to Print.", vbExclamation, "Scale Pass"
            ErrMsg = "Weight Ticket Failed to Print."
            Exit Function
        End If
    End If

    CurrentLine = 47
    'Print the BOL
    lblStatus.Caption = "Printing the BOL"
    DoEvents
'    If PrintBL(PrintValues, PrintStr, DebugOnly) = False Then
'        'Update the printing BOL Log
'        If gbLocalMode = False And DebugOnly = False Then
'            If UpdBOLTblLog(NewKey, False) = False Then
'                'Could not update the BOL Log Records with Success or Failed Print.
'                MsgBox "Could not update the BOL Log Records with Success or Failed Print.", vbExclamation, "Scale Pass"
'
'                ErrMsg = Me.Name & "ProcessBOL()" & vbCrLf & "Could not update the BOL Log Records with Success or Failed Print."
'                If RSBOL.State = 1 Then
'                    If RSBOL.EOF Or RSBOL.BOF Then
'                        InsErrorLog 0, Date + Time, ErrMsg
'                    Else
'                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
'                    End If
'                Else
'                    InsErrorLog 0, Date + Time, ErrMsg
'                End If
'            End If
'        End If
'
'        Exit Function
'    End If
    
    CurrentLine = 4800
    If Abs(ConvertToDouble(RSBOL("COARequired").Value)) > 0 Then
    CurrentLine = 4810
        bCertRequired = True
    Else
    CurrentLine = 4820
        bCertRequired = False
    End If
    
    CurrentLine = 4830
    If chAltBOLPrinter.Value = vbChecked Then
        PrintCrystalBOL False, 0, RSBOL("BOLTableKey").Value, gbAltBOLPrinter, Empty, True
    Else
        PrintCrystalBOL False, 0, RSBOL("BOLTableKey").Value, Empty, Empty, True
    End If

'    PrintCrystalBOL False, 0, RSBOL("BOLTableKey").Value, Empty, Empty, True, bCertRequired
    
'    If DebugOnly = True Then
'        frmPrintPreview.rtfText.Text = PrintStr
'        frmPrintPreview.Show vbModal, Me
'        Exit Function
'    End If
    
    CurrentLine = 49
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 50
        If HaveLostMASConnection = True Then
            CurrentLine = 51
            'No longer connected to MAS 500
        End If
    End If
    
    CurrentLine = 52
    lblStatus.Caption = "Did the BOL Print Correctly?"
    DoEvents
    CurrentLine = 53
    If MsgBox("Did the BOL Print Correctly?", vbYesNo, "BOL Printed Correctly?") = vbYes Then
        CurrentLine = 54
        
        'RKL DEJ 1/20/14 (START)
        Select Case UCase(Trim(GetMASShipToState(Trim(txtContract.Text), Trim(RSBOL("ProductID").Value))))
            Case "NV"
                CurrentLine = 5400
                'Placeholder DEJ
                TotalGrossWt = ConvertToDouble(frmLoadProcessing.txtTotalGrossWt.Text)
                
                CurrentLine = 5500
                If Trim(txtTrailer2.Text) <> Empty Then
                    CurrentLine = 5600
                    'There is a pup
                    'May need Nevada Permit
                    MsgBox "This load is going to Nevada and has both a front and rear compartment. Does it need a Nevada Permit?", vbInformation, "Need Nevada Permit"

                ElseIf TotalGrossWt >= 80000 Then
                    CurrentLine = 5700
                    'May need Nevada Permit
                    MsgBox "This load is going to Nevada and weighs over 80,000 lbs. Does it need a Nevada permit?", vbInformation, "Need Nevada Permit"
                End If

                CurrentLine = 5800
'                'RKL DEJ 1/24/14 Kevin said that for now we don't need to worry about the FOB Logic.
'                Select Case UCase(Trim(GetMASFOB(Trim(txtContract.Text), Trim(RSBOL("ProductID").Value))))
'                    Case "JOB SITE"
'                        MsgBox "Don't forget the Nevada permit.  Any loads that are delivered into Nevada need a permit.", vbInformation, "Need Nevada Permit"
'                        CurrentLine = 5401
'                    Else
'                        'Do Nothing
'                        CurrentLine = 5402
'                End Select
                
            Case Else
                'Do Nothing
                CurrentLine = 5403
        End Select
        'RKL DEJ 1/20/14 (STOP)
        
        'Set the Flag that the BOL printed Successfuly
        RSBOL("HasPrintedSuccessfully").Value = 1
        CurrentLine = 55
'        RSBOL.UpdateBatch
        If UpdateRS(RSBOL) = False Then
            CurrentLine = 56
            lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
            CurrentLine = 57
            MsgBox lcErrMsg, vbInformation, "Warning"
        
            CurrentLine = 58
            If RSBOL.State = 1 Then
                CurrentLine = 59
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 60
                    InsErrorLog 0, Date + Time, lcErrMsg
                Else
                    CurrentLine = 61
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                End If
            Else
                CurrentLine = 62
                InsErrorLog 0, Date + Time, lcErrMsg
            End If
        End If
        
        CurrentLine = 63
        'Update the printing BOL Log
        If gbLocalMode = False Then
            CurrentLine = 64
            If UpdBOLTblLog(NewKey, True) = False Then
                CurrentLine = 65
                'Could not update the BOL Log Records with Success or Failed Print.
                lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "Could not update the BOL Log Records with Success or Failed Print." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
            
                CurrentLine = 66
                MsgBox lcErrMsg, vbExclamation, "Scale Pass"
                
                CurrentLine = 67
                If RSBOL.State = 1 Then
                    CurrentLine = 68
                    If RSBOL.EOF Or RSBOL.BOF Then
                        CurrentLine = 69
                        InsErrorLog 0, Date + Time, lcErrMsg
                    Else
                        CurrentLine = 70
                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                    End If
                Else
                    CurrentLine = 71
                    InsErrorLog 0, Date + Time, lcErrMsg
                End If
            End If
            CurrentLine = 72
            
        End If
        CurrentLine = 73
        
        'SGS DEJ 4/19/11 added Cypress print
        If gbPrintBOLCypress = True Then
            'Only print to Cypress when:
            '1) The setup checkbox "print to Cypress" has been checked
            '2) After the BOL has printed Successfully
            
            Call PrintCypressBOL(False, RSBOL("BOLTableKey").Value, Empty)
        End If
        
        'RKL DEJ - Print extra copy of BOL after it has been saved to the database. (START)
        If gbPrintExtraBOL = True Then
            If gbPrintExtraBOLCOAOnly = False Or (gbPrintExtraBOLCOAOnly = True And bCertRequired = True) Then
                PrintCrystalBOL True, 1, RSBOL("BOLTableKey").Value, gbExtraBOLPrinter, "Plant Copy", False
            End If
        End If
        'RKL DEJ - Print extra copy of BOL after it has been saved to the database. (STOP)
        
    Else
        CurrentLine = 74
        'Update the printing BOL Log
        If gbLocalMode = False Then
            CurrentLine = 75
            If UpdBOLTblLog(NewKey, False) = False Then
                CurrentLine = 76
                'Could not update the BOL Log Records with Success or Failed Print.
                lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "Could not update the BOL Log Records with Success or Failed Print." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                
                CurrentLine = 77
                MsgBox lcErrMsg, vbExclamation, "Scale Pass"
                CurrentLine = 78
                If RSBOL.State = 1 Then
                    CurrentLine = 79
                    If RSBOL.EOF Or RSBOL.BOF Then
                        CurrentLine = 80
                        InsErrorLog 0, Date + Time, lcErrMsg
                    Else
                        CurrentLine = 81
                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                    End If
                Else
                    CurrentLine = 82
                    InsErrorLog 0, Date + Time, lcErrMsg
                End If
            End If
        End If
        CurrentLine = 83
        
        ErrMsg = Empty
        Exit Function
    End If
    
    CurrentLine = 84
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 85
        If HaveLostMASConnection = True Then
            CurrentLine = 86
            'No longer connected to MAS 500
        End If
    End If
    
    CurrentLine = 87
    'Call the SO API and send BOLPrinting record to MAS
    If gbLocalMode = False Then
        
        CurrentLine = 88
        If PrintValues.IsCreditReturn = True Then
            CurrentLine = 89
            lblStatus.Caption = "Creating a Credit Return in MAS 500"
            DoEvents
            
            CurrentLine = 90
            If RunCreditRtnAPI(PrintValues) = False Then
                CurrentLine = 91
    '            MsgBox "The Return Order was not created.", vbExclamation, "Return Order Failed"
                '********************************************************************
                '********************************************************************
                'RKL DEJ 2016-09-08 (START)
                '********************************************************************
                '********************************************************************
                ProcessBOL = False
                RSBOL("HasPrintedSuccessfully").Value = 0
                CurrentLine = 9101
                If UpdateRS(RSBOL) = False Then
                
                    CurrentLine = 9102
                    lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                    CurrentLine = 9103
                    MsgBox lcErrMsg, vbInformation, "Warning"
                    
                    CurrentLine = 91004
                    If RSBOL.State = 1 Then
                        CurrentLine = 9105
                        If RSBOL.EOF Or RSBOL.BOF Then
                            CurrentLine = 9106
                            InsErrorLog 0, Date + Time, lcErrMsg
                        Else
                            CurrentLine = 9107
                            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                        End If
                    Else
                        CurrentLine = 9108
                        InsErrorLog 0, Date + Time, lcErrMsg
                    End If
                End If
                
                ErrMsg = "Create Return Order in MAS 500 Failed (RunCreditRtnAPI)" & vbCrLf & ErrMsg
                CurrentLine = 9109
                Exit Function
                '********************************************************************
                '********************************************************************
                'RKL DEJ 2016-09-08 (STOP)
                '********************************************************************
                '********************************************************************
    
            Else
                CurrentLine = 92
                'Set the Flag that MAS Sales Order has been created Successfuly
                RSBOL("HasMASSOBeenCreated").Value = 1
                CurrentLine = 93
                If UpdateRS(RSBOL) = False Then
                    CurrentLine = 94
                    lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                
                    CurrentLine = 95
                    MsgBox lcErrMsg, vbInformation, "Warning"
                    CurrentLine = 96
                    If RSBOL.State = 1 Then
                        CurrentLine = 97
                        If RSBOL.EOF Or RSBOL.BOF Then
                            CurrentLine = 98
                            InsErrorLog 0, Date + Time, lcErrMsg
                        Else
                            CurrentLine = 99
                            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                        End If
                    Else
                        CurrentLine = 100
                        InsErrorLog 0, Date + Time, lcErrMsg
                    End If
                End If
                
                CurrentLine = 101
                
            End If
            CurrentLine = 102
            
        Else
            CurrentLine = 103
            lblStatus.Caption = "Creating a Sales Order in MAS 500"
            DoEvents
            
            CurrentLine = 104
            If RunAPI(PrintValues, False, ErrMsg) = False Then
                CurrentLine = 105
    '            MsgBox "The Sales Order was not created.", vbExclamation, "Sales Order Failed"
                '*********************************************
                'RKL DEJ 7/15/14 Added this logic (START)
                '*********************************************
                'If the RunAPI fails it means the order was not created we will need to reprocess the Order. and reprint the BOL
                'To reprocess the order we need to set the HasPrintedSuccessfully flag back to 0
                ProcessBOL = False
                RSBOL("HasPrintedSuccessfully").Value = 0
                CurrentLine = 1051
                If UpdateRS(RSBOL) = False Then
                
                    CurrentLine = 1052
                    lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                    CurrentLine = 1053
                    MsgBox lcErrMsg, vbInformation, "Warning"
                    
                    CurrentLine = 1054
                    If RSBOL.State = 1 Then
                        CurrentLine = 1055
                        If RSBOL.EOF Or RSBOL.BOF Then
                            CurrentLine = 1056
                            InsErrorLog 0, Date + Time, lcErrMsg
                        Else
                            CurrentLine = 1057
                            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                        End If
                    Else
                        CurrentLine = 1058
                        InsErrorLog 0, Date + Time, lcErrMsg
                    End If
                End If
                
                ErrMsg = "Create Sales Order in MAS 500 Failed (RunAPI)" & vbCrLf & ErrMsg
                CurrentLine = 1059
                Exit Function
                '*********************************************
                'RKL DEJ 7/15/14 Added this logic (STOP)
                '*********************************************
            Else
                CurrentLine = 106
                'Set the Flag that MAS Sales Order has been created Successfuly
                RSBOL("HasMASSOBeenCreated").Value = 1
                CurrentLine = 107
                If UpdateRS(RSBOL) = False Then
                    CurrentLine = 108
                    lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                
                    CurrentLine = 109
                    MsgBox lcErrMsg, vbInformation, "Warning"
                    CurrentLine = 110
                    If RSBOL.State = 1 Then
                        CurrentLine = 111
                        If RSBOL.EOF Or RSBOL.BOF Then
                            CurrentLine = 112
                            InsErrorLog 0, Date + Time, lcErrMsg
                        Else
                            CurrentLine = 113
                            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                        End If
                    Else
                        CurrentLine = 114
                        InsErrorLog 0, Date + Time, lcErrMsg
                    End If
                End If
                
                CurrentLine = 115
            End If
            CurrentLine = 116
        End If
        
        CurrentLine = 117
        'Send the BOL Printing Record to MAS
        lblStatus.Caption = "Copying the Scale Pass data to MAS 500"
        DoEvents
        CurrentLine = 118
        If CreateMASBOLPrinting() = True Then
            CurrentLine = 119
            'SGS DEJ 7/2/12 (Print COA)
            'RKL DEJ 5/30/14 (Changed so COA can print enven if the EnforceCOA is not selected
            If RSBOL("PrintCOA").Value = 1 And Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) <> 1 Then
'            If RSBOL("PrintCOA").Value = 1 And gbEnforceCOA = True And Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) <> 1 Then
                If Trim(txtLotNr.Text) <> Empty Then
                    Call PrintCOA(False, Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value), ConvertToString(RSBOL("BOLNBR").Value), 0, Empty)
                    If gbPrintCOACypress = True Then
                        Call PrintCypressCOA(False, Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value), ConvertToString(RSBOL("BOLNBR").Value))
                    End If
                End If
            End If
            
'****************************************************************
'RKL DEJ 9/25/13 (START) - Print Lables
'****************************************************************
            If gbPrintLabels = True And gbPrintLabelAfterBOL = True And Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) <> 1 Then
                If Abs(ConvertToDouble(RSBOL("COARequired").Value)) > 0 Then
                    bCertRequired = True
                Else
                    bCertRequired = False
                End If
                
                If Abs(ConvertToDouble(RSBOL("HighRisk").Value)) > 0 Then
                    bIsHighRisk = True
                Else
                    bIsHighRisk = False
                End If
                    
                If PrintValues.IsTransfer = False Then     'RKL DEJ 5/22/14 Kevin asked not to print for transfers
                    'Print the Label
                    If basMethods.PrintLabel(RSBOL("LoadsTableKey").Value, RSBOL("FrontRearFlag").Value, bIsHighRisk, bCertRequired, False, Empty, 0, True) = False Then
                        MsgBox "There was an error printing the Label", vbInformation, "Bad Label Print"
                    End If
                End If
            End If
'****************************************************************
'RKL DEJ 9/25/13 (STOP) - Print Lables
'****************************************************************
            
            'Set the Flag that the BOL record has been sent to MAS 500
            RSBOL("HasBeenSentToMAS").Value = 1
            CurrentLine = 120
'            RSBOL.UpdateBatch
            If UpdateRS(RSBOL) = False Then
                CurrentLine = 121
                lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
            
                CurrentLine = 122
                MsgBox lcErrMsg, vbInformation, "Warning"
                CurrentLine = 123
                If RSBOL.State = 1 Then
                    CurrentLine = 124
                    If RSBOL.EOF Or RSBOL.BOF Then
                        CurrentLine = 125
                        InsErrorLog 0, Date + Time, lcErrMsg
                    Else
                        CurrentLine = 126
                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                    End If
                Else
                    CurrentLine = 127
                    InsErrorLog 0, Date + Time, lcErrMsg
                End If
            End If
        Else
            'CreateMASBOLPrinting() Failed
            'SGS DEJ 7/2/12 (Print COA)
            CurrentLine = 10125
            If RSBOL("PrintCOA").Value = 1 Then
                CurrentLine = 10126
                If Trim(txtLotNr.Text) <> Empty Then
                    CurrentLine = 10127
                    'Cannot print the COA because the data did not get posted to MAS 500
                    lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "The COA cannot be printed because the BOL data was not written to MAS 500." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                    CurrentLine = 10128
                    InsErrorLog 0, Date + Time, lcErrMsg
                    
                    CurrentLine = 10129
                    MsgBox lcErrMsg, vbExclamation, "COA Can Not Print"
                                        
                End If
            End If
            
        End If
    End If
    
    CurrentLine = 128
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 129
        If HaveLostMASConnection = True Then
            CurrentLine = 130
            'No longer connected to MAS 500
        End If
    End If
        
    CurrentLine = 131
    '7/21/09... If Connection was not lost and we made it to here then set all status to 1 (Processed)
    'The only time Kevin wants to update MAS on Startup is if the network went down.  If the SO failed they
    'will manually create the SO...
    If gbLocalMode = False Then
        CurrentLine = 132
        RSBOL("HasMASBOLLogBeenCreated").Value = 1
        CurrentLine = 133
        RSBOL("HasMASSOBeenCreated").Value = 1
        CurrentLine = 134
        RSBOL("HasBeenSentToMAS").Value = 1
        
        CurrentLine = 135
        If UpdateRS(RSBOL) = False Then
            CurrentLine = 136
            lcErrMsg = Me.Name & ".ProcessBOL()" & vbCrLf & "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
        
            CurrentLine = 137
            MsgBox lcErrMsg, vbInformation, "Warning"
            CurrentLine = 138
            If RSBOL.State = 1 Then
                CurrentLine = 139
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 140
                    InsErrorLog 0, Date + Time, lcErrMsg
                Else
                    CurrentLine = 141
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                End If
            Else
                CurrentLine = 142
                InsErrorLog 0, Date + Time, lcErrMsg
            End If
        End If
    End If
    
    CurrentLine = 143
    'Successfully Ran though Processing
    ProcessBOL = True
    
    CurrentLine = 144
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lcErrMsg = Me.Name & ".ProcessBOL() As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    MsgBox lcErrMsg, vbCritical, "Error"
    
    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, lcErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, lcErrMsg
    End If
    
    ErrMsg = lcErrMsg
    
    gbLastActivityTime = Date + Time
End Function


'This is used to pass data back to MAS 500... this will only need to be called
'if the connection to MAS 500 was down at the time the BOL was printed.  This
'will be called at the time Scale Pass starts.
Function ReProcessBOL() As Boolean
    On Error GoTo Error
    
    Dim PrintValues As BOLFields
    Dim PrintStr As String
    Dim NewKey As Double
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    'Default Failure
    ReProcessBOL = False
    
    CurrentLine = 1
    'Validate MAS Connection
    If gbLocalMode = True Then
        CurrentLine = 2
        Exit Function
    Else
        CurrentLine = 3
        If HaveLostMASConnection = True Then
            CurrentLine = 4
            Exit Function
        End If
    End If
    
    CurrentLine = 5
    
    'Make sure the BOL record is available
    If RSBOL.EOF Or RSBOL.BOF Then
        CurrentLine = 6
        Exit Function
    End If
    
    CurrentLine = 7
    'Make sure the Header record is available
    If rsLoad.EOF Or rsLoad.BOF Then
        CurrentLine = 8
        Exit Function
    End If
        
    CurrentLine = 9
    'Get the Print Values
    If GetPrintValues(PrintValues, True) = False Then
        CurrentLine = 10
        Exit Function
    End If
        
    CurrentLine = 11
    'Validate MAS Connection
    If gbLocalMode = True Then
        CurrentLine = 12
        Exit Function
    Else
        CurrentLine = 13
        If HaveLostMASConnection = True Then
            CurrentLine = 14
            Exit Function
        End If
    End If
    
    CurrentLine = 15
    'Create / keep a log of printing BOL in Scale Pass (Only If Connected to MAS)
    If ConvertToDouble(RSBOL("HasMASBOLLogBeenCreated").Value) <> 1 Then
        CurrentLine = 16
        If InstsoBOLTblLog(PrintValues, NewKey, True) = True Then
            CurrentLine = 17
            'Set the Flag that the BOL Log has been created in MAS 500
            RSBOL("HasMASBOLLogBeenCreated").Value = 1
'            RSBOL.UpdateBatch
            CurrentLine = 18
            If UpdateRS(RSBOL) = False Then
                CurrentLine = 19
                MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
                
                CurrentLine = 20
                ErrMsg = "There was an error saving the data.  If this persists contact your IT support."
                CurrentLine = 21
                If RSBOL.State = 1 Then
                    CurrentLine = 22
                    If RSBOL.EOF Or RSBOL.BOF Then
                        CurrentLine = 23
                        InsErrorLog 0, Date + Time, ErrMsg
                    Else
                        CurrentLine = 24
                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                    End If
                Else
                    CurrentLine = 25
                    InsErrorLog 0, Date + Time, ErrMsg
                End If
            End If
        
            CurrentLine = 26
            If UpdBOLTblLog(NewKey, True, True) = False Then
                'Could not update the BOL Log Records with Success or Failed Print.
                
                CurrentLine = 27
                ErrMsg = "Could not update the BOL Log Records with Success or Failed Print."
                CurrentLine = 28
                If RSBOL.State = 1 Then
                    CurrentLine = 29
                    If RSBOL.EOF Or RSBOL.BOF Then
                        CurrentLine = 30
                        InsErrorLog 0, Date + Time, ErrMsg
                    Else
                        CurrentLine = 31
                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                    End If
                Else
                    CurrentLine = 32
                    InsErrorLog 0, Date + Time, ErrMsg
                End If
            End If
        End If
    End If
    
    CurrentLine = 33
    
    'Validate MAS Connection
    If gbLocalMode = True Then
        CurrentLine = 34
        Exit Function
    Else
        CurrentLine = 35
        If HaveLostMASConnection = True Then
            CurrentLine = 36
            Exit Function
        End If
    End If
    
    CurrentLine = 37
    'Call the SO API
    If ConvertToDouble(RSBOL("HasMASSOBeenCreated").Value) <> 1 Then
        
        CurrentLine = 38
        If PrintValues.IsCreditReturn = True Then
            CurrentLine = 39
            lblStatus.Caption = "Creating a Credit Return in MAS 500"
            DoEvents
            
            CurrentLine = 40
            If RunCreditRtnAPI(PrintValues, True) = True Then
                CurrentLine = 41
                'Set the Flag that MAS Sales Order has been created Successfuly
                RSBOL("HasMASSOBeenCreated").Value = 1
                CurrentLine = 42
                If UpdateRS(RSBOL) = False Then
                    CurrentLine = 43
                    MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
                
                    CurrentLine = 44
                    ErrMsg = "There was an error saving the data.  If this persists contact your IT support."
                    CurrentLine = 45
                    If RSBOL.State = 1 Then
                        CurrentLine = 46
                        If RSBOL.EOF Or RSBOL.BOF Then
                            CurrentLine = 47
                            InsErrorLog 0, Date + Time, ErrMsg
                        Else
                            CurrentLine = 48
                            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                        End If
                    Else
                        CurrentLine = 49
                        InsErrorLog 0, Date + Time, ErrMsg
                    End If
                End If
            End If
        Else
            CurrentLine = 50
            lblStatus.Caption = "Creating a Sales Order in MAS 500"
            DoEvents
            
            CurrentLine = 51
            If RunAPI(PrintValues, True) = True Then
                CurrentLine = 52
                'Set the Flag that MAS Sales Order has been created Successfuly
                RSBOL("HasMASSOBeenCreated").Value = 1
                CurrentLine = 53
                If UpdateRS(RSBOL) = False Then
                    CurrentLine = 54
                    MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
                
                    CurrentLine = 55
                    ErrMsg = "There was an error saving the data.  If this persists contact your IT support."
                    CurrentLine = 56
                    If RSBOL.State = 1 Then
                        CurrentLine = 57
                        If RSBOL.EOF Or RSBOL.BOF Then
                            CurrentLine = 58
                            InsErrorLog 0, Date + Time, ErrMsg
                        Else
                            CurrentLine = 59
                            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                        End If
                    Else
                        CurrentLine = 60
                        InsErrorLog 0, Date + Time, ErrMsg
                    End If
                End If
            End If
        End If
        
        CurrentLine = 61
        
        
    End If
    
    CurrentLine = 62
    'Validate MAS Connection
    If gbLocalMode = True Then
        CurrentLine = 63
        Exit Function
    Else
        CurrentLine = 64
        If HaveLostMASConnection = True Then
            CurrentLine = 65
            Exit Function
        End If
    End If
    
    CurrentLine = 66
    'Send the BOL Printing Record to MAS
    If ConvertToDouble(RSBOL("HasBeenSentToMAS").Value) <> 1 Then
        CurrentLine = 67
        If CreateMASBOLPrinting(True) = True Then
            CurrentLine = 68
            'Set the Flag that the BOL record has been sent to MAS 500
            RSBOL("HasBeenSentToMAS").Value = 1
'            RSBOL.UpdateBatch
            CurrentLine = 69
            If UpdateRS(RSBOL) = False Then
                CurrentLine = 70
                MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
            
                CurrentLine = 71
                ErrMsg = "There was an error saving the data.  If this persists contact your IT support."
                CurrentLine = 72
                If RSBOL.State = 1 Then
                    CurrentLine = 73
                    If RSBOL.EOF Or RSBOL.BOF Then
                        CurrentLine = 74
                        InsErrorLog 0, Date + Time, ErrMsg
                    Else
                        CurrentLine = 75
                        InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                    End If
                Else
                    CurrentLine = 76
                    InsErrorLog 0, Date + Time, ErrMsg
                End If
            End If
        End If
    End If
    
    CurrentLine = 77
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 78
        If HaveLostMASConnection = True Then
            CurrentLine = 79
            'No longer connected to MAS 500
        End If
    End If
        
    CurrentLine = 80
    '7/21/09... If Connection was not lost and we made it to here then set all status to 1 (Processed)
    'The only time Kevin wants to update MAS on Startup is if the network went down.
    If gbLocalMode = False Then
        CurrentLine = 81
        If ConvertToDouble(RSBOL("HasMASSOBeenCreated").Value) <> 1 Then
            CurrentLine = 82
            'Notify the user that they will need to manually create the SO.
            MsgBox "There was an error creating the SO in MAS 500 for BOL-No: " & ConvertToString(RSBOL("BOLNBR").Value) & " You will need to manualy create this order.", vbExclamation, "Scale Pass to MAS 500 Warning"
        
            CurrentLine = 83
            ErrMsg = "There was an error creating the SO in MAS 500 for BOL-No: " & ConvertToString(RSBOL("BOLNBR").Value) & " You will need to manualy create this order."
            CurrentLine = 84
            If RSBOL.State = 1 Then
                CurrentLine = 85
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 86
                    InsErrorLog 0, Date + Time, ErrMsg
                Else
                    CurrentLine = 87
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                End If
            Else
                CurrentLine = 88
                InsErrorLog 0, Date + Time, ErrMsg
            End If
        End If
        
        CurrentLine = 89
        RSBOL("HasMASBOLLogBeenCreated").Value = 1
        CurrentLine = 90
        RSBOL("HasMASSOBeenCreated").Value = 1
        CurrentLine = 91
        RSBOL("HasBeenSentToMAS").Value = 1
        
        CurrentLine = 92
        If UpdateRS(RSBOL) = False Then
            CurrentLine = 93
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        
            CurrentLine = 94
            ErrMsg = "There was an error saving the data.  If this persists contact your IT support."
            CurrentLine = 95
            If RSBOL.State = 1 Then
                CurrentLine = 96
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 97
                    InsErrorLog 0, Date + Time, ErrMsg
                Else
                    CurrentLine = 98
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                End If
            Else
                CurrentLine = 99
                InsErrorLog 0, Date + Time, ErrMsg
            End If
        End If
    End If
    
    CurrentLine = 100
    'Successfully Ran though Processing
    ReProcessBOL = True
    
    CurrentLine = 101
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    ErrMsg = Me.Name & ".ReProcessBOL() As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
'    MsgBox Me.Name & ".ReProcessBOL() As Boolean" & vbCrLf & _
'    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    MsgBox ErrMsg, vbCritical, "Error"
    
    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, ErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, ErrMsg
    End If
    
    gbLastActivityTime = Date + Time
End Function



Public Property Let UpdateMASOnly(Val As Boolean)
    On Error Resume Next
        
    lcUpdateOnly = Val
    Err.Clear

    gbLastActivityTime = Date + Time
End Property

Public Property Get UpdateMASOnly() As Boolean
    On Error Resume Next
    UpdateMASOnly = lcUpdateOnly
    Err.Clear

    gbLastActivityTime = Date + Time
End Property


Private Function ValidateData(PrintValues As BOLFields, Optional ErrMsg As String = Empty) As Boolean
    Err.Clear
    On Error GoTo Error
    Dim CMD As New ADODB.Command

    Dim CreateStatus As Integer
    Dim CreateMessage As String
    Dim ErrorColumn As String
    Dim BOLNo As String
    Dim lcErrMsg As String
    
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    ValidateData = False
    
    CurrentLine = 1
    
    'Validate MAS Connection
    If gbLocalMode = False Then
        If HaveLostMASConnection = True Then
            'No longer connected to MAS 500
            ErrMsg = "The connection to MAS has been lost."
            Exit Function
        End If
    End If
    
    CurrentLine = 2
    
    If gbMASConn.State <> 1 Then
'        MsgBox "The connection to MAS has been lost.", vbExclamation, "Server Validation Failed"
        ErrMsg = "The connection to MAS has been lost."
        Exit Function
    End If
    
'    gbMASConn.BeginTrans
    
    
    CurrentLine = 3
    
    Set CMD.ActiveConnection = gbMASConn
    CurrentLine = 4
    CMD.CommandType = adCmdStoredProc
    CurrentLine = 5
    CMD.CommandText = "spsoScalePutValidate_SGS"
    CurrentLine = 6
    CMD.Parameters.Refresh
    CurrentLine = 7
    CMD.CommandTimeout = 0      'Added to prevent time outs

    CurrentLine = 8
    'Assign input parameter Values
    CMD.Parameters("@userid").Value = Left(CStr(PrintValues.UserID), 16)
    CurrentLine = 9
    CMD.Parameters("@loadid").Value = ConvertToDouble(PrintValues.LoadID)
    CurrentLine = 10
    CMD.Parameters("@prebillid").Value = ConvertToDouble(PrintValues.PreBillID)
    CurrentLine = 11
    CMD.Parameters("@facility").Value = ConvertToDouble(PrintValues.Facility)
    CurrentLine = 12
    CMD.Parameters("@weightticket").Value = ConvertToDouble(PrintValues.WeightTicketNo) 'txtWeightTicket
    CurrentLine = 13
    CMD.Parameters("@scalepass").Value = ConvertToDouble(PrintValues.BOLNumber)
    CurrentLine = 14
    CMD.Parameters("@shipmentdate").Value = CDate(PrintValues.BOLDate)
    CurrentLine = 15
    CMD.Parameters("@customer").Value = Left(PrintValues.Customer, 8)
    CurrentLine = 16
    CMD.Parameters("@customername").Value = Left(PrintValues.CustomerName, 40)
    CurrentLine = 17
    CMD.Parameters("@contract").Value = ConvertToDouble(PrintValues.ContractNo)  'txtContract.Text
    CurrentLine = 18
    CMD.Parameters("@destination").Value = Left(PrintValues.Destination, 60)
    CurrentLine = 19
    CMD.Parameters("@projectnumber").Value = Left(PrintValues.ProjectNo, 60)
    CurrentLine = 20
    CMD.Parameters("@projectdescription").Value = Left(PrintValues.ProjectDescription, 60)
    CurrentLine = 21
    CMD.Parameters("@carrier").Value = Left(PrintValues.Carrier, 30)
    CurrentLine = 22
    CMD.Parameters("@truck").Value = Left(PrintValues.TruckNo, 16)
    CurrentLine = 23
    CMD.Parameters("@trailer1").Value = Left(PrintValues.Trailer1No, 16)
    CurrentLine = 24
    CMD.Parameters("@trailer2").Value = Left(PrintValues.Trailer2No, 16)

    CurrentLine = 25
    'RKL DEJ 2015-08-06 (START)
    'Added logic to test for valide dates
    If IsDate(PrintValues.LoadTime) = True Then
        CurrentLine = 2501
        CMD.Parameters("@loadtime").Value = CDate(PrintValues.LoadTime)
        CurrentLine = 2502
    Else
        CurrentLine = 2503
        CMD.Parameters("@loadtime").Value = Null
        CurrentLine = 2504
    End If
    
    CurrentLine = 26
    If IsDate(PrintValues.DeliveryTime) = True Then
        CurrentLine = 2601
        CMD.Parameters("@deliverytime").Value = CDate(PrintValues.DeliveryTime)
        CurrentLine = 2602
    Else
        CurrentLine = 2603
        CMD.Parameters("@deliverytime").Value = Null
        CurrentLine = 2604
    End If
    
    CurrentLine = 27
    If IsDate(PrintValues.LoadTimeIn) = True Then
        CurrentLine = 2701
        CMD.Parameters("@loadtimein").Value = CDate(PrintValues.LoadTimeIn) ' CDate(pvLoadTimeIn.Time) & CDate(pvDateTimeIn.DateString)
        CurrentLine = 2702
    Else
        CurrentLine = 2703
        CMD.Parameters("@loadtimein").Value = Null
        CurrentLine = 2704
    End If
    
    CurrentLine = 28
    If IsDate(PrintValues.LoadTimeOut) = True Then
        CurrentLine = 2801
        CMD.Parameters("@loadtimeout").Value = CDate(PrintValues.LoadTimeOut) ' CDate(pvLoadTimeOut.Time) & CDate(pvDateTimeOut.DateString)
        CurrentLine = 2802
    Else
        CurrentLine = 2803
        CMD.Parameters("@loadtimeout").Value = Null
        CurrentLine = 2804
    End If
    
    CurrentLine = 29
    If IsDate(PrintValues.BOLDate) = True Then
        CurrentLine = 2901
        CMD.Parameters("@timearrived").Value = CDate(PrintValues.BOLDate)
        CurrentLine = 2902
    Else
        CurrentLine = 2903
        CMD.Parameters("@timearrived").Value = Null
        CurrentLine = 2904
    End If
    'Added logic to test for valide dates
    'RKL DEJ 2015-08-06 (STOP)

    CurrentLine = 30
    CMD.Parameters("@remark").Value = CStr(Left(PrintValues.Remarks, 60))
    CurrentLine = 31
    CMD.Parameters("@penetration").Value = ConvertToDouble(PrintValues.Penetration)
    CurrentLine = 32
    CMD.Parameters("@penetrationtemperature").Value = ConvertToDouble(PrintValues.PenetrationTemp)
    CurrentLine = 33
    CMD.Parameters("@penetrationremark").Value = CStr(Left(PrintValues.PenetrationRemark, 16))
    CurrentLine = 34
    CMD.Parameters("@flash").Value = ConvertToDouble(PrintValues.Flashpoint)
    CurrentLine = 35
    CMD.Parameters("@lbspergallon").Value = ConvertToDouble(PrintValues.LbsPerGal)
    CurrentLine = 36
    CMD.Parameters("@specificgravity").Value = ConvertToDouble(PrintValues.SpecGravity)
    CurrentLine = 37
    CMD.Parameters("@viscosity").Value = ConvertToDouble(PrintValues.Viscosity)
    CurrentLine = 38
    CMD.Parameters("@viscositytemperature").Value = ConvertToDouble(PrintValues.ViscosityTemp)
    CurrentLine = 39
    CMD.Parameters("@viscosityremark").Value = CStr(Left(PrintValues.ViscosityRemark, 16))
    CurrentLine = 40
    CMD.Parameters("@specificationremark").Value = CStr(Left(PrintValues.ProductSpecRemarks, 16))
    CurrentLine = 41
    CMD.Parameters("@additive").Value = CStr(Left(PrintValues.AdditiveID, 16))
    CurrentLine = 42
    CMD.Parameters("@additivepercent").Value = ConvertToDouble(PrintValues.AdditivePercent)
    CurrentLine = 43
    CMD.Parameters("@temperature").Value = ConvertToDouble(PrintValues.Temp)
    CurrentLine = 44
    CMD.Parameters("@grossgallons").Value = ConvertToDouble(PrintValues.GrossGal)
    CurrentLine = 45
    CMD.Parameters("@netgallons").Value = ConvertToDouble(PrintValues.NetGal)

    CurrentLine = 46
    CMD.Parameters("@grossweight1").Value = ConvertToDouble(PrintValues.GrossWtLbs1)
    CurrentLine = 47
    CMD.Parameters("@grossweight2").Value = ConvertToDouble(PrintValues.GrossWtLbs2)
    CurrentLine = 48
    CMD.Parameters("@tareweight1").Value = ConvertToDouble(PrintValues.TareWtLbs1)
    CurrentLine = 49
    CMD.Parameters("@tareweight2").Value = ConvertToDouble(PrintValues.TareWtLbs2)
    
    CurrentLine = 50
    CMD.Parameters("@item").Value = CStr(Left(PrintValues.ProductID, 16))
    CurrentLine = 51
    CMD.Parameters("@itemprice").Value = 0
    CurrentLine = 52
    CMD.Parameters("@additiveprice").Value = 0
    CurrentLine = 53
    CMD.Parameters("@inbound").Value = CStr(Left(PrintValues.InBoundOperator, 3))
    CurrentLine = 54
    CMD.Parameters("@outbound").Value = CStr(Left(PrintValues.OutBoundOperator, 3))

    CurrentLine = 55
    CMD.Parameters("@ladingsibling").Value = CStr(Left(PrintValues.LadingSibling, 23))
    CurrentLine = 56
    CMD.Parameters("@binid").Value = CStr(Left(PrintValues.TankNumber, 15))

    CurrentLine = 57
    CMD.Parameters("@customerpo").Value = CStr(Left(PrintValues.PONo, 23))
    CurrentLine = 58
    CMD.Parameters("@lot").Value = PrintValues.LotNumber
    
    CurrentLine = 59
    CMD.Parameters("@IsTransfer").Value = PrintValues.IsTransfer
    CurrentLine = 60
    CMD.Parameters("@IsBetweenCompanies").Value = PrintValues.IsBtwnCmpny
    CurrentLine = 61
    CMD.Parameters("@FromCompanyID").Value = Left(PrintValues.FromCompany, 3)
    CurrentLine = 62
    CMD.Parameters("@ToCompanyID").Value = Left(PrintValues.ToCompany, 3)
    CurrentLine = 63
    CMD.Parameters("@FromWhseID").Value = Left(PrintValues.FromWhse, 15)
    CurrentLine = 64
    CMD.Parameters("@ToWhseID").Value = Left(PrintValues.ToWhse, 15)

    'RKL DEJ 2017-12-13 (Valero)
    CurrentLine = 6400
    CMD.Parameters("@CustOwnedProd").Value = gbCustOwnedProd 'RKL DEJ 2017-12-04 Added For Valero project
    
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 65
    'Execute The procedure
    CMD.Execute

    gbLastActivityTime = Date + Time
    
    CurrentLine = 66
AfterExecute:
    CurrentLine = 67
    'Obtain Output Parameters
    CreateStatus = ConvertToDouble(CMD.Parameters.Item("@status").Value)
    CurrentLine = 68
    CreateMessage = ConvertToString(CMD.Parameters.Item("@statustext").Value)
    CurrentLine = 69
    ErrorColumn = ConvertToString(CMD.Parameters.Item("@statuscolumn").Value)
    CurrentLine = 70
    BOLNo = ConvertToString(CMD.Parameters.Item("@BOLNo").Value)

'Dim par As ADODB.Parameter
'For Each par In CMD.Parameters
'    Debug.Print par.Name
'Next

    CurrentLine = 71
    Select Case CreateStatus
        Case 0
            CurrentLine = 72
'            gbMASConn.CommitTrans
            
            ValidateData = True
            
            
            If Trim(CreateMessage) <> Empty Then
'                MsgBox CreateMessage, vbInformation, "Validate Success"
                ErrMsg = CreateMessage
            Else
'                MsgBox "The BOL Validate was Successfuly.", vbInformation, "Validate Success"
            End If
            
            CurrentLine = 73
        
        Case Else
            CurrentLine = 74
'            gbMASConn.RollbackTrans
            
            ValidateData = False
            
            CurrentLine = 75
            
            If Trim(CreateMessage) <> Empty Then
'                MsgBox CreateMessage, vbExclamation, "Validate Failed"
                ErrMsg = CreateMessage
            Else
'                MsgBox "The BOL Server Validation was NOT Successfull.", vbExclamation, "Validate Failed"
                ErrMsg = "The BOL Server Validation was NOT Successfull."
            End If
            
            CurrentLine = 76
            
'            ErrMsg = Me.Name & ".ValidateData()" & vbCrLf & "BOLNo = " & BOLNo & vbCrLf & "CreateStatus = " & CreateStatus & vbCrLf & " ErrorColumn = " & ErrorColumn & vbCrLf & " CreateMessage = " & CreateMessage
            lcErrMsg = Me.Name & ".ValidateData()" & vbCrLf & "BOLNo = " & BOLNo & vbCrLf & "CreateStatus = " & CreateStatus & vbCrLf & " ErrorColumn = " & ErrorColumn & vbCrLf & " CreateMessage = " & CreateMessage
            
            ErrMsg = "BOLNo = " & BOLNo & vbCrLf & "CreateStatus = " & CreateStatus & vbCrLf & " ErrorColumn = " & ErrorColumn & vbCrLf & " CreateMessage = " & CreateMessage
            
            CurrentLine = 77
            
            If RSBOL.State = 1 Then
                CurrentLine = 78
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 79
                    InsErrorLog 0, Date + Time, lcErrMsg
                Else
                    CurrentLine = 80
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                End If
                CurrentLine = 81
            Else
                CurrentLine = 82
                InsErrorLog 0, Date + Time, lcErrMsg
            End If
            
            CurrentLine = 83
            
    End Select

    CurrentLine = 84
    
    
CleanUP:
    CurrentLine = 85
    Err.Clear
    CurrentLine = 86
    On Error Resume Next
    CurrentLine = 87
    Set CMD = Nothing
    CurrentLine = 88

    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lcErrMsg = Me.Name & ".ValidateData(PrintValues As BOLFields) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    MsgBox lcErrMsg, vbCritical, "Error"
    
    lcErrMsg = lcErrMsg & GetPrintValuesStr(PrintValues)

    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, lcErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, lcErrMsg
    End If

    Err.Clear
    On Error Resume Next
    
'    gbMASConn.RollbackTrans

    ErrMsg = Empty
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP


End Function


Private Function RunAPI(PrintValues As BOLFields, Optional SuppressMsg As Boolean = False, Optional ErrMsg As String = Empty) As Boolean

    On Error GoTo Error
    Dim CMD As New ADODB.Command

    Dim CreateStatus As Integer
    Dim CreateMessage As String
    Dim ErrorColumn As String
    Dim BOLNo As String
    Dim CurrentLine As Integer
    Dim lcErrMsg As String

    gbLastActivityTime = Date + Time
    
    '*********************************************************************************
    '*********************************************************************************
    '*********************************************************************************
    'RKL DEJ 2017-12-05 (START)
    CurrentLine = -1
    If gbCustOwnedProd = True Then
        RunAPI = RunAPICustOwned(PrintValues, SuppressMsg, ErrMsg, 701)
        Exit Function
    End If
    'RKL DEJ 2017-12-05 (STOP)
    '*********************************************************************************
    '*********************************************************************************
    '*********************************************************************************
    
    CurrentLine = 0
    
    RunAPI = False
    
    CurrentLine = 1
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 2
        If HaveLostMASConnection = True Then
            CurrentLine = 3
            'No longer connected to MAS 500
            ErrMsg = "The connection to MAS has been lost.  The Sales order was not created."
            Exit Function
        End If
    End If
    
    CurrentLine = 4
    If gbMASConn.State <> 1 Then
        CurrentLine = 5
        If SuppressMsg = False Then
            CurrentLine = 6
'            MsgBox "The connection to MAS has been lost.  The Sales order was not created.", vbExclamation, "SO Not Created"
            ErrMsg = "The connection to MAS has been lost.  The Sales order was not created."
        End If
        CurrentLine = 7
        Exit Function
    End If
    
'    gbMASConn.BeginTrans
    
    CurrentLine = 8
    Set CMD.ActiveConnection = gbMASConn
    CurrentLine = 9
    CMD.CommandType = adCmdStoredProc
    CurrentLine = 10
    CMD.CommandText = "spsoScalePut_SGS"
    CurrentLine = 11
    CMD.Parameters.Refresh
    CurrentLine = 12
    CMD.CommandTimeout = 0      'Added to prevent time outs

    CurrentLine = 13
    'Assign input parameter Values
    CMD.Parameters("@userid").Value = Left(CStr(PrintValues.UserID), 16)
    CurrentLine = 14
    CMD.Parameters("@loadid").Value = ConvertToDouble(PrintValues.LoadID)
    CurrentLine = 15
    CMD.Parameters("@prebillid").Value = ConvertToDouble(PrintValues.PreBillID)
    CurrentLine = 16
    CMD.Parameters("@facility").Value = ConvertToDouble(PrintValues.Facility)
    CurrentLine = 17
    CMD.Parameters("@weightticket").Value = ConvertToDouble(PrintValues.WeightTicketNo) 'txtWeightTicket
    CurrentLine = 18
    CMD.Parameters("@scalepass").Value = ConvertToDouble(PrintValues.BOLNumber)
    
    CurrentLine = 19
    If IsDate(PrintValues.BOLDate) = True Then
        CurrentLine = 1901
        CMD.Parameters("@shipmentdate").Value = CDate(PrintValues.BOLDate)
        CurrentLine = 1902
    Else
        CurrentLine = 1903
        CMD.Parameters("@shipmentdate").Value = Null
        CurrentLine = 1904
    End If
    
    CurrentLine = 20
    CMD.Parameters("@customer").Value = Left(PrintValues.Customer, 8)
    CurrentLine = 21
    CMD.Parameters("@customername").Value = Left(PrintValues.CustomerName, 40)
    CurrentLine = 22
    CMD.Parameters("@contract").Value = ConvertToDouble(PrintValues.ContractNo)  'txtContract.Text
    CurrentLine = 23
    CMD.Parameters("@destination").Value = Left(PrintValues.Destination, 60)
    CurrentLine = 24
    CMD.Parameters("@projectnumber").Value = Left(PrintValues.ProjectNo, 60)
    CurrentLine = 25
    CMD.Parameters("@projectdescription").Value = Left(PrintValues.ProjectDescription, 60)
    CurrentLine = 26
    CMD.Parameters("@carrier").Value = Left(PrintValues.Carrier, 30)
    CurrentLine = 27
    CMD.Parameters("@truck").Value = Left(PrintValues.TruckNo, 16)
    CurrentLine = 28
    CMD.Parameters("@trailer1").Value = Left(PrintValues.Trailer1No, 16)
    CurrentLine = 29
    CMD.Parameters("@trailer2").Value = Left(PrintValues.Trailer2No, 16)

    CurrentLine = 30
    If IsDate(PrintValues.LoadTime) = True Then
        CurrentLine = 3001
        CMD.Parameters("@loadtime").Value = CDate(PrintValues.LoadTime)
        CurrentLine = 3002
    Else
        CurrentLine = 3003
        CMD.Parameters("@loadtime").Value = Null
        CurrentLine = 3004
    End If
    
    CurrentLine = 31
    If IsDate(PrintValues.DeliveryTime) = True Then
        CurrentLine = 3101
        CMD.Parameters("@deliverytime").Value = CDate(PrintValues.DeliveryTime)
        CurrentLine = 3102
    Else
        CurrentLine = 3103
        CMD.Parameters("@deliverytime").Value = Null
        CurrentLine = 3104
    End If
    
    CurrentLine = 32
    If IsDate(PrintValues.LoadTimeIn) = True Then
        CurrentLine = 3201
        CMD.Parameters("@loadtimein").Value = CDate(PrintValues.LoadTimeIn)
        CurrentLine = 3202
    Else
        CurrentLine = 3203
        CMD.Parameters("@loadtimein").Value = Null
        CurrentLine = 3204
    End If
    
    CurrentLine = 33
    If IsDate(PrintValues.LoadTimeOut) = True Then
        CurrentLine = 3301
        CMD.Parameters("@loadtimeout").Value = CDate(PrintValues.LoadTimeOut)
        CurrentLine = 3302
    Else
        CurrentLine = 3303
        CMD.Parameters("@loadtimeout").Value = Null
        CurrentLine = 3304
    End If
    
    CurrentLine = 34
    If IsDate(PrintValues.BOLDate) = True Then
        CurrentLine = 3401
        CMD.Parameters("@timearrived").Value = CDate(PrintValues.BOLDate)
        CurrentLine = 3402
    Else
        CurrentLine = 3403
        CMD.Parameters("@timearrived").Value = Null
        CurrentLine = 3404
    End If

    CurrentLine = 35
    CMD.Parameters("@remark").Value = CStr(Left(PrintValues.Remarks, 60))
    CurrentLine = 36
    CMD.Parameters("@penetration").Value = ConvertToDouble(PrintValues.Penetration)
    CurrentLine = 37
    CMD.Parameters("@penetrationtemperature").Value = ConvertToDouble(PrintValues.PenetrationTemp)
    CurrentLine = 38
    CMD.Parameters("@penetrationremark").Value = CStr(Left(PrintValues.PenetrationRemark, 16))
    CurrentLine = 39
    CMD.Parameters("@flash").Value = ConvertToDouble(PrintValues.Flashpoint)
    CurrentLine = 40
    CMD.Parameters("@lbspergallon").Value = ConvertToDouble(PrintValues.LbsPerGal)
    CurrentLine = 41
    CMD.Parameters("@specificgravity").Value = ConvertToDouble(PrintValues.SpecGravity)
    CurrentLine = 42
    CMD.Parameters("@viscosity").Value = ConvertToDouble(PrintValues.Viscosity)
    CurrentLine = 43
    CMD.Parameters("@viscositytemperature").Value = ConvertToDouble(PrintValues.ViscosityTemp)
    CurrentLine = 44
    CMD.Parameters("@viscosityremark").Value = CStr(Left(PrintValues.ViscosityRemark, 16))
    CurrentLine = 45
    CMD.Parameters("@specificationremark").Value = CStr(Left(PrintValues.ProductSpecRemarks, 16))
    CurrentLine = 46
    CMD.Parameters("@additive").Value = CStr(Left(PrintValues.AdditiveID, 16))
    CurrentLine = 47
    CMD.Parameters("@additivepercent").Value = ConvertToDouble(PrintValues.AdditivePercent)
    CurrentLine = 48
    CMD.Parameters("@temperature").Value = ConvertToDouble(PrintValues.Temp)
    CurrentLine = 49
    CMD.Parameters("@grossgallons").Value = ConvertToDouble(PrintValues.GrossGal)
    CurrentLine = 50
    CMD.Parameters("@netgallons").Value = ConvertToDouble(PrintValues.NetGal)

    CurrentLine = 51
    CMD.Parameters("@grossweight1").Value = ConvertToDouble(PrintValues.GrossWtLbs1)
    CurrentLine = 52
    CMD.Parameters("@grossweight2").Value = ConvertToDouble(PrintValues.GrossWtLbs2)
    CurrentLine = 53
    CMD.Parameters("@tareweight1").Value = ConvertToDouble(PrintValues.TareWtLbs1)
    CurrentLine = 54
    CMD.Parameters("@tareweight2").Value = ConvertToDouble(PrintValues.TareWtLbs2)
    
    CurrentLine = 55
    CMD.Parameters("@item").Value = CStr(Left(PrintValues.ProductID, 16))
    CurrentLine = 56
    CMD.Parameters("@itemprice").Value = 0
    CurrentLine = 57
    CMD.Parameters("@additiveprice").Value = 0
    CurrentLine = 58
    CMD.Parameters("@inbound").Value = CStr(Left(PrintValues.InBoundOperator, 3))
    CurrentLine = 59
    CMD.Parameters("@outbound").Value = CStr(Left(PrintValues.OutBoundOperator, 3))

    CurrentLine = 60
    CMD.Parameters("@ladingsibling").Value = CStr(Left(PrintValues.LadingSibling, 23))
    CurrentLine = 61
    CMD.Parameters("@binid").Value = CStr(Left(PrintValues.TankNumber, 15))

    CurrentLine = 62
    CMD.Parameters("@customerpo").Value = CStr(Left(PrintValues.PONo, 23))
    CurrentLine = 63
    CMD.Parameters("@lot").Value = PrintValues.LotNumber
    
    CurrentLine = 64
    CMD.Parameters("@IsTransfer").Value = PrintValues.IsTransfer
    CurrentLine = 65
    CMD.Parameters("@IsBetweenCompanies").Value = PrintValues.IsBtwnCmpny
    CurrentLine = 66
    CMD.Parameters("@FromCompanyID").Value = Left(PrintValues.FromCompany, 3)
    CurrentLine = 67
    CMD.Parameters("@ToCompanyID").Value = Left(PrintValues.ToCompany, 3)
    CurrentLine = 68
    CMD.Parameters("@FromWhseID").Value = Left(PrintValues.FromWhse, 15)
    CurrentLine = 69
    CMD.Parameters("@ToWhseID").Value = Left(PrintValues.ToWhse, 15)
    
    CurrentLine = 6901
    CMD.Parameters("@LocationCode").Value = Null
    CurrentLine = 6902
    CMD.Parameters("@ShipToAddrLine1").Value = Null
    CurrentLine = 6903
    CMD.Parameters("@ShipToAddrLine2").Value = Null
    CurrentLine = 6904
    CMD.Parameters("@ShipToAddrLine3").Value = Null
    CurrentLine = 6905
    CMD.Parameters("@ShipToAddrLine4").Value = Null
    CurrentLine = 6906
    CMD.Parameters("@ShipToAddrLine5").Value = Null
    CurrentLine = 6907
    CMD.Parameters("@ShipToCity").Value = Null
    CurrentLine = 6908
    CMD.Parameters("@ShipToState").Value = Null
    CurrentLine = 6909
    CMD.Parameters("@ShipToPostalCode").Value = Null
    
    CurrentLine = 6910
    CMD.Parameters("@SecurityTapeNumber").Value = Left(PrintValues.SecTapeNbr, 75)
    
    CurrentLine = 6911
    CMD.Parameters("@BatchNumber").Value = Left(PrintValues.BatchNumber, 100)
    CurrentLine = 6912
    CMD.Parameters("@WeightTktNo").Value = Left(PrintValues.WeightTktNo, 30)
    CurrentLine = 6913
    CMD.Parameters("@Carrier_ID").Value = Left(PrintValues.Carrier_ID, 10)
    CurrentLine = 6914
    CMD.Parameters("@Carrier_Name").Value = Left(PrintValues.Carrier_Name, 30)
    CurrentLine = 6915
    CMD.Parameters("@Purchase_Order").Value = Left(PrintValues.Purchase_Order, 255)
    CurrentLine = 6916
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 70
    'Execute The procedure
    CMD.Execute

    gbLastActivityTime = Date + Time

    CurrentLine = 71
AfterExecute:
    CurrentLine = 72
    'Obtain Output Parameters
    CreateStatus = ConvertToDouble(CMD.Parameters.Item("@status").Value)
    CurrentLine = 73
    CreateMessage = ConvertToString(CMD.Parameters.Item("@statustext").Value)
    CurrentLine = 74
    ErrorColumn = ConvertToString(CMD.Parameters.Item("@statuscolumn").Value)
    CurrentLine = 75
    BOLNo = ConvertToString(CMD.Parameters.Item("@BOLNo").Value)

    CurrentLine = 76
    Select Case CreateStatus
        Case 0
            CurrentLine = 77
'            gbMASConn.CommitTrans
            
            CurrentLine = 78
            RunAPI = True
            
            CurrentLine = 79
            If SuppressMsg = False Then
                CurrentLine = 80
'RKL DEJ 9/24/13 Kevin asked to not display these messages (START)
'                If Trim(CreateMessage) <> Empty Then
'                    CurrentLine = 81
'                    MsgBox CreateMessage, vbInformation, "SO API Success"
'                Else
'                    CurrentLine = 82
'                    MsgBox "The BOL SO API was Successfull.", vbInformation, "SO API Success"
'                End If
'RKL DEJ 9/24/13 Kevin asked to not display these messages (STOP)
            End If
            
            CurrentLine = 83
            
        Case Else
            CurrentLine = 84
'            gbMASConn.RollbackTrans
            
            RunAPI = False
            
            CurrentLine = 85
'            If SuppressMsg = False Then
                CurrentLine = 86
                If Trim(CreateMessage) <> Empty Then
                    CurrentLine = 87
                    
'                    MsgBox CreateMessage, vbExclamation, "SO API Failed"
                    
                    CurrentLine = 88
                    
                    lcErrMsg = Me.Name & ".RunAPI(PrintValues As BOLFields) As Boolean" & vbCrLf & _
                    "CreateStatus: " & CreateStatus & vbCrLf & _
                    "ErrorColumn: " & ErrorColumn & vbCrLf & _
                    "BOLNo: " & BOLNo & vbCrLf & _
                    "CreateMessage: " & CreateMessage
                
                    CurrentLine = 89
                    
                Else
                    CurrentLine = 90
                    
'                    MsgBox "The BOL SO API was NOT created Successfully.", vbExclamation, "SO API Failed"
                    
                    CurrentLine = 91
                    
                    lcErrMsg = Me.Name & ".RunAPI(PrintValues As BOLFields) As Boolean" & vbCrLf & _
                    "CreateStatus: " & CreateStatus & vbCrLf & _
                    "ErrorColumn: " & ErrorColumn & vbCrLf & _
                    "BOLNo: " & BOLNo & vbCrLf & _
                    "CreateMessage: " & "The BOL SO API was NOT created Successfully."
                
                    CurrentLine = 92
                    
                End If
'            Else
                CurrentLine = 93
            
'            End If
            
            CurrentLine = 94
            
            If RSBOL.State = 1 Then
                CurrentLine = 95
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 96
                    InsErrorLog 0, Date + Time, lcErrMsg
                Else
                    CurrentLine = 97
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                End If
            Else
                CurrentLine = 98
                InsErrorLog 0, Date + Time, lcErrMsg
            End If
            CurrentLine = 99
            
            ErrMsg = lcErrMsg
    End Select
    
    CurrentLine = 100
    
CleanUP:
    CurrentLine = 101
    Err.Clear
    CurrentLine = 102
    On Error Resume Next
    CurrentLine = 103
    Set CMD = Nothing

    CurrentLine = 104
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lcErrMsg = Me.Name & ".RunAPI(PrintValues As BOLFields, Optional SuppressMsg As Boolean = False) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & _
    "SuppressMsg = " & SuppressMsg & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    If SuppressMsg = False Then
        MsgBox lcErrMsg, vbCritical, "Error"
    End If

    lcErrMsg = lcErrMsg & GetPrintValuesStr(PrintValues)

    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, lcErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, lcErrMsg
    End If

    Err.Clear
    On Error Resume Next
    
'    gbMASConn.RollbackTrans
    
    ErrMsg = lcErrMsg
    
    gbLastActivityTime = Date + Time
    
    
    GoTo CleanUP


End Function

Private Function RunAPICustOwned(PrintValues As BOLFields, Optional SuppressMsg As Boolean = False, Optional ErrMsg As String = Empty, Optional TranType As Long = 701) As Boolean

    On Error GoTo Error
    Dim CMD As New ADODB.Command

    Dim CreateStatus As Integer
    Dim CreateMessage As String
    Dim ErrorColumn As String
    Dim BOLNo As String
    Dim CurrentLine As Integer
    Dim lcErrMsg As String
    
    Dim oErrMsg As String
    Dim oRtnVal As Long

    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    RunAPICustOwned = False
    
    CurrentLine = 1
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 2
        If HaveLostMASConnection = True Then
            CurrentLine = 3
            'No longer connected to MAS 500
            ErrMsg = "The connection to MAS has been lost.  The Sales order was not created."
            Exit Function
        End If
    End If
    
    CurrentLine = 4
    If gbMASConn.State <> 1 Then
        CurrentLine = 5
        If SuppressMsg = False Then
            CurrentLine = 6
'            MsgBox "The connection to MAS has been lost.  The Sales order was not created.", vbExclamation, "SO Not Created"
            ErrMsg = "The connection to MAS has been lost.  The Sales order was not created."
        End If
        CurrentLine = 7
        Exit Function
    End If
    
'    gbMASConn.BeginTrans
    
    CurrentLine = 8
    Set CMD.ActiveConnection = gbMASConn
    CurrentLine = 9
    CMD.CommandType = adCmdStoredProc
    CurrentLine = 10
    CMD.CommandText = "spsoScalePutCreateIMTransaction_SGS"
    CurrentLine = 11
    CMD.Parameters.Refresh
    CurrentLine = 12
    CMD.CommandTimeout = 0      'Added to prevent time outs

    CurrentLine = 13
    'Assign input parameter Values
    
    CurrentLine = 10001
    CMD.Parameters("@CustOwnedProd").Value = gbCustOwnedProd 'RKL DEJ 2017-12-04 Added For Valero project
    CurrentLine = 10002
    CMD.Parameters("@AutoPostIMTrans").Value = gbAutoPostIMTrans    'PrintValues.AutoPostIMTrans     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 10003
    CMD.Parameters("@NotAllowSplitLoads").Value = gbNotAllowSplitLoads  'PrintValues.NotAllowSplitLoads     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 10004
    CMD.Parameters("@SrcVariance").Value = gbSrcVariance    'PrintValues.SrcVariance     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 10005
    CMD.Parameters("@IMTranReasonCode").Value = gbIMTranReasonCode  'PrintValues.IMTranReasonCode     'RKL DEJ 2017-12-18 Added For Valero project
    CurrentLine = 10006
    CMD.Parameters("@ForceVarianceCheck").Value = gbForceVarianceCheck  'PrintValues.ForceVarianceCheck     'RKL DEJ 2017-12-18 Added For Valero project
    
    CurrentLine = 10010
    CMD.Parameters("@BOLTableKey").Value = RSBOL("BOLTableKey").Value
    CurrentLine = 10011
    CMD.Parameters("@LoadsTableKey").Value = RSBOL("LoadsTableKey").Value
    CurrentLine = 10012
    CMD.Parameters("@FrontRearFlag").Value = RSBOL("FrontRearFlag").Value
    CurrentLine = 10013
    CMD.Parameters("@TruckNo").Value = RSBOL("TruckNo").Value
    CurrentLine = 10014
    CMD.Parameters("@Hauler").Value = RSBOL("Hauler").Value
    CurrentLine = 10015
    CMD.Parameters("@Customer").Value = RSBOL("Customer").Value
    CurrentLine = 10016
    CMD.Parameters("@Project").Value = RSBOL("Project").Value
    CurrentLine = 10017
    CMD.Parameters("@ContractNbr").Value = RSBOL("ContractNbr").Value
    CurrentLine = 10018
    CMD.Parameters("@BOLNbr").Value = RSBOL("BOLNbr").Value
    CurrentLine = 10019
    CMD.Parameters("@Destination").Value = RSBOL("Destination").Value
    CurrentLine = 10020
    CMD.Parameters("@DestDirections").Value = RSBOL("DestDirections").Value
    CurrentLine = 10021
    CMD.Parameters("@Remarks").Value = RSBOL("Remarks").Value
    CurrentLine = 10022
    CMD.Parameters("@FrontGrossWt").Value = RSBOL("FrontGrossWt").Value
    CurrentLine = 10023
    CMD.Parameters("@FrontTareWt").Value = RSBOL("FrontTareWt").Value
    CurrentLine = 10024
    CMD.Parameters("@FrontNetWt").Value = RSBOL("FrontNetWt").Value
    CurrentLine = 10025
    CMD.Parameters("@FrontBaseGallons").Value = RSBOL("FrontBaseGallons").Value
    CurrentLine = 10026
    CMD.Parameters("@FrontTemp").Value = RSBOL("FrontTemp").Value
    CurrentLine = 10027
    CMD.Parameters("@FrontAdditive").Value = RSBOL("FrontAdditive").Value
    CurrentLine = 10028
    CMD.Parameters("@FrontAdditiveGallons").Value = RSBOL("FrontAdditiveGallons").Value
    CurrentLine = 10029
    CMD.Parameters("@RearGrossWt").Value = RSBOL("RearGrossWt").Value
    CurrentLine = 10030
    CMD.Parameters("@RearTareWt").Value = RSBOL("RearTareWt").Value
    CurrentLine = 10031
    CMD.Parameters("@RearNetWt").Value = RSBOL("RearNetWt").Value
    CurrentLine = 10032
    CMD.Parameters("@RearBaseGallons").Value = RSBOL("RearBaseGallons").Value
    CurrentLine = 10033
    CMD.Parameters("@RearTemp").Value = RSBOL("RearTemp").Value
    CurrentLine = 10034
    CMD.Parameters("@RearAdditive").Value = RSBOL("RearAdditive").Value
    CurrentLine = 10035
    CMD.Parameters("@RearAdditiveGallons").Value = RSBOL("RearAdditiveGallons").Value
    CurrentLine = 10036
    CMD.Parameters("@TotalGrossWt").Value = RSBOL("TotalGrossWt").Value
    CurrentLine = 10037
    CMD.Parameters("@TotalTareWt").Value = RSBOL("TotalTareWt").Value
    CurrentLine = 10038
    CMD.Parameters("@TotalNetWt").Value = RSBOL("TotalNetWt").Value
    CurrentLine = 10039
    CMD.Parameters("@TotalBaseGallons").Value = RSBOL("TotalBaseGallons").Value
    CurrentLine = 10040
    CMD.Parameters("@TotalAdditiveGallons").Value = RSBOL("TotalAdditiveGallons").Value
    CurrentLine = 10041
    CMD.Parameters("@NetTons").Value = RSBOL("NetTons").Value
    CurrentLine = 10042
    CMD.Parameters("@LbsPerGallon").Value = RSBOL("LbsPerGallon").Value
    CurrentLine = 10043
    CMD.Parameters("@TotalGallons").Value = RSBOL("TotalGallons").Value
    CurrentLine = 10044
    CMD.Parameters("@LoadTime").Value = RSBOL("LoadTime").Value
    CurrentLine = 10045
    CMD.Parameters("@DeliverTime").Value = RSBOL("DeliverTime").Value
    CurrentLine = 10046
    CMD.Parameters("@ScaleInTime").Value = RSBOL("ScaleInTime").Value
    CurrentLine = 10047
    CMD.Parameters("@ScaleOutTime").Value = RSBOL("ScaleOutTime").Value
    CurrentLine = 10048
    CMD.Parameters("@WhseID").Value = RSBOL("WhseID").Value
    CurrentLine = 10049
    CMD.Parameters("@ProductClass").Value = RSBOL("ProductClass").Value
    CurrentLine = 10050
    CMD.Parameters("@ProductID").Value = RSBOL("ProductID").Value
    CurrentLine = 10051
    CMD.Parameters("@ProductDesc").Value = RSBOL("ProductDesc").Value
    CurrentLine = 10052
    CMD.Parameters("@AdditiveClass").Value = RSBOL("AdditiveClass").Value
    CurrentLine = 10053
    CMD.Parameters("@AdditiveID").Value = RSBOL("AdditiveID").Value
    CurrentLine = 10054
    CMD.Parameters("@AdditiveDesc").Value = RSBOL("AdditiveDesc").Value
    CurrentLine = 10055
    CMD.Parameters("@GrossGallons").Value = RSBOL("GrossGallons").Value
    CurrentLine = 10056
    CMD.Parameters("@NetGallons").Value = RSBOL("NetGallons").Value
    CurrentLine = 10057
    CMD.Parameters("@Flashpoint").Value = RSBOL("Flashpoint").Value
    CurrentLine = 10058
    CMD.Parameters("@LbsPerGallon2").Value = RSBOL("LbsPerGallon2").Value
    CurrentLine = 10059
    CMD.Parameters("@SpecificGravity").Value = RSBOL("SpecificGravity").Value
    CurrentLine = 10060
    CMD.Parameters("@LotNbr").Value = RSBOL("LotNbr").Value
    CurrentLine = 10061
    CMD.Parameters("@LoadTemp").Value = RSBOL("LoadTemp").Value
    CurrentLine = 10062
    CMD.Parameters("@Viscosity").Value = RSBOL("Viscosity").Value
    CurrentLine = 10063
    CMD.Parameters("@Residue").Value = RSBOL("Residue").Value
    CurrentLine = 10064
    CMD.Parameters("@RackNbr").Value = RSBOL("RackNbr").Value
    CurrentLine = 10065
    CMD.Parameters("@Tons").Value = RSBOL("Tons").Value
    CurrentLine = 10066
    CMD.Parameters("@PrintDate").Value = RSBOL("PrintDate").Value
    CurrentLine = 10067
    CMD.Parameters("@ViscosityTemp").Value = RSBOL("ViscosityTemp").Value
    CurrentLine = 10068
    CMD.Parameters("@IsCreditReturn").Value = RSBOL("IsCreditReturn").Value
    CurrentLine = 10069
    CMD.Parameters("@IsSplitLoad").Value = RSBOL("IsSplitLoad").Value
    CurrentLine = 10070
    CMD.Parameters("@IsHandKeyed").Value = RSBOL("IsHandKeyed").Value
    CurrentLine = 10071
    CMD.Parameters("@IsScaleEnabled").Value = RSBOL("IsScaleEnabled").Value
    CurrentLine = 10072
    CMD.Parameters("@DidMASAPIRunSuccessfully").Value = RSBOL("DidMASAPIRunSuccessfully").Value
    CurrentLine = 10073
    CMD.Parameters("@UserID").Value = RSBOL("UserID").Value
    CurrentLine = 10074
    CMD.Parameters("@MASloadid").Value = RSBOL("MASloadid").Value
    CurrentLine = 10075
    CMD.Parameters("@MAScustref").Value = RSBOL("MAScustref").Value
    CurrentLine = 10076
    CMD.Parameters("@MASprebillid").Value = RSBOL("MASprebillid").Value
    CurrentLine = 10078
    CMD.Parameters("@MAScustomer").Value = RSBOL("MAScustomer").Value
    CurrentLine = 10079
    CMD.Parameters("@MAScustomername").Value = RSBOL("MAScustomername").Value
    CurrentLine = 10080
    CMD.Parameters("@MASscaledate").Value = RSBOL("MASscaledate").Value
    CurrentLine = 10081
    CMD.Parameters("@MAScontract").Value = RSBOL("MAScontract").Value
    CurrentLine = 10082
    CMD.Parameters("@MAScustomerpo").Value = RSBOL("MAScustomerpo").Value
    CurrentLine = 10083
    CMD.Parameters("@MASFacility").Value = RSBOL("MASFacility").Value
    CurrentLine = 10084
    CMD.Parameters("@MASFacilityName").Value = RSBOL("MASFacilityName").Value
    CurrentLine = 10085
    CMD.Parameters("@MASProjectNumber").Value = RSBOL("MASProjectNumber").Value
    CurrentLine = 10086
    CMD.Parameters("@MASProjectDescription").Value = RSBOL("MASProjectDescription").Value
    CurrentLine = 10087
    CMD.Parameters("@MAScarrier").Value = RSBOL("MAScarrier").Value
    CurrentLine = 10088
    CMD.Parameters("@MAStruck").Value = RSBOL("MAStruck").Value
    CurrentLine = 10089
    CMD.Parameters("@MAStrailer1").Value = RSBOL("MAStrailer1").Value
    CurrentLine = 10090
    CMD.Parameters("@MAStrailer2").Value = RSBOL("MAStrailer2").Value
    CurrentLine = 10091
    CMD.Parameters("@MASloadtime").Value = RSBOL("MASloadtime").Value
    CurrentLine = 10092
    CMD.Parameters("@MASdeliverytime").Value = RSBOL("MASdeliverytime").Value
    CurrentLine = 10093
    CMD.Parameters("@MASshortdestination").Value = RSBOL("MASshortdestination").Value
    CurrentLine = 10094
    CMD.Parameters("@MASdestination").Value = RSBOL("MASdestination").Value
    CurrentLine = 10095
    CMD.Parameters("@MASpenetration").Value = RSBOL("MASpenetration").Value
    CurrentLine = 10096
    CMD.Parameters("@MASpenetrationtemperature").Value = RSBOL("MASpenetrationtemperature").Value
    CurrentLine = 10097
    CMD.Parameters("@MASpenetrationremark").Value = RSBOL("MASpenetrationremark").Value
    CurrentLine = 10098
    CMD.Parameters("@MASflash").Value = RSBOL("MASflash").Value
    CurrentLine = 10099
    CMD.Parameters("@MASlbspergallon").Value = RSBOL("MASlbspergallon").Value
    CurrentLine = 10100
    CMD.Parameters("@MASspecificgravity").Value = RSBOL("MASspecificgravity").Value
    CurrentLine = 10101
    CMD.Parameters("@MASviscosity").Value = RSBOL("MASviscosity").Value
    CurrentLine = 10102
    CMD.Parameters("@MASviscositytemperature").Value = RSBOL("MASviscositytemperature").Value
    CurrentLine = 10103
    CMD.Parameters("@MASviscosityremark").Value = RSBOL("MASviscosityremark").Value
    CurrentLine = 10104
    CMD.Parameters("@MAScompliance").Value = RSBOL("MAScompliance").Value
    CurrentLine = 10105
    CMD.Parameters("@MASadditive").Value = RSBOL("MASadditive").Value
    CurrentLine = 10106
    CMD.Parameters("@MASadditivedescription").Value = RSBOL("MASadditivedescription").Value
    CurrentLine = 10107
    CMD.Parameters("@MASadditivepercent").Value = RSBOL("MASadditivepercent").Value
    CurrentLine = 10108
    CMD.Parameters("@MAStemperaturemultiplier").Value = RSBOL("MAStemperaturemultiplier").Value
    CurrentLine = 10109
    CMD.Parameters("@MASspecificationremark").Value = RSBOL("MASspecificationremark").Value
    CurrentLine = 10110
    CMD.Parameters("@MASplacardtype").Value = RSBOL("MASplacardtype").Value
    CurrentLine = 10111
    CMD.Parameters("@MASitem").Value = RSBOL("MASitem").Value
    CurrentLine = 10113
    CMD.Parameters("@MASitemdescription").Value = RSBOL("MASitemdescription").Value
    CurrentLine = 10114
    CMD.Parameters("@MASCommodity_ID").Value = RSBOL("MASCommodity_ID").Value
    CurrentLine = 10115
    CMD.Parameters("@MASCommodity").Value = RSBOL("MASCommodity").Value
    CurrentLine = 10116
    CMD.Parameters("@MASFromCompanyID").Value = RSBOL("MASFromCompanyID").Value
    CurrentLine = 10117
    CMD.Parameters("@MASFromWhseID").Value = RSBOL("MASFromWhseID").Value
    CurrentLine = 10118
    CMD.Parameters("@MASToCompanyID").Value = RSBOL("MASToCompanyID").Value
    CurrentLine = 10119
    CMD.Parameters("@MASToWhseID").Value = RSBOL("MASToWhseID").Value
    CurrentLine = 10120
    CMD.Parameters("@LadingSibling").Value = RSBOL("LadingSibling").Value
    CurrentLine = 10121
    CMD.Parameters("@TankNumber").Value = RSBOL("TankNumber").Value
    CurrentLine = 10122
    CMD.Parameters("@InBoundOperator").Value = RSBOL("InBoundOperator").Value
    CurrentLine = 10123
    CMD.Parameters("@OutBoundOperator").Value = RSBOL("OutBoundOperator").Value
    CurrentLine = 10124
    CMD.Parameters("@HasPrintedSuccessfully").Value = RSBOL("HasPrintedSuccessfully").Value
    CurrentLine = 10125
    CMD.Parameters("@HasBeenSentToMAS").Value = RSBOL("HasBeenSentToMAS").Value
    CurrentLine = 10126
    CMD.Parameters("@HasMASBOLLogBeenCreated").Value = RSBOL("HasMASBOLLogBeenCreated").Value
    CurrentLine = 10127
    CMD.Parameters("@HasMASSOBeenCreated").Value = RSBOL("HasMASSOBeenCreated").Value
    CurrentLine = 10128
    CMD.Parameters("@ScaleDate").Value = RSBOL("ScaleDate").Value
    CurrentLine = 10129
    CMD.Parameters("@TargetGrossWt").Value = RSBOL("TargetGrossWt").Value
    CurrentLine = 10130
    CMD.Parameters("@TargetNetWt").Value = RSBOL("TargetNetWt").Value
    CurrentLine = 10131
    CMD.Parameters("@WtPerGallon").Value = RSBOL("WtPerGallon").Value
    CurrentLine = 10132
    CMD.Parameters("@TargetGallons").Value = RSBOL("TargetGallons").Value
    CurrentLine = 10133
    CMD.Parameters("@SelectedGallons").Value = RSBOL("SelectedGallons").Value
    CurrentLine = 10134
    CMD.Parameters("@Trailer1_ID").Value = RSBOL("Trailer1_ID").Value
    CurrentLine = 10135
    CMD.Parameters("@Trailer2_ID").Value = RSBOL("Trailer2_ID").Value
    CurrentLine = 10136
    CMD.Parameters("@Purchase_Order").Value = RSBOL("Purchase_Order").Value
    CurrentLine = 10137
    CMD.Parameters("@Carrier").Value = RSBOL("Carrier").Value
    CurrentLine = 10138
    CMD.Parameters("@PrintCOA").Value = RSBOL("PrintCOA").Value
    CurrentLine = 10139
    CMD.Parameters("@COARequired").Value = RSBOL("COARequired").Value
    CurrentLine = 10140
    CMD.Parameters("@HighRisk").Value = RSBOL("HighRisk").Value
    CurrentLine = 10141
    CMD.Parameters("@SecurityTapNumber").Value = RSBOL("SecurityTapNumber").Value
    CurrentLine = 10142
    CMD.Parameters("@BOLAddressAndPhone").Value = RSBOL("BOLAddressAndPhone").Value
    CurrentLine = 10143
    CMD.Parameters("@PASSPatent").Value = RSBOL("PASSPatent").Value
    CurrentLine = 10144
    CMD.Parameters("@PrintPASSPatent").Value = RSBOL("PrintPASSPatent").Value
    CurrentLine = 10145
    CMD.Parameters("@LotNbrCaption").Value = RSBOL("LotNbrCaption").Value
    CurrentLine = 10146
    CMD.Parameters("@ShipToState").Value = RSBOL("ShipToState").Value
    CurrentLine = 10147
    CMD.Parameters("@BatchNumber").Value = RSBOL("BatchNumber").Value
    CurrentLine = 10148
    CMD.Parameters("@Carrier_ID").Value = RSBOL("Carrier_ID").Value
    CurrentLine = 10149
    CMD.Parameters("@Carrier_Name").Value = RSBOL("Carrier_Name").Value
    CurrentLine = 10150
    CMD.Parameters("@WeightTktNo").Value = RSBOL("WeightTktNo").Value
    CurrentLine = 10151
    CMD.Parameters("@MASDeliveryLoc").Value = RSBOL("MASDeliveryLoc").Value
    CurrentLine = 10152
    CMD.Parameters("@FrontSrcTankNbrOne").Value = RSBOLExt("FrontSrcTankNbrOne").Value
    CurrentLine = 10153
    CMD.Parameters("@FrontSrcTankNbrTwo").Value = RSBOLExt("FrontSrcTankNbrTwo").Value
    CurrentLine = 10154
    CMD.Parameters("@FrontSrcTankNbrThree").Value = RSBOLExt("FrontSrcTankNbrThree").Value
    CurrentLine = 10155
    CMD.Parameters("@FrontSrcProductIDOne").Value = RSBOLExt("FrontSrcProductIDOne").Value
    CurrentLine = 10156
    CMD.Parameters("@FrontSrcProductOne").Value = RSBOLExt("FrontSrcProductOne").Value
    CurrentLine = 10157
    CMD.Parameters("@FrontSrcProductClassOne").Value = RSBOLExt("FrontSrcProductClassOne").Value
    CurrentLine = 10158
    CMD.Parameters("@FrontSrcProductIDTwo").Value = RSBOLExt("FrontSrcProductIDTwo").Value
    CurrentLine = 10159
    CMD.Parameters("@FrontSrcProductTwo").Value = RSBOLExt("FrontSrcProductTwo").Value
    CurrentLine = 10160
    CMD.Parameters("@FrontSrcProductClassTwo").Value = RSBOLExt("FrontSrcProductClassTwo").Value
    CurrentLine = 10161
    CMD.Parameters("@FrontSrcProductIDThree").Value = RSBOLExt("FrontSrcProductIDThree").Value
    CurrentLine = 10162
    CMD.Parameters("@FrontSrcProductThree").Value = RSBOLExt("FrontSrcProductThree").Value
    CurrentLine = 10163
    CMD.Parameters("@FrontSrcProductClassThree").Value = RSBOLExt("FrontSrcProductClassThree").Value
    CurrentLine = 10164
    CMD.Parameters("@RearSrcTankNbrOne").Value = RSBOLExt("RearSrcTankNbrOne").Value
    CurrentLine = 10165
    CMD.Parameters("@RearSrcTankNbrTwo").Value = RSBOLExt("RearSrcTankNbrTwo").Value
    CurrentLine = 10166
    CMD.Parameters("@RearSrcTankNbrThree").Value = RSBOLExt("RearSrcTankNbrThree").Value
    CurrentLine = 10167
    CMD.Parameters("@RearSrcProductIDOne").Value = RSBOLExt("RearSrcProductIDOne").Value
    CurrentLine = 10168
    CMD.Parameters("@RearSrcProductOne").Value = RSBOLExt("RearSrcProductOne").Value
    CurrentLine = 10169
    CMD.Parameters("@RearSrcProductClassOne").Value = RSBOLExt("RearSrcProductClassOne").Value
    CurrentLine = 10170
    CMD.Parameters("@RearSrcProductIDTwo").Value = RSBOLExt("RearSrcProductIDTwo").Value
    CurrentLine = 10171
    CMD.Parameters("@RearSrcProductTwo").Value = RSBOLExt("RearSrcProductTwo").Value
    CurrentLine = 10172
    CMD.Parameters("@RearSrcProductClassTwo").Value = RSBOLExt("RearSrcProductClassTwo").Value
    CurrentLine = 10173
    CMD.Parameters("@RearSrcProductIDThree").Value = RSBOLExt("RearSrcProductIDThree").Value
    CurrentLine = 10174
    CMD.Parameters("@RearSrcProductThree").Value = RSBOLExt("RearSrcProductThree").Value
    CurrentLine = 10175
    CMD.Parameters("@RearSrcProductClassThree").Value = RSBOLExt("RearSrcProductClassThree").Value
    CurrentLine = 10176
    CMD.Parameters("@SrcTankNbrOne").Value = RSBOLExt("SrcTankNbrOne").Value
    CurrentLine = 10177
    CMD.Parameters("@SrcTankNbrTwo").Value = RSBOLExt("SrcTankNbrTwo").Value
    CurrentLine = 10178
    CMD.Parameters("@SrcTankNbrThree").Value = RSBOLExt("SrcTankNbrThree").Value
    CurrentLine = 10179
    CMD.Parameters("@SrcProductIDOne").Value = RSBOLExt("SrcProductIDOne").Value
    CurrentLine = 10180
    CMD.Parameters("@SrcProductOne").Value = RSBOLExt("SrcProductOne").Value
    CurrentLine = 10181
    CMD.Parameters("@SrcProductClassOne").Value = RSBOLExt("SrcProductClassOne").Value
    CurrentLine = 10182
    CMD.Parameters("@SrcProductIDTwo").Value = RSBOLExt("SrcProductIDTwo").Value
    CurrentLine = 10183
    CMD.Parameters("@SrcProductTwo").Value = RSBOLExt("SrcProductTwo").Value
    CurrentLine = 10184
    CMD.Parameters("@SrcProductClassTwo").Value = RSBOLExt("SrcProductClassTwo").Value
    CurrentLine = 10185
    CMD.Parameters("@SrcProductIDThree").Value = RSBOLExt("SrcProductIDThree").Value
    CurrentLine = 10186
    CMD.Parameters("@SrcProductThree").Value = RSBOLExt("SrcProductThree").Value
    CurrentLine = 10187
    CMD.Parameters("@SrcProductClassThree").Value = RSBOLExt("SrcProductClassThree").Value
    CurrentLine = 10188
    CMD.Parameters("@FrontSrcQtyOne").Value = RSBOLExt("FrontSrcQtyOne").Value
    CurrentLine = 10189
    CMD.Parameters("@FrontSrcQtyTwo").Value = RSBOLExt("FrontSrcQtyTwo").Value
    CurrentLine = 10190
    CMD.Parameters("@FrontSrcQtyThree").Value = RSBOLExt("FrontSrcQtyThree").Value
    CurrentLine = 10191
    CMD.Parameters("@RearSrcQtyOne").Value = RSBOLExt("RearSrcQtyOne").Value
    CurrentLine = 10192
    CMD.Parameters("@RearSrcQtyTwo").Value = RSBOLExt("RearSrcQtyTwo").Value
    CurrentLine = 10193
    CMD.Parameters("@RearSrcQtyThree").Value = RSBOLExt("RearSrcQtyThree").Value
    CurrentLine = 10194
    CMD.Parameters("@SrcQtyOne").Value = RSBOLExt("SrcQtyOne").Value
    CurrentLine = 10195
    CMD.Parameters("@SrcQtyTwo").Value = RSBOLExt("SrcQtyTwo").Value
    CurrentLine = 10196
    CMD.Parameters("@SrcQtyThree").Value = RSBOLExt("SrcQtyThree").Value
    CurrentLine = 10197
    CMD.Parameters("@COATestRsltsKey").Value = RSBOLExt("COATestRsltsKey").Value
    
    CurrentLine = 10198
    CMD.Parameters("@IsTruckBlend").Value = RSBOLExt("IsTruckBlend").Value
   
    CurrentLine = 11100
    CMD.Parameters("@LMEDestLocationCode").Value = RSBOLExt("LMEDestLocationCode").Value
    CurrentLine = 11101
    CMD.Parameters("@LMEDestAddress1").Value = RSBOLExt("LMEDestAddress1").Value
    CurrentLine = 11102
    CMD.Parameters("@LMEDestAddress2").Value = RSBOLExt("LMEDestAddress2").Value
    CurrentLine = 11103
    CMD.Parameters("@LMEDestcity_Name").Value = RSBOLExt("LMEDestcity_Name").Value
    CurrentLine = 11104
    CMD.Parameters("@LMEDestState").Value = RSBOLExt("LMEDestState").Value
    CurrentLine = 11105
    CMD.Parameters("@LMEDestzip_code").Value = RSBOLExt("LMEDestzip_code").Value
    CurrentLine = 11106
    CMD.Parameters("@LMEDestCustomer_ID").Value = RSBOLExt("LMEDestCustomer_ID").Value
    CurrentLine = 11107
    CMD.Parameters("@LMEDestAddrName").Value = RSBOLExt("LMEDestAddrName").Value
    
    
    CurrentLine = 10199
    CMD.Parameters("@TranType").Value = TranType
    CurrentLine = 10200
    CMD.Parameters("@status").Value = CreateStatus
    CurrentLine = 10201
    CMD.Parameters("@statustext").Value = CreateMessage
    CurrentLine = 10202
    CMD.Parameters("@statuscolumn").Value = ErrorColumn
    CurrentLine = 10203
    CMD.Parameters("@BOLNo").Value = BOLNo
    CurrentLine = 10204
    CMD.Parameters("@oRtnVal").Value = oRtnVal
    CurrentLine = 10205
    CMD.Parameters("@oErrMsg").Value = oErrMsg
        
    CurrentLine = 10206
    gbLastActivityTime = Date + Time
    
    CurrentLine = 70
    'Execute The procedure
    CMD.Execute
       
    gbLastActivityTime = Date + Time

    CurrentLine = 71
AfterExecute:
    CurrentLine = 72
    'Obtain Output Parameters
    CreateStatus = ConvertToDouble(CMD.Parameters.Item("@status").Value)
    CurrentLine = 73
    CreateMessage = ConvertToString(CMD.Parameters.Item("@statustext").Value)
    CurrentLine = 74
    ErrorColumn = ConvertToString(CMD.Parameters.Item("@statuscolumn").Value)
    CurrentLine = 75
    BOLNo = ConvertToString(CMD.Parameters.Item("@BOLNo").Value)

    CurrentLine = 75
    oErrMsg = ConvertToString(CMD.Parameters.Item("@oErrMsg").Value)

    CurrentLine = 75
    oRtnVal = CMD.Parameters.Item("@oRtnVal").Value
    
    'Check for Success
    'CurrentLine = 76
    If oRtnVal = 1 Then
        'Success
        CurrentLine = 78
        RunAPICustOwned = True
        
        'Check for a return message
        If Trim(CreateMessage) <> Empty Or Trim(oErrMsg) <> Empty Then
            
            CurrentLine = 88
            lcErrMsg = Me.Name & ".RunAPICustOwned(PrintValues As BOLFields) As Boolean" & vbCrLf & _
            "CreateStatus: " & CreateStatus & vbCrLf & _
            "Column: " & ErrorColumn & vbCrLf & _
            "BOLNo: " & BOLNo & vbCrLf & _
            "CreateMessage: " & CreateMessage & vbCrLf & _
            "Stored Procedure Msg: " & oErrMsg
            
            'Save Off the message
            If RSBOL.State = 1 Then
                CurrentLine = 95
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 96
                    InsErrorLog 0, Date + Time, lcErrMsg
                Else
                    CurrentLine = 97
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
                End If
            Else
                CurrentLine = 98
                InsErrorLog 0, Date + Time, lcErrMsg
            End If
            
            MsgBox lcErrMsg, vbInformation, "Message From SAGE 500"
        
        End If
    Else
        'Falure
        CurrentLine = 84
        RunAPICustOwned = False
    
        CurrentLine = 86
        If Trim(CreateMessage) <> Empty Then
            
            CurrentLine = 88
            lcErrMsg = Me.Name & ".RunAPICustOwned(PrintValues As BOLFields) As Boolean" & vbCrLf & _
            "CreateStatus: " & CreateStatus & vbCrLf & _
            "ErrorColumn: " & ErrorColumn & vbCrLf & _
            "BOLNo: " & BOLNo & vbCrLf & _
            "CreateMessage: " & CreateMessage & vbCrLf & _
            "Stored Procedure ErrMsg: " & oErrMsg
            
            
        Else
            CurrentLine = 91
            lcErrMsg = Me.Name & ".RunAPICustOwned(PrintValues As BOLFields) As Boolean" & vbCrLf & _
            "CreateStatus: " & CreateStatus & vbCrLf & _
            "ErrorColumn: " & ErrorColumn & vbCrLf & _
            "BOLNo: " & BOLNo & vbCrLf & _
            "CreateMessage: " & "The BOL IM Transaction API was NOT created Successfully." & vbCrLf & _
            "Stored Procedure ErrMsg: " & oErrMsg
        
        End If
        
        CurrentLine = 94
        
        If RSBOL.State = 1 Then
            CurrentLine = 95
            If RSBOL.EOF Or RSBOL.BOF Then
                CurrentLine = 96
                InsErrorLog 0, Date + Time, lcErrMsg
            Else
                CurrentLine = 97
                InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
            End If
        Else
            CurrentLine = 98
            InsErrorLog 0, Date + Time, lcErrMsg
        End If
        
        CurrentLine = 99
        
        ErrMsg = lcErrMsg
    End If
    

    CurrentLine = 105
    
CleanUP:
    CurrentLine = 106
    Err.Clear
    CurrentLine = 107
    On Error Resume Next
    CurrentLine = 108
    Set CMD = Nothing

    CurrentLine = 1009
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lcErrMsg = Me.Name & ".RunAPICustOwned(PrintValues As BOLFields, Optional SuppressMsg As Boolean = False) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & _
    "SuppressMsg = " & SuppressMsg & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    If SuppressMsg = False Then
        MsgBox lcErrMsg, vbCritical, "Error"
    End If

    lcErrMsg = lcErrMsg & GetPrintValuesStr(PrintValues)

    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, lcErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, lcErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, lcErrMsg
    End If

    Err.Clear
    On Error Resume Next
    
'    gbMASConn.RollbackTrans
    
    ErrMsg = lcErrMsg
    
    gbLastActivityTime = Date + Time
    
    
    GoTo CleanUP


End Function





Private Function RunCreditRtnAPI(PrintValues As BOLFields, Optional SuppressMsg As Boolean = False) As Boolean

    On Error GoTo Error
    Dim CMD As New ADODB.Command

    Dim CreateStatus As Integer
    Dim CreateMessage As String
    Dim ErrorColumn As String
    Dim BOLNo As String
    Dim sSQL As String
    Dim CurrentLine As Integer

    gbLastActivityTime = Date + Time
    
    '*********************************************************************************
    '*********************************************************************************
    '*********************************************************************************
    'RKL DEJ 2017-12-05 (START)
    Dim ErrMsg As String
    
    CurrentLine = -1
    If gbCustOwnedProd = True Then
        RunCreditRtnAPI = RunAPICustOwned(PrintValues, SuppressMsg, ErrMsg, 702)
        
        If Trim(ErrMsg) <> Empty Then
            MsgBox Me.Name & "RunCreditRtnAPI.RunAPICustOwned()" & vbCrLf & _
            "Error: " & ErrMsg, vbExclamation, "Sales Return Failure"
        End If
        
        Exit Function
    End If
    'RKL DEJ 2017-12-05 (STOP)
    '*********************************************************************************
    '*********************************************************************************
    '*********************************************************************************
    
    CurrentLine = 0
    RunCreditRtnAPI = False
    
    CurrentLine = 1
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 2
        If HaveLostMASConnection = True Then
            CurrentLine = 3
            'No longer connected to MAS 500
            Exit Function
        End If
    End If
    
    CurrentLine = 4
    If gbMASConn.State <> 1 Then
        CurrentLine = 5
        If SuppressMsg = False Then
            CurrentLine = 6
            MsgBox "The connection to MAS has been lost.  The Credit Return was not created.", vbExclamation, "Credit Return Not Created"
        End If
        CurrentLine = 7
        Exit Function
    End If
    
'    gbMASConn.BeginTrans
    
    CurrentLine = 8
'   Create an empty ScalePass Return temp table used in the spsoScalePutCreditReturn_SGS
    sSQL = "IF OBJECT_ID('tempdb..#tsoScalePassReturn_SGS') IS NOT NULL" & vbCrLf
    CurrentLine = 9
    sSQL = sSQL & " TRUNCATE TABLE #tsoScalePassReturn_SGS " & vbCrLf
    CurrentLine = 10
    sSQL = sSQL & " ELSE BEGIN" & vbCrLf
    CurrentLine = 11
    sSQL = sSQL & " CREATE TABLE #tsoScalePassReturn_SGS" & vbCrLf
    CurrentLine = 12
    sSQL = sSQL & " (CustID varchar(12) NOT NULL" & vbCrLf
    CurrentLine = 13
    sSQL = sSQL & " ,ContractNo varchar(10) NOT NULL" & vbCrLf
    CurrentLine = 14
    sSQL = sSQL & " ,WhseID varchar(6) NOT NULL" & vbCrLf
    CurrentLine = 15
    sSQL = sSQL & " ,ItemID varchar(30) NOT NULL" & vbCrLf
    CurrentLine = 16
    sSQL = sSQL & " ,BinID varchar(15) NOT NULL" & vbCrLf
    CurrentLine = 17
    sSQL = sSQL & " ,ReasonCodeID varchar(15) NOT NULL" & vbCrLf
    CurrentLine = 18
    sSQL = sSQL & " ,BOLNo varchar(15) NOT NULL" & vbCrLf
    CurrentLine = 19
    sSQL = sSQL & " ,ReturnQty DECIMAL(16,8) NOT NULL) END"
    
    CurrentLine = 20
    gbMASConn.Execute sSQL
    
    CurrentLine = 21
    Set CMD.ActiveConnection = gbMASConn
    CurrentLine = 22
    CMD.CommandType = adCmdStoredProc
    CurrentLine = 23
    CMD.CommandText = "spsoScalePutCreditReturn_SGS"
    CurrentLine = 24
    CMD.Parameters.Refresh
    CurrentLine = 25
    CMD.CommandTimeout = 0      'Added to prevent time outs

    CurrentLine = 26
    'Assign input parameter Values
    CMD.Parameters("@userid").Value = Left(CStr(PrintValues.UserID), 16)
    CurrentLine = 27
    CMD.Parameters("@loadid").Value = ConvertToDouble(PrintValues.LoadID)
    CurrentLine = 28
    CMD.Parameters("@prebillid").Value = ConvertToDouble(PrintValues.PreBillID)
    CurrentLine = 29
    CMD.Parameters("@facility").Value = ConvertToDouble(PrintValues.Facility)
    CurrentLine = 30
    CMD.Parameters("@weightticket").Value = ConvertToDouble(PrintValues.WeightTicketNo) 'txtWeightTicket
    CurrentLine = 31
    CMD.Parameters("@scalepass").Value = ConvertToDouble(PrintValues.BOLNumber)
    
    CurrentLine = 32
    If IsDate(PrintValues.BOLDate) = True Then
        CurrentLine = 3201
        CMD.Parameters("@shipmentdate").Value = CDate(PrintValues.BOLDate)
        CurrentLine = 3202
    Else
        CurrentLine = 3203
        CMD.Parameters("@shipmentdate").Value = Null
        CurrentLine = 3204
    End If
    
    CurrentLine = 33
    CMD.Parameters("@customer").Value = Left(PrintValues.Customer, 8)
    CurrentLine = 34
    CMD.Parameters("@customername").Value = Left(PrintValues.CustomerName, 40)
    CurrentLine = 35
    CMD.Parameters("@contract").Value = ConvertToDouble(PrintValues.ContractNo)  'txtContract.Text
    CurrentLine = 36
    CMD.Parameters("@destination").Value = Left(PrintValues.Destination, 60)
    CurrentLine = 37
    CMD.Parameters("@projectnumber").Value = Left(PrintValues.ProjectNo, 60)
    CurrentLine = 38
    CMD.Parameters("@projectdescription").Value = Left(PrintValues.ProjectDescription, 60)
    CurrentLine = 39
    CMD.Parameters("@carrier").Value = Left(PrintValues.Carrier, 30)
    CurrentLine = 40
    CMD.Parameters("@truck").Value = Left(PrintValues.TruckNo, 16)
    CurrentLine = 41
    CMD.Parameters("@trailer1").Value = Left(PrintValues.Trailer1No, 16)
    CurrentLine = 42
    CMD.Parameters("@trailer2").Value = Left(PrintValues.Trailer2No, 16)

    CurrentLine = 43
    If IsDate(PrintValues.LoadTime) = True Then
        CurrentLine = 4301
        CMD.Parameters("@loadtime").Value = CDate(PrintValues.LoadTime)
        CurrentLine = 4302
    Else
        CurrentLine = 4303
        CMD.Parameters("@loadtime").Value = Null
        CurrentLine = 4304
    End If
    
    CurrentLine = 44
    If IsDate(PrintValues.DeliveryTime) = True Then
        CurrentLine = 4401
        CMD.Parameters("@deliverytime").Value = CDate(PrintValues.DeliveryTime)
        CurrentLine = 4402
    Else
        CurrentLine = 4403
        CMD.Parameters("@deliverytime").Value = Null
        CurrentLine = 4404
    End If
    
    CurrentLine = 45
    If IsDate(PrintValues.LoadTimeIn) = True Then
        CurrentLine = 4501
        CMD.Parameters("@loadtimein").Value = CDate(PrintValues.LoadTimeIn)
        CurrentLine = 4502
    Else
        CurrentLine = 4503
        CMD.Parameters("@loadtimein").Value = Null
        CurrentLine = 4504
    End If
    
    CurrentLine = 46
    If IsDate(PrintValues.LoadTimeOut) = True Then
        CurrentLine = 4601
        CMD.Parameters("@loadtimeout").Value = CDate(PrintValues.LoadTimeOut)
        CurrentLine = 4602
    Else
        CurrentLine = 4603
        CMD.Parameters("@loadtimeout").Value = Null
        CurrentLine = 4604
    End If
    
    CurrentLine = 47
    If IsDate(PrintValues.BOLDate) = True Then
        CurrentLine = 4701
        CMD.Parameters("@timearrived").Value = CDate(PrintValues.BOLDate)
        CurrentLine = 4702
    Else
        CurrentLine = 4703
        CMD.Parameters("@timearrived").Value = Null
        CurrentLine = 4704
    End If

    CurrentLine = 48
    CMD.Parameters("@remark").Value = CStr(Left(PrintValues.Remarks, 60))
    CurrentLine = 49
    CMD.Parameters("@penetration").Value = ConvertToDouble(PrintValues.Penetration)
    CurrentLine = 50
    CMD.Parameters("@penetrationtemperature").Value = ConvertToDouble(PrintValues.PenetrationTemp)
    CurrentLine = 51
    CMD.Parameters("@penetrationremark").Value = CStr(Left(PrintValues.PenetrationRemark, 16))
    CurrentLine = 52
    CMD.Parameters("@flash").Value = ConvertToDouble(PrintValues.Flashpoint)
    CurrentLine = 53
    CMD.Parameters("@lbspergallon").Value = ConvertToDouble(PrintValues.LbsPerGal)
    CurrentLine = 54
    CMD.Parameters("@specificgravity").Value = ConvertToDouble(PrintValues.SpecGravity)
    CurrentLine = 55
    CMD.Parameters("@viscosity").Value = ConvertToDouble(PrintValues.Viscosity)
    CurrentLine = 56
    CMD.Parameters("@viscositytemperature").Value = ConvertToDouble(PrintValues.ViscosityTemp)
    CurrentLine = 57
    CMD.Parameters("@viscosityremark").Value = CStr(Left(PrintValues.ViscosityRemark, 16))
    CurrentLine = 58
    CMD.Parameters("@specificationremark").Value = CStr(Left(PrintValues.ProductSpecRemarks, 16))
    CurrentLine = 59
    CMD.Parameters("@additive").Value = CStr(Left(PrintValues.AdditiveID, 16))
    CurrentLine = 60
    CMD.Parameters("@additivepercent").Value = ConvertToDouble(PrintValues.AdditivePercent)
    CurrentLine = 61
    CMD.Parameters("@temperature").Value = ConvertToDouble(PrintValues.Temp)
    CurrentLine = 62
    CMD.Parameters("@grossgallons").Value = ConvertToDouble(PrintValues.GrossGal)
    CurrentLine = 63
    CMD.Parameters("@netgallons").Value = ConvertToDouble(PrintValues.NetGal)

    CurrentLine = 64
    CMD.Parameters("@grossweight1").Value = ConvertToDouble(PrintValues.GrossWtLbs1)
    CurrentLine = 65
    CMD.Parameters("@grossweight2").Value = ConvertToDouble(PrintValues.GrossWtLbs2)
    CurrentLine = 66
    CMD.Parameters("@tareweight1").Value = ConvertToDouble(PrintValues.TareWtLbs1)
    CurrentLine = 67
    CMD.Parameters("@tareweight2").Value = ConvertToDouble(PrintValues.TareWtLbs2)
    
    CurrentLine = 68
    CMD.Parameters("@item").Value = CStr(Left(PrintValues.ProductID, 16))
    CurrentLine = 69
    CMD.Parameters("@itemprice").Value = 0
    CurrentLine = 70
    CMD.Parameters("@additiveprice").Value = 0
    CurrentLine = 71
    CMD.Parameters("@inbound").Value = CStr(Left(PrintValues.InBoundOperator, 3))
    CurrentLine = 72
    CMD.Parameters("@outbound").Value = CStr(Left(PrintValues.OutBoundOperator, 3))

    CurrentLine = 73
    CMD.Parameters("@ladingsibling").Value = CStr(Left(PrintValues.LadingSibling, 23))
    CurrentLine = 74
    CMD.Parameters("@binid").Value = CStr(Left(PrintValues.TankNumber, 15))

    CurrentLine = 75
    CMD.Parameters("@customerpo").Value = CStr(Left(PrintValues.PONo, 23))
    CurrentLine = 76
    CMD.Parameters("@lot").Value = PrintValues.LotNumber
    
    CurrentLine = 77
    CMD.Parameters("@IsTransfer").Value = PrintValues.IsTransfer
    CurrentLine = 78
    CMD.Parameters("@IsBetweenCompanies").Value = PrintValues.IsBtwnCmpny
    CurrentLine = 79
    CMD.Parameters("@FromCompanyID").Value = Left(PrintValues.FromCompany, 3)
    CurrentLine = 80
    CMD.Parameters("@ToCompanyID").Value = Left(PrintValues.ToCompany, 3)
    CurrentLine = 81
    CMD.Parameters("@FromWhseID").Value = Left(PrintValues.FromWhse, 15)
    CurrentLine = 82
    CMD.Parameters("@ToWhseID").Value = Left(PrintValues.ToWhse, 15)
    
    'RKL DEJ 2016-09-07 (START)
    CurrentLine = 8201
    CMD.Parameters("@LocationCode").Value = Null
    CurrentLine = 8202
    CMD.Parameters("@ShipToAddrLine1").Value = Null
    CurrentLine = 8203
    CMD.Parameters("@ShipToAddrLine2").Value = Null
    CurrentLine = 8204
    CMD.Parameters("@ShipToAddrLine3").Value = Null
    CurrentLine = 8205
    CMD.Parameters("@ShipToAddrLine4").Value = Null
    CurrentLine = 8206
    CMD.Parameters("@ShipToAddrLine5").Value = Null
    CurrentLine = 8207
    CMD.Parameters("@ShipToCity").Value = Null
    CurrentLine = 8208
    CMD.Parameters("@ShipToState").Value = Null
    CurrentLine = 8209
    CMD.Parameters("@ShipToPostalCode").Value = Null
    
    CurrentLine = 8210
    CMD.Parameters("@SecurityTapeNumber").Value = Left(PrintValues.SecTapeNbr, 75)
    
    CurrentLine = 8211
    CMD.Parameters("@BatchNumber").Value = Left(PrintValues.BatchNumber, 100)
    CurrentLine = 8212
    CMD.Parameters("@WeightTktNo").Value = Left(PrintValues.WeightTicketNo, 30)
    CurrentLine = 8213
    CMD.Parameters("@Carrier_ID").Value = Left(PrintValues.Carrier_ID, 10)
    CurrentLine = 8214
    CMD.Parameters("@Carrier_Name").Value = Left(PrintValues.Carrier_Name, 30)
    CurrentLine = 8215
    CMD.Parameters("@Purchase_Order").Value = Left(PrintValues.Purchase_Order, 255)
    CurrentLine = 8216
    'RKL DEJ 2016-09-07 (STOP)
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 83
    'Execute The procedure
    CMD.Execute

    gbLastActivityTime = Date + Time
    
    CurrentLine = 84
AfterExecute:
    CurrentLine = 85
    'Obtain Output Parameters
    CreateStatus = ConvertToDouble(CMD.Parameters.Item("@status").Value)
    CurrentLine = 86
    CreateMessage = ConvertToString(CMD.Parameters.Item("@statustext").Value)
    CurrentLine = 87
    ErrorColumn = ConvertToString(CMD.Parameters.Item("@statuscolumn").Value)
    CurrentLine = 88
    BOLNo = ConvertToString(CMD.Parameters.Item("@BOLNo").Value)

    CurrentLine = 89
    Select Case CreateStatus
        Case 0
            CurrentLine = 9
'            gbMASConn.CommitTrans
            
            CurrentLine = 91
            RunCreditRtnAPI = True
            
            CurrentLine = 92
            If SuppressMsg = False Then
                CurrentLine = 93
                If Trim(CreateMessage) <> Empty Then
                    CurrentLine = 94
                    MsgBox CreateMessage, vbInformation, "Credit Return API Success"
                Else
                    CurrentLine = 95
                    MsgBox "The BOL Credit Return API was Successfull.", vbInformation, "Credit Return API Success"
                End If
            End If
            CurrentLine = 96
            
        Case Else
            CurrentLine = 97
'            gbMASConn.RollbackTrans
            
            RunCreditRtnAPI = False
            
            CurrentLine = 98
            If SuppressMsg = False Then
                CurrentLine = 99
                If Trim(CreateMessage) <> Empty Then
                    CurrentLine = 100
                    MsgBox CreateMessage, vbExclamation, "Credit Return API Failed"
                    
                    CurrentLine = 101
                    
                    ErrMsg = Me.Name & ".RunCreditRtnAPI(PrintValues As BOLFields) As Boolean" & vbCrLf & _
                    "CreateStatus = " & CreateStatus & vbCrLf & _
                    "ErrorColumn = " & ErrorColumn & vbCrLf & _
                    "BOLNo = " & BOLNo & vbCrLf & _
                    "CreateMessage = " & CreateMessage
                    
                    CurrentLine = 102
                Else
                    CurrentLine = 103
                    
                    MsgBox "The BOL Credit Return API was NOT created Successfully.", vbExclamation, "Credit Return API Failed"
                
                    CurrentLine = 104
                    
                    ErrMsg = Me.Name & ".RunCreditRtnAPI(PrintValues As BOLFields) As Boolean" & vbCrLf & _
                    "CreateStatus = " & CreateStatus & vbCrLf & _
                    "ErrorColumn = " & ErrorColumn & vbCrLf & _
                    "BOLNo = " & BOLNo & vbCrLf & _
                    "CreateMessage = " & "The BOL Credit Return API was NOT created Successfull."
                    
                    CurrentLine = 105
                End If
            Else
                CurrentLine = 106
                
                ErrMsg = Me.Name & ".RunCreditRtnAPI(PrintValues As BOLFields) As Boolean" & vbCrLf & _
                "CreateStatus = " & CreateStatus & vbCrLf & _
                "ErrorColumn = " & ErrorColumn & vbCrLf & _
                "BOLNo = " & BOLNo & vbCrLf & _
                "CreateMessage = " & CreateMessage
            
                CurrentLine = 107
            End If
            
            CurrentLine = 108
            
            If RSBOL.State = 1 Then
                CurrentLine = 109
                If RSBOL.EOF Or RSBOL.BOF Then
                    CurrentLine = 110
                    InsErrorLog 0, Date + Time, ErrMsg
                Else
                    CurrentLine = 111
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                End If
            Else
                CurrentLine = 112
                InsErrorLog 0, Date + Time, ErrMsg
            End If
            
            CurrentLine = 113
            
    End Select
    
    CurrentLine = 114
    
    
CleanUP:
    CurrentLine = 115
    Err.Clear
    CurrentLine = 116
    On Error Resume Next
    CurrentLine = 117
    Set CMD = Nothing

    CurrentLine = 118
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    ErrMsg = Me.Name & ".RunCreditRtnAPI(PrintValues As BOLFields, Optional SuppressMsg As Boolean = False) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & _
    "SuppressMsg = " & SuppressMsg & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
        
    If SuppressMsg = False Then
        MsgBox ErrMsg, vbCritical, "Error"
    End If
    
    ErrMsg = ErrMsg & GetPrintValuesStr(PrintValues)

    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, ErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, ErrMsg
    End If

    Err.Clear
    On Error Resume Next
    
'    gbMASConn.RollbackTrans

    gbLastActivityTime = Date + Time
    
    GoTo CleanUP


End Function




Public Function UpdBOLTblLog(TblKey As Double, BOLPrtCrtly As Boolean, Optional SuppressMsg As Boolean = False) As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    'Validate MAS Connection
    If gbLocalMode = False Then
        If HaveLostMASConnection = True Then
            'No longer connected to MAS 500
            Exit Function
        End If
    End If
    
    
    CurrentLine = 1
    
    SQL = "Update tsoBOL_Log_SGS Set DidBOLPrintCorrectly = '"
    
    CurrentLine = 2
    
    If BOLPrtCrtly = True Then
        SQL = SQL & "Yes" & "' "
    Else
        SQL = SQL & "No" & "' "
    End If
    
    CurrentLine = 3
    
    SQL = SQL & "Where TblKey = " & TblKey
    
    CurrentLine = 4
    
    gbMASConn.Execute SQL
    
    CurrentLine = 5
    
    UpdBOLTblLog = True
    
    CurrentLine = 6
    
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    Exit Function
    
Error:
    ErrMsg = Me.Name & ".UpdBOLTblLog(TblKey As Double, BOLPrtCrtly As Boolean) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine
    
    If SuppressMsg = False Then
        MsgBox ErrMsg, vbExclamation, "Error"
    End If
    
    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, ErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, ErrMsg
    End If
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function


Sub SetupDropDowns()
    On Error GoTo Error
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim CurrentLine As Integer
    
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    'Validate MAS Connection
    If gbLocalMode = False Then
        CurrentLine = 2
        If HaveLostMASConnection = True Then
            CurrentLine = 3
            'No longer connected to MAS 500
        End If
    End If
    
    CurrentLine = 4
    SQL = "Select Distinct ItemShortDesc From vluGetItemAndWarehouse_SGS Where ItemClassID IN('Cutback', 'Antistrips', 'Fuels') and FacilitySeqNo = " & ConvertToDouble(gbPlantPrefix) & " And WhseID = '" & gbWhseID & "' "
'    SQL = "Select * From vluGetItemAndWarehouse_SGS Where ItemClassID IN('Cutback', 'Antistrips', 'Dilute Emuls') and FacilitySeqNo = " & ConvertToDouble(gbPlantPrefix)
'    SQL = "Select * From vluGetItemAndWarehouse_SGS Where ItemClassID IN('Cutback', 'Antistrips', 'Dilute Emuls') and WhseID = 'B1' "
    
    CurrentLine = 5
'    RS.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    RS.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
    
    CurrentLine = 6
    While Not RS.EOF And Not RS.BOF
        CurrentLine = 7
        txtFrontAdditive.AddItem RS("ItemShortDesc").Value ', RS("ItemKey").Value
        CurrentLine = 8
        txtRearAdditive.AddItem RS("ItemShortDesc").Value ', RS("ItemKey").Value
    
        CurrentLine = 9
        RS.MoveNext
    Wend
    
    CurrentLine = 10
    
CleanUP:
    On Error Resume Next
    CurrentLine = 11
    If RS.State = 1 Then RS.Close
    CurrentLine = 12
    Set RS = Nothing
    CurrentLine = 13
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    ErrMsg = Me.Name & ".SetupDropDowns()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    MsgBox ErrMsg, vbCritical, "Error"
    
    ErrMsg = Empty
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Sub

'Return The New Key to the log table for update of the field: DidBOLPrintCorrectly
Private Function InstsoBOLTblLog(PrintValues As BOLFields, Optional ByRef NewKey As Double = 0, Optional SuppressMsg As Boolean = False) As Boolean

    On Error GoTo Error
    Dim CMD As New ADODB.Command

    Dim CreateStatus As Integer
    Dim CreateMessage As String

    gbLastActivityTime = Date + Time
    
    InstsoBOLTblLog = False
    
    'Validate MAS Connection
    If gbLocalMode = False Then
        If HaveLostMASConnection = True Then
            'No longer connected to MAS 500
            Exit Function
        End If
    End If
    
    If gbMASConn.State <> 1 Then
        If SuppressMsg = False Then
            MsgBox "The connection to MAS has been lost.  The Sales order was not created.", vbExclamation, "SO Not Created"
        End If
        Exit Function
    End If
    
    gbMASConn.BeginTrans
    
    Set CMD.ActiveConnection = gbMASConn
    CMD.CommandType = adCmdStoredProc
    CMD.CommandText = "spsoScalePut_Ins_tsoBOL_Log_SGS"
    CMD.Parameters.Refresh
    CMD.CommandTimeout = 0      'Added to prevent time outs

    'Assign input parameter Values
    CMD.Parameters("@userid").Value = Left(CStr(PrintValues.UserID), 16)
    CMD.Parameters("@loadid").Value = ConvertToDouble(PrintValues.LoadID)
    CMD.Parameters("@prebillid").Value = ConvertToDouble(PrintValues.PreBillID)
    CMD.Parameters("@facility").Value = ConvertToDouble(PrintValues.Facility)
    CMD.Parameters("@weightticket").Value = ConvertToDouble(PrintValues.WeightTicketNo) 'txtWeightTicket
    CMD.Parameters("@scalepass").Value = ConvertToDouble(PrintValues.BOLNumber)
    
    If IsDate(PrintValues.BOLDate) = True Then
        CMD.Parameters("@shipmentdate").Value = CDate(PrintValues.BOLDate)
    Else
        CMD.Parameters("@shipmentdate").Value = Null
    End If
    
    CMD.Parameters("@customer").Value = Left(PrintValues.Customer, 8)
    CMD.Parameters("@customername").Value = Left(PrintValues.CustomerName, 40)
    CMD.Parameters("@contract").Value = ConvertToDouble(PrintValues.ContractNo)  'txtContract.Text
    CMD.Parameters("@destination").Value = Left(PrintValues.Destination, 60)
    CMD.Parameters("@projectnumber").Value = Left(PrintValues.ProjectNo, 60)
    CMD.Parameters("@projectdescription").Value = Left(PrintValues.ProjectDescription, 60)
    CMD.Parameters("@carrier").Value = Left(PrintValues.Carrier, 30)
    CMD.Parameters("@truck").Value = Left(PrintValues.TruckNo, 16)
    CMD.Parameters("@trailer1").Value = Left(PrintValues.Trailer1No, 16)
    CMD.Parameters("@trailer2").Value = Left(PrintValues.Trailer2No, 16)

    If IsDate(PrintValues.LoadTime) = True Then
        CMD.Parameters("@loadtime").Value = CDate(PrintValues.LoadTime)
    Else
        CMD.Parameters("@loadtime").Value = Null
    End If
    
    If IsDate(PrintValues.DeliveryTime) = True Then
        CMD.Parameters("@deliverytime").Value = CDate(PrintValues.DeliveryTime)
    Else
        CMD.Parameters("@deliverytime").Value = Null
    End If
    
    If IsDate(PrintValues.LoadTimeIn) = True Then
        CMD.Parameters("@loadtimein").Value = CDate(PrintValues.LoadTimeIn)
    Else
        CMD.Parameters("@loadtimein").Value = Null
    End If
    
    If IsDate(PrintValues.LoadTimeOut) = True Then
        CMD.Parameters("@loadtimeout").Value = CDate(PrintValues.LoadTimeOut)
    Else
        CMD.Parameters("@loadtimeout").Value = Null
    End If
    
    If IsDate(PrintValues.BOLDate) = True Then
        CMD.Parameters("@timearrived").Value = CDate(PrintValues.BOLDate)
    Else
        CMD.Parameters("@timearrived").Value = Null
    End If

    CMD.Parameters("@remark").Value = CStr(Left(PrintValues.Remarks, 60))
    CMD.Parameters("@penetration").Value = ConvertToDouble(PrintValues.Penetration)
    CMD.Parameters("@penetrationtemperature").Value = ConvertToDouble(PrintValues.PenetrationTemp)
    CMD.Parameters("@penetrationremark").Value = CStr(Left(PrintValues.PenetrationRemark, 16))
    CMD.Parameters("@flash").Value = ConvertToDouble(PrintValues.Flashpoint)
    CMD.Parameters("@lbspergallon").Value = ConvertToDouble(PrintValues.LbsPerGal)
    CMD.Parameters("@specificgravity").Value = ConvertToDouble(PrintValues.SpecGravity)
    CMD.Parameters("@viscosity").Value = ConvertToDouble(PrintValues.Viscosity)
    CMD.Parameters("@viscositytemperature").Value = ConvertToDouble(PrintValues.ViscosityTemp)
    CMD.Parameters("@viscosityremark").Value = CStr(Left(PrintValues.ViscosityRemark, 16))
    CMD.Parameters("@specificationremark").Value = CStr(Left(PrintValues.ProductSpecRemarks, 16))
    CMD.Parameters("@additive").Value = CStr(Left(PrintValues.AdditiveID, 16))
    CMD.Parameters("@additivepercent").Value = ConvertToDouble(PrintValues.AdditivePercent)
    CMD.Parameters("@temperature").Value = ConvertToDouble(PrintValues.Temp)
    CMD.Parameters("@grossgallons").Value = ConvertToDouble(PrintValues.GrossGal)
    CMD.Parameters("@netgallons").Value = ConvertToDouble(PrintValues.NetGal)

    CMD.Parameters("@grossweight1").Value = ConvertToDouble(PrintValues.GrossWtLbs1)
    CMD.Parameters("@grossweight2").Value = ConvertToDouble(PrintValues.GrossWtLbs2)
    CMD.Parameters("@tareweight1").Value = ConvertToDouble(PrintValues.TareWtLbs1)
    CMD.Parameters("@tareweight2").Value = ConvertToDouble(PrintValues.TareWtLbs2)
    
    CMD.Parameters("@item").Value = CStr(Left(PrintValues.ProductID, 16))
    CMD.Parameters("@itemprice").Value = 0
    CMD.Parameters("@additiveprice").Value = 0
    CMD.Parameters("@inbound").Value = CStr(Left(PrintValues.InBoundOperator, 3))
    CMD.Parameters("@outbound").Value = CStr(Left(PrintValues.OutBoundOperator, 3))

    CMD.Parameters("@ladingsibling").Value = CStr(Left(PrintValues.LadingSibling, 23))
    CMD.Parameters("@binid").Value = CStr(Left(PrintValues.TankNumber, 15))

    CMD.Parameters("@customerpo").Value = CStr(Left(PrintValues.PONo, 23))
    CMD.Parameters("@lot").Value = PrintValues.LotNumber
    
    CMD.Parameters("@IsTransfer").Value = PrintValues.IsTransfer
    CMD.Parameters("@IsBetweenCompanies").Value = PrintValues.IsBtwnCmpny
    CMD.Parameters("@FromCompanyID").Value = Left(PrintValues.FromCompany, 3)
    CMD.Parameters("@ToCompanyID").Value = Left(PrintValues.ToCompany, 3)
    CMD.Parameters("@FromWhseID").Value = Left(PrintValues.FromWhse, 15)
    CMD.Parameters("@ToWhseID").Value = Left(PrintValues.ToWhse, 15)

    'RKL DEJ 2016-09-07 START
    CMD.Parameters("@TableKey").Value = Null
    CMD.Parameters("@SecurityTapeNumber").Value = Null
    CMD.Parameters("@BatchNumber").Value = Left(PrintValues.BatchNumber, 100)
    CMD.Parameters("@WeightTktNo").Value = Left(PrintValues.WeightTktNo, 30)
    CMD.Parameters("@Carrier_ID").Value = Left(PrintValues.Carrier_ID, 10)
    CMD.Parameters("@Carrier_Name").Value = Left(PrintValues.Carrier_Name, 30)
    CMD.Parameters("@Purchase_Order").Value = Left(PrintValues.Purchase_Order, 255)
    
    'RKL DEJ 2016-09-07 STOP
    
    gbLastActivityTime = Date + Time
    
    'Execute The procedure
    CMD.Execute

    gbLastActivityTime = Date + Time

AfterExecute:
    'Obtain Output Parameters
    NewKey = ConvertToDouble(CMD.Parameters.Item("@TableKey").Value)
    CreateStatus = ConvertToDouble(CMD.Parameters.Item("@status").Value)
    CreateMessage = ConvertToString(CMD.Parameters.Item("@statustext").Value)
    
    Select Case CreateStatus
        Case 0
            gbMASConn.CommitTrans
            
            InstsoBOLTblLog = True
            
            If SuppressMsg = False Then
                If Trim(CreateMessage) <> Empty Then
'                    MsgBox CreateMessage, vbInformation, "Log Success"
                Else
'                    MsgBox "The BOL Log was created Successfuly.", vbInformation, "Log Success"
                End If
            End If
            
        Case Else
            gbMASConn.RollbackTrans
            
            InstsoBOLTblLog = False
            
            If SuppressMsg = False Then
                If Trim(CreateMessage) <> Empty Then
                    MsgBox CreateMessage, vbExclamation, "Log Failed"
                Else
                    MsgBox "The BOL Log was NOT created Successfuly.", vbExclamation, "Log Failed"
                End If
            End If
            
            ErrMsg = Me.Name & ".InstsoBOLTblLog()" & vbCrLf & "NewKey = " & NewKey & vbCrLf & "CreateStatus = " & CreateStatus & vbCrLf & " CreateMessage = " & CreateMessage
            If RSBOL.State = 1 Then
                If RSBOL.EOF Or RSBOL.BOF Then
                    InsErrorLog 0, Date + Time, ErrMsg
                Else
                    InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                End If
            Else
                InsErrorLog 0, Date + Time, ErrMsg
            End If
            
    End Select
    
    
CleanUP:
    Err.Clear
    On Error Resume Next
    Set CMD = Nothing

    gbLastActivityTime = Date + Time
    Exit Function
Error:
    If SuppressMsg = False Then
        ErrMsg = Me.Name & ".InstsoBOLTblLog(PrintValues As BOLFields, Optional ByRef NewKey As Double = 0) As Boolean" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description
        
'        MsgBox Me.Name & ".InstsoBOLTblLog(PrintValues As BOLFields, Optional ByRef NewKey As Double = 0) As Boolean" & vbCrLf & _
'        "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
        MsgBox ErrMsg, vbCritical, "Error"
    
        If RSBOL.State = 1 Then
            If RSBOL.EOF Or RSBOL.BOF Then
                InsErrorLog 0, Date + Time, ErrMsg
            Else
                InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
            End If
        Else
            InsErrorLog 0, Date + Time, ErrMsg
        End If
    End If
    
    If Err.Number = 2147217873 Then
        If SuppressMsg = False Then
            'This is a duplicate record error.
            MsgBox "This is a duplicate record error.  Attempting to continue on.", vbInformation, "Pressing On"
        End If
        
        ErrMsg = Me.Name & ".InstsoBOLTblLog()" & vbCrLf & "This is a duplicate record error.  Attempting to continue on."
        If RSBOL.State = 1 Then
            If RSBOL.EOF Or RSBOL.BOF Then
                InsErrorLog 0, Date + Time, ErrMsg
            Else
                InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
            End If
        Else
            InsErrorLog 0, Date + Time, ErrMsg
        End If
        
        Err.Clear
        On Error GoTo Error
        
        gbLastActivityTime = Date + Time
        
        GoTo AfterExecute
    End If
    
    
        
    Err.Clear
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    gbMASConn.RollbackTrans

    gbLastActivityTime = Date + Time
    
    GoTo CleanUP

End Function

Function ValidateLocal(Optional ErrMsg As String = Empty) As Boolean
    On Error GoTo Error
    
    Dim CurrentLine As Integer
'    Dim ErrMsg As String
    
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    ValidateLocal = False
       
    CurrentLine = 1
    
    'Make Sure the Product Exists
    If Trim(txtProduct.Text) = Empty Then
'        MsgBox "The product is missing.  Cannot proceed.", vbExclamation, "Warning"
        ErrMsg = "The product is missing.  Cannot proceed."
        Exit Function
    End If
    
    CurrentLine = 2
    
    If txtLotNr.Locked = False Then
        If gbRequireLotNbr = True Then
            If Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) <> 1 Then    'RKL DEJ 9/24/13 - Added - only enforce Lot if not a Return
                'Lot Number is required
                If Trim(txtLotNr.Text) = Empty Then
'                    MsgBox "The Lot Number is required.", vbExclamation, "Warning"
                    ErrMsg = "The Lot Number is required."
                    txtLotNr.SetFocus
                    Exit Function
                End If
            End If
        End If
    End If
    
    CurrentLine = 3
    
    If Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) = 1 Then
        ValidateLocal = True
        Exit Function
    End If
    
    CurrentLine = 4
    
    'Check Viscosity
    If txtViscocity.Locked = False Then
        If Trim(txtViscocity.Text) = Empty Or ConvertToDouble(txtViscocity.Text) <= 0 Then
'            MsgBox "The Viscosity is required.", vbExclamation, "Warning"
            ErrMsg = "The Viscosity is required."
            txtViscocity.SetFocus
            Exit Function
        End If
    End If
            
    
    CurrentLine = 5
    
    'Check Residue
    If txtResidue.Locked = False Then
        If Trim(txtResidue.Text) = Empty Or ConvertToDouble(txtResidue.Text) <= 0 Then
'            MsgBox "The Residue is required.", vbExclamation, "Warning"
            ErrMsg = "The Residue is required."
            txtViscocity.SetFocus
            Exit Function
        End If
    End If
    
    
    CurrentLine = 6
    
    'Gross Gallons
    If Trim(txtGrossGals.Text) = Empty Or ConvertToDouble(txtGrossGals.Text) = 0 Then
'        MsgBox "The Gross Gallons is required.", vbExclamation, "Warning"
        ErrMsg = "The Gross Gallons is required."
        txtGrossGals.SetFocus
        Exit Function
    End If
    
    
    CurrentLine = 7
    
    'Net Gallons
    If Trim(txtNetGals.Text) = Empty Or ConvertToDouble(txtNetGals.Text) = 0 Then
'        MsgBox "The Net Gallons is required.", vbExclamation, "Warning"
        ErrMsg = "The Net Gallons is required."
        txtNetGals.SetFocus
        Exit Function
    End If
    
    
    CurrentLine = 8
    
    If ConvertToDouble(txtFrontBaseGals.Text) = 0 Then
'        MsgBox "The Front Base Gallons is required.", vbExclamation, "Warning"
        ErrMsg = "The Front Base Gallons is required."
        txtFrontBaseGals.SetFocus
        Exit Function
    End If
    
    
    CurrentLine = 9
    
    If ConvertToDouble(txtFrontTemp.Text) = 0 Then
'        MsgBox "The Front Temp is required.", vbExclamation, "Warning"
        ErrMsg = "The Front Temp is required."
        txtFrontTemp.SetFocus
        Exit Function
    End If
    
    
    CurrentLine = 10
    
'    If Abs(SplitLoad) = 1 Then
    If ConvertToDouble(txtRearGrossWt.Text) <> 0 Then
    
        CurrentLine = 11
    
        If ConvertToDouble(txtRearBaseGals.Text) = 0 Then
'            MsgBox "The Rear Base Gallons is required.", vbExclamation, "Warning"
            ErrMsg = "The Rear Base Gallons is required."
            txtRearBaseGals.SetFocus
            Exit Function
        End If
        
    
        CurrentLine = 12
        
        If ConvertToDouble(txtRearTemp.Text) = 0 Then
            MsgBox "The Rear Temp is required.", vbExclamation, "Warning"
            ErrMsg = "The Rear Temp is required."
            txtRearTemp.SetFocus
            Exit Function
        End If
        
    End If

    
    CurrentLine = 13
    
    If sBOLPass = "Front" And txtAdditive.Text <> Empty And Trim(ConvertToString(txtFrontAdditive.Text)) = Empty Then
'        MsgBox "The Front Additive is required.", vbExclamation, "Warning"
        ErrMsg = "The Front Additive is required."
        txtFrontAdditive.SetFocus
        Exit Function
    End If
    
    
    CurrentLine = 14
    
    If sBOLPass = "Rear" And txtAdditive.Text <> Empty And Trim(ConvertToString(txtRearAdditive.Text)) = Empty Then
'        MsgBox "The Rear Additive is required.", vbExclamation, "Warning"
        ErrMsg = "The Rear Additive is required."
        txtRearAdditive.SetFocus
        Exit Function
    End If
    
    
    CurrentLine = 15
    
    If txtFrontAdditiveGals.Locked = False Then
    
        CurrentLine = 16
    
        If sBOLPass = "Front" And txtAdditive.Text <> Empty And ConvertToDouble(txtFrontAdditiveGals.Text) = 0 Then
            
    
            CurrentLine = 17
    
            If UCase(ConvertToString(txtFrontAdditive.Text)) = "NONE" Then
                'No Additive Gallons Required
    
                CurrentLine = 18
    
            ElseIf UCase(ConvertToString(txtFrontAdditive.Text)) = "NO ANTISTRIP IN PRODUCT" Then
                'No Additive Gallons Required
    
                CurrentLine = 19
    
            Else
    
                CurrentLine = 20
    
'                MsgBox "The Front Additive Gallons is required.", vbExclamation, "Warning"
                ErrMsg = "The Front Additive Gallons is required."
                txtFrontAdditiveGals.SetFocus
                Exit Function
            End If
        End If
    End If
    
    
    CurrentLine = 21
    
    If txtRearAdditiveGals.Locked = False Then
    
        CurrentLine = 22
    
        If sBOLPass = "Rear" And txtAdditive.Text <> Empty And ConvertToDouble(txtRearAdditiveGals.Text) = 0 Then
    
            CurrentLine = 23
    
            If UCase(ConvertToString(txtRearAdditive.Text)) = "NONE" Then
                'No Additive Gallons Required
    
                CurrentLine = 24
    
            ElseIf UCase(ConvertToString(txtRearAdditive.Text)) = "NO ANTISTRIP IN PRODUCT" Then
                'No Additive Gallons Required
    
                CurrentLine = 25
    
            Else
    
                CurrentLine = 26
    
'                MsgBox "The Rear Additive Gallons is required.", vbExclamation, "Warning"
                ErrMsg = "The Rear Additive Gallons is required."
                txtRearAdditiveGals.SetFocus
                Exit Function
            End If
        End If
    End If
    
    
    CurrentLine = 26
    
    'Make sure the QTY is Positive
    '6/26/09 Kevin and Suzanne said to validate the return on Total Gallons not Tons
'    If ConvertToDouble(txtNetTons.Text) <= 0 And Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) = 0 Then
    If ConvertToDouble(txtTotalGals.Text) <= 0 And Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) = 0 Then
'        MsgBox "The weight reflects a return value but the return checkbox was not selected.  Cannot proceed.", vbExclamation, "Warning"
        ErrMsg = "The weight reflects a return value but the return checkbox was not selected.  Cannot proceed."
        Exit Function
    End If
'    If ConvertToDouble(txtNetTons.Text) <= 0 Then
'        MsgBox "The amount is negative or does not exist.  This appears to be a return.  This screen does not process returns.", vbExclamation, "Warning"
'        Exit Function
'    End If
    
    
    CurrentLine = 28
    
    Dim LotNumber As String
    Dim PrintAnyWay As Variant
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 2000
    
    If RSBOL("PrintCOA").Value = 1 And Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) <> 1 Then
        'Check to see if the Lot is Active and the COA is ready to print
        LotNumber = Trim(GetLotNumber(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value)))
        
        CurrentLine = 2010
        If LotNumber <> Empty Then
            'Check that the Active Lab Testing Lot Matchs the current Scale Pass Lot
            CurrentLine = 2020
            If UCase(Trim(LotNumber)) <> UCase(Trim(txtLotNr.Text)) Then
                CurrentLine = 2030
                
                PrintAnyWay = MsgBox("The currently Active Lot in MAS 500 for this Tank, Warehouse, and Item is not the same as the Lot listed here on this Scale Pass record." & vbCrLf & _
                "MAS 500 Lot Nbr: '" & LotNumber & "'" & vbCrLf & _
                "Current Scale Pass Lot Nbr: '" & txtLotNr.Text & "'" & vbCrLf & vbCrLf & _
                "If you continue to Print, the COA will not print properly, the Lot on the COA will not match the Lot on the BOL." & vbCrLf & vbCrLf & _
                "Do you want to continue to print anyway?", vbExclamation + vbYesNo, "Lot Does Not Match")
                
                gbLastActivityTime = Date + Time
                
                CurrentLine = 2040
                If PrintAnyWay <> vbYes Then
                    ErrMsg = Empty
                    CurrentLine = 2050
                    'Do not Print
                    Exit Function
                End If
            End If
        End If
    End If
    CurrentLine = 2060
    
    'If we made it this far then the data is valid
    ValidateLocal = True
    
    CurrentLine = 29
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    ErrMsg = Me.Name & ".ValidateLocal() as Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine
    
    MsgBox ErrMsg, vbCritical, "Error"
    
    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, ErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, ErrMsg
    End If

    Err.Clear
    
    ErrMsg = Empty
    gbLastActivityTime = Date + Time
    
End Function


Private Sub chDestDirectEdit_Click()
    If chDestDirectEdit.Value = vbChecked Then
        txtDestination.Enabled = True
        txtDestination.BackColor = &H80000005
    Else
        txtDestination.Enabled = False
        txtDestination.BackColor = &H8000000F
    End If
End Sub

Private Sub cmdPrintBOL_Click()
    On Error GoTo Error
    
    Dim dDate As Date
    Dim lErrMsg As String
    Dim CurrentLine As Integer
    
    tmrAutoReturnNoActivity.Enabled = False
    
    gbLastActivityTime = Date + Time
    
    
    If chHighRisk.Value = vbChecked Then
        If Trim(txtSecTapeNbr.Text) = Empty Then
            MsgBox "'" & Trim(txtContract.Text) & "' - This is a high Risk Contract.  The Security Tape Number is required before printing the BOL.", vbExclamation, "Warning"
            txtSecTapeNbr.SetFocus
            Exit Sub
        End If
    End If
    
    CurrentLine = 0
    
    dDate = Date + Time
    
    CurrentLine = 1
    Me.MousePointer = vbHourglass
    
    CurrentLine = 2
    lblStatus.Visible = True
    CurrentLine = 3
    lblStatus.Caption = "Saving the BOL Data"
    DoEvents
   
    CurrentLine = 4
'    RSBOL.UpdateBatch
    If UpdateRS(RSBOL) = False Then
        CurrentLine = 5
        lErrMsg = dDate & vbCrLf & Me.Name & ".cmdPrintBOL_Click()" & vbCrLf & _
        "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & _
        "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
        
        CurrentLine = 6
        InsErrorLog 0, dDate, lErrMsg
        
        CurrentLine = 7
        MsgBox lErrMsg, vbInformation, "Warning"
        Exit Sub
    End If
    
    CurrentLine = 8
    DoEvents
    
    CurrentLine = 9
    If ProcessBOL(lErrMsg) = False Then
        CurrentLine = 10
        If lErrMsg <> Empty Then
            lErrMsg = "Process BOL Failed" & vbCrLf & lErrMsg
            
            MsgBox lErrMsg, vbInformation, "Warning"
        End If
        
        lErrMsg = dDate & vbCrLf & Me.Name & ".cmdPrintBOL_Click()" & vbCrLf & _
        lErrMsg & vbCrLf & vbCrLf & _
        "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
        
        CurrentLine = 111
        InsErrorLog 0, dDate, lErrMsg
        
        CurrentLine = 12
'        MsgBox lErrMsg, vbInformation, "Warning"
        
        CurrentLine = 13
        'Either the BOL did not print or MAS 500 did not create a Sales Order
        lblStatus.Caption = Empty
        CurrentLine = 14
        lblStatus.Visible = False
        CurrentLine = 15
        Me.MousePointer = vbDefault
        CurrentLine = 16
        
        gbLastActivityTime = Date + Time
        tmrAutoReturnNoActivity.Enabled = True

        Exit Sub
    End If
    
    CurrentLine = 17
    If Abs(rsLoad("SplitLoad").Value) = 1 Then
        CurrentLine = 18
        'This is a split load
        
        If sBOLPass = "Front" Then
            CurrentLine = 19
            sBOLPass = "Rear"
            
            CurrentLine = 20
            lblStatus.Caption = "Getting the trailer data."
            DoEvents
            
            CurrentLine = 21
            If LoadBOL = False Then
                CurrentLine = 22
'            If LoadRearBOL = False Then
                
                lErrMsg = dDate & vbCrLf & Me.Name & ".cmdPrintBOL_Click()" & vbCrLf & _
                "LoadBOL = False... Error creating Rear BOL.  Contact your IT support or SGS Technology Group." & vbCrLf & vbCrLf & _
                "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
                
                CurrentLine = 23
                InsErrorLog 0, dDate, lErrMsg
                        
                CurrentLine = 24
                MsgBox lErrMsg, vbExclamation, "Failure"
                
                lblStatus.Caption = Empty
                CurrentLine = 25
                lblStatus.Visible = False
                CurrentLine = 26
                Me.MousePointer = vbDefault
                CurrentLine = 27
                
                gbLastActivityTime = Date + Time
                tmrAutoReturnNoActivity.Enabled = True
                Exit Sub
            Else
                CurrentLine = 28
                'The Rear trailer has been loaded... Exit so user can update data if nessisary.  The user will then execute the cmdPrintBol_Click for the rear
                lblStatus.Caption = Empty
                CurrentLine = 29
                lblStatus.Visible = False
                CurrentLine = 30
                Me.MousePointer = vbDefault
                CurrentLine = 31
                
                gbLastActivityTime = Date + Time
                tmrAutoReturnNoActivity.Enabled = True
                Exit Sub
            End If
            CurrentLine = 32
        End If
        CurrentLine = 33
        
    End If
            
    CurrentLine = 34
    lblStatus.Caption = "Changing the load from open to closed."
    DoEvents
    
    CurrentLine = 35
    'Update the Load Data
    rsLoad("LoadIsOpen").Value = 2
    
    CurrentLine = 36
    If gbLocalMode = False Then
        CurrentLine = 37
        'The network is up
        rsLoad("NetworkIsDown").Value = 0
    Else
        CurrentLine = 38
        'The Network is Down
        rsLoad("NetworkIsDown").Value = 1
    End If
    
    CurrentLine = 39
'    rsLoad.UpdateBatch
    If UpdateRS(rsLoad) = False Then
        CurrentLine = 40
        lErrMsg = dDate & vbCrLf & Me.Name & ".cmdPrintBOL_Click()" & vbCrLf & _
        "There was an error saving the data.  If this persists contact your IT support." & vbCrLf & vbCrLf & _
        "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
        
        CurrentLine = 41
        InsErrorLog 0, dDate, lErrMsg
        
        CurrentLine = 42
        MsgBox lErrMsg, vbInformation, "Warning"
    End If
    
    CurrentLine = 43
    lblStatus.Caption = Empty
    CurrentLine = 44
    lblStatus.Visible = False
    CurrentLine = 45
    Me.MousePointer = vbDefault
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
    
    CurrentLine = 46
    Unload Me
    
    CurrentLine = 47
    Exit Sub
Error:
    lErrMsg = dDate & vbCrLf & Me.Name & ".cmdPrintBOL_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    InsErrorLog 0, dDate, lErrMsg
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    lblStatus.Caption = Empty
    lblStatus.Visible = False
    Me.MousePointer = vbDefault
    
    Err.Clear

    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
End Sub

Private Function PrintWtTicket()
    On Error GoTo Error
    
    Dim wt As WeightTicket
    Dim SQL As String
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    'Default Return Value is False (Did not print Successfully)
    PrintWtTicket = False
    
    CurrentLine = 1
    If Abs(ConvertToDouble(rsLoad("DriverOn").Value)) = 1 Then
        CurrentLine = 2
        wt.DriverOn = True
    Else
        CurrentLine = 3
        wt.DriverOn = False
    End If
    
    CurrentLine = 4
    wt.GrossFront = ConvertToDouble(rsLoad("FrontGrossWt").Value)
    CurrentLine = 5
    wt.GrossRear = ConvertToDouble(rsLoad("RearGrossWt").Value)
    CurrentLine = 6
    wt.GrossTotal = wt.GrossFront + wt.GrossRear
    
    CurrentLine = 7
    wt.InBoundOpr = ConvertToString(rsLoad("InboundOperator").Value)
    CurrentLine = 8
    wt.OutBoundOpr = ConvertToString(rsLoad("OutboundOperator").Value)
    
    CurrentLine = 9
    If Abs(ConvertToDouble(rsLoad("ManualScaling").Value)) = 1 Then
        CurrentLine = 10
        wt.ManualScaleing = True
    Else
        CurrentLine = 11
        wt.ManualScaleing = False
    End If
    
    CurrentLine = 12
    wt.NetFront = ConvertToDouble(rsLoad("FrontNetWt").Value)
    CurrentLine = 13
    wt.NetRear = ConvertToDouble(rsLoad("RearNetWt").Value)
    CurrentLine = 14
    wt.NetTotal = wt.NetFront + wt.NetRear
    
    CurrentLine = 15
    wt.TareFront = ConvertToDouble(rsLoad("FrontTareWt").Value)
    CurrentLine = 16
    wt.TareRear = ConvertToDouble(rsLoad("RearTareWt").Value)
    CurrentLine = 17
    wt.TareTotal = wt.TareFront + wt.TareRear
    
    CurrentLine = 18
    wt.TicketNo = Empty     'No Ticket Numbers....
    
    CurrentLine = 19
    wt.TruckNo = Trim(rsLoad("TruckNo").Value)
    
    CurrentLine = 20
    If PrintWeightTicket(wt, 2) = True Then
        CurrentLine = 21
        'MsgBox "Ticket has been sent to the printer.", vbInformation, "Ticket Printed"
        PrintWtTicket = True
    Else
        CurrentLine = 22
        MsgBox "There was an error printing the Ticket.", vbExclamation, "Not Printed"
    End If
    
    CurrentLine = 23
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".PrintWTTicket" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function


Private Function PrintBL(PrintValues As BOLFields, Optional ByRef PrintPreview As String = Empty, Optional DebugOnly As Boolean = False) As Boolean
    On Error GoTo Error
    
    Dim ErrMsg As String
    Dim i As Integer
    Dim PrintLine As String         'Text Being printed.  One line at a time.
    Dim CurrentLine As Integer      'This is the line on the page that is being printed
    Dim Prntr As Printer
    Dim PrtHandle As Long
    
    gbLastActivityTime = Date + Time
    
    'Default Return Value is False (Did not print Successfully)
    PrintBL = False
    
    '****NOTE: Each Line is only 80 Charaters long.  Must print each line in the correct order
    '**** Also it appears as though the Page is 78 lines long and you have to print each line
    
    CurrentLine = 1                 'Start at line one
    
    For Each Prntr In Printers
'        Debug.Print Prntr.DeviceName
'        If UCase(Prntr.DeviceName) = UCase(gbSCALEPASSPRINTER) Then
        If UCase(Prntr.DeviceName) = UCase(gbBLPRINTER) Then
'    ' Set instance of Printer
'    Set Printer = Printers(PrtHandle)
    
            PrtHandle = Prntr.hDC
            Set Printer = Prntr
            GoTo GotPrinter
        End If
    Next
    
    'This code will only be reached if the printer is not found
    ErrMsg = "Lading Printer Not properly installed."
    
    MsgBox ErrMsg, vbOKOnly, "Printing Error"
    
    PrintBL = False
    
    gbLastActivityTime = Date + Time
    Exit Function
    
GotPrinter:
    'Clear the varable
    PrintPreview = Empty
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 1 - 5: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 5
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 6: Print BOL Number
    'The BOL starts in position 70 so pad with 69 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 6
    PrintLine = Left(Space(69) & PrintValues.BOLNumber, 80)
    Printer.Print PrintLine
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 7 - 9: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 9
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 10: If this is a credit return then print Credit Return
    'The BOL starts in position 70 so pad with 36 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    If PrintValues.IsCreditReturn = True Then
        'This is a credit Return
        PrintLine = Left(Space(35) & UCase("* * * * * Credit Return * * * * *"), 80)
    Else
        'This is not a credit Return
        PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    End If
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 10
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 11: Print Front or Rear Load
    'Start in Postion 58 so pad with 57 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    If PrintValues.IsSplitLoad = True Then
        'Is a Split Load
        PrintLine = Left(Space(57) & UCase("Split Load (" & PrintValues.SplitLoadType & ")"), 80)   'sBOLPass will be one of: Front, Rear
    Else
        'Is Not a Split Load
        PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    End If
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 11
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 12 -13: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 13
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 14: Print Customer Name and Date
    'Start in Postion 8 so pad with 7 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(7)                                                'Start In position 8
'    PrintLine = PrintLine & Left(Trim(UCase(.txtCustomerName)), 55)     'The Customer Name can only be 55 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.CustomerName)), 55)     'The Customer Name can only be 55 Characters Long
    PrintLine = PrintLine & Space(67 - Len(PrintLine))                  'Pad to position = 68
    PrintLine = PrintLine & Format(PrintValues.BOLDate, "mm/dd/yy")                  'The date
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 14
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 15: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 15
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 16: Print Contract, P.O. Number, Origin, PA GROUP No.
    'Start in Postion 12 so pad with 11 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(11)                                               'Pad to position = 12
'    PrintLine = PrintLine & Left(Trim(UCase(.txtContract)), 10)         'The Contract can only be 10 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.ContractNo)), 10)         'The Contract can only be 10 Characters Long
    PrintLine = PrintLine & Space(28 - Len(PrintLine))                  'Pad to position = 29
'    PrintLine = PrintLine & Left(Trim(UCase(.txtCustomerPO)), 12)       'The PO Number can only be 12 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.PONo)), 12)       'The PO Number can only be 12 Characters Long
    PrintLine = PrintLine & Space(45 - Len(PrintLine))                  'Pad to position = 46
'    PrintLine = PrintLine & Left(Trim(UCase(.txtOrigin)), 16)           'The Origin can only be 16 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Origin)), 16)           'The Origin can only be 16 Characters Long
    PrintLine = PrintLine & Space(71 - Len(PrintLine))                  'Pad to position = 72
    PrintLine = PrintLine & Space(1)                                    'The PA Group
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 16
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 17: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 17
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 18: Print Destination
    'Start in Postion 9 so pad with 8 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
'    PrintLine = Space(8) & Left(UCase(PrintValues.Destination), 72)        'The Destination can only be 72 Characters Long
    PrintLine = Space(8) & Left(UCase(PrintValues.DestDirections), 72)        'The Destination can only be 72 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 18
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 19: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 19
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 20: Print Project No.
    'Start in Postion 9 so pad with 8 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
'    PrintLine = Space(8) & Left(UCase(.txtProjectNumber), 72)        'The Project No. can only be 72 Characters Long
    PrintLine = Space(8) & Left(UCase(PrintValues.ProjectNo), 72)        'The Project No. can only be 72 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 20
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf

    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 21: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 21
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 22: Print Project Description
    'Start in Postion 14 so pad with 13 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
'    PrintLine = Space(13) & Left(UCase(.txtDescription), 68)        'The Project Description can only be 68 Characters Long
    PrintLine = Space(13) & Left(UCase(PrintValues.ProjectDescription), 68)        'The Project Description can only be 68 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 22
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf

    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 23: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 23
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 24: Print Carrier and (Refinery Manifest or Wt. Tikt No.)
    'Start in Postion 6 so pad with 5 spaces
    'NOTE: The code from old Scale as the Refinery Manifest or Wt. Tikt No. at 9 and that pushesthe line length to 81 char
    '************************************************************************************************************************
    '************************************************************************************************************************
'    PrintLine = Space(5) & Left(Trim(UCase(.txtCarrierID)), 25)           'The Carrier can only be 25 Characters Long
    PrintLine = Space(5) & Left(Trim(UCase(PrintValues.Carrier)), 25)           'The Carrier can only be 25 Characters Long
    PrintLine = PrintLine & Space(72 - Len(PrintLine))                  'Pad to position = 73
'    PrintLine = PrintLine & Left(Trim(UCase(.txtWeightTicket)), 9)      'The Refinery Manifest or Wt. Tikt No. can only be 9 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.WeightTicketNo)), 9)      'The Refinery Manifest or Wt. Tikt No. can only be 9 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 24
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf

    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 25: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 25
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 26: Print Truc No, Trailer No, Load Time, Delivery Time
    'Start in Postion 7 so pad with 6 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(6)                                                'Pad to position = 7
'    PrintLine = PrintLine & Left(Trim(UCase(.txtTruckNumber)), 9)       'The Truck No can only be 9 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.TruckNo)), 9)       'The Truck No can only be 9 Characters Long
    PrintLine = PrintLine & Space(23 - Len(PrintLine))                  'Pad to position = 24
'    PrintLine = PrintLine & Left(Trim(UCase(.txtTrailerNumber)), 7)     'The Trailer No can only be 7 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Trailer1No)), 7)     'The Trailer No can only be 7 Characters Long
    PrintLine = PrintLine & Space(38 - Len(PrintLine))                  'Pad to position = 39
'    PrintLine = PrintLine & Left(Trim(UCase(.txtTrailerRear)), 8)       'The Trailer No can only be 8 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Trailer2No)), 8)       'The Trailer No can only be 8 Characters Long
    PrintLine = PrintLine & Space(54 - Len(PrintLine))                  'Pad to position = 55
'    PrintLine = PrintLine & UCase(.pvLoadTime.Time)                     'The Load Time
    PrintLine = PrintLine & UCase(Format(PrintValues.LoadTime, "hh:mmAMPM"))                    'The Load Time
    PrintLine = PrintLine & Space(71 - Len(PrintLine))                  'Pad to position = 72
'    PrintLine = PrintLine & Format(.pvDeliverTime.Time, "hh:mmAMPM")    'The Delivery Time
    PrintLine = PrintLine & Format(PrintValues.DeliveryTime, "hh:mmAMPM")    'The Delivery Time
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 26
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf

    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 27: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 27
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 28: Print Load Time In, Load Time Out
    'Start in Postion 9 so pad with 8 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(8)                                                'Pad to position = 9
'    PrintLine = PrintLine & UCase(.pvLoadTimeIn.Time)                   'The Load Time In
    PrintLine = PrintLine & UCase(Format(PrintValues.LoadTimeIn, "hh:mmAMPM"))                   'The Load Time In
    PrintLine = PrintLine & Space(25 - Len(PrintLine))                  'Pad to position = 26
'    PrintLine = PrintLine & UCase(.pvLoadTimeOut.Time)                  'The Load Time Out
    PrintLine = PrintLine & UCase(Format(PrintValues.LoadTimeOut, "hh:mmAMPM"))                   'The Load Time Out
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 28
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 29: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 29
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 30: Print Remarks
    'Start in Postion 7 so pad with 6 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(6)                                                'Pad to position = 7
'    PrintLine = PrintLine & Left(Trim(UCase(.txtRemarks)), 74)          'The Truck No can only be 74 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Remarks)), 74)          'The Truck No can only be 74 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 30
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 31 - 33: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 33
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 34: Print Penetration, Temperature, Remark, Flash Point, Lbs/Gal
    'Start in Postion 24 so pad with 23 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(23)                                                       'Pad to position = 24
'    PrintLine = PrintLine & Left(Trim(UCase(.txtPenetration)), 8)               'The Penetration can only be 8 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Penetration)), 8)               'The Penetration can only be 8 Characters Long
    PrintLine = PrintLine & Space(34 - Len(PrintLine))                          'Pad to position = 35
'    PrintLine = PrintLine & Left(Trim(UCase(.txtPenetrationTemperature)), 5)    'The Temperature can only be 5 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.PenetrationTemp)), 5)    'The Temperature can only be 5 Characters Long
    PrintLine = PrintLine & Space(41 - Len(PrintLine))                          'Pad to position = 42
'    PrintLine = PrintLine & Left(Trim(UCase(.txtPenetrationRemark)), 13)        'The Remark can only be 13 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.PenetrationRemark)), 13)        'The Remark can only be 13 Characters Long
    PrintLine = PrintLine & Space(63 - Len(PrintLine))                          'Pad to position = 64
'    PrintLine = PrintLine & Left(Trim(UCase(.txtFlashpoint)), 6)                'The Flash Point can only be 6 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Flashpoint)), 6)                'The Flash Point can only be 6 Characters Long
    PrintLine = PrintLine & Space(75 - Len(PrintLine))                          'Pad to position = 76
'    PrintLine = PrintLine & Left(Trim(UCase(.txtLbsPerGallon)), 5)              'The Lbs/Gal can only be 5 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.LbsPerGal)), 5)              'The Lbs/Gal can only be 5 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 34
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    gbLastActivityTime = Date + Time
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 35: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 35
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 36: Print Spec. Gravity, Viscosity, Temperature, Viscosity Remarks, Specification Remarks
    'Start in Postion 15 so pad with 14 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(14)                                                       'Pad to position = 15
'    PrintLine = PrintLine & Left(Trim(UCase(.txtSpecificGravity)), 6)           'The Spec. Gravity can only be 6 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.SpecGravity)), 6)           'The Spec. Gravity can only be 6 Characters Long
    PrintLine = PrintLine & Space(27 - Len(PrintLine))                          'Pad to position = 28
'    PrintLine = PrintLine & Left(Trim(UCase(.txtViscosity)), 6)                 'The Viscosity can only be 6 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Viscosity)), 6)                 'The Viscosity can only be 6 Characters Long
    PrintLine = PrintLine & Space(36 - Len(PrintLine))                          'Pad to position = 37
'    PrintLine = PrintLine & Left(Trim(UCase(.txtViscosityTemperature)), 4)      'The Temperature can only be 4 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.ViscosityTemp)), 4)      'The Temperature can only be 4 Characters Long
    PrintLine = PrintLine & Space(42 - Len(PrintLine))                          'Pad to position = 43
'    PrintLine = PrintLine & Left(Trim(UCase(.txtViscosityRemark)), 10)          'The Viscosity Remarks can only be 10 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.ViscosityRemark)), 10)          'The Viscosity Remarks can only be 10 Characters Long
    PrintLine = PrintLine & Space(60 - Len(PrintLine))                          'Pad to position = 61
'    PrintLine = PrintLine & Left(Trim(UCase(.txtSpecificationRemark)), 20)      'The Specification Remarks can only be 20 Characters Long
'    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.ProductSpecRemarks)), 20)      'The Specification Remarks can only be 20 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.LotNumber)), 20)      'The Specification Remarks can only be 20 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 36
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 37: Print Residue
    'Start in Postion 50 so pad with 49 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(62)                                                       'Pad to position = 63
    PrintLine = PrintLine & "Residue: " & Format(Left(Trim(UCase(PrintValues.Residue)), 30), "##0.00#") & "%"          'Residue %
    PrintLine = PrintLine & Space(80 - Len(PrintLine))                          'Pad to position = 28
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 37
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 38 - 41: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 41
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 42: Print Certificate of Compliance
    'Start in Postion 18 so pad with 17 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(17)                                                       'Pad to position = 18
'    PrintLine = PrintLine & Left(Trim(UCase(.txtCompliance)), 62)               'The Certificate of Compliance can only be 62 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.CertOfCompliance)), 62)               'The Certificate of Compliance can only be 62 Characters Long
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 42
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 43 - 46: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 46
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 47: Print Weight Hand Keyed
    'Start in Postion 56 or 31 so pad with 55 or 30 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
'    If gbPlantPrefix < 6 Then 'Woods Cross & Rawlins (Plants 6&7) opting out of scale verbiage
    If gbPlantPrefix = 0 Then 'ALL plants are opting out
    '    If ScaleInOut.chkHandKey = vbChecked Then
        If PrintValues.IsHandKeyed = True Then
            
    '        If IASScaleEnabled = vbTrue Then
            If PrintValues.IsScaleEnabled = True Then
                PrintLine = Space(55)                               'Pad to position = 56
                PrintLine = PrintLine & "<-----KEYED----->"
            Else
                PrintLine = Space(30)                               'Pad to position = 31
                PrintLine = PrintLine & "Weighed on a certified scale, verify ticket"
            End If
        
        Else
            PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
        End If
    Else
            PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    End If
    
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 47
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 48 - 49: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 49
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 50: Print Weight Line 1: Additive %, Temp, Gross Gal., Net Gal. Gross Wt, Tar Wt, Net Wt
    'Start in Postion 2 so pad with 1 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(1)                                                       'Pad to position = 2
'    PrintLine = PrintLine & Format(.txtAdditivePercent, "#0.00")                'The Additive %
    PrintLine = PrintLine & Format(PrintValues.AdditivePercent, "#0.00")                'The Additive %
    PrintLine = PrintLine & Space(22 - Len(PrintLine))                          'Pad to position = 23
'    PrintLine = PrintLine & Left(Trim(UCase(.txtTemp)), 4)                      'The Temp can only be 4 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.Temp)), 4)                      'The Temp can only be 4 Characters Long
    PrintLine = PrintLine & Space(28 - Len(PrintLine))                          'Pad to position = 29
'    PrintLine = PrintLine & Format(.txtGrossGal, "###,##0.00")                  'The Gross Gal.
    PrintLine = PrintLine & Format(PrintValues.GrossGal, "###,##0.00")                  'The Gross Gal.
    PrintLine = PrintLine & Space(38 - Len(PrintLine))                          'Pad to position = 39
'    PrintLine = PrintLine & Format(.txtNetGal, "###,##0.00")                    'The Net Gal.
    PrintLine = PrintLine & Format(PrintValues.NetGal, "###,##0.00")                    'The Net Gal.
'    PrintLine = PrintLine & Space((58 - Len(Trim(UCase(.txtGrossFront)))) - Len(PrintLine)) 'Right Align end on position 58
    PrintLine = PrintLine & Space((58 - Len(Trim(UCase(PrintValues.GrossWtLbs1)))) - Len(PrintLine)) 'Right Align end on position 58
'    PrintLine = PrintLine & Trim(UCase(.txtGrossFront))                         'The Gross Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.GrossWtLbs1))                         'The Gross Wt
'    PrintLine = PrintLine & Space((68 - Len(Trim(UCase(.txtTareFront)))) - Len(PrintLine)) 'Right Align end on position 68
    PrintLine = PrintLine & Space((68 - Len(Trim(UCase(PrintValues.TareWtLbs1)))) - Len(PrintLine)) 'Right Align end on position 68
'    PrintLine = PrintLine & Trim(UCase(.txtTareFront))                          'The Tare Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.TareWtLbs1))                          'The Tare Wt
'    PrintLine = PrintLine & Space((78 - Len(Trim(UCase(.txtNetFront)))) - Len(PrintLine)) 'Right Align end on position 78
    PrintLine = PrintLine & Space((78 - Len(Trim(UCase(PrintValues.NetWtLbs1)))) - Len(PrintLine)) 'Right Align end on position 78
'    PrintLine = PrintLine & Trim(UCase(.txtNetFront))                           'The Net Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.NetWtLbs1))                           'The Net Wt
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 50
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 51: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 51
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    gbLastActivityTime = Date + Time
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 52: Print Weight Line 2: Additive, Additive Description, Gross Wt, Tare Wt, Net Wt
    'Start in Postion 2 so pad with 1 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(1)                                                        'Pad to position = 2
'    PrintLine = PrintLine & Left(Trim(UCase(.txtAdditiveDescription)), 20)      'The Additive Description can only be 20 Characters Long
    PrintLine = PrintLine & Left(Trim(UCase(PrintValues.AdditiveDesc)), 20)      'The Additive Description can only be 20 Characters Long
    PrintLine = PrintLine & Space(28 - Len(PrintLine))                          'Pad to position = 29
'    PrintLine = PrintLine & UCase(RTrim(.txtItemSpec)) & "/" & UCase(RTrim(.txtProductionName)) 'The Product
'    PrintLine = PrintLine & UCase(RTrim(PrintValues.ItemSpec)) & "/" & UCase(RTrim(PrintValues.ProductName)) 'The Product
'    PrintLine = PrintLine & Space((58 - Len(Trim(UCase(.txtGrossRear)))) - Len(PrintLine)) 'Right Align end on position 58
    PrintLine = PrintLine & Space((58 - Len(Trim(UCase(PrintValues.GrossWtLbs2)))) - Len(PrintLine)) 'Right Align end on position 58
'    PrintLine = PrintLine & Trim(UCase(.txtGrossRear))                         'The Gross Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.GrossWtLbs2))                         'The Gross Wt
'    PrintLine = PrintLine & Space((68 - Len(Trim(UCase(.txtTareRear)))) - Len(PrintLine)) 'Right Align end on position 68
    PrintLine = PrintLine & Space((68 - Len(Trim(UCase(PrintValues.TareWtLbs2)))) - Len(PrintLine)) 'Right Align end on position 68
'    PrintLine = PrintLine & Trim(UCase(.txtTareRear))                          'The Tare Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.TareWtLbs2))                          'The Tare Wt
'    PrintLine = PrintLine & Space((78 - Len(Trim(UCase(.txtNetRear)))) - Len(PrintLine)) 'Right Align end on position 78
    PrintLine = PrintLine & Space((78 - Len(Trim(UCase(PrintValues.NetWtLbs2)))) - Len(PrintLine)) 'Right Align end on position 78
'    PrintLine = PrintLine & Trim(UCase(.txtNetRear))                           'The Net Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.NetWtLbs2))                           'The Net Wt
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 52
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
        
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 53: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 53
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 54: Print Weight Line 3: Total Gross Wt, Total Tare Wt, Total Net Wt
    'Start in Postion 58
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Empty
'    PrintLine = PrintLine & Space((58 - Len(Trim(UCase(.txtGrossTotal)))) - Len(PrintLine)) 'Right Align end on position 58
    PrintLine = PrintLine & Space((58 - Len(Trim(UCase(PrintValues.GrossWtLbs3)))) - Len(PrintLine)) 'Right Align end on position 58
'    PrintLine = PrintLine & Trim(UCase(.txtGrossTotal))                         'The Total Gross Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.GrossWtLbs3))                         'The Total Gross Wt
'    PrintLine = PrintLine & Space((68 - Len(Trim(UCase(.txtTareTotal)))) - Len(PrintLine)) 'Right Align end on position 68
    PrintLine = PrintLine & Space((68 - Len(Trim(UCase(PrintValues.TareWtLbs3)))) - Len(PrintLine)) 'Right Align end on position 68
'    PrintLine = PrintLine & Trim(UCase(.txtTareTotal))                          'The Total Tare Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.TareWtLbs3))                          'The Total Tare Wt
'    PrintLine = PrintLine & Space((78 - Len(Trim(UCase(.txtNetTotal)))) - Len(PrintLine)) 'Right Align end on position 78
    PrintLine = PrintLine & Space((78 - Len(Trim(UCase(PrintValues.NetWtLbs3)))) - Len(PrintLine)) 'Right Align end on position 78
'    PrintLine = PrintLine & Trim(UCase(.txtNetTotal))                           'The Total Net Wt
    PrintLine = PrintLine & Trim(UCase(PrintValues.NetWtLbs3))                           'The Total Net Wt
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 54
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    '************************************************************************************************************************
    '************************************************************************************************************************
    '           Print Lines 55 - 57 or 55 - 59 or 55 - 61 or 55 - 63 Depending upon the Placard Type
    '************************************************************************************************************************
    '************************************************************************************************************************
    '************************************************************************************************************************
    '************************************************************************************************************************
'    Select Case .txtPlacardType
    Select Case PrintValues.PlacardType
'        Case 1      'Elevated Temperature
        Case PlacardTypes.ElevatedTemperature
            '************************************************************************************************************************
            '************************************************************************************************************************
            'Print Line 55 - 57: Print Blank Lines
            '************************************************************************************************************************
            '************************************************************************************************************************
            PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
            For CurrentLine = CurrentLine To 57
                Printer.Print PrintLine
                PrintPreview = PrintPreview & PrintLine & vbCrLf
            Next
'        Case 2      'Hot Tars
        Case PlacardTypes.HotTars
            '************************************************************************************************************************
            '************************************************************************************************************************
            'Print Line 55 - 59: Print Blank Lines
            '************************************************************************************************************************
            '************************************************************************************************************************
            PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
            For CurrentLine = CurrentLine To 59
                Printer.Print PrintLine
                PrintPreview = PrintPreview & PrintLine & vbCrLf
            Next
'        Case 0, 3   'Not Regulated
        Case PlacardTypes.NotRegulated, PlacardTypes.NotRegulated0
            '************************************************************************************************************************
            '************************************************************************************************************************
            'Print Line 55 - 61: Print Blank Lines
            '************************************************************************************************************************
            '************************************************************************************************************************
            PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
            For CurrentLine = CurrentLine To 61
                Printer.Print PrintLine
                PrintPreview = PrintPreview & PrintLine & vbCrLf
            Next
'        Case 4      'Other
        Case PlacardTypes.other
            '************************************************************************************************************************
            '************************************************************************************************************************
            'Print Line 55 - 63: Print Blank Lines
            '************************************************************************************************************************
            '************************************************************************************************************************
            PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
            For CurrentLine = CurrentLine To 63
                Printer.Print PrintLine
                PrintPreview = PrintPreview & PrintLine & vbCrLf
            Next
        Case Else   'Other
            '************************************************************************************************************************
            '************************************************************************************************************************
            'Print Line 55 - 63: Print Blank Lines
            '************************************************************************************************************************
            '************************************************************************************************************************
            PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
            For CurrentLine = CurrentLine To 63
                Printer.Print PrintLine
                PrintPreview = PrintPreview & PrintLine & vbCrLf
            Next
    End Select
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 58 or 60 or 62 or 64 Depending upon the Placard Type
    'Quantity, HM, Product, Net Tons
    'Start in Postion 3 to pad 2 spaces
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(2)
    PrintLine = PrintLine & "1 TK" 'The Quantity
    
'    If .txtPlacardType <> "0" And .txtPlacardType <> "3" Then
    If PrintValues.PlacardType <> NotRegulated And PrintValues.PlacardType <> NotRegulated0 Then
        PrintLine = PrintLine & Space(8 - Len(PrintLine))   'Pad to position = 28
        PrintLine = PrintLine & "XX"                        'The HM
    End If
    
'    PrintLine = PrintLine & Space((70 - Len(Left(Trim(.txtItemDescription), 16))) - Len(PrintLine))     'Right Align end on position 70
'    PrintLine = PrintLine & 70 - Len(Left(Trim(.txtItemDescription), 16))                               'The Product
    PrintLine = PrintLine & Space((70 - Len(Left(Trim(PrintValues.ProductDesc), 16))) - Len(PrintLine))     'Right Align end on position 70
    PrintLine = PrintLine & Left(Trim(PrintValues.ProductDesc), 16)                               'The Product
    
    PrintLine = PrintLine & Space(72 - Len(PrintLine))      'Pad to position = 73
'    PrintLine = PrintLine & Format(.txtNetTons, "#0.00")    'The Net Tons
    PrintLine = PrintLine & Format(PrintValues.NetTons, "#0.00")    'The Net Tons
    
    CurrentLine = CurrentLine + 1   'Update line Counter: Line 58 or 60 or 62 or 64 Depending upon the Placard Type
    Printer.Print PrintLine         'Print the Line
    PrintPreview = PrintPreview & PrintLine & vbCrLf
    
    '************************************************************************************************************************
    '************************************************************************************************************************
    'Print Line 59 - 78 or 61 - 78 or 63 - 78 or 65 - 78: Print Blank Lines
    '************************************************************************************************************************
    '************************************************************************************************************************
    PrintLine = Space(10)   'Old Scale Pass Code padded blank lines with 10 spaces Don't know why
    For CurrentLine = CurrentLine To 78
        Printer.Print PrintLine
        PrintPreview = PrintPreview & PrintLine & vbCrLf
    Next
    
    If DebugOnly = True Then
        'Debug
        Printer.KillDoc
    Else
        Printer.EndDoc
    End If
    
    PrintBL = True
    
'    'Ask the user if the BOL printed Successfuly.
'    If MsgBox("Did the B/L print ok?", vbYesNo, "Print Success?") = vbYes Then
'        'The BOL was printed Successfully
'        PrintBL = True
'    End If
    
    gbLastActivityTime = Date + Time
    Exit Function

Error:
    If Err.Number = 9 Then
        ErrMsg = "Lading Printer Not properly installed."
        
        MsgBox ErrMsg, vbOKOnly, "Printing Error"
        
        PrintBL = False
    Else
        ErrMsg = Me.Name & ".PrintBL()" & vbCrLf & vbCrLf
        ErrMsg = ErrMsg & "Printing Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf
        ErrMsg = ErrMsg & "Call IS Support"
        
        Err.Clear
        
        MsgBox ErrMsg, vbCritical, "Printing Error"
        
        PrintBL = False
    End If
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Function



'This was used in the old system but will not be used in the new system
Public Function GetNextWeightTicketNumber() As Long

    On Error GoTo Error:

    Dim RS As New ADODB.Recordset
    Dim WeightTicket As Long
    Dim SQL As String
    
    gbLastActivityTime = Date + Time
    
    SQL = "Select * From WeightTicketNum"
    
    GetNextWeightTicketNumber = 0
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenDynamic
    
    RS.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    If RS.RecordCount <= 0 Then
        RS.AddNew
        RS("WTID").Value = 1
'        RS.UpdateBatch
        If UpdateRS(RS) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    
    RS.MoveFirst
    
    WeightTicket = ConvertToDouble(RS("WTID").Value)
    
    GetNextWeightTicketNumber = WeightTicket
    
    WeightTicket = WeightTicket + 1

    RS("WTID").Value = WeightTicket

'    RS.UpdateBatch
    If UpdateRS(RS) = False Then
        MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Function

Error:
    MsgBox Me.Name & ".sldk" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear
    
    GetNextWeightTicketNumber = -1

    gbLastActivityTime = Date + Time
End Function


Private Sub cmdPrintBOL_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdTestPrint_Click()
    On Error Resume Next
    
    DebugOnly = True
    
    Call cmdPrintBOL_Click
    
    DebugOnly = False
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdTestPrint_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Load()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Me.Left = 500
    Me.Top = 500
    
    Set rsLoad = New ADODB.Recordset
    Set rsLoadExt = New ADODB.Recordset
    
    Set RSBOL = New ADODB.Recordset
    Set RSBOLExt = New ADODB.Recordset
        
    'Validate MAS Connection
    If gbLocalMode = False Then
        If HaveLostMASConnection = True Then
            'No longer connected to MAS 500
        End If
    End If
    
    If lcUpdateOnly = False Then
        'Update Only is called during startup to update MAS 500 tables with records created during a downed network.
        'Call SetupDropDowns
        
        'Load the front
        sBOLPass = "Front"
        If LoadBOL = False Then
            MsgBox "Could not obtain open load or create the " & sBOLPass & " BOL.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
        End If
    '    If LoadFrontBOL = False Then
    '        MsgBox "Could not obtain open load or create the Front BOL.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
    '    End If
    End If
    
    Call ShowHideRearFields
    
    tmrAutoReturnNoActivity.Enabled = gbAutoRtnOpenLoads
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub




Function LoadBOL() As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim RSGetBOLData As New ADODB.Recordset
    Dim RSGetFormulaData As New ADODB.Recordset
'    Dim WeightTicketNo As Long
    
    Dim FrontBaseGallons As Variant
    Dim RearBaseGallons As Variant
    Dim FrontTemp As Variant
    Dim RearTemp As Variant
    Dim FrontAdditive As Variant
    Dim RearAdditive As Variant
    Dim FrontAdditiveGallons As Variant
    Dim RearAdditiveGallons As Variant
    Dim TotalAdditiveGallons As Variant
    Dim TotalBaseGallons As Variant
'    Dim LbsPerGallon As Variant
    Dim TotalGallons As Variant
    Dim GrossGallons As Variant
'    Dim NetGallons As Variant
    Dim HasPrintedSuccessfully As Long
    Dim SplitLoad As Long
    Dim FrontBOL As String
    Dim bUpdateMode As Boolean
    
    Dim Trailer1_ID As String
    Dim Trailer2_ID As String
    Dim Carrier As String
    
    Dim LotNumber As String
    Dim COATestRsltsKey As Long 'RKL DEJ 2017-12-21
    
    gbLastActivityTime = Date + Time
    
    'Set the caption on the form to Front or Rear Load
    If gbLocalMode Then
        Me.Caption = "Bill of Lading (" & sBOLPass & " Load) (Server Disconnected) - Valero"
    Else
        Me.Caption = "Bill of Lading (" & sBOLPass & " Load) - Valero"
    End If

    
    gbLastActivityTime = Date + Time
    
    
    'Copy values from Front Load
    If RSBOL.State = 1 Then
        If Not RSBOL.EOF And Not RSBOL.BOF Then
            FrontBaseGallons = RSBOL("FrontBaseGallons").Value
            RearBaseGallons = RSBOL("RearBaseGallons").Value
            FrontTemp = RSBOL("FrontTemp").Value
            RearTemp = RSBOL("RearTemp").Value
            FrontAdditive = RSBOL("FrontAdditive").Value
            RearAdditive = RSBOL("RearAdditive").Value
            FrontAdditiveGallons = RSBOL("FrontAdditiveGallons").Value
            RearAdditiveGallons = RSBOL("RearAdditiveGallons").Value
            TotalAdditiveGallons = RSBOL("TotalAdditiveGallons").Value
            TotalBaseGallons = RSBOL("TotalBaseGallons").Value
'            LbsPerGallon = RSBOL("LbsPerGallon").Value
            TotalGallons = RSBOL("TotalGallons").Value
            GrossGallons = RSBOL("GrossGallons").Value
'            NetGallons = RSBOL("NetGallons").Value
            FrontBOL = ConvertToString(RSBOL("BOLNbr").Value)
            
            Trailer1_ID = ConvertToString(RSBOL("Trailer1_ID").Value)
            Trailer2_ID = ConvertToString(RSBOL("Trailer2_ID").Value)
            Carrier = ConvertToString(RSBOL("Carrier").Value)
            
        End If
        
        RSBOL.Close
    End If
    
    'RKL DEJ 2018-01-09 Added close rs logic
    If RSBOLExt.State = 1 Then
        RSBOLExt.Close
    End If
    
    'Open the Parent Load Record
    If rsLoad.State <> 1 Then
        SQL = "Select * From Loads Where LoadsTableKey = " & gbLoadsTableKey
        
        rsLoad.CursorLocation = adUseClient
        rsLoad.CursorType = adOpenDynamic
        
        rsLoad.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    End If
    
    If rsLoadExt.State <> 1 Then
        SQL = "Select * From LoadsExt Where LoadsTableKey = " & gbLoadsTableKey
        
        rsLoadExt.CursorLocation = adUseClient
        rsLoadExt.CursorType = adOpenDynamic
        
        rsLoadExt.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    End If
    
    'If the Parent Load is Open then load the BOL Else Return False
    If rsLoad.State = 1 Then
        If rsLoad.RecordCount <= 0 Then
            'The Parent Load Record was not obtained.
            MsgBox "Could not retrieve the Load data.", vbExclamation, "Data Not Available"
            gbLastActivityTime = Date + Time
            Exit Function
        End If
        
        If rsLoadExt.State = 1 Then
            If rsLoadExt.RecordCount <= 0 Then
                'The Parent Load Record was not obtained.
                MsgBox "Could not retrieve the Load Ext data.", vbExclamation, "Data Not Available"
                gbLastActivityTime = Date + Time
                Exit Function
            End If
        Else
            MsgBox "Could not retrieve the Load Ext data.", vbExclamation, "Data Not Available"
            gbLastActivityTime = Date + Time
            Exit Function
        End If
        
        SplitLoad = ConvertToDouble(rsLoad("SplitLoad").Value)
        
        SQL = "Select * From BOLPrinting Where LoadsTableKey = " & gbLoadsTableKey & " AND FrontRearFlag = '" & sBOLPass & "'"  'LoadIsOpen = 1"
   
        RSBOL.CursorLocation = adUseClient
        RSBOL.CursorType = adOpenDynamic
                
        RSBOL.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
        
        If RSBOL.State = 1 Then
            If RSBOL.EOF Or RSBOL.BOF Then
                '*****************************************************************************
                '*****************************************************************************
                ' Create New Record
                '*****************************************************************************
                '*****************************************************************************
                RSBOL.AddNew
                bUpdateMode = False
                
                'RKL DEJ 2018-01-09 Added a save for new records to get the BOLTableKey
                If UpdateRS(RSBOL) = False Then
                    MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
                End If
            Else
                bUpdateMode = True
                'Record Already Exists
                
                'If this is the front Load check to see if the BOL has already printed.  If so then load the Rear.
                If sBOLPass = "Front" Then
                    HasPrintedSuccessfully = ConvertToDouble(RSBOL("HasPrintedSuccessfully").Value)
                    If HasPrintedSuccessfully = 1 And Abs(SplitLoad) = 1 Then
                        sBOLPass = "Rear"
                        LoadBOL = LoadBOL()
                        Exit Function
                    End If
                End If
            End If
    
            'RKL DEJ 2018-01-09 (START)
            SQL = "Select * From BOLPrintingExt Where BOLTableKey = " & ConvertToDouble(RSBOL("BOLTableKey").Value)
            RSBOLExt.CursorLocation = adUseClient
            RSBOLExt.CursorType = adOpenDynamic
                    
            RSBOLExt.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
            
            If RSBOL.State = 1 Then
                If RSBOLExt.EOF Or RSBOL.BOF Then
                    RSBOLExt.AddNew
                    RSBOLExt("BOLTableKey").Value = ConvertToDouble(RSBOL("BOLTableKey").Value)
                    
                    If UpdateRS(RSBOLExt) = False Then
                        MsgBox "There was an error saving the Ext data in LoadBOL.  If this persists contact your IT support.", vbInformation, "Warning"
                    End If
                End If
            Else
                'The BOL Ext Table was not obtained.
                MsgBox "Could not retrieve the BOL Ext data.", vbExclamation, "Data Not Available"
                Exit Function
            End If
            'RKL DEJ 2018-01-09 (STOP)
            
    gbLastActivityTime = Date + Time
                
            '*****************************************************************************
            '*****************************************************************************
            ' RKL DEJ 2016-05-03
            ' Populate fields from the Settings
            '*****************************************************************************
            '*****************************************************************************
            RSBOL("BOLAddressAndPhone").Value = gbBOLAddrPhone
            RSBOL("PASSPatent").Value = gbPASSPatent
            RSBOL("PrintPASSPatent").Value = Abs(gbPrintPASSPatent)
            
            '*****************************************************************************
            '*****************************************************************************
            ' Get Data from Load Record
            '*****************************************************************************
            '*****************************************************************************
            RSBOL("FrontRearFlag").Value = sBOLPass
            RSBOL("LoadsTableKey").Value = gbLoadsTableKey
            RSBOL("TruckNo").Value = rsLoad("TruckNo").Value
                            
            RSBOL("FrontGrossWt").Value = rsLoad("FrontGrossWt").Value
            RSBOL("RearGrossWt").Value = rsLoad("RearGrossWt").Value
            RSBOL("TotalGrossWt").Value = Val(RSBOL("FrontGrossWt").Value) + Val(RSBOL("RearGrossWt").Value)
            
            RSBOL("FrontTareWt").Value = rsLoad("FrontTareWt").Value
            RSBOL("RearTareWt").Value = rsLoad("RearTareWt").Value
            RSBOL("TotalTareWt").Value = Val(RSBOL("FrontTareWt").Value) + Val(RSBOL("RearTareWt").Value)
            
            RSBOL("FrontNetWt").Value = rsLoad("FrontNetWt").Value
            RSBOL("RearNetWt").Value = rsLoad("RearNetWt").Value
            RSBOL("TotalNetWt").Value = Val(RSBOL("FrontNetWt").Value) + Val(RSBOL("RearNetWt").Value)
            
            RSBOL("IsHandKeyed").Value = rsLoad("ManualScaling").Value
            RSBOL("IsSplitLoad").Value = rsLoad("SplitLoad").Value
            
            If Abs(ConvertToDouble(RSBOL("IsSplitLoad").Value)) = 1 Then
                If ConvertToString(RSBOL("FrontRearFlag").Value) = "Front" Then
                    RSBOL("NetTons").Value = ConvertToDouble(rsLoad("FrontTons").Value)
                Else
                    RSBOL("NetTons").Value = ConvertToDouble(rsLoad("RearTons").Value)
                End If
            Else
                RSBOL("NetTons").Value = ConvertToDouble(rsLoad("FrontTons").Value) + ConvertToDouble(rsLoad("RearTons").Value)
            End If
            
            
            RSBOL("ScaleInTime").Value = rsLoad("TimeIn").Value
            RSBOL("ScaleOutTime").Value = rsLoad("TimeOut").Value
                            
            RSBOL("InboundOperator").Value = rsLoad("InboundOperator").Value
            RSBOL("OutBoundOperator").Value = rsLoad("OutBoundOperator").Value

            'Get Front Additive (PTC)
            Select Case UCase(Trim(rsLoad("FrontProductClass").Value))
                   Case "DILUTE EMULS"
                        RSBOL("FrontAdditive").Value = "Water"
                   Case "CUTBACK"
                        SQL = "Select * From vimGetItemFormulaInfo_SGS Where FormulaName = '" & ConvertToString(rsLoad("FrontFormula").Value) & "' AND ItemID = '" & ConvertToString(rsLoad("FrontProductID").Value) & "' AND WhseID = '" & ConvertToString(rsLoad("FrontFacilityID").Value) & "'"
                        RSGetFormulaData.CursorLocation = adUseClient
                        RSGetFormulaData.CursorType = adOpenForwardOnly
                        RSGetFormulaData.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
                        If RSGetFormulaData.State = 1 Then
                            If RSGetFormulaData.RecordCount > 0 Then
                                RSGetFormulaData.MoveFirst
                                RSBOL("FrontAdditive").Value = RSGetFormulaData("AdditiveItemDesc").Value
                            End If
                        End If
                        RSGetFormulaData.Close
                   Case Else
                        If Trim(rsLoad("FrontAdditiveID").Value) = "" Or rsLoad("FrontAdditiveID").Value = "None" Then
                            RSBOL("FrontAdditive").Value = "None"
                        Else
                            RSBOL("FrontAdditive").Value = rsLoad("FrontAdditive").Value
                        End If
            End Select
            
            'Get Rear Additive (PTC)
            If Val(RSBOL("RearNetWt").Value) > 0 Then
                If Abs(SplitLoad) = 1 Then
                    Select Case UCase(rsLoad("RearProductClass").Value)
                           Case "DILUTE EMULS"
                                RSBOL("RearAdditive").Value = "Water"
                           Case "CUTBACK"
                                SQL = "Select * From vimGetItemFormulaInfo_SGS Where FormulaName = '" & ConvertToString(rsLoad("RearFormula").Value) & "' AND RearProductID = '" & ConvertToString(rsLoad("RearProductID").Value) & "' AND RearFacilityID = '" & ConvertToString(rsLoad("RearFacilityID").Value) & "'"
                                RSGetFormulaData.CursorLocation = adUseClient
                                RSGetFormulaData.CursorType = adOpenForwardOnly
                                RSGetFormulaData.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
                                If RSGetFormulaData.State = 1 Then
                                    If RSGetFormulaData.RecordCount > 0 Then
                                        RSGetFormulaData.MoveFirst
                                        RSBOL("RearAdditive").Value = RSGetFormulaData("AdditiveItemDesc").Value
                                    End If
                                End If
                                RSGetFormulaData.Close
                           Case Else
                                If Trim(rsLoad("RearAdditiveID").Value) = "" Or rsLoad("RearAdditiveID").Value = "None" Then
                                    RSBOL("RearAdditive").Value = "None"
                                Else
                                    RSBOL("RearAdditive").Value = rsLoad("RearAdditive").Value
                                End If
                    End Select
                Else
                    RSBOL("RearAdditive").Value = RSBOL("FrontAdditive").Value
                End If
            Else
                RSBOL("RearAdditive").Value = ""
            End If
            '(End PTC)

    gbLastActivityTime = Date + Time
            
            If sBOLPass = "Front" Then
                RSBOL("PrintCOA").Value = Abs(rsLoad("FrontPrintCOA").Value)
                RSBOL("COARequired").Value = rsLoad("FrontCOARequired").Value
                
                RSBOL("Trailer1_ID").Value = rsLoad("Trailer1_ID").Value
                RSBOL("Trailer2_ID").Value = rsLoad("Trailer2_ID").Value
                RSBOL("Carrier").Value = rsLoad("Carrier").Value
                RSBOL("Purchase_Order").Value = rsLoad("FrontPurchase_Order").Value

                RSBOL("Destination").Value = rsLoad("FrontDestAddress").Value
                RSBOL("DestDirections").Value = rsLoad("FrontDestDirections").Value
                RSBOL("Hauler").Value = rsLoad("FrontHauler").Value
                RSBOL("Customer").Value = rsLoad("FrontCustomer").Value
                RSBOL("Project").Value = rsLoad("FrontProject").Value
                RSBOL("ContractNbr").Value = rsLoad("FrontContractNbr").Value
                RSBOL("LoadTime").Value = rsLoad("FrontArrivalDt").Value
                RSBOL("DeliverTime").Value = rsLoad("FrontDestDt").Value
                
                RSBOL("WhseID").Value = rsLoad("FrontFacilityID").Value
                
                RSBOL("ProductClass").Value = rsLoad("FrontProductClass").Value
                RSBOL("ProductID").Value = rsLoad("FrontProductID").Value
                RSBOL("ProductDesc").Value = rsLoad("FrontProduct").Value
                
                RSBOL("AdditiveClass").Value = rsLoad("FrontAdditiveClass").Value
                RSBOL("AdditiveID").Value = rsLoad("FrontAdditiveID").Value
                RSBOL("AdditiveDesc").Value = rsLoad("FrontAdditive").Value
                
                RSBOL("TankNumber").Value = rsLoad("FrontTankNbr").Value
                RSBOL("RackNbr").Value = rsLoad("FrontRackNbr").Value
                RSBOL("Tons").Value = rsLoad("FrontTons").Value
                
                RSBOL("BOLNbr").Value = rsLoad("FrontBOL").Value
                RSBOL("LadingSibling").Value = Null
                
                RSBOL("IsCreditReturn").Value = rsLoad("FrontCreditReturn").Value
                
                RSBOL("FrontBaseGallons").Value = 0
                RSBOL("RearBaseGallons").Value = 0
                RSBOL("FrontTemp").Value = 0
                RSBOL("RearTemp").Value = 0
'                RSBOL("FrontAdditive").Value = ""
'                RSBOL("RearAdditive").Value = ""
                RSBOL("FrontAdditiveGallons").Value = 0
                RSBOL("RearAdditiveGallons").Value = 0
                RSBOL("TotalAdditiveGallons").Value = 0
                RSBOL("TotalBaseGallons").Value = 0 'ptc
                If SplitLoad = 0 Then
                    RSBOL("TargetGrossWt").Value = rsLoad("FrontTargetGrossWt").Value + rsLoad("RearTargetGrossWt").Value
                    RSBOL("TargetNetWt").Value = rsLoad("FrontTargetNetWt").Value + rsLoad("RearTargetNetWt").Value
                    RSBOL("WtPerGallon").Value = rsLoad("FrontWtPerGallon").Value
                    RSBOL("TargetGallons").Value = rsLoad("FrontTargetGallons").Value + rsLoad("RearTargetGallons").Value
                    RSBOL("SelectedGallons").Value = rsLoad("SelectedFrontGallons").Value + rsLoad("SelectedRearGallons").Value
                Else
                    RSBOL("TargetGrossWt").Value = rsLoad("FrontTargetGrossWt").Value
                    RSBOL("TargetNetWt").Value = rsLoad("FrontTargetNetWt").Value
                    RSBOL("WtPerGallon").Value = rsLoad("FrontWtPerGallon").Value
                    RSBOL("TargetGallons").Value = rsLoad("FrontTargetGallons").Value
                    RSBOL("SelectedGallons").Value = rsLoad("SelectedFrontGallons").Value
                End If
                
                RSBOL("HighRisk").Value = rsLoad("FrontHighRisk").Value
                RSBOL("SecurityTapNumber").Value = rsLoad("FrontSecurityTapNumber").Value
                
                RSBOL("Carrier_ID").Value = rsLoad("FrontCarrier_ID").Value
                RSBOL("Carrier_Name").Value = rsLoad("FrontCarrier_Name").Value
                
                'RKL DEJ 2017-12-18 (START)
                If rsLoadExt.State = 1 Then
                    RSBOLExt("SrcTankNbrOne").Value = rsLoadExt("FrontSrcTankNbrOne").Value
                    RSBOLExt("SrcTankNbrTwo").Value = rsLoadExt("FrontSrcTankNbrTwo").Value
                    RSBOLExt("SrcTankNbrThree").Value = rsLoadExt("FrontSrcTankNbrThree").Value
                    
                    RSBOLExt("SrcQtyOne").Value = rsLoadExt("FrontSrcQtyOne").Value
                    RSBOLExt("SrcQtyTwo").Value = rsLoadExt("FrontSrcQtyTwo").Value
                    RSBOLExt("SrcQtyThree").Value = rsLoadExt("FrontSrcQtyThree").Value
                    
                    RSBOLExt("SrcProductIDOne").Value = rsLoadExt("FrontSrcProductIDOne").Value
                    RSBOLExt("SrcProductOne").Value = rsLoadExt("FrontSrcProductOne").Value
                    RSBOLExt("SrcProductClassOne").Value = rsLoadExt("FrontSrcProductClassOne").Value
                    
                    RSBOLExt("SrcProductIDTwo").Value = rsLoadExt("FrontSrcProductIDTwo").Value
                    RSBOLExt("SrcProductTwo").Value = rsLoadExt("FrontSrcProductTwo").Value
                    RSBOLExt("SrcProductClassTwo").Value = rsLoadExt("FrontSrcProductClassTwo").Value
                    
                    RSBOLExt("SrcProductIDThree").Value = rsLoadExt("FrontSrcProductIDThree").Value
                    RSBOLExt("SrcProductThree").Value = rsLoadExt("FrontSrcProductThree").Value
                    RSBOLExt("SrcProductClassThree").Value = rsLoadExt("FrontSrcProductClassThree").Value
                End If
                'RKL DEJ 2017-12-18 (STOP)

            Else 'Rear BOL
                RSBOL("PrintCOA").Value = Abs(rsLoad("RearPrintCOA").Value)
                RSBOL("COARequired").Value = rsLoad("RearCOARequired").Value
                
                RSBOL("Trailer1_ID").Value = Trailer1_ID
                RSBOL("Trailer2_ID").Value = Trailer2_ID
                RSBOL("Carrier").Value = Carrier
                RSBOL("Purchase_Order").Value = rsLoad("RearPurchase_Order").Value
                
                RSBOL("Destination").Value = rsLoad("RearDestAddress").Value
                RSBOL("DestDirections").Value = rsLoad("RearDestDirections").Value
                RSBOL("Hauler").Value = rsLoad("RearHauler").Value
                RSBOL("Customer").Value = rsLoad("RearCustomer").Value
                RSBOL("Project").Value = rsLoad("RearProject").Value
                RSBOL("ContractNbr").Value = rsLoad("RearContractNbr").Value
                RSBOL("LoadTime").Value = rsLoad("RearArrivalDt").Value
                RSBOL("DeliverTime").Value = rsLoad("RearDestDt").Value
                
                RSBOL("WhseID").Value = rsLoad("RearFacilityID").Value
                
                RSBOL("ProductClass").Value = rsLoad("RearProductClass").Value
                RSBOL("ProductID").Value = rsLoad("RearProductID").Value
                RSBOL("ProductDesc").Value = rsLoad("RearProduct").Value
                
                RSBOL("AdditiveClass").Value = rsLoad("RearAdditiveClass").Value
                RSBOL("AdditiveID").Value = rsLoad("RearAdditiveID").Value
                RSBOL("AdditiveDesc").Value = rsLoad("RearAdditive").Value
                
                RSBOL("TankNumber").Value = rsLoad("RearTankNbr").Value
                RSBOL("RackNbr").Value = rsLoad("RearRackNbr").Value
                RSBOL("Tons").Value = rsLoad("RearTons").Value
                
                RSBOL("BOLNbr").Value = rsLoad("RearBOL").Value
                RSBOL("LadingSibling").Value = FrontBOL
            
                RSBOL("IsCreditReturn").Value = rsLoad("RearCreditReturn").Value
                
                RSBOL("FrontBaseGallons").Value = FrontBaseGallons
                RSBOL("RearBaseGallons").Value = RearBaseGallons
                RSBOL("FrontTemp").Value = FrontTemp
                RSBOL("RearTemp").Value = RearTemp
                RSBOL("FrontAdditive").Value = FrontAdditive
                RSBOL("RearAdditive").Value = RearAdditive
                RSBOL("FrontAdditiveGallons").Value = FrontAdditiveGallons
                RSBOL("RearAdditiveGallons").Value = RearAdditiveGallons
                RSBOL("TotalAdditiveGallons").Value = TotalAdditiveGallons
                RSBOL("TotalBaseGallons").Value = TotalBaseGallons 'ptc
                
                RSBOL("TotalGallons").Value = TotalGallons
                RSBOL("GrossGallons").Value = GrossGallons
                
                RSBOL("LoadTemp").Value = RearTemp
                RSBOL("TargetGrossWt").Value = rsLoad("RearTargetGrossWt").Value
                RSBOL("TargetNetWt").Value = rsLoad("RearTargetNetWt").Value
                RSBOL("WtPerGallon").Value = rsLoad("RearWtPerGallon").Value
                RSBOL("TargetGallons").Value = rsLoad("RearTargetGallons").Value
                RSBOL("SelectedGallons").Value = rsLoad("SelectedRearGallons").Value
            
                RSBOL("HighRisk").Value = rsLoad("RearHighRisk").Value
                RSBOL("SecurityTapNumber").Value = rsLoad("RearSecurityTapNumber").Value
                
                RSBOL("Carrier_ID").Value = rsLoad("RearCarrier_ID").Value
                RSBOL("Carrier_Name").Value = rsLoad("RearCarrier_Name").Value
                
                'RKL DEJ 2017-12-18 (START)
                RSBOLExt("SrcTankNbrOne").Value = rsLoadExt("RearSrcTankNbrOne").Value
                RSBOLExt("SrcTankNbrTwo").Value = rsLoadExt("RearSrcTankNbrTwo").Value
                RSBOLExt("SrcTankNbrThree").Value = rsLoadExt("RearSrcTankNbrThree").Value
                
                RSBOLExt("SrcQtyOne").Value = rsLoadExt("RearSrcQtyOne").Value
                RSBOLExt("SrcQtyTwo").Value = rsLoadExt("RearSrcQtyTwo").Value
                RSBOLExt("SrcQtyThree").Value = rsLoadExt("RearSrcQtyThree").Value
                
                RSBOLExt("SrcProductIDOne").Value = rsLoadExt("RearSrcProductIDOne").Value
                RSBOLExt("SrcProductOne").Value = rsLoadExt("RearSrcProductOne").Value
                RSBOLExt("SrcProductClassOne").Value = rsLoadExt("RearSrcProductClassOne").Value
                
                RSBOLExt("SrcProductIDTwo").Value = rsLoadExt("RearSrcProductIDTwo").Value
                RSBOLExt("SrcProductTwo").Value = rsLoadExt("RearSrcProductTwo").Value
                RSBOLExt("SrcProductClassTwo").Value = rsLoadExt("RearSrcProductClassTwo").Value
                
                RSBOLExt("SrcProductIDThree").Value = rsLoadExt("RearSrcProductIDThree").Value
                RSBOLExt("SrcProductThree").Value = rsLoadExt("RearSrcProductThree").Value
                RSBOLExt("SrcProductClassThree").Value = rsLoadExt("RearSrcProductClassThree").Value
                'RKL DEJ 2017-12-18 (STOP)
                
            End If
            
            
            'RKL DEJ 2017-12-18 (START)
            RSBOLExt("FrontSrcTankNbrOne").Value = rsLoadExt("FrontSrcTankNbrOne").Value
            RSBOLExt("FrontSrcTankNbrTwo").Value = rsLoadExt("FrontSrcTankNbrTwo").Value
            RSBOLExt("FrontSrcTankNbrThree").Value = rsLoadExt("FrontSrcTankNbrThree").Value
            
            RSBOLExt("FrontSrcQtyOne").Value = rsLoadExt("FrontSrcQtyOne").Value
            RSBOLExt("FrontSrcQtyTwo").Value = rsLoadExt("FrontSrcQtyTwo").Value
            RSBOLExt("FrontSrcQtyThree").Value = rsLoadExt("FrontSrcQtyThree").Value
            
            RSBOLExt("FrontSrcProductIDOne").Value = rsLoadExt("FrontSrcProductIDOne").Value
            RSBOLExt("FrontSrcProductOne").Value = rsLoadExt("FrontSrcProductOne").Value
            RSBOLExt("FrontSrcProductClassOne").Value = rsLoadExt("FrontSrcProductClassOne").Value
            
            RSBOLExt("FrontSrcProductIDTwo").Value = rsLoadExt("FrontSrcProductIDTwo").Value
            RSBOLExt("FrontSrcProductTwo").Value = rsLoadExt("FrontSrcProductTwo").Value
            RSBOLExt("FrontSrcProductClassTwo").Value = rsLoadExt("FrontSrcProductClassTwo").Value
            
            RSBOLExt("FrontSrcProductIDThree").Value = rsLoadExt("FrontSrcProductIDThree").Value
            RSBOLExt("FrontSrcProductThree").Value = rsLoadExt("FrontSrcProductThree").Value
            RSBOLExt("FrontSrcProductClassThree").Value = rsLoadExt("FrontSrcProductClassThree").Value
            
            RSBOLExt("RearSrcTankNbrOne").Value = rsLoadExt("RearSrcTankNbrOne").Value
            RSBOLExt("RearSrcTankNbrTwo").Value = rsLoadExt("RearSrcTankNbrTwo").Value
            RSBOLExt("RearSrcTankNbrThree").Value = rsLoadExt("RearSrcTankNbrThree").Value
            
            RSBOLExt("RearSrcQtyOne").Value = rsLoadExt("RearSrcQtyOne").Value
            RSBOLExt("RearSrcQtyTwo").Value = rsLoadExt("RearSrcQtyTwo").Value
            RSBOLExt("RearSrcQtyThree").Value = rsLoadExt("RearSrcQtyThree").Value
            
            RSBOLExt("RearSrcProductIDOne").Value = rsLoadExt("RearSrcProductIDOne").Value
            RSBOLExt("RearSrcProductOne").Value = rsLoadExt("RearSrcProductOne").Value
            RSBOLExt("RearSrcProductClassOne").Value = rsLoadExt("RearSrcProductClassOne").Value
            
            RSBOLExt("RearSrcProductIDTwo").Value = rsLoadExt("RearSrcProductIDTwo").Value
            RSBOLExt("RearSrcProductTwo").Value = rsLoadExt("RearSrcProductTwo").Value
            RSBOLExt("RearSrcProductClassTwo").Value = rsLoadExt("RearSrcProductClassTwo").Value
            
            RSBOLExt("RearSrcProductIDThree").Value = rsLoadExt("RearSrcProductIDThree").Value
            RSBOLExt("RearSrcProductThree").Value = rsLoadExt("RearSrcProductThree").Value
            RSBOLExt("RearSrcProductClassThree").Value = rsLoadExt("RearSrcProductClassThree").Value
            
            RSBOLExt("IsTruckBlend").Value = rsLoadExt("IsTruckBlend").Value
            
            RSBOLExt("LMEDestLocationCode").Value = rsLoadExt("LMEDestLocationCode").Value
            RSBOLExt("LMEDestAddress1").Value = rsLoadExt("LMEDestAddress1").Value
            RSBOLExt("LMEDestAddress2").Value = rsLoadExt("LMEDestAddress2").Value
            RSBOLExt("LMEDestcity_Name").Value = rsLoadExt("LMEDestcity_Name").Value
            RSBOLExt("LMEDestState").Value = rsLoadExt("LMEDestState").Value
            RSBOLExt("LMEDestzip_code").Value = rsLoadExt("LMEDestzip_code").Value
            RSBOLExt("LMEDestCustomer_ID").Value = rsLoadExt("LMEDestCustomer_ID").Value
            RSBOLExt("LMEDestAddrName").Value = rsLoadExt("LMEDestAddrName").Value

            'RKL DEJ 2017-12-18 (STOP)
            
            '*********************************************************************************************
            'RKL DEJ 5/1/14 Kevin Added more logic to the ViscosityTemp So moved to new methods (START)
            '*********************************************************************************************
'            'Change Request by Kevin on July 23, 2012 3:34 PM
'            '"Need to make a change. If it start with CQS, CSS, SC-H118, or quickseal, then it need to be 77F. Everything else stays 122."
'            'Change made by DEJ 7/31/12
''            If Mid(UCase(ConvertToString(RSBOL("ProductDesc").Value)), 1, 5) = "CSS-1" Or _
''               Mid(UCase(ConvertToString(RSBOL("ProductDesc").Value)), 1, 7) = "SC-H118" Then
'            If Mid(UCase(ConvertToString(RSBOL("ProductDesc").Value)), 1, 9) = "QUICKSEAL" Or _
'               Mid(UCase(ConvertToString(RSBOL("ProductDesc").Value)), 1, 7) = "SC-H118" Or _
'               Mid(UCase(ConvertToString(RSBOL("ProductDesc").Value)), 1, 3) = "CQS" Or _
'               Mid(UCase(ConvertToString(RSBOL("ProductDesc").Value)), 1, 3) = "CSS" _
'               Then
'                    RSBOL("ViscosityTemp").Value = 77
'            Else
'                    RSBOL("ViscosityTemp").Value = 122
'            End If
'
            RSBOL("ViscosityTemp").Value = GetViscosityTemp(ConvertToString(RSBOL("ProductID").Value), ConvertToString(RSBOL("ProductDesc").Value))
            '*********************************************************************************************
            'RKL DEJ 5/1/14 Kevin Added more logic to the ViscosityTemp So moved to new methods (STOP)
            '*********************************************************************************************
            
            
            If bUpdateMode = True Then 'ptc
                'Skip updating this stuff again in order to avoid loading more columns than the
                '   recordset can apparently handle.  The UpdateBatch fails when RS contains
                '   more than 120 fields
            Else
                '*****************************************************************************
                '*****************************************************************************
                ' Get Data from vluGetBOLInfo_SGS View in MAS
                '*****************************************************************************
                '*****************************************************************************
                'This will get the Custom Data from MAS 500
                If sBOLPass = "Front" Then
                    SQL = "Select Distinct * From vluGetBOLInfo_SGS Where loadid = " & ConvertToDouble(rsLoad("FrontMcLeodOrderNumber").Value)
    '                SQL = "Select Distinct * From vluGetBOLInfo_SGS Where loadid = " & ConvertToDouble(rsLoad("Frontbol").Value)
                Else
                    If ConvertToDouble(rsLoad("RearMcLeodOrderNumber").Value) = 0 Then
                        'Order Number NOT saved in Load Processing Screen (Bug!) - Get Order Number from BOL
                        Dim sOrderNo As String
                        sOrderNo = Mid(Trim(ConvertToString(rsLoad("RearBOL").Value)), 3)
                        rsLoad("RearMcLeodOrderNumber").Value = Right("0000000" + sOrderNo, 7)
                    End If
                    SQL = "Select Distinct * From vluGetBOLInfo_SGS Where loadid = " & ConvertToDouble(rsLoad("RearMcLeodOrderNumber").Value)
    '                SQL = "Select Distinct * From vluGetBOLInfo_SGS Where loadid = " & ConvertToDouble(rsLoad("Rearbol").Value)
                End If
                
                RSGetBOLData.CursorLocation = adUseClient
                RSGetBOLData.CursorType = adOpenDynamic
                RSGetBOLData.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
                
                If RSGetBOLData.State = 1 Then
                    If RSGetBOLData.RecordCount > 0 Then
                        RSGetBOLData.MoveFirst
                        
                        RSBOL("MASloadid").Value = RSGetBOLData("loadid").Value
                        RSBOL("MAScustref").Value = RSGetBOLData("custref").Value
                        RSBOL("MASprebillid").Value = RSGetBOLData("prebillid").Value
                        RSBOL("MAScustomer").Value = RSGetBOLData("customer").Value
                        RSBOL("MAScustomername").Value = RSGetBOLData("customername").Value
                        RSBOL("MASscaledate").Value = RSGetBOLData("scaledate").Value
                        RSBOL("MAScontract").Value = RSGetBOLData("contract").Value
                        RSBOL("MAScustomerpo").Value = RSGetBOLData("customerpo").Value
                        
                        RSBOL("MASFacility").Value = RSGetBOLData("Facility").Value
                        RSBOL("MASFacilityName").Value = RSGetBOLData("FacilityName").Value
                            
                        RSBOL("BOLNbr").Value = ConvertToDouble(RSGetBOLData("Facility").Value) & "-" & ConvertToDouble(RSGetBOLData("loadid").Value)
                        
                        RSBOL("MASProjectNumber").Value = RSGetBOLData("ProjectNumber").Value
                        RSBOL("MASProjectDescription").Value = RSGetBOLData("ProjectDescription").Value
                        RSBOL("MAScarrier").Value = RSGetBOLData("carrier").Value
                        RSBOL("MAStruck").Value = RSGetBOLData("truck").Value
                        RSBOL("MAStrailer1").Value = RSGetBOLData("trailer1").Value
                        RSBOL("MAStrailer2").Value = RSGetBOLData("trailer2").Value
                        RSBOL("MASloadtime").Value = RSGetBOLData("loadtime").Value
                        RSBOL("MASdeliverytime").Value = RSGetBOLData("deliverytime").Value
                        RSBOL("MASshortdestination").Value = RSGetBOLData("shortdestination").Value
                        RSBOL("MASdestination").Value = RSGetBOLData("destination").Value
                        RSBOL("MASpenetration").Value = RSGetBOLData("penetration").Value
                        RSBOL("MASpenetrationtemperature").Value = RSGetBOLData("penetrationtemperature").Value
                        RSBOL("MASpenetrationremark").Value = RSGetBOLData("penetrationremark").Value
                        
                        RSBOL("MASflash").Value = RSGetBOLData("flash").Value
                        RSBOL("Flashpoint").Value = ConvertToDouble(RSGetBOLData("flash").Value)
                        
                        RSBOL("MASlbspergallon").Value = RSGetBOLData("lbspergallon").Value
                        RSBOL("lbspergallon").Value = ConvertToDouble(RSGetBOLData("lbspergallon").Value)
                        RSBOL("lbspergallon2").Value = ConvertToDouble(RSGetBOLData("lbspergallon").Value)
                        
                        RSBOL("MASspecificgravity").Value = RSGetBOLData("specificgravity").Value
                        RSBOL("SpecificGravity").Value = ConvertToDouble(RSGetBOLData("specificgravity").Value)
                        
                        RSBOL("MASviscosity").Value = RSGetBOLData("viscosity").Value
                        RSBOL("Viscosity").Value = ConvertToDouble(RSGetBOLData("viscosity").Value)
                        
                        RSBOL("MASviscositytemperature").Value = RSGetBOLData("viscositytemperature").Value
    ''Debug / Compare Code
    ''ViscosityTemp
    'If ConvertToDouble(RSBOL("MASviscositytemperature").Value) <> 0 And ConvertToDouble(RSBOL("MASviscositytemperature").Value) <> RSBOL("MASviscositytemperature").Value Then
    '    MsgBox "The MAS 500 Viscosity Temperature is not the same as the calcaulated Temperature.", vbExclamation, "Viscosity Temp Not the Same"
    'End If
                        RSBOL("MASviscosityremark").Value = RSGetBOLData("viscosityremark").Value
                        RSBOL("MAScompliance").Value = RSGetBOLData("compliance").Value
                        RSBOL("MASadditive").Value = RSGetBOLData("additive").Value
                        RSBOL("MASadditivedescription").Value = RSGetBOLData("additivedescription").Value
                        RSBOL("MASadditivepercent").Value = RSGetBOLData("additivepercent").Value
                        RSBOL("MAStemperaturemultiplier").Value = ConvertToDouble(RSGetBOLData("temperaturemultiplier").Value)
                        RSBOL("MASspecificationremark").Value = RSGetBOLData("specificationremark").Value
                        RSBOL("MASplacardtype").Value = RSGetBOLData("placardtype").Value
                        RSBOL("MASitem").Value = RSGetBOLData("item").Value
                        RSBOL("MASitemdescription").Value = RSGetBOLData("itemdescription").Value
                        RSBOL("MASCommodity_ID").Value = RSGetBOLData("Commodity_ID").Value
                        RSBOL("MASCommodity").Value = RSGetBOLData("Commodity").Value
                        RSBOL("MASFromCompanyID").Value = RSGetBOLData("FromCompanyID").Value
                        RSBOL("MASFromWhseID").Value = RSGetBOLData("FromWhseID").Value
                        RSBOL("MASToCompanyID").Value = RSGetBOLData("ToCompanyID").Value
                        RSBOL("MASToWhseID").Value = RSGetBOLData("ToWhseID").Value
                        
                        'RKL DEJ 2016-09-07 Start
                        RSBOL("MASDeliveryLoc").Value = RSGetBOLData("DeliveryLoc").Value
                        'RKL DEJ 2016-09-07 Stop
                        
                    End If
                End If
            End If 'ptc
            
    gbLastActivityTime = Date + Time
            '*****************************************************************************
            '*****************************************************************************
            ' Calculated Fields
            '*****************************************************************************
            '*****************************************************************************
            If IsNull(RSBOL("LbsPerGallon").Value) = True Then
                RSBOL("LbsPerGallon").Value = 0
            End If
            
            If IsNull(RSBOL("LbsPerGallon2").Value) = True Then
                RSBOL("LbsPerGallon2").Value = 0
            End If
            
            If IsNull(RSBOL("TotalNetWt").Value) = True Then
                RSBOL("TotalNetWt").Value = 0
            End If
            
            If RSBOL("LbsPerGallon2").Value = 0 Then
                RSBOL("NetGallons").Value = 0
            Else
                If Abs(SplitLoad) = 1 Then
                    If sBOLPass = "Front" Then
                        RSBOL("NetGallons").Value = RSBOL("FrontNetWt").Value / RSBOL("LbsPerGallon2").Value
                    Else 'Rear
                        RSBOL("NetGallons").Value = RSBOL("RearNetWt").Value / RSBOL("LbsPerGallon2").Value
                    End If
                Else
                    RSBOL("NetGallons").Value = RSBOL("TotalNetWt").Value / RSBOL("LbsPerGallon2").Value
                End If
            End If
            
            If RSBOL("MAStemperaturemultiplier").Value = 0 Then
                RSBOL("GrossGallons").Value = 0
            Else
                RSBOL("GrossGallons").Value = RSBOL("NetGallons").Value / RSBOL("MAStemperaturemultiplier").Value
            End If
            
            'RKL DEJ 2018-01-12 Instructed to defalut Base Gals from Net Galls
            RSBOL("FrontBaseGallons").Value = RSBOL("NetGallons").Value
            
''Debug
'If RSBOL("NetTons").Value <> (RSBOL("TotalNetWt").Value / 2000) Then
'    MsgBox "The Net Tons Calculation is Off: " & vbCrLf & _
'    "'RSBOL('NetTons').Value' = " & RSBOL("NetTons").Value & _
'    "'(RSBOL('TotalNetWt').Value / 2000)' = " & (RSBOL("TotalNetWt").Value / 2000), vbExclamation, "Net Tons Calculation Off"
'End If
            
            RSBOL("Remarks").Value = "(" & Format(ConvertToDouble(RSBOL("NetTons").Value) * 0.9072, "#,###0.000") & " METRIC TONS) "
            
'                '*****************************************************************************
'                '*****************************************************************************
'                ' Get the next Weight Ticket Number
'                '*****************************************************************************
'                '*****************************************************************************
'                WeightTicketNo = ConvertToDouble(GetNextWeightTicketNumber)
'                If WeightTicketNo <= 0 Then
'                    MsgBox "Could not create the next Weight Ticket Number.", vbExclamation, "Missing Weight Ticket"
'                Else
'                    RSBOL("WeightTicketNo").Value = WeightTicketNo
'                End If
                            
            '*****************************************************************************
            '*****************************************************************************
            ' Set Defaults
            '*****************************************************************************
            '*****************************************************************************
            
            RSBOL("Flashpoint").Value = ConvertToDouble(RSBOL("Flashpoint").Value)
            RSBOL("SpecificGravity").Value = ConvertToDouble(RSBOL("SpecificGravity").Value)
            RSBOL("LotNbr").Value = ""
            RSBOL("LoadTemp").Value = 0
            RSBOL("Viscosity").Value = ConvertToDouble(RSBOL("Viscosity").Value)
            RSBOL("Residue").Value = 0
            
            RSBOL("UserID").Value = gbPlant
            If gbMANUALSCALING = True Then
                RSBOL("IsScaleEnabled").Value = 0
            Else
                RSBOL("IsScaleEnabled").Value = 1
            End If
            
            Select Case gbPlantPrefix
                   Case 6 'Woods Cross (Plant 6) has fewer requirements
                        txtLotNr.BackColor = &H8000000F
                        txtLotNr.Locked = True
'********************************************************************************************************
'********************************************************************************************************
'RKL DEJ 9/25/13 (START)
'Comment out the origianl code of always locking the Additive Gallons
'Add logic to only lock if not a Emulsion or Dilute Emul
'********************************************************************************************************
'********************************************************************************************************

'                        txtFrontAdditiveGals.BackColor = &H8000000F
'                        txtFrontAdditiveGals.Locked = True
'                        txtRearAdditiveGals.BackColor = &H8000000F
'                        txtRearAdditiveGals.Locked = True

                        'If Product Class = 'DILUTE EMULS' then require and allow edit
                        If Trim(UCase(ConvertToString(RSBOL("ProductClass").Value))) = "DILUTE EMULS" Then
                            txtFrontAdditiveGals.BackColor = &H80000005
                            txtFrontAdditiveGals.Locked = False
                            txtRearAdditiveGals.BackColor = &H80000005
                            txtRearAdditiveGals.Locked = False
                        Else
                            txtFrontAdditiveGals.BackColor = &H8000000F
                            txtFrontAdditiveGals.Locked = True
                            txtRearAdditiveGals.BackColor = &H8000000F
                            txtRearAdditiveGals.Locked = True
                        End If
'********************************************************************************************************
'********************************************************************************************************
'RKL DEJ 9/25/13 (STOP)
'********************************************************************************************************
'********************************************************************************************************

                   Case 7 'Rawlins (Plant 7) has fewer requirements
                        txtLotNr.BackColor = &H8000000F
                        txtLotNr.Locked = True
                   Case Else
                        txtLotNr.BackColor = &H80000005
                        txtLotNr.Locked = False
                        'If Product Class = Emulsion then require and allow edit
                        If Trim(UCase(ConvertToString(RSBOL("ProductClass").Value))) = "EMULSION" Or Trim(UCase(ConvertToString(RSBOL("ProductClass").Value))) = "DILUTE EMULS" Then
                            txtViscocity.BackColor = &H80000005
                            txtViscocity.Locked = False
                            If gbPlantPrefix <> 5 Then 'Blackfoot (Plant 5) has fewer requirements
                                txtResidue.BackColor = &H80000005
                                txtResidue.Locked = False
                            End If
                        Else
                            txtViscocity.BackColor = &H8000000F
                            txtViscocity.Locked = True
                            txtResidue.BackColor = &H8000000F
                            txtResidue.Locked = True
                        End If
            End Select
            
'RKL DEJ 3/20/14 (START)
'Lookup Lot and Viscosity and Residue for all records
    LotNumber = Trim(GetLotNumber(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value), COATestRsltsKey))
    If LotNumber <> Empty Then
        RSBOL("LotNbr").Value = LotNumber
        
        'Pull Viscosity and Residue from COA tests
        RSBOL("Viscosity").Value = ConvertToDouble(GetCOAViscosity(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value), ConvertToString(RSBOL("Viscosity").Value)))
        RSBOL("Residue").Value = GetCOAResidue(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value), "0")
        
        'RKL DEJ 2017-12-21 If COATestRsltsKey was aquired then save it to record
        If COATestRsltsKey > 0 Then
'            RSBOL("COATestRsltsKey").Value = COATestRsltsKey
            RSBOLExt("COATestRsltsKey").Value = COATestRsltsKey
        End If
    End If
'RKL DEJ 3/20/14 (STOP)

            
'Get COA Lot Number if required
If RSBOL("PrintCOA").Value = 1 And gbEnforceCOA = True And Abs(ConvertToDouble(RSBOL("IsCreditReturn").Value)) <> 1 Then
    'Commented out... this was moved above RKL DEJ 3/20/14
    LotNumber = Trim(GetLotNumber(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value)))
    
    If LotNumber = Empty Then
        'There is no COA data to print (Not a Active Lot Number)
        'If Require don't allow to print/proceed Else give a warning
        If RSBOL("COARequired").Value = 1 Then
            'Required don't allow to print unless User allows printing in MAS
            If CanPrintBOL(RSBOL("ContractNbr").Value) = True Then
                'Required but allow to print BOL
                MsgBox "The COA is required, and there is not an Active Lot Number to print a COA.  You can still print the BOL but no COA will be printed.", vbInformation, "Scale Pass"
                cmdPrintBOL.Enabled = True
                txtLotNr.Locked = False
            Else
                'Required don't allow to print
                MsgBox "The COA is required, and there is not an Active Lot Number to print a COA.  You cannot print the BOL without a Active Lot and valid COA.", vbInformation, "Scale Pass"
                cmdPrintBOL.Enabled = False
                txtLotNr.Locked = True
            End If
        Else
            'Not required so allow to print
            MsgBox "There is not an Active Lot Number to print a COA.  You can still print the BOL but no COA will be printed.", vbInformation, "Scale Pass"
            cmdPrintBOL.Enabled = True
            txtLotNr.Locked = False
        End If
    Else
        'Commented out... this was moved above RKL DEJ 3/20/14
'        RSBOL("LotNbr").Value = LotNumber
    
    
        txtLotNr.BackColor = &H8000000F
        txtLotNr.Locked = True
        
        'Commented out... this was moved above RKL DEJ 3/20/14
'        'RKL DEJ 9/25/13 (START) - Pull Viscosity and Residue from COA tests
'        RSBOL("Viscosity").Value = ConvertToDouble(GetCOAViscosity(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value), ConvertToString(RSBOL("Viscosity").Value)))
'        RSBOL("Residue").Value = GetCOAResidue(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value), 0)
'        'RKL DEJ 9/25/13 (STOP) - Pull Viscosity and Residue from COA tests
    
    End If
Else
    cmdPrintBOL.Enabled = True
    txtLotNr.Locked = False
End If
            
'            If Abs(SplitLoad) = 1 Then
'                txtRearBaseGals.BackColor = &H80000005
'                txtRearBaseGals.Locked = False
'
'                txtRearTemp.BackColor = &H80000005
'                txtRearTemp.Locked = False
'
'                cboRearAdditive.BackColor = &H80000005
'                cboRearAdditive.Locked = False
'
'                txtRearAdditiveGals.BackColor = &H80000005
'                txtRearAdditiveGals.Locked = False
'
'            Else
'                txtRearBaseGals.BackColor = &H8000000F
'                txtRearBaseGals.Locked = True
'
'                txtRearTemp.BackColor = &H8000000F
'                txtRearTemp.Locked = True
'
'                cboRearAdditive.BackColor = &H8000000F
'                cboRearAdditive.Locked = True
'
'                txtRearAdditiveGals.BackColor = &H8000000F
'                txtRearAdditiveGals.Locked = True
'
'            End If
            '*****************************************************************************
            '*****************************************************************************
            ' Save Record
            '*****************************************************************************
            '*****************************************************************************
'            RSBOL.Update
            If UpdateRS(RSBOL) = False Then
                MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
            End If
        End If
    
    Else
        'The Parent Load Table was not obtained.
        MsgBox "Could not retrieve the Load or BOL data.", vbExclamation, "Data Not Available"
        Exit Function
    End If
    
    
    
    gbLastActivityTime = Date + Time
    
    Call BindBOLObjects

    'RKL DEJ 2016-06-29 (START)
    Dim lcShipToState As String
    
    If gbCustOwnedProd = False Then
        lcShipToState = UCase(Trim(GetMASShipToState(Trim(txtContract.Text), Trim(RSBOL("ProductID").Value))))
        
        'Test code only (START)
        If 1 = 2 Then
            lcShipToState = "NM"
            lcShipToState = "TX"
        End If
        'Test code only (STOP)
        
        RSBOL("ShipToState").Value = lcShipToState
    Else
        'RKL DEJ 2018-01-05 added if statement and this new ShiptToState logic.
        GetLMEContractData RSBOL("MASCustomer").Value, RSBOL("ContractNbr").Value
        
        lcShipToState = ConvertToString(RSBOL("ShipToState").Value)
    End If
    
    
    If UCase(gbPlant) = "ROSWELL" Then
        txtBatchNbr.Visible = True
        lblBatchNbr.Visible = True
        
        Select Case lcShipToState
            Case "TX"
                RSBOL("LotNbrCaption").Value = "Lab #"
            Case "NM"
                RSBOL("LotNbrCaption").Value = "Seal #"
            Case Else
                RSBOL("LotNbrCaption").Value = "Lot No."
        End Select
    Else
        txtBatchNbr.Visible = False
        lblBatchNbr.Visible = False
        
        Select Case lcShipToState
            Case "TX"
                RSBOL("LotNbrCaption").Value = "Lab No."
            Case "NM"
                RSBOL("LotNbrCaption").Value = "Seal No."
            Case Else
                RSBOL("LotNbrCaption").Value = "Lot No."
        End Select
    End If
    
    lblLotNumber.Caption = RSBOL("LotNbrCaption").Value
    'RKL DEJ 2016-06-29 (STOP)
    
    'RKL DEJ 2018-01-16 added call to txtFrontBaseGals_Change to populate the totals
    Call txtFrontBaseGals_Change

    'BOL was loaded Successfully
    LoadBOL = True
    
CleanUP:
    On Error Resume Next
    If RSGetBOLData.State = 1 Then RSGetBOLData.Close
    Set RSGetBOLData = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".LoadBOL()" & vbCrLf & _
    "The following error occurred while creating / retreaving the Load: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    Resume Next
    
    gbLastActivityTime = Date + Time
    GoTo CleanUP
End Function




Public Function UpdateMAS500() As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim LoadsTableKey As Long
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    'Validate MAS Connection
    If gbLocalMode = False Then
        
        CurrentLine = 1
        
        If HaveLostMASConnection = True Then
            
            CurrentLine = 2
            
            'No longer connected to MAS 500
            Exit Function
        End If
    End If
    
    
    CurrentLine = 3
    
    SQL = "Select * From BOLPrinting Where HasPrintedSuccessfully = 1 and (HasMASBOLLogBeenCreated is null or HasMASBOLLogBeenCreated <> 1 or HasMASSOBeenCreated is null or HasMASSOBeenCreated <> 1 or HasBeenSentToMAS is null or HasBeenSentToMAS <> 1)"
        
    CurrentLine = 4
    
    'RKL DEJ 2017-07-27 (START) Added becuase we are getting errors that the RSBOL is already open
    If RSBOL.State = 1 Then
        CurrentLine = 4001
        RSBOL.Close
        CurrentLine = 4002
    End If
    CurrentLine = 4003
    'RKL DEJ 2017-07-27 (STOP) Added becuase we are getting errors that the RSBOL is already open
    
    
    RSBOL.CursorLocation = adUseClient
    CurrentLine = 5
    RSBOL.CursorType = adOpenDynamic
    
    CurrentLine = 6
    RSBOL.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    CurrentLine = 7
    If RSBOL.State = 1 Then
        CurrentLine = 8
        If RSBOL.RecordCount > 0 Then
            CurrentLine = 9
            RSBOL.MoveFirst
            CurrentLine = 10
            While Not RSBOL.EOF And Not RSBOL.BOF
                
                CurrentLine = 11
                frmMASUpdate.Label1.Caption = "Updating MAS 500 Record " & RSBOL.Bookmark & " of  " & RSBOL.RecordCount
                
                CurrentLine = 12
                'Get Loads Record
                LoadsTableKey = ConvertToDouble(RSBOL("LoadsTableKey").Value)
                
                CurrentLine = 13
                'Open the Parent Load Record
                If rsLoad.State = 1 Then
                    CurrentLine = 14
                    rsLoad.Close
                End If
                
                CurrentLine = 15
                SQL = "Select * From Loads Where NetworkIsDown = 1 And LoadsTableKey = " & LoadsTableKey
                
                CurrentLine = 16
                rsLoad.CursorLocation = adUseClient
                CurrentLine = 17
                rsLoad.CursorType = adOpenDynamic
                
                CurrentLine = 18
                rsLoad.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
                
                CurrentLine = 19
                Call ReProcessBOL
                
                '*****************************************************************************
                '*****************************************************************************
                ' Save Record and move to next record
                '*****************************************************************************
                '*****************************************************************************
                CurrentLine = 20
'                RSBOL.UpdateBatch
                If UpdateRS(RSBOL) = False Then
                    CurrentLine = 21
                    MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
                
                    CurrentLine = 22
                    ErrMsg = "There was an error saving the data.  If this persists contact your IT support."
                    CurrentLine = 23
                    If RSBOL.State = 1 Then
                        CurrentLine = 24
                        If RSBOL.EOF Or RSBOL.BOF Then
                            CurrentLine = 25
                            InsErrorLog 0, Date + Time, ErrMsg
                        Else
                            CurrentLine = 26
                            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
                        End If
                    Else
                        CurrentLine = 27
                        InsErrorLog 0, Date + Time, ErrMsg
                    End If
                End If
                
                CurrentLine = 28
                RSBOL.MoveNext
            Wend
        End If
    End If
    
    CurrentLine = 29
    'BOL was loaded Successfully
    UpdateMAS500 = True
    
    CurrentLine = 30
    
CleanUP:
    On Error Resume Next
    CurrentLine = 31
    If rsLoad.State = 1 Then rsLoad.Close
    CurrentLine = 32
    If RSBOL.State = 1 Then RSBOL.Close
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 33
    Exit Function
Error:
    ErrMsg = Me.Name & ".UpdateMAS500()" & vbCrLf & _
    "The following error occurred while updating MAS 500 with Scale Pass data: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
'    MsgBox Me.Name & ".UpdateMAS500()" & vbCrLf & _
    "The following error occurred while updating MAS 500 with Scale Pass data: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    MsgBox ErrMsg, vbCritical, "Error"
    
    If RSBOL.State = 1 Then
        If RSBOL.EOF Or RSBOL.BOF Then
            InsErrorLog 0, Date + Time, ErrMsg
        Else
            InsErrorLog ConvertToDouble(RSBOL("BOLTableKey").Value), Date + Time, ErrMsg
        End If
    Else
        InsErrorLog 0, Date + Time, ErrMsg
    End If
        
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function





Sub BindBOLObjects()
    On Error GoTo Error
    'If RSBOL.State <> 1 Then Exit Sub
    
    gbLastActivityTime = Date + Time
    
    Set txtTruck.DataSource = RSBOL
    Set txtHauler.DataSource = RSBOL
    Set txtCust.DataSource = RSBOL
    Set txtDestAddress.DataSource = RSBOL
    Set txtContract.DataSource = RSBOL
    Set txtBOLNbr.DataSource = RSBOL
    Set txtShipToState.DataSource = RSBOL
    Set txtDestination.DataSource = RSBOL
    Set txtRemarks.DataSource = RSBOL
    
    Set txtFrontGrossWt.DataSource = RSBOL
    Set txtRearGrossWt.DataSource = RSBOL
    Set txtTotalGrossWt.DataSource = RSBOL
    Set txtFrontTareWt.DataSource = RSBOL
    Set txtRearTareWt.DataSource = RSBOL
    Set txtTotalTareWt.DataSource = RSBOL
    Set txtFrontNetWt.DataSource = RSBOL
    Set txtRearNetWt.DataSource = RSBOL
    Set txtTotalNetWt.DataSource = RSBOL
    Set txtFrontBaseGals.DataSource = RSBOL
    Set txtRearBaseGals.DataSource = RSBOL
    
    Set txtFrontTemp.DataSource = RSBOL
    Set txtRearTemp.DataSource = RSBOL
    
    Set txtLbsPerGal.DataSource = RSBOL
    Set txtTotalGals.DataSource = RSBOL
    Set txtTotalBaseGals.DataSource = RSBOL
    Set txtFrontAdditive.DataSource = RSBOL
    Set txtRearAdditive.DataSource = RSBOL
    Set txtFrontAdditiveGals.DataSource = RSBOL
    Set txtRearAdditiveGals.DataSource = RSBOL
    Set txtTotalAdditiveGals.DataSource = RSBOL
    Set txtNetTons.DataSource = RSBOL
    Set txtLoadDt.DataSource = RSBOL
    Set txtDeliverDt.DataSource = RSBOL
    Set txtInDt.DataSource = RSBOL
    Set txtOutDt.DataSource = RSBOL
    
    Set txtProduct.DataSource = RSBOL
    Set txtAdditive.DataSource = RSBOL
    Set txtGrossGals.DataSource = RSBOL
    Set txtNetGals.DataSource = RSBOL
    Set txtFlashpoint.DataSource = RSBOL
    Set txtLbsPerGal2.DataSource = RSBOL
    Set txtSpecificGravity.DataSource = RSBOL
    Set txtLotNr.DataSource = RSBOL
    Set txtLoadTemp.DataSource = RSBOL
    Set txtViscocity.DataSource = RSBOL
    Set txtResidue.DataSource = RSBOL
    
    Set txtViscocityTemp.DataSource = RSBOL
    
    Set txtTrailer1.DataSource = RSBOL
    Set txtTrailer2.DataSource = RSBOL
    Set txtPO.DataSource = RSBOL
    Set txtCarrierID.DataSource = RSBOL
    Set txtCarrierName.DataSource = RSBOL
    
    Set chHighRisk.DataSource = RSBOL
    Set txtSecTapeNbr.DataSource = RSBOL
    
    Set txtBatchNbr.DataSource = RSBOL
    Set txtWtTktNo.DataSource = RSBOL
    
    Set txtDeliveryLoc.DataSource = RSBOL
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".BindBOLObjects()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Sub UnBindBOLObjects()
    On Error GoTo Error
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    Set txtTruck.DataSource = Nothing
    CurrentLine = 1
    Set txtHauler.DataSource = Nothing
    CurrentLine = 2
    Set txtCust.DataSource = Nothing
    CurrentLine = 3
    Set txtDestAddress.DataSource = Nothing
    CurrentLine = 4
    Set txtContract.DataSource = Nothing
    CurrentLine = 5
    Set txtBOLNbr.DataSource = Nothing
    CurrentLine = 6
    Set txtShipToState.DataSource = Nothing
    CurrentLine = 7
    Set txtDestination.DataSource = Nothing
    CurrentLine = 8
    Set txtRemarks.DataSource = Nothing
    CurrentLine = 9
    
    Set txtFrontGrossWt.DataSource = Nothing
    CurrentLine = 10
    Set txtRearGrossWt.DataSource = Nothing
    CurrentLine = 11
    Set txtTotalGrossWt.DataSource = Nothing
    CurrentLine = 12
    Set txtFrontTareWt.DataSource = Nothing
    CurrentLine = 13
    Set txtRearTareWt.DataSource = Nothing
    CurrentLine = 14
    Set txtTotalTareWt.DataSource = Nothing
    CurrentLine = 15
    Set txtFrontNetWt.DataSource = Nothing
    CurrentLine = 16
    Set txtRearNetWt.DataSource = Nothing
    CurrentLine = 17
    Set txtTotalNetWt.DataSource = Nothing
    CurrentLine = 18
    Set txtFrontBaseGals.DataSource = Nothing
    CurrentLine = 19
    Set txtRearBaseGals.DataSource = Nothing
    
    CurrentLine = 20
    Set txtFrontTemp.DataSource = Nothing
    CurrentLine = 21
    Set txtRearTemp.DataSource = Nothing
    
    CurrentLine = 22
    Set txtLbsPerGal.DataSource = Nothing
    CurrentLine = 23
    Set txtTotalGals.DataSource = Nothing
    CurrentLine = 24
    Set txtTotalBaseGals.DataSource = Nothing
    CurrentLine = 25
    Set txtFrontAdditive.DataSource = Nothing
    CurrentLine = 26
    Set txtRearAdditive.DataSource = Nothing
    CurrentLine = 27
    Set txtFrontAdditiveGals.DataSource = Nothing
    CurrentLine = 28
    Set txtRearAdditiveGals.DataSource = Nothing
    CurrentLine = 29
    Set txtTotalAdditiveGals.DataSource = Nothing
    CurrentLine = 30
    Set txtNetTons.DataSource = Nothing
    CurrentLine = 31
    Set txtLoadDt.DataSource = Nothing
    CurrentLine = 32
    Set txtDeliverDt.DataSource = Nothing
    CurrentLine = 33
    Set txtInDt.DataSource = Nothing
    CurrentLine = 34
    Set txtOutDt.DataSource = Nothing
    
    CurrentLine = 35
    Set txtProduct.DataSource = Nothing
    CurrentLine = 36
    Set txtAdditive.DataSource = Nothing
    CurrentLine = 37
    Set txtGrossGals.DataSource = Nothing
    CurrentLine = 38
    Set txtNetGals.DataSource = Nothing
    CurrentLine = 39
    Set txtFlashpoint.DataSource = Nothing
    CurrentLine = 40
    Set txtLbsPerGal2.DataSource = Nothing
    CurrentLine = 41
    Set txtSpecificGravity.DataSource = Nothing
    CurrentLine = 42
    Set txtLotNr.DataSource = Nothing
    CurrentLine = 43
    Set txtLoadTemp.DataSource = Nothing
    CurrentLine = 44
    Set txtViscocity.DataSource = Nothing
    CurrentLine = 45
    Set txtResidue.DataSource = Nothing
    
    CurrentLine = 46
    Set txtViscocityTemp.DataSource = Nothing
    
    CurrentLine = 47
    Set txtTrailer1.DataSource = Nothing
    CurrentLine = 48
    Set txtTrailer2.DataSource = Nothing
    CurrentLine = 49
    Set txtPO.DataSource = Nothing
    CurrentLine = 50
    Set txtCarrierID.DataSource = Nothing
    CurrentLine = 51
    Set txtCarrierName.DataSource = Nothing
    
    CurrentLine = 52
    Set chHighRisk.DataSource = Nothing
    CurrentLine = 53
    Set txtSecTapeNbr.DataSource = Nothing
    
    CurrentLine = 54
    Set txtBatchNbr.DataSource = Nothing
    CurrentLine = 55
    Set txtWtTktNo.DataSource = Nothing
    
    CurrentLine = 56
    Set txtDeliveryLoc.DataSource = Nothing
    
    CurrentLine = 57
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".UnBindBOLObjects()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If UpdateRS(RSBOL) = False Then
        MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
    End If
    
    'RKL DEJ 2016-09-07 START
    UnBindBOLObjects
    'RKL DEJ 2016-09-07 STOP
    
    
    If rsLoad.State = 1 Then rsLoad.Close
    Set rsLoad = Nothing
    
    If rsLoadExt.State = 1 Then rsLoadExt.Close
    Set rsLoadExt = Nothing
    
    If RSBOL.State = 1 Then RSBOL.Close
    Set RSBOL = Nothing
    
    If RSBOLExt.State = 1 Then RSBOLExt.Close
    Set RSBOLExt = Nothing
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Frame1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label10_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label11_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label12_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label13_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label14_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label15_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label16_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label17_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label18_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label19_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label20_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label21_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label22_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label23_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label24_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label25_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label26_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label27_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label28_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label29_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label30_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label31_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label32_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label33_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label34_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label35_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label36_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label37_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label38_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label39_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label40_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label41_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label42_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label43_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label6_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label8_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label9_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub lblSpecGravity_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub lblStatus_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub lblViscocity_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
                       
            Unload Me
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False

End Sub

Private Sub txtAdditive_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtBOLNbr_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtCarrierID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtContract_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtCust_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtDeliverDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtDeliveryLoc_Change()
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtDeliveryLoc_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time

End Sub


Private Sub txtDestAddress_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtDestination_GotFocus()
'    ActiveControl.SelStart = 0
'    ActiveControl.SelLength = Len(ActiveControl.Text)
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtDestination_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFlashpoint_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFlashpoint_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFlashpoint_Validate(Cancel As Boolean)
    
    gbLastActivityTime = Date + Time
    
    If IsNumeric(txtFlashpoint.Text) = False Then
        Cancel = True
        MsgBox "The Flashpoint must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtFrontAdditive_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontAdditiveGals_Change()
    txtTotalAdditiveGals.Text = Val(txtFrontAdditiveGals.Text) + Val(txtRearAdditiveGals.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontAdditiveGals_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontAdditiveGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontAdditiveGals_Validate(Cancel As Boolean)
    If IsNumeric(txtFrontAdditiveGals.Text) = False Then
        Cancel = True
        MsgBox "The Additive Gallons must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontBaseGals_Change()
    txtTotalBaseGals.Text = Val(txtFrontBaseGals.Text) + Val(txtRearBaseGals.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontBaseGals_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontBaseGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontBaseGals_Validate(Cancel As Boolean)
    If IsNumeric(txtFrontBaseGals.Text) = False Then
        Cancel = True
        MsgBox "The Base Gallons must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontNetWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTareWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTemp_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTemp_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTemp_Validate(Cancel As Boolean)
    If sBOLPass = "Front" Then
        txtLoadTemp.Text = txtFrontTemp.Text
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtGrossGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtHauler_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtInDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtLbsPerGal_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtLbsPerGal2_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtLbsPerGal2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtLbsPerGal2_Validate(Cancel As Boolean)
    If IsNumeric(txtLbsPerGal2.Text) = False Then
        Cancel = True
        MsgBox "The Lbs Per Gallon must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtLoadDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtLoadTemp_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtLoadTemp_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtLoadTemp_Validate(Cancel As Boolean)
    If IsNumeric(txtLoadTemp.Text) = False Then
        Cancel = True
        MsgBox "The Load Temp must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtLotNr_Change()
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtLotNr_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

   gbLastActivityTime = Date + Time
End Sub

Private Sub txtLotNr_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtNetGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtNetTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtOutDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtPO_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtProduct_Change()
'    Select Case txtProduct.Text
'           Case "CSS-1", "CSS-1H", "SC-H118"
'                lblViscocity.Caption = "@ 77�F"
'           Case Else
'                lblViscocity.Caption = "@ 122�F"
'    End Select

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtProduct_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearAdditive_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearAdditiveGals_Change()
    txtTotalAdditiveGals.Text = Val(txtFrontAdditiveGals.Text) + Val(txtRearAdditiveGals.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearAdditiveGals_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearAdditiveGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearAdditiveGals_Validate(Cancel As Boolean)
    If IsNumeric(txtRearAdditiveGals.Text) = False Then
        Cancel = True
        MsgBox "The Additive Gallons must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearBaseGals_Change()
    txtTotalBaseGals.Text = Val(txtFrontBaseGals.Text) + Val(txtRearBaseGals.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearBaseGals_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearBaseGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearBaseGals_Validate(Cancel As Boolean)
    If IsNumeric(txtRearBaseGals.Text) = False Then
        Cancel = True
        MsgBox "The Base Gallons must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearNetWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTareWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTemp_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTemp_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTemp_Validate(Cancel As Boolean)
    If sBOLPass = "Rear" Then
        txtLoadTemp.Text = txtRearTemp.Text
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtRemarks_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRemarks_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtResidue_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtResidue_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtResidue_Validate(Cancel As Boolean)
    If IsNumeric(txtResidue.Text) = False Then
        Cancel = True
        MsgBox "The Residue must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtSpecificGravity_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtSpecificGravity_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtSpecificGravity_Validate(Cancel As Boolean)
    If IsNumeric(txtSpecificGravity.Text) = False Then
        Cancel = True
        MsgBox "The Specific Gravity must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtTotalAdditiveGals_Change()
    txtTotalGals.Text = Val(txtTotalBaseGals.Text) + Val(txtTotalAdditiveGals.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtTotalAdditiveGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalBaseGals_Change()
    txtTotalGals.Text = Val(txtTotalBaseGals.Text) + Val(txtTotalAdditiveGals.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtTotalBaseGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalGals_Change()
'    On Error Resume Next
    If ConvertToDouble(txtTotalNetWt.Text) = 0 Then
        txtLbsPerGal.Text = 0
    Else
        If ConvertToDouble(txtTotalGals.Text) = 0 Then
            txtLbsPerGal.Text = 0
        Else
            txtLbsPerGal.Text = ConvertToDouble(txtTotalNetWt.Text) / ConvertToDouble(txtTotalGals.Text)
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtTotalGals_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalNetWt_Change()
'    txtNetTons.Text = txtTotalNetWt.Text / 2000

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtTotalNetWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalTareWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTrailer1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTrailer2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTruck_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtViscocity_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtViscocity_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtViscocity_Validate(Cancel As Boolean)
    If IsNumeric(txtViscocity.Text) = False Then
        Cancel = True
        MsgBox "The Viscocity must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtViscocityTemp_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtViscocityTemp_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtViscocityTemp_Validate(Cancel As Boolean)
    If IsNumeric(txtViscocity.Text) = False Then
        Cancel = True
        MsgBox "The Viscocity Temperature must be a valid number.", vbInformation, "Invalid Data"
        SendKeys "{HOME}"
        SendKeys "^+{END}"
    End If

    gbLastActivityTime = Date + Time
End Sub


'SGS DEJ 7/6/10 Created for debugging
Private Function GetPrintValuesStr(PrintValues As BOLFields) As String
    On Error GoTo Error
    Dim AllValues As String
    Dim lErrMsg As String
    Dim CurrentLine As Integer
    Dim dDate As Date
    
    gbLastActivityTime = Date + Time
    
    dDate = Date + Time
    
    CurrentLine = 0
    
    AllValues = "PrintValues As BOLFields: " & vbCrLf
    
    CurrentLine = 1
    With PrintValues
        CurrentLine = 2
        AllValues = AllValues & .FromCompany & vbCrLf
        CurrentLine = 3
        AllValues = AllValues & .ToCompany & vbCrLf
        CurrentLine = 4
        AllValues = AllValues & .FromWhse & vbCrLf
        CurrentLine = 5
        AllValues = AllValues & .ToWhse & vbCrLf
        CurrentLine = 6
        AllValues = AllValues & .CommodityID & vbCrLf
        CurrentLine = 7
        AllValues = AllValues & .IsBtwnCmpny & vbCrLf
        CurrentLine = 8
        AllValues = AllValues & .IsTransfer & vbCrLf
        CurrentLine = 9
        AllValues = AllValues & .IsCreditReturn & vbCrLf
        CurrentLine = 10
        AllValues = AllValues & .CreditReturnComment & vbCrLf
        CurrentLine = 11
        AllValues = AllValues & .IsSplitLoad & vbCrLf
        CurrentLine = 12
        AllValues = AllValues & .SplitLoadType & vbCrLf
        CurrentLine = 13
        AllValues = AllValues & .IsHandKeyed & vbCrLf
        CurrentLine = 14
        AllValues = AllValues & .IsScaleEnabled & vbCrLf
        CurrentLine = 15
        AllValues = AllValues & .ScaleWeightMsg & vbCrLf
        CurrentLine = 16
        AllValues = AllValues & .PlacardType & vbCrLf
        CurrentLine = 17
        AllValues = AllValues & .UserID & vbCrLf
        CurrentLine = 18
        AllValues = AllValues & .LoadID & vbCrLf
        CurrentLine = 19
        AllValues = AllValues & .PreBillID & vbCrLf
        CurrentLine = 20
        AllValues = AllValues & .CustomerName & vbCrLf
        CurrentLine = 21
        AllValues = AllValues & .ScaleDate & vbCrLf
        CurrentLine = 22
        AllValues = AllValues & .Facility & vbCrLf
        CurrentLine = 23
        AllValues = AllValues & .FacilityName & vbCrLf
        CurrentLine = 24
        AllValues = AllValues & .InBoundOperator & vbCrLf
        CurrentLine = 25
        AllValues = AllValues & .OutBoundOperator & vbCrLf
        CurrentLine = 26
        AllValues = AllValues & .LadingSibling & vbCrLf
        CurrentLine = 27
        AllValues = AllValues & .TankNumber & vbCrLf
        CurrentLine = 28
        AllValues = AllValues & .RackNbr & vbCrLf
        CurrentLine = 29
        AllValues = AllValues & .Tons & vbCrLf
        CurrentLine = 30
        AllValues = AllValues & .BOLNumber & vbCrLf
        CurrentLine = 31
        AllValues = AllValues & .RtnOrigBOLNumber & vbCrLf
        CurrentLine = 32
        AllValues = AllValues & .Customer & vbCrLf
        CurrentLine = 33
        AllValues = AllValues & .BOLDate & vbCrLf
        CurrentLine = 34
        AllValues = AllValues & .ContractNo & vbCrLf
        CurrentLine = 35
        AllValues = AllValues & .PONo & vbCrLf
        CurrentLine = 36
        AllValues = AllValues & .Origin & vbCrLf
        CurrentLine = 37
        AllValues = AllValues & .PAGroupNo & vbCrLf
        CurrentLine = 38
        AllValues = AllValues & .Destination & vbCrLf
        CurrentLine = 39
        AllValues = AllValues & .DestDirections & vbCrLf
        CurrentLine = 40
        AllValues = AllValues & .ProjectNo & vbCrLf
        CurrentLine = 41
        AllValues = AllValues & .ProjectDescription & vbCrLf
        CurrentLine = 42
        AllValues = AllValues & .Carrier & vbCrLf
        CurrentLine = 43
        AllValues = AllValues & .FreightBillNo & vbCrLf
        CurrentLine = 44
        AllValues = AllValues & .WeightTicketNo & vbCrLf
        CurrentLine = 45
        AllValues = AllValues & .TruckNo & vbCrLf
        CurrentLine = 46
        AllValues = AllValues & .Trailer1No & vbCrLf
        CurrentLine = 47
        AllValues = AllValues & .Trailer2No & vbCrLf
        CurrentLine = 48
        AllValues = AllValues & .LoadTime & vbCrLf
        CurrentLine = 49
        AllValues = AllValues & .DeliveryTime & vbCrLf
        CurrentLine = 50
        AllValues = AllValues & .LoadTimeIn & vbCrLf
        CurrentLine = 51
        AllValues = AllValues & .LoadTimeOut & vbCrLf
        CurrentLine = 52
        AllValues = AllValues & .TimeArrived & vbCrLf
        CurrentLine = 53
        AllValues = AllValues & .StartUnload & vbCrLf
        CurrentLine = 54
        AllValues = AllValues & .FinishUnload & vbCrLf
        CurrentLine = 55
        AllValues = AllValues & .Remarks & vbCrLf
        CurrentLine = 56
        AllValues = AllValues & .Penetration & vbCrLf
        CurrentLine = 57
        AllValues = AllValues & .PenetrationTemp & vbCrLf
        CurrentLine = 58
        AllValues = AllValues & .PenetrationRemark & vbCrLf
        CurrentLine = 59
        AllValues = AllValues & .Flashpoint & vbCrLf
        CurrentLine = 60
        AllValues = AllValues & .LbsPerGal & vbCrLf
        CurrentLine = 61
        AllValues = AllValues & .SpecGravity & vbCrLf
        CurrentLine = 62
        AllValues = AllValues & .Viscosity & vbCrLf
        CurrentLine = 63
        AllValues = AllValues & .ViscosityTemp & vbCrLf
        CurrentLine = 64
        AllValues = AllValues & .ViscosityRemark & vbCrLf
        CurrentLine = 65
        AllValues = AllValues & .LotNumber & vbCrLf
        CurrentLine = 66
        AllValues = AllValues & .ProductSpecRemarks & vbCrLf
        CurrentLine = 67
        AllValues = AllValues & .Residue & vbCrLf
        CurrentLine = 68
        AllValues = AllValues & .CertOfCompliance & vbCrLf
        CurrentLine = 69
        AllValues = AllValues & .AdditivePercent & vbCrLf
        CurrentLine = 70
        AllValues = AllValues & .Temp & vbCrLf
        CurrentLine = 71
        AllValues = AllValues & .GrossGal & vbCrLf
        CurrentLine = 72
        AllValues = AllValues & .NetGal & vbCrLf
        CurrentLine = 73
        AllValues = AllValues & .GrossWtLbs1 & vbCrLf
        CurrentLine = 74
        AllValues = AllValues & .TareWtLbs1 & vbCrLf
        CurrentLine = 75
        AllValues = AllValues & .NetWtLbs1 & vbCrLf
        CurrentLine = 76
        AllValues = AllValues & .AdditiveID & vbCrLf
        CurrentLine = 77
        AllValues = AllValues & .AdditiveDesc & vbCrLf
        CurrentLine = 78
        AllValues = AllValues & .ItemSpec & vbCrLf
        CurrentLine = 79
        AllValues = AllValues & .ProductName & vbCrLf
        CurrentLine = 80
        AllValues = AllValues & .GrossWtLbs2 & vbCrLf
        CurrentLine = 81
        AllValues = AllValues & .TareWtLbs2 & vbCrLf
        CurrentLine = 82
        AllValues = AllValues & .NetWtLbs2 & vbCrLf
        CurrentLine = 83
        AllValues = AllValues & .GrossWtLbs3 & vbCrLf
        CurrentLine = 84
        AllValues = AllValues & .TareWtLbs3 & vbCrLf
        CurrentLine = 85
        AllValues = AllValues & .NetWtLbs3 & vbCrLf
        CurrentLine = 86
        AllValues = AllValues & .Quantity & vbCrLf
        CurrentLine = 87
        AllValues = AllValues & .HM & vbCrLf
        CurrentLine = 88
        AllValues = AllValues & .ProductID & vbCrLf
        CurrentLine = 89
        AllValues = AllValues & .ProductDesc & vbCrLf
        CurrentLine = 90
        AllValues = AllValues & .NetTons & vbCrLf
        CurrentLine = 91
    End With
    
    CurrentLine = 92
    GetPrintValuesStr = AllValues
    
    CurrentLine = 93
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = Me.Name & ".GetPrintValuesStr(PrintValues As BOLFields) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    InsErrorLog 0, dDate, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function


Sub ShowHideRearFields()
    On Error GoTo Error
    
    Dim bVisible As Boolean
    
    If gbNotAllowSplitLoads = True Then
        bVisible = False
    Else
        bVisible = True
    End If
    
    Label41.Visible = bVisible
    txtTrailer2.Visible = bVisible
    Label15.Visible = bVisible
    txtRearGrossWt.Visible = bVisible
    txtRearTareWt.Visible = bVisible
    txtRearNetWt.Visible = bVisible
    txtRearBaseGals.Visible = bVisible
    txtRearTemp.Visible = bVisible
    txtRearAdditive.Visible = bVisible
    txtRearAdditiveGals.Visible = bVisible
    
    Exit Sub
Error:
    MsgBox Me.Name & ".ShowHideRearFields()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
End Sub


'RKL DEJ 2018-01-05 added GetLMEContractData
Public Function GetLMEContractData(Customer_ID As String, Contract_no As String) As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim RtnVal As String
    
    If IsObject(gbMASConn) <> True Then
        GetLMEContractData = False
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        GetLMEContractData = False
        Exit Function
    End If
    
    SQL = "Select * From vluLME_iasx_contracts_RKL Where Contract_no = '" & Replace(Contract_no, "'", "''") & "' And Customer_ID = '" & Replace(Customer_ID, "'", "''") & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        
    Else
        'Records found
        RS.MoveFirst
        
        RSBOL("ShipToState").Value = ConvertToString(RS("Job_State").Value)
        RSBOL("MASProjectDescription").Value = ConvertToString(RS("descr").Value)
        RSBOL("MASProjectNumber").Value = ConvertToString(RS("project_number").Value)
        RSBOL("MASDeliveryLoc").Value = ConvertToString(RS("delivery_location").Value)
    End If
    
    GetLMEContractData = True
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    MsgBox "basMethods.GetLMEContractData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    GetLMEContractData = False
    
    GoTo CleanUP
End Function



