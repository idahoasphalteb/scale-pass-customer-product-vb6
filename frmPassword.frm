VERSION 5.00
Begin VB.Form frmPassword 
   BackColor       =   &H00FF00FF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Password - Valero"
   ClientHeight    =   825
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   4890
   Icon            =   "frmPassword.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   825
   ScaleWidth      =   4890
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdClear 
      Caption         =   "Clear Password"
      Height          =   255
      Left            =   2040
      TabIndex        =   4
      ToolTipText     =   "Remove the password... Make the password blank."
      Top             =   120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox txtPWD 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   120
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   360
      Width           =   3255
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancel"
      Height          =   255
      Left            =   3600
      TabIndex        =   3
      Top             =   480
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   255
      Left            =   3600
      TabIndex        =   2
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "Security Password"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1815
   End
End
Attribute VB_Name = "frmPassword"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private lcPWD As String
Private lcSuccess As Boolean
Private lcSetup As Boolean
Private lcSetupPWD1 As String
Private lcPWDAttempts As Integer

Public Property Let Password(PWD As String)
    lcPWD = PWD

    gbLastActivityTime = Date + Time
End Property

Public Property Get Password() As String
    Password = lcPWD

    gbLastActivityTime = Date + Time
End Property

Public Property Get Success() As Boolean
    Success = lcSuccess

    gbLastActivityTime = Date + Time
End Property

Public Property Let Setup(Val As Boolean)
    lcSetup = Val

    gbLastActivityTime = Date + Time
End Property

Public Property Get Setup() As Boolean
    lcSetupPWD1 = Empty
    txtPWD.Text = Empty
    Setup = lcSetup

    gbLastActivityTime = Date + Time
End Property


Private Sub CancelButton_Click()
    lcSuccess = False
    Me.Hide

    gbLastActivityTime = Date + Time
End Sub

Private Sub CancelButton_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub CmdClear_Click()
    If txtPWD.Text = Empty Then
        If MsgBox("Are you sure you want to make the password blank?", vbYesNo, "Clear Password?") = vbYes Then
            lcSetupPWD1 = Empty
            lcPWD = Empty
            lcSuccess = True
            txtPWD.Text = Empty
            lcSuccess = True
            Me.Hide
        End If
    Else
        MsgBox "To clear the password there cannot be a value in the password field.", vbInformation, "Clear Failed"
    End If
End Sub

Private Sub Form_Activate()
    If Setup = True And SetupPWD <> Empty Then
        CmdClear.Visible = True
    Else
        CmdClear.Visible = False
    End If

End Sub

Private Sub Form_Load()
    lcSetupPWD1 = Empty
    txtPWD.Text = Empty
    

    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub OKButton_Click()
    gbLastActivityTime = Date + Time
    
    If lcSetup = True Then
        If lcSetupPWD1 = Empty Then
            If txtPWD.Text = Empty Then
                MsgBox "You need to enter a password first.", vbOKOnly, "No Password Entered"
            Else
                lcSetupPWD1 = txtPWD.Text
                txtPWD.Text = Empty
                MsgBox "Please enter the password again for verification.", vbOKOnly, "Verify Password"
            End If
        Else
            If txtPWD.Text <> lcSetupPWD1 Then
                lcSetupPWD1 = Empty
                txtPWD.Text = Empty
                MsgBox "Start over, the passwords did not match.  Please enter the password again.", vbOKOnly, "Re-Enter Password"
            Else
                lcPWD = lcSetupPWD1
                lcSuccess = True
                txtPWD.Text = Empty
                Me.Hide
            End If
        End If
    Else
        If txtPWD.Text = lcPWD Then
            lcSuccess = True
            txtPWD.Text = Empty
            Me.Hide
        Else
            lcPWDAttempts = lcPWDAttempts + 1
            MsgBox "The password is not correct.", vbOKOnly, "Not Correct"
            
            If lcPWDAttempts > 5 Then
                lcPWDAttempts = 0
                
                If MsgBox("Do you want to reset the password?", vbYesNo, "Reset Password?") = vbYes Then
                    SetupPWD = Empty
                    SaveSetting App.Title, "SetupPasswords", "SetupPWD", Encrypt(SetupPWD)
                    Me.Hide
                End If
            End If
                
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub OKButton_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtPWD_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub
