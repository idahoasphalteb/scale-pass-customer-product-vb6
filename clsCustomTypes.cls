VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCustomTypes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Type LabelData
    Product As String
    ScaleInDate As String
    BLNbr As String
    Tank As String
    TonsShipped As String
    Customer As String
    ProjectNbr As String
    TruckNbr As String
    Company As String
    Plant As String
    ProductClass As String
End Type

