VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRePrintLBS 
   BackColor       =   &H00FF00FF&
   Caption         =   "Reprint LBS - Valero"
   ClientHeight    =   7095
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9600
   Icon            =   "frmRePrintLBS.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   9600
   StartUpPosition =   3  'Windows Default
   Begin VB.CheckBox chDateLess 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   22
      Top             =   2400
      Width           =   1455
   End
   Begin VB.CheckBox chDate 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   19
      Top             =   2040
      Width           =   1455
   End
   Begin VB.CheckBox chSplit 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   16
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Frame frmSplitLoad 
      Height          =   615
      Left            =   1680
      TabIndex        =   13
      Top             =   1440
      Width           =   1935
      Begin VB.OptionButton OptnSplitFalse 
         Caption         =   "False"
         Height          =   255
         Left            =   960
         TabIndex        =   15
         Top             =   240
         Width           =   735
      End
      Begin VB.OptionButton OptnSplitTrue 
         Caption         =   "True"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.ComboBox cmboPrinter 
      Height          =   315
      Left            =   6960
      TabIndex        =   28
      Text            =   "Combo1"
      Top             =   840
      Width           =   2295
   End
   Begin VB.TextBox txtCopyCount 
      Alignment       =   2  'Center
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "0"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   1033
         SubFormatType   =   1
      EndProperty
      Height          =   285
      Left            =   6960
      TabIndex        =   27
      Text            =   "1"
      Top             =   360
      Width           =   1335
   End
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Print LBS"
      Height          =   375
      Left            =   6960
      TabIndex        =   24
      Top             =   1440
      Width           =   1455
   End
   Begin VB.CheckBox chTruck 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   11
      Top             =   1200
      Width           =   1455
   End
   Begin VB.TextBox txtTruck 
      Height          =   285
      Left            =   1680
      TabIndex        =   10
      Top             =   1200
      Width           =   1935
   End
   Begin VB.CheckBox chCustomer 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   8
      Top             =   840
      Width           =   1455
   End
   Begin VB.TextBox txtCustomer 
      Height          =   285
      Left            =   1680
      TabIndex        =   7
      Top             =   840
      Width           =   1935
   End
   Begin VB.CheckBox chContract 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   5
      Top             =   480
      Width           =   1455
   End
   Begin VB.TextBox txtContract 
      Height          =   285
      Left            =   1680
      TabIndex        =   4
      Top             =   480
      Width           =   1935
   End
   Begin VB.CheckBox chBOL 
      Caption         =   "Include in filter"
      Height          =   255
      Left            =   3840
      TabIndex        =   2
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox txtBOL 
      Height          =   285
      Left            =   1680
      TabIndex        =   1
      Top             =   120
      Width           =   1935
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh Data"
      Height          =   375
      Left            =   0
      TabIndex        =   23
      Top             =   2880
      Width           =   1575
   End
   Begin MSDataGridLib.DataGrid grdBOLRecs 
      Align           =   2  'Align Bottom
      Height          =   3735
      Left            =   0
      TabIndex        =   25
      Top             =   3360
      Width           =   9600
      _ExtentX        =   16933
      _ExtentY        =   6588
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "BOL Records"
      ColumnCount     =   16
      BeginProperty Column00 
         DataField       =   "LoadsTableKey"
         Caption         =   "LoadsTableKey"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "SplitLoad"
         Caption         =   "Split Load"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   5
            Format          =   ""
            HaveTrueFalseNull=   1
            TrueValue       =   "True"
            FalseValue      =   "False"
            NullValue       =   ""
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   7
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "TimeIn"
         Caption         =   "Scale In"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column03 
         DataField       =   "TimeOut"
         Caption         =   "Scale Out"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column04 
         DataField       =   "FrontBOL"
         Caption         =   "Front BOL"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column05 
         DataField       =   "FrontCustomer"
         Caption         =   "Front Customer"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column06 
         DataField       =   "FrontProject"
         Caption         =   "Front Project"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column07 
         DataField       =   "TruckNo"
         Caption         =   "Truck No"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column08 
         DataField       =   "FrontContractNbr"
         Caption         =   "Front Contract Nbr"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column09 
         DataField       =   "FrontTankNbr"
         Caption         =   "Front Tank Nbr"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column10 
         DataField       =   "RearBOL"
         Caption         =   "Rear BOL"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column11 
         DataField       =   "RearCustomer"
         Caption         =   "Rear Customer"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column12 
         DataField       =   "RearContractNbr"
         Caption         =   "Rear Contract Nbr"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column13 
         DataField       =   "RearProject"
         Caption         =   "Rear Project"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column14 
         DataField       =   "RearTankNbr"
         Caption         =   "Rear Tank Nbr"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column15 
         DataField       =   "Carrier"
         Caption         =   "Carrier"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            Object.Visible         =   0   'False
         EndProperty
         BeginProperty Column01 
         EndProperty
         BeginProperty Column02 
            ColumnWidth     =   1725.165
         EndProperty
         BeginProperty Column03 
            ColumnWidth     =   1709.858
         EndProperty
         BeginProperty Column04 
         EndProperty
         BeginProperty Column05 
            ColumnWidth     =   1995.024
         EndProperty
         BeginProperty Column06 
         EndProperty
         BeginProperty Column07 
         EndProperty
         BeginProperty Column08 
         EndProperty
         BeginProperty Column09 
         EndProperty
         BeginProperty Column10 
         EndProperty
         BeginProperty Column11 
         EndProperty
         BeginProperty Column12 
         EndProperty
         BeginProperty Column13 
         EndProperty
         BeginProperty Column14 
         EndProperty
         BeginProperty Column15 
         EndProperty
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtScaleOut 
      Height          =   285
      Left            =   1680
      TabIndex        =   18
      Top             =   2040
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   503
      _Version        =   393216
      Format          =   171638785
      CurrentDate     =   42060
   End
   Begin MSComCtl2.DTPicker dtScaleOutLess 
      Height          =   285
      Left            =   1680
      TabIndex        =   21
      Top             =   2400
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   503
      _Version        =   393216
      Format          =   171638785
      CurrentDate     =   42060
   End
   Begin VB.Label Label3 
      Caption         =   "Scale Out Date >="
      Height          =   375
      Left            =   0
      TabIndex        =   17
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Scale Out Date <="
      Height          =   375
      Left            =   0
      TabIndex        =   20
      Top             =   2400
      Width           =   1455
   End
   Begin VB.Label lblSplit 
      Caption         =   "Split Load:"
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Line Line1 
      X1              =   5520
      X2              =   5520
      Y1              =   120
      Y2              =   3120
   End
   Begin VB.Label Label1 
      Caption         =   "Printer:"
      Height          =   255
      Left            =   6000
      TabIndex        =   29
      Top             =   840
      Width           =   975
   End
   Begin VB.Label lblCopyCount 
      Caption         =   "# of Copies"
      Height          =   255
      Left            =   6000
      TabIndex        =   26
      Top             =   360
      Width           =   855
   End
   Begin VB.Label lblTruck 
      Caption         =   "Truck No Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label lblCustomer 
      Caption         =   "Customer Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   6
      Top             =   840
      Width           =   1455
   End
   Begin VB.Label lblContract 
      Caption         =   "Contract Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   3
      Top             =   480
      Width           =   1455
   End
   Begin VB.Label lblBOL 
      Caption         =   "BOL Nbr Contains: "
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "frmRePrintLBS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim SQL As String
Dim RS As New ADODB.Recordset

Private Sub cmdPrint_Click()
    On Error GoTo Error
    gbLastActivityTime = Date + Time
    
    Dim lcNbrOfCopies As Integer
    Dim BOLTableKey As Long
    Dim LoadsTableKey As Long
    
    If RS.EOF Or RS.BOF Then
        MsgBox "You need to select a BOL Nbr below first.", vbInformation, "Missing BOL Record"
        Exit Sub
    End If
    
'    BOLTableKey = RS("BOLTableKey").Value
    LoadsTableKey = RS("LoadsTableKey").Value
    
    If IsNumeric(txtCopyCount.Text) = True Then
        lcNbrOfCopies = CInt(txtCopyCount.Text)
    Else
        lcNbrOfCopies = 1
    End If
    
    If lcNbrOfCopies < 1 Then
        MsgBox "The number of copies must be greater than 0.", vbInformation, "Wrong Number of Copies"
        Exit Sub
    End If
    
    If basMethods.PrintLBS(LoadsTableKey, True, lcNbrOfCopies, cmboPrinter.Text) = False Then
        'There was an error printing.
        MsgBox "The print LBS function returned failure.", vbInformation, "Scale Pass"
        
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdPrint_Click()" & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub


Private Sub cmdRefresh_Click()
    gbLastActivityTime = Date + Time
    Call GetData
    gbLastActivityTime = Date + Time

End Sub


Private Sub Form_Load()
    
    gbLastActivityTime = Date + Time
    
    'Default filter by date
    dtScaleOut.Value = DateAdd("d", -7, Date)
    chDate.Value = vbChecked
        
    Call GetData
    Call PopulateCombos
    gbLastActivityTime = Date + Time
    
    
End Sub


Sub GetData()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
'    SQL = "Select top 100 BOLTableKey, BOLNbr, ContractNbr, Customer, Project, TruckNo, ProductID, ProductDesc, ScaleInTime, ScaleOutTime, LotNbr "
    SQL = "Select * "
    SQL = SQL & "From Loads "
    SQL = SQL & "Where 1=1 "
    
    'Add Filter Cryterial
    If chBOL.Value = vbChecked And Trim(txtBOL.Text) <> Empty Then
        SQL = SQL & "And (FrontBOL like '%" & txtBOL.Text & "%' or RearBOL like '%" & txtBOL.Text & "%') "
    End If
        
    If chContract.Value = vbChecked And Trim(txtContract.Text) <> Empty Then
        SQL = SQL & "And (FrontContractNbr like '%" & txtContract.Text & "%' or RearContractNbr like '%" & txtContract.Text & "%') "
    End If
        
    If chCustomer.Value = vbChecked And Trim(txtCustomer.Text) <> Empty Then
        SQL = SQL & "And (FrontCustomer like '%" & txtCustomer.Text & "%' or RearCustomer like '%" & txtCustomer.Text & "%') "
    End If
        
    If chTruck.Value = vbChecked And Trim(txtTruck.Text) <> Empty Then
        SQL = SQL & "And TruckNo like '%" & txtTruck.Text & "%' "
    End If
    
    If chSplit.Value = vbChecked And (OptnSplitTrue.Value = True Or OptnSplitFalse.Value = True) Then
        If OptnSplitTrue.Value = True Then
            SQL = SQL & "And SplitLoad = 1 "
        Else
            SQL = SQL & "And SplitLoad = 0 "
        End If
    End If
    
    If chDate.Value = vbChecked And IsDate(dtScaleOut.Value) = True Then
        SQL = SQL & "And TimeOut >= #" & dtScaleOut.Value & "# "
    End If
    
    If chDateLess.Value = vbChecked And IsDate(dtScaleOutLess.Value) = True Then
        SQL = SQL & "And TimeOut <= #" & dtScaleOutLess.Value & "# "
    End If

    SQL = SQL & "Order By TimeOut Desc, TimeIn Desc "
    
    Set grdBOLRecs.DataSource = Nothing
    
    If RS.State = 1 Then
        RS.Close
    End If
    
    RS.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    Set grdBOLRecs.DataSource = RS
    
    grdBOLRecs.Caption = "BOL Records (" & RS.RecordCount & ")"
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".GetData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub


Private Sub grdBOLRecs_HeadClick(ByVal ColIndex As Integer)
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Dim SortStr As String
    Dim SortDirection As String
    
    
    SortStr = UCase(Trim(RS.Sort))
    
    If SortStr = Empty Then
        SortDirection = "Desc"
    Else
        If InStr(1, SortStr, " DESC", vbTextCompare) > 0 Then
            SortDirection = "Asc"
        Else
            SortDirection = "Desc"
        End If
    End If
    
    SortStr = "[" & grdBOLRecs.Columns(ColIndex).DataField & "] " & SortDirection
    
    RS.Sort = SortStr
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    Err.Clear
    gbLastActivityTime = Date + Time
    
End Sub

Private Sub grdBOLRecs_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub


Private Sub txtCopyCount_Validate(Cancel As Boolean)
    If IsNumeric(txtCopyCount.Text) = True Then
        txtCopyCount.Text = CInt(txtCopyCount.Text)
    Else
        txtCopyCount.Text = 1
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Sub PopulateCombos()
    On Error GoTo Error
    
    Dim Prntr As Printer
    
    gbLastActivityTime = Date + Time
        
    cmboPrinter.Clear
    
    For Each Prntr In Printers
        cmboPrinter.AddItem Prntr.DeviceName
    Next
    
    cmboPrinter.Text = gbSCALEPASSPRINTER
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".PopulateCombos()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


