VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmOperators 
   BackColor       =   &H00FF00FF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Maintian Operators - Valero"
   ClientHeight    =   4275
   ClientLeft      =   30
   ClientTop       =   480
   ClientWidth     =   6495
   Icon            =   "frmOperators.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   6495
   Begin VB.Timer tmrAutoReturnNoActivity 
      Interval        =   10000
      Left            =   0
      Top             =   0
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   255
      Left            =   5040
      TabIndex        =   9
      Top             =   960
      Width           =   975
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Save and &Exit"
      Height          =   255
      Left            =   3600
      TabIndex        =   8
      Top             =   960
      Width           =   1335
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Default         =   -1  'True
      Height          =   255
      Left            =   2520
      TabIndex        =   7
      Top             =   960
      Width           =   975
   End
   Begin VB.CommandButton cmdDelete 
      Caption         =   "&Delete"
      Height          =   255
      Left            =   1440
      TabIndex        =   6
      Top             =   960
      Width           =   975
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "&Add New"
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   960
      Width           =   975
   End
   Begin VB.CheckBox chActive 
      Alignment       =   1  'Right Justify
      Caption         =   "&Is Active"
      DataField       =   "ActiveFg"
      Height          =   255
      Left            =   3240
      TabIndex        =   2
      Top             =   120
      Width           =   975
   End
   Begin VB.TextBox txtName 
      DataField       =   "OperatorName"
      Height          =   285
      Left            =   1320
      MaxLength       =   30
      TabIndex        =   4
      Text            =   "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWX"
      Top             =   480
      Width           =   5055
   End
   Begin VB.TextBox txtID 
      DataField       =   "OperatorID"
      Height          =   285
      Left            =   1320
      MaxLength       =   3
      TabIndex        =   1
      Top             =   120
      Width           =   735
   End
   Begin MSDataGridLib.DataGrid DataGrid1 
      Align           =   2  'Align Bottom
      Height          =   2895
      Left            =   0
      TabIndex        =   10
      Top             =   1380
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   5106
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   -1  'True
      HeadLines       =   1
      RowHeight       =   15
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   3
      BeginProperty Column00 
         DataField       =   "OperatorID"
         Caption         =   "Operator ID"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "OperatorName"
         Caption         =   "Operator Name"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "ActiveFg"
         Caption         =   "Is Active"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   5
            Format          =   ""
            HaveTrueFalseNull=   1
            TrueValue       =   "True"
            FalseValue      =   "False"
            NullValue       =   ""
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   7
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            ColumnWidth     =   1005.165
         EndProperty
         BeginProperty Column01 
            ColumnWidth     =   4094.929
         EndProperty
         BeginProperty Column02 
            ColumnWidth     =   1005.165
         EndProperty
      EndProperty
   End
   Begin VB.Label Label2 
      Caption         =   "Operator &Name:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "&Operator ID:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "frmOperators"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const SQL = "Select * From Operator where OperatorID <> '___'"
Private RS As New ADODB.Recordset


Private Sub chActive_Click()
    
    If RS.EOF Or RS.BOF Then Exit Sub
    
    If chActive.Value = vbChecked Then
        RS("ActiveFg").Value = True
    Else
        RS("ActiveFg").Value = False
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chActive_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdAdd_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If RS.State = 1 Then
        RS.AddNew
        chActive.Value = vbChecked
        RS("ActiveFg").Value = True
        txtID.SetFocus
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdAdd_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdAdd_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdCancel_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If RS.State = 1 Then
        If Not RS.EOF And Not RS.BOF Then
            RS.Delete
            If RS.RecordCount > 0 Then
                RS.MoveFirst
            End If
        End If
        RS.Close
    End If
    
    Unload Me
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdAdd_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdCancel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdDelete_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If RS.State = 1 Then
        If Not RS.EOF And Not RS.BOF Then
            RS.Delete
            If RS.RecordCount > 0 Then
                RS.MoveFirst
            End If
        End If
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdAdd_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdDelete_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdExit_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If RS.State = 1 Then
        RS.UpdateBatch
    End If
    
    Unload Me
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdAdd_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdExit_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdSave_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If RS.State = 1 Then
        RS.UpdateBatch
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdAdd_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdSave_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub DataGrid1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Load()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If gbScaleConn.State <> 1 Then Exit Sub
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenDynamic
    
    RS.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    Set DataGrid1.DataSource = RS
    
    Set txtID.DataSource = RS
    Set txtName.DataSource = RS
    Set chActive.DataSource = RS
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".From_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Resize()
    gbLastActivityTime = Date + Time
'    DataGrid1.Width = Me.Width
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    Set DataGrid1.DataSource = Nothing
    
    If RS.State = 1 Then
        RS.UpdateBatch
        RS.Close
    End If
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
End Sub


Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
                        
            Call cmdExit_Click
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False

End Sub

Private Sub txtID_GotFocus()
    SendKeys "{HOME}"
    SendKeys "^+{END}"

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtName_GotFocus()
    SendKeys "{HOME}"
    SendKeys "^+{END}"

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtName_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub
