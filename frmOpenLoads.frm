VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmOpenLoads 
   BackColor       =   &H00FF00FF&
   Caption         =   "Open Loads - Valero"
   ClientHeight    =   4575
   ClientLeft      =   2385
   ClientTop       =   5115
   ClientWidth     =   15255
   Icon            =   "frmOpenLoads.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4575
   ScaleWidth      =   15255
   Begin VB.CommandButton cmdShowLMEMsg 
      Caption         =   "Get LME Message"
      Height          =   330
      Left            =   4320
      TabIndex        =   9
      ToolTipText     =   "Show the LME message for the selected Load."
      Top             =   360
      Width           =   1815
   End
   Begin VB.Timer tmrMASConnection 
      Interval        =   60000
      Left            =   1590
      Top             =   0
   End
   Begin VB.Timer tmrScale 
      Interval        =   5000
      Left            =   1320
      Top             =   0
   End
   Begin VB.TextBox txtScaleMsg 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   11055
   End
   Begin MSDataGridLib.DataGrid grdOpenLoads 
      Align           =   2  'Align Bottom
      Height          =   3795
      Left            =   0
      TabIndex        =   0
      Top             =   780
      Width           =   15255
      _ExtentX        =   26908
      _ExtentY        =   6694
      _Version        =   393216
      AllowUpdate     =   0   'False
      AllowArrows     =   -1  'True
      Enabled         =   -1  'True
      HeadLines       =   1
      RowHeight       =   15
      TabAction       =   2
      AllowDelete     =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Number of Open Loads:"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         ScrollBars      =   2
         AllowRowSizing  =   0   'False
         AllowSizing     =   0   'False
         Locked          =   -1  'True
         RecordSelectors =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame frameButtons 
      Height          =   495
      Left            =   9960
      TabIndex        =   1
      Top             =   270
      Width           =   5280
      Begin VB.CommandButton CmdRefresh 
         Caption         =   "Refresh"
         Height          =   330
         Left            =   4200
         TabIndex        =   5
         ToolTipText     =   "Create a new record to recieve product"
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton cmdNewShip 
         Caption         =   "&Ship / Return New Load"
         Height          =   330
         Left            =   2040
         TabIndex        =   3
         ToolTipText     =   "Create a new record to ship or return product"
         Top             =   120
         Width           =   2055
      End
      Begin VB.CommandButton cmdNewRecieve 
         Caption         =   "&Recieve New Load"
         Height          =   330
         Left            =   120
         TabIndex        =   2
         ToolTipText     =   "Create a new record to recieve product"
         Top             =   120
         Width           =   1815
      End
   End
   Begin VB.Frame FrameCert 
      Height          =   495
      Left            =   6240
      TabIndex        =   6
      Top             =   240
      Width           =   3615
      Begin VB.CommandButton cmdPrintCert 
         Caption         =   "&Print Certificate(s)"
         Height          =   330
         Left            =   1920
         TabIndex        =   8
         ToolTipText     =   "Print Required Certificates"
         Top             =   120
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "0 Certificate(s) to Print"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   195
         Width           =   1695
      End
   End
   Begin VB.Menu mnuSetup 
      Caption         =   "Setup"
      Begin VB.Menu mnuChangePWD 
         Caption         =   "&Change Password"
         Shortcut        =   ^C
      End
      Begin VB.Menu mnuSettings 
         Caption         =   "&Settings"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuScaleInterface 
         Caption         =   "Scale &Interface"
         Shortcut        =   ^I
      End
      Begin VB.Menu mnuOperator 
         Caption         =   "&Operators"
         Shortcut        =   ^O
      End
   End
   Begin VB.Menu mnuDrillDown 
      Caption         =   "Drill Down"
      Visible         =   0   'False
      Begin VB.Menu mnuOpenLoad 
         Caption         =   "Open Load"
      End
      Begin VB.Menu mnuDelete 
         Caption         =   "Delete Load"
      End
   End
   Begin VB.Menu mnuRePrint 
      Caption         =   "Reprint"
      Begin VB.Menu mnuReprintBOL 
         Caption         =   "&BOL"
      End
      Begin VB.Menu mnuReprintCOA 
         Caption         =   "&COA"
      End
      Begin VB.Menu mnuReprintLabel 
         Caption         =   "&Label"
      End
      Begin VB.Menu mnuRePrintLBS 
         Caption         =   "&LBS"
      End
      Begin VB.Menu mnuBlankFrm 
         Caption         =   "&Blank Forms"
         Begin VB.Menu mnuBlankBOL 
            Caption         =   "&BOL"
         End
         Begin VB.Menu mnuBlankCOA 
            Caption         =   "&COA"
            Visible         =   0   'False
         End
         Begin VB.Menu mnuBlankLabel 
            Caption         =   "&Label"
            Visible         =   0   'False
         End
         Begin VB.Menu mnuBlankLBS 
            Caption         =   "&LBS"
            Visible         =   0   'False
         End
      End
   End
End
Attribute VB_Name = "frmOpenLoads"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim WithEvents rsOpenLoads As ADODB.Recordset
Attribute rsOpenLoads.VB_VarHelpID = -1
Dim WithEvents SGSScale As SGSScaleComm.clsScaleComm
Attribute SGSScale.VB_VarHelpID = -1

Function GetOpenLoads() As Boolean
    On Error GoTo Error
    Dim SQL As String
    Dim iResponse As Integer
    Dim cnt As Long
    
    gbLastActivityTime = Date + Time
    
    If gbLocalMode = True And gbFirstRun = False Then
        'Test the connection to see if the server connection is back online
        iResponse = MsgBox("Server Disconnected. Try reconnecting?", vbQuestion + vbYesNo, "Server Connection Test")
        If iResponse = vbYes Then
            frmOpenLoads.MousePointer = vbHourglass
            gbLocalMode = False
            If OpenMASConn() = False Then
            'If MASConnectionRestored = False Then
                'Server Connect Still Down
                Me.Caption = "Open Loads (Server Disconnected) - Valero"
            Else
                If gbLocalMode = True Then
                    'Server Connect Still Down
                    MsgBox "Reconnect Attempt Failed.  Server still down", vbInformation, "Server Connection Test"
                    Me.Caption = "Open Loads (Server Disconnected) - Valero"
                Else
                    Me.Caption = "Open Loads - Valero"
                    frmMASUpdate.Show vbModal, Me
                End If
            End If
            Me.MousePointer = vbArrow
        End If
    End If
    gbFirstRun = False
    
    SQL = "Select * From OpenLoadsView Where LoadIsOpen = 1"
    
    Set grdOpenLoads.DataSource = Nothing
    
    If rsOpenLoads.State = 1 Then
        rsOpenLoads.UpdateBatch
        rsOpenLoads.Close
    End If
    
    rsOpenLoads.CursorLocation = adUseClient
    rsOpenLoads.CursorType = adOpenDynamic
'    Set rsOpenLoads = gbScaleConn.Execute(SQL)
    rsOpenLoads.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    If rsOpenLoads.State = 1 Then
        GetOpenLoads = True
        
        Set grdOpenLoads.DataSource = rsOpenLoads
        
        Call SetupGrid
        
        grdOpenLoads.Caption = "Number of Open Loads: " & rsOpenLoads.RecordCount
'        grdOpenLoads.SelBookmarks.Add grdOpenLoads.RowBookmark(0)
        If rsOpenLoads.RecordCount > 0 Then
            grdOpenLoads.Row = 0
            grdOpenLoads.col = 1
        End If
    End If
    
    cnt = CertCount()
    
    Label1.Caption = cnt & " Certificate(s) to Print"
    
    If cnt <= 0 Then
        FrameCert.Visible = False
    Else
        FrameCert.Visible = True
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Function
Error:
    MsgBox Me.Name & ".GetOpenLoads()" & vbCrLf & _
    "The following error occurred while connecting to the Scale Pass database: " & vbCrLf & vbCrLf & _
    "Error: " & str(Err.Number) & " " & Err.Description, "Error"
    Resume Next

    gbLastActivityTime = Date + Time
End Function


Sub SetupGrid()
    On Error GoTo Error
    
    Dim col As Column
    
    gbLastActivityTime = Date + Time
    
    'RKL DEJ 2018-01-05 (Start)
    Dim bVisible As Boolean
    
    If gbNotAllowSplitLoads = True Then
        bVisible = False
    Else
        bVisible = True
    End If
    'RKL DEJ 2018-01-05 (Stop)
    
    
    For Each col In grdOpenLoads.Columns
        Select Case UCase(col.DataField)
            Case "TIMEIN"
                col.Visible = True
                col.Width = 1900
                col.Caption = "Time In"
            Case "TRUCKNO"
                col.Visible = True
                col.Width = 1200
                col.Caption = "Truck No"
                col.Alignment = dbgCenter
            Case "FRONTBOL"
                col.Visible = True
                col.Width = 1200
                col.Caption = "Front BOL"
            Case "FRONTPRODUCT"
                col.Visible = True
                col.Width = 2000
                col.Caption = "Front Product"
            Case "FRONTADDITIVE"
                col.Visible = True
                col.Width = 2000
                col.Caption = "Front Additive"
            Case "REARBOL"
                col.Visible = bVisible  'RKL DEJ Changed from True to bVisible
                col.Width = 800
                col.Caption = "Rear BOL"
            Case "REARPRODUCT"
                col.Visible = bVisible  'RKL DEJ Changed from True to bVisible
                col.Width = 2000
                col.Caption = "Rear Product"
            Case "REARADDITIVE"
                col.Visible = bVisible  'RKL DEJ Changed from True to bVisible
                col.Width = 2000
                col.Caption = "Rear Additive"
            Case "INBOUNDOPERATOR"
                col.Visible = True
                col.Width = 800
                col.Caption = "Operator"
                col.Alignment = dbgCenter
            Case "FRONTTANKNBR"
                col.Visible = True
                col.Width = 900
                col.Caption = "Front Tank"
                col.Alignment = dbgCenter
            Case "REARTANKNBR"
                col.Visible = bVisible  'RKL DEJ Changed from True to bVisible
                col.Width = 900
                col.Caption = "Rear Tank"
                col.Alignment = dbgCenter
            Case "LOADISOPEN"
                col.Visible = False
            Case "LOADSTABLEKEY"
                col.Visible = False

'            Case "MASLOADSTABLEKEY"
'                col.Visible = False
'            Case "DRIVERON"
'                col.DataFormat = FormatType.fmtCheckbox
'            Case "SPLITLOAD"
'                col.DataFormat = FormatType.fmtCheckbox
'            Case "SELECTFORWEIGHTTARGETS"
'                col.DataFormat = FormatType.fmtCheckbox
'            Case "FRONTBOLHASPRINTED"
'                col.DataFormat = FormatType.fmtCheckbox
'            Case "REARBOLHASPRINTED"
'                col.DataFormat = FormatType.fmtCheckbox
'            Case "WTTICKETHASPRINTED"
'                col.DataFormat = FormatType.fmtCheckbox
'            Case "BSHASPRINTED"
'                col.DataFormat = FormatType.fmtCheckbox
'            Case "LOADISOPEN"
'                col.DataFormat = FormatType.fmtCheckbox
                
            Case Else
                col.Visible = False
'                col.Caption = CammelSpace(col.DataField)
                
        End Select
    Next
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".SetupGrid()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    gbLastActivityTime = Date + Time
End Sub



Private Sub cmdNewRecieve_Click()
    
    gbLastActivityTime = Date + Time
    
    rsOpenLoads.Close
    
    frmRecieve.LoadsTableKey = 0
    
    frmRecieve.Show vbModal
    
    Call GetOpenLoads

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdNewRecieve_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdNewShip_Click()
    
    gbLastActivityTime = Date + Time
    
    frmLoadProcessing.LoadsTableKey = 0
    
    frmLoadProcessing.Show vbModal
    
    rsOpenLoads.Close
    Call GetOpenLoads

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdNewShip_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdPrintCert_Click()
    On Error GoTo Error
    
    Dim cnt As Long
    
    cnt = CertCount()
    
    Label1.Caption = cnt & " Certificate(s) to Print"
    
    If cnt <= 0 Then
        FrameCert.Visible = False
    End If

    gbLastActivityTime = Date + Time

    Exit Sub
Error:
    MsgBox Me.Name & ".cmdPrintCert_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Private Sub cmdRefresh_Click()
    On Error GoTo Error
    
    rsOpenLoads.Close
    Call GetOpenLoads

    gbLastActivityTime = Date + Time

    Exit Sub
Error:
    MsgBox Me.Name & ".CmdRefresh_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
End Sub




'RKL DEJ 01/07/15 (START) added get message from LME
Private Sub cmdShowLMEMsg_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Dim LoadID As String
    Dim LoadIDFront As String
    Dim LoadIDRear As String
    
    Dim LMEMsg As String
    Dim LMEMsgFront As String
    Dim LMEMsgRear As String
    Dim lcLoadTblKey As Long
    Dim sSQL As String
    Dim RS As New ADODB.Recordset
    
    If gbShowLMEMsg = True And gbAllowGetLMEMsg = True Then
        If Not rsOpenLoads.EOF And Not rsOpenLoads.BOF Then
            'Get the Front Message
            lcLoadTblKey = CLng(ConvertToDouble(rsOpenLoads("LoadsTableKey")))
            sSQL = "Select * From Loads Where LoadsTableKey = " & lcLoadTblKey
            RS.Open sSQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
            
            If Not RS.EOF And Not RS.BOF Then
                LoadIDFront = ConvertToString(RS("FrontMcLeodOrderNumber").Value)
                LoadIDRear = ConvertToString(RS("RearMcLeodOrderNumber").Value)
                
                If Trim(UCase(LoadIDFront)) = Trim(UCase(LoadIDRear)) Then
                    LoadIDRear = Empty
                End If
                
                RS.Close
            End If
            
            LoadID = LoadIDFront
            
            LMEMsgFront = GetLMEMessage(LoadID)
            If LMEMsgFront <> Empty Then
                LMEMsg = "Front LME Load(" & LoadID & ") Msg: " & vbCrLf & LMEMsgFront
            End If
            
gbLastActivityTime = Date + Time

            'Get the Rear Message
            LoadID = LoadIDRear
            
            LMEMsgRear = GetLMEMessage(LoadID)
            If LMEMsgRear <> Empty Then
                If LMEMsg = Empty Then
                    LMEMsg = "Rear LME Load(" & LoadID & ") Msg: " & vbCrLf & LMEMsgRear
                Else
                    LMEMsg = LMEMsg & vbCrLf & vbCrLf & "Rear LME Load(" & LoadID & ") Msg: " & vbCrLf & LMEMsgRear
                End If
            End If
        
            If LMEMsg <> Empty Then
                MsgBox LMEMsg, vbInformation, "Message from LME"
            End If
        End If
        
    End If
    
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdShowLMEMsg_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
End Sub
'RKL DEJ 01/07/15 (STOP) added get message from LME

Private Sub Form_Load()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Me.Top = 1500
    Me.Left = 0
    
'    Set SGSScale = New SGSScaleComm.clsScaleComm
    Set SGSScale = CreateObject("SGSScaleComm.clsScaleComm")
'    SGSScale.ParentAppTitle = App.Title     'RKL SGS 10/4/13 Replaced with connection string
    'RKL DEJ
    SGSScale.conn = gbScaleConnStr
        
    Call EnableForm
    
'    Set gbMASConn = New ADODB.Connection       '*** This is set in the frmSplash.Timer1_Timer() event
    Set gbScaleConn = New ADODB.Connection
    Set rsOpenLoads = New ADODB.Recordset
    
    If OpenScaleConn() = False Then
        MsgBox "You need to setup the Scale Pass Connection before you can do any work.", vbInformation, "Setup Required"
        Call DisableForm
        Exit Sub
    End If
        
    If gbLocalMode = True Then
        MsgBox "Unable to connect successfully to MAS 500.  Server connection may have been lost or 'Setup' not performed properly.  All data will be pulled from synchronized Access database.", vbInformation + vbOKOnly, "Server Disconnected Notice"
        Me.Caption = "Open Loads (Server Disconnected) - Valero"
    Else
        frmMASUpdate.Show vbModal, Me
    End If
    
    If GetOpenLoads() = False Then
        MsgBox "Could not obtain obtain the open loads.  Contact your IT suppor or SGS Technology.", vbExclamation, "Failure"
        Call DisableForm
    End If
    
    'RkL DEJ 1/7/15 (start)
    If gbShowLMEMsg = True Then
        cmdShowLMEMsg.Visible = gbAllowGetLMEMsg
    Else
        cmdShowLMEMsg.Visible = False
    End If
    'RkL DEJ 1/7/15 (stop)
        
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Resize()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    frameButtons.Left = Me.Width - (frameButtons.Width + 200)
    
    If Me.Height > 1600 Then
        grdOpenLoads.Height = Me.Height - 1600
    End If
    
'    grdOpenLoads.Row = 1
'    grdOpenLoads.col = 1
    grdOpenLoads.SetFocus
    
    gbLastActivityTime = Date + Time
End Sub


Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    Dim frm As Form
    
    gbLastActivityTime = Date + Time
    
    For Each frm In Forms
        If Not frm Is Me Then
            Unload frm
        End If
    Next

    Set grdOpenLoads.DataSource = Nothing
    
    If rsOpenLoads.State = 1 Then
        rsOpenLoads.UpdateBatch
        rsOpenLoads.Close
    End If
    
    Set rsOpenLoads = Nothing
    
    If gbScaleConn.State = 1 Then gbScaleConn.Close
    Set gbScaleConn = Nothing
    
    If gbMASConn.State = 1 Then gbMASConn.Close
    Set gbMASConn = Nothing
    
    SGSScale.ClosePort
    Set SGSScale = Nothing

    gbLastActivityTime = Date + Time
End Sub

Private Sub frameButtons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub grdOpenLoads_DblClick()
    
    
    Dim LoadType As String
    
    gbLastActivityTime = Date + Time
    
    If Not rsOpenLoads.EOF And Not rsOpenLoads.BOF Then
        LoadType = Trim(UCase(ConvertToString(rsOpenLoads("LoadType").Value)))
        
Dim FLD As ADODB.Field

'For Each FLD In rsOpenLoads.Fields
'    Debug.Print FLD.Name
'Next

        Select Case LoadType
            Case "LOADFROMLME"
                frmLoadProcessing.LoadsTableKey = rsOpenLoads("LoadsTableKey").Value
                frmLoadProcessing.Show vbModal
                
            Case "RECIEVELOAD"
                frmRecieve.LoadsTableKey = rsOpenLoads("LoadsTableKey").Value
                frmRecieve.Show vbModal
            
            Case Else
                MsgBox "Don't know what type of load this is...", vbExclamation + vbCritical, "Missing Information"
                Exit Sub
        End Select
        
    Else
        'No record to process (And we don't know what type of record to process
        Exit Sub
    End If
    
'    rsOpenLoads.Close
    
'    frmLoadProcessing.Show vbModal
    
    rsOpenLoads.Close
    Call GetOpenLoads

    gbLastActivityTime = Date + Time
End Sub

Private Sub grdOpenLoads_HeadClick(ByVal ColIndex As Integer)
    On Error Resume Next
    
    Dim SortField As String
    Dim SortString As String
    
    gbLastActivityTime = Date + Time
    
    If rsOpenLoads.State <> 1 Then Exit Sub
    
    SortField = "[" & grdOpenLoads.Columns(ColIndex).DataField & "]"
    If InStr(rsOpenLoads.Sort, "Asc") Then
        SortString = SortField & " Desc"
    Else
        SortString = SortField & " Asc"
    End If
    rsOpenLoads.Sort = SortString
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub grdOpenLoads_KeyDown(KeyCode As Integer, Shift As Integer)
   On Error GoTo Error
   Dim gbResponse As Integer
   
    gbLastActivityTime = Date + Time
   
   If KeyCode = vbKeyDelete Then
        If mnuDelete.Enabled = True Then
            Call mnuDelete_Click
        End If
'      KeyCode = 0
'      gbResponse = MsgBox("Are you sure you want to delete the selected load?", vbQuestion + vbYesNo, sSystemName)
'      If gbResponse = vbYes Then
'         gbScaleConn.Execute "DELETE FROM Loads WHERE LoadsTableKey = " & rsOpenLoads.Fields("LoadsTableKey")
'         gbScaleConn.Execute "DELETE FROM BOLPrinting WHERE LoadsTableKey = " & rsOpenLoads.Fields("LoadsTableKey")
'         rsOpenLoads.Requery
'         grdOpenLoads.Caption = "Number of Open Loads: " & rsOpenLoads.RecordCount
'      End If
   End If
   
    gbLastActivityTime = Date + Time
   Exit Sub
Error:
   MsgBox Me.Name & ".grdOpenLoads_KeyDown()" & vbCrLf & _
   "Error: " & Err.Number & " " & Err.Description

    gbLastActivityTime = Date + Time
End Sub

Private Sub grdOpenLoads_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
      KeyAscii = 0
      grdOpenLoads_DblClick
   End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub grdOpenLoads_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    gbLastActivityTime = Date + Time
    
    Select Case Button
        Case 1  'Left Mouse
        Case 2  'Right Mouse
            If rsOpenLoads.State = 1 Then
                If rsOpenLoads.EOF Or rsOpenLoads.BOF Then
                    mnuOpenLoad.Enabled = False
                    mnuDelete.Enabled = False
                Else
                    mnuOpenLoad.Enabled = True
                    mnuDelete.Enabled = True
                End If
            Else
                mnuOpenLoad.Enabled = False
                mnuDelete.Enabled = False
            End If
            
            PopupMenu mnuDrillDown ', , X, Y
            
        Case 4  'Scroll Button
        Case Else
    End Select

    gbLastActivityTime = Date + Time
End Sub

Private Sub grdOpenLoads_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub mnuBlankBOL_Click()
    ' Create BOLPrinting Record (with minamal data)
    ' Print the BOL
    ' Delete teh BOL Printing Record
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    
    If CanPrintBlankBOL() = False Then
        Exit Sub
    End If
    
    frmPrintBlankBOL.Show vbModal, Me

    gbLastActivityTime = Date + Time

    Exit Sub
Error:
    MsgBox Me.Name & ".mnuReprintCOA_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    gbLastActivityTime = Date + Time
End Sub

Private Sub mnuChangePWD_Click()
    
    gbLastActivityTime = Date + Time
    
    If SetupPWD <> Empty Then
        Load frmPassword
        frmPassword.Setup = False
        frmPassword.Password = SetupPWD
        
        frmPassword.Show vbModal, Me
        
        If frmPassword.Success = True Then
            frmPassword.Password = Empty
            frmPassword.Setup = True
            
            MsgBox "Enter the new password.", vbOKOnly, "New Password"
            
            frmPassword.Show vbModal, Me
            
            If frmPassword.Success = True Then
                SetupPWD = frmPassword.Password
                SaveSetting App.Title, "SetupPasswords", "SetupPWD", Encrypt(SetupPWD)
            End If
            
        End If
        
        Unload frmPassword
        
    Else
        Load frmPassword
        frmPassword.Setup = True
        
        MsgBox "Enter the new password.", vbOKOnly, "New Password"
        
        frmPassword.Show vbModal, Me
        
        If frmPassword.Success = True Then
            SetupPWD = frmPassword.Password
            SaveSetting App.Title, "SetupPasswords", "SetupPWD", Encrypt(SetupPWD)
        End If
        
        Unload frmPassword
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub mnuDelete_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If Not rsOpenLoads.EOF And Not rsOpenLoads.BOF Then
        If MsgBox("Are you sure you want to delete this load?", vbYesNo, "Delete?") = vbYes Then
            rsOpenLoads.Delete
            rsOpenLoads.UpdateBatch
            rsOpenLoads.Resync
        End If
    End If
        
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".mnuDelete_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub mnuOpenLoad_Click()
    Call grdOpenLoads_DblClick
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub mnuOperator_Click()
    gbLastActivityTime = Date + Time
    
    If CanModifySetup() <> True Then
        Exit Sub
    End If
    
    frmOperators.Show vbModal, Me
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub mnuReprintBOL_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    frmRePrintBOL.Show vbModal, Me

    gbLastActivityTime = Date + Time

    Exit Sub
Error:
    MsgBox Me.Name & ".mnuReprintCOA_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    gbLastActivityTime = Date + Time


End Sub

Private Sub mnuReprintCOA_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    frmPrintCOA.Show vbModal, Me

    gbLastActivityTime = Date + Time

    Exit Sub
Error:
    MsgBox Me.Name & ".mnuReprintCOA_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    gbLastActivityTime = Date + Time

End Sub

Private Sub mnuReprintLabel_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    frmRePrintLabel.Show vbModal, Me

    gbLastActivityTime = Date + Time

    Exit Sub
Error:
    MsgBox Me.Name & ".mnuReprintCOA_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    gbLastActivityTime = Date + Time


End Sub

Private Sub mnuRePrintLBS_Click()

    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    frmRePrintLBS.Show vbModal, Me

    gbLastActivityTime = Date + Time

    Exit Sub
Error:
    MsgBox Me.Name & ".mnuRePrintLBS_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    gbLastActivityTime = Date + Time
End Sub

Private Sub mnuScaleInterface_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If CanModifySetup() <> True Then
        Exit Sub
    End If
    
    SGSScale.ClosePort
    
    SGSScale.Setup
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub mnuSettings_Click()
    On Error Resume Next
    Dim lcConnStr As String
    
    gbLastActivityTime = Date + Time
    
    If CanModifySetup() <> True Then
        Exit Sub
    End If
    
    lcConnStr = UCase(gbScaleConnStr)
    
    frmSetup.Show vbModal, Me
    
    txtScaleMsg.Text = "Obtaining Connection to the Scale Pass database (Access)."
    txtScaleMsg.Visible = True
    If OpenScaleConn() = False Then
        Call DisableForm
    Else
        txtScaleMsg.Text = "Obtaining Connection to the MAS 500 database."
        If OpenMASConn() = False Then       'This is to connect to SQL
            MsgBox "Could not obtain access to the MAS 500 data.  Please fix the MAS 500 Connection.  If this persits contact your IT support.", vbExclamation, "Warning"
            Call DisableForm
        Else
            If gbLocalMode = True Then
                MsgBox "Unable to connect successfully to MAS 500.  Server connection may have been lost or 'Setup' not performed properly.  All data will be pulled from synchronized Access database.", vbInformation + vbOKOnly, "Server Disconnected Notice"
                Me.Caption = "Open Loads (Server Disconnected) - Valero"
            Else
                Me.Caption = "Open Loads - Valero"
            End If
        End If
        
        txtScaleMsg.Text = "Obtaining Open Loads."
        If GetOpenLoads() = False Then
            Call DisableForm
        Else
            Call EnableForm
        End If
    End If
    
    'RkL DEJ 1/7/15 (start)
    If gbShowLMEMsg = True Then
        cmdShowLMEMsg.Visible = gbAllowGetLMEMsg
    Else
        cmdShowLMEMsg.Visible = False
    End If
    'RkL DEJ 1/7/15 (stop)
    
    txtScaleMsg.Text = Empty
    txtScaleMsg.Visible = False
    
    GetMASCompanyID
    
    gbLastActivityTime = Date + Time
    
    

End Sub

Sub DisableForm()
    grdOpenLoads.Enabled = False
'    cmdNewReturn.Enabled = False
    cmdNewRecieve.Enabled = False
    cmdNewShip.Enabled = False
    mnuDrillDown.Enabled = False
    mnuOperator.Enabled = False
    
    cmdRefresh.Enabled = False

    gbLastActivityTime = Date + Time
End Sub

Sub EnableForm()
    grdOpenLoads.Enabled = True
'    cmdNewReturn.Enabled = True
    cmdNewRecieve.Enabled = True
    cmdNewShip.Enabled = True
    mnuDrillDown.Enabled = True
    mnuOperator.Enabled = True

    cmdRefresh.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_Error(ErrMsg As String)
    txtScaleMsg.Text = ErrMsg & vbTab & txtScaleMsg.Text
    txtScaleMsg.Visible = True
    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_StatusChange(StatusStr As String)
'    txtScaleMsg.Text = StatusStr & vbTab & txtScaleMsg.Text
'    txtScaleMsg.Visible = True
'    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
'    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_Warning(WarningMsg As String)
'    txtScaleMsg.Text = WarningMsg & vbTab & txtScaleMsg.Text
'    txtScaleMsg.Visible = True
'    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
'    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub



Private Sub tmrAutoReturnNoActivity_Timer()

End Sub

Private Sub tmrMASConnection_Timer()
    gbLastActivityTime = Date + Time
    
    If gbLocalMode = True Then
        'Use Tag property to keep track of minutes
        If Val(tmrMASConnection.Tag) < 15 Then
            'Increment counter
            tmrMASConnection.Tag = Val(tmrMASConnection.Tag) + 1
        Else
            'Test the connection to see if the server connection is back online
            Call GetOpenLoads
            tmrMASConnection.Tag = "0"
        End If
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub tmrScale_Timer()
    tmrScale.Enabled = False
    txtScaleMsg.Visible = False
    txtScaleMsg.Text = Empty

    gbLastActivityTime = Date + Time
End Sub


Function CanModifySetup() As Boolean
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If SetupPWD <> Empty Then
        Load frmPassword
        frmPassword.Setup = False
        frmPassword.Password = SetupPWD
        
        frmPassword.Show vbModal, Me
        
        CanModifySetup = frmPassword.Success
        
        Unload frmPassword
        
    Else
        CanModifySetup = True
        
        If MsgBox("You currently do not have a security password.  Do you want to setup a security password now?", vbYesNo, "Set Security Password?") = vbYes Then
            Call mnuChangePWD_Click
        End If
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".CanModifySetup() as Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function

Function CanPrintBlankBOL() As Boolean
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If SetupPWD <> Empty Then
        Load frmPassword
        frmPassword.Setup = False
        frmPassword.Password = SetupPWD
        
        frmPassword.Show vbModal, Me
        
        CanPrintBlankBOL = frmPassword.Success
        
        Unload frmPassword
        
    Else
        CanPrintBlankBOL = True
        
'        If MsgBox("You currently do not have a security password.  Do you want to setup a security password now?", vbYesNo, "Set Security Password?") = vbYes Then
'            Call mnuChangePWD_Click
'        End If
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".CanPrintBlankBOL() as Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function


Private Sub txtScaleMsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub
