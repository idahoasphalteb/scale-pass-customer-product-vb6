VERSION 5.00
Begin VB.Form frmCommError 
   BackColor       =   &H00FF00FF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Scale Indicator Communication Errors - Valero"
   ClientHeight    =   3585
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6540
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmCommError.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3585
   ScaleWidth      =   6540
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrScale 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   0
      Top             =   0
   End
   Begin VB.TextBox txtErrMsg 
      Height          =   2895
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   120
      Width           =   6255
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   2520
      TabIndex        =   0
      Top             =   3120
      Width           =   1575
   End
End
Attribute VB_Name = "frmCommError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    Unload Me

    Err.Clear
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    On Error Resume Next
    tmrScale.Enabled = False
    
End Sub

Private Sub tmrScale_Timer()
    On Error Resume Next
    
    tmrScale.Enabled = False
    
    Unload Me
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub


Public Sub DisplayError(ErrMsg As String)
    On Error Resume Next
    
'    If tmrScale.Enabled = True Then
'        tmrScale.Enabled = False
'        txtErrMsg.Text = ErrMsg & vbCrLf & txtErrMsg.Text
'    Else
'        tmrScale.Enabled = False
'        txtErrMsg.Text = ErrMsg
'    End If
'
'    Me.Show
'
'    tmrScale.Enabled = True
    
    txtErrMsg.Text = ErrMsg
    
    gbLastActivityTime = Date + Time
    
    Err.Clear
End Sub
