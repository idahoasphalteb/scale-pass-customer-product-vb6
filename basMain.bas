Attribute VB_Name = "basMain"
Option Explicit

'************************************************************************************************************************
'Ini file Properties (START)
'************************************************************************************************************************
Global INIFilePath As String    'This will always be created and read from the application path

Global SetupPWD As String       'Password to all setup (Operators, Database, Scale)

'Database Connections
Global gbScaleConn As ADODB.Connection
Global gbMASConn As ADODB.Connection

'Lookup Values
Global gbMASWhseKey As Long
Global gbMASCompanyID As String
Global gbMASLocation As String  'RKL DEJ added for weight ticket

'Misc Settings
Global gbPlant As String                'Name of the plant using scale pass
Global gbPlantPrefix As String          'Number of plant using scale pass
Global gbWhseID As String
Global gbSCALEPASSPRINTER As String     'Name of the Scale Pass Printer
Global gbBLPRINTER As String            'Name of the BOL printer
Global gbBOLCypressPRINTER As String            'Name of the BOL printer
Global gbALTERNATEPRINTER As String     'Name of the Alternate printer
'Global gbDAYSTOKEEP As Integer          '???
Global gbMANUALSCALING As Boolean       'Determines if plant is hooked to a scale
Global gbAutoRtnOpenLoads As Boolean    'Determins if the idle screen will automaticaly return to the Open Loads screen after idel for the number of seconds
Global gbAutoRtrnSecnds As Integer      'After this number of seconds of no activity (No Mouse movement) then return to Open Loads screen
Global gbLastActivityTime As Date

Global gbEnforceCOA As Boolean
Global gbRequireLotNbr As Boolean

'RKL DEJ 9/25/13 (Start)
Global gbCOAPrinter As String           'RKL DEJ

Global gbPrintExtraBOL As Boolean    'RKL DEJ 9/25/13
Global gbPrintExtraBOLCOAOnly As Boolean    'RKL DEJ 9/25/13
Global gbPrintExtraCOA As Boolean    'RKL DEJ 9/25/13
Global gbPrintExtraLblCert As Boolean    'RKL DEJ 9/25/13
Global gbPrintExtraLblRisk As Boolean    'RKL DEJ 9/25/13
Global gbPrintExtraLblWY As Boolean    'RKL DEJ 9/25/13
Global gbHideOverShip As Boolean       'RKL DEJ 6/12/14
Global gbShowLMEMsg As Boolean       'RKL DEJ 01/07/15
Global gbAllowGetLMEMsg As Boolean       'RKL DEJ 01/07/15
Global gbDriverOn As Boolean           'RKL DEJ 12/17/14

Global gbNbrOfCOACopies As Integer      'Number of COA copies to print. Default to 2
Global gbLablePrinter As String         'Printer for Sample Lables
Global gbExtraBOLPrinter As String         'Extra copy Printer
Global gbExtraCOAPrinter As String         'Extra copy Printer
Global gbExtraLabelPrinter As String         'Extra copy Printer for Sample Lables
Global gbPrintLabels As Boolean         'Allow Label Printing
Global gbPrintLabelAfterBOL As Boolean         'Allow Label Printing
Global gbAltBOLPrinter As String        'RKL DEJ 2016-06-11
'RKL DEJ 9/25/13 (Stop)

'Misc Variables
Global gbNewLoad As Boolean
Global gbProceed As Boolean
Global gbResponse As Integer
Global gbLoadsTableKey As Long
Global gbLocalMode As Boolean
Global gbFirstRun As Boolean

'May not need these
Global gbMODE As String             'Debug/Live
Global gbPrintBOLCypress As Boolean             'Debug/Live
Global gbPrintCOACypress As Boolean             'Debug/Live

'Report Viewer
Global crxApp As CRAXDRt.Application
Global crxRpt As CRAXDRt.Report
'Global crxApp As CRAXDRT.Application
'Global crxRpt As CRAXDRT.Report
'Global crxDatabase As CRAXDRT.Database
'Global crxDatabaseTables As CRAXDRT.DatabaseTables
'Global crxDatabaseTable As CRAXDRT.DatabaseTable
'Global crxDatabaseField As CRAXDRT.DatabaseFieldDefinition
'Global crxSortField As CRAXDRT.FormulaFieldDefinition

'Scale Pass Database
Global gbScaleConnStr As String
Global gbScaleDSN As String
Global gbScaleUID As String
Global gbScalePWD As String
Global gbScaleDB As String
Global gbScaleDriver As String
Global gbScaleServer As String
Global gbScaleAdlPrty As String
Global gbScaleWinAuth As Boolean

'McLeod Database
Global gbMcLeodConnStr As String
Global gbMcLeodDSN As String
Global gbMcLeodUID As String
Global gbMcLeodPWD As String
Global gbMcLeodDB As String
Global gbMcLeodDriver As String
Global gbMcLeodServer As String
Global gbMcLeodAdlPrty As String
Global gbMcLeodWinAuth As Boolean

'MAS 500 Database
Global gbMASConnStr As String
Global gbMASDSN As String
Global gbMASUID As String
Global gbMASPWD As String
Global gbMASDB As String
Global gbMASDriver As String
Global gbMASServer As String
Global gbMASAdlPrty As String
Global gbMASWinAuth As Boolean

'RKL DEJ 4/23/15 (START)
Global gbStdTon As Integer
Global gbMaxTonVar As Integer
Global gbScaleInWarnTons As Boolean
Global gbScaleOutWarnVar As Boolean
Global gbProcessBOLWarnVar As Boolean
'RKL DEJ 4/23/15 (STOP)

'RKL DEJ 2016-05-03 (START)
Global gbPrintPASSPatent As Boolean
Global gbPASSPatent As String
Global gbBOLAddrPhone As String
'RKL DEJ 2016-05-03 (STOP)

'RKL DEJ 2017-12-04 (START)
Global gbCustOwnedProd As Boolean
Global gbAutoPostIMTrans As Boolean
Global gbNotAllowSplitLoads As Boolean
Global gbSrcVariance As Double
Global gbIMTranReasonCode As String
Global gbForceVarianceCheck As Boolean
'RKL DEJ 2017-12-04 (STOP)

'************************************************************************************************************************
'Ini file Properties (STOP)
'************************************************************************************************************************


'************************************************************************************************************************
'declarations for working with Ini files (START)
'************************************************************************************************************************
Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias _
"GetPrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpReturnedString As String, _
ByVal nSize As Long, ByVal lpFileName As String) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
"GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, _
ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, _
ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias _
"WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal lpString As String, _
ByVal lpFileName As String) As Long

Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
"WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, _
ByVal lpString As Any, ByVal lpFileName As String) As Long
'************************************************************************************************************************
'declarations for working with Ini files (STOP)
'************************************************************************************************************************

Public Type WeightTicket
    InBoundOpr As String
    OutBoundOpr As String
    ManualScaleing As Boolean
    DriverOn As Boolean
    TruckNo As String
    TicketNo As String
    GrossFront As Double
    GrossRear As Double
    GrossTotal As Double
    TareFront As Double
    TareRear As Double
    TareTotal As Double
    NetFront As Double
    NetRear As Double
    NetTotal As Double
    Product As String
    Tank As String
    
End Type

Public Type LabelData
    Product As String
    Additive As String
    ScaleInDate As String
    BLNbr As String
    Tank As String
    TonsShipped As String
    Customer As String
    ProjectNbr As String
    TruckNbr As String
    Company As String
    Plant As String
    ProductClass As String
    TonsCaption As String       'RKL DEJ 1/22/14
End Type

Public Function PrePad(s As String, StrLen As Integer)

    PrePad = Right((Space(StrLen) & s), StrLen)

    gbLastActivityTime = Date + Time
End Function



Public Function PrintWeightTicket(wt As WeightTicket, Optional copies As Integer = 1) As Boolean

    Dim CurrentTime As Date
    Dim Currentdate As Date
    Dim CurrentPrinter As String
    Dim i As Integer
    Dim NetFront, NetRear, NetTotal As Long
    Dim oPrinter As Printer
    Dim PrinterIndex As Integer
    Dim PrintSuccess As Integer
    Dim PrintLine As String
    Dim PrintString As String
    Dim PrinterFound As Integer
    Dim TempLine As String
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
    i = 0
    'Get Weight Ticket Printer
    PrinterFound = vbNo
    For Each oPrinter In Printers
        CurrentPrinter = UCase(Trim(oPrinter.DeviceName))
        'Debug.Print CurrentPrinter & _
                    Trim(gbSCALEPASSPRINTER)
        

        If (CurrentPrinter = UCase(Trim(gbSCALEPASSPRINTER))) Then
            'Debug.Print "gbSCALEPASSPRINTER Selected"
            PrinterIndex = i
            PrinterFound = vbYes
        End If
        
        i = i + 1
    Next
    
    'Was the printer found?
    If (PrinterFound = vbNo) Then
        MsgBox "ScalePass Printer Not Found!", vbCritical, "Printer Not Found"
        GoTo OutaHere
    End If

    ' Select appropriate printer
    'Debug.Print PrinterIndex
    Set Printer = Printers(PrinterIndex)

BeginPrint:
    ' Printer font,
    
    Printer.FontSize = "16"
    Printer.Font = "FontB24"

    Printer.FontBold = True
    
    'Header
    Printer.Print
    Printer.Print
    
    'RKL DEJ 2017-04-24 (START)
    'Added new logic to print the WEI companies as well.
'    If gbPlantPrefix >= 6 Then 'Woods Cross and Rawlins (Plants 6&7)
'        PrintLine = vbTab & "Peak Asphalt Supply, Inc."
'    Else
'        PrintLine = vbTab & "Idaho Asphalt Supply, Inc."
'    End If
    
    Select Case Trim(UCase(gbMASCompanyID))
        Case "PEA"
            PrintLine = vbTab & "Peak Asphalt Supply, Inc."
        Case "IDA"
            PrintLine = vbTab & "Idaho Asphalt Supply, Inc."
        Case "WEI"
            PrintLine = vbTab & "Western Emulsions, Inc."
        Case "WEC"
            PrintLine = vbTab & "Western Emulsions Coolidge"
        Case Else
            PrintLine = vbTab & "UNKNOWN COMPANY " & Trim(UCase(gbMASCompanyID))
    End Select
    'RKL DEJ 2017-04-24 (STOP)
    
    Printer.Print PrintLine
    Printer.Print
        
    'Location
    PrintLine = vbTab & gbMASLocation   'RKL DEJ 2017-05-24 Changed to this and commented out the select satement below
'    Select Case gbPlantPrefix
'        Case 3
'            PrintLine = vbTab & "Nampa, Idaho"
'        Case 4
'            PrintLine = vbTab & "Hauser, Idaho"
'        Case 5
'            PrintLine = vbTab & "Blackfoot, Idaho"
'        Case 6
'            PrintLine = vbTab & "Woods Cross, Utah"
'        Case 7
'            PrintLine = vbTab & "Rawlins, Wyoming"
'    End Select
    
    Printer.Print PrintLine
    
    'Type of pass
    Printer.Print
    PrintLine = vbTab & "WEIGHT TICKET"
    Printer.Print PrintLine
    
    Printer.FontBold = False
    
    'Date/Time issued
    Printer.Print
    Printer.Print
    PrintLine = vbTab & Format(Now, "mm/dd/yy    hh:mmAMPM")
    Printer.Print PrintLine
    
    'Truck/Driver
    Printer.Print
    Printer.Print

    TempLine = vbTab & "TRUCK NO. " & CStr(wt.TruckNo)
    PrintLine = TempLine
    Printer.Print PrintLine
    Printer.Print
                
    TempLine = ""
    If wt.DriverOn = False Then
        TempLine = TempLine + "Driver: OFF"
    Else
        TempLine = TempLine + "Driver: ON"
    End If
    PrintLine = vbTab & TempLine
    Printer.Print PrintLine
    Printer.Print
    Printer.Print
    
    'Product/Tank
    TempLine = vbTab & "Product: " & CStr(wt.Product)
    PrintLine = TempLine
    Printer.Print PrintLine
    Printer.Print
                
    TempLine = vbTab & "Tank: " & CStr(wt.Tank)
    PrintLine = TempLine
    Printer.Print PrintLine
    Printer.Print
    Printer.Print
                
    'Handkeyed or not???
    If wt.ManualScaleing = True Then
        If gbMANUALSCALING = True Then
            PrintLine = vbTab & "<--------- Keyed --------->"
        Else
            PrintLine = vbTab & "Check separate scale ticket"
        End If
        Printer.Print PrintLine
        Printer.Print
        Printer.Print
    End If
    
    'Weights
    'Gross Weight
    PrintLine = vbTab & "GROSS: " & vbTab & PrePad(CStr(wt.GrossFront), 6) & _
        "  " & PrePad(CStr(wt.GrossRear), 6) & _
        "  " & PrePad(CStr(wt.GrossTotal), 6)
    Printer.Print PrintLine
        
    'Tare Weight
    Printer.Print
    PrintLine = vbTab & "TARE: " & vbTab & vbTab & PrePad(CStr(wt.TareFront), 6) & _
        "  " & PrePad(CStr(wt.TareRear), 6) & _
        "  " & PrePad(CStr(wt.TareTotal), 6)
    Printer.Print PrintLine
    
    'Net Weight
    Printer.Print
    PrintLine = vbTab & "NET " & vbTab & vbTab & _
        PrePad(CStr(wt.NetFront), 6) & _
        "  " & PrePad(CStr(wt.NetRear), 6) & _
        "  " & PrePad(CStr(wt.NetTotal), 6)
    Printer.Print PrintLine

    'State certified scale
    Printer.Print
    Printer.Print
    PrintLine = vbTab & "Weighed on a state certified scale"
    Printer.Print PrintLine

    'Scale Operators
    Printer.Print
    PrintLine = vbTab & "BY: " & vbTab & vbTab & wt.InBoundOpr & _
        "     " & wt.OutBoundOpr
    Printer.Print PrintLine

    'Driver's Signature
    Printer.Print
    Printer.Print
    Printer.Print
    PrintLine = vbTab & "Driver's Signature:   ________________________________"
    Printer.Print PrintLine

    Printer.EndDoc
    
    PrintWeightTicket = True
    
    ' Reprint if necessary
    copies = copies - 1
    If copies > 0 Then
            GoTo BeginPrint
    End If

OutaHere:
    gbLastActivityTime = Date + Time
Exit Function


ErrHandler:
    gbLastActivityTime = Date + Time
    If Err.Number = 9 Then
        lErrMsg = "WeightTicket Printer Not properly installed"
        
        MsgBox lErrMsg, vbCritical, "No Printer"
    Else
        lErrMsg = "PrintWeightTicket(WT As WeightTicket, Optional copies As Integer = 1) As Boolean" & vbCrLf & _
        "Error: " & Err.Number & " " & Err.Description
        
        MsgBox lErrMsg, vbCritical, "Error"
    End If
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    Err.Clear
End Function



Public Function CenterLine(L As String, f As String) As String

    Dim PrintWidth As Integer

    gbLastActivityTime = Date + Time
    Select Case f
        Case "FontA24"
            PrintWidth = 80
        Case "FontB24"
            PrintWidth = 80
    End Select
    
    CenterLine = String((PrintWidth - Len(L)) - _
        ((PrintWidth - Len(L)) / 2), " ") & L

    gbLastActivityTime = Date + Time
End Function



Function CammelSpace(str As String) As String
    On Error GoTo Error
    Dim Cptn As String
    Dim i As Integer
    Dim LastPosition As Integer
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    CammelSpace = str
    
    For i = 1 To Len(str)
        Select Case Mid(str, i, 1)
            Case "A" To "Z"
                If Cptn = Empty Then
                    Cptn = Cptn & Mid(str, i, 1)
                Else
                    Cptn = Cptn & " " & Mid(str, i, 1)
                End If
            Case Else
                Cptn = Cptn & Mid(str, i, 1)
        End Select
    Next
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMain.CammelSpace(Str As String) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    Err.Clear
End Function


Function GetItemClassID(ItemID As Variant, WhseID As Variant) As String
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select * From vluGetItemAndWareHouse_SGS Where ItemID = '" & ItemID & "' And WhseID = '" & WhseID & "'"
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenStatic
    
    RS.Open SQL, gbMASConn, adOpenStatic, adLockReadOnly
    
    If RS.State = 1 Then
        If Not RS.EOF And Not RS.BOF Then
            GetItemClassID = ConvertToString(RS("ItemClassID").Value)
        End If
    End If
    
CleanUP:
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    gbLastActivityTime = Date + Time
    
    lErrMsg = "basMain.GetItemClassID(ItemID, WhseID) as String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function


Function IsValidItem(ByRef ItemDesc As Variant, FacilitySeqNo As Variant, Optional ByRef cnt As Integer = 0, Optional ByRef ItemID As String = Empty, Optional ByRef ItemClassID As String = Empty) As Boolean
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    cnt = -1
    
    SQL = "Select * From vluGetItemAndWarehouse_SGS Where ItemShortDesc = '" & ItemDesc & "' And FacilitySeqNo = " & ConvertToDouble(FacilitySeqNo) & " And Coalesce(CustOwnedProd,0) = " & Int(Abs(gbCustOwnedProd))   '2018-01-05 Added CustOwnedProd Filter
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenStatic
    
    RS.Open SQL, gbMASConn, adOpenStatic, adLockReadOnly
    
    If RS.State = 1 Then
        If Not RS.EOF And Not RS.BOF Then
            
            If RS.RecordCount > 1 Then
                RS.MoveFirst
                While Not RS.EOF And Not RS.BOF
                    If Trim(UCase(ItemID)) = Trim(UCase(ConvertToString(RS("ItemID").Value))) Then
                        cnt = 1
                        GoTo AssignValues
                    End If
                    
                    RS.MoveNext
                Wend
            End If
            
AssignValues:
            If RS.EOF Or RS.BOF Then RS.MoveFirst
            
            ItemDesc = ConvertToString(RS("ItemShortDesc").Value)
            ItemID = ConvertToString(RS("ItemID").Value)
            ItemClassID = ConvertToString(RS("ItemClassID").Value)
            
            If cnt <> 1 Then
                cnt = RS.RecordCount
            End If
            
            IsValidItem = True
        End If
    End If
    
CleanUP:
    On Error Resume Next
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMain.IsValidItem(ItemDesc As Variant, FacilitySeqNo As Variant, Optional ByRef Cnt As Integer = 0) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    GoTo CleanUP
End Function


'RKL DEJ 2018-01-06 Added method IsValidItemID
Function IsValidItemID(ByRef ItemDesc As Variant, FacilitySeqNo As Variant, Optional ByRef cnt As Integer = 0, Optional ByRef ItemID As String = Empty, Optional ByRef ItemClassID As String = Empty) As Boolean
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    cnt = -1
    
    SQL = "Select * From vluGetItemAndWarehouse_SGS Where ItemID = " & Quoted(ItemID) & " And FacilitySeqNo = " & ConvertToDouble(FacilitySeqNo) & " And Coalesce(CustOwnedProd,0) = " & Int(Abs(gbCustOwnedProd))   '2018-01-05 Added CustOwnedProd Filter
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenStatic
    
    RS.Open SQL, gbMASConn, adOpenStatic, adLockReadOnly
    
    If RS.State = 1 Then
        If Not RS.EOF And Not RS.BOF Then
            
            If RS.RecordCount > 1 Then
                RS.MoveFirst
                While Not RS.EOF And Not RS.BOF
                    If Trim(UCase(ItemID)) = Trim(UCase(ConvertToString(RS("ItemID").Value))) Then
                        cnt = 1
                        GoTo AssignValues
                    End If
                    
                    RS.MoveNext
                Wend
            End If
            
AssignValues:
            If RS.EOF Or RS.BOF Then RS.MoveFirst
            
            ItemDesc = ConvertToString(RS("ItemShortDesc").Value)
            ItemID = ConvertToString(RS("ItemID").Value)
            ItemClassID = ConvertToString(RS("ItemClassID").Value)
            
            If cnt <> 1 Then
                cnt = RS.RecordCount
            End If
            
            IsValidItemID = True
        End If
    End If
    
CleanUP:
    On Error Resume Next
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMain.IsValidItemID(ItemDesc As Variant, FacilitySeqNo As Variant, Optional ByRef Cnt As Integer = 0) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    GoTo CleanUP
End Function


'The intent of this is to validate a connection to MAS
Public Function HaveLostMASConnection() As Boolean
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    'default to true
    HaveLostMASConnection = True
    
    If gbLocalMode = True Then
        Exit Function
    End If
    
    gbMASConn.Execute "Select 1 'Success'"
    
    'If we made it this far then the connection is good.
    HaveLostMASConnection = False
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    Select Case Err.Number
        Case -2147467259
            If OpenMASConn(True) = False Then
                lErrMsg = "Unable to connect successfully to MAS 500.  Server connection may have been lost or 'Setup' not performed properly.  All data will be pulled from synchronized Access database."
                
                MsgBox lErrMsg, vbInformation + vbOKOnly, "Server Disconnected Notice"
            Else
                If gbLocalMode = True Then
                    frmOpenLoads.Caption = "Open Loads (Server Disconnected) - Valero"
                    lErrMsg = "Unable to connect successfully to MAS 500.  Server connection may have been lost or 'Setup' not performed properly.  All data will be pulled from synchronized Access database."
                    
                    MsgBox lErrMsg, vbInformation + vbOKOnly, "Server Disconnected Notice"
                End If
            End If
            
        Case Else
            lErrMsg = "basMain.HaveLostMASConnection(ErrNbr As Long) As Boolean" & vbCrLf & _
            "Error: " & Err.Number & " " & Err.Description
            
            MsgBox lErrMsg, vbCritical, "Error"
    End Select
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    Err.Clear
    gbLastActivityTime = Date + Time
End Function

'The intent of this is to retest for a connection to MAS
Public Function MASConnectionRestored() As Boolean
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    'default to true
    MASConnectionRestored = False
    
    gbMASConn.Execute "Select 1 'Success'"
    
    'If we made it this far then the connection is good.
    MASConnectionRestored = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    Select Case Err.Number
        Case -2147467259
            'No swet... expected as much.  Just means the connection still lost
        Case Else
            'No swet... expected as much.  Just means the connection still lost
    End Select
    
    Err.Clear
    
'    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
End Function

Sub Main()
    gbLastActivityTime = Date + Time
    frmSplash.Show vbModal
    
'    INIFilePath = App.Path & "\ScalePass2009.ini"
'
'    Call GetSettings
'
'    Unload frmSplash
    
'    cnScalePass2009.Open Conn_String
    gbFirstRun = True
    frmOpenLoads.Show
    
    gbLastActivityTime = Date + Time
End Sub


'// INI CONTROLLING PROCEDURES
'reads an Ini string
Public Function ReadIni(Filename As String, Section As String, Key As String) As String
    Dim RetVal As String * 255, v As Long
    gbLastActivityTime = Date + Time
    v = GetPrivateProfileString(Section, Key, "", RetVal, 255, Filename)
    ReadIni = Left(RetVal, v)
'    If v > 0 Then
'        ReadIni = Left(RetVal, v - 1)
'    Else
'        ReadIni = Left(RetVal, v)
'    End If
    gbLastActivityTime = Date + Time
End Function

'reads an Ini section
Public Function ReadIniSection(Filename As String, Section As String) As String
    Dim RetVal As String * 255, v As Long
    gbLastActivityTime = Date + Time
    v = GetPrivateProfileSection(Section, RetVal, 255, Filename)
    If v > 0 Then
        ReadIniSection = Left(RetVal, v - 1)
    Else
        ReadIniSection = Left(RetVal, v)
    End If
    gbLastActivityTime = Date + Time
End Function

Function UpdateRS(ByRef RS As ADODB.Recordset, Optional SingleRecOnly As Boolean = False) As Boolean
    On Error GoTo Error
    Dim CurrentLine As Integer
    Dim lErrMsg As String
    Dim dDate As Date
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    dDate = Date + Time
    
    CurrentLine = 1
    If RS Is Nothing Then
        CurrentLine = 2
        UpdateRS = True
        CurrentLine = 3
        Exit Function
    End If
    
    CurrentLine = 4
    If RS.State <> 1 Then
        CurrentLine = 5
        UpdateRS = True
        CurrentLine = 6
        Exit Function
    End If
    
'    If Not RS.EOF And Not RS.BOF Then
'        Select Case RS.Status
'            Case RecordStatusEnum.adRecCanceled
'                Debug.Print "RS.Status = RecordStatusEnum.adRecCanceled = " & RecordStatusEnum.adRecCanceled
'            Case RecordStatusEnum.adRecCantRelease
'                Debug.Print "RS.Status = RecordStatusEnum.adRecCantRelease = " & RecordStatusEnum.adRecCantRelease; ""
'            Case RecordStatusEnum.adRecConcurrencyViolation
'                Debug.Print "RS.Status = RecordStatusEnum.adRecConcurrencyViolation = " & RecordStatusEnum.adRecConcurrencyViolation; ""
'            Case RecordStatusEnum.adRecDBDeleted
'                Debug.Print "RS.Status = RecordStatusEnum.adRecDBDeleted = " & RecordStatusEnum.adRecDBDeleted; ""
'            Case RecordStatusEnum.adRecDeleted
'                Debug.Print "RS.Status = RecordStatusEnum.adRecDeleted = " & RecordStatusEnum.adRecDeleted; ""
'            Case RecordStatusEnum.adRecIntegrityViolation
'                Debug.Print "RS.Status = RecordStatusEnum.adRecIntegrityViolation = " & RecordStatusEnum.adRecIntegrityViolation; ""
'            Case RecordStatusEnum.adRecInvalid
'                Debug.Print "RS.Status = RecordStatusEnum.adRecInvalid = " & RecordStatusEnum.adRecInvalid; ""
'            Case RecordStatusEnum.adRecMaxChangesExceeded
'                Debug.Print "RS.Status = RecordStatusEnum.adRecMaxChangesExceeded = " & RecordStatusEnum.adRecMaxChangesExceeded; ""
'            Case RecordStatusEnum.adRecModified
'                Debug.Print "RS.Status = RecordStatusEnum.adRecModified = " & RecordStatusEnum.adRecModified; ""
'            Case RecordStatusEnum.adRecMultipleChanges
'                Debug.Print "RS.Status = RecordStatusEnum.adRecMultipleChanges = " & RecordStatusEnum.adRecMultipleChanges; ""
'            Case RecordStatusEnum.adRecNew
'                Debug.Print "RS.Status = RecordStatusEnum.adRecNew = " & RecordStatusEnum.adRecNew; ""
'            Case RecordStatusEnum.adRecObjectOpen
'                Debug.Print "RS.Status = RecordStatusEnum.adRecObjectOpen = " & RecordStatusEnum.adRecObjectOpen; ""
'            Case RecordStatusEnum.adRecOK
'                Debug.Print "RS.Status = RecordStatusEnum.adRecOK = " & RecordStatusEnum.adRecOK; ""
'            Case RecordStatusEnum.adRecOutOfMemory
'                Debug.Print "RS.Status = RecordStatusEnum.adRecOutOfMemory = " & RecordStatusEnum.adRecOutOfMemory; ""
'            Case RecordStatusEnum.adRecPendingChanges
'                Debug.Print "RS.Status = RecordStatusEnum.adRecPendingChanges = " & RecordStatusEnum.adRecPendingChanges; ""
'            Case RecordStatusEnum.adRecPermissionDenied
'                Debug.Print "RS.Status = RecordStatusEnum.adRecPermissionDenied = " & RecordStatusEnum.adRecPermissionDenied; ""
'            Case RecordStatusEnum.adRecSchemaViolation
'                Debug.Print "RS.Status = RecordStatusEnum.adRecSchemaViolation = " & RecordStatusEnum.adRecSchemaViolation; ""
'            Case RecordStatusEnum.adRecUnmodified
'                Debug.Print "RS.Status = RecordStatusEnum.adRecUnmodified = " & RecordStatusEnum.adRecUnmodified; ""
'            Case Else
'                Debug.Print "RS.Status = RecordStatusEnum.???? = " & RS.Status
'        End Select
'    End If
    
'Debug.Print "RS.Status = " & RS.Status & " If this is 2050 this means error (modified and concurrency problem) "
'
'If RS.ActiveConnection Is Nothing Then
'    Debug.Print "No active connection"
'ElseIf RS.ActiveConnection Is gbScaleConn Then
'    Debug.Print "No active connection"
'Else
'    Debug.Print RS.ActiveConnection.ConnectionString
'End If


    CurrentLine = 7
    If SingleRecOnly = True Then
        CurrentLine = 8
        RS.Update
    Else
        CurrentLine = 9
        RS.UpdateBatch
    End If
    
    CurrentLine = 10
    RS.Resync
    
    CurrentLine = 11
    UpdateRS = True
    
    CurrentLine = 12
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    
    lErrMsg = dDate & vbCrLf & "basMain.UpdateRS(RS As ADODB.Recordset) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    basMain.WriteToLogFile lErrMsg
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18

'    If Err.Number = -2147467259 Or Err.Number = -2147217864 Then
        'Bug Resolution... Don't know why but RS.Update does the trick vs RS.UpdateBatch
'        RS.Update
'        Resume Next
        If SingleRecOnly = False Then
            If UpdateRS(RS, True) = False Then
                MsgBox lErrMsg, vbCritical, "Error Saving Data"
                
                Err.Clear
                
                RS.Resync
            Else
                UpdateRS = True
                RS.Resync
            End If
        Else
            MsgBox lErrMsg, vbCritical, "Error Saving Data"
            
            Err.Clear
            RS.Resync
        End If
        
'    Else
'        MsgBox "basMain.UpdateRS(RS As ADODB.Recordset) As Boolean" & vbCrLf & _
'        "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error Saving Data"
'        RS.Resync
'        Err.Clear
'    End If
    
    gbLastActivityTime = Date + Time
End Function

'writes an Ini string
Public Sub WriteIni(Filename As String, Section As String, Key As String, Value As String)
    gbLastActivityTime = Date + Time
    WritePrivateProfileString Section, Key, Value, Filename
    gbLastActivityTime = Date + Time
End Sub

'writes an Ini section
Public Sub WriteIniSection(Filename As String, Section As String, Value As String)
    gbLastActivityTime = Date + Time
    WritePrivateProfileSection Section, Value, Filename
    gbLastActivityTime = Date + Time
End Sub

Public Function Encrypt(Val As String) As String
    On Error GoTo Error
    
    Dim i As Long
    Dim str As String
    Dim NewVal As Long
    Dim EncryptedVal As String
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    For i = 1 To Len(Val)
        str = Mid(Val, i, 1)
        
        NewVal = 1000 + Asc(str)
        EncryptedVal = EncryptedVal & NewVal
    Next
    
    Encrypt = EncryptedVal
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMain.Encrypt(Val As String) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
End Function

Public Function Decrypt(Val As String) As String
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    Dim i As Long
    Dim str As String
    Dim NewVal As Variant
    Dim DecryptedVal As String
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    For i = 1 To Len(Val)
        str = Mid(Val, i, 4)
        
        If IsNumeric(str) Then
            str = str - 1000
        End If
        
        i = i + 3
        
        NewVal = Chr(CLng(str))
        DecryptedVal = DecryptedVal & NewVal
        
    Next
    
    Decrypt = DecryptedVal
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMain.Decrypt(Val As String) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
End Function

Public Sub SaveSettings()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    'Save Settings to System Registry
    SaveSetting App.Title, "Properties", "SetupPasswords", Encrypt(SetupPWD)
    
    SaveSetting App.Title, "ScaleDatabase", "ConnStr", Encrypt(gbScaleConnStr)
    SaveSetting App.Title, "ScaleDatabase", "UID", Encrypt(gbScaleUID)
    SaveSetting App.Title, "ScaleDatabase", "PWD", Encrypt(gbScalePWD)
    
    SaveSetting App.Title, "McLeodDatabase", "ConnStr", Encrypt(gbMcLeodConnStr)
    SaveSetting App.Title, "McLeodDatabase", "UID", Encrypt(gbMcLeodUID)
    SaveSetting App.Title, "McLeodDatabase", "PWD", Encrypt(gbMcLeodPWD)
    
    SaveSetting App.Title, "MASDatabase", "ConnStr", Encrypt(gbMASConnStr)
    SaveSetting App.Title, "MASDatabase", "UID", Encrypt(gbMASUID)
    SaveSetting App.Title, "MASDatabase", "PWD", Encrypt(gbMASPWD)
    
    
    'Write to the ini file
    WriteIni INIFilePath, "Miscellaneous", "Plant", gbPlant
    WriteIni INIFilePath, "Miscellaneous", "PlantPrefix", gbPlantPrefix
    WriteIni INIFilePath, "Miscellaneous", "MODE", gbMODE
'    WriteIni INIFilePath, "Miscellaneous", "DAYSTOKEEP", CStr(gbDAYSTOKEEP)
    WriteIni INIFilePath, "Miscellaneous", "MANUALSCALING", CStr(gbMANUALSCALING)
    
    WriteIni INIFilePath, "Miscellaneous", "AutoRtnOpenLoads", CStr(gbAutoRtnOpenLoads)
    WriteIni INIFilePath, "Miscellaneous", "AutoRtrnSecnds", CStr(gbAutoRtrnSecnds)
    
    WriteIni INIFilePath, "Miscellaneous", "EnforceCOA", CStr(gbEnforceCOA)
    WriteIni INIFilePath, "Miscellaneous", "RequireLotNbr", CStr(gbRequireLotNbr)
    
    WriteIni INIFilePath, "Miscellaneous", "PrintExtraBOL", CStr(gbPrintExtraBOL) 'RKL DEJ 9/25/13
    WriteIni INIFilePath, "Miscellaneous", "PrintExtraBOLCOAOnly", CStr(gbPrintExtraBOLCOAOnly) 'RKL DEJ 9/25/13
    WriteIni INIFilePath, "Miscellaneous", "PrintExtraCOA", CStr(gbPrintExtraCOA) 'RKL DEJ 9/25/13
    WriteIni INIFilePath, "Miscellaneous", "PrintExtraLabel", CStr(gbPrintExtraLblCert) 'RKL DEJ 9/25/13
    
    WriteIni INIFilePath, "Miscellaneous", "PrintExtraLblRisk", CStr(gbPrintExtraLblRisk) 'RKL DEJ 9/25/13
    WriteIni INIFilePath, "Miscellaneous", "PrintExtraLblWY", CStr(gbPrintExtraLblWY) 'RKL DEJ 9/25/13
    
    WriteIni INIFilePath, "Miscellaneous", "HideOverShip", CStr(gbHideOverShip) 'RKL DEJ 6/12/14
    WriteIni INIFilePath, "Miscellaneous", "ShowLMEMsg", CStr(gbShowLMEMsg) 'RKL DEJ 01/07/15
    WriteIni INIFilePath, "Miscellaneous", "AllowGetLMEMsg", CStr(gbAllowGetLMEMsg) 'RKL DEJ 01/07/15
    WriteIni INIFilePath, "Miscellaneous", "DriverOn", CStr(gbDriverOn) 'RKL DEJ 12/17/14
    
    WriteIni INIFilePath, "Miscellaneous", "CustOwnedProd", CStr(gbCustOwnedProd) 'RKL DEJ 2017-12-04
    WriteIni INIFilePath, "Miscellaneous", "AutoPostIMTrans", CStr(gbAutoPostIMTrans) 'RKL DEJ 2017-12-04
    WriteIni INIFilePath, "Miscellaneous", "NotAllowSplitLoads", CStr(gbNotAllowSplitLoads) 'RKL DEJ 2017-12-04
    WriteIni INIFilePath, "Miscellaneous", "SrcVariance", CStr(gbSrcVariance) 'RKL DEJ 2017-12-04
    WriteIni INIFilePath, "Miscellaneous", "IMTranReasonCode", CStr(gbIMTranReasonCode) 'RKL DEJ 2017-12-04
    WriteIni INIFilePath, "Miscellaneous", "ForceVarianceCheck", CStr(gbForceVarianceCheck) 'RKL DEJ 2017-12-04
    
    WriteIni INIFilePath, "Warnings", "StdTon", CStr(gbStdTon) 'RKL DEJ 04/23/15
    WriteIni INIFilePath, "Warnings", "MaxTonVar", CStr(gbMaxTonVar) 'RKL DEJ 04/23/15
    WriteIni INIFilePath, "Warnings", "ScaleInWarnTons", CStr(gbScaleInWarnTons) 'RKL DEJ 04/23/15
    WriteIni INIFilePath, "Warnings", "ScaleOutWarnVar", CStr(gbScaleOutWarnVar) 'RKL DEJ 04/23/15
    WriteIni INIFilePath, "Warnings", "ProcessBOLWarnVar", CStr(gbProcessBOLWarnVar) 'RKL DEJ 04/23/15

    WriteIni INIFilePath, "Warnings", "PrintPASSPatent", CStr(gbPrintPASSPatent) 'RKL DEJ 2016-05-03
    WriteIni INIFilePath, "Warnings", "PASSPatent", CStr(gbPASSPatent) 'RKL DEJ 2016-05-03
    WriteIni INIFilePath, "Warnings", "BOLAddrPhone", CStr(gbBOLAddrPhone) 'RKL DEJ 2016-05-03
    
    WriteIni INIFilePath, "Printing", "SCALEPASSPRINTER", gbSCALEPASSPRINTER
    WriteIni INIFilePath, "Printing", "BLPRINTER", gbBLPRINTER
    WriteIni INIFilePath, "Printing", "ALTERNATEPRINTER", gbALTERNATEPRINTER
    WriteIni INIFilePath, "Printing", "BOLCypressPRINTER", gbBOLCypressPRINTER
    WriteIni INIFilePath, "Printing", "PrintBOLCypress", CStr(gbPrintBOLCypress)
    WriteIni INIFilePath, "Printing", "PrintCOACypress", CStr(gbPrintCOACypress)
    WriteIni INIFilePath, "Printing", "LablePrinter", CStr(gbLablePrinter)          'RKL DEJ 9/25/13 Added
    
    WriteIni INIFilePath, "Printing", "ExtraBOLPrinter", CStr(gbExtraBOLPrinter)          'RKL DEJ 9/25/13 Added
    WriteIni INIFilePath, "Printing", "ExtraCOAPrinter", CStr(gbExtraCOAPrinter)          'RKL DEJ 9/25/13 Added
    WriteIni INIFilePath, "Printing", "ExtraLabelPrinter", CStr(gbExtraLabelPrinter)          'RKL DEJ 9/25/13 Added
    WriteIni INIFilePath, "Printing", "AltBOLPrinter", CStr(gbAltBOLPrinter)          'RKL DEJ 2016-06-11 Added

    WriteIni INIFilePath, "Printing", "NbrOfCOACopies", CStr(gbNbrOfCOACopies)      'RKL DEJ 9/25/13 Added
    WriteIni INIFilePath, "Printing", "PrintLabels", CStr(gbPrintLabels)            'RKL DEJ 9/25/13 Added
    WriteIni INIFilePath, "Printing", "PrintLabelAfterBOL", CStr(gbPrintLabelAfterBOL)            'RKL DEJ 9/25/13 Added
    WriteIni INIFilePath, "Printing", "COAPrinter", CStr(gbCOAPrinter)            'RKL DEJ 9/25/13 Added
    
    
    WriteIni INIFilePath, "ScaleDatabase", "ConnStr", gbScaleConnStr
    WriteIni INIFilePath, "ScaleDatabase", "DSN", gbScaleDSN
    WriteIni INIFilePath, "ScaleDatabase", "UID", gbScaleUID
    WriteIni INIFilePath, "ScaleDatabase", "PWD", gbScalePWD
    WriteIni INIFilePath, "ScaleDatabase", "Database", gbScaleDB
    WriteIni INIFilePath, "ScaleDatabase", "Driver", gbScaleDriver
    WriteIni INIFilePath, "ScaleDatabase", "Server", gbScaleServer
    WriteIni INIFilePath, "ScaleDatabase", "AdditionalProperties", gbScaleAdlPrty
    WriteIni INIFilePath, "ScaleDatabase", "WindowsAuthentication", CStr(gbScaleWinAuth)
    
    WriteIni INIFilePath, "McLeodDatabase", "ConnStr", gbMcLeodConnStr
    WriteIni INIFilePath, "McLeodDatabase", "DSN", gbMcLeodDSN
    WriteIni INIFilePath, "McLeodDatabase", "UID", gbMcLeodUID
    WriteIni INIFilePath, "McLeodDatabase", "PWD", gbMcLeodPWD
    WriteIni INIFilePath, "McLeodDatabase", "Database", gbMcLeodDB
    WriteIni INIFilePath, "McLeodDatabase", "Driver", gbMcLeodDriver
    WriteIni INIFilePath, "McLeodDatabase", "Server", gbMcLeodServer
    WriteIni INIFilePath, "McLeodDatabase", "AdditionalProperties", gbMcLeodAdlPrty
    WriteIni INIFilePath, "McLeodDatabase", "WindowsAuthentication", CStr(gbMcLeodWinAuth)
    
    WriteIni INIFilePath, "MASDatabase", "ConnStr", gbMASConnStr
    WriteIni INIFilePath, "MASDatabase", "DSN", gbMASDSN
    WriteIni INIFilePath, "MASDatabase", "UID", gbMASUID
    WriteIni INIFilePath, "MASDatabase", "PWD", gbMASPWD
    WriteIni INIFilePath, "MASDatabase", "Database", gbMASDB
    WriteIni INIFilePath, "MASDatabase", "Driver", gbMASDriver
    WriteIni INIFilePath, "MASDatabase", "Server", gbMASServer
    WriteIni INIFilePath, "MASDatabase", "AdditionalProperties", gbMASAdlPrty
    WriteIni INIFilePath, "MASDatabase", "WindowsAuthentication", CStr(gbMASWinAuth)
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Public Sub GetSettings()
    On Error Resume Next
    Dim lAutoRtrnSecnds As String
    Dim lcNbrOfCOACopies As String
    Dim RtnVal As String    'RKL DEJ 4/23/15
    
    gbLastActivityTime = Date + Time
    
    'Read Settings From System Registry
    SetupPWD = Decrypt(GetSetting(App.Title, "SetupPasswords", "SetupPWD", Empty))
    
'RKL DEJ 9/25/13 (START) Move all settings from the registry to the ini file
'    gbScaleConnStr = Decrypt(GetSetting(App.Title, "ScaleDatabase", "ConnStr", Empty))
'    gbScaleUID = Decrypt(GetSetting(App.Title, "ScaleDatabase", "UID", Empty))
'    gbScalePWD = Decrypt(GetSetting(App.Title, "ScaleDatabase", "PWD", Empty))
'
'    gbMcLeodConnStr = Decrypt(GetSetting(App.Title, "McLeodDatabase", "ConnStr", Empty))
'    gbMcLeodUID = Decrypt(GetSetting(App.Title, "McLeodDatabase", "UID", Empty))
'    gbMcLeodPWD = Decrypt(GetSetting(App.Title, "McLeodDatabase", "PWD", Empty))
'
'    gbMASConnStr = Decrypt(GetSetting(App.Title, "MASDatabase", "ConnStr", Empty))
'    gbMASUID = Decrypt(GetSetting(App.Title, "MASDatabase", "UID", Empty))
'    gbMASPWD = Decrypt(GetSetting(App.Title, "MASDatabase", "PWD", Empty))
'RKL DEJ 9/25/13 (STOP) Move all settings from the registry to the ini file
    
    
    
    'read the ini file
    gbPlant = ReadIni(INIFilePath, "Miscellaneous", "Plant")
    gbPlantPrefix = ReadIni(INIFilePath, "Miscellaneous", "PlantPrefix")
    gbMODE = ReadIni(INIFilePath, "Miscellaneous", "MODE")
'    gbDAYSTOKEEP = CInt(ReadIni(INIFilePath, "Miscellaneous", "DAYSTOKEEP"))
    gbMANUALSCALING = CBool(ReadIni(INIFilePath, "Miscellaneous", "MANUALSCALING"))
    
    gbAutoRtnOpenLoads = CBool(ReadIni(INIFilePath, "Miscellaneous", "AutoRtnOpenLoads"))
    lAutoRtrnSecnds = ReadIni(INIFilePath, "Miscellaneous", "AutoRtrnSecnds")
    If Trim(lAutoRtrnSecnds) = Empty Then
        gbAutoRtrnSecnds = 60
    ElseIf IsNumeric(lAutoRtrnSecnds) Then
        gbAutoRtrnSecnds = CInt(lAutoRtrnSecnds)
    Else
        gbAutoRtrnSecnds = CInt(lAutoRtrnSecnds)
    End If
    
    gbEnforceCOA = CBool(ReadIni(INIFilePath, "Miscellaneous", "EnforceCOA"))
    gbRequireLotNbr = CBool(ReadIni(INIFilePath, "Miscellaneous", "RequireLotNbr"))
    
    gbPrintExtraBOL = CBool(ReadIni(INIFilePath, "Miscellaneous", "PrintExtraBOL"))   'RKL DEJ 9/25/13
    gbPrintExtraBOLCOAOnly = CBool(ReadIni(INIFilePath, "Miscellaneous", "PrintExtraBOLCOAOnly"))   'RKL DEJ 9/25/13
    gbPrintExtraCOA = CBool(ReadIni(INIFilePath, "Miscellaneous", "PrintExtraCOA"))   'RKL DEJ 9/25/13
    gbPrintExtraLblCert = CBool(ReadIni(INIFilePath, "Miscellaneous", "PrintExtraLabel"))   'RKL DEJ 9/25/13
    gbPrintExtraLblRisk = CBool(ReadIni(INIFilePath, "Miscellaneous", "PrintExtraLblRisk"))   'RKL DEJ 9/25/13
    gbPrintExtraLblWY = CBool(ReadIni(INIFilePath, "Miscellaneous", "PrintExtraLblWY"))   'RKL DEJ 9/25/13
    
    gbHideOverShip = CBool(ReadIni(INIFilePath, "Miscellaneous", "HideOverShip"))   'RKL DEJ 6/12/14
    gbShowLMEMsg = CBool(ReadIni(INIFilePath, "Miscellaneous", "ShowLMEMsg"))   'RKL DEJ 01/07/15
    gbAllowGetLMEMsg = CBool(ReadIni(INIFilePath, "Miscellaneous", "AllowGetLMEMsg"))   'RKL DEJ 01/07/15
    gbDriverOn = CBool(ReadIni(INIFilePath, "Miscellaneous", "DriverOn"))   'RKL DEJ 12/17/14
    
    gbCustOwnedProd = CBool(ReadIni(INIFilePath, "Miscellaneous", "CustOwnedProd"))   'RKL DEJ 2017-12-04
    gbAutoPostIMTrans = CBool(ReadIni(INIFilePath, "Miscellaneous", "AutoPostIMTrans"))   'RKL DEJ 2017-12-04
    gbNotAllowSplitLoads = CBool(ReadIni(INIFilePath, "Miscellaneous", "NotAllowSplitLoads"))   'RKL DEJ 2017-12-04
    gbForceVarianceCheck = CBool(ReadIni(INIFilePath, "Miscellaneous", "ForceVarianceCheck"))   'RKL DEJ 2017-12-04
    RtnVal = CStr(ReadIni(INIFilePath, "Miscellaneous", "SrcVariance"))   'RKL DEJ 2017-12-04
    If Trim(RtnVal) <> Empty Then
        If IsNumeric(RtnVal) = True Then
            gbSrcVariance = CDbl(RtnVal)
        Else
            gbSrcVariance = 0   'Default to 0
        End If
    Else
        gbSrcVariance = 0   'Default to 0
    End If
    RtnVal = Empty
    gbIMTranReasonCode = CStr(ReadIni(INIFilePath, "Miscellaneous", "IMTranReasonCode"))   'RKL DEJ 2017-12-04
    
    
    'RKL DEJ 04/23/15 (START)
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "StdTon"))
        If Trim(RtnVal) <> Empty Then
            If IsNumeric(RtnVal) = True Then
                gbStdTon = CInt(RtnVal)
            Else
                gbStdTon = 33   'Default to 33
            End If
        Else
            gbStdTon = 33   'Default to 33
        End If
    RtnVal = Empty
    
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "MaxTonVar"))
        If Trim(RtnVal) <> Empty Then
            If IsNumeric(RtnVal) = True Then
                gbMaxTonVar = CInt(RtnVal)
            Else
                gbMaxTonVar = 2   'Default to 2
            End If
        Else
            gbMaxTonVar = 2   'Default to 2
        End If
    RtnVal = Empty
    
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "ScaleInWarnTons"))
        If Trim(RtnVal) <> Empty Then
            gbScaleInWarnTons = CBool(RtnVal)
        Else
            gbScaleInWarnTons = True   'Default to True
        End If
    RtnVal = Empty
    
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "ScaleOutWarnVar"))
        If Trim(RtnVal) <> Empty Then
            gbScaleOutWarnVar = CBool(RtnVal)
        Else
            gbScaleOutWarnVar = True   'Default to True
        End If
    RtnVal = Empty
    
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "ProcessBOLWarnVar"))
        If Trim(RtnVal) <> Empty Then
            gbProcessBOLWarnVar = CBool(RtnVal)
        Else
            gbProcessBOLWarnVar = True   'Default to True
        End If
    RtnVal = Empty
    'RKL DEJ 04/23/15 (STOP)
    
    'RKL DEJ 2016-05-03 (START)
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "PrintPASSPatent"))
        If Trim(RtnVal) <> Empty Then
            gbPrintPASSPatent = CBool(RtnVal)
        Else
            gbPrintPASSPatent = False   'Default to False
        End If
    RtnVal = Empty
    
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "PASSPatent"))
    gbPASSPatent = Trim(RtnVal)
    RtnVal = Empty
    
    RtnVal = CStr(ReadIni(INIFilePath, "Warnings", "BOLAddrPhone"))
    gbBOLAddrPhone = Trim(RtnVal)
    RtnVal = Empty
    'RKL DEJ 2016-05-03 (STOP)
    
    
    gbSCALEPASSPRINTER = ReadIni(INIFilePath, "Printing", "SCALEPASSPRINTER")
    gbBLPRINTER = ReadIni(INIFilePath, "Printing", "BLPRINTER")
    gbALTERNATEPRINTER = ReadIni(INIFilePath, "Printing", "ALTERNATEPRINTER")
    gbBOLCypressPRINTER = ReadIni(INIFilePath, "Printing", "BOLCypressPRINTER")
    gbPrintBOLCypress = CBool(ReadIni(INIFilePath, "Printing", "PrintBOLCypress"))
    gbPrintCOACypress = CBool(ReadIni(INIFilePath, "Printing", "PrintCOACypress"))
    
    'RKL DEJ 9/25/13 (Start)
    gbCOAPrinter = ReadIni(INIFilePath, "Printing", "COAPrinter")
    gbLablePrinter = ReadIni(INIFilePath, "Printing", "LablePrinter")
    
    gbExtraBOLPrinter = ReadIni(INIFilePath, "Printing", "ExtraBOLPrinter")
    gbExtraCOAPrinter = ReadIni(INIFilePath, "Printing", "ExtraCOAPrinter")
    gbExtraLabelPrinter = ReadIni(INIFilePath, "Printing", "ExtraLabelPrinter")
    gbAltBOLPrinter = ReadIni(INIFilePath, "Printing", "AltBOLPrinter") 'RKL DEJ 2016-06-11
    
    
    gbPrintLabels = ReadIni(INIFilePath, "Printing", "PrintLabels")
    gbPrintLabelAfterBOL = ReadIni(INIFilePath, "Printing", "PrintLabelAfterBOL")

    lcNbrOfCOACopies = ReadIni(INIFilePath, "Printing", "NbrOfCOACopies")
    If IsNumeric(lcNbrOfCOACopies) Then
        gbNbrOfCOACopies = CInt(lcNbrOfCOACopies)
    Else
        gbNbrOfCOACopies = 2
    End If
    
    If gbNbrOfCOACopies < 1 Then
        gbNbrOfCOACopies = 2
    End If
    'RKL DEJ 9/25/13 (Stop)
    
    
    gbScaleConnStr = ReadIni(INIFilePath, "ScaleDatabase", "ConnStr")
    gbScaleDSN = ReadIni(INIFilePath, "ScaleDatabase", "DSN")
    gbScaleUID = ReadIni(INIFilePath, "ScaleDatabase", "UID")
    gbScalePWD = ReadIni(INIFilePath, "ScaleDatabase", "PWD")
    gbScaleDB = ReadIni(INIFilePath, "ScaleDatabase", "Database")
    gbScaleDriver = ReadIni(INIFilePath, "ScaleDatabase", "Driver")
    gbScaleServer = ReadIni(INIFilePath, "ScaleDatabase", "Server")
    gbScaleAdlPrty = ReadIni(INIFilePath, "ScaleDatabase", "AdditionalProperties")
    gbScaleWinAuth = CBool(ReadIni(INIFilePath, "ScaleDatabase", "WindowsAuthentication"))

    gbMcLeodConnStr = ReadIni(INIFilePath, "McLeodDatabase", "ConnStr")
    gbMcLeodDSN = ReadIni(INIFilePath, "McLeodDatabase", "DSN")
    gbMcLeodUID = ReadIni(INIFilePath, "McLeodDatabase", "UID")
    gbMcLeodPWD = ReadIni(INIFilePath, "McLeodDatabase", "PWD")
    gbMcLeodDB = ReadIni(INIFilePath, "McLeodDatabase", "Database")
    gbMcLeodDriver = ReadIni(INIFilePath, "McLeodDatabase", "Driver")
    gbMcLeodServer = ReadIni(INIFilePath, "McLeodDatabase", "Server")
    gbMcLeodAdlPrty = ReadIni(INIFilePath, "McLeodDatabase", "AdditionalProperties")
    gbMcLeodWinAuth = CBool(ReadIni(INIFilePath, "McLeodDatabase", "WindowsAuthentication"))

    gbMASConnStr = ReadIni(INIFilePath, "MASDatabase", "ConnStr")
    gbMASDSN = ReadIni(INIFilePath, "MASDatabase", "DSN")
    gbMASUID = ReadIni(INIFilePath, "MASDatabase", "UID")
    gbMASPWD = ReadIni(INIFilePath, "MASDatabase", "PWD")
    gbMASDB = ReadIni(INIFilePath, "MASDatabase", "Database")
    gbMASDriver = ReadIni(INIFilePath, "MASDatabase", "Driver")
    gbMASServer = ReadIni(INIFilePath, "MASDatabase", "Server")
    gbMASAdlPrty = ReadIni(INIFilePath, "MASDatabase", "AdditionalProperties")
    gbMASWinAuth = CBool(ReadIni(INIFilePath, "MASDatabase", "WindowsAuthentication"))

    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Function OpenScaleConn() As Boolean
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
    If gbScaleConn.State = 1 Then gbScaleConn.Close
    
    gbScaleConn.CursorLocation = adUseClient
    gbScaleConn.IsolationLevel = adXactBrowse
    
    If Trim(gbScaleConnStr) = Empty Then Exit Function
    
    DoEvents
    
    gbScaleConn.ConnectionString = gbScaleConnStr
    
    DoEvents
    
    gbScaleConn.Open
    
    If gbScaleConn.State = 1 Then
        OpenScaleConn = True
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "OpenScaleConn()" & vbCrLf & _
    "The following error occurred while connecting to the Scale Pass database: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Error"

    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
End Function

Function OpenMASConn(Optional UseAccess As Boolean = False) As Boolean
    On Error GoTo Error
    Dim ConnStr As String
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
    If gbMASConn.State = 1 Then gbMASConn.Close
    
    gbLocalMode = False
    
    gbMASConn.CursorLocation = adUseClient
    gbMASConn.IsolationLevel = adXactBrowse
    
    gbMASConn.CommandTimeout = 0
'    gbMASConn.ConnectionTimeout = 30
    
    If Trim(gbMASConnStr) = Empty Then
        If Trim(gbScaleConnStr) = Empty Then
            Exit Function
        Else
            ConnStr = gbScaleConnStr
            gbLocalMode = True
        End If
    Else
        If UseAccess = True And Trim(gbScaleConnStr) <> Empty Then
            ConnStr = gbScaleConnStr
            gbLocalMode = True
        Else
            ConnStr = gbMASConnStr
        End If
    End If
        
TryConnection:
    DoEvents
    
    gbMASConn.ConnectionString = ConnStr
    
    DoEvents
    
    gbMASConn.Open
    
    DoEvents
    
    If gbMASConn.State = 1 Then
        OpenMASConn = True
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    If ConnStr = gbMASConnStr And Trim(gbScaleConnStr) <> Empty And gbMASConnStr <> gbScaleConnStr Then
        Err.Clear
        DoEvents
        On Error GoTo Error
        ConnStr = gbScaleConnStr
        gbLocalMode = True
        GoTo TryConnection
    End If
    
    lErrMsg = "OpenMASConn()" & vbCrLf & _
    "The following error occurred while connecting to the MAS 500 database: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time

End Function


Function ConvertToDouble(Val As Variant) As Double
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
    If IsNumeric(Val) Then
        ConvertToDouble = CDbl(Val)
    Else
        ConvertToDouble = 0
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMain.ConvertToDouble(Val As Variant) As Double" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
End Function



Public Function ConvertToString(Val As Variant) As String
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time

'    Debug.Print TypeName(Val)
    
    Select Case VarType(Val)
        Case VbVarType.vbArray
            ConvertToString = ConvertToString(Val(0))
        
        Case VbVarType.vbBoolean
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbByte
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbCurrency
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDataObject
            ConvertToString = Empty
        
        Case VbVarType.vbDate
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDecimal
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDouble
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbEmpty
            ConvertToString = Empty
        
        Case VbVarType.vbError
            ConvertToString = Val.Number & " " & Val.Description
        
        Case VbVarType.vbInteger
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbLong
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbNull
            ConvertToString = Empty
        
        Case VbVarType.vbObject
            ConvertToString = Empty
            
        Case VbVarType.vbSingle
            ConvertToString = Empty
        
        Case VbVarType.vbString
            ConvertToString = Val
        
        Case VbVarType.vbUserDefinedType
            ConvertToString = Empty
        
        Case VbVarType.vbVariant
            ConvertToString = CStr(Val)
        
        Case Else
            If IsArray(Val) = True Then
                ConvertToString = ConvertToString(Val(0))
            Else
                ConvertToString = Empty
            End If
    
    End Select
        
    gbLastActivityTime = Date + Time
    
    Exit Function
Error:
    lErrMsg = "basMain.ConvertToString(Val As Variant) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
End Function

Public Function ConvertToDate(Val As Variant) As String
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
    If IsNull(Val) = True Then
        ConvertToDate = Date
        Exit Function
    End If
    
    If IsDate(Val) = True Then
        ConvertToDate = CDate(Val)
    Else
        ConvertToDate = Date
    End If
    
        
    gbLastActivityTime = Date + Time
    
    Exit Function
Error:
    lErrMsg = "basMain.ConvertToDate(Val As Variant) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
End Function


'SGS DEJ 7/06/2010 Added for debugging/trouble shooting purposes
Public Function WriteToLogFile(ErrMsg As String) As Boolean
    On Error GoTo Error
    Dim FSO As New Scripting.FileSystemObject
    Dim TS As Scripting.TextStream
    Dim sPath As String
    Dim ErrDate As Date
    
    gbLastActivityTime = Date + Time
    
    sPath = App.Path & "\ScalePassErrorLog.log"
    ErrDate = Date + Time
    
    Set TS = FSO.OpenTextFile(sPath, ForAppending, True, TristateMixed)
    
    TS.WriteLine Replace(Space(60), " ", "*")
    TS.WriteLine Replace(Space(10), " ", "*") & vbTab & ErrDate & " START" & vbTab & Replace(Space(12), " ", "*")
    TS.WriteLine Replace(Space(60), " ", "*")
        
    TS.Write ErrMsg & vbCrLf
    
    TS.WriteLine Replace(Space(60), " ", "*")
    TS.WriteLine Replace(Space(10), " ", "*") & vbTab & ErrDate & " END" & vbTab & Replace(Space(20), " ", "*")
    TS.WriteLine Replace(Space(60), " ", "*")
    TS.WriteLine Empty
    
    
CleanUP:
    TS.Close
    Set TS = Nothing
    Set FSO = Nothing
    
    gbLastActivityTime = Date + Time
    
    Exit Function
Error:
    MsgBox "basMain.WriteToLogFile(ErrMsg As String) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function


'SGS DEJ 2/8/11 added
Public Function CertCount() As Long
    On Error GoTo Error
    
CertCount = 0
Exit Function
    
    Dim lSQL As String
    Dim lRS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    lSQL = "Select count(*) as cnt from BOLPrinting Where CertIsRequired = 1 and CertHasPrinted = 0 and HasPrintedSuccessfully = 1 "
    
    lRS.CursorLocation = adUseClient
    lRS.CursorType = adOpenDynamic
    
    lRS.Open lSQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    CertCount = CLng(lRS("cnt").Value)
    
    lRS.Close
    Set lRS = Nothing
    
    Exit Function
Error:
    lErrMsg = "basMain.CertCount()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
End Function


'RKL DEJ 2018-01-05 Added Method IsValidTank
Function IsValidTank(Tank As String) As Boolean
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    If gbMASConn.State <> 1 Then
        'We don't have connection to MAS 500 to skip this validation
        IsValidTank = True
        Exit Function
    End If
    
    
    'SQL = "Select * From vluGetWarehouseBin_SGS Where WhseBinID = '" & Replace(Trim(Tank), "'", "''") & "' And WhseID = '" & gbWhseID & "' "
    SQL = "Select * From vluGetWarehouseBin_SGS Where WhseBinID = '" & Trim(Tank) & "' And FacilitySeqNo = " & gbPlantPrefix & " And Coalesce(CustOwnedProd,0) = " & Int(Abs(gbCustOwnedProd))
    
    gbMASCompanyID = gbMASCompanyID
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenStatic
    
    RS.Open SQL, gbMASConn, adOpenStatic, adLockReadOnly
    
    If RS.State = 1 Then
        If Not RS.EOF And Not RS.BOF Then
            
            If RS.RecordCount >= 1 Then
                IsValidTank = True
            End If
        End If
    End If
    
CleanUP:
    On Error Resume Next
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMain.IsValidTank(Tank As String) As Boolean" & vbCrLf & _
    "Tank = " & Tank & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    GoTo CleanUP
End Function


Public Function Quoted(StrVal As String) As String
    On Error GoTo Error
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
    Quoted = "'" & Replace(StrVal, "'", "''") & "'"
    
    Exit Function
Error:
    lErrMsg = "basMain.Quoted(StrVal As String) As Boolean" & vbCrLf & _
    "StrVal = " & StrVal & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    gbLastActivityTime = Date + Time
    
End Function
