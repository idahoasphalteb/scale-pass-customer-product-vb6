VERSION 5.00
Begin VB.Form frmRecieve 
   BackColor       =   &H00FF00FF&
   Caption         =   "Recieve Load - Valero"
   ClientHeight    =   4875
   ClientLeft      =   2850
   ClientTop       =   3345
   ClientWidth     =   10410
   ControlBox      =   0   'False
   Icon            =   "frmRecieve.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4875
   ScaleWidth      =   10410
   Begin VB.Timer tmrAutoReturnNoActivity 
      Interval        =   10
      Left            =   0
      Top             =   720
   End
   Begin VB.Frame frameRearLoad 
      Caption         =   "Trailer Info"
      Height          =   2130
      Left            =   5280
      TabIndex        =   45
      Top             =   1680
      Width           =   4980
      Begin VB.Frame Frame7 
         Caption         =   "Invisible"
         Height          =   1575
         Left            =   2190
         TabIndex        =   50
         Top             =   270
         Visible         =   0   'False
         Width           =   2475
         Begin VB.TextBox txtRearFacilityID 
            DataField       =   "RearFacilityID"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   1080
            TabIndex        =   54
            TabStop         =   0   'False
            Text            =   "FacilityID"
            Top             =   210
            Width           =   855
         End
         Begin VB.TextBox txtRearProductID 
            DataField       =   "RearProductID"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   120
            TabIndex        =   53
            TabStop         =   0   'False
            Text            =   "ProductID"
            Top             =   210
            Width           =   855
         End
         Begin VB.TextBox txtRearDestDt 
            DataField       =   "RearDestDt"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   120
            TabIndex        =   52
            TabStop         =   0   'False
            Text            =   "Dest Dt"
            Top             =   1170
            Width           =   855
         End
         Begin VB.TextBox txtRearArrivalDt 
            DataField       =   "RearArrivalDt"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   120
            TabIndex        =   51
            TabStop         =   0   'False
            Text            =   "Arrival Dt"
            Top             =   840
            Width           =   855
         End
      End
      Begin VB.TextBox txtRearTare 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearTareWt"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   49
         TabStop         =   0   'False
         Top             =   360
         Width           =   1005
      End
      Begin VB.TextBox txtRearGross 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearGrossWt"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   48
         TabStop         =   0   'False
         Top             =   720
         Width           =   1005
      End
      Begin VB.TextBox txtRearNet 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearNetWt"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   47
         TabStop         =   0   'False
         Top             =   1080
         Width           =   1005
      End
      Begin VB.TextBox txtRearTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "RearTons"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1110
         Locked          =   -1  'True
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   1440
         Width           =   1005
      End
      Begin VB.Label Label27 
         Caption         =   "Tare"
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   405
         Width           =   855
      End
      Begin VB.Label Label26 
         Caption         =   "Gross"
         Height          =   255
         Left            =   120
         TabIndex        =   57
         Top             =   765
         Width           =   855
      End
      Begin VB.Label Label25 
         Caption         =   "Net"
         Height          =   255
         Left            =   120
         TabIndex        =   56
         Top             =   1125
         Width           =   855
      End
      Begin VB.Label Label24 
         Caption         =   "Tons"
         Height          =   255
         Left            =   120
         TabIndex        =   55
         Top             =   1485
         Width           =   855
      End
   End
   Begin VB.CommandButton cmdPrintWTTicket 
      Caption         =   "Print &W.T."
      Height          =   375
      Left            =   9240
      TabIndex        =   43
      Top             =   4320
      Width           =   1005
   End
   Begin VB.CheckBox chManualScale 
      Caption         =   "Manual Scaling ON"
      DataField       =   "ManualScaling"
      DataSource      =   "Adodc1"
      Height          =   285
      Left            =   120
      TabIndex        =   42
      Top             =   1320
      Width           =   1845
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Close"
      Height          =   375
      Left            =   8040
      TabIndex        =   41
      Top             =   4320
      Width           =   1005
   End
   Begin VB.TextBox txtTotalGrossWt 
      Alignment       =   1  'Right Justify
      BackColor       =   &H8000000F&
      DataField       =   "FrontGrossWt"
      DataSource      =   "Adodc1"
      Height          =   285
      Left            =   6165
      Locked          =   -1  'True
      TabIndex        =   40
      TabStop         =   0   'False
      Top             =   4365
      Width           =   1005
   End
   Begin VB.Frame Frame1 
      Caption         =   "Load Info"
      Height          =   2970
      Left            =   120
      TabIndex        =   17
      Top             =   1680
      Width           =   4980
      Begin VB.TextBox txtFrontTruck 
         DataField       =   "TruckNo"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1350
         TabIndex        =   31
         Top             =   315
         Width           =   1455
      End
      Begin VB.TextBox txtFrontTank 
         DataField       =   "FrontTankNbr"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1350
         TabIndex        =   30
         Top             =   975
         Width           =   1005
      End
      Begin VB.TextBox txtFrontProduct 
         DataField       =   "FrontProduct"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1350
         TabIndex        =   29
         Top             =   645
         Width           =   2295
      End
      Begin VB.TextBox txtFrontTare 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontTareWt"
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   28
         TabStop         =   0   'False
         Top             =   1320
         Width           =   1005
      End
      Begin VB.TextBox txtFrontGross 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontGrossWt"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   27
         TabStop         =   0   'False
         Top             =   1680
         Width           =   1005
      End
      Begin VB.TextBox txtFrontNet 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontNetWt"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   2040
         Width           =   1005
      End
      Begin VB.TextBox txtFrontTons 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000F&
         DataField       =   "FrontTons"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1350
         Locked          =   -1  'True
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   2400
         Width           =   1005
      End
      Begin VB.TextBox txtFrontRack 
         DataField       =   "FrontRackNbr"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   3600
         TabIndex        =   24
         Top             =   975
         Width           =   1005
      End
      Begin VB.Frame Frame6 
         Caption         =   "Invisible"
         Height          =   1545
         Left            =   2430
         TabIndex        =   19
         Top             =   1260
         Visible         =   0   'False
         Width           =   2475
         Begin VB.TextBox txtFrontArrivalDt 
            DataField       =   "FrontArrivalDt"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   150
            TabIndex        =   23
            TabStop         =   0   'False
            Text            =   "Arrival Dt"
            Top             =   840
            Width           =   855
         End
         Begin VB.TextBox txtFrontDestDt 
            DataField       =   "FrontDestDt"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   150
            TabIndex        =   22
            TabStop         =   0   'False
            Text            =   "Dest Dt"
            Top             =   1170
            Width           =   855
         End
         Begin VB.TextBox txtFrontProductID 
            DataField       =   "FrontProductID"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   150
            TabIndex        =   21
            TabStop         =   0   'False
            Text            =   "ProductID"
            Top             =   210
            Width           =   855
         End
         Begin VB.TextBox txtFrontFacilityID 
            DataField       =   "FrontFacilityID"
            DataSource      =   "Adodc1"
            Height          =   315
            Left            =   1110
            TabIndex        =   20
            TabStop         =   0   'False
            Text            =   "FacilityID"
            Top             =   210
            Width           =   855
         End
      End
      Begin VB.CommandButton cmdFrntSrchPrdct 
         Caption         =   "Search"
         Height          =   285
         Left            =   3720
         TabIndex        =   18
         ToolTipText     =   "Search for a Product"
         Top             =   675
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Truck #"
         Height          =   255
         Left            =   360
         TabIndex        =   39
         Top             =   345
         Width           =   855
      End
      Begin VB.Label Label7 
         Caption         =   "Tank #"
         Height          =   255
         Left            =   360
         TabIndex        =   38
         Top             =   1020
         Width           =   855
      End
      Begin VB.Label Label12 
         Caption         =   "Product"
         Height          =   255
         Left            =   360
         TabIndex        =   37
         Top             =   675
         Width           =   855
      End
      Begin VB.Label Label16 
         Caption         =   "Tare"
         Height          =   255
         Left            =   405
         TabIndex        =   36
         Top             =   1365
         Width           =   855
      End
      Begin VB.Label Label17 
         Caption         =   "Gross"
         Height          =   255
         Left            =   360
         TabIndex        =   35
         Top             =   1725
         Width           =   855
      End
      Begin VB.Label Label18 
         Caption         =   "Net"
         Height          =   255
         Left            =   360
         TabIndex        =   34
         Top             =   2085
         Width           =   855
      End
      Begin VB.Label Label19 
         Caption         =   "Tons"
         Height          =   255
         Left            =   360
         TabIndex        =   33
         Top             =   2445
         Width           =   855
      End
      Begin VB.Label Label45 
         Caption         =   "Rack #"
         Height          =   255
         Left            =   2880
         TabIndex        =   32
         Top             =   1020
         Width           =   585
      End
   End
   Begin VB.TextBox txtScaleMsg 
      BackColor       =   &H8000000F&
      Height          =   285
      Left            =   0
      Locked          =   -1  'True
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   10335
   End
   Begin VB.Timer tmrScale 
      Interval        =   5000
      Left            =   0
      Top             =   360
   End
   Begin VB.Frame Frame4 
      Caption         =   "General Info"
      Height          =   1080
      Left            =   6390
      TabIndex        =   5
      Top             =   90
      Width           =   3885
      Begin VB.TextBox txtOutboundTime 
         BackColor       =   &H8000000F&
         DataField       =   "TimeOut"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1740
         TabIndex        =   10
         Top             =   675
         Width           =   2010
      End
      Begin VB.TextBox txtInboundTime 
         BackColor       =   &H8000000F&
         DataField       =   "TimeIn"
         DataSource      =   "Adodc1"
         Height          =   285
         Left            =   1740
         TabIndex        =   9
         Top             =   285
         Width           =   2010
      End
      Begin VB.ComboBox cboOutboundOperator 
         DataField       =   "OutBoundOperator"
         DataSource      =   "Adodc1"
         Height          =   315
         Left            =   945
         TabIndex        =   2
         Top             =   675
         Width           =   750
      End
      Begin VB.ComboBox cboInboundOperator 
         DataField       =   "InboundOperator"
         DataSource      =   "Adodc1"
         Height          =   315
         Left            =   945
         TabIndex        =   1
         Top             =   285
         Width           =   750
      End
      Begin VB.Label Label9 
         Caption         =   "Inbound"
         Height          =   255
         Left            =   135
         TabIndex        =   7
         Top             =   330
         Width           =   765
      End
      Begin VB.Label Label8 
         Caption         =   "Outbound"
         Height          =   255
         Left            =   135
         TabIndex        =   6
         Top             =   720
         Width           =   810
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Scale Capture"
      Height          =   1080
      Left            =   135
      TabIndex        =   3
      Top             =   90
      Width           =   6180
      Begin VB.CheckBox chDriverOn 
         Caption         =   "Driver On"
         DataField       =   "DriverOn"
         DataSource      =   "Adodc1"
         Height          =   225
         Left            =   630
         TabIndex        =   15
         Top             =   810
         Width           =   1245
      End
      Begin VB.CommandButton cmdTareRear 
         Caption         =   "Tare (F5)"
         Height          =   330
         Left            =   4095
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   585
         Width           =   915
      End
      Begin VB.CommandButton cmdGrossRear 
         Caption         =   "Gross (F7)"
         Height          =   330
         Left            =   5085
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   585
         Width           =   915
      End
      Begin VB.CommandButton cmdTareFront 
         Caption         =   "Tare (F2)"
         Height          =   330
         Left            =   2025
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   585
         Width           =   915
      End
      Begin VB.CommandButton cmdGrossFront 
         Caption         =   "Gross (F4)"
         Height          =   330
         Left            =   3015
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   585
         Width           =   915
      End
      Begin VB.TextBox txtScale 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   21.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   525
         Left            =   180
         TabIndex        =   0
         Text            =   "0"
         Top             =   270
         Width           =   1770
      End
      Begin VB.Line Line1 
         X1              =   4095
         X2              =   4725
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Line Line8 
         X1              =   5355
         X2              =   5985
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label23 
         Alignment       =   2  'Center
         Caption         =   "Rear"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4770
         TabIndex        =   12
         Top             =   315
         Width           =   555
      End
      Begin VB.Line Line6 
         X1              =   3285
         X2              =   3915
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Line Line5 
         X1              =   2025
         X2              =   2655
         Y1              =   405
         Y2              =   405
      End
      Begin VB.Label Label22 
         Alignment       =   2  'Center
         Caption         =   "Front"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2700
         TabIndex        =   11
         Top             =   315
         Width           =   555
      End
   End
   Begin VB.Label Label50 
      Caption         =   "Tot. Gross"
      Height          =   255
      Left            =   5235
      TabIndex        =   44
      Top             =   4410
      Width           =   855
   End
   Begin VB.Menu mnuGetWeight 
      Caption         =   "Get Weight"
      Visible         =   0   'False
      Begin VB.Menu mnuFrontTare 
         Caption         =   "Front Tare"
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuFrontGross 
         Caption         =   "Front Gross"
         Shortcut        =   {F4}
      End
      Begin VB.Menu mnuRearTare 
         Caption         =   "Rear Tare"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuRearGross 
         Caption         =   "Rear Gross"
         Shortcut        =   {F7}
      End
   End
End
Attribute VB_Name = "frmRecieve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents rsLoad As ADODB.Recordset
Attribute rsLoad.VB_VarHelpID = -1
Dim WithEvents SGSScale As SGSScaleComm.clsScaleComm
Attribute SGSScale.VB_VarHelpID = -1


Private Sub chDriverOn_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chManualScale_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If chManualScale.Value = vbChecked Then
        txtScale.Locked = False
        txtScale.ForeColor = &H80000008
        txtScale.BackColor = &H80000005
        txtScale.TabStop = True
        SGSScale.ClosePort
    Else
        txtScale.Locked = True
        txtScale.ForeColor = &HFFFFFF
        txtScale.BackColor = &H0&
        txtScale.TabStop = False
        SGSScale.OpenPort
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chManualScale_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdFrntSrchPrdct_Click()
    On Error GoTo Error
    
    tmrAutoReturnNoActivity.Enabled = False
    gbLastActivityTime = Date + Time
    
    frmProductSearch.ItemID = ConvertToString(rsLoad("FrontProductID").Value)
    frmProductSearch.ItemDesc = ConvertToString(rsLoad("FrontProduct").Value)
    frmProductSearch.ItemClassID = ConvertToString(rsLoad("FrontProductClass").Value)
    
    Load frmProductSearch
    
    frmProductSearch.Show vbModal, Me
    
    rsLoad("FrontProductID").Value = ConvertToString(frmProductSearch.ItemID)
    rsLoad("FrontProduct").Value = ConvertToString(frmProductSearch.ItemDesc)
    rsLoad("FrontProductClass").Value = ConvertToString(frmProductSearch.ItemClassID)
    
    Unload frmProductSearch
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdFrntSrchPrdct_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
End Sub

Private Sub cmdFrntSrchPrdct_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdGrossFront_Click()
    gbLastActivityTime = Date + Time
    
    If Val(txtFrontGross.Text) > 0 Then
        gbResponse = MsgBox("Front Gross Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    End If
    txtFrontGross.Text = Val(txtScale.Text)
    txtOutboundTime.Text = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
    txtScale.Text = 0
    txtScale.SetFocus

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdGrossRear_Click()
    gbLastActivityTime = Date + Time
    
    If Val(txtRearGross.Text) > 0 Then
        gbResponse = MsgBox("Rear Gross Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    End If
    txtRearGross.Text = Val(txtScale.Text)
    txtScale.Text = 0
    txtScale.SetFocus

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdGrossRear_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdPrintWTTicket_Click()
    On Error GoTo Error
    
    Dim wt As WeightTicket
    Dim SQL As String
    
    tmrAutoReturnNoActivity.Enabled = False
    gbLastActivityTime = Date + Time
    
    rsLoad.UpdateBatch
    
    If chDriverOn.Value = vbChecked Then
        wt.DriverOn = True
    Else
        wt.DriverOn = False
    End If
    
    wt.GrossFront = ConvertToDouble(txtFrontGross.Text)
    wt.GrossRear = ConvertToDouble(txtRearGross.Text)
    wt.GrossTotal = wt.GrossFront + wt.GrossRear
    
    wt.InBoundOpr = ConvertToString(cboInboundOperator.Text)
    wt.OutBoundOpr = ConvertToString(cboOutboundOperator.Text)
    
    If chManualScale.Value = vbChecked Then
        wt.ManualScaleing = True
    Else
        wt.ManualScaleing = False
    End If
    
    wt.NetFront = ConvertToDouble(txtFrontNet.Text)
    wt.NetRear = ConvertToDouble(txtRearNet.Text)
    wt.NetTotal = wt.NetFront + wt.NetRear
    
    wt.TareFront = ConvertToDouble(txtFrontTare.Text)
    wt.TareRear = ConvertToDouble(txtRearTare.Text)
    wt.TareTotal = wt.TareFront + wt.TareRear
    
    wt.TicketNo = Empty     'No Ticket Numbers....
    
    wt.TruckNo = Trim(txtFrontTruck.Text)
    
    wt.Product = Trim(txtFrontProduct.Text)
    wt.Tank = Trim(txtFrontTank.Text)
    
    If PrintWeightTicket(wt, 2) = True Then
        If wt.TareFront > 0 And wt.GrossFront > 0 Then
            If MsgBox("Ticket has been sent to the printer." & vbCrLf & vbCrLf & "Do you want to remove this load from the Open Loads list?", vbYesNo, "Close Load?") = vbYes Then
                'Update the Load Data
                rsLoad("LoadIsOpen").Value = 2
                rsLoad.UpdateBatch
                
                gbLastActivityTime = Date + Time
                If gbAutoRtnOpenLoads = True Then
                    tmrAutoReturnNoActivity.Enabled = True
                End If
                
                Unload Me
                
                gbLastActivityTime = Date + Time
                
                Exit Sub
            End If
        Else
            MsgBox "Ticket has been sent to the printer.", vbInformation, "Ticket Printed"
        End If
    Else
        MsgBox "There was an error printing the Ticket.", vbExclamation, "Not Printed"
    End If
    
    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdPrintWTTicket_Click()" & vbCrLf & "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear

    gbLastActivityTime = Date + Time
    tmrAutoReturnNoActivity.Enabled = True
End Sub


Private Sub cmdPrintWTTicket_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdSave_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
    End If
    DoEvents
    Unload Me
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSave_Click" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    Unload Me

    gbLastActivityTime = Date + Time
End Sub



Function GetLoad() As Boolean
    On Error GoTo Error
    Dim SQL As String
    
    gbLastActivityTime = Date + Time
    
    SQL = "Select * From Loads Where LoadsTableKey = " & gbLoadsTableKey  'LoadIsOpen = 1"
    
    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
        rsLoad.Close
    End If
    
    rsLoad.CursorLocation = adUseClient
    rsLoad.CursorType = adOpenDynamic
    
'    Set rsLoad = gbScaleConn.Execute(SQL)
    rsLoad.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
        
    If rsLoad.State = 1 Then
        GetLoad = True
        
        
        If rsLoad.EOF Or rsLoad.BOF Then
            rsLoad.AddNew
            'Load Defaults
'            rsLoad("TimeIn").Value = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
            rsLoad("DriverOn").Value = 0
            rsLoad("Laps").Value = 0
            rsLoad("SplitLoad").Value = 0
            rsLoad("SelectForWeightTargets").Value = 0
            rsLoad("FrontTargetGrossWt").Value = 0
            rsLoad("FrontTargetNetWt").Value = 0
            rsLoad("FrontWtPerGallon").Value = 0
            rsLoad("FrontTargetGallons").Value = 0
            rsLoad("SelectedFrontGallons").Value = 0
            rsLoad("RearTargetGrossWt").Value = 0
            rsLoad("RearTargetNetWt").Value = 0
            rsLoad("RearWtPerGallon").Value = 0
            rsLoad("RearTargetGallons").Value = 0
            rsLoad("SelectedRearGallons").Value = 0
            rsLoad("FrontTareWt").Value = 0
            rsLoad("FrontGrossWt").Value = 0
            rsLoad("FrontNetWt").Value = 0
            rsLoad("FrontTons").Value = 0
            rsLoad("RearTareWt").Value = 0
            rsLoad("RearGrossWt").Value = 0
            rsLoad("RearNetWt").Value = 0
            rsLoad("RearTons").Value = 0
            rsLoad("FrontBOLHasPrinted").Value = 0
            rsLoad("RearBOLHasPrinted").Value = 0
            rsLoad("WtTicketHasPrinted").Value = 0
            rsLoad("BSHasPrinted").Value = 0
            rsLoad("LoadIsOpen").Value = 1
            rsLoad("LoadType").Value = "RecieveLoad"
            
'            rsLoad.UpdateBatch
            If UpdateRS(rsLoad) = False Then
                MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
            End If
            LoadsTableKey = ConvertToDouble(rsLoad("LoadsTableKey").Value)

'            rsLoad.MoveFirst
        End If
        
        Call BindLoadObjects
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Function
Error:
    MsgBox Me.Name & ".GetLoad()" & vbCrLf & _
    "The following error occurred while creating / retreaving the Load: " & vbCrLf & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Function



Private Sub cmdSave_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdTareFront_Click()
    gbLastActivityTime = Date + Time
    
    If Val(txtFrontTare.Text) > 0 Then
        gbResponse = MsgBox("Front Tare Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    End If
    txtFrontTare.Text = Val(txtScale.Text)
    txtScale.Text = 0
    txtInboundTime.Text = Format(Date, "m/d/YYYY") + " " + Format(Time, "HH:MM:SS AM/PM")
    
    
    txtScale.SetFocus
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdTareFront_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdTareRear_Click()
    gbLastActivityTime = Date + Time
    
    If Val(txtRearTare.Text) > 0 Then
        gbResponse = MsgBox("Rear Tare Weight already captured.  Override?", vbQuestion + vbYesNo, "Weight Capture")
        If gbResponse = vbNo Then
            Exit Sub
        End If
    End If
    txtRearTare.Text = Val(txtScale.Text)
    txtScale.Text = 0
    txtScale.SetFocus

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdTareRear_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    gbLastActivityTime = Date + Time
    
    Select Case KeyCode
           Case vbKeyF2
                cmdTareFront_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
           Case vbKeyF4
                cmdGrossFront_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
           Case vbKeyF5
                cmdTareRear_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
           Case vbKeyF7
                cmdGrossRear_Click
                ActiveControl.SelStart = 0
                ActiveControl.SelLength = Len(ActiveControl.Text)
    End Select

    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_Load()
    On Error GoTo Error
    gbLastActivityTime = Date + Time
    
    Me.KeyPreview = True
    
'    Set SGSScale = New SGSScaleComm.clsScaleComm
    Set SGSScale = CreateObject("SGSScaleComm.clsScaleComm")
'    SGSScale.ParentAppTitle = App.Title    'RKL SGS 10/4/13 Replaced with connection string
    'RKL DEJ
    SGSScale.conn = gbScaleConnStr

    Set rsLoad = New ADODB.Recordset

    If LoadDropDowns() = False Then
        MsgBox "Could not obtain list box info from Access DB.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
        Exit Sub
    End If
    
    'Set Form Defaults
    If gbLocalMode = True Then
        'Don't bother checking
    Else
        If HaveLostMASConnection = True Then
            gbLocalMode = True
        End If
    End If
    
    If gbLocalMode Then
        Me.Caption = "Receive Load (Server Disconnected) - Valero"
    Else
        Me.Caption = "Receive Load - Valero"
    End If


    If gbMANUALSCALING = True Then
        chManualScale = vbChecked
        txtScale.Locked = False
        txtScale.ForeColor = &H80000008
        txtScale.BackColor = &H80000005
        txtScale.TabStop = True
        SGSScale.ClosePort
    Else
        chManualScale = vbUnchecked
        txtScale.Locked = True
        txtScale.ForeColor = &HFFFFFF
        txtScale.BackColor = &H0&
        txtScale.TabStop = False
        SGSScale.OpenPort
    End If
    
    If GetLoad() = False Then
        MsgBox "Could not obtain or create the open load.  Contact your IT support or SGS Technology Group.", vbExclamation, "Failure"
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    gbLastActivityTime = Date + Time
'    cmdSave_Click
'    txtScale.SetFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    gbLastActivityTime = Date + Time
    
    If rsLoad.State = 1 Then
'        rsLoad.UpdateBatch
        If UpdateRS(rsLoad) = False Then
            MsgBox "There was an error saving the data.  If this persists contact your IT support.", vbInformation, "Warning"
        End If
        rsLoad.Close
    End If
    
    Call UnBindLoadObjects
    
    Set rsLoad = Nothing
'
'    If gbScaleConn.State = 1 Then gbScaleConn.Close
'    Set gbScaleConn = Nothing
    
'    If gbMASConn.State = 1 Then gbMASConn.Close
'    Set gbMASConn = Nothing
    
    SGSScale.ClosePort
    Set SGSScale = Nothing

    gbLastActivityTime = Date + Time
End Sub

Public Property Let LoadsTableKey(KeyVlu As Long)
    gbLoadsTableKey = KeyVlu
    
    gbLastActivityTime = Date + Time
End Property


Public Property Get LoadsTableKey() As Long
    LoadsTableKey = gbLoadsTableKey

    gbLastActivityTime = Date + Time
End Property


Sub BindLoadObjects()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If rsLoad.State <> 1 Then Exit Sub
    
    Set cboInboundOperator.DataSource = rsLoad
    Set txtInboundTime.DataSource = rsLoad
    Set cboOutboundOperator.DataSource = rsLoad
    Set txtOutboundTime.DataSource = rsLoad
    Set chDriverOn.DataSource = rsLoad
    
    
    Set txtFrontFacilityID.DataSource = rsLoad
    Set txtFrontTruck.DataSource = rsLoad
    
    
    Set txtFrontProductID.DataSource = rsLoad
    Set txtFrontProduct.DataSource = rsLoad
    Set txtFrontTank.DataSource = rsLoad
    Set txtFrontRack.DataSource = rsLoad
    Set txtFrontTare.DataSource = rsLoad
    Set txtFrontGross.DataSource = rsLoad
    Set txtFrontNet.DataSource = rsLoad
    Set txtFrontTons.DataSource = rsLoad
    Set txtFrontArrivalDt.DataSource = rsLoad
    Set txtFrontDestDt.DataSource = rsLoad
    
    
    Set txtRearFacilityID.DataSource = rsLoad
    
    Set txtRearTare.DataSource = rsLoad
    Set txtRearGross.DataSource = rsLoad
    Set txtRearNet.DataSource = rsLoad
    Set txtRearTons.DataSource = rsLoad
    Set txtRearArrivalDt.DataSource = rsLoad
    Set txtRearDestDt.DataSource = rsLoad
    
    Set chManualScale.DataSource = rsLoad
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".BindLoadObjects()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub


Sub UnBindLoadObjects()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Set cboInboundOperator.DataSource = rsLoad
    Set txtInboundTime.DataSource = rsLoad
    Set cboOutboundOperator.DataSource = rsLoad
    Set txtOutboundTime.DataSource = rsLoad
    Set chDriverOn.DataSource = rsLoad
    
    
    Set txtFrontFacilityID.DataSource = rsLoad
    Set txtFrontTruck.DataSource = rsLoad
    
    
    Set txtFrontProductID.DataSource = rsLoad
    Set txtFrontProduct.DataSource = rsLoad
    Set txtFrontTank.DataSource = rsLoad
    Set txtFrontRack.DataSource = rsLoad
    Set txtFrontTare.DataSource = rsLoad
    Set txtFrontGross.DataSource = rsLoad
    Set txtFrontNet.DataSource = rsLoad
    Set txtFrontTons.DataSource = rsLoad
    Set txtFrontArrivalDt.DataSource = rsLoad
    Set txtFrontDestDt.DataSource = rsLoad
    
    
    Set txtRearFacilityID.DataSource = rsLoad
    
    Set txtRearTare.DataSource = rsLoad
    Set txtRearGross.DataSource = rsLoad
    Set txtRearNet.DataSource = rsLoad
    Set txtRearTons.DataSource = rsLoad
    Set txtRearArrivalDt.DataSource = rsLoad
    Set txtRearDestDt.DataSource = rsLoad
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".BindLoadObjects()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Function LoadDropDowns()
    On Error GoTo Error
    Dim SQL As String
    Dim rsOperator As New ADODB.Recordset
    
    gbLastActivityTime = Date + Time
    
    SQL = "Select * From Operator Where ActiveFg = 1 Order By OperatorID ASC"
    
    rsOperator.CursorLocation = adUseClient
    rsOperator.Open SQL, gbScaleConn, adOpenForwardOnly, adLockReadOnly
    
    rsOperator.MoveFirst
    cboInboundOperator.Clear
    cboOutboundOperator.Clear
    Do While Not rsOperator.EOF
        cboInboundOperator.AddItem rsOperator("OperatorID")
        cboOutboundOperator.AddItem rsOperator("OperatorID")
        rsOperator.MoveNext
    Loop
    
    rsOperator.Close
    Set rsOperator = Nothing
    
    LoadDropDowns = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".LoadDropDowns()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Function

Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame6_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub frameRearLoad_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label12_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label16_Click()
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label16_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label17_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label18_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label19_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label22_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label23_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label24_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label25_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label26_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label27_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label45_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label50_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label7_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label8_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label9_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub SGSScale_Error(ErrMsg As String)
    txtScaleMsg.Text = ErrMsg & vbTab & txtScaleMsg.Text
    txtScaleMsg.Visible = True
    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_ScaleWeight(Weight As Double, Sign As SGSScaleComm.Polarity, UnitOfMeasure As SGSScaleComm.WeightType, GN As SGSScaleComm.GrossOrNet)
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If Sign = Negative Then
        If Weight < 0 Then
            txtScale.Text = Weight
        Else
            txtScale.Text = Weight * -1
        End If
    ElseIf Sign = Positive Then
        If Weight > 0 Then
            txtScale.Text = Weight
        Else
            txtScale.Text = Weight * -1
        End If
    Else
        txtScale.Text = Weight
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    txtScaleMsg.Text = Me.Name & ".SGSScale_ScaleWeight(): Error: " & Err.Number & " " & Err.Description & vbTab & txtScaleMsg.Text
    txtScaleMsg.Visible = True
    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
    tmrScale.Enabled = True

    gbLastActivityTime = Date + Time
End Sub

Private Sub SGSScale_StatusChange(StatusStr As String)
    gbLastActivityTime = Date + Time

'    txtScaleMsg.Text = StatusStr & vbTab & txtScaleMsg.Text
'    txtScaleMsg.Visible = True
'    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
'    tmrScale.Enabled = True

End Sub

Private Sub SGSScale_Warning(WarningMsg As String)
    gbLastActivityTime = Date + Time

'    txtScaleMsg.Text = WarningMsg & vbTab & txtScaleMsg.Text
'    txtScaleMsg.Visible = True
'    tmrScale.Enabled = False    'This will reset the timer if the timer is already running
'    tmrScale.Enabled = True
End Sub

Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
            
'            MsgBox Me.Name & vbTab & "Add Code to close Locked records and return to Open Loads"
            
            Call cmdSave_Click
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False

End Sub

Private Sub tmrScale_Timer()
    gbLastActivityTime = Date + Time
    
    tmrScale.Enabled = False
    txtScaleMsg.Visible = False
    txtScaleMsg.Text = Empty

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtFrontArrivalDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontDestDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontFacilityID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontGross_Change()
    txtFrontNet.Text = Val(txtFrontGross.Text) - Val(txtFrontTare.Text)
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontGross_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontGross_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontNet_Change()
    txtFrontTons.Text = txtFrontNet.Text / 2000

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontNet_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontProduct_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontProductID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontRack_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTank_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTare_Change()
    txtFrontNet.Text = Val(txtFrontGross.Text) - Val(txtFrontTare.Text)
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTare_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtFrontTare_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtFrontTruck_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtInboundTime_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtOutboundTime_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearArrivalDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearDestDt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearFacilityID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearGross_Change()
    txtRearNet.Text = txtRearGross.Text - txtRearTare.Text
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGross_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearGross_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearNet_Change()
    txtRearTons.Text = txtRearNet.Text / 2000

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtRearNet_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearProductID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTare_Change()
    txtRearNet.Text = Val(txtRearGross.Text) - Val(txtRearTare.Text)
    txtTotalGrossWt.Text = Val(txtFrontGross.Text) + Val(txtRearGross.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtRearTare_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub



Private Sub txtRearTare_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtRearTons_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtScale_GotFocus()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtScale_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtScaleMsg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtTotalGrossWt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub
