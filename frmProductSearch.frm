VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Begin VB.Form frmProductSearch 
   BackColor       =   &H00FF00FF&
   Caption         =   "Search for Product and Additives - Valero"
   ClientHeight    =   7185
   ClientLeft      =   1185
   ClientTop       =   1200
   ClientWidth     =   10380
   Icon            =   "frmProductSearch.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7185
   ScaleWidth      =   10380
   Begin VB.Timer tmrAutoReturnNoActivity 
      Interval        =   10000
      Left            =   0
      Top             =   0
   End
   Begin MSDataGridLib.DataGrid grdItems 
      Height          =   4935
      Left            =   120
      TabIndex        =   23
      Top             =   2160
      Width           =   10095
      _ExtentX        =   17806
      _ExtentY        =   8705
      _Version        =   393216
      AllowUpdate     =   0   'False
      HeadLines       =   1
      RowHeight       =   15
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "Products and Additives"
      ColumnCount     =   2
      BeginProperty Column00 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   ""
         Caption         =   ""
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         MarqueeStyle    =   3
         AllowRowSizing  =   0   'False
         AllowSizing     =   0   'False
         Locked          =   -1  'True
         RecordSelectors =   0   'False
         BeginProperty Column00 
         EndProperty
         BeginProperty Column01 
         EndProperty
      EndProperty
   End
   Begin VB.Frame Frame2 
      Caption         =   "Filter By"
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10095
      Begin VB.CommandButton cmdRemove 
         Caption         =   "Remove"
         Height          =   375
         Left            =   8760
         TabIndex        =   22
         ToolTipText     =   "Remove Filter"
         Top             =   480
         Width           =   1095
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "Apply"
         Height          =   375
         Left            =   7560
         TabIndex        =   21
         ToolTipText     =   "Apply Filter"
         Top             =   480
         Width           =   1095
      End
      Begin VB.TextBox txtValue 
         Height          =   285
         Index           =   0
         Left            =   5040
         TabIndex        =   17
         Top             =   600
         Width           =   2295
      End
      Begin VB.ComboBox cmboCondition 
         Height          =   315
         Index           =   0
         ItemData        =   "frmProductSearch.frx":0442
         Left            =   2760
         List            =   "frmProductSearch.frx":0458
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   600
         Width           =   2175
      End
      Begin VB.ComboBox cmboFileName 
         Height          =   315
         Index           =   0
         ItemData        =   "frmProductSearch.frx":049F
         Left            =   120
         List            =   "frmProductSearch.frx":04A1
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   600
         Width           =   2535
      End
      Begin VB.TextBox txtValue 
         Height          =   285
         Index           =   1
         Left            =   5040
         TabIndex        =   14
         Top             =   960
         Width           =   2295
      End
      Begin VB.ComboBox cmboCondition 
         Height          =   315
         Index           =   1
         ItemData        =   "frmProductSearch.frx":04A3
         Left            =   2760
         List            =   "frmProductSearch.frx":04B9
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   960
         Width           =   2175
      End
      Begin VB.ComboBox cmboFileName 
         Height          =   315
         Index           =   1
         ItemData        =   "frmProductSearch.frx":0500
         Left            =   120
         List            =   "frmProductSearch.frx":0502
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Top             =   960
         Width           =   2535
      End
      Begin VB.TextBox txtValue 
         Height          =   285
         Index           =   2
         Left            =   5040
         TabIndex        =   11
         Top             =   1320
         Width           =   2295
      End
      Begin VB.ComboBox cmboCondition 
         Height          =   315
         Index           =   2
         ItemData        =   "frmProductSearch.frx":0504
         Left            =   2760
         List            =   "frmProductSearch.frx":051A
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   1320
         Width           =   2175
      End
      Begin VB.ComboBox cmboFileName 
         Height          =   315
         Index           =   2
         ItemData        =   "frmProductSearch.frx":0561
         Left            =   120
         List            =   "frmProductSearch.frx":0563
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1320
         Width           =   2535
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   0
         Left            =   7440
         TabIndex        =   5
         Top             =   840
         Width           =   2535
         Begin VB.OptionButton OptnAnd 
            Caption         =   "And"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   120
            Width           =   615
         End
         Begin VB.OptionButton optnOr 
            Caption         =   "Or"
            Height          =   195
            Index           =   0
            Left            =   960
            TabIndex        =   7
            Top             =   120
            Width           =   495
         End
         Begin VB.OptionButton OptnNA 
            Caption         =   "N/A"
            Height          =   195
            Index           =   0
            Left            =   1680
            TabIndex        =   6
            ToolTipText     =   "Do not include this line in the filter"
            Top             =   120
            Value           =   -1  'True
            Width           =   615
         End
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   1
         Left            =   7440
         TabIndex        =   1
         Top             =   1320
         Width           =   2535
         Begin VB.OptionButton optnOr 
            Caption         =   "Or"
            Height          =   195
            Index           =   1
            Left            =   960
            TabIndex        =   4
            Top             =   120
            Width           =   495
         End
         Begin VB.OptionButton OptnAnd 
            Caption         =   "And"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   3
            Top             =   120
            Width           =   615
         End
         Begin VB.OptionButton OptnNA 
            Caption         =   "N/A"
            Height          =   195
            Index           =   1
            Left            =   1680
            TabIndex        =   2
            ToolTipText     =   "Do not include this line in the filter"
            Top             =   120
            Value           =   -1  'True
            Width           =   615
         End
      End
      Begin VB.Label Label3 
         Caption         =   "Field Name"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Filter Condition"
         Height          =   255
         Left            =   2760
         TabIndex        =   19
         Top             =   360
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Filter Value"
         Height          =   255
         Left            =   5040
         TabIndex        =   18
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Menu mnuGrid 
      Caption         =   "Grid Menu"
      Visible         =   0   'False
      Begin VB.Menu mnuSelect 
         Caption         =   "Select Record"
      End
   End
End
Attribute VB_Name = "frmProductSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim WithEvents RS As ADODB.Recordset
Attribute RS.VB_VarHelpID = -1
Private lcItemID As String
Private lcItemDesc As String
Private lcItemClassID As String
Private lcItemDescFilter As String
Private lcItemIDFilter As String

Sub GetRSData(Optional ApplyUserFilter As Boolean = True)
    On Error GoTo Error
    
    Dim SQL As String
'    Dim i As Integer
'    Dim SQL As String
    
    gbLastActivityTime = Date + Time
    
    Set grdItems.DataSource = Nothing
    
    If gbMASConn.State <> 1 Then
        MsgBox "The database connection has been lost.", vbExclamation, "No Database Connection"
        Unload Me
        Exit Sub
    End If
    
    If RS.State = 1 Then RS.Close
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenDynamic
    
'    SQL = "Select ItemID, ItemShortDesc, ItemLongDesc, WhseID, WhseDesc, ItemClassID, ItemClassName From vluGetItemAndWarehouse_SGS Where WhseID = '" & gbWhseID & "'"
    SQL = "Select ItemID, ItemShortDesc, ItemLongDesc, WhseID, WhseDesc, ItemClassID, ItemClassName From vluGetItemAndWarehouse_SGS Where FacilitySeqNo = " & gbPlantPrefix & " And Coalesce(CustOwnedProd,0) = " & Int(Abs(gbCustOwnedProd)) & " "
'    SQL = "Select ItemID, ItemShortDesc, ItemLongDesc, WhseID, WhseDesc, ItemClassID, ItemClassName From vluGetItemAndWarehouse_SGS "
    
    If lcItemDescFilter <> Empty Then
        SQL = SQL & " And ItemShortDesc = '" & Replace(lcItemDescFilter, "'", "''") & "' "
    End If
    
    If lcItemIDFilter <> Empty Then
        SQL = SQL & " And ItemID = '" & Replace(lcItemIDFilter, "'", "''") & "' "
    End If
    
    If cmboFileName(0).Text <> Empty And cmboCondition(0).Text <> Empty And ApplyUserFilter = True Then
        SQL = SQL & " And (" & GetCondition(cmboFileName(0).Text, cmboCondition(0).Text, txtValue(0).Text)
    
        If cmboFileName(1).Text <> Empty And cmboCondition(1).Text <> Empty And OptnNA(0).Value = False Then
            If optnOr(0).Value = True Then
                SQL = SQL & " Or " & GetCondition(cmboFileName(1).Text, cmboCondition(1).Text, txtValue(1).Text)
            Else
                SQL = SQL & " And " & GetCondition(cmboFileName(1).Text, cmboCondition(1).Text, txtValue(1).Text)
            End If
        End If
        
        If cmboFileName(2).Text <> Empty And cmboCondition(2).Text <> Empty And OptnNA(1).Value = False Then
            If optnOr(1).Value = True Then
                SQL = SQL & " Or " & GetCondition(cmboFileName(2).Text, cmboCondition(2).Text, txtValue(2).Text)
            Else
                SQL = SQL & " And " & GetCondition(cmboFileName(2).Text, cmboCondition(2).Text, txtValue(2).Text)
            End If
        End If
        
        SQL = SQL & ")"
    End If
        
    RS.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
    
    If RS.State = 1 Then
        Set grdItems.DataSource = RS
        
        grdItems.Caption = "(" & RS.RecordCount & ") Products and Additives"
        
        If lcItemID <> Empty And Not RS.BOF And Not RS.EOF Then
            RS.Find "ItemID = '" & Replace(lcItemID, "'", "''") & "'"
        End If
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".GetRSData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub


Public Property Get ItemClassID() As String
    ItemClassID = lcItemClassID

    gbLastActivityTime = Date + Time
End Property

Public Property Let ItemClassID(ClassID As String)
    lcItemClassID = ClassID

    gbLastActivityTime = Date + Time
End Property


Public Property Get ItemID() As String
    ItemID = lcItemID

    gbLastActivityTime = Date + Time
End Property

Public Property Let ItemID(Vlu As String)
    lcItemID = Vlu

    gbLastActivityTime = Date + Time
End Property

Public Property Get ItemDesc() As String
    ItemDesc = lcItemDesc

    gbLastActivityTime = Date + Time
End Property


Public Property Let ItemDesc(Vlu As String)
    lcItemDesc = Vlu

    gbLastActivityTime = Date + Time
End Property


Public Property Let ItemDescFilter(Vlu As String)
    lcItemDescFilter = Vlu

    gbLastActivityTime = Date + Time
End Property

Public Property Let ItemIDFilter(Vlu As String)
    lcItemIDFilter = Vlu

    gbLastActivityTime = Date + Time
End Property



Sub SetupFilters()
    On Error Resume Next
    
    Dim FLD As ADODB.Field
    
    gbLastActivityTime = Date + Time
    
    For Each FLD In RS.Fields
        cmboFileName(0).AddItem FLD.Name
        cmboFileName(1).AddItem FLD.Name
        cmboFileName(2).AddItem FLD.Name
    Next
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdApply_Click()
    On Error GoTo Error
        
    gbLastActivityTime = Date + Time
        
    Call GetRSData
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdApply_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Private Sub cmdRemove_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Call GetRSData(False)
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdRemove_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time

End Sub


Private Sub Form_Load()
    On Error GoTo Error
    
    Dim SQL As String
    
    gbLastActivityTime = Date + Time
    
    Set RS = New ADODB.Recordset
    
    grdItems.Left = 100
        
    'Validate MAS Connection
    If gbLocalMode = False Then
        If HaveLostMASConnection = True Then
            'No longer connected to MAS 500
        End If
    End If
    
    Call GetRSData
    
    Call SetupFilters
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Resize()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If Me.Width >= 400 Then
        grdItems.Width = Me.Width - 350
    Else
        grdItems.Width = 100
    End If
    
    If Me.Height >= 2900 Then
        grdItems.Height = Me.Height - 2800
    Else
        grdItems.Height = 100
    End If
    

    gbLastActivityTime = Date + Time
End Sub


Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    lcItemDescFilter = Empty
    
    Set grdItems.DataSource = Nothing
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub Frame1_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Frame2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub grdItems_DblClick()
    Call mnuSelect_Click
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub grdItems_HeadClick(ByVal ColIndex As Integer)
    On Error GoTo Error
    
    Dim SortField As String
    Dim SortString As String
        
    gbLastActivityTime = Date + Time
    
    SortField = "[" & grdItems.Columns(ColIndex).DataField & "]"
    
    If InStr(RS.Sort, "Asc") Then
        SortString = SortField & " Desc"
    Else
        SortString = SortField & " Asc"
    End If
    
    RS.Sort = SortString
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".grdItems_HeadClick(ByVal ColIndex As Integer)" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
End Sub


Private Sub grdItems_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time
    
    Select Case Button
        Case 1  'Left Mouse
        Case 2  'Right Mouse
            If RS.State = 1 Then
                If RS.EOF Or RS.BOF Then
                    mnuSelect.Enabled = False
                Else
                    mnuSelect.Enabled = True
                End If
            Else
                mnuSelect.Enabled = False
            End If
            
            PopupMenu mnuGrid ', , X, Y
            
        Case 4  'Scroll Button
        Case Else
    End Select


    gbLastActivityTime = Date + Time
End Sub


Private Sub grdItems_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label3_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label4_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label5_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub mnuSelect_Click()
    
    gbLastActivityTime = Date + Time
    
    If RS.State <> 1 Then
        Exit Sub
    End If
    
    If RS.EOF Or RS.BOF Then
        Exit Sub
    End If
    
    lcItemID = ConvertToString(RS("ItemID").Value)
    lcItemDesc = ConvertToString(RS("ItemShortDesc").Value)
    lcItemClassID = ConvertToString(RS("ItemClassID").Value)
    
    gbLastActivityTime = Date + Time
    
    Unload Me
    
    gbLastActivityTime = Date + Time
End Sub


Function GetCondition(FieldName As String, Condition As String, Val As String) As String
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Select Case UCase(Condition)
        Case "BEGINS WITH"
            GetCondition = "[" & FieldName & "] Like '" & Replace(Val, "'", "''") & "%'"
            
        Case "ENDS WITH"
            GetCondition = "ltrim(Rtrim([" & FieldName & "])) Like '%" & Replace(Val, "'", "''") & "'"
        
        Case "CONTAINS"
            GetCondition = "[" & FieldName & "] Like '%" & Replace(Val, "'", "''") & "%'"
        
        Case "EQUALS"
            GetCondition = "[" & FieldName & "] = '" & Replace(Val, "'", "''") & "'"
            
        Case "GREATER THAN"
            GetCondition = "[" & FieldName & "] > '" & Replace(Val, "'", "''") & "'"
        
        Case "LESS THAN"
            GetCondition = "[" & FieldName & "] < '" & Replace(Val, "'", "''") & "'"
            
        Case Else
            GetCondition = "[" & FieldName & "] = '" & Replace(Val, "'", "''") & "'"
    
    End Select
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox Me.Name & ".GetCondition(Val As String) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function

Private Sub OptnAnd_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub OptnNA_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub optnOr_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
                        
            Unload Me
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False

End Sub

Private Sub txtValue_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub
