VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmPrintPreview 
   BackColor       =   &H00FF00FF&
   Caption         =   "BOL Print Preview - Valero"
   ClientHeight    =   2430
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   4635
   Icon            =   "frmPrintPreview.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2430
   ScaleWidth      =   4635
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer tmrAutoReturnNoActivity 
      Interval        =   65000
      Left            =   0
      Top             =   0
   End
   Begin RichTextLib.RichTextBox rtfText 
      Height          =   855
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   1508
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"frmPrintPreview.frx":0442
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Lucida Console"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgCommonDialog 
      Left            =   2400
      Top             =   240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Menu mnuPrint 
      Caption         =   "Print"
   End
End
Attribute VB_Name = "frmPrintPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Resize()
    gbLastActivityTime = Date + Time
    
    rtfText.Left = 0
    rtfText.Top = 0
    
    rtfText.Width = Me.Width - 200
    rtfText.Height = Me.Height - 900

    gbLastActivityTime = Date + Time
End Sub


Private Sub mnuPrint_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time

    With dlgCommonDialog
        .DialogTitle = "Print"
        .CancelError = True
        .Flags = cdlPDReturnDC + cdlPDNoPageNums
        If Me.rtfText.SelLength = 0 Then
            .Flags = .Flags + cdlPDAllPages
        Else
            .Flags = .Flags + cdlPDSelection
        End If
        .ShowPrinter
        If Err <> MSComDlg.cdlCancel Then
            Me.rtfText.SelPrint .hDC
        End If
    End With

    gbLastActivityTime = Date + Time
End Sub


Private Sub rtfText_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub tmrAutoReturnNoActivity_Timer()
    On Error GoTo Error
    
    Dim CurrentTime As Date
    
    
    tmrAutoReturnNoActivity.Enabled = False
    
    CurrentTime = Date + Time
    
    If gbAutoRtnOpenLoads = True Then
        If CurrentTime >= DateAdd("S", CDbl(gbAutoRtrnSecnds), gbLastActivityTime) Then
            'Time is up need to return to Open Loads
                        
            Unload Me
            
            Exit Sub
        End If
        
        tmrAutoReturnNoActivity.Enabled = True
        
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".tmrAutoReturnNoActivity_Timer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical
    
    Err.Clear
    
    tmrAutoReturnNoActivity.Enabled = False

End Sub
