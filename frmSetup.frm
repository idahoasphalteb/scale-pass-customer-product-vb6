VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmSetup 
   BackColor       =   &H00FF00FF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Scale Pass Setup - Valero"
   ClientHeight    =   6870
   ClientLeft      =   2790
   ClientTop       =   4230
   ClientWidth     =   10095
   ControlBox      =   0   'False
   Icon            =   "frmSetup.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   10095
   Begin TabDlg.SSTab SSTab1 
      Height          =   6615
      Left            =   0
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   8535
      _ExtentX        =   15055
      _ExtentY        =   11668
      _Version        =   393216
      Tabs            =   8
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "General"
      TabPicture(0)   =   "frmSetup.frx":000C
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label7"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label6"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label11"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label12"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmboCypressBOLPrntr"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "chManualScaling"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "cmoMode"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "cmoPlant"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "chRequireLotNbr"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "frmeReturn"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "chHideOverShip"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "chDriverOn"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "chShowLMEMsg"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "chAllowGetLMEMsg"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "chCustOwned"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "chNotAllowSplitLoads"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "txtSrcVariance"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "chAutoPostIMTrans"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "txtIMTranReasonCode"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "chForceVarianceCheck"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).ControlCount=   21
      TabCaption(1)   =   "BOL"
      TabPicture(1)   =   "frmSetup.frx":0028
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "SSTab2"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "COA"
      TabPicture(2)   =   "frmSetup.frx":0044
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "cmboExtraCOAPrinter"
      Tab(2).Control(1)=   "chPrntCOACypress"
      Tab(2).Control(2)=   "chExtraCOA"
      Tab(2).Control(3)=   "chEnforceCOA"
      Tab(2).Control(4)=   "txtCOACnt"
      Tab(2).Control(5)=   "cmboCOAPrinter"
      Tab(2).Control(6)=   "Label8"
      Tab(2).Control(7)=   "lblCOACnt"
      Tab(2).Control(8)=   "lblCOA"
      Tab(2).ControlCount=   9
      TabCaption(3)   =   "Label"
      TabPicture(3)   =   "frmSetup.frx":0060
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame1"
      Tab(3).Control(1)=   "chExtraLabelWY"
      Tab(3).Control(2)=   "chExtraLabelRisk"
      Tab(3).Control(3)=   "txtLblCnt"
      Tab(3).Control(3).Enabled=   0   'False
      Tab(3).Control(4)=   "chPrintLabelAfterBOL"
      Tab(3).Control(5)=   "chExtraLabelCert"
      Tab(3).Control(6)=   "cmboLabelPrinter"
      Tab(3).Control(7)=   "chPrintLabels"
      Tab(3).Control(8)=   "lblNbrCopies"
      Tab(3).Control(9)=   "lblLablePrinter"
      Tab(3).ControlCount=   10
      TabCaption(4)   =   "Loading Batch Sheet"
      TabPicture(4)   =   "frmSetup.frx":007C
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "cmboScalePrntr"
      Tab(4).Control(1)=   "cmboAlternatePrntr"
      Tab(4).Control(2)=   "Label3"
      Tab(4).Control(3)=   "Label2"
      Tab(4).ControlCount=   4
      TabCaption(5)   =   "Databases"
      TabPicture(5)   =   "frmSetup.frx":0098
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "cmdMcLeod"
      Tab(5).Control(1)=   "cmdMAS"
      Tab(5).Control(2)=   "cmdScale"
      Tab(5).ControlCount=   3
      TabCaption(6)   =   "Warnings"
      TabPicture(6)   =   "frmSetup.frx":00B4
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "lblStandardTons"
      Tab(6).Control(1)=   "lblTonsVariance"
      Tab(6).Control(2)=   "chScaleInTonsWarn"
      Tab(6).Control(3)=   "txtStandardTons"
      Tab(6).Control(4)=   "chScaleOutVarWarning"
      Tab(6).Control(5)=   "txtTonsVariance"
      Tab(6).Control(6)=   "chPrcessBOLVarWrn"
      Tab(6).ControlCount=   7
      TabCaption(7)   =   "Data Mainenance"
      TabPicture(7)   =   "frmSetup.frx":00D0
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "cmdDelRecords"
      Tab(7).Control(1)=   "chBOLPrinting"
      Tab(7).Control(2)=   "chBOLPrintingErrors"
      Tab(7).Control(3)=   "chLoads"
      Tab(7).Control(4)=   "frmDeleteOptions"
      Tab(7).Control(5)=   "lblNote"
      Tab(7).Control(6)=   "lblTableLise"
      Tab(7).ControlCount=   7
      Begin VB.CheckBox chForceVarianceCheck 
         Alignment       =   1  'Right Justify
         Caption         =   "Force Source Product Variance Check"
         Height          =   255
         Left            =   4800
         TabIndex        =   91
         ToolTipText     =   "Validate and enforce the Variance and source products."
         Top             =   1920
         Width           =   3135
      End
      Begin VB.TextBox txtIMTranReasonCode 
         Height          =   285
         Left            =   4920
         MaxLength       =   15
         TabIndex        =   90
         ToolTipText     =   "All IM Sales Returns will have this reason code."
         Top             =   2760
         Width           =   2535
      End
      Begin VB.CheckBox chAutoPostIMTrans 
         Alignment       =   1  'Right Justify
         Caption         =   "Auto Post IM Transactions"
         Enabled         =   0   'False
         Height          =   255
         Left            =   840
         TabIndex        =   88
         ToolTipText     =   "Auto post the IM Transactions created from the IM Sales Or Sales Return transaction"
         Top             =   2040
         Width           =   3255
      End
      Begin VB.TextBox txtSrcVariance 
         Alignment       =   1  'Right Justify
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0.000"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   4800
         MaxLength       =   7
         TabIndex        =   87
         Text            =   "0"
         ToolTipText     =   "The allowed variance between the sum of the source products weight and the actual weight."
         Top             =   1560
         Width           =   1695
      End
      Begin VB.CheckBox chNotAllowSplitLoads 
         Alignment       =   1  'Right Justify
         Caption         =   "Do Not Allow Split Loads"
         Height          =   255
         Left            =   240
         TabIndex        =   85
         ToolTipText     =   "Do not allow the user to select split loads, and hide Rear Fields."
         Top             =   2280
         Width           =   3855
      End
      Begin VB.CheckBox chCustOwned 
         Alignment       =   1  'Right Justify
         Caption         =   "Customer Owned Products"
         Height          =   255
         Left            =   240
         TabIndex        =   84
         ToolTipText     =   "All shipments are customer owned shipments and will only result in a IM Sales or Sales Return transaction."
         Top             =   1800
         Width           =   3855
      End
      Begin VB.CommandButton cmdDelRecords 
         Caption         =   "Delete"
         Height          =   375
         Left            =   -70800
         TabIndex        =   79
         ToolTipText     =   "Delete records from the selected tables."
         Top             =   2640
         Width           =   1935
      End
      Begin VB.CheckBox chBOLPrinting 
         Caption         =   "BOLPrinting"
         Height          =   255
         Left            =   -70920
         TabIndex        =   78
         Top             =   2040
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.CheckBox chBOLPrintingErrors 
         Caption         =   "Error Log"
         Height          =   255
         Left            =   -70920
         TabIndex        =   77
         Top             =   1800
         Width           =   2175
      End
      Begin VB.CheckBox chLoads 
         Caption         =   "Loads"
         Height          =   255
         Left            =   -70920
         TabIndex        =   76
         Top             =   1560
         Width           =   2175
      End
      Begin VB.Frame frmDeleteOptions 
         Caption         =   "Delete Options"
         Height          =   2055
         Left            =   -74760
         TabIndex        =   71
         Top             =   1080
         Width           =   3615
         Begin VB.OptionButton optionDelDate 
            Caption         =   "Delete records from the selected tables that are older than the selected date."
            Height          =   435
            Left            =   120
            TabIndex        =   73
            Top             =   600
            Width           =   3375
         End
         Begin VB.OptionButton optionDelAll 
            Caption         =   "Delete All Records in the Selected Tables"
            Height          =   375
            Left            =   120
            TabIndex        =   72
            Top             =   240
            Width           =   3375
         End
         Begin MSComCtl2.DTPicker dtDelOlder 
            Height          =   375
            Left            =   1440
            TabIndex        =   74
            Top             =   1080
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   661
            _Version        =   393216
            Format          =   172032001
            CurrentDate     =   42815
         End
      End
      Begin TabDlg.SSTab SSTab2 
         Height          =   5565
         Left            =   -74880
         TabIndex        =   82
         Top             =   960
         Width           =   8115
         _ExtentX        =   14314
         _ExtentY        =   9816
         _Version        =   393216
         Tab             =   1
         TabHeight       =   520
         WordWrap        =   0   'False
         TabCaption(0)   =   "BOL Print Caption"
         TabPicture(0)   =   "frmSetup.frx":00EC
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "cmdAddBOLPrnt"
         Tab(0).Control(1)=   "cmdDelBOLPrnt"
         Tab(0).Control(2)=   "cmdBOLDflts"
         Tab(0).Control(3)=   "grdBOLPrint"
         Tab(0).ControlCount=   4
         TabCaption(1)   =   "BOL Setup Data"
         TabPicture(1)   =   "frmSetup.frx":0108
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "lblBOLAddrAndPhone"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "cmdRestoreBOLAddrDflt"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "txtBOLAddrPhone"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).Control(3)=   "cmdRestPASSPatent"
         Tab(1).Control(3).Enabled=   0   'False
         Tab(1).Control(4)=   "txtPASSPatent"
         Tab(1).Control(4).Enabled=   0   'False
         Tab(1).Control(5)=   "chPrintPASSPatent"
         Tab(1).Control(5).Enabled=   0   'False
         Tab(1).ControlCount=   6
         TabCaption(2)   =   "BOL Printers"
         TabPicture(2)   =   "frmSetup.frx":0124
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "cmboAltBOLPrinter"
         Tab(2).Control(1)=   "chBOLCypress"
         Tab(2).Control(2)=   "cmboBOLPrntr"
         Tab(2).Control(3)=   "chExtraBOL"
         Tab(2).Control(4)=   "cmboExtraBOLPrinter"
         Tab(2).Control(5)=   "chExtraBOLCOAOnly"
         Tab(2).Control(6)=   "Label13"
         Tab(2).Control(7)=   "Label4"
         Tab(2).Control(8)=   "Label9"
         Tab(2).ControlCount=   9
         Begin VB.ComboBox cmboAltBOLPrinter 
            Height          =   315
            Left            =   -73200
            TabIndex        =   25
            Text            =   "Combo1"
            Top             =   2520
            Width           =   3855
         End
         Begin VB.CheckBox chBOLCypress 
            Alignment       =   1  'Right Justify
            Caption         =   "Print BOL to Cypress"
            Height          =   255
            Left            =   -74760
            TabIndex        =   19
            Top             =   960
            Width           =   3855
         End
         Begin VB.ComboBox cmboBOLPrntr 
            Height          =   315
            Left            =   -73200
            TabIndex        =   18
            Text            =   "Combo1"
            Top             =   600
            Width           =   3855
         End
         Begin VB.CheckBox chExtraBOL 
            Alignment       =   1  'Right Justify
            Caption         =   "Print Extra Copy of the BOL"
            Height          =   255
            Left            =   -74760
            TabIndex        =   20
            ToolTipText     =   "Print one extra copy of the BOL, COA, and Label"
            Top             =   1200
            Width           =   3855
         End
         Begin VB.ComboBox cmboExtraBOLPrinter 
            Height          =   315
            Left            =   -73200
            TabIndex        =   22
            Text            =   "Combo1"
            Top             =   1440
            Width           =   3855
         End
         Begin VB.CheckBox chExtraBOLCOAOnly 
            Alignment       =   1  'Right Justify
            Caption         =   "Only print the Extra BOL when the COA is requred."
            Height          =   255
            Left            =   -74760
            TabIndex        =   23
            ToolTipText     =   "Only print the Extra BOL when the COA is requred."
            Top             =   1800
            Width           =   3855
         End
         Begin VB.CheckBox chPrintPASSPatent 
            Alignment       =   1  'Right Justify
            Caption         =   "Print PASS Patent Info on the BOL"
            Height          =   255
            Left            =   240
            TabIndex        =   26
            ToolTipText     =   "Print one extra copy of the BOL, COA, and Label"
            Top             =   645
            Width           =   3855
         End
         Begin VB.TextBox txtPASSPatent 
            Enabled         =   0   'False
            Height          =   285
            Left            =   600
            MaxLength       =   200
            TabIndex        =   28
            Top             =   885
            Width           =   6975
         End
         Begin VB.CommandButton cmdRestPASSPatent 
            Caption         =   "Reset Default"
            Height          =   255
            Left            =   6240
            TabIndex        =   27
            ToolTipText     =   "Reset PASS Patent to the default text."
            Top             =   600
            Width           =   1335
         End
         Begin VB.TextBox txtBOLAddrPhone 
            Height          =   285
            Left            =   240
            MaxLength       =   500
            TabIndex        =   31
            Top             =   1605
            Width           =   7335
         End
         Begin VB.CommandButton cmdRestoreBOLAddrDflt 
            Caption         =   "Reset Default"
            Height          =   255
            Left            =   6240
            TabIndex        =   30
            ToolTipText     =   "Reset the default BOL Address and Phone"
            Top             =   1305
            Width           =   1335
         End
         Begin VB.CommandButton cmdAddBOLPrnt 
            Caption         =   "Add Print Copy"
            Height          =   375
            Left            =   -69960
            TabIndex        =   32
            Top             =   720
            Width           =   1215
         End
         Begin VB.CommandButton cmdDelBOLPrnt 
            Caption         =   "Del Print Copy"
            Height          =   375
            Left            =   -69960
            TabIndex        =   34
            ToolTipText     =   "Delete the selected copy "
            Top             =   1200
            Width           =   1215
         End
         Begin VB.CommandButton cmdBOLDflts 
            Caption         =   "Reset Defaults"
            Height          =   375
            Left            =   -69960
            TabIndex        =   35
            ToolTipText     =   "Delete the selected copy "
            Top             =   1680
            Width           =   1215
         End
         Begin MSDataGridLib.DataGrid grdBOLPrint 
            Height          =   2055
            Left            =   -74640
            TabIndex        =   33
            Top             =   720
            Width           =   4575
            _ExtentX        =   8070
            _ExtentY        =   3625
            _Version        =   393216
            HeadLines       =   1
            RowHeight       =   15
            FormatLocked    =   -1  'True
            BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Caption         =   "BOL Print Specifics"
            ColumnCount     =   2
            BeginProperty Column00 
               DataField       =   "CopyNbr"
               Caption         =   "Print Order"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
            EndProperty
            BeginProperty Column01 
               DataField       =   "PrintCaption"
               Caption         =   "Print Caption"
               BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
                  Type            =   0
                  Format          =   ""
                  HaveTrueFalseNull=   0
                  FirstDayOfWeek  =   0
                  FirstWeekOfYear =   0
                  LCID            =   1033
                  SubFormatType   =   0
               EndProperty
            EndProperty
            SplitCount      =   1
            BeginProperty Split0 
               BeginProperty Column00 
                  Locked          =   -1  'True
                  ColumnWidth     =   1005.165
               EndProperty
               BeginProperty Column01 
                  ColumnWidth     =   3195.213
               EndProperty
            EndProperty
         End
         Begin VB.Label Label13 
            Caption         =   "Alternate BOL Printer"
            Height          =   255
            Left            =   -74760
            TabIndex        =   24
            Top             =   2520
            Width           =   1575
         End
         Begin VB.Label Label4 
            Caption         =   "BOL Printer"
            Height          =   255
            Left            =   -74760
            TabIndex        =   17
            Top             =   630
            Width           =   1335
         End
         Begin VB.Label Label9 
            Caption         =   "Extra BOL Printer"
            Height          =   255
            Left            =   -74760
            TabIndex        =   21
            Top             =   1470
            Width           =   1335
         End
         Begin VB.Label lblBOLAddrAndPhone 
            Caption         =   "BOL Address and Phone"
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   1365
            Width           =   2295
         End
      End
      Begin VB.CheckBox chPrcessBOLVarWrn 
         Alignment       =   1  'Right Justify
         Caption         =   "On Process BOL warn when the difference between the requested and actual tons are more than the Max Tons Variance."
         Height          =   495
         Left            =   -74760
         TabIndex        =   68
         ToolTipText     =   "On click of the Process BOL button display a pop up warning message."
         Top             =   3240
         Width           =   5655
      End
      Begin VB.TextBox txtTonsVariance 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -69960
         TabIndex        =   70
         Top             =   3840
         Width           =   855
      End
      Begin VB.CheckBox chScaleOutVarWarning 
         Alignment       =   1  'Right Justify
         Caption         =   "On Scale Out warn when the difference between the requested and actual tons are more than the Tons Variance."
         Height          =   495
         Left            =   -74760
         TabIndex        =   67
         ToolTipText     =   "The Req. Tons field will flash differenct colors."
         Top             =   2640
         Width           =   5655
      End
      Begin VB.TextBox txtStandardTons 
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -69960
         TabIndex        =   66
         Top             =   1320
         Width           =   855
      End
      Begin VB.CheckBox chScaleInTonsWarn 
         Alignment       =   1  'Right Justify
         Caption         =   "On Scale In warn when the requested tons are below the standard tons"
         Height          =   255
         Left            =   -74760
         TabIndex        =   64
         ToolTipText     =   "The Req. Tons field will flash differenct colors."
         Top             =   960
         Width           =   5655
      End
      Begin VB.CheckBox chAllowGetLMEMsg 
         Alignment       =   1  'Right Justify
         Caption         =   "Allow Get LME Message on demand"
         Height          =   255
         Left            =   720
         TabIndex        =   15
         ToolTipText     =   "Alow user to get the LME message for a selected record on the Open Loads screen.  Dispaly ""Get LME Message"" button."
         Top             =   5400
         Width           =   3255
      End
      Begin VB.CheckBox chShowLMEMsg 
         Alignment       =   1  'Right Justify
         Caption         =   "Show LME message"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         ToolTipText     =   "When selecting a load from LME if there is a message then display it on a popup.  Also will dispaly on the over ship message."
         Top             =   5160
         Width           =   3855
      End
      Begin VB.CheckBox chDriverOn 
         Alignment       =   1  'Right Justify
         Caption         =   "Require Driver On Yes/No Answer"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         ToolTipText     =   "Require the Driver On (Yes/No) to be answered before obtaining the tare weight."
         Top             =   5640
         Width           =   3855
      End
      Begin VB.CheckBox chHideOverShip 
         Alignment       =   1  'Right Justify
         Caption         =   "Hide over shipping message"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         ToolTipText     =   $"frmSetup.frx":0140
         Top             =   4920
         Width           =   3855
      End
      Begin VB.Frame Frame1 
         Caption         =   "Future Use"
         Height          =   1215
         Left            =   -69960
         TabIndex        =   54
         Top             =   3360
         Visible         =   0   'False
         Width           =   2535
         Begin VB.ComboBox cmboExtraLabelPrinter 
            Height          =   315
            Left            =   240
            TabIndex        =   56
            Text            =   "Combo1"
            Top             =   600
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label Label10 
            Caption         =   "Extra Label Printer"
            Height          =   255
            Left            =   240
            TabIndex        =   55
            Top             =   240
            Visible         =   0   'False
            Width           =   1455
         End
      End
      Begin VB.CheckBox chExtraLabelWY 
         Alignment       =   1  'Right Justify
         Caption         =   "Print Extra Copy of the Label for emulsion product ending in WY"
         Height          =   495
         Left            =   -74760
         TabIndex        =   51
         ToolTipText     =   "Print one extra copy of the Label for any emulsion that is destined to Wyoming."
         Top             =   3240
         Width           =   3855
      End
      Begin VB.CheckBox chExtraLabelRisk 
         Alignment       =   1  'Right Justify
         Caption         =   "Print Extra Copy of the Label for High Risk"
         Height          =   255
         Left            =   -74760
         TabIndex        =   50
         ToolTipText     =   "Print one extra copy of the Label for High Risk shipments"
         Top             =   2880
         Width           =   3855
      End
      Begin VB.TextBox txtLblCnt 
         Alignment       =   2  'Center
         BackColor       =   &H80000004&
         Height          =   285
         Left            =   -71880
         Locked          =   -1  'True
         TabIndex        =   53
         TabStop         =   0   'False
         Text            =   "0"
         Top             =   4200
         Width           =   615
      End
      Begin VB.CheckBox chPrintLabelAfterBOL 
         Alignment       =   1  'Right Justify
         Caption         =   "Print Label after BOL"
         Height          =   255
         Left            =   -74760
         TabIndex        =   46
         ToolTipText     =   "The label by default prints with the Load Batch Sheet.  This options changes the print to after the BOL."
         Top             =   1320
         Width           =   3855
      End
      Begin VB.ComboBox cmboExtraCOAPrinter 
         Height          =   315
         Left            =   -73200
         TabIndex        =   44
         Text            =   "Combo1"
         Top             =   3000
         Width           =   3855
      End
      Begin VB.CheckBox chPrntCOACypress 
         Alignment       =   1  'Right Justify
         Caption         =   "Print COA to Cypress"
         Height          =   255
         Left            =   -74760
         TabIndex        =   41
         Top             =   2160
         Width           =   3855
      End
      Begin VB.Frame frmeReturn 
         Height          =   1095
         Left            =   1800
         TabIndex        =   9
         Top             =   3840
         Width           =   2895
         Begin VB.ComboBox cmboSeconds 
            Height          =   315
            ItemData        =   "frmSetup.frx":01DD
            Left            =   1440
            List            =   "frmSetup.frx":0568
            Style           =   2  'Dropdown List
            TabIndex        =   12
            Top             =   600
            Width           =   1095
         End
         Begin VB.CheckBox chAutoReturn 
            Alignment       =   1  'Right Justify
            Caption         =   "Auto Return to Open Load"
            Height          =   375
            Left            =   120
            TabIndex        =   10
            Top             =   240
            Width           =   2415
         End
         Begin VB.Label Label5 
            Caption         =   "Seconds"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   600
            Width           =   855
         End
      End
      Begin VB.CheckBox chRequireLotNbr 
         Alignment       =   1  'Right Justify
         Caption         =   "Require Lot #"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   3120
         Width           =   3855
      End
      Begin VB.ComboBox cmboScalePrntr 
         Height          =   315
         Left            =   -73200
         TabIndex        =   58
         Text            =   "Combo1"
         Top             =   960
         Width           =   3855
      End
      Begin VB.ComboBox cmboAlternatePrntr 
         Height          =   315
         ItemData        =   "frmSetup.frx":0ADF
         Left            =   -73200
         List            =   "frmSetup.frx":0AE1
         TabIndex        =   60
         Text            =   "Combo1"
         Top             =   1785
         Width           =   3855
      End
      Begin VB.CommandButton cmdMcLeod 
         Caption         =   "&McLeod Database"
         Height          =   375
         Left            =   -74760
         TabIndex        =   63
         ToolTipText     =   "Setup the connection to the McLeod database"
         Top             =   1620
         Width           =   1815
      End
      Begin VB.CommandButton cmdMAS 
         Caption         =   "M&AS 500 Database"
         Height          =   375
         Left            =   -74760
         TabIndex        =   62
         ToolTipText     =   "Setup the connection to the MAS 500 Database"
         Top             =   1230
         Width           =   1815
      End
      Begin VB.CommandButton cmdScale 
         Caption         =   "Scale &Pass Database"
         Height          =   375
         Left            =   -74760
         TabIndex        =   61
         ToolTipText     =   "Setup the connection to the Scale Pass Database"
         Top             =   840
         Width           =   1815
      End
      Begin VB.ComboBox cmoPlant 
         Height          =   315
         ItemData        =   "frmSetup.frx":0AE3
         Left            =   1800
         List            =   "frmSetup.frx":0AE5
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1410
         Width           =   2295
      End
      Begin VB.ComboBox cmoMode 
         Height          =   315
         Left            =   1800
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   960
         Width           =   2295
      End
      Begin VB.CheckBox chManualScaling 
         Alignment       =   1  'Right Justify
         Caption         =   "Manual Scaling"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         ToolTipText     =   "Do not use the automatic scale reader."
         Top             =   2880
         Width           =   3855
      End
      Begin VB.ComboBox cmboCypressBOLPrntr 
         Height          =   315
         Left            =   1800
         TabIndex        =   8
         Text            =   "Combo1"
         Top             =   3510
         Width           =   3855
      End
      Begin VB.CheckBox chExtraLabelCert 
         Alignment       =   1  'Right Justify
         Caption         =   "Print Extra Copy of the Label for required COA"
         Height          =   255
         Left            =   -74760
         TabIndex        =   49
         ToolTipText     =   "Print one extra copy of the Label when the COA is required."
         Top             =   2520
         Width           =   3855
      End
      Begin VB.CheckBox chExtraCOA 
         Alignment       =   1  'Right Justify
         Caption         =   "Print Extra Copy of the COA"
         Height          =   255
         Left            =   -74760
         TabIndex        =   42
         ToolTipText     =   "Print one extra copy of the BOL, COA, and Label"
         Top             =   2760
         Width           =   3855
      End
      Begin VB.CheckBox chEnforceCOA 
         Alignment       =   1  'Right Justify
         Caption         =   "Enforce COA"
         Height          =   255
         Left            =   -74760
         TabIndex        =   36
         Top             =   960
         Width           =   3855
      End
      Begin VB.TextBox txtCOACnt 
         Alignment       =   2  'Center
         BeginProperty DataFormat 
            Type            =   1
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
         Height          =   285
         Left            =   -71760
         TabIndex        =   40
         Top             =   1680
         Width           =   855
      End
      Begin VB.ComboBox cmboCOAPrinter 
         Height          =   315
         Left            =   -73200
         TabIndex        =   38
         Text            =   "Combo1"
         Top             =   1320
         Width           =   3855
      End
      Begin VB.ComboBox cmboLabelPrinter 
         Height          =   315
         Left            =   -73200
         TabIndex        =   48
         Text            =   "Combo1"
         Top             =   1680
         Width           =   3855
      End
      Begin VB.CheckBox chPrintLabels 
         Alignment       =   1  'Right Justify
         Caption         =   "Print Labels"
         Height          =   255
         Left            =   -74760
         TabIndex        =   45
         Top             =   960
         Width           =   3855
      End
      Begin VB.Label Label12 
         Caption         =   "IM Sales Return Reason Code"
         Height          =   255
         Left            =   4920
         TabIndex        =   89
         Top             =   2520
         Width           =   2295
      End
      Begin VB.Label Label11 
         Caption         =   "Allowed Src Product Variance"
         Height          =   255
         Left            =   4800
         TabIndex        =   86
         Top             =   1320
         Width           =   2175
      End
      Begin VB.Label lblNote 
         Caption         =   $"frmSetup.frx":0AE7
         Height          =   855
         Left            =   -74400
         TabIndex        =   83
         Top             =   5400
         Width           =   6855
      End
      Begin VB.Label lblTableLise 
         Caption         =   "Tables to Delete Records From"
         Height          =   255
         Left            =   -70920
         TabIndex        =   75
         Top             =   1320
         Width           =   3135
      End
      Begin VB.Label lblTonsVariance 
         Caption         =   "Max Tons Variance"
         Height          =   255
         Left            =   -71400
         TabIndex        =   69
         Top             =   3840
         Width           =   1455
      End
      Begin VB.Label lblStandardTons 
         Caption         =   "Standard Tons"
         Height          =   255
         Left            =   -71160
         TabIndex        =   65
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label lblNbrCopies 
         Caption         =   "Possible Total Number of Labels:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   -74760
         TabIndex        =   52
         Top             =   4200
         Width           =   2895
      End
      Begin VB.Label Label8 
         Caption         =   "Extra COA Printer"
         Height          =   255
         Left            =   -74760
         TabIndex        =   43
         Top             =   3030
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Scale Printer"
         Height          =   255
         Left            =   -74760
         TabIndex        =   57
         Top             =   1005
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "Alternate Printer"
         Height          =   255
         Left            =   -74760
         TabIndex        =   59
         Top             =   1815
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Pla&nt"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   1470
         Width           =   1335
      End
      Begin VB.Label Label6 
         Caption         =   "Mode"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   1020
         Width           =   1335
      End
      Begin VB.Label Label7 
         Caption         =   "Cypress Printer"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   3510
         Width           =   1455
      End
      Begin VB.Label lblCOACnt 
         Caption         =   "# Of COA Copies:"
         Height          =   255
         Left            =   -74760
         TabIndex        =   39
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label lblCOA 
         Caption         =   "COA Printer"
         Height          =   255
         Left            =   -74760
         TabIndex        =   37
         Top             =   1350
         Width           =   1455
      End
      Begin VB.Label lblLablePrinter 
         Caption         =   "Label Printer"
         Height          =   255
         Left            =   -74760
         TabIndex        =   47
         Top             =   1710
         Width           =   1455
      End
   End
   Begin VB.CommandButton cmdSave 
      Caption         =   "&Save"
      Height          =   375
      Left            =   8640
      TabIndex        =   80
      TabStop         =   0   'False
      Top             =   3120
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   8640
      TabIndex        =   81
      TabStop         =   0   'False
      Top             =   3720
      Width           =   1335
   End
   Begin MSComDlg.CommonDialog cdDDEPath 
      Left            =   2040
      Top             =   -2400
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Select the DDE Application"
      Filter          =   "DDE (DDE.exe)|DDE.exe|Exacutables (*.exe)|*.exe|All (*.*)|*.*"
   End
End
Attribute VB_Name = "frmSetup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private lcPlant As String                'Name of the plant using scale pass
Private lcPlantPrefix As String          'Number of the plant using scale pass
Private lcSCALEPASSPRINTER As String     'Name of the Scale Pass Printer
Private lcBLPRINTER As String            'Name of the BOL printer
Private lcALTERNATEPRINTER As String     'Name of the Alternate printer
'Private lcDAYSTOKEEP As Integer          '???
Private lcMANUALSCALING As Boolean       'Determines if plant is hooked to a scale
Private lcBOLCypressPRINTER As String     'Name of the BOL Cypress Printer

Private lcCOAPrinter As String         'Printer for the COA     'RKL DeJ 9/25/13
Private lcLablePrinter As String         'Printer for the Sample Lables     'RKL DeJ 9/25/13
Private lcNbrOfCOACopies As Integer      'Number of COA copies to print     'RKL DEJ 9/25/13
Private lcPrintLabels As Boolean     'Allow Lable Printing              'RKL DEJ 9/25/13
Private lcPrintLabelAfterBOL As Boolean     'Allow Lable Printing              'RKL DEJ 9/25/13

Private lcExtraBOLPrinter As String         'Extra BOL Printer     'RKL DeJ 9/25/13
Private lcExtraCOAPrinter As String         'Extra BOL Printer     'RKL DeJ 9/25/13
Private lcExtraLabelPrinter As String         'Extra BOL Printer     'RKL DeJ 9/25/13
Private lcAltBOLPrinter As String           'Alternate BOL Printer RKL DEJ 2016-06-01

Private lcEnforceCOA As Boolean
Private lcPrintExtraBOL As Boolean   'Print an extra copy of the BOL, COA, And Label 'RKL DEJ 9/25/13
Private lcPrintExtraBOLCOAOnly As Boolean   'Print an extra copy of the BOL, COA, And Label 'RKL DEJ 9/25/13
Private lcPrintExtraCOA As Boolean   'Print an extra copy of the BOL, COA, And Label 'RKL DEJ 9/25/13
Private lcPrintExtraLblCert As Boolean   'Print an extra copy of the BOL, COA, And Label 'RKL DEJ 9/25/13

Private lcPrintExtraLblRisk As Boolean   'Print an extra copy of the Label 'RKL DEJ 9/25/13
Private lcPrintExtraLblWY As Boolean   'Print an extra copy of the Label 'RKL DEJ 9/25/13

Private lcHideOverShip As Boolean    'RkL DEJ 6/12/14
Private lcShowLMEMsg As Boolean    'RkL DEJ 01/07/15
Private lcAllowGetLMEMsg As Boolean    'RkL DEJ 01/07/15
Private lcDriverOn As Boolean    'RkL DEJ 12/17/14

Private lcScaleInWarnTons As Boolean    'RkL DEJ 04/23/2015
Private lcScaleOutWarnVar As Boolean    'RkL DEJ 04/23/2015
Private lcProcessBOLWarnVar As Boolean    'RkL DEJ 04/23/2015
Private lcStdTon As Integer   'RkL DEJ 04/23/2015
Private lcMaxTonVar As Integer   'RkL DEJ 04/23/2015

Private lcCustOwnedProd As Boolean   'RkL DEJ 2017-12-04
Private lcAutoPostIMTrans As Boolean   'RkL DEJ 2017-12-04
Private lcNotAllowSplitLoads As Boolean    'RKL DEJ 2017-12-16
Private lcSrcVariance As Double   'RkL DEJ 2017-12-16
Private lcIMTranReasonCode As String    'RKL DEJ 2017-12-16
Private lcForceVarianceCheck As Boolean   'RkL DEJ 2017-12-04

Private lcPrintPASSPatent As Boolean  'RKL DEJ 2016-05-03
Private lcPASSPatent As String  'RKL DEJ 2016-05-03
Private lcBOLAddrPhone As String  'RKL DEJ 2016-05-03

Private lcRequireLotNbr As Boolean

'May not need these
Private lcMODE As String             'Debug/Live
Private lcPrintBOLCypress As Boolean '
Private lcPrintCOACypress As Boolean '

Private lcAutoRtnOpenLoads As Boolean
Private lcAutoRtrnSecnds As Integer

'Scale Pass Database
Private lcScaleConnStr As String
Private lcScaleDSN As String
Private lcScaleUID As String
Private lcScalePWD As String
Private lcScaleDB As String
Private lcScaleDriver As String
Private lcScaleServer As String
Private lcScaleAdlPrty As String
Private lcScaleWinAuth As Boolean

Private BOLRS As New ADODB.Recordset    'RKL DEJ 2016-04-29
Private lcSettingsConn As New ADODB.Connection    'RKL DEJ 2016-04-29

'McLeod Database
Private lcMcLeodConnStr As String
Private lcMcLeodDSN As String
Private lcMcLeodUID As String
Private lcMcLeodPWD As String
Private lcMcLeodDB As String
Private lcMcLeodDriver As String
Private lcMcLeodServer As String
Private lcMcLeodAdlPrty As String
Private lcMcLeodWinAuth As Boolean

'MAS 500 Database
Private lcMASConnStr As String
Private lcMASDSN As String
Private lcMASUID As String
Private lcMASPWD As String
Private lcMASDB As String
Private lcMASDriver As String
Private lcMASServer As String
Private lcMASAdlPrty As String
Private lcMASWinAuth As Boolean

'RKL DEJ 1/7/15 added method chAllowGetLMEMsg_Click
Private Sub chAllowGetLMEMsg_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chAllowGetLMEMsg.Value = vbChecked Then
        lcAllowGetLMEMsg = True
    Else
        lcAllowGetLMEMsg = False
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chAllowGetLMEMsg_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

'RKL DEJ 2017-12-16 added chForceVarianceCheck_Click
Private Sub chForceVarianceCheck_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chForceVarianceCheck.Value = vbChecked Then
        lcForceVarianceCheck = True
    Else
        lcForceVarianceCheck = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chForceVarianceCheck_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

'RKL DEJ 2017-12-16 added chNotAllowSplitLoads_Click
Private Sub chNotAllowSplitLoads_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chNotAllowSplitLoads.Value = vbChecked Then
        lcNotAllowSplitLoads = True
    Else
        lcNotAllowSplitLoads = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chNotAllowSplitLoads_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

'RKL DEJ 2017-12-16 added chAutoPostIMTrans_Click
Private Sub chAutoPostIMTrans_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chAutoPostIMTrans.Value = vbChecked Then
        lcAutoPostIMTrans = True
    Else
        lcAutoPostIMTrans = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chAutoPostIMTrans_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chAutoReturn_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chAutoReturn.Value = vbChecked Then
        lcAutoRtnOpenLoads = True
    Else
        lcAutoRtnOpenLoads = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chAutoReturn_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub



Private Sub chBOLCypress_Click()

    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chBOLCypress.Value = vbChecked Then
        lcPrintBOLCypress = True
    Else
        lcPrintBOLCypress = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chBOLCypress_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub



'2017-12-04 Added for Coolage CC1
Private Sub chCustOwned_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chCustOwned.Value = vbChecked Then
        lcCustOwnedProd = True
        chAutoPostIMTrans.Enabled = True
    Else
        lcCustOwnedProd = False
        chAutoPostIMTrans.Enabled = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chCustOwned_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub chDriverOn_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chDriverOn.Value = vbChecked Then
        lcDriverOn = True
    Else
        lcDriverOn = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chDriverOn_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chEnforceCOA_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chEnforceCOA.Value = vbChecked Then
        lcEnforceCOA = True
    Else
        lcEnforceCOA = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chEnforceCOA_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chEnforceCOA_GotFocus()
    SSTab1.Tab = 2

End Sub

Private Sub chExtraBOL_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chExtraBOL.Value = vbChecked Then
        lcPrintExtraBOL = True
    Else
        lcPrintExtraBOL = False
    End If
    
    cmboExtraBOLPrinter.Enabled = lcPrintExtraBOL
    chExtraBOLCOAOnly.Enabled = lcPrintExtraBOL
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chExtraBOL_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chExtraBOL_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chExtraBOLCOAOnly_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chExtraBOLCOAOnly.Value = vbChecked Then
        lcPrintExtraBOLCOAOnly = True
    Else
        lcPrintExtraBOLCOAOnly = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chExtraBOLCOAOnly_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chExtraBOLCOAOnly_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chExtraCOA_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chExtraCOA.Value = vbChecked Then
        lcPrintExtraCOA = True
    Else
        lcPrintExtraCOA = False
    End If
    
    cmboExtraCOAPrinter.Enabled = lcPrintExtraCOA
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chExtraCOA_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chExtraLabelCert_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chExtraLabelCert.Value = vbChecked Then
        lcPrintExtraLblCert = True
    Else
        lcPrintExtraLblCert = False
    End If
    
    Call CalculateLabelCnt
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chExtraLabelCert_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub


Private Sub chExtraLabelRisk_Click()

    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chExtraLabelRisk.Value = vbChecked Then
        lcPrintExtraLblRisk = True
    Else
        lcPrintExtraLblRisk = False
    End If
    
    Call CalculateLabelCnt
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chExtraLabelRisk_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub chExtraLabelWY_Click()

    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chExtraLabelWY.Value = vbChecked Then
        lcPrintExtraLblWY = True
    Else
        lcPrintExtraLblWY = False
    End If
    
    Call CalculateLabelCnt
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chExtraLabelWY_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub chExtraLabelWY_LostFocus()
'    SSTab1.Tab = 4

End Sub

Private Sub chHideOverShip_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chHideOverShip.Value = vbChecked Then
        lcHideOverShip = True
    Else
        lcHideOverShip = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chHideOverShip_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chManualScaling_Click()
    gbLastActivityTime = Date + Time
    
    If chManualScaling.Value = vbChecked Then
        lcMANUALSCALING = True
    Else
        lcMANUALSCALING = False
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chPrcessBOLVarWrn_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chPrcessBOLVarWrn.Value = vbChecked Then
        lcProcessBOLWarnVar = True
    Else
        lcProcessBOLWarnVar = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chPrcessBOLVarWrn_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub chPrintLabelAfterBOL_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chPrintLabelAfterBOL.Value = vbChecked Then
        lcPrintLabelAfterBOL = True
    Else
        lcPrintLabelAfterBOL = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chPrintLabelAfterBOL_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub



Private Sub chPrintLabels_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chPrintLabels.Value = vbChecked Then
        lcPrintLabels = True
    Else
        lcPrintLabels = False
    End If
    
    Call CalculateLabelCnt
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chPrintLabels_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chPrintLabels_GotFocus()
    SSTab1.Tab = 3

End Sub

Private Sub chPrintPASSPatent_Click()
    On Error GoTo Error
    
    If chPrintPASSPatent.Value = vbChecked Then
        txtPASSPatent.Enabled = True
        cmdRestPASSPatent.Enabled = True
        lcPrintPASSPatent = True
    Else
        txtPASSPatent.Enabled = False
        cmdRestPASSPatent.Enabled = False
        lcPrintPASSPatent = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chPrintPASSPatent_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chPrintPASSPatent_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chPrntCOACypress_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chPrntCOACypress.Value = vbChecked Then
        lcPrintCOACypress = True
    Else
        lcPrintCOACypress = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chPrntCOACypress_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chRequireLotNbr_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chRequireLotNbr.Value = vbChecked Then
        lcRequireLotNbr = True
    Else
        lcRequireLotNbr = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chRequireLotNbr_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub chScaleInTonsWarn_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chScaleInTonsWarn.Value = vbChecked Then
        lcScaleInWarnTons = True
    Else
        lcScaleInWarnTons = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chScaleInTonsWarn_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub chScaleInTonsWarn_GotFocus()
    SSTab1.Tab = 6

End Sub

Private Sub chScaleOutVarWarning_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chScaleOutVarWarning.Value = vbChecked Then
        lcScaleOutWarnVar = True
    Else
        lcScaleOutWarnVar = False
    End If
    
    gbLastActivityTime = Date + Time
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chScaleOutVarWarning_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

'RKL DEJ 1/7/15 added method chShowLMEMsg_Click
Private Sub chShowLMEMsg_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    If chShowLMEMsg.Value = vbChecked Then
        lcShowLMEMsg = True
        chAllowGetLMEMsg.Enabled = True
    Else
        lcShowLMEMsg = False
        chAllowGetLMEMsg.Enabled = False
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".chShowLMEMsg_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboAltBOLPrinter_Change()
    lcAltBOLPrinter = cmboAltBOLPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboAltBOLPrinter_Click()
    lcAltBOLPrinter = cmboAltBOLPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboAltBOLPrinter_LostFocus()
    lcAltBOLPrinter = cmboAltBOLPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboAlternatePrntr_Click()
    lcALTERNATEPRINTER = cmboAltBOLPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboAlternatePrntr_Change()
    lcALTERNATEPRINTER = cmboAlternatePrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboAlternatePrntr_LostFocus()
'    SSTab1.Tab = 5

End Sub

Private Sub cmboBOLPrntr_Click()
    lcBLPRINTER = cmboBOLPrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboBOLPrntr_Change()
    lcBLPRINTER = cmboBOLPrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboBOLPrntr_GotFocus()
    SSTab1.Tab = 1

End Sub

Private Sub cmboCOAPrinter_Change()
    lcCOAPrinter = cmboCOAPrinter.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboCOAPrinter_Click()
    lcCOAPrinter = cmboCOAPrinter.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboCypressBOLPrntr_Change()
    lcBOLCypressPRINTER = cmboCypressBOLPrntr.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboCypressBOLPrntr_Click()
    lcBOLCypressPRINTER = cmboCypressBOLPrntr.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboExtraBOLPrinter_Change()
    lcExtraBOLPrinter = cmboExtraBOLPrinter.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboExtraBOLPrinter_Click()
    lcExtraBOLPrinter = cmboExtraBOLPrinter.Text

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboExtraBOLPrinter_LostFocus()
'    SSTab1.Tab = 2

End Sub

Private Sub cmboExtraCOAPrinter_Change()
    lcExtraCOAPrinter = cmboExtraCOAPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboExtraCOAPrinter_Click()
    lcExtraCOAPrinter = cmboExtraCOAPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboExtraCOAPrinter_LostFocus()
'    SSTab1.Tab = 3

End Sub

Private Sub cmboExtraLabelPrinter_Change()
    lcExtraLabelPrinter = cmboExtraLabelPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboExtraLabelPrinter_Click()
    lcExtraLabelPrinter = cmboExtraLabelPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboLabelPrinter_Change()
    lcLablePrinter = cmboLabelPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboLabelPrinter_Click()
    lcLablePrinter = cmboLabelPrinter.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboScalePrntr_Click()
    lcSCALEPASSPRINTER = cmboScalePrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboScalePrntr_Change()
    lcSCALEPASSPRINTER = cmboScalePrntr.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmboScalePrntr_GotFocus()
    SSTab1.Tab = 4

End Sub

Private Sub cmboSeconds_Change()
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_Change()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time

End Sub

Private Sub cmboSeconds_Click()
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub cmboSeconds_LostFocus()
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    
    gbLastActivityTime = Date + Time
    
'    SSTab1.Tab = 1
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_LostFocus()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub cmboSeconds_Validate(Cancel As Boolean)
    On Error GoTo Error
    
    lcAutoRtrnSecnds = CInt(cmboSeconds.Text)
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
    
Error:
    MsgBox Me.Name & ".cmboSeconds_Validate()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time


End Sub

Private Sub cmdAddBOLPrnt_Click()
    On Error GoTo Error

    BOLRS.AddNew
    BOLRS("CopyNbr").Value = BOLRS.RecordCount
    BOLRS("PrintCaption").Value = ""
    
    BOLRS.Sort = "[CopyNbr] Asc"
    
    BOLRS.MoveLast
    
    grdBOLPrint.ReBind

    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdAddBOLPrnt_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdBOLDflts_Click()
    On Error GoTo Error
    
    Dim i As Integer
    
    'Delete all current settings.
    While BOLRS.RecordCount > 0
        BOLRS.MoveFirst
        BOLRS.Delete
    Wend
    
    'Create the six base prints.
    For i = 1 To 6
        Select Case i
            Case 1
                BOLRS.AddNew
                BOLRS("CopyNbr").Value = i
                BOLRS("PrintCaption").Value = "Idaho Falls Office Copy"
                
            Case 2
                BOLRS.AddNew
                BOLRS("CopyNbr").Value = i
                BOLRS("PrintCaption").Value = "Customer Copy"
                
            Case 3
                BOLRS.AddNew
                BOLRS("CopyNbr").Value = i
                BOLRS("PrintCaption").Value = "Project Engineer Copy"
                
            Case 4
                BOLRS.AddNew
                BOLRS("CopyNbr").Value = i
                BOLRS("PrintCaption").Value = "State Copy"
                
            Case 5
                BOLRS.AddNew
                BOLRS("CopyNbr").Value = i
                BOLRS("PrintCaption").Value = "Transporter Copy"
                
            Case 6
                BOLRS.AddNew
                BOLRS("CopyNbr").Value = i
                BOLRS("PrintCaption").Value = "Plant Copy"
                
        End Select
    Next
    
    BOLRS.Sort = "[CopyNbr] Asc"
    BOLRS.MoveFirst
    
    grdBOLPrint.ReBind
    grdBOLPrint.Refresh
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdBOLDflts_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdCancel_Click()
    gbLastActivityTime = Date + Time
    
    Unload Me

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdDelBOLPrnt_Click()
    On Error GoTo Error
    Dim iPrintOrder As Integer
    
    If BOLRS.State <> 1 Then
        Exit Sub
    End If
    
    If BOLRS.EOF Then
        Exit Sub
    End If
    
    If BOLRS.BOF Then
        Exit Sub
    End If
    
    BOLRS.Delete
    
    If BOLRS.RecordCount > 0 Then
        BOLRS.MoveFirst
            
        iPrintOrder = 1
        
        While Not BOLRS.EOF
            BOLRS("CopyNbr").Value = iPrintOrder
            
            iPrintOrder = iPrintOrder + 1
            BOLRS.MoveNext
        Wend
        
        BOLRS.Sort = "[CopyNbr] Asc"
        
        BOLRS.MoveFirst
    End If

    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdDelBOLPrnt_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time

End Sub



Private Sub cmdDelRecords_Click()
    On Error GoTo Error
    
    Dim sSQL As String
    Dim lcConn As New ADODB.Connection
    Dim lLoadRecs As Long
    Dim lPrintRecs As Long
    Dim lErrorRecs As Long
    
    If Trim(lcScaleConnStr) = Empty Then
        MsgBox "You need to setup the Scale Pass database connection first.", vbInformation, "Scale Pass"
        Exit Sub
    End If
    
    If chLoads.Value <> vbChecked And chBOLPrinting.Value <> vbChecked And chBOLPrintingErrors.Value <> vbChecked Then
        MsgBox "You need to select the table(s) to delete records from first.", vbInformation, "Scale Pass"
        Exit Sub
    End If
    
    If optionDelAll.Value <> True And optionDelDate.Value <> True Then
        MsgBox "You need to select the Delete Option first.", vbInformation, "Scale Pass"
        Exit Sub
    End If
    
    If MsgBox("Are you sure you want to delete these records?  This will permanantly delete the records from the database.", vbYesNo, "Scale Pass") <> vbYes Then
        Exit Sub
    End If
    
    'Open a connection to the database
    lcConn.ConnectionString = lcScaleConnStr
    lcConn.CursorLocation = adUseClient
    lcConn.IsolationLevel = adXactChaos
    lcConn.Open
    
    'Delete Loads Data
    If chLoads.Value = vbChecked Then
        If optionDelAll.Value = True Then
            sSQL = "Delete * from Loads"
        Else
            sSQL = "Delete * from Loads where TimeIn < #" & dtDelOlder.Value & "#"
        End If
        
        lcConn.Execute sSQL, lLoadRecs
    End If
    
'    'Delete BOLPrinting Data
'    'We don't neet this one because of cascase delete on the database
'    If chBOLPrinting.Value = vbChecked Then
'        If optionDelAll.Value = True Then
'            sSQL = "Delete * from BOLPrinting"
'        Else
'            sSQL = "Delete * from BOLPrinting where LoadTime < #" & dtDelOlder.Value & "#"
'        End If
'
'        lcConn.Execute sSQL, lPrintRecs
'    End If
    
    'Delete Loads Data
    If chBOLPrintingErrors.Value = vbChecked Then
        If optionDelAll.Value = True Then
            sSQL = "Delete * from BOLPrintingErrors"
        Else
            sSQL = "Delete * from BOLPrintingErrors where ErrorDate < #" & dtDelOlder.Value & "#"
        End If
        
        lcConn.Execute sSQL, lErrorRecs
    End If
    
    
    gbLastActivityTime = Date + Time
    'On success Remind the user:
    MsgBox "The records have been successfully deleted." & vbCrLf & vbCrLf & _
    lLoadRecs & " Loads records were deleted." & vbCrLf & _
    lErrorRecs & " Error records were deleted." & vbCrLf & _
    "You will need to close the Scale Pass program and open the Scale Pass Access database and do a 'Compact and Repare' on the database, to obtain best performance.", vbInformation, "Scale Pass"
        
CleanUP:
    gbLastActivityTime = Date + Time
    Err.Clear
    
    If lcConn.State = 1 Then lcConn.Close
    Set lcConn = Nothing
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdDelRecords_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Sub

Private Sub cmdMAS_Click()
    On Error GoTo Error
    
    Dim frm As New frmODBCLogon
    
    gbLastActivityTime = Date + Time
    
    frm.ClearProperties
    frm.GetDSNsAndDrivers
    
    frm.ConnString = lcMASConnStr
    frm.DSN = lcMASDSN
    frm.UID = lcMASUID
    frm.Password = lcMASPWD
    frm.Database = lcMASDB
    frm.Driver = lcMASDriver
    frm.Server = lcMASServer
    frm.AddlProperties = lcMASAdlPrty
    frm.WinAuth = lcMASWinAuth
    
    frm.Show vbModal, Me
    
    lcMASConnStr = frm.ConnString
    lcMASDSN = frm.DSN
    lcMASUID = frm.UID
    lcMASPWD = frm.Password
    lcMASDB = frm.Database
    lcMASDriver = frm.Driver
    lcMASServer = frm.Server
    lcMASAdlPrty = frm.AddlProperties
    lcMASWinAuth = frm.WinAuth
    
    Set frm = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdMAS_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdMcLeod_Click()
    On Error GoTo Error
    
    Dim frm As New frmODBCLogon
    
    gbLastActivityTime = Date + Time
    
    frm.ClearProperties
    frm.GetDSNsAndDrivers
    
    frm.ConnString = lcMcLeodConnStr
    frm.DSN = lcMcLeodDSN
    frm.UID = lcMcLeodUID
    frm.Password = lcMcLeodPWD
    frm.Database = lcMcLeodDB
    frm.Driver = lcMcLeodDriver
    frm.Server = lcMcLeodServer
    frm.AddlProperties = lcMcLeodAdlPrty
    frm.WinAuth = lcMcLeodWinAuth
    
    frm.Show vbModal, Me
    
    lcMcLeodConnStr = frm.ConnString
    lcMcLeodDSN = frm.DSN
    lcMcLeodUID = frm.UID
    lcMcLeodPWD = frm.Password
    lcMcLeodDB = frm.Database
    lcMcLeodDriver = frm.Driver
    lcMcLeodServer = frm.Server
    lcMcLeodAdlPrty = frm.AddlProperties
    lcMcLeodWinAuth = frm.WinAuth
    
    Set frm = Nothing
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdMcLeod_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdMcLeod_LostFocus()
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdRestoreBOLAddrDflt_Click()
    txtBOLAddrPhone.Text = "P.O. Box 50538 * Idaho Falls, ID 83405 * (208) 524-5871"
End Sub

Private Sub cmdRestoreBOLAddrDflt_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdRestPASSPatent_Click()
    txtPASSPatent.Text = "PASS Patent # 5, 180, 428 and Other Patents Pending, PASS is a Registered Trademark"
End Sub

Private Sub cmdRestPASSPatent_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdSave_Click()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    'update global properties/settings
    Call UpdateGlobalVar
    
    Call SaveSettings
    
    'SAVE BOL Print Settings    'RKL DEJ 2016-04-29
    Call SaveBOLPrintSettings  'RKL DEJ 2016-04-29
    
    Unload Me
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdSave_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdScale_Click()
    On Error GoTo Error
    
    Dim frm As New frmODBCLogon
    Dim currentConnStr As String    'RKL DEJ 2016-04-29
    
    gbLastActivityTime = Date + Time
    
    currentConnStr = Trim(UCase(lcScaleConnStr))
    
    frm.ClearProperties
    frm.GetDSNsAndDrivers
    
    frm.ConnString = lcScaleConnStr
    frm.DSN = lcScaleDSN
    frm.UID = lcScaleUID
    frm.Password = lcScalePWD
    frm.Database = lcScaleDB
    frm.Driver = lcScaleDriver
    frm.Server = lcScaleServer
    frm.AddlProperties = lcScaleAdlPrty
    frm.WinAuth = lcScaleWinAuth
    
    frm.Show vbModal, Me
    
    lcScaleConnStr = frm.ConnString
    lcScaleDSN = frm.DSN
    lcScaleUID = frm.UID
    lcScalePWD = frm.Password
    lcScaleDB = frm.Database
    lcScaleDriver = frm.Driver
    lcScaleServer = frm.Server
    lcScaleAdlPrty = frm.AddlProperties
    lcScaleWinAuth = frm.WinAuth
    
    Set frm = Nothing
    
    If currentConnStr <> Trim(UCase(lcScaleConnStr)) Then
        'Only Save if the connection string is different.
        Call OpenBOLRS  'RKL DEJ 2016-04-29
    End If
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdScale_Click()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdScale_GotFocus()
    SSTab1.Tab = 5
End Sub




Private Sub cmoMode_GotFocus()
    SSTab1.Tab = 0
End Sub

Private Sub cmoMode_Validate(Cancel As Boolean)
    lcMODE = cmoMode.Text

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmoPlant_Validate(Cancel As Boolean)
    lcPlant = cmoPlant.Text
    lcPlantPrefix = cmoPlant.ItemData(cmoPlant.ListIndex)

    gbLastActivityTime = Date + Time
End Sub



Private Sub Form_Load()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    chAllowGetLMEMsg.Enabled = False    'RKL DEJ 1/7/15
    
    Call PopulateCombos
    
    Call SetupLocalVar
    
    Call SetupFrmObjects
    
    Call OpenBOLRS   'RKL DEJ 2016-04-29
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".Form_Load()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Sub SetupLocalVar()
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    lcPlant = gbPlant
    lcPlantPrefix = gbPlantPrefix
    lcSCALEPASSPRINTER = gbSCALEPASSPRINTER
    lcBLPRINTER = gbBLPRINTER
    lcALTERNATEPRINTER = gbALTERNATEPRINTER
'    lcDAYSTOKEEP = gbDAYSTOKEEP
    lcMANUALSCALING = gbMANUALSCALING
    lcBOLCypressPRINTER = gbBOLCypressPRINTER
    
    lcCOAPrinter = gbCOAPrinter                 'RKL DEJ 9/25/13
    lcLablePrinter = gbLablePrinter             'RKL DEJ 9/25/13
    lcExtraBOLPrinter = gbExtraBOLPrinter             'RKL DEJ 9/25/13
    lcExtraCOAPrinter = gbExtraCOAPrinter             'RKL DEJ 9/25/13
    lcExtraLabelPrinter = gbExtraLabelPrinter             'RKL DEJ 9/25/13
    lcNbrOfCOACopies = gbNbrOfCOACopies         'RKL DEJ 9/25/13
    lcPrintLabels = gbPrintLabels               'RKL DEJ 9/25/13
    lcPrintLabelAfterBOL = gbPrintLabelAfterBOL               'RKL DEJ 9/25/13
    
    lcAltBOLPrinter = gbAltBOLPrinter           'RKL DEJ 2016-06-11
    
    lcPrintExtraBOL = gbPrintExtraBOL     'RKL DEJ 9/25/13
    lcPrintExtraBOLCOAOnly = gbPrintExtraBOLCOAOnly     'RKL DEJ 9/25/13
    lcPrintExtraCOA = gbPrintExtraCOA     'RKL DEJ 9/25/13
    lcPrintExtraLblCert = gbPrintExtraLblCert     'RKL DEJ 9/25/13
    lcPrintExtraLblRisk = gbPrintExtraLblRisk     'RKL DEJ 9/25/13
    lcPrintExtraLblWY = gbPrintExtraLblWY     'RKL DEJ 9/25/13
    
    lcHideOverShip = gbHideOverShip 'RKL DEJ 6/12/14
    lcShowLMEMsg = gbShowLMEMsg 'RKL DEJ 01/07/15
    lcAllowGetLMEMsg = gbAllowGetLMEMsg 'RKL DEJ 01/07/15
    
    lcDriverOn = gbDriverOn 'RKL DEJ 12/17/14
    
    lcCustOwnedProd = gbCustOwnedProd 'RKL DEJ 2017-12-04
    lcAutoPostIMTrans = gbAutoPostIMTrans 'RKL DEJ 2017-12-16
    lcNotAllowSplitLoads = gbNotAllowSplitLoads 'RKL DEJ 2017-12-16
    lcSrcVariance = gbSrcVariance 'RKL DEJ 2017-12-16
    lcIMTranReasonCode = gbIMTranReasonCode 'RKL DEJ 2017-12-16
    lcForceVarianceCheck = gbForceVarianceCheck 'RKL DEJ 2017-12-18
        
    lcScaleInWarnTons = gbScaleInWarnTons 'RKL DEJ 4/23/15
    lcScaleOutWarnVar = gbScaleOutWarnVar 'RKL DEJ 4/23/15
    lcProcessBOLWarnVar = gbProcessBOLWarnVar 'RKL DEJ 4/23/15
    lcStdTon = gbStdTon 'RKL DEJ 4/23/15
    lcMaxTonVar = gbMaxTonVar 'RKL DEJ 4/23/15
    
    lcPrintPASSPatent = gbPrintPASSPatent 'RKL DEJ 2016-05-03
    lcPASSPatent = gbPASSPatent 'RKL DEJ 2016-05-03
    lcBOLAddrPhone = gbBOLAddrPhone 'RKL DEJ 2016-05-03
    
    lcEnforceCOA = gbEnforceCOA
    lcRequireLotNbr = gbRequireLotNbr
    
    lcAutoRtnOpenLoads = gbAutoRtnOpenLoads
    lcAutoRtrnSecnds = gbAutoRtrnSecnds
    
    'May not need these
    lcMODE = gbMODE
    
    'Cypress
    lcPrintBOLCypress = gbPrintBOLCypress
    lcPrintCOACypress = gbPrintCOACypress
        
    'Scale Pass Database
    lcScaleConnStr = gbScaleConnStr
    lcScaleDSN = gbScaleDSN
    lcScaleUID = gbScaleUID
    lcScalePWD = gbScalePWD
    lcScaleDB = gbScaleDB
    lcScaleDriver = gbScaleDriver
    lcScaleServer = gbScaleServer
    lcScaleAdlPrty = gbScaleAdlPrty
    lcScaleWinAuth = gbScaleWinAuth
    
    'McLeod Database
    lcMcLeodConnStr = gbMcLeodConnStr
    lcMcLeodDSN = gbMcLeodDSN
    lcMcLeodUID = gbMcLeodUID
    lcMcLeodPWD = gbMcLeodPWD
    lcMcLeodDB = gbMcLeodDB
    lcMcLeodDriver = gbMcLeodDriver
    lcMcLeodServer = gbMcLeodServer
    lcMcLeodAdlPrty = gbMcLeodAdlPrty
    lcMcLeodWinAuth = gbMcLeodWinAuth
    
    'MAS 500 Database
    lcMASConnStr = gbMASConnStr
    lcMASDSN = gbMASDSN
    lcMASUID = gbMASUID
    lcMASPWD = gbMASPWD
    lcMASDB = gbMASDB
    lcMASDriver = gbMASDriver
    lcMASServer = gbMASServer
    lcMASAdlPrty = gbMASAdlPrty
    lcMASWinAuth = gbMASWinAuth
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SetupLocalVar()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Sub UpdateGlobalVar()
    On Error GoTo Error
    gbLastActivityTime = Date + Time
    
    gbPlant = lcPlant
    gbPlantPrefix = lcPlantPrefix
    gbSCALEPASSPRINTER = lcSCALEPASSPRINTER
    gbBLPRINTER = lcBLPRINTER
    gbALTERNATEPRINTER = lcALTERNATEPRINTER
'    gbDAYSTOKEEP = lcDAYSTOKEEP
    gbMANUALSCALING = lcMANUALSCALING
    gbBOLCypressPRINTER = lcBOLCypressPRINTER
    
    gbCOAPrinter = lcCOAPrinter                     'RKL DEJ 9/25/13
    gbLablePrinter = lcLablePrinter                 'RKL DEJ 9/25/13
    gbExtraBOLPrinter = lcExtraBOLPrinter             'RKL DEJ 9/25/13
    gbExtraCOAPrinter = lcExtraCOAPrinter             'RKL DEJ 9/25/13
    gbExtraLabelPrinter = lcExtraLabelPrinter         'RKL DEJ 9/25/13
    gbNbrOfCOACopies = lcNbrOfCOACopies             'RKL DEJ 9/25/13
    gbPrintLabels = lcPrintLabels                   'RKL DEJ 9/25/13
    gbPrintLabelAfterBOL = lcPrintLabelAfterBOL     'RKL DEJ 9/25/13
    gbAltBOLPrinter = lcAltBOLPrinter               'RKL DEJ 2016-06-11

    gbPrintExtraBOL = lcPrintExtraBOL         'RKL DEJ 9/25/13
    gbPrintExtraBOLCOAOnly = lcPrintExtraBOLCOAOnly         'RKL DEJ 9/25/13
    gbPrintExtraCOA = lcPrintExtraCOA         'RKL DEJ 9/25/13
    gbPrintExtraLblCert = lcPrintExtraLblCert         'RKL DEJ 9/25/13
    gbPrintExtraLblRisk = lcPrintExtraLblRisk         'RKL DEJ 9/25/13
    gbPrintExtraLblWY = lcPrintExtraLblWY         'RKL DEJ 9/25/13
    gbHideOverShip = lcHideOverShip 'RKL DEJ 6/12/14
    gbShowLMEMsg = lcShowLMEMsg 'RKL DEJ 01/07/15
    gbAllowGetLMEMsg = lcAllowGetLMEMsg 'RKL DEJ 01/07/15
    
    gbDriverOn = lcDriverOn 'RKL DEJ 12/17/14
    
    gbCustOwnedProd = lcCustOwnedProd 'RKL DEJ 2017-12-04
    gbAutoPostIMTrans = lcAutoPostIMTrans 'RKL DEJ 2017-12-16
    gbNotAllowSplitLoads = lcNotAllowSplitLoads 'RKL DEJ 2017-12-16
    gbSrcVariance = lcSrcVariance 'RKL DEJ 2017-12-16
    gbIMTranReasonCode = lcIMTranReasonCode 'RKL DEJ 2017-12-16
    gbForceVarianceCheck = lcForceVarianceCheck 'RKL DEJ 2017-12-18

    
    gbScaleInWarnTons = lcScaleInWarnTons 'RKL DEJ 4/23/15
    gbScaleOutWarnVar = lcScaleOutWarnVar 'RKL DEJ 4/23/15
    gbProcessBOLWarnVar = lcProcessBOLWarnVar 'RKL DEJ 4/23/15
    gbStdTon = lcStdTon 'RKL DEJ 4/23/15
    gbMaxTonVar = lcMaxTonVar 'RKL DEJ 4/23/15
    
    gbPrintPASSPatent = lcPrintPASSPatent 'RKL DEJ 4/23/15
    gbPASSPatent = lcPASSPatent 'RKL DEJ 4/23/15
    gbBOLAddrPhone = lcBOLAddrPhone 'RKL DEJ 4/23/15
    
    gbEnforceCOA = lcEnforceCOA
    gbRequireLotNbr = lcRequireLotNbr


    'May not need these
    gbMODE = lcMODE
    gbPrintBOLCypress = lcPrintBOLCypress
    gbPrintCOACypress = lcPrintCOACypress
    
    gbAutoRtnOpenLoads = lcAutoRtnOpenLoads
    gbAutoRtrnSecnds = lcAutoRtrnSecnds
    
    'Scale Pass Database
    gbScaleConnStr = lcScaleConnStr
    gbScaleDSN = lcScaleDSN
    gbScaleUID = lcScaleUID
    gbScalePWD = lcScalePWD
    gbScaleDB = lcScaleDB
    gbScaleDriver = lcScaleDriver
    gbScaleServer = lcScaleServer
    gbScaleAdlPrty = lcScaleAdlPrty
    gbScaleWinAuth = lcScaleWinAuth
    
    'McLeod Database
    gbMcLeodConnStr = lcMcLeodConnStr
    gbMcLeodDSN = lcMcLeodDSN
    gbMcLeodUID = lcMcLeodUID
    gbMcLeodPWD = lcMcLeodPWD
    gbMcLeodDB = lcMcLeodDB
    gbMcLeodDriver = lcMcLeodDriver
    gbMcLeodServer = lcMcLeodServer
    gbMcLeodAdlPrty = lcMcLeodAdlPrty
    gbMcLeodWinAuth = lcMcLeodWinAuth
    
    'MAS 500 Database
    gbMASConnStr = lcMASConnStr
    gbMASDSN = lcMASDSN
    gbMASUID = lcMASUID
    gbMASPWD = lcMASPWD
    gbMASDB = lcMASDB
    gbMASDriver = lcMASDriver
    gbMASServer = lcMASServer
    gbMASAdlPrty = lcMASAdlPrty
    gbMASWinAuth = lcMASWinAuth
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox Me.Name & ".UpdateGlobalVar()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub

Sub PopulateCombos()
    On Error GoTo Error
    
    Dim Prntr As Printer
    
    gbLastActivityTime = Date + Time
    
    cmoMode.Clear
    cmoMode.AddItem "Live"
    cmoMode.AddItem "Debug"
    
    cmoPlant.Clear
    cmoPlant.AddItem "Billings"
    cmoPlant.ItemData(0) = 10
    cmoPlant.AddItem "Blackfoot"
    cmoPlant.ItemData(1) = 5
    
    cmoPlant.AddItem "Coolidge"
    cmoPlant.ItemData(2) = 16
'    cmoPlant.AddItem "Coolidge Cust Owned"
'    cmoPlant.ItemData(3) = 16
    
    cmoPlant.AddItem "Hauser"
    cmoPlant.ItemData(3) = 4
    cmoPlant.AddItem "Irwindale"
    cmoPlant.ItemData(4) = 12
    
    cmoPlant.AddItem "Nampa"
    cmoPlant.ItemData(5) = 3
    
    cmoPlant.AddItem "North Salt Lake"
    cmoPlant.ItemData(6) = 17
    cmoPlant.AddItem "North Woods Cross"
    cmoPlant.ItemData(7) = 18
    
    cmoPlant.AddItem "Rawlins"
    cmoPlant.ItemData(8) = 7
    cmoPlant.AddItem "Roswell"
    cmoPlant.ItemData(9) = 13
    cmoPlant.AddItem "Temple"
    cmoPlant.ItemData(10) = 15
    cmoPlant.AddItem "Tucson"
    cmoPlant.ItemData(11) = 14
    cmoPlant.AddItem "White City"
    cmoPlant.ItemData(12) = 11
    cmoPlant.AddItem "Woods Cross"
    cmoPlant.ItemData(13) = 6
    
    
    cmboScalePrntr.Clear
    cmboBOLPrntr.Clear
    cmboAlternatePrntr.Clear
    cmboCypressBOLPrntr.Clear
    cmboLabelPrinter.Clear      'RKL DEJ 9/25/13
    cmboCOAPrinter.Clear      'RKL DEJ 9/25/13
    cmboExtraCOAPrinter.Clear      'RKL DEJ 9/25/13
    cmboExtraBOLPrinter.Clear      'RKL DEJ 9/25/13
    cmboExtraLabelPrinter.Clear      'RKL DEJ 9/25/13
    cmboExtraBOLPrinter.Clear       'RKL DEJ 2016-06-01
    cmboAltBOLPrinter.Clear         'RKL DEJ 2016-06-11
    
    For Each Prntr In Printers
        cmboScalePrntr.AddItem Prntr.DeviceName
        cmboBOLPrntr.AddItem Prntr.DeviceName
        cmboAlternatePrntr.AddItem Prntr.DeviceName
        cmboCypressBOLPrntr.AddItem Prntr.DeviceName
        cmboLabelPrinter.AddItem Prntr.DeviceName   'RKL DEJ 9/25/13
        cmboCOAPrinter.AddItem Prntr.DeviceName   'RKL DEJ 9/25/13
        cmboExtraCOAPrinter.AddItem Prntr.DeviceName   'RKL DEJ 9/25/13
        cmboExtraBOLPrinter.AddItem Prntr.DeviceName   'RKL DEJ 9/25/13
        cmboExtraLabelPrinter.AddItem Prntr.DeviceName   'RKL DEJ 9/25/13
        cmboExtraBOLPrinter.AddItem Prntr.DeviceName     'RKL DEJ 2016-06-01
        cmboAltBOLPrinter.AddItem Prntr.DeviceName       'RKL DEJ 2016-06-11
    Next
    
    gbLastActivityTime = Date + Time
    
    Exit Sub
Error:
    MsgBox Me.Name & ".PopulateCombos()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Sub SetupFrmObjects()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    cmoMode.Text = lcMODE
    cmoPlant.Text = lcPlant
    
    cmboScalePrntr.Text = lcSCALEPASSPRINTER
    cmboBOLPrntr.Text = lcBLPRINTER
    cmboAlternatePrntr.Text = lcALTERNATEPRINTER
    cmboCypressBOLPrntr.Text = lcBOLCypressPRINTER
    
    cmboExtraBOLPrinter.Text = lcExtraBOLPrinter    'RKL DEJ 9/25/13
    cmboExtraCOAPrinter.Text = lcExtraCOAPrinter    'RKL DEJ 9/25/13
    cmboAltBOLPrinter.Text = lcAltBOLPrinter      'RKL DEJ 2016-06-01
    
    cmboCOAPrinter.Text = lcCOAPrinter          'RKL DEJ 9/25/13
    cmboLabelPrinter.Text = lcLablePrinter      'RKL DEJ 9/25/13
    txtCOACnt.Text = CStr(lcNbrOfCOACopies)     'RKL DEJ 9/25/13
    
    chManualScaling.Value = lcMANUALSCALING * -1
    
    If lcAutoRtnOpenLoads = True Then
        chAutoReturn.Value = vbChecked
    Else
        chAutoReturn.Value = vbUnchecked
    End If
    
    cmboSeconds.Text = lcAutoRtrnSecnds
    
    If lcEnforceCOA = True Then
        chEnforceCOA.Value = vbChecked
    Else
        chEnforceCOA.Value = vbUnchecked
    End If
    
    'RKL DEJ 9/25/13 (START)
    
    If lcPrintExtraBOL = True Then
        chExtraBOL.Value = vbChecked
    Else
        chExtraBOL.Value = vbUnchecked
    End If
    
    If lcPrintExtraBOLCOAOnly = True Then
        chExtraBOLCOAOnly.Value = vbChecked
    Else
        chExtraBOLCOAOnly.Value = vbUnchecked
    End If
    
    If lcPrintExtraCOA = True Then
        chExtraCOA.Value = vbChecked
    Else
        chExtraCOA.Value = vbUnchecked
    End If
    
    If lcPrintExtraLblCert = True Then
        chExtraLabelCert.Value = vbChecked
    Else
        chExtraLabelCert.Value = vbUnchecked
    End If
    
    If lcPrintExtraLblRisk = True Then
        chExtraLabelRisk.Value = vbChecked
    Else
        chExtraLabelRisk.Value = vbUnchecked
    End If
    
    If lcPrintExtraLblWY = True Then
        chExtraLabelWY.Value = vbChecked
    Else
        chExtraLabelWY.Value = vbUnchecked
    End If
    
    If lcHideOverShip = True Then
        chHideOverShip.Value = vbChecked
    Else
        chHideOverShip.Value = vbUnchecked
    End If
    
    If lcShowLMEMsg = True Then
        chShowLMEMsg.Value = vbChecked
    Else
        chShowLMEMsg.Value = vbUnchecked
    End If
    
    If lcAllowGetLMEMsg = True Then
        chAllowGetLMEMsg.Value = vbChecked
    Else
        chAllowGetLMEMsg.Value = vbUnchecked
    End If
    
    If lcDriverOn = True Then
        chDriverOn.Value = vbChecked
    Else
        chDriverOn.Value = vbUnchecked
    End If
    
    'RKL DEJ 9/25/13 (STOP)

    'RKL DEJ 2017-12-04 (START)
    If lcCustOwnedProd = True Then
        chCustOwned.Value = vbChecked
    Else
        chCustOwned.Value = vbUnchecked
    End If
    
    If lcAutoPostIMTrans = True Then
        chAutoPostIMTrans.Value = vbChecked
    Else
        chAutoPostIMTrans.Value = vbUnchecked
    End If
    
    If lcNotAllowSplitLoads = True Then
        chNotAllowSplitLoads.Value = vbChecked
    Else
        chNotAllowSplitLoads.Value = vbUnchecked
    End If
    
    If lcForceVarianceCheck = True Then
        chForceVarianceCheck.Value = vbChecked
    Else
        chForceVarianceCheck.Value = vbUnchecked
    End If
    
    txtSrcVariance.Text = lcSrcVariance
    txtIMTranReasonCode.Text = lcIMTranReasonCode
    'RKL DEJ 2017-12-04 (STOP)

    If lcRequireLotNbr = True Then
        chRequireLotNbr.Value = vbChecked
    Else
        chRequireLotNbr.Value = vbUnchecked
    End If
    
    
    If lcMANUALSCALING = True Then
        chManualScaling.Value = vbChecked
    Else
        chManualScaling.Value = vbUnchecked
    End If
    
    If lcPrintBOLCypress = True Then
        chBOLCypress.Value = vbChecked
    Else
        chBOLCypress.Value = vbUnchecked
    End If
    
    If lcPrintCOACypress = True Then
        chPrntCOACypress.Value = vbChecked
    Else
        chPrntCOACypress.Value = vbUnchecked
    End If
    
    'RKL DEJ 9/25/13 (START)
    If lcPrintLabels = True Then
        chPrintLabels.Value = vbChecked
    Else
        chPrintLabels.Value = vbUnchecked
    End If
    
    If lcPrintLabelAfterBOL = True Then
        chPrintLabelAfterBOL.Value = vbChecked
    Else
        chPrintLabelAfterBOL.Value = vbUnchecked
    End If
    'RKL DEJ 9/25/13 (STOP)
    
    'RKL DEJ 4/23/15 (START)
    If lcScaleInWarnTons = True Then
        chScaleInTonsWarn.Value = vbChecked
    Else
        chScaleInTonsWarn.Value = vbUnchecked
    End If
    
    If lcScaleOutWarnVar = True Then
        chScaleOutVarWarning.Value = vbChecked
    Else
        chScaleOutVarWarning.Value = vbUnchecked
    End If
    
    If lcProcessBOLWarnVar = True Then
        chPrcessBOLVarWrn.Value = vbChecked
    Else
        chPrcessBOLVarWrn.Value = vbUnchecked
    End If
    
    txtStandardTons.Text = lcStdTon
    txtTonsVariance.Text = lcMaxTonVar

    'RKL DEJ 4/23/15 (STOP)
    
    'RKL DEJ 2016-05-03 (START)
    If lcPrintPASSPatent = True Then
        chPrintPASSPatent.Value = vbChecked
    Else
        chPrintPASSPatent.Value = vbUnchecked
    End If
    
    txtPASSPatent.Text = lcPASSPatent
    txtBOLAddrPhone.Text = lcBOLAddrPhone
    
    'RKL DEJ 2016-05-03 (STOP)
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub
    




Private Sub SSTab1_GotFocus()
    Select Case SSTab1.Tab
        Case 0
            cmoMode.SetFocus
        Case 1
            cmboBOLPrntr.SetFocus
        Case 2
            chEnforceCOA.SetFocus
        Case 3
            chPrintLabels.SetFocus
        Case 4
            cmboScalePrntr.SetFocus
        Case 5
            cmdScale.SetFocus
        Case 6
            chScaleInTonsWarn.SetFocus
    End Select
End Sub

Private Sub txtBOLAddrPhone_Change()
    lcBOLAddrPhone = txtBOLAddrPhone.Text
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtBOLAddrPhone_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtCOACnt_Change()
    If IsNumeric(txtCOACnt.Text) = True Then
        lcNbrOfCOACopies = CInt(txtCOACnt.Text)
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtCOACnt_Validate(Cancel As Boolean)
    If IsNumeric(txtCOACnt.Text) = False Then
        Cancel = True
        MsgBox "Must be a valid number", vbInformation, "Not a Number"
    End If
    gbLastActivityTime = Date + Time

End Sub

Sub CalculateLabelCnt()
    gbLastActivityTime = Date + Time
    Dim cnt As Integer
    
    If chPrintLabels.Value = vbChecked Then
    
        cnt = 1
        
        If chExtraLabelCert.Value = vbChecked Then
            cnt = cnt + 1
        End If
        
        If chExtraLabelRisk.Value = vbChecked Then
            cnt = cnt + 1
        End If
        
        If chExtraLabelWY.Value = vbChecked Then
            cnt = cnt + 1
        End If
        
    Else
        cnt = 0
    End If
    
    txtLblCnt.Text = cnt
End Sub




'RKL DEJ 2017-12-16 added txtIMTranReasonCode_Change
Private Sub txtIMTranReasonCode_Change()
    lcIMTranReasonCode = txtIMTranReasonCode.Text
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtPASSPatent_Change()
    lcPASSPatent = txtPASSPatent.Text
    gbLastActivityTime = Date + Time
End Sub

Private Sub txtPASSPatent_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub


'RKL DEJ 2017-12-16 added txtSrcVariance_Change
Private Sub txtSrcVariance_Change()
    If IsNumeric(txtSrcVariance.Text) = True Then
        lcSrcVariance = CDbl(txtSrcVariance.Text)
    End If

End Sub

'RKL DEJ 2017-12-16 added txtSrcVariance_Validate
Private Sub txtSrcVariance_Validate(Cancel As Boolean)
    If IsNumeric(txtSrcVariance.Text) = False Then
        Cancel = True
        
        MsgBox "The Standard Tons must be a valid number.", vbInformation, "Not a Valid Number"
    Else
        lcSrcVariance = CDbl(txtSrcVariance.Text)
        Cancel = False
    End If

End Sub

Private Sub txtStandardTons_Change()
    If IsNumeric(txtStandardTons.Text) = True Then
        lcStdTon = txtStandardTons.Text
    End If
    
End Sub

Private Sub txtStandardTons_Validate(Cancel As Boolean)
    If IsNumeric(txtStandardTons.Text) = False Then
        Cancel = True
        
        MsgBox "The Standard Tons must be a valid number.", vbInformation, "Not a Valid Number"
    End If

End Sub

Private Sub txtTonsVariance_Change()
    If IsNumeric(txtTonsVariance.Text) = True Then
        lcMaxTonVar = txtTonsVariance.Text
    End If

End Sub

Private Sub txtTonsVariance_Validate(Cancel As Boolean)
    If IsNumeric(txtTonsVariance.Text) = False Then
        Cancel = True
        MsgBox "The Max Tons Variance must be a valid number.", vbInformation, "Not a Valid Number"
    End If

End Sub


Sub OpenBOLRS()
    On Error GoTo Error
    
    Dim sSQL As String
    
    grdBOLPrint.Enabled = False
    cmdAddBOLPrnt.Enabled = False
    cmdDelBOLPrnt.Enabled = False
    
    'Trim(UCase(lcScaleConnStr))
    
    If BOLRS.State = 1 Then
        BOLRS.Close
        Set grdBOLPrint.DataSource = Nothing
    End If
    
    If lcSettingsConn.State = 1 Then
        lcSettingsConn.Close
    End If
    
    lcSettingsConn.ConnectionString = lcScaleConnStr
    
    lcSettingsConn.CursorLocation = adUseClient
    lcSettingsConn.IsolationLevel = adXactChaos
    lcSettingsConn.Open
    
    sSQL = "Select * From BOLPrintingSetup Order by [CopyNbr] Asc "
    
    BOLRS.CursorLocation = adUseClient
    BOLRS.CursorType = adOpenDynamic
    
    BOLRS.Open sSQL, lcSettingsConn, adOpenDynamic, adLockBatchOptimistic
    
    BOLRS.Sort = "[CopyNbr] Asc"
    
    Set grdBOLPrint.DataSource = BOLRS
    
    grdBOLPrint.Enabled = True
    cmdAddBOLPrnt.Enabled = True
    cmdDelBOLPrnt.Enabled = True
    
    Exit Sub
Error:
    MsgBox Me.Name & ".OpenBOLRS()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
    
End Sub

Sub SaveBOLPrintSettings()
    On Error GoTo Error
    
    If UpdateRS(BOLRS, False) = False Then
        'There was an error saving the BOL Print Settings
        MsgBox "There was an error saving the BOL Print Settings.  The BOL Print Settings were not saved.", vbExclamation, "Scale Pass Save Error"
    End If
    
    Exit Sub
Error:
    MsgBox Me.Name & ".SaveBOLPrintSettings()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub
