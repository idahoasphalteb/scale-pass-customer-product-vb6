VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form1 
   Caption         =   "Test Data Export"
   ClientHeight    =   6645
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10290
   LinkTopic       =   "Form1"
   ScaleHeight     =   6645
   ScaleWidth      =   10290
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdExportTimer 
      Caption         =   "Export - KnowledgeSync"
      Height          =   315
      Left            =   2880
      TabIndex        =   8
      Top             =   2280
      Width           =   3015
   End
   Begin VB.CommandButton cmdExport 
      Caption         =   "Export Data - MAS 500"
      Height          =   375
      Left            =   360
      TabIndex        =   7
      Top             =   2280
      Width           =   2295
   End
   Begin VB.TextBox txtSavePath 
      Height          =   285
      Left            =   240
      TabIndex        =   1
      Top             =   1560
      Width           =   5535
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      Height          =   255
      Left            =   5880
      TabIndex        =   0
      ToolTipText     =   "Browse to the Access Database"
      Top             =   1560
      Width           =   495
   End
   Begin MSComCtl2.DTPicker dtMinDate 
      Height          =   375
      Left            =   480
      TabIndex        =   2
      Top             =   720
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   51707905
      CurrentDate     =   43102
   End
   Begin MSComCtl2.DTPicker dtMaxDate 
      Height          =   375
      Left            =   2280
      TabIndex        =   3
      Top             =   720
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   51707905
      CurrentDate     =   43102
   End
   Begin MSComDlg.CommonDialog cdExportPath 
      Left            =   5520
      Top             =   840
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DefaultExt      =   """*.mdb | *.*"""
      Filter          =   "MS Access (*.mdb)|*.mdb;All (*.*):*.*"
   End
   Begin VB.Label lblMinDate 
      Caption         =   "Min Date"
      Height          =   255
      Left            =   480
      TabIndex        =   6
      Top             =   480
      Width           =   1575
   End
   Begin VB.Label lblMaxDate 
      Caption         =   "Max Date"
      Height          =   255
      Left            =   2280
      TabIndex        =   5
      Top             =   480
      Width           =   1575
   End
   Begin VB.Label lblSavePath 
      Caption         =   "Export To This Path:"
      Height          =   255
      Left            =   240
      TabIndex        =   4
      Top             =   1320
      Width           =   2055
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private FSO As New Scripting.FileSystemObject


Private Sub cmdBrowse_Click()
    On Error GoTo Error
    Dim FilePath As String
    
    cdExportPath.Filter = "Comma-Separated (*.csv)|*.csv|All (*.*)|*.*"

    cdExportPath.FileName = txtSavePath.Text
    cdExportPath.DialogTitle = "Save Exort As"
'    cdExportPath.DefaultExt = "*.csv"
    cdExportPath.ShowOpen
    
    If cdExportPath.FileName = Empty Then Exit Sub
    
    FilePath = cdExportPath.FileName


    txtSavePath.Text = FilePath
    
    Exit Sub
Error:
    MsgBox Me.Name & ".cmdBrowse_Click()" & vbCrLf & "Browsing Error: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    Err.Clear
End Sub


Function IsValid() As Boolean
    On Error GoTo Error
    
    Dim FilePath As String
    Dim FolderPath As String
    
    '*******************************************************
    'Validate the Date Range
    '*******************************************************
    If dtMinDate.Value > dtMaxDate.Value Then
        MsgBox "The Min Date cannot be after the Max Date.", vbInformation, "Date Range Not Valid"
        IsValid = False
        Exit Function
    End If
    
    '*******************************************************
    'validate the File path
    '*******************************************************
    FilePath = Trim(txtSavePath.Text)
    
    If Len(FilePath) <= 0 Then
        MsgBox "You need to specify a export file path first.", vbInformation, "Missing Export Path"
        IsValid = False
        Exit Function
    Else
        If FSO.FileExists(FilePath) = True Then
            If MsgBox("The file '" & vbCrLf & FilePath & "'" & vbCrLf & "already exists.  Do you want to overwrite the existing file?", vbYesNo) <> vbYes Then
                IsValid = False
                Exit Function
            End If
        Else
            'Validate the file path
            FolderPath = FSO.FolderExists(FilePath)
            FolderPath = FSO.GetParentFolderName(FilePath)
            If FSO.FolderExists(FolderPath) = False Then
                MsgBox "The path to that file either does not exist or is invalid.  Fix the file path before proceding.", vbInformation, "Not a Valid Path"
                IsValid = False
                Exit Function
            End If
        End If
    End If
    
    'If we get to this point then the data is valid
    IsValid = True
    
    Exit Function
Error:
    MsgBox Me.Name & ".IsValid()" & vbCrLf & "Browsing Error: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    Err.Clear
    
End Function


Function GetConnStrDB() As String
    Dim ConnStr As String
    
    'ConnStr = ADOConnectionString(moClass.moSysSession.SysADOConnect, oClass, "MAS 500 Valero Export", "Valero Export")
    GetConnStrDB = "Provider=SQLOLEDB;User ID=admin;Data Source=testdb2012;Initial Catalog=mas500_app_IAS02;Trusted_Connection=No;OLE DB Services= -2;Application Name=Sage 500 ERP/Valero Export/1010014013/WEC/RES-DJONES/2;"

End Function


Private Sub cmdExport_Click()
    Dim ErrMsg As String
    
    'Validate the data
    If IsValid() = False Then
        'Did not pass validation so stop here
        Exit Sub
    End If
    
    'Create the export ActiveX Object
    Dim exp As New ValeroExport.clsValeroExport
    
    'Set Export Parameters
    exp.ConnStr = GetConnStrDB()
    exp.ExportFilePath = txtSavePath.Text
    exp.MinDate = dtMinDate.Value
    exp.MaxDate = dtMaxDate.Value
    exp.UseMinMaxDate = True
    
    'Call the export
    If Not exp.ExportData(ErrMsg) = True Then
        MsgBox "The Export failed with this error message:" & ErrMsg, vbInformation, "Failure"
    Else
        MsgBox "The export was successfull.", vbInformation, "Success"
    End If
    
    Set exp = Nothing
End Sub

Private Sub cmdExportTimer_Click()
    Dim ErrMsg
    Dim FileName
    Dim FilePath
    Dim ConnStr
    Dim MinDate
    Dim MaxDate
    
    ErrMsg = Empty
    FileName = "ValeroExport-" & CStr(Format(Now, "yyyymmdd")) & ".csv"
    
    FilePath = "C:\Scale Pass 2009\Export\" & FileName
    
    'IAS Server
    ConnStr = "Provider=SQLOLEDB;User ID=admin;Password=eyeball;Data Source=IASSQL01;Initial Catalog=mas500_app_Varlero;Trusted_Connection=No;OLE DB Services= -2;Application Name=KnowledgeSync/Valero Export;"
    
    'RKL test
    ConnStr = "Provider=SQLOLEDB;User ID=admin;Data Source=testdb2012;Initial Catalog=mas500_app_IAS02;Trusted_Connection=No;OLE DB Services= -2;Application Name=Sage 500 ERP/Valero Export/1010014013/WEC/RES-DJONES/2;"
    
    'Create the export ActiveX Object
    Dim exp
    Set exp = CreateObject("ValeroExport.clsValeroExport")
    
    'Set Export Parameters
    exp.ConnStr = ConnStr
    exp.ExportFilePath = FilePath
'    exp.MinDate = dtMinDate.Value
'    exp.MaxDate = dtMaxDate.Value
    exp.UseMinMaxDate = False
    
    'Call the export
    If Not exp.ExportData(CStr(ErrMsg)) = True Then
        MsgBox "The Export failed with this error message:" & ErrMsg, vbInformation, "Failure"
    Else
        MsgBox "The export was successfull.", vbInformation, "Success"
    End If
    
    Set exp = Nothing

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Set FSO = Nothing
End Sub
