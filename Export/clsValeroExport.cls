VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsValeroExport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Private FSO As New Scripting.FileSystemObject
Private Conn As New ADODB.Connection

Private lcConnStr As String
Private lcMinDate As Date
Private lcMaxDate As Date
Private lcExportFilePath As String
Private lcbUseMinMaxDate As Boolean

Public Property Let ExportFilePath(FilePath As String)
    lcExportFilePath = FilePath

End Property

Public Property Get ExportFilePath() As String
    ExportFilePath = lcExportFilePath

End Property

Public Property Let ConnStr(ConnectionStr As String)
    lcConnStr = ConnectionStr

End Property

Public Property Get ConnStr() As String
    ConnStr = lcConnStr

End Property

Public Property Let MinDate(StartDate As Date)
    lcMinDate = StartDate

End Property

Public Property Get MinDate() As Date
    MinDate = lcMinDate

End Property

Public Property Let MaxDate(EndDate As Date)
    lcMaxDate = EndDate

End Property

Public Property Get MaxDate() As Date
    MaxDate = lcMaxDate

End Property

Public Property Let UseMinMaxDate(bVal As Boolean)
    lcbUseMinMaxDate = bVal

End Property

Public Property Get UseMinMaxDate() As Boolean
    UseMinMaxDate = lcbUseMinMaxDate

End Property

Public Function ExportData(Optional ByRef ErrMsg As String = "") As Boolean
    On Error GoTo Error
    
    Dim RS As New ADODB.Recordset
    Dim TS As Scripting.TextStream
    Dim fld As ADODB.Field
    Dim LineToWrite As String
    Dim bFldOne As Boolean
    
    ErrMsg = Empty
    
    'Validate Data First:
    If IsValid(ErrMsg) = False Then
        ExportData = False
        Exit Function
    End If
    
    ErrMsg = Empty
    
    If OpenDatabaseConn(ErrMsg) = False Then
        ExportData = False
        Exit Function
    End If
    
    ErrMsg = Empty
    
    Set RS = GetValeroData(ErrMsg)
    
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            'The recordset was returned successfully
            
            'Open/create the export file
            Set TS = FSO.OpenTextFile(lcExportFilePath, ForWriting, True)
            
            LineToWrite = Empty
            bFldOne = True
            For Each fld In RS.Fields
                If bFldOne = True Then
                    LineToWrite = fld.Name
                    bFldOne = False
                Else
                    LineToWrite = LineToWrite & "," & fld.Name
                End If
            Next
            
            TS.WriteLine LineToWrite
            
            LineToWrite = Empty
            
            If Not RS.EOF And Not RS.BOF Then
                RS.MoveFirst
            End If
            
            While Not RS.EOF And Not RS.BOF
                LineToWrite = Empty
                bFldOne = True
                                
                For Each fld In RS.Fields
                    If bFldOne = True Then
                        LineToWrite = ConvertToString(fld.Value)
                        bFldOne = False
                    Else
                        LineToWrite = LineToWrite & "," & ConvertToString(fld.Value)
                    End If
                Next
                
                TS.WriteLine LineToWrite
                RS.MoveNext
            Wend
            
            'Close the file
            TS.Close
            
            'Close the Recordset
            RS.Close
            Set RS = Nothing
        Else
            'The records set returned closed
            If Trim(ErrMsg) = Empty Then
                ErrMsg = "An Unknown Error occurred in the GetValeroData method (the call to the stored procedure 'spIMValeroExport_SGS')"
                ExportData = False
                Exit Function
            End If
        End If
    Else
        'A Recordset was not returned... there may have been an error.
        If Trim(ErrMsg) <> Empty Then
            ExportData = False
            Exit Function
        End If
    End If
    
    'If we get to this point then return success:
    ExportData = True
    
CleanUp:
    Call CloseDB
    
    Exit Function
Error:
    ErrMsg = "The following error occurred durring the Data Export:" & vbCrLf
    ErrMsg = ErrMsg & "Error Occurred in the ExportData method." & vbCrLf
    ErrMsg = ErrMsg & "Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    ExportData = False
    
    GoTo CleanUp
End Function

Public Function GetValeroData(Optional ByRef ErrMsg As String = "") As ADODB.Recordset
    On Error GoTo Error

    Dim CMD As New ADODB.Command
    Dim RS As New ADODB.Recordset
    Dim RtnVal As Long
    
    CMD.ActiveConnection = Conn
    CMD.CommandType = adCmdStoredProc
    CMD.CommandText = "spIMValeroExport_SGS"
    CMD.Parameters.Refresh
    CMD.CommandTimeout = 0      'Added to prevent time outs

    'Assign input parameter Values
    If lcbUseMinMaxDate = True Then
        CMD.Parameters("@MinDate").Value = lcMinDate
        CMD.Parameters("@MaxDate").Value = lcMaxDate
    Else
        'The default min and max are null... the proc will handle this logic
    End If
    
    CMD.Parameters("@oRtnVal").Value = RtnVal
    CMD.Parameters("@oErrMsg").Value = ErrMsg
    
    'Execute the stored procedure
    Set RS = CMD.Execute()
    
    'Get Return Values
    RtnVal = CMD.Parameters("@oRtnVal").Value
    ErrMsg = CMD.Parameters("@oErrMsg").Value
    
    Set GetValeroData = RS
    
    Exit Function
Error:
    ErrMsg = "The following error occurred when calling the stored procedure 'spIMValeroExport_SGS':" & vbCrLf
    ErrMsg = ErrMsg & "Error Occurred in the GetValeroData method." & vbCrLf
    ErrMsg = ErrMsg & "Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    Set GetValeroData = Nothing
End Function


Public Function IsValid(Optional ByRef WarningMsg As String) As Boolean
    On Error GoTo Error
    Dim FilePath As String
    Dim FolderPath As String
    
    '*******************************************************
    'Validate the Date Range
    '*******************************************************
    If lcbUseMinMaxDate = True Then
        'User has specifed to use the dates passed in
        If lcMinDate > lcMaxDate Then
            WarningMsg = "The Min Date cannot be after the Max Date."
            IsValid = False
            Exit Function
        End If
    End If
    
    '*******************************************************
    'validate the File path
    '*******************************************************
    FilePath = Trim(lcExportFilePath)
    
    If Len(FilePath) <= 0 Then
        WarningMsg = "The export file path was not specified"
        IsValid = False
        Exit Function
    Else
        If FSO.FileExists(FilePath) <> True Then
            'Validate the file path
            FolderPath = FSO.GetParentFolderName(FilePath)
            If FSO.FolderExists(FolderPath) = False Then
                WarningMsg = "The export file path does not exist or is invalid."
                IsValid = False
                Exit Function
            End If
        End If
    End If
    
    '*******************************************************
    'validate the Database Connection String
    '*******************************************************
    If IsValidConnStr(WarningMsg) = False Then
        IsValid = False
        Exit Function
    End If
    
    'If we get to this point then the data is valid
    IsValid = True
    
    Exit Function
Error:
    WarningMsg = "The following error occurred durring the validation" & vbCrLf
    WarningMsg = WarningMsg & "Error Occurred in the IsValid method." & vbCrLf
    WarningMsg = WarningMsg & "Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    IsValid = False
End Function

Public Function IsValidConnStr(Optional ByRef ErrMsg As String = "") As Boolean
    On Error GoTo Error
    
    Dim lcConn As New ADODB.Connection
    
    lcConn.ConnectionString = lcConnStr
    
    lcConn.Open
    
    IsValidConnStr = True
    
CleanUp:
    If lcConn.State = 1 Then
        lcConn.Close
    End If
    
    Set lcConn = Nothing

    Exit Function
Error:
    ErrMsg = "The following error occurred durring the connection string validation:" & vbCrLf
    ErrMsg = ErrMsg & "Error Occurred in the IsValidConnStr method." & vbCrLf
    ErrMsg = ErrMsg & "Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    IsValidConnStr = False
    
    GoTo CleanUp
End Function


Public Function HowToUse() As String
    On Error GoTo Error
    Dim Msg As String
    
    Msg = "1) Populate the properties: " & vbCrLf
    Msg = Msg & vbTab & "- ConnStr - The connection string to the MAS 500 Data.  This is for a ADODB.Connection." & vbCrLf
    Msg = Msg & vbTab & "- MinDate - The min or begin date to include in the data filter." & vbCrLf
    Msg = Msg & vbTab & "- MaxDate - The max or end date to include in the data filter." & vbCrLf
    Msg = Msg & vbTab & "- ExportFilePath - The full path encluding the file name of the export file.  This context this app runs needs to have read/write permissions to this file and location.  If the file does not exist it will be created." & vbCrLf
    Msg = Msg & vbTab & "- UseMinMaxDate - When true the MinDate and MaxDate parperties will be used to filter the data.  When false yesterdays date will be used for both the min and max date for the filter." & vbCrLf & vbCrLf
    
    Msg = Msg & "2) Call the method ExportData - This method validates the properties in step one then call the stored procedure 'spIMValeroExport_SGS' and the result query is exported to the export file specified in the ExportFilePath property." & vbCrLf
    Msg = Msg & vbTab & "- To change the export fields and the order of the fields, modify the procedure 'spIMValeroExport_SGS'." & vbCrLf
    Msg = Msg & vbTab & "- The export, exports the result set from the stored procedures in the order the procedure send it.  All fields in the result set will be exported."
    
    HowToUse = Msg
    
    Exit Function
Error:
    MsgBox "ValeroExport.clsValeroExport.HowToUse()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical + vbExclamation, "Error on How To Use Method"
    
    Err.Clear
End Function


Public Function OpenDatabaseConn(Optional ByRef ErrMsg As String = "") As Boolean
    On Error GoTo Error
    
    'First make sure the connection is closed
    
    Call CloseDB
    
    Conn.CursorLocation = adUseClient
    Conn.IsolationLevel = adXactChaos
    
    Conn.ConnectionString = lcConnStr
    
    Conn.Open
    
    If Conn.State = 1 Then
        OpenDatabaseConn = True
    Else
        OpenDatabaseConn = False
    End If
    
    Exit Function
Error:
    ErrMsg = "The following error occurred when opening the database connection:" & vbCrLf
    ErrMsg = ErrMsg & "Error Occurred in the OpenDatabaseConn method." & vbCrLf
    ErrMsg = ErrMsg & "Error: " & Err.Number & " " & Err.Description
    
    Err.Clear
    
    OpenDatabaseConn = False
End Function

Private Sub CloseDB()
    On Error Resume Next
    
    If Not Conn Is Nothing Then
        If Conn.State = 1 Then
            Conn.Close
        End If
    End If
    
    Set Conn = Nothing
End Sub


Private Sub Class_Terminate()
    On Error Resume Next
    Call CloseDB
End Sub

Public Function ConvertToString(Val As Variant) As String
    On Error GoTo Error

'    Debug.Print TypeName(Val)
    
    Select Case VarType(Val)
        Case VbVarType.vbArray
            ConvertToString = ConvertToString(Val(0))
        
        Case VbVarType.vbBoolean
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbByte
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbCurrency
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDataObject
            ConvertToString = Empty
        
        Case VbVarType.vbDate
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDecimal
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbDouble
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbEmpty
            ConvertToString = Empty
        
        Case VbVarType.vbError
            ConvertToString = Val.Number & " " & Val.Description
        
        Case VbVarType.vbInteger
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbLong
            ConvertToString = CStr(Val)
        
        Case VbVarType.vbNull
            ConvertToString = Empty
        
        Case VbVarType.vbObject
            ConvertToString = Empty
            
        Case VbVarType.vbSingle
            ConvertToString = Empty
        
        Case VbVarType.vbString
            ConvertToString = Val
        
        Case VbVarType.vbUserDefinedType
            ConvertToString = Empty
        
        Case VbVarType.vbVariant
            ConvertToString = CStr(Val)
        
        Case Else
            If IsArray(Val) = True Then
                ConvertToString = ConvertToString(Val(0))
            Else
                ConvertToString = Empty
            End If
    
    End Select
        
    
    Exit Function
Error:
    MsgBox "ValeroExport.clsValeroExport..ConvertToString(Val As Variant) As String" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    Err.Clear
    
End Function



