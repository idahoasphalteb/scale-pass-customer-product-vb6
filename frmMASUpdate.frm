VERSION 5.00
Begin VB.Form frmMASUpdate 
   BackColor       =   &H00FF00FF&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Update MAS 500 - Valero"
   ClientHeight    =   780
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9150
   ControlBox      =   0   'False
   Icon            =   "frmMASUpdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   780
   ScaleWidth      =   9150
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Interval        =   3000
      Left            =   6600
      Top             =   0
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H0000FFFF&
      Caption         =   "Checking For Updates To MAS 500"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8895
   End
End
Attribute VB_Name = "frmMASUpdate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Label1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Timer1_Timer()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    Timer1.Enabled = False
    
    frmBOLPreview.UpdateMASOnly = True
    
    Load frmBOLPreview
    
    If frmBOLPreview.UpdateMAS500 = False Then
        MsgBox "There were errors updating MAS 500", vbExclamation, "MAS 500 Update Errors"
    End If
    
    frmBOLPreview.UpdateMASOnly = False
    
    Unload frmBOLPreview
    
    gbLastActivityTime = Date + Time
    Unload Me

    gbLastActivityTime = Date + Time
End Sub


