﻿
Create Table BOLPrintingSetup (CopyNbr Int Not Null, PrintCaption Varchar(100) Null, Primary Key (CopyNbr) )

Alter Table BOLPrinting Add BOLAddressAndPhone Varchar(255) Null
Alter Table BOLPrinting Add PASSPatent Varchar(200) Null
Alter Table BOLPrinting Add PrintPASSPatent Numeric Null

Alter Table BOLPrinting Add LotNbrCaption Varchar(30) Null
Alter Table BOLPrinting Add LabNbrCaption Varchar(30) Null
Alter Table BOLPrinting Add SealNbrCaption Varchar(30) Null

Alter Table BOLPrinting Add LabNbr Varchar(12) Null
Alter Table BOLPrinting Add SealNbr Varchar(12) Null


Update BOLPrinting Set BOLAddressAndPhone = 'P.O. Box 50538 * Idaho Falls, ID 83405 * (208) 524-5871'
Update BOLPrinting Set LotNbrCaption = 'Lot No.'
Update BOLPrinting Set PrintPassPatent = 0

Alter Table BOLPrinting Drop LabNbrCaption
Alter Table BOLPrinting Drop SealNbrCaption
Alter Table BOLPrinting Drop LabNbr
Alter Table BOLPrinting Drop SealNbr

Alter Table BOLPrinting Add ShipToState Varchar(3) Null
