VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmODBCLogon 
   BackColor       =   &H00FF00FF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Setup Connection - Valero"
   ClientHeight    =   3630
   ClientLeft      =   2850
   ClientTop       =   1755
   ClientWidth     =   7980
   ControlBox      =   0   'False
   Icon            =   "frmODBCLogon.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3630
   ScaleWidth      =   7980
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton txtView 
      Cancel          =   -1  'True
      Caption         =   "View Conn"
      Height          =   450
      Left            =   5160
      TabIndex        =   19
      ToolTipText     =   "View the Connection String"
      Top             =   3000
      Width           =   1200
   End
   Begin MSComDlg.CommonDialog cdDatabase 
      Left            =   4320
      Top             =   2040
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DefaultExt      =   """*.mdb | *.*"""
      Filter          =   "MS Access (*.mdb)|*.mdb;All (*.*):*.*"
   End
   Begin VB.TextBox txtProperties 
      Height          =   2655
      Left            =   5160
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   15
      Top             =   240
      Width           =   2655
   End
   Begin VB.CommandButton cmdTest 
      Caption         =   "Test"
      Height          =   450
      Left            =   1200
      TabIndex        =   16
      ToolTipText     =   "Test Connection"
      Top             =   3000
      Width           =   1200
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   450
      Left            =   3840
      TabIndex        =   18
      Top             =   3000
      Width           =   1200
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      Height          =   450
      Left            =   2520
      TabIndex        =   17
      Top             =   3000
      Width           =   1200
   End
   Begin VB.Frame fraStep3 
      Caption         =   "Connection Values"
      Height          =   2775
      Index           =   0
      Left            =   120
      TabIndex        =   20
      Top             =   120
      Width           =   4935
      Begin VB.CheckBox chWinAuth 
         Alignment       =   1  'Right Justify
         Caption         =   "Windows Authentication"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1320
         Width           =   2655
      End
      Begin VB.TextBox txtServer 
         Enabled         =   0   'False
         Height          =   330
         Left            =   1110
         TabIndex        =   5
         Top             =   945
         Width           =   3015
      End
      Begin VB.TextBox txtUID 
         Height          =   300
         Left            =   1110
         TabIndex        =   8
         Top             =   1680
         Width           =   3015
      End
      Begin VB.ComboBox cboDrivers 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1110
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   600
         Width           =   3015
      End
      Begin VB.ComboBox txtDatabase 
         Height          =   315
         Left            =   1110
         TabIndex        =   12
         Top             =   2340
         Width           =   3015
      End
      Begin VB.CommandButton cmdBrowse 
         Caption         =   "..."
         Height          =   255
         Left            =   4200
         TabIndex        =   13
         ToolTipText     =   "Browse to the Access Database"
         Top             =   2400
         Width           =   495
      End
      Begin VB.TextBox txtPWD 
         Height          =   300
         IMEMode         =   3  'DISABLE
         Left            =   1110
         PasswordChar    =   "*"
         TabIndex        =   10
         Top             =   2010
         Width           =   3015
      End
      Begin VB.ComboBox cboDSNList 
         Height          =   315
         ItemData        =   "frmODBCLogon.frx":000C
         Left            =   1125
         List            =   "frmODBCLogon.frx":000E
         Sorted          =   -1  'True
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   3000
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "&Server:"
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   4
         Top             =   1020
         Width           =   510
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "Dri&ver:"
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   2
         Top             =   675
         Width           =   465
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "&DSN:"
         Height          =   195
         Index           =   1
         Left            =   135
         TabIndex        =   0
         Top             =   285
         Width           =   390
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "&UID:"
         Height          =   195
         Index           =   2
         Left            =   135
         TabIndex        =   7
         Top             =   1710
         Width           =   330
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "&Password:"
         Height          =   195
         Index           =   3
         Left            =   135
         TabIndex        =   9
         Top             =   2055
         Width           =   735
      End
      Begin VB.Label lblStep3 
         AutoSize        =   -1  'True
         Caption         =   "Data&base:"
         Height          =   195
         Index           =   4
         Left            =   135
         TabIndex        =   11
         Top             =   2400
         Width           =   735
      End
   End
   Begin VB.Label lblProperties 
      Caption         =   "Additional Properties"
      Height          =   255
      Left            =   5160
      TabIndex        =   14
      Top             =   0
      Width           =   2415
   End
End
Attribute VB_Name = "frmODBCLogon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function SQLDataSources Lib "ODBC32.DLL" (ByVal henv&, ByVal fDirection%, ByVal szDSN$, ByVal cbDSNMax%, pcbDSN%, ByVal szDescription$, ByVal cbDescriptionMax%, pcbDescription%) As Integer
Private Declare Function SQLAllocEnv% Lib "ODBC32.DLL" (env&)
Const SQL_SUCCESS As Long = 0
Const SQL_FETCH_NEXT As Long = 1


Private lpDSN As String
Private lpUID As String
Private lpPassword As String
Private lpDatabase As String
Private lpDriver As String
Private lpServer As String
Private lpAddlProperties As String
Private lpWinAuth As Boolean
Private lpConnString As String

Public Sub ClearProperties()
    DSN = Empty
    UID = Empty
    Password = Empty
    Database = Empty
    Driver = Empty
    Server = Empty
    AddlProperties = Empty
    WinAuth = False

    gbLastActivityTime = Date + Time
End Sub

Public Sub UpdateProperties()
    lpConnString = GetConnStr(True)
    lpDSN = cboDSNList.Text
    lpUID = txtUID.Text
    lpPassword = txtPWD.Text
    lpDatabase = txtDatabase.Text
    lpDriver = cboDrivers.Text
    lpServer = txtServer.Text
    lpAddlProperties = txtProperties.Text
    
    If chWinAuth.Value = vbChecked Then
        lpWinAuth = True
    Else
        lpWinAuth = False
    End If
    
    gbLastActivityTime = Date + Time
End Sub

Public Property Let ConnString(ConnStr As String)
    lpConnString = ConnStr

    gbLastActivityTime = Date + Time
End Property

Public Property Get ConnString() As String
    ConnString = lpConnString

    gbLastActivityTime = Date + Time
End Property

Public Property Let Database(DB As String)
    lpDatabase = DB
    txtDatabase.Text = lpDatabase

    gbLastActivityTime = Date + Time
End Property

Public Property Get Database() As String
    Database = lpDatabase

    gbLastActivityTime = Date + Time
End Property

Public Property Let DSN(Value As String)
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    lpDSN = Value
    
    If lpDSN = Empty Then
        If cboDSNList.ListCount > 0 Then
            cboDSNList.Text = "(None)"
            lpDSN = "(None)"
        End If
    Else
        cboDSNList.Text = lpDSN
    End If
    
    gbLastActivityTime = Date + Time
    Exit Property
Error:
    MsgBox "Public Property Let DSN(Value As String)" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Property

Public Property Get DSN() As String
    DSN = lpDSN

    gbLastActivityTime = Date + Time
End Property

Function GetConnStr(Optional IsADO As Boolean = True) As String
    On Error GoTo Error
    
    Dim sConnect    As String
    Dim sADOConnect As String
    Dim sDAOConnect As String
    Dim sDSN        As String
    
    gbLastActivityTime = Date + Time
    
    If cboDSNList.ListIndex > 0 Then
        sDSN = "DSN=" & cboDSNList.Text & ";"
    Else
        If Trim(cboDrivers.Text) <> Empty Then
            sConnect = sConnect & "Driver=" & cboDrivers.Text & ";"
        End If
        
        sConnect = sConnect & "Server=" & txtServer.Text & ";"
    End If
    
    sConnect = sConnect & "UID=" & txtUID.Text & ";"
    sConnect = sConnect & "PWD=" & txtPWD.Text & ";"
    
    If Len(txtDatabase.Text) > 0 Then
        If InStr(1, UCase(cboDrivers.Text), "ACCESS") > 0 Then
            sConnect = sConnect & "User Id=" & txtUID.Text & ";"
            sConnect = sConnect & "Jet OLEDB:Database Password=" & txtPWD.Text & ";"
            sConnect = sConnect & "Dbq=" & txtDatabase.Text & ";"
'            sConnect = sConnect & "ExtendedAnsiSQL=1;"

        Else
            sConnect = sConnect & "Database=" & txtDatabase.Text & ";"
        End If
    End If
    
    If chWinAuth.Value = vbChecked Then
        sConnect = sConnect & "Persist Security Info=True;"
    Else
        sConnect = sConnect & "Persist Security Info=False;"
    End If
    
    sConnect = sConnect & txtProperties.Text
    
    sADOConnect = "PROVIDER=MSDASQL;" & sDSN & sConnect
    sDAOConnect = "ODBC;" & sDSN & sConnect
    
    If IsADO = True Then
        GetConnStr = sADOConnect
    Else
        GetConnStr = sDAOConnect
    End If
        
CleanUP:
    Err.Clear
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "Connection Creation Failed." & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function

Sub GetDBList()
    On Error GoTo Error
    
    Dim conn As New ADODB.Connection
    Dim RS As New ADODB.Recordset
    Dim dbname As String
    
    gbLastActivityTime = Date + Time
    
    dbname = txtDatabase.Text
    
    'Clear the list of databases
    txtDatabase.Clear
    txtDatabase.Text = dbname
    
    If cboDSNList.ListIndex > 0 Then
        Exit Sub
    Else
        If InStr(1, UCase(cboDrivers.Text), "SQL SERVER") > 0 Then
            If Trim(txtServer.Text) = Empty Then
                Exit Sub
            End If
        Else
            Exit Sub
        End If
    End If
    
    If chWinAuth.Value <> vbChecked Then
        If Trim(txtUID.Text) = Empty Then
            Exit Sub
        End If
    End If
    
    conn.ConnectionString = GetConnStr(True)
    
    conn.CursorLocation = adUseClient
    conn.IsolationLevel = adXactChaos

    gbLastActivityTime = Date + Time
    conn.Open
    
    gbLastActivityTime = Date + Time
    If conn.State = 1 Then
        Set RS = conn.Execute("sp_databases")
        
        While Not RS.EOF And Not RS.BOF
            txtDatabase.AddItem RS("Database_Name").Value
            RS.MoveNext
        Wend
    End If
    
CleanUP:
    Err.Clear
    On Error Resume Next
    
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    If conn.State = 1 Then conn.Close
    Set conn = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
'    MsgBox "Error obtaining a list of databases for SQL Server." & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbInformation, "Connection Failed"
    
    gbLastActivityTime = Date + Time
    GoTo CleanUP

End Sub

Public Property Let UID(UserID As String)
    lpUID = UserID
    txtUID.Text = lpUID

    gbLastActivityTime = Date + Time
End Property

Public Property Get UID() As String
    UID = lpUID

    gbLastActivityTime = Date + Time
End Property

Public Property Let Password(PWD As String)
    lpPassword = PWD
    txtPWD.Text = lpPassword

    gbLastActivityTime = Date + Time
End Property

Public Property Get Password() As String
    Password = lpPassword

    gbLastActivityTime = Date + Time
End Property

Public Property Let Driver(DRVR As String)
    On Error GoTo Error
    lpDriver = DRVR
    
    gbLastActivityTime = Date + Time
    
    If lpDriver = Empty Then
'        cboDrivers.Text = Empty
    Else
        cboDrivers.Text = lpDriver
    End If
    
    gbLastActivityTime = Date + Time
    Exit Property
Error:
    MsgBox "Public Property Let Driver(DRVR As String)" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Property

Public Property Get Driver() As String
    Driver = lpDriver

    gbLastActivityTime = Date + Time
End Property

Public Property Let Server(Svr As String)
    lpServer = Svr
    txtServer.Text = lpServer

    gbLastActivityTime = Date + Time
End Property

Public Property Get Server() As String
    Server = lpServer

    gbLastActivityTime = Date + Time
End Property

Public Property Let AddlProperties(Prtys As String)
    lpAddlProperties = Prtys
    txtProperties.Text = lpAddlProperties

    gbLastActivityTime = Date + Time
End Property

Public Property Get AddlProperties() As String
    AddlProperties = lpAddlProperties

    gbLastActivityTime = Date + Time
End Property

Public Property Let WinAuth(bWinAuth As Boolean)
    gbLastActivityTime = Date + Time
    
    lpWinAuth = bWinAuth
    If lpWinAuth = True Then
        chWinAuth.Value = vbChecked
    Else
        chWinAuth.Value = vbUnchecked
    End If

    gbLastActivityTime = Date + Time
End Property

Public Property Get WinAuth() As Boolean
    WinAuth = lpWinAuth

    gbLastActivityTime = Date + Time
End Property


Private Sub chWinAuth_Click()
    If chWinAuth.Value = vbChecked Then
        txtUID.Text = Empty
        txtUID.Enabled = False
        txtPWD.Text = Empty
        txtPWD.Enabled = False
    Else
        txtUID.Enabled = True
        txtPWD.Enabled = True
    End If

    gbLastActivityTime = Date + Time
End Sub

Private Sub chWinAuth_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub chWinAuth_Validate(Cancel As Boolean)
    gbLastActivityTime = Date + Time
    
    Call GetDBList

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdBrowse_Click()
    On Error GoTo Error
    Dim fltr As String
    
    gbLastActivityTime = Date + Time

'    cdDatabase.Filter = fltr = "MS Access (*.mdb;*.accdb)|*.mdb;*.accdb|All (*.*)|*.*"
    cdDatabase.Filter = "MS Access (*.mdb;*.accdb)|*.mdb;*.accdb|All (*.*)|*.*"

    cdDatabase.Filename = txtDatabase.Text
    cdDatabase.DialogTitle = "Browse to the database"
'    cdDatabase.DefaultExt = "*.mdb"
    cdDatabase.ShowOpen
    
    If cdDatabase.Filename = Empty Then Exit Sub
    
    txtDatabase.Text = cdDatabase.Filename
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox "Bowsing Error: " & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbCritical, "Error"

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdBrowse_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdCancel_Click()
    
    gbLastActivityTime = Date + Time
    Unload Me

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdCancel_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdOK_Click()
    
    gbLastActivityTime = Date + Time
    
    Call UpdateProperties
    
    Unload Me

    gbLastActivityTime = Date + Time
End Sub

Private Sub cmdOK_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub cmdTest_Click()
    On Error GoTo Error
    Dim conn As New ADODB.Connection

    gbLastActivityTime = Date + Time
    
    conn.ConnectionString = GetConnStr(True)
    
    conn.CursorLocation = adUseClient
    conn.IsolationLevel = adXactChaos
    
    conn.Open
    
    If conn.State = 1 Then
        MsgBox "The connection was successful.", vbInformation, "Success"
    Else
        MsgBox "The connection failed.", vbInformation, "Failure"
    End If
    
CleanUP:
    Err.Clear
    On Error Resume Next
    If conn.State = 1 Then conn.Close
    Set conn = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
    MsgBox "The connection failed." & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbInformation, "Connection Failed"
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Sub


Private Sub cmdTest_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub Form_Load()
    gbLastActivityTime = Date + Time

'    GetDSNsAndDrivers
End Sub

Private Sub cboDSNList_Click()
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    If cboDSNList.Text = "(None)" Then
        txtServer.Enabled = True
        cboDrivers.Enabled = True
    Else
        txtServer.Enabled = False
        cboDrivers.Enabled = False
    End If

    gbLastActivityTime = Date + Time
End Sub

Public Sub GetDSNsAndDrivers()
    Dim i As Integer
    Dim sDSNItem As String * 1024
    Dim sDRVItem As String * 1024
    Dim sDSN As String
    Dim sDRV As String
    Dim iDSNLen As Integer
    Dim iDRVLen As Integer
    Dim lHenv As Long         'handle to the environment

    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    
    cboDSNList.AddItem "(None)"

    'get the DSNs
    If SQLAllocEnv(lHenv) <> -1 Then
        Do Until i <> SQL_SUCCESS
            sDSNItem = Space$(1024)
            sDRVItem = Space$(1024)
            i = SQLDataSources(lHenv, SQL_FETCH_NEXT, sDSNItem, 1024, iDSNLen, sDRVItem, 1024, iDRVLen)
            sDSN = Left$(sDSNItem, iDSNLen)
            sDRV = Left$(sDRVItem, iDRVLen)
                
            If sDSN <> Space(iDSNLen) Then
                cboDSNList.AddItem sDSN
                cboDrivers.AddItem sDRV
            End If
        Loop
    End If
    'remove the dupes
    If cboDSNList.ListCount > 0 Then
        With cboDrivers
            If .ListCount > 1 Then
                i = 0
                While i < .ListCount
                    If .List(i) = .List(i + 1) Then
                        .RemoveItem (i)
                    Else
                        i = i + 1
                    End If
                Wend
            End If
        End With
    End If
    cboDSNList.ListIndex = 0

    gbLastActivityTime = Date + Time
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub fraStep3_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub lblProperties_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub lblStep3_MouseMove(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtProperties_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtPWD_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtPWD_Validate(Cancel As Boolean)
    Call GetDBList

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtServer_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtServer_Validate(Cancel As Boolean)
    Call GetDBList

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtUID_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub

Private Sub txtUID_Validate(Cancel As Boolean)
    Call GetDBList

    gbLastActivityTime = Date + Time
End Sub

Private Sub txtView_Click()
    MsgBox GetConnStr(True)

    gbLastActivityTime = Date + Time
End Sub


Private Sub txtView_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    gbLastActivityTime = Date + Time

End Sub
