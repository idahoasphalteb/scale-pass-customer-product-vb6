Attribute VB_Name = "basMethods"
Option Explicit

Public Sub GetMASCompanyID()
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18

    
    'First Clear out data
    gbMASCompanyID = Empty
    
    If IsObject(gbMASConn) <> True Then
        Exit Sub
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Sub
    End If
    
    SQL = "Select a.CompanyID, a.FacilityID, a.FacilityLoadID, a.City, a.State "
    SQL = SQL & "From  tsoFacilityAddress_SGS A  with(Nolock) "
    SQL = SQL & "Where a.FacilitySeqNo = " & ConvertToDouble(gbPlantPrefix) & " "
    SQL = SQL & "And a.City = '" & gbPlant & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        
    Else
        RS.MoveFirst
        'Records found
        gbMASCompanyID = RS("CompanyID").Value
        gbMASLocation = RS("City").Value & ", " & RS("State").Value     'RKL DEJ 2017-05-24 Added for weight Ticket
        
    End If
    
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Sub
Error:
    lErrMsg = "basMethods.GetMASCompanyID()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Sub

Public Function GetMASCompanyName(CompanyID As String) As String
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "select * "
    SQL = SQL & "From tsmcompany "
    SQL = SQL & "Where companyid = '" & Replace(CompanyID, "'", "''") & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetMASCompanyName = "UNKNOWN COMPANY " & Trim(UCase(CompanyID))
    Else
        RS.MoveFirst
        'Records found
        If IsNull(RS("CompanyName").Value) = True Then
            GetMASCompanyName = "UNKNOWN COMPANY " & Trim(UCase(CompanyID))
        Else
            GetMASCompanyName = UCase(Trim(RS("CompanyName").Value))
        End If
        
    End If
    
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.GetMASCompanyName()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function

Public Function GetMASFacilityName() As String
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select * "
    SQL = SQL & "From  tsoFacilityAddress_SGS A  with(Nolock) "
    SQL = SQL & "Where a.FacilitySeqNo = " & ConvertToDouble(gbPlantPrefix) & " "
    SQL = SQL & "And a.City = '" & gbPlant & "' "
    
    'RKL DEJ 2017-12-06 (START)
    'New filter for Customer Owned facilities
    SQL = SQL & "And Coalesce(a.CustOwnedProd,0) = '" & Int(Abs(gbCustOwnedProd)) & "' "
    'RKL DEJ 2017-12-06 (STOP)
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetMASFacilityName = "UNKNOWN PLANT " & gbPlantPrefix
    Else
        RS.MoveFirst
        'Records found
        If IsNull(RS("FacilityName").Value) = False Then
            GetMASFacilityName = UCase(Trim(RS("FacilityName").Value))
        Else
            GetMASFacilityName = "UNKNOWN PLANT " & gbPlantPrefix
        End If
        
    End If
    
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.GetMASFacilityName()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function


Public Sub GetMASWhseKey()
    On Error GoTo Error

    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18

    'First Clear out data
    gbMASWhseKey = 0

    If IsObject(gbMASConn) <> True Then
        Exit Sub
    End If

    If gbMASConn.State <> 1 Then
        Exit Sub
    End If

    SQL = "Select w.WhseKey, w.WhseID, w.Description, a.* "
    SQL = SQL & "From  timWarehouse W with(Nolock) "
    SQL = SQL & "Inner Join tsoFacilityAddress_SGS A  with(Nolock) "
    SQL = SQL & "On w.companyid = a.companyID "
    SQL = SQL & "and w.WhseID = a.FacilityLoadID "
    SQL = SQL & "Where FacilitySeqNo = " & ConvertToDouble(gbPlantPrefix) & " "
'    SQL = SQL & "And WhseID = '" & gbPlant & "' "
    SQL = SQL & "And City = '" & gbPlant & "' "
    SQL = SQL & "And Coalesce(CustOwnedProd,0) = " & Abs(CInt(gbCustOwnedProd))

    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly

    RS.Open SQL, gbMASConn, adOpenForwardOnly

    If RS.RecordCount <= 0 Then
        'No records found

    Else
        RS.MoveFirst
        'Records found
        gbMASWhseKey = RS("WhseKey").Value

    End If


CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If

        Set RS = Nothing
    End If

    Exit Sub
Error:
    lErrMsg = "basMethods.GetMASWhseKey()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"

    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Sub

'RkL DEJ 2016-06-30 Created to get actual WhseKey
Public Sub GetMASWhseKeyByBSO(TranNo As String, ItemID As String)
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    'First Clear out data
    gbMASWhseKey = 0
    
    If IsObject(gbMASConn) <> True Then
        Exit Sub
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Sub
    End If
    
    SQL = "select top 1 so.CompanyID, so.SOKey, so.TranID, so.TranType, so.TranNo, sl.SOLineKey, sld.SOLineDistKey, i.ItemKey, i.ItemID, w.WhseKey, w.WhseID, fa.FacilityID, fa.FacilityLoadID, fa.City "
    SQL = SQL & "From tsoSalesOrder so with(NoLock) "
    SQL = SQL & "inner join tsoSOLine sl with(NoLock) "
    SQL = SQL & "    on so.SOKey = sl.SOKey "
    SQL = SQL & "inner join tsoSOLineDist sld with(NoLock) "
    SQL = SQL & "    on sl.SOLineKey = sld.SOLineKey "
    SQL = SQL & "Inner Join timItem i with(NoLock) "
    SQL = SQL & "    on sl.ItemKey = i.ItemKey "
    SQL = SQL & "Inner Join timWarehouse w with(NoLock) "
    SQL = SQL & "    on sld.WhseKey = w.WhseKey "
    SQL = SQL & "Inner join tsosolineext_sgs slx with(NoLock) "
    SQL = SQL & "    on sl.solinekey = slx.solinekey "
    SQL = SQL & "    and slx.awarded = 1 "
    SQL = SQL & "Inner Join tsoFacilityAddress_SGS fa with(NoLock) "
    SQL = SQL & "    on so.CompanyID = fa.CompanyID "
    SQL = SQL & "    and w.WhseID = fa.FacilityLoadID "
    SQL = SQL & "Where so.TranType = 802 "
    SQL = SQL & "and fa.FacilitySeqNo = " & ConvertToDouble(gbPlantPrefix) & " "
    SQL = SQL & "and fa.City = '" & gbPlant & "' "
    SQL = SQL & "and so.TranNo = '" & TranNo & "' "
    SQL = SQL & "and i.ItemID = '" & ItemID & "' "
    SQL = SQL & "and so.CompanyID = '" & gbMASCompanyID & "' "
            
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
    Else
        RS.MoveFirst
        'Records found
        gbMASWhseKey = RS("WhseKey").Value
        
    End If
    
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Sub
Error:
    lErrMsg = "basMethods.GetMASWhseKeyByBSO()" & vbCrLf & _
    "TranNo = " & TranNo & vbCrLf & _
    "ItemID = " & ItemID & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Sub

'RkL DEJ 2017-07-31 - Get Warranty Chip Seal flag from MAS Contract
Public Function GetMASWarrantyChipSealFlg(TranNo As String) As Long
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    GetMASWarrantyChipSealFlg = 0
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "select top 1 so.CompanyID, so.SOKey, so.TranID, so.TranType, so.TranNo, sox.WarrantyChipSeal "
    SQL = SQL & "From tsoSalesOrder so with(NoLock) "
    SQL = SQL & "Left Outer Join tsoSalesOrderExt_SGS sox with(NoLock) "
    SQL = SQL & "    on so.SOKey = sox.SOKey "
    SQL = SQL & "Where so.TranType = 802 "
    SQL = SQL & "and so.TranNo = '" & TranNo & "' "
    SQL = SQL & "and so.CompanyID = '" & gbMASCompanyID & "' "
            
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetMASWarrantyChipSealFlg = 0
        
    Else
        RS.MoveFirst
        'Records found
        GetMASWarrantyChipSealFlg = CLng(ConvertToDouble(RS("WarrantyChipSeal").Value))
        
    End If
    
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.GetMASWarrantyChipSealFlg()" & vbCrLf & _
    "TranNo = " & TranNo & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function


Public Function IsCertRequired(ContractNo As String, WhseID As String, ItemID As String) As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    'Defalut to False
    IsCertRequired = False
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select so.SOKey, so.TranID, so.TranNo, so.CompanyID, IsNull(sx.CertRequired,0) 'IsCertRequired', "
    SQL = SQL & "i.ItemKey, i.ItemID, fa.FacilityLoadID, w.WhseKey, w.WhseID, idesc.ShortDesc "
    SQL = SQL & "From tsoSalesOrder so with(NOLock) "
    SQL = SQL & "Inner Join tsoSOLine sl with(NoLock) "
    SQL = SQL & "on so.sokey = sl.sokey "
    SQL = SQL & "Inner Join tsoSOLineDist sd with(NoLock) "
    SQL = SQL & "on sl.solinekey = sd.solinekey "
    SQL = SQL & "Inner Join tsoSOLineExt_SGS sx with(NoLock) "
    SQL = SQL & "on sl.soLinekey = sx.soLinekey "
    SQL = SQL & "Inner Join timItem i with(NOLock) "
    SQL = SQL & "on sl.itemKey = i.itemKey "
    SQL = SQL & "Inner Join timWarehouse w with(NoLock) "
    SQL = SQL & "on sd.whsekey = w.whsekey "
    SQL = SQL & "Inner Join timItemDescription idesc with(NoLock) "
    SQL = SQL & "on i.ItemKey = idesc.ItemKey "
    
    SQL = SQL & "Inner Join tsoFacilityAddress_SGS FA with(NoLock) "
    SQL = SQL & "on w.WhseID = fa.FacilityLoadID "
    
    SQL = SQL & "where so.CompanyID = '" & gbMASCompanyID & "' "
    SQL = SQL & "and so.TranType = '802' "
    SQL = SQL & "and so.Tranno = '" & Trim(ContractNo) & "' "
    SQL = SQL & "and i.ItemID = '" & Trim(ItemID) & "' "
    SQL = SQL & "and w.WhseKey = " & gbMASWhseKey & " "
    SQL = SQL & "And IsNull(sx.Awarded,0) = 1 "
    
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        IsCertRequired = False
    Else
        RS.MoveFirst
        'Records found
        If RS("IsCertRequired").Value = 1 Then
            IsCertRequired = True
        Else
            IsCertRequired = False
        End If
        
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.IsCertRequired()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function

Public Function IsCertRequired_Orig(ContractNo As String) As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    'Defalut to False
    IsCertRequired_Orig = False
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select top 100 so.SOKey, so.TranID, so.TranNo, so.CompanyID, IsNull(sx.CertRequired,0) 'IsCertRequired_Orig' "
    SQL = SQL & "From tsoSalesOrder so with(NOLock) "
    SQL = SQL & "Left Outer Join tsoSalesOrderExt_SGS sx with(NoLock) "
    SQL = SQL & "on so.sokey = sx.sokey "
    SQL = SQL & "where so.CompanyID = '" & gbMASCompanyID & "' "
    SQL = SQL & "and so.TranType = '802' "
    SQL = SQL & "and so.Tranno = '" & Trim(ContractNo) & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        IsCertRequired_Orig = False
    Else
        RS.MoveFirst
        'Records found
        If RS("IsCertRequired_Orig").Value = 1 Then
            IsCertRequired_Orig = True
        Else
            IsCertRequired_Orig = False
        End If
        
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.IsCertRequired_Orig()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
        
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function

Public Function CanPrintBOL(ContractNo As String) As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    'Defalut to False
    CanPrintBOL = False
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select top 100 so.SOKey, so.TranID, so.TranNo, so.CompanyID, IsNull(sx.CanPrintBOL,0) 'CanPrintBOL' "
    SQL = SQL & "From tsoSalesOrder so with(NOLock) "
    SQL = SQL & "Left Outer Join tsoSalesOrderExt_SGS sx with(NoLock) "
    SQL = SQL & "on so.sokey = sx.sokey "
    SQL = SQL & "where so.CompanyID = '" & gbMASCompanyID & "' "
    SQL = SQL & "and so.TranType = '802' "
    SQL = SQL & "and so.Tranno = '" & Trim(ContractNo) & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        CanPrintBOL = False
    Else
        RS.MoveFirst
        'Records found
        If RS("CanPrintBOL").Value = 1 Then
            CanPrintBOL = True
        Else
            CanPrintBOL = False
        End If
        
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.CanPrintBOL()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function



Public Function GetLotNumber(Tank As String, ItemID As String, Optional COATestRsltsKey As Long = 0) As String
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim ItemKey As Long
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    'Defalut to no lot no
    GetLotNumber = Empty
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    
    ItemKey = GetMASItemKey(ItemID)
    
    If ItemKey <= 0 Then
        'Item Key not found
        Exit Function
    End If
    
    '*******************************************************************************************
    'EB DEJ 6/12/13 Changed to Lookup by SQL function (START)
    'This change was to accomidate the possible base item for ('Dilute Emuls', 'Emulsion')
    '*******************************************************************************************
'    SQL = "select * "
'    SQL = SQL & "From vluIMCOATestResultsHdr_SGS "
'    SQL = SQL & "Where IsActive = 1 "
'    SQL = SQL & "and IsNull(IsHistoryRecord,0) = 0 "
'    SQL = SQL & "and CompanyID = '" & gbMASCompanyID & "' "
'    SQL = SQL & "And Ltrim(Rtrim(Tank)) = '" & Trim(Tank) & "' "
'    SQL = SQL & "and ItemKey = " & ItemKey & " "
'    SQL = SQL & "and WhseKey = " & gbMASWhseKey & " "

    SQL = "select * "
    SQL = SQL & "From vluIMCOATestResultsHdr_SGS "
    SQL = SQL & "Where COATestRsltsKey = dbo.fnImGetCOATestRsltsKeyNoLotNbr_SGS_EB(" & ItemKey & ", " & gbMASWhseKey & ", '" & Trim(Tank) & "', GetDate()) "
    '*******************************************************************************************
    'This change was to accomidate the possible base item for ('Dilute Emuls', 'Emulsion')
    'EB DEJ 6/12/13 Changed to Lookup by SQL function (STOP)
    '*******************************************************************************************

    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetLotNumber = Empty
    Else
        RS.MoveFirst
        'Records found
        GetLotNumber = Trim(RS("LotNumber").Value)
        COATestRsltsKey = CLng(ConvertToDouble(RS("COATestRsltsKey").Value))    'RKL DEJ 2017-12-21 Added so we can pass the COATestRsltsKey back to 500
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.GetLotNumber()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function

Public Function GetCOARptFile(Tank As String, ItemID As String) As String
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim ItemKey As Long
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    'Defalut to no lot no
    GetCOARptFile = Empty
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    
    ItemKey = GetMASItemKey(ItemID)
    
    If ItemKey <= 0 Then
        'Item Key not found
        Exit Function
    End If
    
    
    '*******************************************************************************************
    'EB DEJ 6/12/13 Changed to Lookup by SQL function (START)
    'This change was to accomidate the possible base item for ('Dilute Emuls', 'Emulsion')
    '*******************************************************************************************
'    SQL = "select * "
'    SQL = SQL & "From vluIMCOATestResultsHdr_SGS "
'    SQL = SQL & "Where IsActive = 1 "
'    SQL = SQL & "and IsNull(IsHistoryRecord,0) = 0 "
'    SQL = SQL & "and CompanyID = '" & gbMASCompanyID & "' "
'    SQL = SQL & "And Ltrim(Rtrim(Tank)) = '" & Trim(Tank) & "' "
'    SQL = SQL & "and ItemKey = " & ItemKey & " "
'    SQL = SQL & "and WhseKey = " & gbMASWhseKey & " "

    SQL = "select * "
    SQL = SQL & "From vluIMCOATestResultsHdr_SGS "
    SQL = SQL & "Where COATestRsltsKey = dbo.fnImGetCOATestRsltsKeyNoLotNbr_SGS_EB(" & ItemKey & ", " & gbMASWhseKey & ", '" & Trim(Tank) & "', GetDate()) "
    '*******************************************************************************************
    'This change was to accomidate the possible base item for ('Dilute Emuls', 'Emulsion')
    'EB DEJ 6/12/13 Changed to Lookup by SQL function (STOP)
    '*******************************************************************************************


    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetCOARptFile = Empty
    Else
        RS.MoveFirst
        'Records found
        If IsNull(RS("CrystalRptFileName").Value) = True Then
            GetCOARptFile = Empty
        Else
            GetCOARptFile = Trim(RS("CrystalRptFileName").Value)
        End If
        
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.GetCOARptFile()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function



Public Function GetMASItemKey(ItemID As String) As Long
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim lErrMsg As String   'RKL DEJ 2018-01-18
    
    'Set Default
    GetMASItemKey = 0
    
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select * "
    SQL = SQL & "From timItem with(NoLock) "
    SQL = SQL & "Where ItemID = '" & Trim(ItemID) & "' "
    SQL = SQL & "And CompanyID = '" & gbMASCompanyID & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        
    Else
        RS.MoveFirst
        'Records found
        GetMASItemKey = RS("ItemKey").Value
        
    End If
    
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.GetMASItemKey()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    InsErrorLog 0, Date + Time, lErrMsg 'RKL DEJ 2018-01-18
    
    GoTo CleanUP
End Function

Function UpdateRptDBRefNew(ByRef Rpt As CRAXDRt.Report) As Boolean
    On Error GoTo Error
        
    Dim crxDatabase As CRAXDRt.Database
    Dim crxDatabaseTables As CRAXDRt.DatabaseTables
    Dim crxDatabaseTable As CRAXDRt.DatabaseTable
    
    Dim ConnPrty As CRAXDRt.ConnectionProperty
    
    Dim bTrustedConnSet As Boolean
    bTrustedConnSet = False

    
    'Set your Database object to the Report object's Database object
    Set crxDatabase = Rpt.Database
    
    'Set your DatabaseTables object to the Database object's Tables object
    Set crxDatabaseTables = crxDatabase.Tables
    
    'Loop through each DatabaseTable object in the DatabaseTables collection and then set the location
    'of the database file for each table
    
    For Each crxDatabaseTable In crxDatabaseTables

'        'If you are using physical path of the access database file (Native connection to PC database)
'        crxDatabaseTable.Location = App.Path & "\xtremelite.mdb"

        'Use next line, if you are using Native connection to SQL database
        'crxDatabaseTable.SetLogOnInfo "servername", "databasename", "userid", "password"

        'Use next line, if you are using ODBC connection to a PC or SQL database
'        crxDatabaseTable.SetLogOnInfo "MAS_COA", "MAS500_App_IAS_74", "admin", "eyeball"
        
        
        For Each ConnPrty In crxDatabaseTable.ConnectionProperties
            Select Case UCase(Trim(ConnPrty.Name))
                Case "DSN"
                    If UCase(Trim(gbMASDSN)) = "(NONE)" Then
'                        ConnPrty.Value = Empty
                        ConnPrty.Value = ConnPrty.Value
                    Else
                        ConnPrty.Value = Trim(gbMASDSN)
                    End If
                Case "TABLE NAME"
                    Debug.Print UCase(Trim(ConnPrty.Name))
                Case "TABLE TYPE"
                    Debug.Print UCase(Trim(ConnPrty.Name))
                Case "CATALOG"
                    ConnPrty.Value = Trim(gbMASDB)
                Case "OWNER"
                    Debug.Print UCase(Trim(ConnPrty.Name))
                Case "FILE NAME"
                    Debug.Print UCase(Trim(ConnPrty.Name))
                Case "OVERRIDDEN QUALIFIED TABLE NAME"
                    Debug.Print UCase(Trim(ConnPrty.Name))
'                Case "USER ID"
'                    ConnPrty.Value = txtUID.Text
                Case "PASSWORD", "PWD"
                        ConnPrty.Value = gbMASPWD
                Case "DATABASE"
                    ConnPrty.Value = Trim(gbMASDB)
                Case "USEDSNPROPERTIES"
'                    ConnPrty.Value = False
'                    ConnPrty.Value = True
                    
                    If UCase(Trim(gbMASDSN)) = "(NONE)" Then
                        ConnPrty.Value = False
                    Else
                        ConnPrty.Value = True
                    End If
                    
                Case "PREQESERVERTYPE"
                    ConnPrty.Value = "ODBC (RDO)"
                Case "PREQESERVERNAME"
                    ConnPrty.Value = Trim(gbMASDSN)
                Case "PREQEDATABASENAME"
                    ConnPrty.Value = Trim(gbMASDB)
                Case "USERID", "USER ID"
                    ConnPrty.Value = gbMASUID
                Case "SERVER"
                    ConnPrty.Value = Trim(gbMASServer)
                Case "INTEGRATED SECURITY", "INTEGRATEDSECURITY", "INTEGRATED_SECURITY"
                    If gbMASWinAuth = True Then
                        bTrustedConnSet = True
                        ConnPrty.Value = "SSPI"
                    End If
                Case "TRUSTED_CONNECTION", "TRUSTEDCONNECTION", "TRUSTED CONNECTION"
                    If gbMASWinAuth = True Then
                        bTrustedConnSet = True
                        ConnPrty.Value = True
                    End If
                Case Else
                    Debug.Print UCase(Trim(ConnPrty.Name))
            End Select

        Next
        
        If gbMASWinAuth = True Then
            crxDatabaseTable.ConnectionProperties.Add "Integrated Security", "SSPI"
            crxDatabaseTable.ConnectionProperties.Add "TRUSTED_CONNECTION", True
        End If
        
        If UCase(Trim(gbMASDSN)) = "(NONE)" Then
            crxDatabaseTable.SetLogOnInfo Empty, Trim(gbMASDB), gbMASUID, gbMASPWD
        Else
            crxDatabaseTable.SetLogOnInfo Trim(gbMASDSN), Trim(gbMASDB), gbMASUID, gbMASPWD
        End If
        
'        crxDatabaseTable.SetLogOnInfo Trim(gbMASDSN), Trim(gbMASDB), gbMASUID, gbMASPWD
        
        crxDatabaseTable.Location = Trim(gbMASDB) & ".dbo." & crxDatabaseTable.Location
        
                
        
    Next crxDatabaseTable
    
    
    Set ConnPrty = Nothing
    Set crxDatabase = Nothing
    Set crxDatabaseTable = Nothing
    Set crxDatabaseTables = Nothing

    UpdateRptDBRefNew = True
    
    Exit Function
Error:
    MsgBox "basMethods.UpdateRptDBRefNew()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear

End Function

Function UpdateSubRptDBRefNew(ByRef Rpt As CRAXDRt.Report) As Boolean
    On Error GoTo Error
    
    Dim crxSubreport As CRAXDRt.Report
    Dim crxSubreportObject As SubreportObject
    Dim crxDatabase As CRAXDRt.Database
    Dim crxDatabaseTables As CRAXDRt.DatabaseTables
    Dim crxDatabaseTable As CRAXDRt.DatabaseTable
    Dim crxSections As CRAXDRt.Sections
    Dim crxSection As CRAXDRt.Section
    Dim CRXReportObject As Object
    
    Dim ConnPrty As CRAXDRt.ConnectionProperty
    
    Dim bTrustedConnSet As Boolean
    bTrustedConnSet = False

    Set crxSections = Rpt.Sections
    
    
    For Each crxSection In crxSections
        For Each CRXReportObject In crxSection.ReportObjects
           If CRXReportObject.Kind = crSubreportObject Then
             Set crxSubreportObject = CRXReportObject
             Set crxSubreport = crxSubreportObject.OpenSubreport
             Set crxDatabase = crxSubreport.Database
             Set crxDatabaseTables = crxDatabase.Tables
             
             For Each crxDatabaseTable In crxDatabaseTables
                'If you are using a  Native connection to PC database
                'crxDatabaseTable.Location = App.Path & "\xtremelite.mdb"
                
                'Use next line, if you are using Native connection to SQL database
                'crxDatabaseTable.SetLogOnInfo "servername", "databasename", "userid", "password"
                
                'Use next line, if you are using ODBC connection to a PC or SQL database
'                crxDatabaseTable.SetLogOnInfo "MAS 500", Trim(gbMASDB), "admin", "eyeball"
             
             
                For Each ConnPrty In crxDatabaseTable.ConnectionProperties
                    Select Case UCase(Trim(ConnPrty.Name))
                        Case "DSN"
                            If UCase(Trim(gbMASDSN)) = "(NONE)" Then
                                ConnPrty.Value = Empty
                            Else
                                ConnPrty.Value = Trim(gbMASDSN)
                            End If
                        Case "TABLE NAME"
                            Debug.Print UCase(Trim(ConnPrty.Name))
                        Case "TABLE TYPE"
                            Debug.Print UCase(Trim(ConnPrty.Name))
                        Case "CATALOG"
                            ConnPrty.Value = Trim(gbMASDB)
                        Case "OWNER"
                            Debug.Print UCase(Trim(ConnPrty.Name))
                        Case "FILE NAME"
                            Debug.Print UCase(Trim(ConnPrty.Name))
                        Case "OVERRIDDEN QUALIFIED TABLE NAME"
                            Debug.Print UCase(Trim(ConnPrty.Name))
'                        Case "USER ID"
'                            ConnPrty.Value = gbMASUID
                        Case "PASSWORD"
                                ConnPrty.Value = gbMASPWD
                        Case "DATABASE"
                            ConnPrty.Value = Trim(gbMASDB)
                        Case "USEDSNPROPERTIES"
'                            ConnPrty.Value = False
                            If UCase(Trim(gbMASDSN)) = "(NONE)" Then
                                ConnPrty.Value = False
                            Else
                                ConnPrty.Value = True
                            End If
                            
                        Case "PREQESERVERTYPE"
                            ConnPrty.Value = "ODBC (RDO)"
                        Case "PREQESERVERNAME"
                            ConnPrty.Value = Trim(gbMASDSN)
                        Case "PREQEDATABASENAME"
                            ConnPrty.Value = Trim(gbMASDB)
                        Case "USERID", "USER ID"
                            ConnPrty.Value = gbMASUID
                        Case "SERVER"
                            ConnPrty.Value = Trim(gbMASServer)
                        Case "INTEGRATED SECURITY", "INTEGRATEDSECURITY", "INTEGRATED_SECURITY"
                            If gbMASWinAuth = True Then
                                bTrustedConnSet = True
                                ConnPrty.Value = "SSPI"
                            End If
                        Case "TRUSTED_CONNECTION", "TRUSTEDCONNECTION", "TRUSTED CONNECTION"
                            If gbMASWinAuth = True Then
                                bTrustedConnSet = True
                                ConnPrty.Value = True
                            End If
        
                        Case Else
                            Debug.Print UCase(Trim(ConnPrty.Name))
                    End Select
        
                Next
                
                If gbMASWinAuth = True Then
                    crxDatabaseTable.ConnectionProperties.Add "Integrated Security", "SSPI"
                    crxDatabaseTable.ConnectionProperties.Add "TRUSTED_CONNECTION", True
                End If
                
                
                crxDatabaseTable.SetLogOnInfo Trim(gbMASDSN), Trim(gbMASDB), gbMASUID, gbMASPWD
                
                crxDatabaseTable.Location = Trim(gbMASDB) & ".dbo." & crxDatabaseTable.Location
             
             Next
             
             Set crxSubreport = Nothing
           End If
        Next
    Next
    
    Set crxDatabase = Nothing
    Set crxDatabaseTable = Nothing
    Set crxDatabaseTables = Nothing
    Set crxSections = Nothing
    Set crxSection = Nothing
    Set crxSubreportObject = Nothing
    
    UpdateSubRptDBRefNew = True
    
    Exit Function
Error:
    MsgBox "basMethods.UpdateSubRptDBRefNew()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    

End Function


Function UpdateRptDBRef(Optional TypeOfConn As String = "ODBC") As Boolean
    On Error GoTo Error
    'This method is assuming that the MAS 500 connection is the connection unless we are
    'connecting to the access database in which it used the Scale Pass database
    
    Dim crxDatabase As CRAXDRt.Database
    Dim crxDatabaseTables As CRAXDRt.DatabaseTables
    Dim crxDatabaseTable As CRAXDRt.DatabaseTable
    
    Dim MAS500DB As String
    
    MAS500DB = gbMASConn.DefaultDatabase
    
    'Set your Database object to the Report object's Database object
    Set crxDatabase = crxRpt.Database
    
    'Set your DatabaseTables object to the Database object's Tables object
    Set crxDatabaseTables = crxDatabase.Tables
    
    'Loop through each DatabaseTable object in the DatabaseTables collection and then set the location
    'of the database file for each table
    
    For Each crxDatabaseTable In crxDatabaseTables
        Select Case Trim(UCase(TypeOfConn))
            Case "PCDATABASE"
                'If you are using a  Native connection to PC database
                'crxDatabaseTable.Location = App.Path & "\xtremelite.mdb"
                crxDatabaseTable.Location = gbScaleDB
            
            Case "NATIVESQL"
                'Use next line, if you are using Native connection to SQL database
                'crxDatabaseTable.SetLogOnInfo "servername", "databasename", "userid", "password"
'                crxDatabaseTable.SetLogOnInfo gbMASServer, gbMASDB, gbMASUID, gbMASPWD
                crxDatabaseTable.SetLogOnInfo gbMASServer, MAS500DB, gbMASUID, gbMASPWD
                crxDatabaseTable.Location = MAS500DB & ".." & crxDatabaseTable.Location
            
            Case "ODBC"
                'Use next line, if you are using ODBC connection to a PC or SQL database
                'crxDatabaseTable.SetLogOnInfo "ODBC_DSN", "databasename", "userid", "password"
                'crxDatabaseTable.SetLogOnInfo gbMASDSN, gbMASDB, gbMASUID, gbMASPWD
'                crxDatabaseTable.SetLogOnInfo "MAS_COA", gbMASDB, gbMASUID, gbMASPWD
                crxDatabaseTable.SetLogOnInfo "MAS 500", MAS500DB, gbMASUID, gbMASPWD
                crxDatabaseTable.Location = MAS500DB & ".." & crxDatabaseTable.Location
                
            Case Else
                'Unknown so default to odbc
                'Use next line, if you are using ODBC connection to a PC or SQL database
                'crxDatabaseTable.SetLogOnInfo gbMASDSN, gbMASDB, gbMASUID, gbMASPWD
                If Trim(gbMASDB) = Empty Then
                Else
                End If
                crxDatabaseTable.SetLogOnInfo "MAS 500", MAS500DB, gbMASUID, gbMASPWD
                crxDatabaseTable.Location = MAS500DB & ".." & crxDatabaseTable.Location
                
        End Select
    Next crxDatabaseTable
    
    crxDatabase.Verify
    
    Set crxDatabase = Nothing
    Set crxDatabaseTable = Nothing
    Set crxDatabaseTables = Nothing

    UpdateRptDBRef = True
    
    Exit Function
Error:
    MsgBox "basMethods.UpdateRptDBRef()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
End Function

Function UpdateSubRptDBRef(Optional TypeOfConn As String = "ODBC") As Boolean
    On Error GoTo Error
    'This method is assuming that the MAS 500 connection is the connection unless we are
    'connecting to the access database in which it used the Scale Pass database
    
    Dim crxSubreport As CRAXDRt.Report
    Dim crxSubreportObject As SubreportObject
    Dim crxDatabase As CRAXDRt.Database
    Dim crxDatabaseTables As CRAXDRt.DatabaseTables
    Dim crxDatabaseTable As CRAXDRt.DatabaseTable
    Dim crxSections As CRAXDRt.Sections
    Dim crxSection As CRAXDRt.Section
    Dim CRXReportObject As Object
    Dim MAS500DB As String
    
    MAS500DB = gbMASConn.DefaultDatabase

    Set crxSections = crxRpt.Sections
    
    For Each crxSection In crxSections
        For Each CRXReportObject In crxSection.ReportObjects
           If CRXReportObject.Kind = crSubreportObject Then
             Set crxSubreportObject = CRXReportObject
             Set crxSubreport = crxSubreportObject.OpenSubreport
             Set crxDatabase = crxSubreport.Database
             Set crxDatabaseTables = crxDatabase.Tables
             
             For Each crxDatabaseTable In crxDatabaseTables
                Select Case Trim(UCase(TypeOfConn))
                    Case "PCDATABASE"
                        'If you are using a  Native connection to PC database
                        'crxDatabaseTable.Location = App.Path & "\xtremelite.mdb"
                        crxDatabaseTable.Location = gbScaleDB
                    
                    Case "NATIVESQL"
                        'Use next line, if you are using Native connection to SQL database
                        crxDatabaseTable.SetLogOnInfo gbMASServer, MAS500DB, gbMASUID, gbMASPWD
                        crxDatabaseTable.Location = gbMASDB & ".." & crxDatabaseTable.Location
                    
                    Case "ODBC"
                        'Use next line, if you are using ODBC connection to a PC or SQL database
                        crxDatabaseTable.SetLogOnInfo gbMASDSN, MAS500DB, gbMASUID, gbMASPWD
                        crxDatabaseTable.Location = gbMASDB & ".." & crxDatabaseTable.Location
                        
                    Case Else
                        'Unknown so default to odbc
                        'Use next line, if you are using ODBC connection to a PC or SQL database
                        crxDatabaseTable.SetLogOnInfo gbMASDSN, MAS500DB, gbMASUID, gbMASPWD
                        crxDatabaseTable.Location = gbMASDB & ".." & crxDatabaseTable.Location
                        
                End Select
             Next
             
             Set crxSubreport = Nothing
             
             crxDatabase.Verify
             
           End If
            
        Next
    Next
    
    
    Set crxDatabase = Nothing
    Set crxDatabaseTable = Nothing
    Set crxDatabaseTables = Nothing
    Set crxSections = Nothing
    Set crxSection = Nothing
    Set crxSubreportObject = Nothing
    
    UpdateSubRptDBRef = True
    
    Exit Function
Error:
    MsgBox "basMethods.UpdateSubRptDBRef()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    

End Function



Public Function InsMASErrorLog(BOLPrintingErrorKey As Long, BOLTableKey As Long, ErrorDate As Date, ErrorMessage As String) As Boolean
    On Error GoTo Error
    
    Dim SQL As String
    Dim lErrMsg As String
    Dim CMD As New ADODB.Command
        
    Dim ErrorLogKey As Long
    Dim SQLErrorNbr As Long
    Dim SQLErrorMsg As String
    Dim bSuccess As Long
    Dim lcErrMsg As String  'RkL DEJ 2016-06-29
    
    gbLastActivityTime = Date + Time
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    'We don't want to loose the error log to a transaction so we are assuming that due to errors we will rollback any possible open transaction to MAS
    Call MASRollBack
    
'    @CompanyID varchar(3),
'    @WhseKey Int ,
'    @BOLPrintingErrorKey int ,
'    @BOLTableKey int ,
'    @ErrorDate DateTime ,
'    @ErrorMessage Varchar(8000) ,
'    @ErrorLogKey int ,
'    @SQLErrorNbr int,
'    @SQLErrorMsg Varchar(8000),
'    @Success int
    
    'RkL DEJ 2016-06-29 (start)
    lcErrMsg = ErrorMessage & vbCrLf & "Forms Data: " & vbCrLf & GetCurrentData()
    'RkL DEJ 2016-06-29 (stop)
    
    Set CMD.ActiveConnection = gbMASConn
    CMD.CommandType = adCmdStoredProc
    CMD.CommandText = "spSOLogScalePassErrors_SGS"
    CMD.Parameters.Refresh
    CMD.CommandTimeout = 0      'Added to prevent time outs

    'Assign input parameter Values
    CMD.Parameters("@CompanyID").Value = gbMASCompanyID
    CMD.Parameters("@WhseKey").Value = gbMASWhseKey
    CMD.Parameters("@BOLPrintingErrorKey").Value = BOLPrintingErrorKey
    CMD.Parameters("@BOLTableKey").Value = BOLTableKey
    CMD.Parameters("@ErrorDate").Value = ErrorDate
    CMD.Parameters("@ErrorMessage").Value = lcErrMsg        'RkL DEJ 2016-06-29 changed ErrorMessage to lcErrMsg
    CMD.Parameters("@ErrorLogKey").Value = ErrorLogKey
    CMD.Parameters("@SQLErrorNbr").Value = SQLErrorNbr
    CMD.Parameters("@SQLErrorMsg").Value = SQLErrorMsg
    CMD.Parameters("@Success").Value = bSuccess
    
    CMD.Execute
    
'    ErrorLogKey = ConvertToDouble(CMD.Parameters("@ErrorLogKey").Value)
'    SQLErrorNbr = CMD.Parameters("@SQLErrorNbr").Value
'    SQLErrorMsg = CMD.Parameters("@SQLErrorMsg").Value
    bSuccess = CMD.Parameters("@Success").Value
    
    If bSuccess = 1 Then
        InsMASErrorLog = True
    Else
        InsMASErrorLog = False
    End If
    
    
CleanUP:
    On Error Resume Next
    Set CMD = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMethods.InsMASErrorLog(BOLPrintingErrorKey As Long, BOLTableKey As Long, ErrorDate As Date, ErrorMessage As String) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    lErrMsg = lErrMsg & vbCrLf & vbCrLf & _
    "BOLPrintingErrorKey = " & BOLPrintingErrorKey & vbCrLf & _
    "BOLTableKey = " & BOLTableKey & vbCrLf & _
    "ErrorDate = " & ErrorDate & vbCrLf & _
    "ErrorMessage = " & ErrorMessage & vbCrLf
    
    basMain.WriteToLogFile lErrMsg
    
'    MsgBox lErrMsg, vbCritical, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function


Public Sub MASRollBack()
    On Error Resume Next
    gbMASConn.RollbackTrans
    Err.Clear
End Sub


Public Function IsHighRiskBSO(ContractNo As String, WhseID As String, ItemID As String) As Boolean
    On Error GoTo Error
'Global gbMASWhseKey As Long
'Global gbMASCompanyID As String
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    
    'Defalut to False
    IsHighRiskBSO = False
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
'    SQL = "Select so.SOKey, so.TranID, so.TranNo, so.CompanyID, IsNull(slx.QASample,0) 'HighRisk' "
'    SQL = SQL & "From tsoSalesOrder so with(NOLock) "
'    SQL = SQL & "Left Outer Join tsoSalesOrderExt_SGS sx with(NoLock) "
'    SQL = SQL & "on so.sokey = sx.sokey "
'    SQL = SQL & "Inner Join tsoSOLine sl with(NoLock) "
'    SQL = SQL & "on so.SOKey = sl.SOKey "
'    SQL = SQL & "Left Outer Join tsoSOLineExt_SGS slx with(NOLock) "
'    SQL = SQL & "on sl.SOLineKey = slx.SOLineKey "
'    SQL = SQL & "where so.CompanyID = '" & gbMASCompanyID & "' "
'    SQL = SQL & "and so.TranType = '802' "
'    SQL = SQL & "and so.Tranno = '" & Trim(ContractNo) & "' "
'    SQL = SQL & "And ItemKey = ??? "
'    SQL = SQL & "And WhseKey = ??? "
    
    SQL = "Select so.SOKey, so.TranID, so.TranNo, so.CompanyID, IsNull(sx.QASample,0) 'HighRisk', "
    SQL = SQL & "i.ItemKey, i.ItemID, fa.FacilityLoadID, w.WhseKey, w.WhseID, idesc.ShortDesc "
    SQL = SQL & "From tsoSalesOrder so with(NOLock) "
    SQL = SQL & "Inner Join tsoSOLine sl with(NoLock) "
    SQL = SQL & "on so.sokey = sl.sokey "
    SQL = SQL & "Inner Join tsoSOLineDist sd with(NoLock) "
    SQL = SQL & "on sl.solinekey = sd.solinekey "
    SQL = SQL & "Inner Join tsoSOLineExt_SGS sx with(NoLock) "
    SQL = SQL & "on sl.soLinekey = sx.soLinekey "
    SQL = SQL & "Inner Join timItem i with(NOLock) "
    SQL = SQL & "on sl.itemKey = i.itemKey "
    SQL = SQL & "Inner Join timWarehouse w with(NoLock) "
    SQL = SQL & "on sd.whsekey = w.whsekey "
    SQL = SQL & "Inner Join timItemDescription idesc with(NoLock) "
    SQL = SQL & "on i.ItemKey = idesc.ItemKey "
    
    SQL = SQL & "Inner Join tsoFacilityAddress_SGS FA with(NoLock) "
    SQL = SQL & "on w.WhseID = fa.FacilityLoadID "
    
    SQL = SQL & "where so.CompanyID = '" & gbMASCompanyID & "' "
    SQL = SQL & "and so.TranType = '802' "
    SQL = SQL & "and so.Tranno = '" & Trim(ContractNo) & "' "
    SQL = SQL & "and i.ItemID = '" & Trim(ItemID) & "' "
    SQL = SQL & "and w.WhseKey = " & gbMASWhseKey & " "
    SQL = SQL & "And IsNull(sx.Awarded,0) = 1 "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        IsHighRiskBSO = False
    Else
        RS.MoveFirst
        'Records found
        If IsNull(RS("HighRisk").Value) = True Then
            IsHighRiskBSO = False
        Else
            If RS("HighRisk").Value = 1 Then
                IsHighRiskBSO = True
            Else
                IsHighRiskBSO = False
            End If
        End If
        
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    MsgBox "basMethods.IsHighRiskBSO()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    GoTo CleanUP
End Function

'*******************************************************************************************
'*******************************************************************************************
'RKL DEJ 9/25/13 (START)
'*******************************************************************************************
'*******************************************************************************************
Public Function GetCOAViscosity(Tank As String, ItemID As String, DfltViscosity As String) As String
    On Error GoTo Error
'Global gbMASWhseKey As Long
'Global gbMASCompanyID As String
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim ItemKey As Long
    
    'Defalut to input value
    GetCOAViscosity = DfltViscosity
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    
    ItemKey = GetMASItemKey(ItemID)
    
    If ItemKey <= 0 Then
        'Item Key not found
        Exit Function
    End If
    
    SQL = "select Top 1 * "
    SQL = SQL & "From vluIMCOATestResultsDetl_SGS "
    SQL = SQL & "Where COATestRsltsKey = dbo.fnImGetCOATestRsltsKeyNoLotNbr_SGS_EB(" & ItemKey & ", " & gbMASWhseKey & ", '" & Trim(Tank) & "', GetDate()) "
    SQL = SQL & "And LabTestID like '%viscosity%' "
    SQL = SQL & "order by COATestSortOrder asc "

    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetCOAViscosity = DfltViscosity
    Else
        RS.MoveFirst
        'Records found
        GetCOAViscosity = ConvertToString(RS("TestResults").Value)
        
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    MsgBox "basMethods.GetCOAViscosity()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    GoTo CleanUP
End Function

'***********************************************************************************
'RKL DEJ 4/10/14
'(the datatype in the database is a string so kepping it as a string.)
'Sometimes the conversion is not correct i.e. convert 63.3 to 63.299999999...
'***********************************************************************************
Public Function GetCOAResidue(Tank As String, ItemID As String, DfltResidue As String) As String
'Public Function GetCOAResidue(Tank As String, ItemID As String, DfltResidue As Double) As Double
    On Error GoTo Error
'Global gbMASWhseKey As Long
'Global gbMASCompanyID As String
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim ItemKey As Long
    
    'Defalut to input value
    GetCOAResidue = DfltResidue
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    
    ItemKey = GetMASItemKey(ItemID)
    
    If ItemKey <= 0 Then
        'Item Key not found
        Exit Function
    End If
    
    SQL = "select Top 1 * "
    SQL = SQL & "From vluIMCOATestResultsDetl_SGS "
    SQL = SQL & "Where COATestRsltsKey = dbo.fnImGetCOATestRsltsKeyNoLotNbr_SGS_EB(" & ItemKey & ", " & gbMASWhseKey & ", '" & Trim(Tank) & "', GetDate()) "
    SQL = SQL & "And LabTestID = 'Residue (% by mass)' "
    SQL = SQL & "order by COATestSortOrder asc "

    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetCOAResidue = DfltResidue
    Else
        RS.MoveFirst
        'Records found
        '***********************************************************************************
        'RKL DEJ 4/10/14 (START)
        '(the datatype in the database is a string so kepping it as a string.)
        'Sometimes the conversion is not correct i.e. convert 63.3 to 63.299999999...
        '***********************************************************************************
        GetCOAResidue = Trim(ConvertToString(RS("TestResults").Value))
'        GetCOAResidue = ConvertToDouble(RS("TestResults").Value)
        '***********************************************************************************
        'RKL DEJ 4/10/14 (STOP)
        '***********************************************************************************
        
    End If
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    MsgBox "basMethods.GetCOAResidue()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    GoTo CleanUP
End Function



'*******************************************************************************************
'*******************************************************************************************
'RKL DEJ 9/25/13 (STOP)
'*******************************************************************************************
'*******************************************************************************************

'*****************************************************************************************************************************
'RKL DEJ 9/25/13 (START) New Method PrintLabel()
'*****************************************************************************************************************************
Public Function PrintLabel(LoadsTableKey As Long, FrontRear As String, IsHighRisk As Boolean, CertIsRequired As Boolean, Optional IsReprint As Boolean = False, Optional RePrntPrinterName As String, Optional NbrOfRePrntCopies As Integer = 1, Optional PrintTons As Boolean = False, Optional IsFromLBS As Boolean = False) As Boolean
    On Error GoTo Error
    Dim lErrMsg As String
    
    Dim PrinterName As String
    
    Dim CurrentLine As Integer
    
    Dim IsFront As Boolean
    Dim lblData As LabelData
    Dim NbrCopies As Integer
    
    'Make sure the Front rear flag has a valid value
    Select Case Trim(UCase(FrontRear))
        Case "FRONT"
            IsFront = True
        Case Else
            IsFront = False
    End Select
    
    'Lookup Label Data in table
    lblData = GetLabelData(LoadsTableKey, IsFront, IsFromLBS)
    
    If PrintTons = False Then
        lblData.TonsShipped = Empty
    End If
    
    gbLastActivityTime = Date + Time
    
    
    If IsReprint = False Then
'    If IsReprint = False Or Trim(PrinterName) = Empty Then
        'Get the number of copies
        NbrCopies = 1
        
        If gbPrintExtraLblCert = True And CertIsRequired = True Then
            NbrCopies = NbrCopies + 1
        End If
        
        If gbPrintExtraLblRisk = True And IsHighRisk = True Then
            NbrCopies = NbrCopies + 1
        End If
        
        If gbPrintExtraLblWY = True Then
            If Trim(UCase(lblData.ProductClass)) = "EMULSION" Then
                If Right(Trim(UCase(lblData.Product)), 2) = "WY" Then
                    NbrCopies = NbrCopies + 1
                End If
            End If
        End If
        
        'Identify Printer
        PrinterName = gbLablePrinter
    Else
        'Get the number of copies
        NbrCopies = NbrOfRePrntCopies
        
        'Identify Printer
        PrinterName = RePrntPrinterName
    End If
    
    If PrintLabelData(lblData, PrinterName, NbrCopies) = False Then
        'Error Printing
        PrintLabel = False
    Else
        'Successful Print
        PrintLabel = True
    End If
    
   
    CurrentLine = 40
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMethods" & ".PrintLabel" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    InsErrorLog 0, Date + Time, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function

Public Function PrintLabelData(lblData As LabelData, Optional PrinterName As String, Optional NbrOfRePrntCopies As Integer = 1) As Boolean
    On Error GoTo Error
    Dim lErrMsg As String
    
    Dim prt As Printer
    Dim sDriverName As String
    Dim sDeviceName As String
    Dim sPort As String
    
    gbLastActivityTime = Date + Time
    
    PrintLabelData = False
    
    Set crxApp = New CRAXDRt.Application
    
    Set crxRpt = crxApp.OpenReport(App.Path & "\Reports\SampleLabel.rpt")
    
    crxRpt.ParameterFields.GetItemByName("Product").AddCurrentValue lblData.Product
    crxRpt.ParameterFields.GetItemByName("Additive").AddCurrentValue lblData.Additive
    crxRpt.ParameterFields.GetItemByName("Date").AddCurrentValue lblData.ScaleInDate
    crxRpt.ParameterFields.GetItemByName("BLNbr").AddCurrentValue lblData.BLNbr
    crxRpt.ParameterFields.GetItemByName("Tank").AddCurrentValue lblData.Tank
    crxRpt.ParameterFields.GetItemByName("TonsShipped").AddCurrentValue lblData.TonsShipped
    crxRpt.ParameterFields.GetItemByName("Customer").AddCurrentValue lblData.Customer
    crxRpt.ParameterFields.GetItemByName("ProjectNbr").AddCurrentValue lblData.ProjectNbr
    crxRpt.ParameterFields.GetItemByName("TruckNbr").AddCurrentValue lblData.TruckNbr
    crxRpt.ParameterFields.GetItemByName("Company").AddCurrentValue lblData.Company
    crxRpt.ParameterFields.GetItemByName("Plant").AddCurrentValue lblData.Plant
    crxRpt.ParameterFields.GetItemByName("ProductClass").AddCurrentValue lblData.ProductClass
    
    'RKL DEJ START
    If Trim(lblData.TonsCaption) = Empty Then
        lblData.TonsCaption = "Tons Shipped"
    End If
    
    crxRpt.ParameterFields.GetItemByName("TonsCaption").AddCurrentValue lblData.TonsCaption 'RKL DEJ Added Tons Captioin
    'RKL DEJ STOP
    
    
    GetPrinter PrinterName, sDriverName, sDeviceName, sPort, gbLablePrinter
    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
    
    If Mid(Command$, 1, 7) = "Testing" Then
        'Display the Lable - Print Job
        Call ShowCRViewer
    Else
        crxRpt.PrintOut False, NbrOfRePrntCopies
    End If
        
    PrintLabelData = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMethods" & ".PrintLabelData" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf
    
    InsErrorLog 0, Date + Time, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function
'*****************************************************************************************************************************
'RKL DEJ 9/25/13 (STOP) New Method PrintLabel()
'*****************************************************************************************************************************


Public Sub ShowCRViewer()
    On Error GoTo Error
    
    Dim ErrMsg As String
    
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
    frmReportViewer.CRViewer.ReportSource = crxRpt
    CurrentLine = 1
    frmReportViewer.CRViewer.EnableGroupTree = False
    CurrentLine = 2
    'frmReportViewer.CRViewer.Zoom (100)
    frmReportViewer.CRViewer.ViewReport
    CurrentLine = 3
    DoEvents
    frmReportViewer.Show vbModal
    CurrentLine = 4
    Set crxRpt = Nothing
    CurrentLine = 5
    Set crxApp = Nothing
    CurrentLine = 6
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
'    ErrMsg = Me.Name & ".ShowCRViewer()" & vbCrLf

    ErrMsg = "basMethods" & ".ShowCRViewer()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    MsgBox ErrMsg, vbCritical, "Error"
    
    ErrMsg = Empty
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub


Public Function PrintCOA(Optional IsReprint As Boolean = False, Optional lcTank As String = Empty, Optional lcProductID As String = Empty, Optional lcBOLNbr As String = Empty, Optional RePrntNbrOfCopies As Integer = 1, Optional PrinterName As String) As Boolean
    On Error GoTo Error
    
'RKL DEJ 9/24/13
'Made the method public and added the optional paramters for reprinting
'Optional IsReprint As Boolean = False, Optional lcTank As String = Empty, Optional lcProductID As String = Empty, Optional lcBOLNbr As String = Empty, RePrntNbrOfCopies
'Also added the logic to allow reprinting
    
    Dim prt As Printer
    Dim sDriverName As String
    Dim sDeviceName As String
    Dim sPort As String
    Dim i As Integer
    Dim CurrentLine As Integer
    Dim lErrMsg As String
    Dim LotNumber As String
    Dim PrintAnyWay As Variant
    
    Dim RptFileName As String
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 1000
    
    If IsReprint = False Then
        'Check to see if the Lot is Active and the COA is ready to print
'        LotNumber = Trim(GetLotNumber(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value)))
        LotNumber = Trim(GetLotNumber(Trim(lcTank), Trim(lcProductID)))
    
        CurrentLine = 1001
        
        If LotNumber = Empty Then
            'Not active so don't print.
            CurrentLine = 1002
            Exit Function
        End If
        
        CurrentLine = 1003
        
'        RptFileName = Trim(GetCOARptFile(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value)))
        RptFileName = Trim(GetCOARptFile(Trim(lcTank), Trim(lcProductID)))
    Else
        RptFileName = Trim(GetCOARptFile(Trim(lcTank), Trim(lcProductID)))
    End If
    
    CurrentLine = 1004
    
    If Trim(RptFileName) = Empty Then
        CurrentLine = 1005
        'Default to this so no crash.
        RptFileName = "COA For Valero.rpt"
    End If
    
    CurrentLine = 1006
    
'    Me.MousePointer = vbHourglass
    CurrentLine = 1007
    Set crxApp = New CRAXDRt.Application
    CurrentLine = 1008
    Set crxRpt = crxApp.OpenReport(App.Path & "\Reports\" & RptFileName)
    
    CurrentLine = 1009
    'Obtain the Scale Pass Printer
    
    'RKL DEJ 9/25/13 added reprint option
    If IsReprint = False Or Trim(PrinterName) = Empty Then
        sDeviceName = gbCOAPrinter
    Else
        sDeviceName = PrinterName
    End If
    
    CurrentLine = 1010
    For Each prt In Printers
        CurrentLine = 1011
        If prt.DeviceName = sDeviceName Then
            CurrentLine = 1012
            Set Printer = prt
            CurrentLine = 1013
            sDriverName = prt.DriverName
            CurrentLine = 1014
            sPort = prt.Port
            CurrentLine = 1015
            Exit For
        End If
        CurrentLine = 1016
    Next
    
    CurrentLine = 1017
    'Set the Scale Pass Printer
    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
    
    CurrentLine = 1018
    If UpdateRptDBRefNew(crxRpt) = True Then
'    If UpdateRptDBRef = True Then  'RKL DEJ 2017-04-10 Replaced with UpdateRptDBRefNew
        CurrentLine = 1019
        Debug.Print "Update DB success"
    Else
        CurrentLine = 1020
        Debug.Print "Update DB Failure"
        MsgBox "Could not print the COA report.", vbExclamation, "Scale Pass"
        gbLastActivityTime = Date + Time
        Exit Function
    End If
    
    CurrentLine = 1021
    If UpdateSubRptDBRefNew(crxRpt) = True Then
'    If UpdateSubRptDBRef = True Then 'RKL DEJ 2017-04-10 Replaced with UpdateSubRptDBRefNew
        CurrentLine = 1022
        Debug.Print "Update sub DB success"
    Else
        CurrentLine = 1023
        Debug.Print "Update sub DB Failure"
        MsgBox "Could not print the COA report.", vbExclamation, "Scale Pass"
        gbLastActivityTime = Date + Time
        Exit Function
    End If
    
    'Verify the database
    CurrentLine = 10221
    crxRpt.Database.Verify
    
    CurrentLine = 1024
    If IsReprint = False Then
'        crxRpt.ParameterFields.GetItemByName("BOLNo").AddCurrentValue ConvertToString(RSBOL("BOLNBR").Value)
        crxRpt.ParameterFields.GetItemByName("BOLNo").AddCurrentValue ConvertToString(lcBOLNbr)
    Else
        crxRpt.ParameterFields.GetItemByName("BOLNo").AddCurrentValue ConvertToString(lcBOLNbr)
    End If
    
    CurrentLine = 1025
    If Mid(Command$, 1, 7) = "Testing" Then
        CurrentLine = 1026
        Call ShowCRViewer
        CurrentLine = 1027
    Else
        CurrentLine = 1028
        'Print the hard copy
        If IsReprint = False Then
            'RKL DEJ 9/25/13 - changed from hard copy of 1 to user configurable 'gbNbrOfCOACopies'
'            crxRpt.PrintOut False, 1
            crxRpt.PrintOut False, gbNbrOfCOACopies
            
            'RKL DEJ 9/25/13 - Added print for extra copies
            If gbPrintExtraCOA = True Then
                If UCase(Trim(gbExtraCOAPrinter)) <> UCase(Trim(gbCOAPrinter)) Then
                    GetPrinter gbExtraCOAPrinter, sDriverName, sDeviceName, sPort, gbCOAPrinter
                    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
                End If
                
                crxRpt.PrintOut False, 1
            End If
        Else
            crxRpt.PrintOut False, RePrntNbrOfCopies
        End If
        CurrentLine = 1029
    End If
    
    CurrentLine = 1030
'    Me.MousePointer = vbDefault
   
    CurrentLine = 1031
    
    PrintCOA = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
'    Me.MousePointer = vbNormal
    
'    lErrMsg = Me.Name & ".PrintCOA" & vbCrLf

    lErrMsg = "basMethods" & ".PrintCOA" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
'    frmBOLPreview.InsErrorLog 0, Date + Time, lErrMsg
    InsErrorLog 0, Date + Time, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function


Public Function PrintCypressCOA(Optional IsReprint As Boolean = False, Optional lcTank As String = Empty, Optional lcProductID As String = Empty, Optional lcBOLNbr As String = Empty) As Boolean
    On Error GoTo Error
'RKL DEJ 9/24/13
'Made the method public and added the optional paramters for reprinting
'Optional IsReprint As Boolean = False, Optional lcTank As String = Empty, Optional lcProductID As String = Empty, Optional lcBOLNbr As String = Empty
'Also added the logic to allow reprinting
    
    Dim prt As Printer
    Dim sDriverName As String
    Dim sDeviceName As String
    Dim sPort As String
    Dim i As Integer
    Dim CurrentLine As Integer
    Dim lErrMsg As String
    Dim LotNumber As String
    Dim PrintAnyWay As Variant
    
    Dim RptFileName As String
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = -100

    If IsReprint = False Then
        'Check to see if the Lot is Active and the COA is ready to print
'        LotNumber = Trim(GetLotNumber(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value)))
        LotNumber = Trim(GetLotNumber(Trim(lcTank), Trim(lcProductID)))
    
        CurrentLine = -90
        If LotNumber = Empty Then
            'Not active so don't print.
            CurrentLine = -80
            Exit Function
        End If
        
        CurrentLine = -1
        
'        RptFileName = Trim(GetCOARptFile(Trim(RSBOL("TankNumber").Value), Trim(RSBOL("ProductID").Value)))
        RptFileName = Trim(GetCOARptFile(Trim(lcTank), Trim(lcProductID)))
    Else
        RptFileName = Trim(GetCOARptFile(Trim(lcTank), Trim(lcProductID)))
    End If
    
    If Trim(RptFileName) = Empty Then
        'Default to this so no crash.
        RptFileName = "COA For Valero.rpt"
    End If
    
    CurrentLine = 0
    
'    Me.MousePointer = vbHourglass
    CurrentLine = 1
    Set crxApp = New CRAXDRt.Application
    CurrentLine = 2
    Set crxRpt = crxApp.OpenReport(App.Path & "\Reports\" & RptFileName)
    CurrentLine = 3
    CurrentLine = 4
    sDeviceName = gbBOLCypressPRINTER
    
    CurrentLine = 5
    'Obtain the Printer
    For Each prt In Printers
        CurrentLine = 6
        If prt.DeviceName = sDeviceName Then
            CurrentLine = 7
            Set Printer = prt
            CurrentLine = 8
            sDriverName = prt.DriverName
            CurrentLine = 9
            sPort = prt.Port
            CurrentLine = 10
            Exit For
        End If
        CurrentLine = 11
    Next
    
    CurrentLine = 12
    'Set the Report Printer for Cypress
    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
    
    CurrentLine = 1201
    If UpdateRptDBRefNew(crxRpt) = True Then
'    If UpdateRptDBRef = True Then   'RKL DEJ 2017-04-10 Replaced with UpdateRptDBRefNew
        CurrentLine = 1201
        Debug.Print "Update DB success"
    Else
    CurrentLine = 1203
        Debug.Print "Update DB Failure"
        MsgBox "Could not print the COA report to Cypress.", vbExclamation, "Scale Pass"
        gbLastActivityTime = Date + Time
        Exit Function
    End If
    
    CurrentLine = 1204
    If UpdateSubRptDBRefNew(crxRpt) = True Then
'    If UpdateSubRptDBRef = True Then    'RKL DEJ 2017-04-10 Replaced with UpdateSubRptDBRefNew
        CurrentLine = 1205
        Debug.Print "Update sub DB success"
    Else
        CurrentLine = 1206
        Debug.Print "Update sub DB Failure"
        MsgBox "Could not print the COA report to Cypress.", vbExclamation, "Scale Pass"
        gbLastActivityTime = Date + Time
        Exit Function
    End If
    
    'Verify the database
    CurrentLine = 12051
    crxRpt.Database.Verify
    
    CurrentLine = 1207
    If IsReprint = False Then
'        crxRpt.ParameterFields.GetItemByName("BOLNo").AddCurrentValue ConvertToString(RSBOL("BOLNBR").Value)
        crxRpt.ParameterFields.GetItemByName("BOLNo").AddCurrentValue ConvertToString(lcBOLNbr)
    Else
        crxRpt.ParameterFields.GetItemByName("BOLNo").AddCurrentValue ConvertToString(lcBOLNbr)
    End If
    
    CurrentLine = 13
    If Mid(Command$, 1, 7) = "Testing" Then
        CurrentLine = 15
        Call ShowCRViewer
        CurrentLine = 16
    Else
        CurrentLine = 20
        crxRpt.PrintOut False, 1
        CurrentLine = 21
    End If
        
    CurrentLine = 39
'    Me.MousePointer = vbDefault
   
    CurrentLine = 40
    
    PrintCypressCOA = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
'    Me.MousePointer = vbNormal
    
    lErrMsg = "basMethods" & ".PrintCypressCOA" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
'    frmBOLPreview.InsErrorLog 0, Date + Time, lErrMsg
    InsErrorLog 0, Date + Time, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function



'RKL DEJ 9/25/13 Added optional perameters for reprint
Public Sub PrintCrystalBOL(Optional IsReprint As Boolean = False, Optional NbrOfRePrntCopies As Integer = 1, Optional BOLTableKey As Long, Optional PrinterName As String, Optional Notes As String, Optional bAllCopies As Boolean = False)
'Public Sub PrintCrystalBOL(Optional IsReprint As Boolean = False, Optional NbrOfRePrntCopies As Integer = 1, Optional BOLTableKey As Long, Optional PrinterName As String, Optional Notes As String, Optional bAllCopies As Boolean = False, Optional CertIsRequired As Boolean = False)
    Dim prt As Printer
    Dim sDriverName As String
    Dim sDeviceName As String
    Dim sPort As String
    Dim i As Integer
    Dim lErrMsg As String
    On Error GoTo Error
    
    Dim CurrentLine As Integer
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
'    Me.MousePointer = vbHourglass
    CurrentLine = 1
    Set crxApp = New CRAXDRt.Application
    CurrentLine = 2
    Set crxRpt = crxApp.OpenReport(App.Path & "\Reports\BOL.rpt")
    CurrentLine = 3
    
    'RKL DEJ 9/25/13 Added option for reprint
    If IsReprint = False Then
'        crxRpt.RecordSelectionFormula = "{BOLPrinting.BOLTableKey} = " & RSBOL("BOLTableKey").Value
        crxRpt.RecordSelectionFormula = "{BOLPrinting.BOLTableKey} = " & BOLTableKey
    Else
        crxRpt.RecordSelectionFormula = "{BOLPrinting.BOLTableKey} = " & BOLTableKey
    End If
    
    CurrentLine = 4
    
    'RKL DEJ 9/25/13 Added option for reprint
    If IsReprint = False Or Trim(PrinterName) = Empty Then
        sDeviceName = gbBLPRINTER
    Else
        sDeviceName = PrinterName
    End If
       
    CurrentLine = 5
    For Each prt In Printers
        CurrentLine = 6
        If prt.DeviceName = sDeviceName Then
            CurrentLine = 7
            Set Printer = prt
            CurrentLine = 8
            sDriverName = prt.DriverName
            CurrentLine = 9
            sPort = prt.Port
            CurrentLine = 10
            Exit For
        End If
        CurrentLine = 11
    Next
    CurrentLine = 12
    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
    
    CurrentLine = 13
    If Mid(Command$, 1, 7) = "Testing" Then
        CurrentLine = 14
        crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue "For Testing Purposes Only"
        CurrentLine = 15
        Call ShowCRViewer
        CurrentLine = 16
    Else
        CurrentLine = 17
        
        'RKL DEJ 9/25/13 - Added reprint options
        If IsReprint = False Or (IsReprint = True And bAllCopies = True) Then
        
            'RKL DEJ 2016-04-29 (START)
            Dim sSQL As String
            Dim BOLRS As New ADODB.Recordset
            Dim strBOLCaption As String
            
            sSQL = "Select * From BOLPrintingSetup Order by [CopyNbr] Asc "
            
            BOLRS.CursorLocation = adUseClient
            BOLRS.CursorType = adOpenDynamic
            
            BOLRS.Open sSQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
            
            BOLRS.Sort = "[CopyNbr] Asc"
            
            If BOLRS.RecordCount > 0 Then
                CurrentLine = 19
                BOLRS.MoveFirst
                CurrentLine = 20
                While Not BOLRS.EOF
                    CurrentLine = 21
                    If IsNull(BOLRS("PrintCaption").Value) = True Then
                        CurrentLine = 22
                        strBOLCaption = ""
                    Else
                        CurrentLine = 23
                        strBOLCaption = UCase(BOLRS("PrintCaption").Value)
                    End If
                    CurrentLine = 24
                    
                    crxRpt.ParameterFields.GetItemByName("CopyNote").ClearCurrentValueAndRange
                    crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue strBOLCaption
                    
                    CurrentLine = 25
                    crxRpt.PrintOut False, 1
                    CurrentLine = 26
                    
                    BOLRS.MoveNext
                Wend
            End If
            
            If BOLRS.State = 1 Then
                BOLRS.Close
            End If
            Set BOLRS = Nothing
            'RKL DEJ 2016-04-29 (STOP)

'RKL DEJ 2016-04-29 (START) Comment Orig Code
'            For i = 1 To 6
'                CurrentLine = 18
'                Select Case i
'                       Case 1
'                            CurrentLine = 19
'                              crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("Idaho Falls Office Copy")
'                            CurrentLine = 20
'                              crxRpt.PrintOut False, 1
'                            CurrentLine = 21
'                       Case 2
'                            CurrentLine = 22
'                              crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("Customer Copy")
'                            CurrentLine = 23
'                              crxRpt.PrintOut False, 1
'                            CurrentLine = 24
'                       Case 3
'                            CurrentLine = 25
'                              crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("Project Engineer Copy")
'                            CurrentLine = 26
'                              crxRpt.PrintOut False, 1
'                            CurrentLine = 27
'                       Case 4
'                            CurrentLine = 28
'                              crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("State Copy")
'                            CurrentLine = 29
'                              crxRpt.PrintOut False, 1
'                            CurrentLine = 30
'                       Case 5
'                            CurrentLine = 31
'                              crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("Transporter Copy")
'                            CurrentLine = 32
'                              crxRpt.PrintOut False, 1
'                            CurrentLine = 33
'                       Case 6
'                            CurrentLine = 34
'                              crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("Plant Copy")
'                            CurrentLine = 35
'                              crxRpt.PrintOut False, 1
'                            CurrentLine = 36
'                End Select
'                CurrentLine = 37
'            Next
'RKL DEJ 2016-04-29 (STOP) Comment Orig Code

            CurrentLine = 38
            
''RKL DEJ 9/25/13 (START)
'            If gbPrintExtraBOL = True Then
'                CurrentLine = 3000
'
'                If gbPrintExtraBOLCOAOnly = False Or (gbPrintExtraBOLCOAOnly = True And CertIsRequired = True) Then
'                    If UCase(Trim(gbExtraBOLPrinter)) <> UCase(Trim(gbBLPRINTER)) Then
'                    CurrentLine = 3010
'                        GetPrinter gbExtraBOLPrinter, sDriverName, sDeviceName, sPort, gbBLPRINTER
'                    CurrentLine = 3020
'                        crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
'                    End If
'
'                    CurrentLine = 3030
'                    crxRpt.ParameterFields.GetItemByName("CopyNote").ClearCurrentValueAndRange
'
'                    CurrentLine = 3040
'                    crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("Plant Copy")
'
'                    CurrentLine = 14000
'                    'Print the Extra Copy
'                    crxRpt.PrintOut False, 1
'                    CurrentLine = 15000
'                End If
'            End If
''RKL DEJ 9/25/13 (STOP)
            
        
        Else
            CurrentLine = 34
            crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase(Notes)
            CurrentLine = 35
            crxRpt.PrintOut False, NbrOfRePrntCopies
            CurrentLine = 36
        End If
    End If
        
    CurrentLine = 39
'    Me.MousePointer = vbArrow
   
    CurrentLine = 40
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
'    Me.MousePointer = vbNormal
    
    lErrMsg = "basMethods" & ".PrintCrystalBOL" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
'    frmBOLPreview.InsErrorLog 0, Date + Time, lErrMsg
    InsErrorLog 0, Date + Time, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub



Public Sub PrintCypressBOL(Optional IsReprint As Boolean = False, Optional BOLTableKey As Long, Optional Notes As String)
    On Error GoTo Error
    
    Dim prt As Printer
    Dim sDriverName As String
    Dim sDeviceName As String
    Dim sPort As String
    Dim i As Integer
    Dim CurrentLine As Integer
    Dim lErrMsg As String
    
    gbLastActivityTime = Date + Time
    
    CurrentLine = 0
    
'    Me.MousePointer = vbHourglass
    CurrentLine = 1
    Set crxApp = New CRAXDRt.Application
    CurrentLine = 2
    Set crxRpt = crxApp.OpenReport(App.Path & "\Reports\BOL.rpt")
    CurrentLine = 3
    'RKL DEJ 9/25/13 added optional reprint
    If IsReprint = False Then
'        crxRpt.RecordSelectionFormula = "{BOLPrinting.BOLTableKey} = " & RSBOL("BOLTableKey").Value
        crxRpt.RecordSelectionFormula = "{BOLPrinting.BOLTableKey} = " & BOLTableKey
    Else
        crxRpt.RecordSelectionFormula = "{BOLPrinting.BOLTableKey} = " & BOLTableKey
    End If
    CurrentLine = 4
    sDeviceName = gbBOLCypressPRINTER
       
    CurrentLine = 5
    'Obtain the Printer
    For Each prt In Printers
        CurrentLine = 6
        If prt.DeviceName = sDeviceName Then
            CurrentLine = 7
            Set Printer = prt
            CurrentLine = 8
            sDriverName = prt.DriverName
            CurrentLine = 9
            sPort = prt.Port
            CurrentLine = 10
            Exit For
        End If
        CurrentLine = 11
    Next
    
    CurrentLine = 12
    'Set the Report Printer
    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
    
    CurrentLine = 13
    If Mid(Command$, 1, 7) = "Testing" Then
        CurrentLine = 14
        crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue "Cypress Testing Purposes Only"
        CurrentLine = 15
        Call ShowCRViewer
        CurrentLine = 16
    Else
        CurrentLine = 17
        'RKL DEJ 9/25/13 added optional reprint
        If IsReprint = False Then
            crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase("Idaho Falls Office Copy")
        Else
            crxRpt.ParameterFields.GetItemByName("CopyNote").AddCurrentValue UCase(Notes)
        End If
        CurrentLine = 20
        
        crxRpt.PrintOut False, 1
        
        CurrentLine = 21
    End If
        
    CurrentLine = 39
'    Me.MousePointer = vbDefault
   
    CurrentLine = 40
    
    gbLastActivityTime = Date + Time
    Exit Sub
Error:
'    Me.MousePointer = vbNormal
    
    lErrMsg = "basMethods" & ".PrintCypressBOL" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
'    frmBOLPreview.InsErrorLog 0, Date + Time, lErrMsg
    InsErrorLog 0, Date + Time, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Sub



Public Function InsErrorLog(BOLTableKey As Long, ErrorDate As Date, ErrorMessage As String) As Boolean
    On Error GoTo Error
    
    Dim RS As New ADODB.Recordset
    Dim SQL As String
    Dim lErrMsg As String
    Dim BOLPrintingErrorKey As Long
    
    
    gbLastActivityTime = Date + Time
    
    If gbScaleConn.State <> 1 Then Exit Function
    
    SQL = "Select * From BOLPrintingErrors where 1=2 "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenDynamic
    
    RS.Open SQL, gbScaleConn, adOpenDynamic, adLockBatchOptimistic
    
    RS.AddNew
    RS("BOLTableKey").Value = BOLTableKey
    RS("ErrorDate").Value = ErrorDate
    RS("ErrorMessage").Value = ErrorMessage
    
    RS.UpdateBatch
'    UpdateRS RS
    
    If Not IsNull(RS("BOLPrintingErrorKey").Value) Then
        BOLPrintingErrorKey = RS("BOLPrintingErrorKey").Value
    Else
        BOLPrintingErrorKey = -1
    End If
    
    InsMASErrorLog BOLPrintingErrorKey, BOLTableKey, ErrorDate, ErrorMessage
    
    InsErrorLog = True
    
CleanUP:
    On Error Resume Next
    If RS.State = 1 Then RS.Close
    Set RS = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMethods" & ".InsErrorLog(BOLTableKey As Long, ErrorDate As Date, ErrorMessage As String) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    lErrMsg = lErrMsg & vbCrLf & vbCrLf & _
    "BOLTableKey = " & BOLTableKey & vbCrLf & _
    "ErrorDate = " & ErrorDate & vbCrLf & _
    "ErrorMessage = " & ErrorMessage & vbCrLf
    
    basMain.WriteToLogFile lErrMsg
    
    MsgBox lErrMsg, vbCritical, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function


'RKL DEJ New Method to select printer
Function GetPrinter(ByVal PrinterName As String, ByRef sDriverName As String, ByRef sDeviceName As String, ByRef sPort As String, Optional ByVal AlternatePrinterName As String = Empty) As Boolean
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time
    
    Dim lErrMsg As String
    
    Dim prt As Printer
    Dim bUseAlternate As Boolean
    Dim CurrentLine As Long
    
    bUseAlternate = True 'Assume we cannot find the printer and need to use the alterante
    
    CurrentLine = 1010
    For Each prt In Printers
        CurrentLine = 1011
        If UCase(Trim(prt.DeviceName)) = UCase(Trim(PrinterName)) Then
            sDeviceName = prt.DeviceName
            CurrentLine = 1012
            Set Printer = prt
            CurrentLine = 1013
            sDriverName = prt.DriverName
            CurrentLine = 1014
            sPort = prt.Port
            CurrentLine = 1015
            
            bUseAlternate = False   'We found the printer so don't use the alternate
            GetPrinter = True
            
            Exit For
        End If
        CurrentLine = 1016
    Next
    
    'If printer was not found look for alternate
    If bUseAlternate = True And Trim(AlternatePrinterName) <> Empty Then
        CurrentLine = 1010
        For Each prt In Printers
            CurrentLine = 1011
            If UCase(Trim(prt.DeviceName)) = UCase(Trim(AlternatePrinterName)) Then
                sDeviceName = prt.DeviceName
                CurrentLine = 1012
                Set Printer = prt
                CurrentLine = 1013
                sDriverName = prt.DriverName
                CurrentLine = 1014
                sPort = prt.Port
                CurrentLine = 1015
                
                GetPrinter = True
                
                Exit For
            End If
            CurrentLine = 1016
        Next
    
    End If

CleanUP:
    On Error Resume Next
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMethods" & ".GetPrinter(PrinterName As String, Optional AlternatePrinterName As String = Empty) As Boolean" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    lErrMsg = lErrMsg & vbCrLf & vbCrLf & _
    "PrinterName = " & PrinterName & vbCrLf & _
    "AlternatePrinterName = " & AlternatePrinterName
    
    basMain.WriteToLogFile lErrMsg
    
    MsgBox lErrMsg, vbCritical, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function

Public Function GetMASItemClassID(Optional ItemID As String, Optional ItemKey As Long) As String
    On Error GoTo Error
'Global gbMASWhseKey As Long
'Global gbMASCompanyID As String
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    
    gbLastActivityTime = Date + Time
    
    'Set Default
    GetMASItemClassID = Empty
    
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select i.ItemKey, i.ItemID, ic.* "
    SQL = SQL & "From timItem i with(NoLock) "
    SQL = SQL & "Inner Join timItemClass ic with(NoLock) "
    SQL = SQL & "On i.ItemClassKey = ic.ItemClassKey "
    SQL = SQL & "Where i.CompanyID = '" & gbMASCompanyID & "' "
    
    If ItemKey > 0 Then
        SQL = SQL & "And ItemKey = " & ItemKey & " "
    Else
        SQL = SQL & "And ItemID = '" & Trim(ItemID) & "' "
    End If
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        
    Else
        RS.MoveFirst
        'Records found
        GetMASItemClassID = RS("ItemClassID").Value
        
    End If
    
    gbLastActivityTime = Date + Time
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetMASItemClassID()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function



Public Function GetLabelData(LoadsTableKey As Long, Optional IsFront As Boolean = True, Optional IsFromLBS As Boolean = False) As LabelData
    On Error GoTo Error
'Global gbMASWhseKey As Long
'Global gbMASCompanyID As String
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    
    Dim Product As String
    Dim ScaleInDate As String
    Dim BLNbr As String
    Dim Tank As String
    Dim TonsShipped As String
    Dim Customer As String
    Dim ProjectNbr As String
    Dim TruckNbr As String
    Dim Company As String
    Dim Plant As String
    Dim ProductClass As String
    
    Dim lblData As LabelData
    
    gbLastActivityTime = Date + Time
    
    'Default the current date as the label Date
    lblData.ScaleInDate = Date
    
    'Get the Company Name
    'RKL DEJ 2017-03-20 changed to lookup companyname in MAS 500
    lblData.Company = "Valero"  'GetMASCompanyName(gbMASCompanyID)
'    Select Case Trim(UCase(gbMASCompanyID))
'        Case "PEA"
'            lblData.Company = "PEAK ASPHALT, LLC"
'        Case "IDA"
'            lblData.Company = "IDAHO ASPHALT SUPPLY, INC"
'        Case "WEI"
'            lblData.Company = "WESTERN EMULSIONS, INC."
'        Case "WEC"
'            lblData.Company = "WESTERN EMULSIONS COOLIDGE"
'        Case Else
'            lblData.Company = "UNKNOWN COMPANY " & Trim(UCase(gbMASCompanyID))
'    End Select
    
    'Get the Plant Name
    'RKL DEJ 2017-03-20 changed to lookup companyname in MAS 500
    lblData.Plant = GetMASFacilityName()
'    Select Case ConvertToDouble(gbPlantPrefix)
'        Case 1
'            lblData.Plant = "UNKNOWN 1"
'        Case 2
'            lblData.Plant = "UNKNOWN 2"
'        Case 3
'            lblData.Plant = "NAMPA ASPHALT"
'        Case 4
'            lblData.Plant = "HAUSER ASPHALT"
'        Case 5
'            lblData.Plant = "BLACKFOOT ASPHALT"
'        Case 6
'            lblData.Plant = "WOODS CROSS ASPHALT"
'        Case 7
'            lblData.Plant = "RAWLINS ASPHALT"
'        Case Else
'            lblData.Plant = "UNKNOWN PLANT " & gbPlantPrefix
'    End Select
    
    
    
    If IsObject(gbScaleConn) <> True Then
        Exit Function
    End If
    
    If gbScaleConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select * From Loads where LoadsTableKey = " & LoadsTableKey
        
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbScaleConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        
    Else
        'Records found
        RS.MoveFirst
        
        If IsFront = True Then
            lblData.ProductClass = Trim(UCase(GetMASItemClassID(ConvertToString(RS("FrontProductID").Value))))
            
            lblData.BLNbr = ConvertToString(RS("FrontBOL").Value)
'            lblData.Company = ConvertToString(RS("FieldName").Value)
            lblData.Customer = ConvertToString(RS("FrontCustomer").Value)
'            lblData.Plant = ConvertToString(RS("FieldName").Value)
'            lblData.Product = ConvertToString(RS("FrontProductID").Value)
            lblData.Product = ConvertToString(RS("FrontProduct").Value)
'            lblData.Additive = ConvertToString(RS("FrontAdditiveID").Value)
            lblData.Additive = ConvertToString(RS("FrontAdditive").Value)
            
            lblData.ProductClass = GetLabelProductClass(ConvertToString(RS("FrontProductClass").Value))
            
'            lblData.ProjectNbr = ConvertToString(RS("FrontProject").Value)
            lblData.ProjectNbr = GetMASProjectNumber(ConvertToString(RS("FrontMcLeodOrderNumber").Value))
            
            lblData.ScaleInDate = ConvertToDate(RS("TimeIn").Value)
            lblData.Tank = ConvertToString(RS("FrontTankNbr").Value)
            
            'RKL DEJ START
'            lblData.TonsShipped = ConvertToString(RS("FrontTons").Value)
            If IsFromLBS = True Then
                lblData.TonsCaption = "Tons Anticipated"
                lblData.TonsShipped = ConvertToString(RS("FrontReqTons").Value)
            Else
                lblData.TonsCaption = "Tons Shippped"
                lblData.TonsShipped = ConvertToString(RS("FrontTons").Value)
            End If
            'RKL DEJ STOP
            
            lblData.TruckNbr = ConvertToString(RS("TruckNo").Value)
            
            If ConvertToDouble(RS("SplitLoad").Value) = 0 Then
                'This is not a split load so add front and back
                
                'RKL DEJ START
                If IsFromLBS = True Then
                    'There will not be a rear anticipated/Req tons when is it not a split load.
'                    lblData.TonsShipped = ConvertToString(ConvertToDouble(lblData.TonsShipped) + ConvertToDouble(RS("RearReqTons").Value))
                Else
                    lblData.TonsShipped = ConvertToString(ConvertToDouble(lblData.TonsShipped) + ConvertToDouble(RS("RearTons").Value))
                End If
                
'                lblData.TonsShipped = ConvertToString(ConvertToDouble(lblData.TonsShipped) + ConvertToDouble(RS("RearTons").Value))
                'RKL DEJ STOP
            End If
        
            
        Else
            lblData.ProductClass = Trim(UCase(GetMASItemClassID(ConvertToString(RS("RearProductID").Value))))
            
            lblData.BLNbr = ConvertToString(RS("RearBOL").Value)
'            lblData.Company = ConvertToString(RS("FieldName").Value)
            lblData.Customer = ConvertToString(RS("RearCustomer").Value)
'            lblData.Plant = ConvertToString(RS("FieldName").Value)
'            lblData.Product = ConvertToString(RS("RearProductID").Value)
            lblData.Product = ConvertToString(RS("RearProduct").Value)
'            lblData.Additive = ConvertToString(RS("RearAdditiveID").Value)
            lblData.Additive = ConvertToString(RS("RearAdditive").Value)
            
            lblData.ProductClass = GetLabelProductClass(ConvertToString(RS("RearProductClass").Value))
            
'            lblData.ProjectNbr = ConvertToString(RS("RearProject").Value)
            lblData.ProjectNbr = GetMASProjectNumber(ConvertToString(RS("RearMcLeodOrderNumber").Value))
            
            lblData.ScaleInDate = ConvertToDate(RS("TimeIn").Value)
            lblData.Tank = ConvertToString(RS("RearTankNbr").Value)
            
            'RKL DEJ START
'            lblData.TonsShipped = ConvertToString(RS("RearTons").Value)
            If IsFromLBS = True Then
                lblData.TonsCaption = "Tons Anticipated"
                lblData.TonsShipped = ConvertToString(RS("RearReqTons").Value)
            Else
                lblData.TonsCaption = "Tons Shippped"
                lblData.TonsShipped = ConvertToString(RS("RearTons").Value)
            End If
            'RKL DEJ STOP
            
            lblData.TruckNbr = ConvertToString(RS("TruckNo").Value)
        End If
        
    End If
    
    GetLabelData = lblData

    gbLastActivityTime = Date + Time
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetLabelData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function


Function GetLabelProductClass(ProductClass As String) As String
    gbLastActivityTime = Date + Time
    
    Select Case Trim(UCase(ProductClass))
        Case "AC"
            GetLabelProductClass = "AC"
        Case "CUTBACK"
            GetLabelProductClass = "CUTBACK"
        Case "BLENDED EMUL", "DILUTE EMULS", "EMULSION"
            GetLabelProductClass = "EMULSION"
        Case "PMA"
            GetLabelProductClass = "PMA"
        Case Else
            GetLabelProductClass = Empty
    End Select
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetLabelProductClass()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
        
End Function


Public Function GetMASProjectNumber(McLeodOrderNumber As String) As String
    On Error GoTo Error
    
    Dim RSGetBOLData As New ADODB.Recordset
    Dim SQL As String
    
    GetMASProjectNumber = Empty
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select Distinct * From vluGetBOLInfo_SGS Where loadid = " & ConvertToDouble(McLeodOrderNumber)
                    
    RSGetBOLData.CursorLocation = adUseClient
    RSGetBOLData.CursorType = adOpenDynamic
    RSGetBOLData.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
    
    If RSGetBOLData.State = 1 Then
        If RSGetBOLData.RecordCount > 0 Then
            RSGetBOLData.MoveFirst
            GetMASProjectNumber = ConvertToString(RSGetBOLData("ProjectNumber").Value)
        End If
    End If
    
CleanUP:
    If RSGetBOLData.State = 1 Then
        RSGetBOLData.Close
    End If
    
    Set RSGetBOLData = Nothing


    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetMASProjectNumber()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
        
End Function


'RKL DEJ 1/22/14 - Added Method GetMASShipToState
Public Function GetMASShipToState(TranNo As String, ItemID As String) As String
    On Error GoTo Error
    
    Dim RSGetBOLData As New ADODB.Recordset
    Dim SQL As String
    
    GetMASShipToState = Empty
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
'    SQL = "select so.CompanyID, sta.StateID ShipToStateID, dsta.StateID DfltShipToStateID, so.SOKey, so.TranNo, so.TranID, "
'    SQL = SQL & "sl.Description 'SOLineDesc', sl.ItemKey, i.ItemID, ides.ShortDesc, ides.LongDesc, "
'    SQL = SQL & "sle.Awarded "
'    SQL = SQL & "From tsoSalesOrder so with(NoLock) "
'    SQL = SQL & "Inner Join tsoSOLine sl with(NoLock) "
'    SQL = SQL & "    on so.sokey = sl.sokey "
'    SQL = SQL & "Inner Join tsoSOLineExt_SGS sle with(NoLock) "
'    SQL = SQL & "    on sl.solineKey = sle.solinekey "
'    SQL = SQL & "    and sle.Awarded = 1 "
'    SQL = SQL & "Inner Join tsoSOLineDist sld with(NOLock) "
'    SQL = SQL & "    on sl.solinekey = sld.solinekey "
'    SQL = SQL & "Left Outer Join tciAddress sta with(NoLock) "
'    SQL = SQL & "    on sld.ShipToAddrKey = sta.AddrKey "
'    SQL = SQL & "Left Outer Join tciAddress dsta with(NoLock) "
'    SQL = SQL & "    on so.DfltShipToAddrKey = dsta.AddrKey "
'    SQL = SQL & "Left Outer Join timItem i with(NoLock) "
'    SQL = SQL & "    on sl.ItemKey = i.ItemKey "
'    SQL = SQL & "Left Outer Join timItemDescription ides with(NoLock) "
'    SQL = SQL & "    on i.ItemKey = ides.ItemKey "
'    SQL = SQL & "Where 1 = 1 "
'    SQL = SQL & "and so.TranNo = '" & TranNo & "' "
'    SQL = SQL & "and so.CompanyID = '" & gbMASCompanyID & "' "
'    SQL = SQL & "and sld.whsekey = " & gbMASWhseKey & " "
'    SQL = SQL & "and so.TranType = 802   --Contract (BSO)"
'    SQL = SQL & "and i.ItemID = '" & ItemID & "' "

    SQL = "select top 1 so.CompanyID, so.SOKey, so.TranID, so.TranType, so.TranNo, sl.SOLineKey, sld.SOLineDistKey, i.ItemKey, i.ItemID, w.WhseKey, w.WhseID, fa.FacilityID, fa.FacilityLoadID, fa.City, sta.StateID ShipToStateID, dsta.StateID DfltShipToStateID "
    SQL = SQL & "From tsoSalesOrder so with(NoLock) "
    SQL = SQL & "inner join tsoSOLine sl with(NoLock) "
    SQL = SQL & "    on so.SOKey = sl.SOKey "
    SQL = SQL & "inner join tsoSOLineDist sld with(NoLock) "
    SQL = SQL & "    on sl.SOLineKey = sld.SOLineKey "
    SQL = SQL & "Inner Join timItem i with(NoLock) "
    SQL = SQL & "    on sl.ItemKey = i.ItemKey "
    SQL = SQL & "Inner Join timWarehouse w with(NoLock) "
    SQL = SQL & "    on sld.WhseKey = w.WhseKey "
    SQL = SQL & "Inner join tsosolineext_sgs slx with(NoLock) "
    SQL = SQL & "    on sl.solinekey = slx.solinekey "
    SQL = SQL & "    and slx.awarded = 1 "
    SQL = SQL & "Inner Join tsoFacilityAddress_SGS fa with(NoLock) "
    SQL = SQL & "    on so.CompanyID = fa.CompanyID "
    SQL = SQL & "    and w.WhseID = fa.FacilityLoadID "
    SQL = SQL & "Left Outer Join tciAddress sta with(NoLock) "
    SQL = SQL & "    on sld.ShipToAddrKey = sta.AddrKey "
    SQL = SQL & "Left Outer Join tciAddress dsta with(NoLock) "
    SQL = SQL & "    on so.DfltShipToAddrKey = dsta.AddrKey "
    SQL = SQL & "Where so.TranType = 802 "
    SQL = SQL & "and fa.FacilitySeqNo = " & ConvertToDouble(gbPlantPrefix) & " "
    SQL = SQL & "and fa.City = '" & gbPlant & "' "
    SQL = SQL & "and so.TranNo = '" & TranNo & "' "
    SQL = SQL & "and i.ItemID = '" & ItemID & "' "
    SQL = SQL & "and so.CompanyID = '" & gbMASCompanyID & "' "

                    
    RSGetBOLData.CursorLocation = adUseClient
    RSGetBOLData.CursorType = adOpenDynamic
    RSGetBOLData.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
    
    If RSGetBOLData.State = 1 Then
        If RSGetBOLData.RecordCount > 0 Then
            RSGetBOLData.MoveFirst
            GetMASShipToState = ConvertToString(RSGetBOLData("ShipToStateID").Value)
        End If
    End If
    
CleanUP:
    If RSGetBOLData.State = 1 Then
        RSGetBOLData.Close
    End If
    
    Set RSGetBOLData = Nothing


    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetMASShipToState()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
        
End Function


'RKL DEJ 1/22/14 - Added Method GetMASFOB
Public Function GetMASFOB(TranNo As String, ItemID As String) As String
    On Error GoTo Error
    
    Dim RSGetBOLData As New ADODB.Recordset
    Dim SQL As String
    
    GetMASFOB = Empty
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "select so.CompanyID, sld.FOBKey, f.FOBID, f.Description 'FOBDesc', so.SOKey, so.TranNo, so.TranID, "
    SQL = SQL & "sl.Description 'SOLineDesc', sl.ItemKey, i.ItemID, ides.ShortDesc, ides.LongDesc, "
    SQL = SQL & "sle.Awarded "
    SQL = SQL & "From tsoSalesOrder so with(NoLock) "
    SQL = SQL & "Inner Join tsoSOLine sl with(NoLock) "
    SQL = SQL & "    on so.sokey = sl.sokey "
    SQL = SQL & "Inner Join tsoSOLineExt_SGS sle with(NoLock) "
    SQL = SQL & "    on sl.solineKey = sle.solinekey "
    SQL = SQL & "    and sle.Awarded = 1 "
    SQL = SQL & "Inner Join tsoSOLineDist sld with(NOLock) "
    SQL = SQL & "    on sl.solinekey = sld.solinekey "
    SQL = SQL & "Left Outer Join timItem i with(NoLock) "
    SQL = SQL & "    on sl.ItemKey = i.ItemKey "
    SQL = SQL & "Left Outer Join timItemDescription ides with(NoLock) "
    SQL = SQL & "    on i.ItemKey = ides.ItemKey "
    SQL = SQL & "Left outer Join tciFOB f with(NoLock) "
    SQL = SQL & "    on sld.fobkey = f.fobkey "
    SQL = SQL & "Where 1 = 1 "
    SQL = SQL & "and so.TranNo = '" & TranNo & "' "
    SQL = SQL & "and so.CompanyID = '" & gbMASCompanyID & "' "
    SQL = SQL & "and sld.whsekey = " & gbMASWhseKey & " "
    SQL = SQL & "and so.TranType = 802   --Contract (BSO)"
    SQL = SQL & "and i.ItemID = '" & ItemID & "' "
                    
    RSGetBOLData.CursorLocation = adUseClient
    RSGetBOLData.CursorType = adOpenDynamic
    RSGetBOLData.Open SQL, gbMASConn, adOpenDynamic, adLockBatchOptimistic
    
    If RSGetBOLData.State = 1 Then
        If RSGetBOLData.RecordCount > 0 Then
            RSGetBOLData.MoveFirst
            GetMASFOB = ConvertToString(RSGetBOLData("FOBID").Value)
        End If
    End If
    
CleanUP:
    If RSGetBOLData.State = 1 Then
        RSGetBOLData.Close
    End If
    
    Set RSGetBOLData = Nothing


    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetMASFOB()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
        
End Function


'RKL DEJ 5/1/14 Added New Method (GetViscosityTemp):
Function GetViscosityTemp(ItemID As String, ItemShortDesc As String) As String
    On Error GoTo Error
    
    Dim lcItemKey As Long
    Dim lcViscosityTemp As String
    
    If IsObject(gbMASConn) <> True Then
        GetViscosityTemp = GetViscosityTempLoc(ItemID, ItemShortDesc)
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        GetViscosityTemp = GetViscosityTempLoc(ItemID, ItemShortDesc)
        Exit Function
    End If
    
    'Execute Stored Procedure.  The proc will either have the logic or lookup directly in the tables
    'Exec GetViscosityTemp gbMASCompanyID, ItemID, ItemShortDesc, gbMASWhseKey
    'If Error then: GetViscosityTemp = GetViscosityTempLoc(ItemID, ItemShortDesc)
    
    lcItemKey = GetMASItemKey(ItemID)
    If GetViscosityTempDatabase(lcItemKey, lcViscosityTemp) = True Then
        GetViscosityTemp = lcViscosityTemp
    Else
        GetViscosityTemp = GetViscosityTempLoc(ItemID, ItemShortDesc)
    End If
    
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetViscosityTemp()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Function


'RKL DEJ 5/1/14 Added New Method (GetViscosityTempLoc):
Function GetViscosityTempLoc(ItemID As String, ItemShortDesc As String) As String
    On Error GoTo Error
    
    '******************************************************************
    '5/1/14 - Kevin Requested the following logic: (START)
    '******************************************************************
    'The following groups of products need viscosity at 77F:    '
    'Quickseal
    'SCH
    'MSE
    'CQS
    'CSS
    'PMCSS
    'LMCQS
    'STE
    'ERAC
    'CPC
    'PPC
    'PMRE
    'RE
    '
    'These products need viscosity at 140F:
    '
    'CRS-2PUT (This is the only CRS that needs viscosity at 140F. All the others are tested at 50C)
    'Chr
    '
    'All the other emulsions need viscosity at 122F
    
'The following restrictions came on 5/7/14
    'No viscosity is needed for solutions, bases and Diluted products. These are label this way in their name. The DIL in these products our names for dilutes
    '
    '
    'The following need viscosity at 122F also
    'PMEM
    'SAFLEA_A
    'SAFLEA_B
    'CHFE -150
    'HFE -150
    'HFRS -2
    'HFRS-2P
    '******************************************************************
    '5/1/14 - Kevin Requested the following logic: (STOP)
    '******************************************************************
    
    Dim lcItemID As String
    Dim lcItemDesc As String
    
    'Cleanup Item and Description
    lcItemID = UCase(Trim(ItemID))
    lcItemDesc = UCase(Trim(ItemShortDesc))
    
    If lcItemID = "CRS-2PUT" Then
        GetViscosityTempLoc = "140"
        Exit Function
    End If
    
    If Mid(lcItemDesc, 1, 9) = "QUICKSEAL" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 7) = "SC-H118" Or Mid(lcItemDesc, 1, 6) = "SC-H50" Or Mid(lcItemDesc, 1, 7) = "SCH-118" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 3) = "MSE" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 3) = "CQS" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 3) = "CSS" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 5) = "PMCSS" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 5) = "LMCQS" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 4) = "ERAC" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 3) = "CPC" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 3) = "PPC" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 4) = "PMRE" Then
        GetViscosityTempLoc = "77"
    ElseIf Mid(lcItemDesc, 1, 2) = "RE" Then
        GetViscosityTempLoc = "77"
    
    ElseIf Mid(lcItemDesc, 1, 9) = "CRS-2P_UT" Then
        GetViscosityTempLoc = "140"
                        
    ElseIf Mid(lcItemDesc, 1, 3) = "CRS" Then
            GetViscosityTempLoc = "122"
            
    ElseIf Mid(lcItemDesc, 1, 3) = "CHR" Then
            GetViscosityTempLoc = "140"
            
    ElseIf lcItemDesc = "PMEM" Then
            GetViscosityTempLoc = "122"
            
    ElseIf lcItemDesc = "SAFLEA_A" Then
            GetViscosityTempLoc = "122"
            
    ElseIf lcItemDesc = "SAFLEA_B" Then
            GetViscosityTempLoc = "122"
            
    ElseIf lcItemDesc = "CHFE-150" Then
            GetViscosityTempLoc = "122"
            
    ElseIf lcItemDesc = "HFE-150" Then
            GetViscosityTempLoc = "122"
            
    ElseIf lcItemDesc = "HFRS-2" Then
            GetViscosityTempLoc = "122"
            
    ElseIf lcItemDesc = "HFRS-2P" Then
            GetViscosityTempLoc = "122"
            
    ElseIf InStr(1, lcItemDesc, "DIL") > 0 Then
            GetViscosityTempLoc = Empty
            
    Else
            GetViscosityTempLoc = "122"
            
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetViscosityTempLoc()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
End Function


'RKL DEJ 5/1/14 Added New Method (GetViscosityTempDatabase):
Function GetViscosityTempDatabase(ItemKey As Long, ByRef strViscosity As String) As Boolean
    On Error GoTo Error
    
    gbLastActivityTime = Date + Time

    On Error GoTo Error
    
    Dim SQL As String
    Dim lErrMsg As String
    Dim CMD As New ADODB.Command
        
    Dim lcViscosity As String
    
    gbLastActivityTime = Date + Time
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    Set CMD.ActiveConnection = gbMASConn
    CMD.CommandType = adCmdStoredProc
    CMD.CommandText = "spSOGetViscosityTempForScalePass_RKL"
    CMD.Parameters.Refresh
    CMD.CommandTimeout = 0      'Added to prevent time outs

    'Assign input parameter Values
    CMD.Parameters("@ItemKey").Value = ItemKey
    CMD.Parameters("@Viscosity").Value = lcViscosity
    
    CMD.Execute
    
    lcViscosity = ConvertToString(CMD.Parameters("@Viscosity").Value)
    
    strViscosity = lcViscosity
    
    GetViscosityTempDatabase = True
    
    
CleanUP:
    On Error Resume Next
    Set CMD = Nothing
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    lErrMsg = "basMethods.GetViscosityTempDatabase(ItemKey As Long) As Boolean" & vbCrLf & _
    "ItemKey = " & ItemKey & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function



Function IsTransferOrder(CommodityID As String) As Boolean
    On Error GoTo Error
    Dim lErrMsg As String
    
    If CommodityID <> Empty Then
        'If Left 3 = IPT or Left 2 = PT Then TO or New SO (Not from Blaket or existing SO)
        If UCase(Left(CommodityID, 3)) = "IPT" Or UCase(Left(CommodityID, 2)) = "PT" Or UCase(Left(CommodityID, 4)) = "PTCR" Then
            'Is Transfer
            IsTransferOrder = True
        Else
            'Is Not Transfer
            IsTransferOrder = False
        End If
    Else
        IsTransferOrder = False
    End If
    
    Exit Function
Error:
    lErrMsg = "basMethods.IsTransferOrder(CommodityID As String) As Boolean" & vbCrLf & _
    "CommodityID = " & CommodityID & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description
    
    MsgBox lErrMsg, vbCritical, "Scale Pass Error"
    
    Err.Clear
    
    gbLastActivityTime = Date + Time

End Function

'RKL DEJ added new function PrintLBS
Public Function PrintLBS(LoadsTableKey As Long, Optional IsReprint As Boolean = False, Optional NbrOfCopies As Integer = 1, Optional PrinterName As String) As Boolean
    On Error GoTo Error
    
    Dim prt As Printer
    Dim sDriverName As String
    Dim sDeviceName As String
    Dim sPort As String
    Dim i As Integer
    Dim CurrentLine As Integer
    Dim lErrMsg As String
    Dim LotNumber As String
    Dim PrintAnyWay As Variant
    
    Dim RptFileName As String
    
    CurrentLine = 1000
    If NbrOfCopies <= 0 Then
        NbrOfCopies = 1
    End If
    
    CurrentLine = 1002
    gbLastActivityTime = Date + Time
    
        
    CurrentLine = 1003
    RptFileName = "LoadingBatchSheet.rpt"
        
    CurrentLine = 1007
    Set crxApp = New CRAXDRt.Application
    CurrentLine = 1008
    Set crxRpt = crxApp.OpenReport(App.Path & "\Reports\" & RptFileName)
    
    CurrentLine = 1009
    
    'Printer
    If Trim(PrinterName) = Empty Then
        sDeviceName = gbSCALEPASSPRINTER
    Else
        sDeviceName = PrinterName
    End If
    
    CurrentLine = 1010
    For Each prt In Printers
        CurrentLine = 1011
        If prt.DeviceName = sDeviceName Then
            CurrentLine = 1012
            Set Printer = prt
            CurrentLine = 1013
            sDriverName = prt.DriverName
            CurrentLine = 1014
            sPort = prt.Port
            CurrentLine = 1015
            Exit For
        End If
        CurrentLine = 1016
    Next
    
    CurrentLine = 1017
    'Set the Scale Pass Printer
    crxRpt.SelectPrinter sDriverName, sDeviceName, sPort
        
    CurrentLine = 1024
    crxRpt.RecordSelectionFormula = "{Loads.LoadsTableKey} = " & LoadsTableKey
    
    CurrentLine = 1025
    If Mid(Command$, 1, 7) = "Testing" Then
        CurrentLine = 1026
        Call ShowCRViewer
        CurrentLine = 1027
    Else
        CurrentLine = 1028
        
        'Print the hard copy
        crxRpt.PrintOut False, NbrOfCopies
        
        CurrentLine = 1029
    End If
    
    CurrentLine = 1030
   
    CurrentLine = 1031
    
    PrintLBS = True
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:

    lErrMsg = "basMethods" & ".PrintLBS" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description & vbCrLf & vbCrLf & _
    "Last Line Executed  = " & CurrentLine & vbCrLf & vbCrLf
    
    InsErrorLog 0, Date + Time, lErrMsg
    
    MsgBox lErrMsg, vbExclamation, "Error"
    
    Err.Clear

    gbLastActivityTime = Date + Time
End Function

Public Function GetLMEMessage(LoadID) As String
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    Dim RtnVal As String
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
    
    SQL = "Select * From vluMcLeodBSOLoadData_RKL Where Order_ID = '" & LoadID & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        
    Else
        RS.MoveFirst
        'Records found
        RtnVal = Trim(ConvertToString(RS("McLeod Comments").Value))
        
        If RtnVal = "..." Then
            RtnVal = Empty
        End If
    End If
    
    GetLMEMessage = RtnVal
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    Exit Function
Error:
    MsgBox "basMethods.GetLMEMessage()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    GoTo CleanUP
End Function


'RKL DEJ 2016-06-29 (start)
Public Function GetCurrentData() As String
    On Error GoTo Error
    
    Dim frm As Form
    Dim strInfo As String
    Dim bGotLoadsTableKey As Boolean
    
    strInfo = "gbLoadsTableKey = " & CStr(gbLoadsTableKey) & vbCrLf
    
    For Each frm In Forms
        strInfo = strInfo & "Form " & Trim(frm.Name) & " is Open: " & vbCrLf
        
        Select Case UCase(frm.Name)
            Case "FRMBOLPREVIEW"
                strInfo = strInfo & "txtContract = " & Trim(frm.txtContract.Text) & " " & vbCrLf
                strInfo = strInfo & "txtBOLNbr = " & Trim(frm.txtBOLNbr.Text) & " " & vbCrLf
                strInfo = strInfo & "txtCarrier = " & Trim(frm.txtCarrierID.Text) & " " & vbCrLf
                strInfo = strInfo & "txtProduct = " & Trim(frm.txtProduct.Text) & " " & vbCrLf
                strInfo = strInfo & "txtAdditive = " & Trim(frm.txtAdditive.Text) & " " & vbCrLf
                strInfo = strInfo & "txtLotNr = " & Trim(frm.txtLotNr.Text) & " " & vbCrLf
                
            Case "FRMLOADPROCESSING"
                'Get the LoadsTableKey if it is available
                If IsObject(frm.rsLoad) = True Then
                    If Not frm.rsLoad Is Nothing Then
                        If frm.rsLoad.State = 1 Then
                            If Not frm.rsLoad.EOF And Not frm.rsLoad.BOF Then
                                strInfo = strInfo & "LoadsTableKey = " & ConvertToString(frm.rsLoad("LoadsTableKey").Value) & " " & vbCrLf
                                bGotLoadsTableKey = True
                            End If
                        End If
                    End If
                End If
                
                If bGotLoadsTableKey = False Then
                    strInfo = strInfo & "LoadsTableKey = " & CStr(frm.lcLoadsTableKey) & " " & vbCrLf
                End If
                
                strInfo = strInfo & "chSplitLoad = " & CStr(frm.chSplitLoad.Value) & " " & vbCrLf
                
                strInfo = strInfo & "txtFrontContract = " & Trim(frm.txtFrontContract.Text) & " " & vbCrLf
                strInfo = strInfo & "txtFrontBOLNbr = " & Trim(frm.txtFrontBOLNbr.Text) & " " & vbCrLf
                strInfo = strInfo & "txtFrontCommodity = " & Trim(frm.txtFrontCommodity.Text) & " " & vbCrLf
                strInfo = strInfo & "txtFrontProduct = " & Trim(frm.txtFrontProduct.Text) & " " & vbCrLf
                strInfo = strInfo & "txtFrontAdditive = " & Trim(frm.txtFrontAdditive.Text) & " " & vbCrLf
                strInfo = strInfo & "txtFrontFacilityID = " & Trim(frm.txtFrontFacilityID.Text) & " " & vbCrLf
            
                strInfo = strInfo & "txtrearContract = " & Trim(frm.txtRearContract.Text) & " " & vbCrLf
                strInfo = strInfo & "txtrearBOLNbr = " & Trim(frm.txtRearBOLNbr.Text) & " " & vbCrLf
                strInfo = strInfo & "txtrearCommodity = " & Trim(frm.txtRearCommodity.Text) & " " & vbCrLf
                strInfo = strInfo & "txtrearProduct = " & Trim(frm.txtRearProduct.Text) & " " & vbCrLf
                strInfo = strInfo & "txtrearAdditive = " & Trim(frm.txtRearAdditive.Text) & " " & vbCrLf
                strInfo = strInfo & "txtrearFacilityID = " & Trim(frm.txtRearFacilityID.Text) & " " & vbCrLf
            
        End Select
        
        strInfo = strInfo & vbCrLf
        
    Next
    
    GetCurrentData = strInfo
    
CleanUP:

    Exit Function
Error:
    MsgBox "basMethods.GetCurrentData()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    
    GoTo CleanUP
End Function
'RKL DEJ 2016-06-29 (stop)


'RKL DEJ 2016-09-02 (START)
Public Function GetCustPO(BSOTranNo As String, LME_PO As String) As String
    On Error GoTo Error
    
    Dim SQL As String
    Dim RS As New ADODB.Recordset
    
    gbLastActivityTime = Date + Time
    
    'Set Default
    GetCustPO = Empty
    
    If IsObject(gbMASConn) <> True Then
        Exit Function
    End If
    
    If gbMASConn.State <> 1 Then
        Exit Function
    End If
        
    SQL = "select so.SOKey, so.CustPONo, Coalesce(sox.UseCustPOForScalePass,0) 'UseBSOPO' "
    SQL = SQL & "from tsoSalesOrder so with(NOLock) "
    SQL = SQL & "inner Join tsoSalesOrderExt_SGS sox with(NoLock) "
    SQL = SQL & "    on so.SOKey = sox.SOKey "
    SQL = SQL & "    and so.TranType = 802 "
    SQL = SQL & "where so.CompanyID = '" & gbMASCompanyID & "' "
    SQL = SQL & "and so.TranNo = '" & BSOTranNo & "' "
    
    RS.CursorLocation = adUseClient
    RS.CursorType = adOpenForwardOnly
    
    RS.Open SQL, gbMASConn, adOpenForwardOnly
    
    If RS.RecordCount <= 0 Then
        'No records found
        GetCustPO = LME_PO
    Else
        RS.MoveFirst
        'Records found
        If RS("UseBSOPO").Value = 1 Then
            GetCustPO = RS("CustPONo").Value
        Else
            GetCustPO = LME_PO
        End If
        
    End If
    
    gbLastActivityTime = Date + Time
    
CleanUP:
    If Not RS Is Nothing Then
        If RS.State = 1 Then
            RS.Close
        End If
        
        Set RS = Nothing
    End If
    
    gbLastActivityTime = Date + Time
    Exit Function
Error:
    MsgBox "basMethods.GetCustPO()" & vbCrLf & _
    "Error: " & Err.Number & " " & Err.Description, vbExclamation, "Scale Pass Error"
    
    Err.Clear
    gbLastActivityTime = Date + Time
    
    GoTo CleanUP
End Function
'RKL DEJ 2016-09-02 (STOP)


