VERSION 5.00
Begin VB.Form frmFillInBOL 
   BackColor       =   &H00FF00FF&
   Caption         =   "Fill In BOL Data - Valero"
   ClientHeight    =   10980
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14235
   Icon            =   "frmFillInBOL.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   10980
   ScaleWidth      =   14235
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Caption         =   "Ok"
      Height          =   375
      Left            =   3840
      TabIndex        =   56
      Top             =   120
      Width           =   975
   End
   Begin VB.PictureBox picContainer 
      Height          =   9735
      Left            =   120
      ScaleHeight     =   9675
      ScaleWidth      =   13515
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   720
      Width           =   13575
      Begin VB.PictureBox picChild 
         Height          =   9615
         Left            =   0
         ScaleHeight     =   9555
         ScaleWidth      =   13395
         TabIndex        =   52
         TabStop         =   0   'False
         Top             =   0
         Width           =   13455
         Begin VB.TextBox txtLoadTemp 
            Height          =   300
            Left            =   5115
            TabIndex        =   35
            Top             =   7680
            Width           =   810
         End
         Begin VB.TextBox txtProdDesc 
            Height          =   300
            Left            =   9960
            TabIndex        =   47
            Top             =   9215
            Width           =   2055
         End
         Begin VB.TextBox txtNetTons 
            Height          =   300
            Left            =   12000
            TabIndex        =   48
            Top             =   9215
            Width           =   1250
         End
         Begin VB.TextBox txtTotalGrossWt 
            Height          =   300
            Left            =   9045
            Locked          =   -1  'True
            TabIndex        =   44
            Top             =   8385
            Width           =   1455
         End
         Begin VB.TextBox txtTotalTareWt 
            Height          =   300
            Left            =   10485
            Locked          =   -1  'True
            TabIndex        =   45
            Top             =   8385
            Width           =   1455
         End
         Begin VB.TextBox txtTotalNetWt 
            Height          =   300
            Left            =   11940
            Locked          =   -1  'True
            TabIndex        =   46
            Top             =   8385
            Width           =   1335
         End
         Begin VB.TextBox txtBOL 
            Height          =   375
            Left            =   11520
            TabIndex        =   0
            Top             =   530
            Width           =   1695
         End
         Begin VB.TextBox txtMASCustomer 
            Height          =   300
            Left            =   1320
            TabIndex        =   1
            Top             =   2025
            Width           =   3135
         End
         Begin VB.TextBox txtMASCustomerName 
            Height          =   300
            Left            =   4800
            TabIndex        =   2
            Top             =   2025
            Width           =   6735
         End
         Begin VB.TextBox txtContractNumber 
            Height          =   300
            Left            =   2040
            TabIndex        =   4
            Top             =   2370
            Width           =   1455
         End
         Begin VB.TextBox txtPurchas_Order 
            Height          =   300
            Left            =   4320
            TabIndex        =   5
            Top             =   2370
            Width           =   2295
         End
         Begin VB.TextBox txtMASFacilityName 
            Height          =   300
            Left            =   7440
            TabIndex        =   6
            Top             =   2370
            Width           =   3015
         End
         Begin VB.TextBox txtBOLPrintDate 
            Height          =   300
            Left            =   12120
            TabIndex        =   3
            Top             =   2025
            Width           =   1215
         End
         Begin VB.TextBox txtMASkDeliveryLoc 
            Height          =   300
            Left            =   2040
            TabIndex        =   7
            Top             =   2700
            Width           =   11295
         End
         Begin VB.TextBox txtDestDirections 
            Height          =   300
            Left            =   1440
            TabIndex        =   8
            Top             =   3000
            Width           =   11895
         End
         Begin VB.TextBox txtMASProjNumber 
            Height          =   300
            Left            =   1440
            TabIndex        =   9
            Top             =   3315
            Width           =   2055
         End
         Begin VB.TextBox txtMASProjDesc 
            Height          =   300
            Left            =   4320
            TabIndex        =   10
            Top             =   3315
            Width           =   9015
         End
         Begin VB.TextBox txtCarrierID 
            Height          =   300
            Left            =   1080
            TabIndex        =   11
            Top             =   3645
            Width           =   4455
         End
         Begin VB.TextBox txtWeightTktNbr 
            Height          =   300
            Left            =   11400
            TabIndex        =   12
            Top             =   3645
            Width           =   1935
         End
         Begin VB.TextBox txtTruckNo 
            Height          =   300
            Left            =   1320
            TabIndex        =   13
            Top             =   3960
            Width           =   1575
         End
         Begin VB.TextBox txtTrailer01 
            Height          =   300
            Left            =   4080
            TabIndex        =   14
            Top             =   3960
            Width           =   1455
         End
         Begin VB.TextBox txtTrailer02 
            Height          =   300
            Left            =   6720
            TabIndex        =   15
            Top             =   3960
            Width           =   1335
         End
         Begin VB.TextBox txtLoadTime 
            Height          =   300
            Left            =   9240
            TabIndex        =   16
            Top             =   3960
            Width           =   1335
         End
         Begin VB.TextBox txtDeliveryTime 
            Height          =   300
            Left            =   12000
            TabIndex        =   17
            Top             =   3960
            Width           =   1335
         End
         Begin VB.TextBox txtScaleInTime 
            Height          =   300
            Left            =   1560
            TabIndex        =   18
            Top             =   4320
            Width           =   1335
         End
         Begin VB.TextBox txtScaleOutTime 
            Height          =   300
            Left            =   4440
            TabIndex        =   19
            Top             =   4320
            Width           =   1095
         End
         Begin VB.TextBox txtRemarks 
            Height          =   300
            Left            =   240
            TabIndex        =   20
            ToolTipText     =   "Remarks"
            Top             =   4560
            Width           =   9495
         End
         Begin VB.TextBox txtMASPenatration 
            Height          =   300
            Left            =   4440
            TabIndex        =   22
            ToolTipText     =   "Penetration Temp"
            Top             =   5160
            Width           =   1455
         End
         Begin VB.TextBox txtMASpenetrationtemp 
            Height          =   300
            Left            =   6240
            TabIndex        =   23
            Top             =   5160
            Width           =   1455
         End
         Begin VB.TextBox txtFashPoint 
            Height          =   300
            Left            =   9360
            TabIndex        =   53
            Top             =   5160
            Width           =   1455
         End
         Begin VB.TextBox txtLbsPerGal 
            Height          =   300
            Left            =   12000
            TabIndex        =   25
            Top             =   5160
            Width           =   1335
         End
         Begin VB.TextBox txtSpecificGravity 
            Height          =   300
            Left            =   1680
            TabIndex        =   26
            Top             =   5520
            Width           =   1455
         End
         Begin VB.TextBox txtViscosity 
            Height          =   300
            Left            =   5400
            TabIndex        =   27
            ToolTipText     =   "Viscosity"
            Top             =   5520
            Width           =   1455
         End
         Begin VB.TextBox txtResidue 
            Height          =   300
            Left            =   9600
            TabIndex        =   29
            Top             =   5520
            Width           =   1455
         End
         Begin VB.TextBox txtLotNbr 
            Height          =   300
            Left            =   12360
            TabIndex        =   30
            Top             =   5520
            Width           =   975
         End
         Begin VB.TextBox txtTankNumber 
            Height          =   300
            Left            =   1200
            TabIndex        =   31
            Top             =   5820
            Width           =   1455
         End
         Begin VB.TextBox txtBatchNumber 
            Height          =   300
            Left            =   5280
            TabIndex        =   32
            Top             =   5820
            Width           =   1455
         End
         Begin VB.TextBox txtAdditiveDesc 
            Height          =   300
            Left            =   240
            TabIndex        =   34
            Top             =   7680
            Width           =   4935
         End
         Begin VB.TextBox txtGrossGallons 
            Height          =   300
            Left            =   5880
            TabIndex        =   36
            Top             =   7680
            Width           =   1215
         End
         Begin VB.TextBox txtNetGallons 
            Height          =   300
            Left            =   7080
            TabIndex        =   37
            Top             =   7680
            Width           =   1215
         End
         Begin VB.TextBox txtFrontGrossWt 
            Height          =   300
            Left            =   9045
            TabIndex        =   38
            Top             =   7755
            Width           =   1455
         End
         Begin VB.TextBox txtRearGrossWt 
            Height          =   300
            Left            =   9045
            TabIndex        =   41
            Top             =   8070
            Width           =   1455
         End
         Begin VB.TextBox txtFrontTareWt 
            Height          =   300
            Left            =   10485
            TabIndex        =   39
            Top             =   7755
            Width           =   1455
         End
         Begin VB.TextBox txtFrontNetWt 
            Height          =   300
            Left            =   11940
            TabIndex        =   40
            Top             =   7755
            Width           =   1335
         End
         Begin VB.TextBox txtRearTareWt 
            Height          =   300
            Left            =   10485
            TabIndex        =   42
            Top             =   8070
            Width           =   1455
         End
         Begin VB.TextBox txtRearNetWt 
            Height          =   300
            Left            =   11940
            TabIndex        =   43
            Top             =   8070
            Width           =   1335
         End
         Begin VB.TextBox txtMASCompliance 
            Height          =   300
            Left            =   1440
            TabIndex        =   33
            Top             =   6550
            Width           =   3855
         End
         Begin VB.TextBox txtSecurityTapeNbr 
            Height          =   300
            Left            =   9840
            TabIndex        =   21
            ToolTipText     =   "Security Tape Number"
            Top             =   4560
            Width           =   3495
         End
         Begin VB.TextBox txtViscosityTemp 
            Height          =   300
            Left            =   6960
            TabIndex        =   28
            ToolTipText     =   "Viscosity Temp"
            Top             =   5520
            Width           =   1455
         End
         Begin VB.Line Line1 
            X1              =   4560
            X2              =   4680
            Y1              =   2160
            Y2              =   2160
         End
         Begin VB.Label Label2 
            BackColor       =   &H80000005&
            Caption         =   "Lbs/gal:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   10920
            TabIndex        =   54
            Top             =   5160
            Width           =   1095
         End
         Begin VB.Label Label1 
            BackColor       =   &H80000005&
            Caption         =   "Desc:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   3600
            TabIndex        =   55
            Top             =   3315
            Width           =   1095
         End
         Begin VB.Image imgBOL 
            Height          =   9525
            Left            =   45
            Picture         =   "frmFillInBOL.frx":0442
            Top             =   45
            Width           =   13335
         End
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2640
      TabIndex        =   50
      Top             =   120
      Width           =   975
   End
   Begin VB.VScrollBar VScrollPic 
      Height          =   1575
      Left            =   13800
      Max             =   10
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   720
      Width           =   255
   End
   Begin VB.HScrollBar HScrollPic 
      Height          =   255
      Left            =   120
      Max             =   10
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   10560
      Width           =   3375
   End
End
Attribute VB_Name = "frmFillInBOL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const picHeightDiff = 1605
Const picWidthDiff = 735



Private Sub cmdCancel_Click()
    Unload Me

End Sub

Private Sub cmdOK_Click()
    'ADD CODE TO POPULATE REPORT
    MsgBox "Need to add code top print this data with the report.", vbExclamation, "Scale Pass"
    Unload Me
    
End Sub

Private Sub Form_Load()
    txtBOLPrintDate.Text = Date
End Sub

Private Sub Form_Resize()
    
    Dim picHeight As Long
    Dim picWidth As Long
    
    If Me.Height < picHeightDiff - 100 Then
        picHeight = picHeightDiff - 100
    Else
        picHeight = Me.Height - picHeightDiff
    End If
    
    If Me.Width < picWidthDiff - 100 Then
        picWidth = picWidthDiff - 100
    Else
        picWidth = Me.Width - picWidthDiff
    End If
    
    picContainer.Height = picHeight
    picContainer.Width = picWidth
    
    HScrollPic.Width = picWidth
    HScrollPic.Top = picContainer.Height + picContainer.Top + 100
    HScrollPic.Left = picContainer.Left
    
    VScrollPic.Height = picHeight
    VScrollPic.Left = picContainer.Width + picContainer.Left + 100
    VScrollPic.Top = picContainer.Top
    
    If imgBOL.Width <= picContainer.Width - 50 Then
        HScrollPic.Value = 0
        imgBOL.Left = 50
    End If
    
    If imgBOL.Height <= picContainer.Height - 50 Then
        VScrollPic.Value = 0
        imgBOL.Top = 50
    End If
    
    Call HScrollPic_Change
    
    Call VScrollPic_Change
    
End Sub

Private Sub HScrollPic_Change()
    
    Dim TotalDiff As Double
    Dim TotalDistance As Double
    Dim MovementAmt As Long
    Dim NewLeft As Long
    
    'Get the total distance
    TotalDistance = (picChild.Width + 100)
    
    'Get the Movement Amount
    TotalDiff = TotalDistance - picContainer.Width

    If HScrollPic.Value = 0 Then
        picChild.Left = 50
    Else
        MovementAmt = TotalDiff / 10
        
        If MovementAmt < 0 Then
            picChild.Left = 50
        Else
            NewLeft = (MovementAmt * HScrollPic.Value) * -1
            
            picChild.Left = NewLeft
        End If
    End If
    
End Sub


Private Sub txtAdditiveDesc_GotFocus()
    HighLightText
End Sub

Private Sub txtBatchNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtBOL_GotFocus()
   HighLightText
End Sub


Sub HighLightText()
   ActiveControl.SelStart = 0
   ActiveControl.SelLength = Len(ActiveControl.Text)
End Sub

Private Sub txtBOLPrintDate_GotFocus()
    HighLightText
End Sub

Private Sub txtBOLPrintDate_Validate(Cancel As Boolean)
    If IsDate(txtBOLPrintDate.Text) = False And Trim(txtBOLPrintDate.Text) <> Empty Then
        Cancel = True
        MsgBox "The Print Date Must be a valid date.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtCarrierID_GotFocus()
    HighLightText
End Sub

Private Sub txtContractNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtDeliveryTime_GotFocus()
    HighLightText
End Sub

Private Sub txtDestDirections_GotFocus()
    HighLightText
End Sub

Private Sub txtFashPoint_GotFocus()
    HighLightText
End Sub

Private Sub txtFashPoint_Validate(Cancel As Boolean)
    If Trim(txtFashPoint.Text) = Empty Then txtFashPoint.Text = 0
    
    If IsNumeric(txtFashPoint.Text) = False Then
        Cancel = True
        MsgBox "The FlashPoint must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtFrontGrossWt_Change()
    
    Dim Front As Double
    Dim Rear As Double
    Dim Total As Double
    
    If IsNumeric(txtFrontGrossWt.Text) = True Then
        Front = CDbl(txtFrontGrossWt.Text)
    Else
        Front = 0
    End If
    
    If IsNumeric(txtRearGrossWt.Text) = True Then
        Rear = CDbl(txtRearGrossWt.Text)
    Else
        Rear = 0
    End If
    
    Total = Front + Rear
    
    txtTotalGrossWt.Text = Total
    
End Sub

Private Sub txtFrontGrossWt_GotFocus()
    HighLightText
End Sub

Private Sub txtFrontGrossWt_Validate(Cancel As Boolean)

    If Trim(txtFrontGrossWt.Text) = Empty Then txtFrontGrossWt.Text = 0
    
    If IsNumeric(txtFrontGrossWt.Text) = False Then
        Cancel = True
        MsgBox "The Front Gross Wt must be a valid number.", vbInformation, "Scale Pass"
    End If
End Sub

Private Sub txtFrontNetWt_Change()
    Dim Front As Double
    Dim Rear As Double
    Dim Total As Double
    
    If IsNumeric(txtFrontNetWt.Text) = True Then
        Front = CDbl(txtFrontNetWt.Text)
    Else
        Front = 0
    End If
    
    If IsNumeric(txtRearNetWt.Text) = True Then
        Rear = CDbl(txtRearNetWt.Text)
    Else
        Rear = 0
    End If
    
    Total = Front + Rear
    
    txtTotalNetWt.Text = Total

End Sub

Private Sub txtFrontNetWt_GotFocus()
    HighLightText
End Sub

Private Sub txtFrontNetWt_Validate(Cancel As Boolean)
    If Trim(txtFrontNetWt.Text) = Empty Then txtFrontNetWt.Text = 0
    
    If IsNumeric(txtFrontNetWt.Text) = False Then
        Cancel = True
        MsgBox "The Front Net Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtFrontTareWt_Change()
    Dim Front As Double
    Dim Rear As Double
    Dim Total As Double
    
    If IsNumeric(txtFrontTareWt.Text) = True Then
        Front = CDbl(txtFrontTareWt.Text)
    Else
        Front = 0
    End If
    
    If IsNumeric(txtRearTareWt.Text) = True Then
        Rear = CDbl(txtRearTareWt.Text)
    Else
        Rear = 0
    End If
    
    Total = Front + Rear
    
    txtTotalTareWt.Text = Total

End Sub

Private Sub txtFrontTareWt_GotFocus()
    HighLightText
End Sub

Private Sub txtFrontTareWt_Validate(Cancel As Boolean)
    If Trim(txtFrontTareWt.Text) = Empty Then txtFrontTareWt.Text = 0
    
    If IsNumeric(txtFrontTareWt.Text) = False Then
        Cancel = True
        MsgBox "The Front Tare Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtGrossGallons_GotFocus()
    HighLightText
End Sub

Private Sub txtGrossGallons_Validate(Cancel As Boolean)
    If Trim(txtGrossGallons.Text) = Empty Then txtGrossGallons.Text = 0
    
    If IsNumeric(txtGrossGallons.Text) = False Then
        Cancel = True
        MsgBox "The Gross Gallons must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtLbsPerGal_GotFocus()
    HighLightText
End Sub

Private Sub txtLbsPerGal_Validate(Cancel As Boolean)
    If Trim(txtLbsPerGal.Text) = Empty Then txtLbsPerGal.Text = 0
    
    If IsNumeric(txtLbsPerGal.Text) = False Then
        Cancel = True
        MsgBox "The Lbs Per Gal must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtLoadTemp_GotFocus()
    HighLightText
End Sub

Private Sub txtLoadTime_GotFocus()
    HighLightText
End Sub

Private Sub txtLotNbr_GotFocus()
    HighLightText
End Sub

Private Sub txtMASCompliance_GotFocus()
    HighLightText
End Sub

Private Sub txtMASCustomer_GotFocus()
    HighLightText
End Sub

Private Sub txtMASCustomerName_GotFocus()
    HighLightText
End Sub

Private Sub txtMASFacilityName_GotFocus()
    HighLightText
End Sub

Private Sub txtMASkDeliveryLoc_GotFocus()
    HighLightText
End Sub

Private Sub txtMASPenatration_GotFocus()
    HighLightText
End Sub

Private Sub txtMASPenatration_Validate(Cancel As Boolean)
    If Trim(txtMASPenatration.Text) = Empty Then txtMASPenatration.Text = 0
    
    If IsNumeric(txtMASPenatration.Text) = False Then
        Cancel = True
        MsgBox "The Penatration must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtMASpenetrationtemp_GotFocus()
    HighLightText
End Sub

Private Sub txtMASpenetrationtemp_Validate(Cancel As Boolean)
    If Trim(txtMASpenetrationtemp.Text) = Empty Then txtMASpenetrationtemp.Text = 0
    
    If IsNumeric(txtMASpenetrationtemp.Text) = False Then
        Cancel = True
        MsgBox "The Penatration Temp must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtMASProjDesc_GotFocus()
    HighLightText
End Sub

Private Sub txtMASProjNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtNetGallons_GotFocus()
    HighLightText
End Sub

Private Sub txtNetGallons_Validate(Cancel As Boolean)
    If Trim(txtNetGallons.Text) = Empty Then txtNetGallons.Text = 0
    
    If IsNumeric(txtNetGallons.Text) = False Then
        Cancel = True
        MsgBox "The Net Gallons must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtNetTons_GotFocus()
    HighLightText
End Sub

Private Sub txtNetTons_Validate(Cancel As Boolean)
    If Trim(txtNetTons.Text) = Empty Then txtNetTons.Text = 0
    
    If IsNumeric(txtNetTons.Text) = False Then
        Cancel = True
        MsgBox "The Net Tons must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub


Private Sub txtProdDesc_GotFocus()
    HighLightText
End Sub

Private Sub txtPurchas_Order_GotFocus()
    HighLightText
End Sub

Private Sub txtRearGrossWt_Change()
    Dim Front As Double
    Dim Rear As Double
    Dim Total As Double
    
    If IsNumeric(txtFrontGrossWt.Text) = True Then
        Front = CDbl(txtFrontGrossWt.Text)
    Else
        Front = 0
    End If
    
    If IsNumeric(txtRearGrossWt.Text) = True Then
        Rear = CDbl(txtRearGrossWt.Text)
    Else
        Rear = 0
    End If
    
    Total = Front + Rear
    
    txtTotalGrossWt.Text = Total

End Sub

Private Sub txtRearGrossWt_GotFocus()
    HighLightText
End Sub

Private Sub txtRearGrossWt_Validate(Cancel As Boolean)
    If Trim(txtRearGrossWt.Text) = Empty Then txtRearGrossWt.Text = 0
    
    If IsNumeric(txtRearGrossWt.Text) = False Then
        Cancel = True
        MsgBox "The Rear Gross Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtRearNetWt_Change()
    Dim Front As Double
    Dim Rear As Double
    Dim Total As Double
    
    If IsNumeric(txtFrontNetWt.Text) = True Then
        Front = CDbl(txtFrontNetWt.Text)
    Else
        Front = 0
    End If
    
    If IsNumeric(txtRearNetWt.Text) = True Then
        Rear = CDbl(txtRearNetWt.Text)
    Else
        Rear = 0
    End If
    
    Total = Front + Rear
    
    txtTotalNetWt.Text = Total

End Sub

Private Sub txtRearNetWt_GotFocus()
    HighLightText
End Sub

Private Sub txtRearNetWt_Validate(Cancel As Boolean)
    If Trim(txtRearNetWt.Text) = Empty Then txtRearNetWt.Text = 0
    
    If IsNumeric(txtRearNetWt.Text) = False Then
        Cancel = True
        MsgBox "The Rear Net Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtRearTareWt_Change()
    Dim Front As Double
    Dim Rear As Double
    Dim Total As Double
    
    If IsNumeric(txtFrontTareWt.Text) = True Then
        Front = CDbl(txtFrontTareWt.Text)
    Else
        Front = 0
    End If
    
    If IsNumeric(txtRearTareWt.Text) = True Then
        Rear = CDbl(txtRearTareWt.Text)
    Else
        Rear = 0
    End If
    
    Total = Front + Rear
    
    txtTotalTareWt.Text = Total

End Sub

Private Sub txtRearTareWt_GotFocus()
    HighLightText
End Sub

Private Sub txtRearTareWt_Validate(Cancel As Boolean)
    If Trim(txtRearTareWt.Text) = Empty Then txtRearTareWt.Text = 0
    
    If IsNumeric(txtRearTareWt.Text) = False Then
        Cancel = True
        MsgBox "The Rear Tear Wt must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtRemarks_GotFocus()
    HighLightText
End Sub

Private Sub txtResidue_GotFocus()
    HighLightText
End Sub

Private Sub txtScaleInTime_GotFocus()
    HighLightText
End Sub

Private Sub txtScaleOutTime_GotFocus()
    HighLightText
End Sub

Private Sub txtSecurityTapeNbr_GotFocus()
    HighLightText
End Sub

Private Sub txtSpecificGravity_GotFocus()
    HighLightText
End Sub

Private Sub txtTankNumber_GotFocus()
    HighLightText
End Sub

Private Sub txtTotalGrossWt_GotFocus()
    HighLightText
End Sub

Private Sub txtTotalNetWt_GotFocus()
    HighLightText
End Sub

Private Sub txtTotalTareWt_GotFocus()
    HighLightText
End Sub

Private Sub txtTrailer01_GotFocus()
    HighLightText
End Sub

Private Sub txtTrailer02_GotFocus()
    HighLightText
End Sub

Private Sub txtTruckNo_GotFocus()
    HighLightText
End Sub

Private Sub txtViscosity_GotFocus()
    HighLightText
End Sub

Private Sub txtViscosity_Validate(Cancel As Boolean)
    If Trim(txtViscosity.Text) = Empty Then txtViscosity.Text = 0
    
    If IsNumeric(txtViscosity.Text) = False Then
        Cancel = True
        MsgBox "The Viscosity must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtViscosityTemp_GotFocus()
    HighLightText
End Sub

Private Sub txtViscosityTemp_Validate(Cancel As Boolean)
    If Trim(txtViscosityTemp.Text) = Empty Then txtViscosityTemp.Text = 0
    
    If IsNumeric(txtViscosityTemp.Text) = False Then
        Cancel = True
        MsgBox "The Viscosity Temp must be a valid number.", vbInformation, "Scale Pass"
    End If

End Sub

Private Sub txtWeightTktNbr_GotFocus()
    HighLightText
End Sub

Private Sub VScrollPic_Change()
    Dim TotalDiff As Double
    Dim TotalDistance As Double
    Dim MovementAmt As Long
    Dim NewTop As Long
    
    'Get the total distance
    TotalDistance = (picChild.Height + 100)
    
    'Get the Movement Amount
    TotalDiff = TotalDistance - picContainer.Height

    If VScrollPic.Value = 0 Then
        picChild.Top = 50
    Else
        MovementAmt = TotalDiff / 10
        
        If MovementAmt < 0 Then
            picChild.Top = 50
        Else
            NewTop = (MovementAmt * VScrollPic.Value) * -1
            
            picChild.Top = NewTop
        End If
    End If
End Sub
